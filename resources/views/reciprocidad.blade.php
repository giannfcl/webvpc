@extends('Layouts.layout')

@section('js-libs')
<!-- /librerias -->
<link href="{{URL::asset('css/pnotify.custom.min.css')}}" rel="stylesheet">

<script type="text/javascript" src="{{URL::asset('js/pnotify.custom.min.js')}}"></script>

<!-- START SIGMA IMPORTS -->
<script src="{{ URL::asset('js/sigma.js-1.2.1/src/sigma.core.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/src/conrad.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/src/utils/sigma.utils.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/src/utils/sigma.polyfills.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/src/sigma.settings.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/src/classes/sigma.classes.dispatcher.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/src/classes/sigma.classes.configurable.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/src/classes/sigma.classes.graph.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/src/classes/sigma.classes.camera.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/src/classes/sigma.classes.quad.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/src/classes/sigma.classes.edgequad.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/src/captors/sigma.captors.mouse.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/src/captors/sigma.captors.touch.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/src/renderers/sigma.renderers.canvas.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/src/renderers/sigma.renderers.webgl.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/src/renderers/sigma.renderers.svg.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/src/renderers/sigma.renderers.def.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/src/renderers/webgl/sigma.webgl.nodes.def.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/src/renderers/webgl/sigma.webgl.nodes.fast.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/src/renderers/webgl/sigma.webgl.edges.def.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/src/renderers/webgl/sigma.webgl.edges.fast.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/src/renderers/webgl/sigma.webgl.edges.arrow.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/src/renderers/canvas/sigma.canvas.labels.def.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/src/renderers/canvas/sigma.canvas.hovers.def.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/src/renderers/canvas/sigma.canvas.nodes.def.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/src/renderers/canvas/sigma.canvas.edges.def.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/src/renderers/canvas/sigma.canvas.edges.curve.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/src/renderers/canvas/sigma.canvas.edges.arrow.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/src/renderers/canvas/sigma.canvas.edges.curvedArrow.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/src/renderers/canvas/sigma.canvas.edgehovers.def.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/src/renderers/canvas/sigma.canvas.edgehovers.curve.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/src/renderers/canvas/sigma.canvas.edgehovers.arrow.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/src/renderers/canvas/sigma.canvas.edgehovers.curvedArrow.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/src/renderers/canvas/sigma.canvas.extremities.def.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/src/renderers/svg/sigma.svg.utils.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/src/renderers/svg/sigma.svg.nodes.def.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/src/renderers/svg/sigma.svg.edges.def.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/src/renderers/svg/sigma.svg.edges.curve.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/src/renderers/svg/sigma.svg.labels.def.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/src/renderers/svg/sigma.svg.hovers.def.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/src/middlewares/sigma.middlewares.rescale.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/src/middlewares/sigma.middlewares.copy.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/src/misc/sigma.misc.animation.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/src/misc/sigma.misc.bindEvents.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/src/misc/sigma.misc.bindDOMEvents.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/src/misc/sigma.misc.drawHovers.js')}}"></script>
<!-- END SIGMA IMPORTS -->

<!-- NO OVERLAP -->
<script src="{{ URL::asset('js/sigma.js-1.2.1/plugins/sigma.layout.noverlap/sigma.layout.noverlap.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/plugins/sigma.plugins.animate/sigma.plugins.animate.js')}}"></script>
<script src="{{ URL::asset('js/sigma.js-1.2.1/plugins/sigma.plugins.dragNodes/sigma.plugins.dragNodes.js')}}"></script>


<style>

.modalCargando {
	display:    none;
	position:   fixed;
	z-index:    1000;
	top:        0;
	left:       0;
	height:     100%;
	width:      100%;
	opacity: 0.7;
	background: #FFFFFF/*rgba( 255, 255, 255, .6 ) */
	url('https://k43.kn3.net/taringa/1/6/0/8/5/0/80/dnite/129.gif?3173') 
	50% 50% 
	no-repeat;
}


* { box-sizing: border-box; }

.autocomplete {
	/*the container must be positioned relative:*/
	position: relative;
	display: inline-block;
}
input {
	border: 1px solid transparent;
	background-color: #f1f1f1;
	padding: 10px;
	font-size: 14px;
}
input[type=text] {
	background-color: #f1f1f1;
	width: 100%;
}
input[type=submit] {
	background-color: DodgerBlue;
	/*color: #fff;*/
}
.autocomplete-items {
	position: absolute;
	border: 1px solid #d4d4d4;
	border-bottom: none;
	border-top: none;
	z-index: 99;
	/*position the autocomplete items to be the same width as the container:*/
	top: 100%;
	left: 0;
	right: 0;
}
.autocomplete-items div {
	padding: 10px;
	cursor: pointer;
	background-color: #fff; 
	border-bottom: 1px solid #d4d4d4;   
}
.autocomplete-items div:hover {
	/*when hovering an item:*/
	background-color: #e9e9e9; 
}
.autocomplete-active {
	/*when navigating through the items using the arrow keys:*/
	background-color: DodgerBlue !important; 
	color: #ffffff; 
}
.dummy {
    position: fixed;
    height: 100%;
    width: calc(100% - 190px);
    background: rgba(0,0,0,0.6);
    top: 0;
    left: 0;
    /* z-index: 9; */
    display: flex;
    JUSTIFY-CONTENT: CENTER;
    align-items: center;
    margin-left: 190px;
}
.dummy2 {
    max-height: 600px;
    max-width: 900px;
    background: white;
    border-radius: 20px;
    height: 85%;
    width: 80%;
}    
.container {
  display: flex; /* or inline-flex */
}
.dummy3 {
    max-height: 430px;
    max-width: 370px;
    background: white;
    border-radius: 20px;
    height: 85%;
    width: 80%;
}
.dummy4 {
    max-height: 500px;
    max-width: 750px;
    background: white;
    border-radius: 20px;
    height: 85%;
    width: 80%;
}

{
  box-sizing: border-box;
}

body {
  font: 16px Arial;  
}

/*the container must be positioned relative:*/
.autocomplete {
  position: relative;
  display: inline-block;
}

input {
  border: 1px solid transparent;
  background-color: #f1f1f1;
  padding: 10px;
  font-size: 16px;
}

input[type=text] {
  background-color: #f1f1f1;
  width: 100%;
}

input[type=submit] {
  background-color: DodgerBlue;
  color: #fff;
  cursor: pointer;
}

.autocomplete-items {
  position: absolute;
  border: 1px solid #d4d4d4;
  border-bottom: none;
  border-top: none;
  z-index: 99;
  /*position the autocomplete items to be the same width as the container:*/
  top: 100%;
  left: 0;
  right: 0;
}
   .maskAppear {
        display: flex !important;
        align-items: center;
        justify-content: center;
    }

.autocomplete-items div {
  padding: 10px;
  cursor: pointer;
  background-color: #fff; 
  border-bottom: 1px solid #d4d4d4; 
}
    #maskWaiting {
        position: fixed;
        background: rgba(0, 0, 0, 0.3);
        z-index: 999999999999999;
        top: 0;
        right: 0;
        width: 100vw;
        height: 100vh;
        display: none;
    }

    .NxtBtn {
        margin-top: 8px;
        font-size: 1.2em;
        padding-right: 20px;
        padding-left: 20px;
        padding-top: 10px;
        border-style: solid;
        border-radius: 9px;
        background: #4DD094;
        margin-left: 25px;
        font-weight: bold;
        padding-bottom: 10px;
        color: white;
        transition: 0.25s all;
        border-color: transparent;

    }
/*when hovering an item:*/
.autocomplete-items div:hover {
  background-color: #e9e9e9; 
}

/*when navigating through the items using the arrow keys:*/
.autocomplete-active {
  background-color: DodgerBlue !important; 
  color: #ffffff; 
}
</style>
@stop

@section('content')
<?php
if (isset($usuario)) {
    $usu = $usuario->ID;
}
?>
@section('pageTitle', 'Reciprocidad ')

<div class="row space"></div>
<div id="filtroNav" class=" section-dashboard row justify-content-md-center">
	<form action="{{route('Reciprocidad2')}}"> 
	@if($usu=='VPC')
    <div class="col-md-2 col-sm-2 col-xs-2 justify-content-center">
        <label>BANCA</label>
        <div class="form-group filtro">
            <select class="form-control" id="filtro1" name="filtro1">
                @if(count($bancas)>=1)
                @foreach($bancas as $b)
                @if($b['BANCA'] != null)
                @if($b['BANCA'] == 'BC')
                <option value="{{$b['BANCA']}}" selected><?php echo $b['BANCA'] ?></option>
                @else
                <option value="{{$b['BANCA']}}"><?php echo $b['BANCA'] ?></option>
                @endif
                @endif
                @endforeach
                @endif
            </select>
        </div>
    </div>
    @endif
    @if(!isset($zonal))
    <div class="col-md-2 col-sm-2 col-xs-2 justify-content-center">
        <label>ZONAL</label>
        <div class="form-group filtro">
            <select class="form-control" id="filtro2" name="filtro2">
                <option value="null" selected>Todos</option>
                @if(count($zonales)>=1)
                @foreach($zonales as $z)
                @if($z['NOMBRE_ZONAL'] != null)
                
                <option value="{{$z['NOMBRE_ZONAL']}}" {{($z['NOMBRE_ZONAL'] == $valores_filtro['zonal'])? 'selected="selected"':''}}
                                >{{$z['NOMBRE_ZONAL']}}</option>
                @endif
                @endforeach
                @endif
            </select>
        </div>
    </div>
    @endif
    @if(!isset($jefe) & !in_array($ROL,array('21','20')))
    <div class="col-md-2 col-sm-2 col-xs-2 justify-content-center">
        <label>JEFATURA </label>
        <div class="form-group filtro">
            <select class="form-control" id="filtro3" name="filtro3">
                <option value="null" selected>Todos</option>
                @if($jefes!=null && count($jefes)>=1)
                @foreach($jefes as $j)
                @if($j['NOMBRE_JEFE'] != null)
                {{$valores_filtro['jefe']}}
                <option value="{{$j['NOMBRE_JEFE']}}" {{($j['NOMBRE_JEFE'] == $valores_filtro['jefe'])? 'selected="selected"':''}}
                                >{{$j['NOMBRE_JEFE']}}</option>
                @endif
                @endforeach
                @endif
            </select>
        </div>
    </div>
    @endif
    @if(!isset($ejecutivo))
    <div class="col-md-2 col-sm-2 col-xs-2 justify-content-center">
        <label>ENCARGADO</label>
        <div class="form-group filtro">
            <select class="form-control" id="filtro4" name="filtro4">
                <option value="null" selected>Todos</option>
                @if($ejecutivos!=null && count($ejecutivos)>=1)
                @foreach($ejecutivos as $e)
                @if($e['ENCARGADO'] != null)
                <option value="{{$e['ENCARGADO']}}" {{($e['ENCARGADO'] == $valores_filtro['ejecutivo'])? 'selected="selected"':''}}
                                >{{$e['ENCARGADO']}}</option>
                @endif
                @endforeach
                @endif
            </select>
        </div>
    </div>
	    <div class="col-md-2 col-sm-2 col-xs-2">
	        <button class="NxtBtn"  name='idbuscarinciial' id="buscarfiltros" type="submit">Buscar</button>
	    </div>
    
    @endif
    </form>
</div>

<div class="row space"></div>
<div class="row section-dashboard">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Cumplimiento por carteras</h2>	
				<ul class="nav navbar-right panel_toolbox">
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<div class="row clearfix">				
					<div style="display:flex;align-items: center;    justify-content: flex-end;">
						<div >
							@if(in_array($ROL,array('20','21')))
							<button class="btn btn-primary" style="font-size: 16px;margin-top: 5px;"  id="btnNuevoCompromiso"> <i class="fas fa-plus"></i>Nuevo Compromiso
							</button>
							@endif															
						</div>
						<div >
							<button class="btn btn-danger" style="font-size: 16px;margin-top: 5px;"  id="btnSimularCompromiso"> <i class="fas fa-plus"></i>Simular Compromiso
							</button>															
						</div>
					</div>
					<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
					  <li class="nav-item">
					    <a class="active" id="pills-Cartera-tab" data-toggle="tab" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true" href="#Carteratab">Visión Cartera</a>
					  </li>
					  <li class="nav-item">
					    <a class="nav-link nav-active" id="pills-Ranking-tab" data-toggle="tab" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false" href="Rankingtab">Ranking Compromisos</a>
					  </li>					  
					</ul>			
					<div class="tab-content" id="pills-tabContent">
				        <div class="tab-pane active" id="pills-home" role="tabpanel" aria-labelledby="pills-Cartera-tab">
				        	<br> 	
						  	<div class="row">
						  		<div class="col-md-12 col-sm-12 col-xs-12">
						  			<label style="padding-left: 15px">Información al día</label>
						  		</div>
						  		<br>
						  		<div class="row">
									<div class="col-md-4 col-xs-6" style="margin-left: 25px">
										<select class="form-control" name="Opciones_Pestaña1" id="Opciones_Pestaña1" style="width: 250px;text-align: center;">
											<option>Mensual</option>
											<option>Acumulado</option>
										</select>
									</div>							  		
						  		</div>
						  		<br>
								<div class="form-group col-md-4">
	                        		<form action="{{route('Reciprocidad2')}}">
	                        			<div hidden="hidden"> 
	                        				<input type="text"  name='filtro1'  hidden="hidden" value="{{$valores_filtro['banca']}}">   
	                        				<input type="text" name='filtro2'  hidden="hidden" value="{{$valores_filtro['zonal']}} ">  
	                        				<input type="text" name='filtro3'  hidden="hidden" value="{{$valores_filtro['jefe']}}">   
	                        				<input type="text" name='filtro4'  hidden="hidden" value="{{$valores_filtro['ejecutivo']}}">  
	                        			</div>
	                        			<div>
		                        			<label for="" class="control-label col-md-6" style="padding-left: 14px">Razón Social:</label>
										  	<div class="autocomplete" style="width:300px;padding-left: 15px" >
										    	<input id="myInput" type="text" name="RazonSocial" placeholder="Razón Social">
										  	</div>
									  	</div>

									  	<br>
									  	<label for="" class="control-label col-md-6" style="padding-left: 15px;width:70px">CU:</label>                     			
		                        		<div class="col-md-2 col-xs-6" style="display: flex;align-items: center;padding-left: 10px">
											<input style="height: 30px;width: 150px;" class="formatInputNumber" type="text" value="" name="codUnico" id="rucBuscar" placeholder="Código Unico">
											<button  class="btn btn-primary btn-lg" type="submit" id='buscarnumdoc2'><i class="fas fa-search" style="cursor: pointer;"></i></button>
										</div>
									</form>
								</div>
								<br>
								<br>
									

								<div class="col-md-12 col-sm-12 col-xs-12">
						  			<div style="padding-left: 20px">
						  				<!-- Tabla Mensual  -->
						  				<div id="Tabla_Mensual" style="display: block">
							  				<center >
										  		<table class="table table-striped table-bordered jambo_table" style="align-content:center;">
										  			<thead>
										  				 <tr>
															<th style="vertical-align: middle; text-align: center;">Empresa</th>
												            <th style="vertical-align: middle; text-align: center;">Tipo</th>
												            <th style="vertical-align: middle; text-align: center;">Subtipo</th>
												            <th style="vertical-align: middle; text-align: center;">Imp prop</th>
												            <th style="vertical-align: middle; text-align: center;">Imp real</th>
												            <th style="vertical-align: middle; text-align: center;" style="width: 10px">Cumplimiento</th>
												            <th style="vertical-align: middle; text-align: center;" style="width: 10px">Desde</th>
												            <th style="vertical-align: middle; text-align: center;" style="width: 10px"> Hasta</th>
					      								</tr>
										  			</thead>
										  			<tbody>
										  				@if($elementostabla)
										  				@foreach($elementostabla as $elementotabla)
										  					<tr style="align-items: center;justify-content: center;">
										  						<td style="vertical-align: middle; text-align: center;">{{$elementotabla->NOMBRE}} </td>
										  						<td style="vertical-align: middle; text-align: center;">{{$elementotabla->TIPO_FINAL}}</td>
										  						<td style="vertical-align: middle; text-align: center;">{{$elementotabla->DESCRIPCION}}</td>
										  						<td style="vertical-align: middle; text-align: center;">{{number_format($elementotabla->IMPORTE_PROY_MENSUAL,0)}}</td>
										  						<td style="vertical-align: middle; text-align: center;">{{number_format($elementotabla->IMPORTE_REAL_ULTIMO_MES,0)}}</td>
										  						<td style="vertical-align: middle; text-align: center;">
										  							<div class="row" style="display: flex;justify-content: center">
										  								<div style="padding:4;color: {{($elementotabla->CUMPLIMIENTO_ULTIMO_MES<=80)?'red':(($elementotabla->CUMPLIMIENTO_ULTIMO_MES<100)?'yellow':'green')}}"  style="vertical-align: middle; text-align: center;"><i class="fas fa-circle"></i></div>
																		<div style="vertical-align: middle; text-align: center;">{{$elementotabla->CUMPLIMIENTO_ULTIMO_MES2}}</div>								  								
										  							</div>
										  						</td>
																<td style="vertical-align: middle; text-align: center;">{{$elementotabla->FECHA_DESDE}}</td>
																<td style="vertical-align: middle; text-align: center;">{{$elementotabla->FECHA_HASTA}}</td>								  					
										  					</tr>
										  				@endforeach
										  				@endif
										  			</tbody>
										  		</table>
									  		</center> 
								  		</div>
										<!-- Tabla Acumulado -->
										<div id="Tabla_Acumulada" style="display: none;">
											<center>
										  		<table class="table table-striped table-bordered jambo_table" style="align-content:center;">
										  			<thead>
										  				 <tr>
 															<th style="vertical-align: middle; text-align: center;">Empresa</th>
												            <th style="vertical-align: middle; text-align: center;">Tipo</th>
												            <th style="vertical-align: middle; text-align: center;">Subtipo</th>
												            <th style="vertical-align: middle; text-align: center;">Imp prop.</th>
												            <th style="vertical-align: middle; text-align: center;">Imp real</th>
												            <th style="vertical-align: middle; text-align: center;" style="width: 10px">Cumplimiento</th>
												            <th style="vertical-align: middle; text-align: center;" style="width: 10px">Desde</th>
												            <th style="vertical-align: middle; text-align: center;" style="width: 10px">Hasta</th>
												            <th style="vertical-align: middle; text-align: center;" style="width: 10px">Cumpl </th>

					      								</tr>
										  			</thead>
										  			<tbody>
										  				@if($elementostabla)
										  				@foreach($elementostabla as $elementotabla)
										  					<tr>
										  						<td style="vertical-align: middle; text-align: center;">{{$elementotabla->NOMBRE}}</td>
										  						<td style="vertical-align: middle; text-align: center;">{{$elementotabla->TIPO_FINAL}}</td>
										  						<td style="vertical-align: middle; text-align: center;">{{$elementotabla->DESCRIPCION}}</td>
										  						<td style="vertical-align: middle; text-align: center;">{{number_format($elementotabla->IMPORTE_PROY_ACUMULADO,0)}}</td>
										  						<td style="vertical-align: middle; text-align: center;">{{number_format($elementotabla->IMPORTE_REAL_ACUMULADO,0)}}</td>
										  						<td style="vertical-align: middle; text-align: center;">
										  							<div class="row" style="display: flex;justify-content: center;">
										  								<div style="padding:4;color: {{($elementotabla->CUMPLIMIENTO_ACUMULADO<=80)?'red':(($elementotabla->CUMPLIMIENTO_ACUMULADO<100)?'yellow':'green')}}"  style="vertical-align: middle; text-align: center;"><i class="fas fa-circle"></i></div>
																		<div  style="vertical-align: middle; text-align: center;">{{$elementotabla->CUMPLIMIENTO_ACUMULADO2}}</div>		
										  							</div>
										  						</td>
																<td style="vertical-align: middle; text-align: center;">{{$elementotabla->FECHA_DESDE}}</td>
																<td style="vertical-align: middle; text-align: center;">{{$elementotabla->FECHA_HASTA}}</td>
																<td style="vertical-align: middle; text-align: center;">{{$elementotabla->MESES_CUMPLIMIENTO}}/{{$elementotabla->MESES_OBJETIVO}}</td>										  					
										  					</tr>
										  				@endforeach
										  				@endif
										  			</tbody>
										  		</table>
									  		</center>								  		
								  		</div>
							  		</div>
						  		</div>
						  	</div>
				        </div>
					    <div class="tab-pane" id="pills-profile" role="tabpanel" aria-labelledby="pills-Ranking-tab">
					    	<br>
							<div class="row">					  	
						  		<div class="col-md-12 col-sm-12 col-xs-12">
							  			<label style="padding-left: 20px"><b>TOP 5</b></label>
							  	</div>
								<div class="col-md-12 col-sm-12 col-xs-12">
						  			<div style="padding-left: 20px">
								  		<table class="table table-striped table-bordered jambo_table">
								  			<thead>
								  				 <tr >
										            <th style="vertical-align: middle; text-align: center;">Ejecutivo</th>
										            <th style="vertical-align: middle; text-align: center;">Cliente</th>
										            <th style="vertical-align: middle; text-align: center;">Importe Propuesto</th>
										            <th style="vertical-align: middle; text-align: center;">Imp. Real</th>
										            <th style="vertical-align: middle; text-align: center;">Cumpl.</th>
			      								</tr>
								  			</thead>
								  			<tbody>
								  				@if($elementostablatop)
								  				@foreach($elementostablatop as $elementotabla)
								  					<tr class="parent" id="{{$elementotabla->COD_UNICO}}">
								  						<td style="vertical-align: middle; text-align: center;">{{$elementotabla->ENCARGADO}} <span class="btn btn-default">+</span></td>
								  						<td style="vertical-align: middle; text-align: center;">{{$elementotabla->NOMBRE}}</td>
								  						<td style="vertical-align: middle; text-align: center;">{{number_format($elementotabla->IMPORTE_PROY_ACUMULADO,0)}}</td>
	
								  						<td style="vertical-align: middle; text-align: center;">{{number_format($elementotabla->IMPORTE_REAL_ACUMULADO,0)}}</td>
								  						<td style="vertical-align: middle; text-align: center;">							  					
								  							<div class="row" style="display: flex;justify-content: center;">
								  								<div style="padding:4;color: {{($elementotabla->CUMPLIMIENTO<=80)?'red':(($elementotabla->CUMPLIMIENTO<100)?'yellow':'green')}}"  class="col-md-3"><i class="fas fa-circle"></i></div>
																<div  class="col-md-9">{{$elementotabla->CUMPLIMIENTO2}}</div>		  				
								  							</div>
								  						</td>								  						
								  					</tr>
								  					@if($elementostablatopdet[$elementotabla->COD_UNICO])
								  					<tr class="child-{{$elementotabla->COD_UNICO}}">
												      <td colspan=5>
												      	<table class="table table-striped table-bordered jambo_table">
												           <thead>
												            <tr>
												              <th style="vertical-align: middle; text-align: center;">Tipo</th>
												              <th style="vertical-align: middle; text-align: center;">Subtipo</th>
												              <th style="vertical-align: middle; text-align: center;">Imp. Propuesto</th>
												              <th style="vertical-align: middle; text-align: center;">Imp. Real</th>
												              <th style="vertical-align: middle; text-align: center;">Cumpl.</th>
												              <th style="vertical-align: middle; text-align: center;">Desde</th>
												              <th style="vertical-align: middle; text-align: center;">Hasta</th>
												              <th style="vertical-align: middle; text-align: center;">Cumpli</th>
												            </tr>
												          </thead>
								  					@foreach($elementostablatopdet[$elementotabla->COD_UNICO] as $elementotabladet)
												          <tbody>
												            <tr>
												              <td style="vertical-align: middle; text-align: center;">{{$elementotabladet->TIPO}} </td>
												              <td style="vertical-align: middle; text-align: center;">{{$elementotabladet->DESCRIPCION}}</td>
												              <td style="vertical-align: middle; text-align: center;">{{number_format($elementotabladet->IMPORTE_PROY_ACUMULADO2,0)}}</td>
												              <td style="vertical-align: middle; text-align: center;">{{number_format($elementotabladet->IMPORTE_REAL_ACUMULADO2,0)}}</td>
												              <td>
												              <div style="padding:4;color: {{($elementotabladet->CUMPLIMIENTO_ACUMULADO<=80)?'red':(($elementotabladet->CUMPLIMIENTO_ACUMULADO<100)?'yellow':'green')}}"  class="col-md-3"><i class="fas fa-circle"></i></div>
																<div  class="col-md-9">{{$elementotabladet->CUMPLIMIENTO_ACUMULADO2}}</div>	
															  </td>
												              <td style="vertical-align: middle; text-align: center;">{{$elementotabladet->FECHA_DESDE}}</td>
												              <td style="vertical-align: middle; text-align: center;">{{$elementotabladet->FECHA_HASTA}}</td>
												              <td style="vertical-align: middle; text-align: center;">{{$elementotabladet->MESES_CUMPLIMIENTO}}/{{$elementotabladet->MESES_CUMPLIMIENTO2}}</td>
												            </tr>
												          </tbody>
												          @endforeach
												      </table></td>
												    </tr> 
												    @endif
								  				@endforeach
								  				@endif
								  			</tbody>
								  		</table>
							  		</div>
						  		</div>
						  	</div>						  								
						  	<div class="row">					  	
						  		<div class="col-md-12 col-sm-12 col-xs-12">
							  			<label style="padding-left: 20px"><b>BOTTOM 5</b></label>
							  	</div>
								<div class="col-md-12 col-sm-12 col-xs-12">
						  			<div style="padding-left: 20px">
								  		<table class="table table-striped table-bordered jambo_table">
								  			<thead>
								  				 <tr>
										            <th style="vertical-align: middle; text-align: center;">Ejecutivo</th>
										            <th style="vertical-align: middle; text-align: center;">Cliente</th>
										            <th style="vertical-align: middle; text-align: center;">Importe Propuesto</th>
										            <th style="vertical-align: middle; text-align: center;">Imp. Real</th>
										            <th style="vertical-align: middle; text-align: center;">Cumpl.</th>								            				
			      								</tr>
								  			</thead>
								  			<tbody>
												@if($elementostablabutton)
								  				@foreach($elementostablabutton as $elementotabla)
								  					<tr class="parent" id="{{$elementotabla->COD_UNICO}}-b">
								  						<td style="vertical-align: middle; text-align: center;">{{$elementotabla->ENCARGADO}} <span class="btn btn-default">+</span></td>
								  						<td style="vertical-align: middle; text-align: center;">{{$elementotabla->NOMBRE}}</td>
								  						<td style="vertical-align: middle; text-align: center;">{{number_format($elementotabla->IMPORTE_PROY_ACUMULADO,0)}}</td>
	
								  						<td style="vertical-align: middle; text-align: center;">{{number_format($elementotabla->IMPORTE_REAL_ACUMULADO,0)}}</td>
								  						<td style="vertical-align: middle; text-align: center;">							  					
								  							<div class="row" style="display: flex;justify-content: center;">
								  								<div style="padding:4;color: {{($elementotabla->CUMPLIMIENTO<=80)?'red':(($elementotabla->CUMPLIMIENTO<100)?'yellow':'green')}}"  class="col-md-3"><i class="fas fa-circle"></i></div>
																<div  class="col-md-9">{{$elementotabla->CUMPLIMIENTO2}}</div>		  				
								  							</div>
								  						</td>								  						
								  					</tr>
								  					@if($elementostablabuttondet[$elementotabla->COD_UNICO])
								  					<tr class="child-{{$elementotabla->COD_UNICO}}-b">
												      <td colspan=5>
												      	<table class="table table-striped table-bordered jambo_table">
												           <thead>
												            <tr>
												              <th style="vertical-align: middle; text-align: center;">Tipo</th>
												              <th style="vertical-align: middle; text-align: center;">Subtipo</th>
												              <th style="vertical-align: middle; text-align: center;">Imp. Propuesto</th>
												              <th style="vertical-align: middle; text-align: center;">Imp. Real</th>
												              <th style="vertical-align: middle; text-align: center;">Cumpl.</th>
												              <th style="vertical-align: middle; text-align: center;">Desde</th>
												              <th style="vertical-align: middle; text-align: center;">Hasta</th>
												              <th style="vertical-align: middle; text-align: center;">Cumpli</th>
												            </tr>
												          </thead>
								  					@foreach($elementostablabuttondet[$elementotabla->COD_UNICO] as $elementotabladet)
												          <tbody>
												            <tr>
												              <td style="vertical-align: middle; text-align: center;">{{$elementotabladet->TIPO}} </td>
												              <td style="vertical-align: middle; text-align: center;">{{$elementotabladet->DESCRIPCION}}</td>
												              <td style="vertical-align: middle; text-align: center;">{{number_format($elementotabladet->IMPORTE_PROY_ACUMULADO2,0)}}</td>
												              <td style="vertical-align: middle; text-align: center;">{{number_format($elementotabladet->IMPORTE_REAL_ACUMULADO2,0)}}</td>
												              <td>
												              <div style="padding:4;color: {{($elementotabladet->CUMPLIMIENTO_ACUMULADO<=80)?'red':(($elementotabladet->CUMPLIMIENTO_ACUMULADO<100)?'yellow':'green')}}"  class="col-md-3"><i class="fas fa-circle"></i></div>
																<div  class="col-md-9">{{$elementotabladet->CUMPLIMIENTO_ACUMULADO2}}</div>	
															  </td>
												              <td style="vertical-align: middle; text-align: center;">{{$elementotabladet->FECHA_DESDE}}</td>
												              <td style="vertical-align: middle; text-align: center;">{{$elementotabladet->FECHA_HASTA}}</td>
												              <td style="vertical-align: middle; text-align: center;">{{$elementotabladet->MESES_CUMPLIMIENTO}}/{{$elementotabladet->MESES_CUMPLIMIENTO2}}</td>
												            </tr>
												          </tbody>
												          @endforeach
												      </table></td>
												    </tr> 
												    @endif
								  				@endforeach
								  				@endif
								  			</tbody>
								  		</table>
							  		</div>
						  		</div>
						  	</div>
					    </div>	
			    	</div>
		    	</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel hidden" style="min-height: 650px" id="panelGrafo">		
			<div class="x_content">
				<div id="leyendaGrafo" hidden>
					<div class="form-group">
						<label style="margin-top: 10px" class="control-label col-md-3">Leyenda: </label>
					</div><br><br>
					<div class="form-group">
						<div class="col-md-1" style="width: 50px;padding-right: 0px;">
							<i class="fa fa-circle" style="font-size: 40px;color:#92D050;"></i>
						</div>
						<label style="margin-top: 10px" class="control-label col-md-11">Cliente IBK</label>
					</div><br><br>
					<div class="form-group">
						<div class="col-md-1" style="width: 50px;padding-right: 0px;">
							<i class="fa fa-circle" style="font-size: 40px;color:#FFD966;"></i>
						</div>
						<label style="margin-top: 10px" class="control-label col-md-11">No Cliente IBK</label>
					</div><br><br>
					<div class="form-group">
						<div class="col-md-1" style="width: 50px;padding-right: 0px;">
							<img src = "{{ URL::asset('img/click.gif') }}" style="width: 40px;height: 40px">
						</div>
						<label style="margin-top: 10px" class="control-label col-md-11">Detalle</label>
					</div>					
				</div>
				<div id="container">
					<style>
						#graph-container {
							top: 0;
							bottom: 0;
							left: 0;
							right: 0;
							position: absolute;
						}
					</style>
					<div id="graph-container" style="min-height: 620px;">
						<div id="noEncontrado" hidden>
							<center>
								<h3 style="color:#8CD4F5">No se encontraron resultados <i class="fa fa-sitemap"></i> </h3>
							</center>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel hidden" style="min-height: 650px" id="panelEmpresa">
			<div class="x_title">
				<div class="col-md-12">
					<h4 id="nombreEmpresa">Información</h4>	
				</div>
				<div class="col-md-12">
					<p style="margin-bottom: 0px;">*Montos expresados en miles</p>
				</div>		
				<ul class="nav navbar-right panel_toolbox"></ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_title">	
				<ul class="nav navbar-right panel_toolbox"></ul>
				<div class="clearfix" id="clearfixInfoPrincipal"></div>
			</div>
			<div class="x_title">
				<ul class="nav navbar-right panel_toolbox"></ul>
				<div class="clearfix" id="clearfixInfoDeuda"></div>
			</div>
			<!--<div class="x_title">
				<ul class="nav navbar-right panel_toolbox"></ul>
				<div class="clearfix" id="clearfixCashInOut"></div>
			</div>-->
			<div class="x_title">
				<ul class="nav navbar-right panel_toolbox"></ul>
				<div class="clearfix" id="clearfixAsignacionEcosistema"></div>
			</div>
			<div class="x_content">
				<div id="informacionEmpresa">

				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="ModelGenerarCompromiso">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="Cerrar_Generar_Compromiso"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Generar Compromiso</h4>
            </div>
        <div  class="dummy" id="Vista_inicial_reciprocidad">
				<div class="dummy2">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_title" style="padding-left: 15px;">
								<h2><b>Generar un compromiso</b></h2>	
								<ul class="nav navbar-right panel_toolbox">
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<div class="row clearfix">				
									<div class="form-group col-md-3 col-xs-6">
										<div class="autocomplete col-md-12" style="display: flex;align-items: center;">
											<input style="height: 11px" class="formatInputNumber" type="text" value="" name="Código Único" id="BuscCódigoÚnico" placeholder="Código Único"><i id='buscarnumdoc' class="fas fa-search" style="cursor: pointer;"></i>
										</div>
									</div>
								</div>
							</div>
							<div class="container">
								  <div class="row">
								    <div style="margin-left: 17px" class="col-md-12"> <k><b>Razón Social :</b></k> </div>
								  </div>
							</div>
							<div class="x_content">
								<div class="row clearfix">				
									<div class="form-group col-md-3 col-xs-6">
										<div class="autocomplete col-md-12" >
											<input style="height: 11px"  type="text" value="" name="Empresa Razón SAC" id="EmpresaRazónSAC" disabled="disabled" placeholder="Empresa Razón SAC">
										</div>
									</div>
								</div>
							</div>				 
							<div class="container">
							  <div class="row">
							    <div style="margin-left: 17px" class="col-md-12"> <k><b>Concepto </b></k> </div>
							  </div>
							</div>
							<div class="container">
							 	<div class="row" style="display:flex;justify-content: flex-end;padding-left: 28px;">
									<div >
										<button class="btn btn-primary" style="font-size: 10px;margin-top: 5px;" type="button" id="btnRenovación" >Renovación de linea</button>	
									</div>
									<div >
										<button class="btn btn-primary" style="font-size: 10px;margin-top: 5px;" type="button" id="btnOperación" >Operación puntual</button>	
									</div>
									<div >
										<button class="btn btn-primary" style="font-size: 10px;margin-top: 5px;" type="button" id="btnRevisión" >Revisión de comisiones pref</button>						
									</div>			
							  	</div>
							</div>
							<div class="container">
								<div class="row">
								    <div style="margin-left: 17px" class="col-md-12"> <k><b>Tipo </b></k> </div>
								  
								  	<div class="row" style="display:flex;justify-content: flex-end;">
										<div >
											<button class="btn btn-primary" style="font-size: 10px;margin-top: 5px;" type="button" id="btncASHIN">CASH IN</button>					
										</div>
										<div>
											<button class="btn btn-primary" style="font-size: 10px;margin-top: 5px;" type="button" id="btnCASHOUT" >CASH OUT</button>		
										</div>
									</div>		
								</div>													
							</div>

							<div class="x_title">
								<ul class="nav navbar-right panel_toolbox">
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div class="container">
										<div class="row">
										    <div style="margin-left: 17px" class="col-md-12"> <k><b>Subtipo: </b></k> </div>
										</div>
								 	</div>
									<div class="x_content">
										<div class="row clearfix">				
											<div class="form-group col-md-1 col-xs-12" style="padding-left: 25px;">
												<select class="form-control" name="Opciones" style="width: 350px" id="ComboSubtipo" >
												</select>
											</div>
										</div>
									</div>
									<div class="container">
									  <div class="row" style="padding-left: 12px">
									    <div style="margin-left: 15px" class="col-md-12" > <k><b>Importe Mensual: </b></k> </div>
									  </div>
								 	</div>
									<div class="x_content">
										<div class="row">
											<div class="form-group col-md-3 col-xs-6" style="padding-left: 18px;">
												<div class="autocomplete col-md-12" >
													<input style="height: 11px;width: 150px" class="formatInputNumber" type="number"  value="" name="ImporteMensual" id="IngresarImporte" placeholder="S/">
												</div>
											</div>
											<div class="container">
											  <div class="row" align="center">
											    <div class="col-md-2 col-xs-4" style="padding-left: 45px"> Desde:  </div>
											    <div class="col-md-2 col-xs-4" 	style="padding-left:210px"> Hasta:  </div>
											  </div>
										 	</div>
										 	<div class="row" style="display:flex;justify-content: flex-end;">	
												<div  style="padding-left: 20px;" >
													<select class="form-control" name="Opciones" style="width: 100px" id="ComboDesde">
														<option disabled="disabled" value="1" >Seleccione una opcion:</option>
														<option>201904 </option>
														<option>201905 </option>
														<option>201906 </option>
														<option>201907 </option>
														<option>201908 </option>
														<option>201909 </option>
														<option>201910 </option>
														<option>201911 </option>
														<option>201912 </option>
													</select>
												</div>
												<div  style="padding-left: 120px;" >
													<select class="form-control" name="Opciones" style="width: 100px " id="ComboGuardar">
														<option disabled="disabled" value="1" >Seleccione una opcion:</option>
														<option>201910</option>
														<option>201911</option>
														<option>201912</option>
														<option>202001</option>
														<option>202002 </option>
														<option>202003 </option>
														<option>202004 </option>
														<option>202005 </option>
														<option>202006 </option>
														<option>202007 </option>
														<option>202008 </option>
														<option>202009 </option>
														<option>202010 </option>
														<option>202011 </option>
														<option>202012 </option>
													</select>
												</div>	
											</div>	
											<br>	
											<div class="container">
												<div class="row" style="display:flex;justify-content: flex-end;padding-left: 220px;">
													<div>
														<button class="btn btn-primary" style="font-size: 15px;margin-top: 5px;width: 80px;" type="button" id="btnSalir" >Salir 
														</button>						
													</div>
													<div >
														<button class="btn btn-primary" style="font-size: 15px;margin-top: 5px;width: 80px;background-color: green;" type="button" id="btnGuardar" >Guardar</button>
													</div>																						
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>

						</div>
					</div>
				</div>
        </div><!-- /.modal-content -->
    	</div><!-- /.modal-dialog -->
	</div> 
</div>

<div class="modal" tabindex="-1" role="dialog" id="ModelConfirmarCompromiso"> 
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="Cerrar_Confirmar_Compromiso"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Confirmar Compromiso</h4>
            </div>
           	<div class="dummy3">
				<br>
				<div>
					<form>
						<label style="font-size: 11px;padding-left: 15px;">
							Se generará el siguiente compromiso:
						</label>
						<br>
						<br>
						<label style="font-size: 11px;padding-left: 25px;" id="nombreEmpresalbl">
							Empresa:  <span id="Empresallevar"></span>
						</label>
						<br>
						<br>
						<label style="font-size: 11px;padding-left: 25px;" id="Conceptoguardar">
							Concepto: <span id="Conceptollevar"></span>
						</label>
						<br>
						<br>
						<label style="font-size: 11px;padding-left: 25px;" id="TipoGuardar">
							Tipo :  <span id="Tipollevar"></span>
						</label> 
						<br>
						<br>
						<label style="font-size: 11px;padding-left: 25px;" id="SubtipoGuardar">
							Subtipo: <span id="Subtipollevar"></span>
						</label>
						<br>
						<br>
						<label style="font-size: 11px;padding-left: 25px;" id="ImporteGuardar">
							Importe Mensual:    <span id="Importellevar"></span>
						</label>
						<br>
						<br>
						<div class="container">
							<div class="row" align="center">
							    <div class="col-md-2 col-xs-4" style="padding-left: 35px"> Desde:  </div>
							    <div class="col-md-2 col-xs-4" style="padding-left:160px"> Hasta:  </div>
							</div>					
					 	</div>
						<div class="container">
							<div class="row" align="center">
							    <div class="col-md-1 col-xs-6" style="padding-left: 50px" id="DesdeGuardar"> <label><span id="desdellevar"></span></label> </div>
							    <div class="col-md-1 col-xs-6" style="padding-left:170px" id="HastaGuardar"> <label><span id="hastallevar"></span></label> </div>
							</div>					
					 	</div>				
					 	<br>
						<div class="container">
							<div class="row" align="center">
								<div class="col-md-1 col-xs-4" style="padding-left: 80px">
									<button class="btn btn-primary" style="font-size: 15px;margin-top: 5px;width: 80px;" type="button" id="btnSalir2" >Salir 
									</button>						
								</div>
								<div class="col-md-1 col-xs-4" style="padding-left: 80px;margin-left: 0px;padding-right: 0px;">
									<button type="submit" class="btn btn-primary" style="font-size: 15px;margin-top: 5px;width: 90px;background-color: green;" type="button" id="btnConfirmar">Confirmar</button>
								</div>									
							</div>
						</div>
					</form>
				</div>
			</div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div> 

<div class="modal" tabindex="-1" role="dialog" id="ModelSimularCompromiso">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="Cerrar_Generar_Compromiso"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Simular Compromiso</h4>
            </div>
        <div  class="dummy" id="Vista_inicial_reciprocidad">
				<div class="dummy4">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_title" style="padding-left: 15px;">
								<h2><b>Simular un compromiso</b></h2>	
								<ul class="nav navbar-right panel_toolbox">
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<div class="row clearfix">				
									<div class="form-group col-md-3 col-xs-6">
										<div class="autocomplete col-md-12" style="display: flex;align-items: center;">
											<input style="height: 11px" class="formatInputNumber" type="text" value="" name="Código Único" id="BuscCódigoÚnicoSimular" placeholder="Código Único"><i id='buscarnumdocSimular' class="fas fa-search" style="cursor: pointer;"></i>
										</div>
									</div>
								</div>
							</div>
							<div class="container">
								  <div class="row">
								    <div style="margin-left: 17px" class="col-md-12"> <k><b>Razón Social :</b></k> </div>
								  </div>
							</div>
							<div class="x_content">
								<div class="row clearfix">				
									<div class="form-group col-md-3 col-xs-6">
										<div class="autocomplete col-md-12" >
											<input style="height: 11px"  type="text" value="" name="Empresa Razón SAC" id="EmpresaRazónSACSimular" disabled="disabled" placeholder="Empresa Razón SAC">
										</div>
									</div>
								</div>
							</div>				 
							<div class="container">
								<div class="row">
								    <div style="margin-left: 17px" class="col-md-12"> <k><b>Tipo </b></k> </div>
								  
								  	<div class="row" style="display:flex;justify-content: flex-end;">
										<div >
											<button class="btn btn-primary" style="font-size: 10px;margin-top: 5px;" type="button" id="btncASHINSimular">CASH IN</button>					
										</div>
										<div>
											<button class="btn btn-primary" style="font-size: 10px;margin-top: 5px;" type="button" id="btnCASHOUTSimular" >CASH OUT</button>		
										</div>
									</div>		
								</div>													
							</div>

							<div class="x_title">
								<ul class="nav navbar-right panel_toolbox">
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div class="container">
										<div class="row">
										    <div style="margin-left: 17px" class="col-md-12"> <k><b>Subtipo: </b></k> </div>
										</div>
								 	</div>
									<div class="x_content">
										<div class="row clearfix">				
											<div class="form-group col-md-1 col-xs-12" style="padding-left: 25px;">
												<select class="form-control" name="Opciones" style="width: 350px" id="ComboSubtipoSimular" >
												</select>
											</div>
										</div>
									</div>
									<div class="x_content">
										<div class="row">
											<div class="container">
											  <div class="row" align="center">
											    <div class="col-md-2 col-xs-4" style="padding-left: 45px"> Desde:  </div>
											    <div class="col-md-2 col-xs-4" 	style="padding-left:210px"> Hasta:  </div>
											  </div>
										 	</div>
										 	<div class="row" style="display:flex;justify-content: flex-end;">	
												<div  style="padding-left: 30px;" >
													<select class="form-control" name="Opciones" style="width: 100px" id="ComboDesdeSimular">
														<option disabled="disabled" value="1" >Seleccione una opcion:</option>
														<option>201904 </option>
														<option>201905 </option>
														<option>201906 </option>
														<option>201907 </option>
														<option>201908 </option>
														<option>201909 </option>
														<option>201910 </option>
														<option>201911 </option>
														<option>201912 </option>
													</select>
												</div>
												<div  style="padding-left: 140px;" >
													<select class="form-control" name="Opciones" style="width: 100px " id="ComboGuardarSimular">
														<option disabled="disabled" value="1" >Seleccione una opcion:</option>
														<option>201910</option>
														<option>201911</option>
														<option>201912</option>
														<option>202001</option>
														<option>202002 </option>
														<option>202003 </option>
														<option>202004 </option>
														<option>202005 </option>
														<option>202006 </option>
														<option>202007 </option>
														<option>202008 </option>
														<option>202009 </option>
														<option>202010 </option>
														<option>202011 </option>
														<option>202012 </option>
													</select>
												</div>	
											</div>																					
											<div class="container">
												<div class="row" style="display:flex;justify-content: flex-end;padding-left: 70px;">
													<div>
														<button class="btn btn-primary" style="font-size: 15px;margin-top: 5px;width: 80px;" type="button" id="btnSalirSimular" >Salir 
														</button>						
													</div>
													<div >
														<button class="btn btn-primary" style="font-size: 15px;margin-top: 5px;width: 80px;background-color: green;" type="button" id="btnSimularOF" >Simular</button>
													</div>																						
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>

						</div>
					</div>
				</div>
        </div><!-- /.modal-content -->
    	</div><!-- /.modal-dialog -->
	</div> 
</div>

<div class="modal" tabindex="-1" role="dialog" id="ModelEvaluarCompromiso"> 
    <div class="modal-dialog" role="document">
    	<div class="modal-content"   style="overflow-y: scroll;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="Cerrar_Evaluar_Compromiso"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Simulación de Compromiso</h4>
            </div>
            <div class="dummy4">
            	<div>
					<form>
						<label style="font-size: 11px;padding-left: 25px;" id="nombreEmpresalbl2">
							Empresa:  <span id="Empresallevar2"></span>
						</label>
						<br>
						<label style="font-size: 11px;padding-left: 25px;" id="TipoGuardar2">
							Tipo :  <span id="Tipollevar2"></span>
						</label> 
						<br>
						<label style="font-size: 11px;padding-left: 25px;" id="SubtipoGuardar2">
							Subtipo: <span id="Subtipollevar2"></span>
						</label>
						<br>
						<div class="container">
							<div class="row" align="center">
							    <div class="col-md-2 col-xs-4" style="padding-left: 35px"> Desde:  </div>
							    <div class="col-md-2 col-xs-4" style="padding-left:160px"> Hasta:  </div>
							</div>					
					 	</div>
						<div class="container">
							<div class="row" align="center">
							    <div class="col-md-1 col-xs-6" style="padding-left: 50px" id="DesdeGuardar2"> <label><span id="desdellevar2"></span></label> </div>
							    <div class="col-md-1 col-xs-6" style="padding-left:170px" id="HastaGuardar2"> <label><span id="hastallevar2"></span></label> </div>
							</div>					
					 	</div>
					 	<div style="padding-left: 100px;" id="divTabla">				
						 	<center>
						  		<table id="tabla" class="table table-striped table-bordered jambo_table" style="align-content:center;">
						  			<thead>
						  				 <tr>
								            <th style="vertical-align: middle; text-align: center;">Periodo</th>
								            <th style="vertical-align: middle; text-align: center;">Imp real</th>
	      								</tr>
						  			</thead>
						  			<tbody>
						  				<td style="vertical-align: middle; text-align: center;"></td>
						  				<td style="vertical-align: middle; text-align: center;"></td>
						  			</tbody>
						  		</table>
					  		</center>
				  		</div>
					</form>
					<div class="container">
						<div class="row" align="center">
							<div class="col-md-1 col-xs-4" style="padding-left: 80px" align="center" >
								<button class="btn btn-primary" style="font-size: 15px;margin-top: 5px;width: 150px;" type="button" id="btnSalir3" >Retroceder 
								</button>						
							</div>									
						</div>
					</div>					
				</div>
            </div>
         </div>   
    </div>
</div>    
@stop

@section('js-scripts')
<!-- jquery -->
<script>
function ActualizarFiltroBoton(){
	
	 banca = window.filtro1;
        zonal = window.filtro2;
        jefe = window.filtro3;
        ejecutivo = window.filtro4;
        $.ajax({
            type: 'POST',
            data: {
                "banca": banca,
                "zonal": zonal,
                "jefe": jefe,
                "ejecutivo": ejecutivo,
            },
            async: true,
            url: APP_URL + 'getUsuarioReciprocidad',
            success: function(result) {
           	var res = JSON.stringify(result);
            	var jsonObj = JSON.parse(res);
            	registros=[];
				for(var i = 0; i < jsonObj.length; i++)
				{
					registros.push(jsonObj[i]['REGISTRO']);
				    //(jsonObj[i]['REGISTRO']);
				}
				console.log(registros);
				window.location.replace("{{route('Reciprocidad2')}}?registros="+registros);
            }
        });
}
function autocomplete(inp, arr) {
  var currentFocus;
  inp.addEventListener("input", function(e) {
      var a, b, i, val = this.value;
      closeAllLists();
      if (!val) { return false;}
      currentFocus = -1;
      a = document.createElement("DIV");
      a.setAttribute("id", this.id + "autocomplete-list");
      a.setAttribute("class", "autocomplete-items");
      this.parentNode.appendChild(a);
      /*for each item in the array...*/
      for (i = 0; i < arr.length; i++) {
        if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
          b = document.createElement("DIV");
          b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
          b.innerHTML += arr[i].substr(val.length);
          b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
          b.addEventListener("click", function(e) {
              inp.value = this.getElementsByTagName("input")[0].value;
              closeAllLists();
          });
          a.appendChild(b);
        }
      }
  });
  inp.addEventListener("keydown", function(e) {
      var x = document.getElementById(this.id + "autocomplete-list");
      if (x) x = x.getElementsByTagName("div");
      if (e.keyCode == 40) {
        currentFocus++;
        addActive(x);
      } else if (e.keyCode == 38) { 
        currentFocus--;
        addActive(x);
      } else if (e.keyCode == 13) {
        e.preventDefault();
        if (currentFocus > -1) {
          if (x) x[currentFocus].click();
        }
      }
  });
  function addActive(x) {
    if (!x) return false;
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = (x.length - 1);
    x[currentFocus].classList.add("autocomplete-active");
  }
  function removeActive(x) {
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove("autocomplete-active");
    }
  }
  function closeAllLists(elmnt) {
    var x = document.getElementsByClassName("autocomplete-items");
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }
  document.addEventListener("click", function (e) {
      closeAllLists(e.target);
  });
}
var empresas = [];

@foreach($lead as $tipocompromiso)
	empresas.push("{{$tipocompromiso->NOMBRE}}");
@endforeach
autocomplete(document.getElementById("myInput"), empresas);

$("#buscarnumdoc").click(function(e){
			var codUnicoInput = document.getElementById('BuscCódigoÚnico');
			var codUnico = codUnicoInput.value;
			console.log("Aqui")
			$.ajax({
		        type: 'POST',
		        data: {
		            "codUnico": codUnico
		        },
		        async: false,
		        url: "{{route('reciprocidad')}}",
		        success: function(result) {
		           console.log(result[0].NOMBRE);
		           $("#EmpresaRazónSAC").val(result[0].NOMBRE);
		        }
		    });
		});
		$(function() {
		  $('tr.parent td span.btn')
		    .on("click", function(){
		    var idOfParent = $(this).parents('tr').attr('id');
		    $('tr.child-'+idOfParent).toggle('slow');
		  });
		  $('tr[class^=child-]').hide().children('td');
		});

		$("#buscarnumdocSimular").click(function(e){
			var codUnicoInput = document.getElementById('BuscCódigoÚnicoSimular');
			var codUnico = codUnicoInput.value;
			console.log("Aqui")
			$.ajax({
		        type: 'POST',
		        data: {
		            "codUnico": codUnico
		        },
		        async: false,
		        url: "{{route('reciprocidad')}}",
		        success: function(result) {
		           console.log(result[0].NOMBRE);
		           $("#EmpresaRazónSACSimular").val(result[0].NOMBRE);
		        }
		    });
		});

		$("#Opciones_Pestaña1").change(function(e){
			var texto = $( this ).val();
			console.log(texto)
			if (texto=="Mensual"){
				$("#Tabla_Acumulada").css("display","none");
				$("#Tabla_Mensual").css("display","block");
			} else {
				$("#Tabla_Acumulada").css("display","block");
				$("#Tabla_Mensual").css("display","none");
			}
			
		});

	$(document).ready(function () {
		$("#btnRenovación").click(function(){
		    $("#btnOperación").css("background","grey");
		    $("#btnRevisión").css("background","grey");
		    $("#btnRenovación").css("background","green");
		  }); 

		$("#btnOperación").click(function(){
		    $("#btnOperación").css("background","green");
		    $("#btnRevisión").css("background","grey");
		    $("#btnRenovación").css("background","grey");
		  });

		$("#btnGuardar").click(function(){
			botones=["  Renovación de Linea","  Operación puntual","  Revisión de comisiones pref"];
			coloresbotones=[$("#btnRenovación").css("background-color"),$("#btnOperación").css("background-color"),$("#btnRevisión").css("background-color")];
			if (jQuery.inArray("rgb(0, 128, 0)",coloresbotones)>=0) {
				Concepto=botones[jQuery.inArray("rgb(0, 128, 0)",coloresbotones)];
			}

			botones_tipo=['CASH IN','CASH OUT']
			coloresbotones_tipo=[$("#btncASHIN").css("background-color"),$("#btnCASHOUT").css("background-color")];
			if (jQuery.inArray("rgb(0, 128, 0)",coloresbotones_tipo)>=0) {
				Tipo=botones_tipo[jQuery.inArray("rgb(0, 128, 0)",coloresbotones_tipo)];
			}
			$("#nombreEmpresalbl span").html($("#EmpresaRazónSAC").val());
			$("#Conceptoguardar span").html(Concepto);
			$("#TipoGuardar span").html(Tipo);
			$("#ImporteGuardar span").html($("#IngresarImporte").val());
			$("#SubtipoGuardar span").html($("#ComboSubtipo").val());	
			$("#DesdeGuardar span").html($("#ComboDesde").val());	
			$("#HastaGuardar span").html($("#ComboGuardar").val());	
		    $("#ModelGenerarCompromiso").hide();	
		    $("#ModelConfirmarCompromiso").show();
		  });

		$("#btnRevisión").click(function(){
		    $("#btnOperación").css("background","grey");
		    $("#btnRevisión").css("background","green");
		    $("#btnRenovación").css("background","grey");
		  });

		$("#btncASHIN").click(function(){
		    $("#btncASHIN").css("background","green");
		    $("#btnCASHOUT").css("background","grey");

		    //var myOptions = [{ text: 'Suganthar', value: 1}, {text : 'Suganthar2', value: 2}];
		    var myOptions = [];

		    @foreach($tiposcompromiso as $tipocompromiso)
				myOptions.push({text:"{{$tipocompromiso->CODIGO}}-{{$tipocompromiso->DESCRIPCION}}", value:"{{$tipocompromiso->CODIGO}}-{{$tipocompromiso->DESCRIPCION}}"});
			@endforeach
		    

			$('#ComboSubtipo').empty();
			$.each(myOptions, function(i, el) 
			{    $('#ComboSubtipo').append( new Option(el.text,el.value) );});

		  });

		$("#btncASHINSimular").click(function(){
		    $("#btncASHINSimular").css("background","green");
		    $("#btnCASHOUTSimular").css("background","grey");

		    //var myOptions = [{ text: 'Suganthar', value: 1}, {text : 'Suganthar2', value: 2}];
		    var myOptions = [];

		    @foreach($tiposcompromiso as $tipocompromiso)
				myOptions.push({text:"{{$tipocompromiso->CODIGO}}-{{$tipocompromiso->DESCRIPCION}}", value:"{{$tipocompromiso->CODIGO}}-{{$tipocompromiso->DESCRIPCION}}"});
			@endforeach
		    

			$('#ComboSubtipoSimular').empty();
			$.each(myOptions, function(i, el) 
			{    $('#ComboSubtipoSimular').append( new Option(el.text,el.value) );});

		  });

		$("#btnCASHOUT").click(function(){
		    $("#btncASHIN").css("background","grey");
		    $("#btnCASHOUT").css("background","green");

			var myOptions = [];
		    @foreach($tiposcompromiso2 as $tipocompromiso)
				myOptions.push({text:"{{$tipocompromiso->CODIGO}}-{{$tipocompromiso->DESCRIPCION}}", value:"{{$tipocompromiso->CODIGO}}-{{$tipocompromiso->DESCRIPCION}}"});
			@endforeach
			$('#ComboSubtipo').empty();
			$.each(myOptions, function(i, el) 
			{    $('#ComboSubtipo').append( new Option(el.text,el.value) );});
		  });
		$("#btnCASHOUTSimular").click(function(){
		    $("#btncASHINSimular").css("background","grey");
		    $("#btnCASHOUTSimular").css("background","green");

			var myOptions = [];
		    @foreach($tiposcompromiso2 as $tipocompromiso)
				myOptions.push({text:"{{$tipocompromiso->CODIGO}}-{{$tipocompromiso->DESCRIPCION}}", value:"{{$tipocompromiso->CODIGO}}-{{$tipocompromiso->DESCRIPCION}}"});
			@endforeach
			$('#ComboSubtipoSimular').empty();
			$.each(myOptions, function(i, el) 
			{    $('#ComboSubtipoSimular').append( new Option(el.text,el.value) );});
		  });
		$("#btnNuevoCompromiso").click(function(){
			$("#BuscCódigoÚnico").val('')
			$("#EmpresaRazónSAC").val(' ')
			$("#IngresarImporte").val(' ')
			$("#btnCASHOUT").addClass('btn btn-primary')
			$("#btnCASHOUT").css('background','#337ab7');
			$("#btncASHIN").addClass('btn btn-primary')
			$("#btncASHIN").css('background','#337ab7');
			$("#btnRevisión").addClass('btn btn-primary')
			$("#btnRevisión").css('background','#337ab7');
			$("#btnRenovación").addClass('btn btn-primary')
			$("#btnRenovación").css('background','#337ab7');
			$("#btnOperación").addClass('btn btn-primary')
			$("#btnOperación").css('background','#337ab7');
			$('#ComboDesde').val(1);
			$('#ComboGuardar').val(1);	
			$("#ComboSubtipo").val(1);	
		    $("#ModelGenerarCompromiso").show();	
		  });

			$("#btnSimularCompromiso").click(function(){	
		    $("#ModelSimularCompromiso").show();	

		  });


		$("#btnSimularOF").click(function(){
			$("#ModelEvaluarCompromiso").show();
			
			botones_tipo=['CASH IN','CASH OUT']
			coloresbotones_tipo=[$("#btncASHINSimular").css("background-color"),$("#btnCASHOUTSimular").css("background-color")];
			if (jQuery.inArray("rgb(0, 128, 0)",coloresbotones_tipo)>=0) {
				Tipo=botones_tipo[jQuery.inArray("rgb(0, 128, 0)",coloresbotones_tipo)];
			}
			$("#nombreEmpresalbl2 span").html($("#EmpresaRazónSACSimular").val());
			$("#TipoGuardar2 span").html(Tipo);
			$("#ImporteGuardar2 span").html($("#IngresarImporteSimular").val());
			$("#SubtipoGuardar2 span").html($("#ComboSubtipoSimular").val());	
			$("#DesdeGuardar2 span").html($("#ComboDesdeSimular").val());	
			$("#HastaGuardar2 span").html($("#ComboGuardarSimular").val());	

			if ($("#Tipollevar2").html()=="CASH IN") {
			 	Llevar_final="I"
			 }
			 else{Llevar_final="O"}
			 console.log($("#Tipollevar2").html());
			 console.log(Llevar_final);		 	
			 Subtipo_final=$("#Subtipollevar2").html().substring(0,3)
		   	
		   	$.ajax({
		        type: 'GET',
		        data: {
		            "Codunicollevar" : $("#BuscCódigoÚnicoSimular").val(),
					"tipollevar": Llevar_final,
					"subtipollevar": Subtipo_final,
					"desdellevar": $("#desdellevar2").html(),
					"hastallevar": $("#hastallevar2").html()
		        },
		        async: false,
		        url: "{{route('Reciprocidad2.Resultados_simular')}}",
		        success: function(result) {
		        	//console.log(result);

		        	var table="<table id='tabla' class='table table-striped table-bordered jambo_table' style='align-content:center;'><tr><th style='vertical-align: middle; text-align: center;'>Periodo</th><th style='vertical-align: middle; text-align: center;'>Imp real</th></tr>";

					for (i = 0, len = result.length, text = ""; i < len; i++) {
						console.log(result[i]);
						table+="<tr><td style='vertical-align: middle; text-align: center;'>"+result[i]['PERIODO']+"</td>";
						table+="<td style='vertical-align: middle; text-align: center;'>"+result[i]['IMPORTE_REAL']+"</td></tr>";
					}
					table+="</table>";


		        	$('#tabla').remove();
		        	
            		$('#divTabla').append(table)
		        }
			});
		  });

		$("#btnConfirmar").click(function(){
			 if ($("#Tipollevar").html()=="CASH IN") {
			 	Llevar_final="I"
			 }
			 else{Llevar_final="O"}
			 console.log($("#Tipollevar").html());
			 console.log(Llevar_final);		 	
			 Subtipo_final=$("#Subtipollevar").html().substring(0,3)
		   	$.ajax({
		        type: 'GET',
		        data: {
		            "Codunicollevar" : $("#BuscCódigoÚnico").val(),
		            "empresallevar" : $("#Empresallevar").html(),
					"conceptollevar": $("#Conceptollevar").html().toUpperCase(),
					"tipollevar": Llevar_final,
					"subtipollevar": Subtipo_final,
					"importellevar": $("#Importellevar").html(),
					"desdellevar": $("#desdellevar").html(),
					"hastallevar": $("#hastallevar").html()
		        },
		        async: false,
		        url: "{{route('Reciprocidad2.guardarDatos')}}",
		        success: function(result) {
		        }
			});
			$("#ModelConfirmarCompromiso").hide();
			$("#BuscCódigoÚnico").val('')
			$("#EmpresaRazónSAC").val(' ')
			$("#IngresarImporte").val('')
			$("#btnCASHOUT").addClass('btn btn-primary')
			$("#btnCASHOUT").css('background','#337ab7');
			$("#btncASHIN").addClass('btn btn-primary')
			$("#btncASHIN").css('background','#337ab7');
			$("#btnRevisión").addClass('btn btn-primary')
			$("#btnRevisión").css('background','#337ab7');
			$("#btnRenovación").addClass('btn btn-primary')
			$("#btnRenovación").css('background','#337ab7');
			$("#btnOperación").addClass('btn btn-primary')
			$("#btnOperación").css('background','#337ab7');
			$('#ComboDesde').val(1);
			$('#ComboGuardar').val(1);	
			$("#ComboSubtipo").val(1);			
		});
		$("#btnSalir2").click(function(){
		    $("#ModelConfirmarCompromiso").hide();
		});
		$("#btnSalir3").click(function(){
		    $("#ModelEvaluarCompromiso").hide();
		});
		$("#btnSalir").click(function(){
		    $("#ModelGenerarCompromiso").hide()
		    $("#BuscCódigoÚnico").val('')
			$("#EmpresaRazónSAC").val(' ')
			$("#IngresarImporte").val('')
			$("#btnCASHOUT").addClass('btn btn-primary')
			$("#btnCASHOUT").css('background','#337ab7');
			$("#btncASHIN").addClass('btn btn-primary')
			$("#btncASHIN").css('background','#337ab7');
			$("#btnRevisión").addClass('btn btn-primary')
			$("#btnRevisión").css('background','#337ab7');
			$("#btnRenovación").addClass('btn btn-primary')
			$("#btnRenovación").css('background','#337ab7');
			$("#btnOperación").addClass('btn btn-primary')
			$("#btnOperación").css('background','#337ab7');
			$('#ComboDesde').val(1);
			$('#ComboGuardar').val(1);										
		});
		$("#btnSalirSimular").click(function(){
		    $("#ModelSimularCompromiso").hide()
		    $("#BuscCódigoÚnicoSimular").val('')
			$("#EmpresaRazónSACSimular").val(' ')
			$("#IngresarImporteSimular").val('')
			$("#btnCASHOUTSimular").addClass('btn btn-primary')
			$("#btnCASHOUTSimular").css('background','#337ab7');
			$("#btncASHINSimular").addClass('btn btn-primary')
			$("#btncASHINSimular").css('background','#337ab7');		
			$('#ComboDesdeSimular').val(1);
			$('#ComboGuardarSimular').val(1);
			$("#ComboSubtipoSimular").val(0);			
		});
		$("#Cerrar_Confirmar_Compromiso").click(function(){
		    $("#ModelConfirmarCompromiso").hide();
		  });
		$("#Cerrar_Generar_Compromiso").click(function(){
		    $("#ModelGenerarCompromiso").hide();
		  });
		$("#Cerrar_Evaluar_Compromiso").click(function(){
		    $("#ModelEvaluarCompromiso").hide();
		  });		
	});

	var filtro1;
    var filtro2;
    var filtro3;
    var filtro4;
    var filtroProducto;
    $(document).ready(function() {
        window.filtro1 = "<?php echo isset($banca) ? $banca : 'BC' ?>";
        window.filtro2 = "<?php echo isset($zonal) ? $zonal : 'null' ?>";
        window.filtro3 = "<?php echo isset($jefe) ? $jefe : 'null' ?>";
        window.filtro4 = "<?php echo isset($ejecutivo) ? $ejecutivo : 'null' ?>";
        
        ActualizarFiltro();

/****** BANCA - ZONAL - JEFATURA - EJECUTIVO ******/
        if ($('#filtro1').length > 0){
             getZonales([window.filtro1]);
        } else {

            if (typeof $('#filtro2').val() !== 'undefined' && $('#filtro2').val()!=null){
            	window.filtro2 = $("#filtro2").val();
                getJefes([window.filtro1, window.filtro2]);
                // if ($('#filtro3').val()!=null){
                // 	window.filtro3 = $("#filtro3").val();
                //     getEjecutivos([window.filtro1, window.filtro2, window.filtro3]);  
                // }
            } else {
                if (typeof $('#filtro3').val() !== 'undefined'){
                	window.filtro2 = $("#filtro2").val();
                	window.filtro3 = $("#filtro3").val();
                    getEjecutivos([window.filtro1, window.filtro2, window.filtro3]);  
                }
            }
        }

    });
	 $(document).on("change", "#filtro1", function() {
        window.filtro1 = $("#filtro1").val();
        getZonales([window.filtro1]);
    });
    $(document).on("change", "#filtro2", function() {
        window.filtro2 = $("#filtro2").val();
        getJefes([window.filtro1, window.filtro2]);
    });
    $(document).on("change", "#filtro3", function() {
        window.filtro3 = $("#filtro3").val();
        getEjecutivos([window.filtro1, window.filtro2, window.filtro3]);
    });
    $(document).on("change", "#filtro4", function() {
        window.filtro4 = $("#filtro4").val();
    }); 
    function getZonales(datos) {
        $.ajax({
            type: 'GET',
            data: {
                "banca": datos[0]
            },
            async: false,
            url: "{{route('getZonales')}}",
            success: function(result) {
                var arr = JSON.parse(result);
                $("#filtro2").empty();
                window.filtro2 = "null";
                $("#filtro3").empty();
                window.filtro3 = "null";
                $("#filtro4").empty();
                window.filtro4 = "null";
                const x = document.getElementById("filtro2");
                const y = document.getElementById("filtro3");
                const z = document.getElementById("filtro4");
                var option = document.createElement("option");
                option.appendChild(document.createTextNode("Todos"));
                option.value = null;
                y.appendChild(option);
                z.appendChild(option);
                if (arr.length == 1) {
                    datos.push(null);
                    x.appendChild(option);
                    getJefes(datos);
                } else {
                    arr.forEach(element => {
                        var option = document.createElement("option");
                        if (!element['NOMBRE_ZONAL']) {
                            option.appendChild(document.createTextNode("Todos"));
                        } else {
                            option.appendChild(document.createTextNode(element['NOMBRE_ZONAL']));
                        }
                        option.value = element['NOMBRE_ZONAL'];
                        x.appendChild(option);
                    });
                }
            }
        });
    }

    function getJefes(datos) {
        $.ajax({
            type: 'GET',
            data: {
                "banca": datos[0],
                "zonal": datos[1]
            },
            async: false,
            url: "{{route('getJefes')}}",
            success: function(result) {
                var arr = JSON.parse(result);
                $("#filtro3").empty();
                window.filtro3 = "null";
                $("#filtro4").empty();
                window.filtro4 = "null";
                const x = document.getElementById("filtro3");
                const y = document.getElementById("filtro4");
                var option = document.createElement("option");
                option.appendChild(document.createTextNode("Todos"));
                option.value = null;
                y.appendChild(option);
                if (arr.length == 1) {
                    datos.push(null);
                    x.appendChild(option);
                    getEjecutivos(datos);
                } else {
                    arr.forEach(element => {
                        var option = document.createElement("option");
                        if (!element['NOMBRE_JEFE']) {
                            option.appendChild(document.createTextNode("Todos"));
                        } else {
                            option.appendChild(document.createTextNode(element['NOMBRE_JEFE']));
                        }
                        option.value = element['NOMBRE_JEFE'];
                        x.appendChild(option);
                    });
                    $("#filtro3").val("<?php echo $valores_filtro['jefe'] ?>" );

                }
            }
        });
    }

    function getEjecutivos(datos) {
        $.ajax({
            type: 'GET',
            data: {
                "banca": datos[0],
                "zonal": datos[1],
                "jefe": datos[2]
            },
            async: false,
            url: "{{route('getEjecutivos')}}",
            success: function(result) {
                var arr = JSON.parse(result);
                $("#filtro4").empty();
                window.filtro4 = "null";
                const x = document.getElementById("filtro4");
                if (arr.length == 1) {
                    ActualizarFiltro();
                } else {
                    arr.forEach(element => {
                        var option = document.createElement("option");
                        if (!element['ENCARGADO']) {
                            option.appendChild(document.createTextNode("Todos"));
                        } else {
                            option.appendChild(document.createTextNode(element['ENCARGADO']));
                        }
                        option.value = element['ENCARGADO']
                        x.appendChild(option);
                    });
                    $("#filtro4").val("<?php echo $valores_filtro['ejecutivo'] ?>" );
                }
            }
        });
    }

    function ActualizarFiltro() {
        banca = window.filtro1;
        zonal = window.filtro2;
        jefe = window.filtro3;
        ejecutivo = window.filtro4;
        $.ajax({
            type: 'POST',
            data: {
                "banca": banca,
                "zonal": zonal,
                "jefe": jefe,
                "ejecutivo": ejecutivo,
            },
            async: true,
            url: APP_URL + 'getUsuarioReciprocidad',
            success: function(result) {
            	// console.log(result)
            }
        });
    }


/*Funciones de autocompletado*/
// function autocomplete(inp, arr) {
//   var currentFocus;
//   inp.addEventListener("input", function(e) {
//   	var a, b, i, val = this.value;
//   	closeAllLists();
//   	if (!val) { return false;}
//   	currentFocus = -1;
//   	a = document.createElement("DIV");
//   	a.setAttribute("id", this.id + "autocomplete-list");      
//   	a.setAttribute("class", "autocomplete-items");
//   	this.parentNode.appendChild(a);
//   	var nCar=3;
//   	if(val.length>=nCar){
//   		for (i = 0; i < arr.length; i++) {
//   			if (
//         	arr[i].toUpperCase().indexOf(val.toUpperCase()) > -1
//         	) {
//         		var posicion=arr[i].toUpperCase().indexOf(val.toUpperCase());
//   			b = document.createElement("DIV");
//           b.innerHTML = arr[i].substr(0, posicion).toUpperCase();
//           b.innerHTML += '<strong>' + arr[i].substr(posicion, val.length).toUpperCase() + "</strong>";
//           b.innerHTML += arr[i].substr(posicion+val.length).toUpperCase();
//           b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
//           b.addEventListener("click", function(e) {
//           	inp.value = this.getElementsByTagName("input")[0].value;
//           	console.log("CLICK RAZON");
//           		buscarClick();
//               closeAllLists();
//           });
//           a.appendChild(b);
//       }
//   }
// }
// });
//   inp.addEventListener("keydown", function(e) {
//   	var x = document.getElementById(this.id + "autocomplete-list");
//   	if (x) x = x.getElementsByTagName("div");
//   	if (e.keyCode == 40) {
//         currentFocus++;
//         addActive(x);
//       } else if (e.keyCode == 38) { 
//         currentFocus--;
//         addActive(x);
//     } else if (e.keyCode == 13) {
//     	e.preventDefault();
//     	if (currentFocus > -1) {
//     		if (x) x[currentFocus].click();
//     	}
//     }
// });
//   function addActive(x) {
//   	/*a function to classify an item as "active":*/
//   	if (!x) return false;
//   	/*start by removing the "active" class on all items:*/
//   	removeActive(x);
//   	if (currentFocus >= x.length) currentFocus = 0;
//   	if (currentFocus < 0) currentFocus = (x.length - 1);
//   	/*add class "autocomplete-active":*/
//   	x[currentFocus].classList.add("autocomplete-active");
//   }
//   function removeActive(x) {
//   	/*a function to remove the "active" class from all autocomplete items:*/
//   	for (var i = 0; i < x.length; i++) {
//   		x[i].classList.remove("autocomplete-active");
//   	}
//   }
//   function closeAllLists(elmnt) {
//     /*close all autocomplete lists in the document,
//     except the one passed as an argument:*/
//     var x = document.getElementsByClassName("autocomplete-items");
//     for (var i = 0; i < x.length; i++) {
//     	if (elmnt != x[i] && elmnt != inp) {
//     		x[i].parentNode.removeChild(x[i]);
//     	}
//     }
// }
// document.addEventListener("click", function (e) {
// 	closeAllLists(e.target);
// });
// }
</script>

@stop