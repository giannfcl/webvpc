@extends('Layouts.layout')

@section('js-libs')
<link href="{{ URL::asset('css/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ URL::asset('css/datatables.min.css') }}" rel="stylesheet" type="text/css">

<script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.es.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/chart.bundle.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/utils.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/numeral.min.js') }}"></script>

<!--FORM VALIDATION-->
<link href="{{ URL::asset('css/formValidation.min.css') }}" rel="stylesheet" type="text/css" > 

<script type="text/javascript" src="{{ URL::asset('js/formvalidation/formValidation.popular.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/formvalidation/language/es_CL.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/formvalidation/framework/bootstrap.min.js') }}"></script>
@stop

@section('content')
@section('pageTitle', 'Mi Cumplimiento')
<div class="row">
    <div class="col-sm-12 col-xs-12">
        <div class="x_panel" style="min-height: 280px">
            <div class="x_title">
                <h2>FILTROS</h2>
                <ul class="nav navbar-right panel_toolbox">
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form class="form-horizontal form-label-left input_mask">
                    <div class="form-group">
                        <label class="control-label col-sm-1 col-xs-12">PERIODO</label>
                        <div class="col-sm-2 col-xs-12" style="width: 8%;">
                            <select class="form-control" id="periodos">
                            @if (isset($periodos) && !empty($periodos))
                                @foreach ($periodos as $periodos)
                                    <option value="{{$periodos->periodo}}"> {{$periodos->periodo}}</option>
                                @endforeach
                            @endif
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-1 col-xs-12">BANCA</label>
                        <div class="col-sm-2 col-xs-12">
                            <select class="form-control" id="bancas">
                            @if (isset($bancas) && !empty($bancas))
                                    <option value="">Seleccionar Banca</option>
                                @foreach ($bancas as $bancas)
                                    <option value="{{$bancas->banca}}"> {{$bancas->banca}}</option>
                                @endforeach
                            @endif
                            </select>
                        </div>
                        <label class="control-label col-sm-2 col-xs-12">ZONAL</label>
                        <div class="col-sm-2 col-xs-12">
                            <select class="form-control" id="zonales">
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-10 col-xs-12">
                        </div>
                        <div class="col-sm-2 col-xs-12">
                            <button class="btn btn-primary" type="button" id="buscar_">Buscar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_content">
				<table class="table table-striped jambo_table" style="width: 50%;margin-left: 20%;" id="tablero" hidden="hidden">
		            <thead>
			            <tr>
			            	<th colspan="2" style="width: 15%;" class="text-center">RESUMEN</th>
			            </tr>
			            <tr>
			            	<th style="text-align: center;">NOMBRE</th>
			            	<th style="text-align: center;">NOTA</th>
			            </tr>
			         </thead>
		            <tbody id="datos">
		            </tbody>
		            <tfooter></tfooter>
		        </table>
		    </div>
    	</div>
    </div>
</div>

@stop

@section('js-scripts')
<script>
    $('#bancas').change(function(){
        $('#tablero').attr('hidden','hidden');
        $.ajax({
            type: "POST",
            data: {
                bancas: $(this).val(),
            },
            url: APP_URL + 'micumplimiento/be/zonales',
            dataType: 'json',
            success: function (result) {
                if (result) {
                    $('#tablero').removeAttr('hidden');
                    $('#zonales').html(result);
                    if ($('#zonales').val()) {
                        $('#buscar_').click();
                    }
                }
            },
        });
    });

    $('#buscar_').click(function(){
        $.ajax({
            type: "POST",
            data: {
                bancas: $("#bancas").val(),
                zonales: $("#zonales").val(),
                periodos: $("#periodos").val(),
            },
            url: APP_URL + 'micumplimiento/be/lista',
            dataType: 'json',
            success: function (result) {
                bien="<a><i class='fas fa-check-circle' style='color:green'></i><a>";
                maso="<a><i class='fas fa-exclamation-circle' style='color:#d7e01c'></i><a>";
                mal="<a><i class='fas fa-times-circle' style='color:red'></i><a>";
                a='';
                $.each( result, function( key, value ) {
                    value['NOTATOTAL']=funre(value['NOTATOTAL']);
                    if (value['NOTATOTAL']>=100) {
                        a+='<tr><td style="text-align: center;"> <a href="{{route("mi.detalleindicadores")}}?llave="'+value['PERIODO']+value['GRUPO']+value['BANCA']+'" ">'+value['EJECUTIVO']+'</a> </td><td style="text-align: center;">'+value['NOTATOTAL'] +' % '+bien+'</td></tr>';
                    }else if (80<value['NOTATOTAL']<100) {
                        a+='<tr><td style="text-align: center;"> <a href="{{route("mi.detalleindicadores")}}?llave="'+value['PERIODO']+value['GRUPO']+value['BANCA']+'" ">'+value['EJECUTIVO']+'</a> </td><td style="text-align: center;">'+value['NOTATOTAL'] +' % '+maso+'</td></tr>';
                    }else{
                        a+='<tr><td style="text-align: center;"> <a href="{{route("mi.detalleindicadores")}}?llave="'+value['PERIODO']+value['GRUPO']+value['BANCA']+'" ">'+value['EJECUTIVO']+'</a> </td><td style="text-align: center;">'+value['NOTATOTAL'] +' % '+mal+'</td></tr>';
                    }
                    //target='_blank' href=' {{route('mi.detalleindicadores')}} ? cod_sect_="+value['COD_SECT']+"'
                });
                $("#datos").html(a);
            },
        });
    });

    function funre(numero)
    {
        var flotante = parseFloat(numero);
        var resultado = Math.round(flotante*100)/100;
        return resultado;
    }

    // $(document).on('click','#detalle',function () {
    //     $.ajax({
    //         type: "GET",
    //         data: {
    //             cod_sect: $(this).attr('cod_sect_'),
    //             periodo: $(this).attr('periodos_'),
    //             banca: $(this).attr('bancas_'),
    //         },
    //         url: APP_URL + 'micumplimiento/be/detalleindicadores',
    //         dataType: 'json',
    //         success: function (result) {
    //         },
    //     });
    // });
</script>
@stop