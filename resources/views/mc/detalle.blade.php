@extends('Layouts.layout')

@section('js-libs')
<link href="{{ URL::asset('css/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ URL::asset('css/datatables.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ URL::asset('css/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" >
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

<script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.es.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/chart.bundle.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/utils.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/numeral.min.js') }}"></script>

<!--FORM VALIDATION-->
<link href="{{ URL::asset('css/formValidation.min.css') }}" rel="stylesheet" type="text/css" > 

<script type="text/javascript" src="{{ URL::asset('js/formvalidation/formValidation.popular.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/formvalidation/language/es_CL.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/formvalidation/framework/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.es.min.js') }}"></script>
@stop

@section('content')
@section('pageTitle', 'Detalle Cumplimiento')

<div class="row">
    <div class="col-sm-12 col-xs-12">
        <div class="x_panel" style="min-height: 280px">
            <div class="x_title">
                <h2>FILTROS</h2>
                <ul class="nav navbar-right panel_toolbox">
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form class="form-horizontal form-label-left input_mask">
                    <div class="form-group">
                        <label class="control-label col-sm-1 col-xs-12">PERIODO</label>
                        <div class="col-sm-2 col-xs-12" style="width: 8%;">
                            <select class="form-control" id="periodos">
                            @if (isset($periodos) && !empty($periodos))
                                @foreach ($periodos as $periodos)
                                    <option value="{{$periodos->periodo}}"> {{$periodos->periodo}}</option>
                                @endforeach
                            @endif
                            </select>
                        </div>
                        <label class="control-label col-sm-9 col-xs-12">DESCARGAR ESTIMADOR</label>
                        <div class="col-sm-6 col-xs-12" style="width: 8%;">
                            <a href="{{route('download', ['file' => str_replace('/','|','mc/Mi_Cumplimiento_2019_final.xlsm')])}}" ><i class="fa fa-download fa-2x" ></i></a>
                        </div>
                    </div><br>
                    <div class="form-group">
                        <label class="control-label col-sm-1 col-xs-12">BANCA</label>
                        <div class="col-sm-2 col-xs-12">
                            <select class="form-control" id="bancas">
                            @if (isset($bancas) && !empty($bancas))
                                    <option value="">Seleccionar Banca</option>
                                @foreach ($bancas as $bancas)
                                    <option value="{{$bancas->banca}}"> {{$bancas->banca}}</option>
                                @endforeach
                            @endif
                            </select>
                        </div>
                        <label class="control-label col-sm-2 col-xs-12">ZONAL</label>
                        <div class="col-sm-2 col-xs-12">
                            <select class="form-control" id="zonales">
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2 col-xs-12">JEFATURA</label>
                        <div class="col-sm-2 col-xs-12">
                            <select class="form-control" id="jefatura" disabled="disabled">
                            </select>
                        </div>

                        <label class="control-label col-sm-2 col-xs-12">EJECUTIVOS</label>
                        <div class="col-sm-2 col-xs-12">
                            <select class="form-control" id="ejecutivos">
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-10 col-xs-12">
                        </div>
                        <div class="col-sm-2 col-xs-12">
                            <button class="btn btn-primary" type="button" id="buscar_">Buscar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_content">
                <table class="table tablero jambo_table" hidden="hidden" style="width: 75%;margin-left: 13%;">
                    <thead>
                        <tr>
                            <th style="text-align: center;">Indicador</th>
                            <th style="text-align: left;width: 13%;">Unidad de Medicion</th>
                            <th style="text-align: left;">Real</th>
                            <th style="text-align: center;">Ppto</th>
                            <th style="text-align: right;">Var</th>
                            <th style="text-align: center;">%Cumpl.</th>
                            <th style="text-align: center;">Peso</th>
                            <th style="text-align: center;">Nota</th>
                        </tr>
                     </thead>
                </table>
                <table class="table table-striped jambo_table tablero" hidden="hidden" style="width: 75%;margin-left: 13%;">
                    <thead>
                        <tr>
                            <th colspan="9" style="width: 15%;" class="text-center">FINANCIERA</th>
                        </tr>
                     </thead>
                    <tbody id="datos_f">
                    </tbody>
                    <tfooter></tfooter>
                </table>
                <table class="table table-striped jambo_table tablero" hidden="hidden" style="width: 75%;margin-left: 13%;">
                    <thead>
                        <tr>
                            <th colspan="9" style="width: 15%;" class="text-center">ECOSISTEMA</th>
                        </tr>
                     </thead>
                    <tbody id="datos_e">
                    </tbody>
                    <tfooter></tfooter>
                </table>
                <table class="table table-striped jambo_table tablero" hidden="hidden" style="width: 75%;margin-left: 13%;">
                    <thead>
                        <tr>
                            <th colspan="9" style="width: 15%;" class="text-center">ACELERADOR</th>
                        </tr>
                     </thead>
                    <tbody id="datos_a">
                    </tbody>
                    <tfooter></tfooter>
                </table>
                <table class="table table-striped tablero" style="width: 22%;margin-left: 66%;" hidden="hidden">
                    <tbody id="datos_t" align="right">
                    </tbody>
                    <tfooter></tfooter>
                </table>
            </div>
    	</div>
    </div>
</div>

@stop

@section('js-scripts')
<script>
	$('#bancas').change(function(){
        $('#zonales').html('');
        $('#ejecutivos').html('');
        if ($(this).val()=='BC') {
            $("#jefatura").html('');
            $("#jefatura").attr('disabled','disabled');
        }
        $.ajax({
            type: "POST",
            data: {
                bancas: $(this).val(),
            },
            url: APP_URL + 'micumplimiento/be/zonales',
            dataType: 'json',
            success: function (result) {
                if (result) {
                    $('#zonales').html(result);
                    if ($('#zonales').val()!='todos') {
                        $('#zonales').change();
                    }else{
                        $(".tablero").attr('hidden','hidden');
                    }
                }
            },
        });
    });

    $('#zonales').change(function(){
        $('#ejecutivos').html('');
        if ($('#bancas').val()=='BC') {
            $.ajax({
                type: "POST",
                data: {
                    bancas: $('#bancas').val(),
                    cod_sect: $(this).val(),
                },
                url: APP_URL + 'micumplimiento/be/getEjecutivo',
                dataType: 'json',
                success: function (result) {
                    if (result) {
                        $('#ejecutivos').html(result);
                        if ($('#ejecutivos').val()!='') {
                            $('#buscar_').click();
                        }else{
                            $(".tablero").attr('hidden','hidden');
                        }
                    }
                },
            });
        }else if($('#bancas').val()=='BEL'){
            $.ajax({
                type: "POST",
                data: {
                    bancas: $('#bancas').val(),
                    cod_sect: $(this).val(),
                },
                url: APP_URL + 'micumplimiento/be/getJefaturas',
                dataType: 'json',
                success: function (result) {
                    if (result) {
                        $('#jefatura').removeAttr('disabled');
                        $('#jefatura').html(result);
                    }
                },
            });
        }
    });

    $('#jefatura').change(function(){
        $('#ejecutivos').html('');
        if($('#bancas').val()=='BEL'){
            $.ajax({
                type: "POST",
                data: {
                    bancas: $('#bancas').val(),
                    cod_sect: $('#zonales').val(),
                    jefatura: $(this).val(),
                },
                url: APP_URL + 'micumplimiento/be/getEjecutivo',
                dataType: 'json',
                success: function (result) {
                    if (result) {
                        $('#ejecutivos').html(result);
                        if ($('#ejecutivos').val()!='') {
                            $('#buscar_').click();
                        }else{
                            $(".tablero").attr('hidden','hidden');
                        }
                    }
                },
            });
        }
    });

    $('#buscar_').click(function(){
        $(".tablero").attr('hidden','hidden');
        $.ajax({
            type: "POST",
            data: {
                bancas: $("#bancas").val(),
                zonales: $("#zonales").val(),
                periodos: $("#periodos").val(),
                jefatura: $("#jefatura").val(),
                ejecutivos: $("#ejecutivos").val(),
            },
            url: APP_URL + 'micumplimiento/be/detalle',
            dataType: 'json',
            success: function (result) {
                if (result) {
                    $(".tablero").removeAttr('hidden');
                }
                $("#datos_e").html('');
                $("#datos_f").html('');
                $("#datos_a").html('');
                bien="<a><i class='fas fa-check-circle' style='color:green'></i><a>";
                maso="<a><i class='fas fa-exclamation-circle' style='color:#d7e01c'></i><a>";
                mal="<a><i class='fas fa-times-circle' style='color:red'></i><a>";
                fin=result.financiera;
                eco=result.ecosistema;
                ace=result.aceleradores;
                f='';
                sum_f=0;
                sum_f_p=0;
                $.each( fin, function( key, value ) {
                    sum_f+=parseFloat(value['NOTA']);
                    sum_f_p+=parseFloat(value['PESO']);
                	// value['DIFERENCIA']=funre(value['DIFERENCIA']);
                	// value['CUMPLIMIENTO']=funre(value['CUMPLIMIENTO']*100);
                	// value['PESO']=funre(value['PESO']*100);
                	// value['NOTA']=funre(value['NOTA']*100);
                	// value['AVANCE']=funre(value['AVANCE']);
                	// value['META']=funre(value['META']);
                    if (value['CUMPLIMIENTO']>=100) {
					   f+='<tr><td style="text-align: center;"><i class="fa fa-exclamation-circle" data-toggle="tooltip" data-placement="top" title="Aqui deberia mostrar algo" aria-hidden="true"></i>'+value['INDICADOR']+'</td>	<td style="text-align: center;">'+value['UNIDADES_MEDICION']+'</td><td style="text-align: center;">'+value['AVANCE']+'</td><td style="text-align: center;">'+value['META']+'</td><td style="text-align: center;">'+value['DIFERENCIA']+'</td><td style="text-align: center;">'+value['CUMPLIMIENTO']+' %'+bien+'</td>	<td style="text-align: center;">'+value['PESO']+' %</td><td style="text-align: center;">'+value['NOTA']+' %</td></tr>';
                    }else if (value['CUMPLIMIENTO']<100 && 80<value['CUMPLIMIENTO']) {
                       f+='<tr><td style="text-align: center;"><i class="fa fa-exclamation-circle" data-toggle="tooltip" data-placement="top" title="" aria-hidden="true"></i>'+value['INDICADOR']+'</td>  <td style="text-align: center;">'+value['UNIDADES_MEDICION']+'</td><td style="text-align: center;">'+value['AVANCE']+'</td><td style="text-align: center;">'+value['META']+'</td><td style="text-align: center;">'+value['DIFERENCIA']+'</td><td style="text-align: center;">'+value['CUMPLIMIENTO']+' %'+maso+'</td>   <td style="text-align: center;">'+value['PESO']+' %</td><td style="text-align: center;">'+value['NOTA']+' %</td></tr>';
                    }else{
                        f+='<tr><td style="text-align: center;"><i class="fa fa-exclamation-circle" data-toggle="tooltip" data-placement="top" title="" aria-hidden="true"></i>'+value['INDICADOR']+'</td>  <td style="text-align: center;">'+value['UNIDADES_MEDICION']+'</td><td style="text-align: center;">'+value['AVANCE']+'</td><td style="text-align: center;">'+value['META']+'</td><td style="text-align: center;">'+value['DIFERENCIA']+'</td><td style="text-align: center;">'+value['CUMPLIMIENTO']+' %'+mal+'</td>   <td style="text-align: center;">'+value['PESO']+' %</td><td style="text-align: center;">'+value['NOTA']+' %</td></tr>';
                    }
                });
                f+="<tr><td style='text-align: right;' colspan='6'>TOTAL PERSPECTIVA FINANCIERA</td><td align='right'>"+funre(sum_f_p)+"%</td><td align='right'>"+funre(sum_f)+" %</td></tr>";
                //console.log(sum_f);
                $("#datos_f").html(f);


                sum_e=0;
                sum_e_p=0;
                e='';
                $.each( eco, function( key, valu ) {
                    sum_e+=parseFloat(valu['NOTA']);
                    sum_e_p+=parseFloat(valu['PESO']);
                	// valu['DIFERENCIA']=funre(valu['DIFERENCIA']);
                	// valu['CUMPLIMIENTO']=funre(valu['CUMPLIMIENTO']*100);
                	// valu['PESO']=funre(valu['PESO']*100);
                	// valu['NOTA']=funre(valu['NOTA']*100);
                	// valu['AVANCE']=funre(valu['AVANCE']);
                	// valu['META']=funre(valu['META']);
                    if (valu['CUMPLIMIENTO']>=100) {
                        e+='<tr><td style="text-align: center;"><i class="fa fa-exclamation-circle" data-toggle="tooltip" data-placement="top" title="Aqui deberia mostrar algo" aria-hidden="true"></i>'+valu['INDICADOR']+'</td><td style="text-align: center;">'+valu['UNIDADES_MEDICION']+'</td><td style="text-align: center;">'+valu['AVANCE']+'</td><td style="text-align: center;">'+valu['META']+'</td><td style="text-align: center;">'+valu['DIFERENCIA']+'</td><td style="text-align: center;">'+valu['CUMPLIMIENTO']+' %'+bien+'</td><td style="text-align: center;">'+valu['PESO']+' %</td><td style="text-align: center;">'+valu['NOTA']+' %</td></tr>';
                    }else if (valu['CUMPLIMIENTO']<100 && 80<valu['CUMPLIMIENTO']) {
                        e+='<tr><td style="text-align: center;"><i class="fa fa-exclamation-circle" data-toggle="tooltip" data-placement="top" title="" aria-hidden="true"></i>'+valu['INDICADOR']+'</td><td style="text-align: center;">'+valu['UNIDADES_MEDICION']+'</td><td style="text-align: center;">'+valu['AVANCE']+'</td><td style="text-align: center;">'+valu['META']+'</td><td style="text-align: center;">'+valu['DIFERENCIA']+'</td><td style="text-align: center;">'+valu['CUMPLIMIENTO']+' %'+maso+'</td><td style="text-align: center;">'+valu['PESO']+' %</td><td style="text-align: center;">'+valu['NOTA']+' %</td></tr>';
                    }else{
                        e+='<tr><td style="text-align: center;"><i class="fa fa-exclamation-circle" data-toggle="tooltip" data-placement="top" title="" aria-hidden="true"></i>'+valu['INDICADOR']+'</td><td style="text-align: center;">'+valu['UNIDADES_MEDICION']+'</td><td style="text-align: center;">'+valu['AVANCE']+'</td><td style="text-align: center;">'+valu['META']+'</td><td style="text-align: center;">'+valu['DIFERENCIA']+'</td><td style="text-align: center;">'+valu['CUMPLIMIENTO']+' %'+mal+'</td><td style="text-align: center;">'+valu['PESO']+' %</td><td style="text-align: center;">'+valu['NOTA']+' %</td></tr>';
                    }
                });
                e+="<tr><td style='text-align: right;' colspan='6'>TOTAL PERSPECTIVA ECOSISTEMA</td><td align='right'>"+funre(sum_e_p)+"%</td><td align='right'>"+funre(sum_e)+" %</td></tr>";
                //console.log(sum_e*10);

                $("#datos_e").html(e);

                sum_a=0;
                a='';
                $.each( ace, function( key, val ) {
                    sum_a+=parseFloat(val['NOTA']);
                    // val['CUMPLIMIENTO']=funre(val['CUMPLIMIENTO']*100);
                    // val['PESO']=redondeo_2(val['PESO']*100);
                    // val['NOTA']=redondeo_2(val['NOTA']*100);
                	// val['DIFERENCIA']=redondeo_2(val['DIFERENCIA']*100);
                	// val['AVANCE']=redondeo_2(val['AVANCE']*100);
                	// val['META']=redondeo_2(val['META']*100);
                    if (val['CUMPLIMIENTO']>=100) {
                        a+='<tr><td style="text-align: center;"><i class="fa fa-exclamation-circle" data-toggle="tooltip" data-placement="top" title="Aqui deberia mostrar algo" aria-hidden="true"></i>'+val['INDICADOR']+'</td><td style="text-align: center;">'+val['UNIDADES_MEDICION']+'</td><td style="text-align: center;">'+val['AVANCE']+'</td><td style="text-align: center;">'+val['META']+'</td><td style="text-align: center;">'+val['DIFERENCIA']+'</td><td style="text-align: center;">'+val['CUMPLIMIENTO']+' %'+bien+'</td><td style="text-align: center;">'+val['PESO']+' %</td><td style="text-align: center;">'+val['NOTA']+' %</td></tr>';
                    }else if (val['CUMPLIMIENTO']<100 && 80<val['CUMPLIMIENTO']) {
                        a+='<tr><td style="text-align: center;"><i class="fa fa-exclamation-circle" data-toggle="tooltip" data-placement="top" title="" aria-hidden="true"></i>'+val['INDICADOR']+'</td><td style="text-align: center;">'+val['UNIDADES_MEDICION']+'</td><td style="text-align: center;">'+val['AVANCE']+'</td><td style="text-align: center;">'+val['META']+'</td><td style="text-align: center;">'+val['DIFERENCIA']+'</td><td style="text-align: center;">'+val['CUMPLIMIENTO']+' %'+maso+'</td><td style="text-align: center;">'+val['PESO']+' %</td><td style="text-align: center;">'+val['NOTA']+' %</td></tr>';
                    }else{
                        a+='<tr><td style="text-align: center;"><i class="fa fa-exclamation-circle" data-toggle="tooltip" data-placement="top" title="" aria-hidden="true"></i>'+val['INDICADOR']+'</td><td style="text-align: center;">'+val['UNIDADES_MEDICION']+'</td><td style="text-align: center;">'+val['AVANCE']+'</td><td style="text-align: center;">'+val['META']+'</td><td style="text-align: center;">'+val['DIFERENCIA']+'</td><td style="text-align: center;">'+val['CUMPLIMIENTO']+' %'+mal+'</td><td style="text-align: center;">'+val['PESO']+' %</td><td style="text-align: center;">'+val['NOTA']+' %</td></tr>';
                    }
                });
                a+="<tr><td style='text-align: right;' colspan='7'>TOTAL ACELERADOR</td><td align='right'>"+funre(sum_a)+" %</td></tr>";
                //console.log(sum_a*100);

                $("#datos_a").html(a);

                sum_total=sum_f+sum_e+sum_a;
                t="<tr><td style='text-align: right;' colspan='7'>MI CUMPLIMIENTO</td><td 'text-align: right;'>"+funre(sum_total)+" %</td></tr>";
                $("#datos_t").html(t);

                $('[data-toggle="tooltip"]').tooltip();
            }
        });
    });

    function funre(numero)
    {
        var flotante = parseFloat(numero);
        var resultado = Math.round(flotante*100)/100;
        return resultado;
    }

    function redondeo_2(numero)
    {
        var flotante = parseFloat(numero);
        var resultado = Math.round(flotante*1000)/1000;
        return resultado;
    }
</script>
@stop