<html class=" " lang="en"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>WEBVPC | Página no encontrada</title>

    <link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('css/custom/custom.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('css/custom/webvpc.css') }}" rel="stylesheet" type="text/css">

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <!-- page content -->
        <div class="col-md-12">
          <div class="col-middle">
            <div class="text-center text-center">
              <h1 class="error-number">404</h1>
              <h2>Lo sentimos pero no encontramos lo que estás buscando</h2>
              <p>La página que estas buscando no existe</p>
            </div>
          </div>
        </div>
        <!-- /page content -->
      </div>
    </div>
  

</body></html>