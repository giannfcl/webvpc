@extends('Layouts.layout')
@section('js-libs')
<script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.es.min.js') }}"></script>
@stop

@section('pageTitle','Ficha Covid')
@section('tituloDerecha')
@stop
@section('content')
<form id="formCovid" action="{{route('infinity.me.cliente.covid.save')}}" method="POST" class="x_panel">
  <h2><strong>FICHA: COVID-19</strong></h2>
  <hr>
  {{--  Fila2  --}}
  <div class="form-group row">
    <label class="col-sm-2 col-form-label">Código Único</label>
    <div class="col-sm-4">
      <input id="UniqueCode" name="codunico" type="text" class="form-control" value="{{$code}}" readonly="readonly">
    </div>
  </div>
  {{--  Fila3  --}}
  <div class="form-group row">
    <label class="col-sm-2 col-form-label">Nombre del Cliente:</label>
    <div class="col-sm-4">
      <input id="ClientName" type="text" class="form-control" value="{{$cliente->NOMBRE}}" readonly="readonly"><br/>
    </div>
  </div>

  {{--  Pregunta1  --}}
  <div class="form-row">
    <div class="form-group col-md-8">
      <label>1. Indicar si la pandemia COVID-19 ha tenido un impacto negativo en la gestión financiera:</label>
    </div>
    <div class="form-group col-md-2">
      <label>Rpta:</label>
    </div>
    <div class="form-group col-md-2">
      <select id="slcQ1" name="slcQ1" class="form-control custom-select is-invalid" required>
        <option {{isset($datoscovid) && !empty($datoscovid->PREGUNTA_1)?'' : 'selected'}} disabled value="">-Seleccionar-</option>
        <option value="1" {{isset($datoscovid) && $datoscovid->PREGUNTA_1=='1' ?'selected':''}}>Sí</option>
        <option value="2" {{isset($datoscovid) && $datoscovid->PREGUNTA_1=='2' ?'selected':''}}>No</option>
      </select>
    </div>
  </div>

  {{--  Pregunta2  --}}
  <div class="form-row">
    <div class="form-group col-md-8">
      <label> 2. En qué porcentaje se ha reducido su ingreso mensual en marzo 2020 vs meses anteriores</label>
    </div>
    <div class="form-group col-md-1">
      <label>Rpta:</label>
    </div>
     <div class="form-group col-md-1">
       <label style="font-weight: 100;margin-top: 5px;">Porc %</label>
    </div>
    <div class="form-group col-md-2">
       <input id="porcRpta2" name="porcRpta2" onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)" type="number" min="0" max="100" class="form-control" value="{{isset($datoscovid)?$datoscovid->PREGUNTA_2:''}}" required>
    </div>
  </div>

  {{--  Pregunta3  --}}
  <div class="form-row">
    <div class="form-group col-md-8">
      <label>3. En cuánto espera proyectar ventas al cierre del 2020 </label>
    </div>
    <div class="form-group col-md-2">
      <label>Rpta:</label>
    </div>
    <div class="form-group col-md-1">
       <label style="font-weight: 100;">Porc %</label><input id="porcRpta3" name="porcRpta3" type="number" class="form-control" onkeypress="return isNumeric(event)" onchange="validateQuestion3(event,this)"  value="{{isset($datoscovid)?$datoscovid->PREGUNTA_3_CAMPO_1:''}}" required>
    </div>
    <div class="form-group col-md-1">
      <label style="font-weight: 100;">Miles US$</label> <input id="solesRpta3" name="solesRpta3" type="number" class="form-control" onchange="validateQuestion3(event,this)" required value="{{isset($datoscovid)?$datoscovid->PREGUNTA_3_CAMPO_2:''}}" min="0">
    </div>
  </div>

   {{--  Pregunta4 Informativa  --}}
  <div class="form-row">
    <div class="form-group col-md-12">
      <p><strong>4.¿Qué esta haciendo con los vencimientos de obligaciones con sus proveedores que vencen en Marzo y Abril?.</strong></p>
      <p style="padding-left: 1%;"> (Coloque un <i class="fa fa-check-square-o" aria-hidden="true"></i>; en caso seleccionar la primera comentar las preguntas de la primera opción) 
      <strong>(MÚLTIPLE SELECCIÓN)</strong></p>
    </div>
  </div>

  {{--  Check1  --}}
  <div class="form-row">
    <div class="form-group col-md-12" style="padding-left: 3%;">
     <input type="checkbox" class="custom-control-input" id="chk1" name="chk1" required {{isset($datoscovid) && $datoscovid->PREGUNTA_4_CHECK_1?'checked':''}}>
     <label class="form-check-label" for="Check1"> Reprogramación con los proveedores.</label>
    </div>
  </div>

    {{--  DivCheck1-1 --}}
  <div id="divCheck1-1" class="form-row">
    <div class="form-group col-md-8" style="padding-left: 3%;">
        <label style="font-weight:normal">¿A qué plazo reprogramó?</label>
    </div>
     <div class="form-group col-md-1">
      <label>Rpta:</label>
    </div>
    <div class="form-group col-md-2">
        <select id="slcQ4-plazo" name="slcQ4-plazo" class="form-control custom-select is-invalid" >
            <option {{isset($datoscovid) && !empty($datoscovid->PREGUNTA_4_CHECK_1_Q1_A)?'' : 'selected'}} disabled value="">-Seleccionar-</option>
            <option value="días" {{isset($datoscovid) && $datoscovid->PREGUNTA_4_CHECK_1_Q1_A=='días'?'selected' : ''}}>Días</option>
            <option value="meses" {{isset($datoscovid) && $datoscovid->PREGUNTA_4_CHECK_1_Q1_A=='meses'?'selected' : ''}}>Meses</option>
        </select>
    </div>
    <div class="form-group col-md-1">
       <input id="txtQ4-plazo" name="txtQ4-plazo" type="number" class="form-control" value="{{isset($datoscovid)?$datoscovid->PREGUNTA_4_CHECK_1_Q1_B:''}}" min="0">
    </div>
  </div>

    {{--  DivCheck1-1-2 --}}
  <div id="divCheck1-2" class="form-row">
    <div class="form-group col-md-8" style="padding-left: 3%;">
        <label style="font-weight:normal">¿Ha tenido cancelación de sus líneas?</label>
    </div>
    <div class="form-group col-md-1">
      <label>Rpta:</label>
    </div>
    <div class="form-group col-md-2">
      <select id="slcChk1-2" class="form-control custom-select is-invalid" name="slcChk1-2">
        <option {{isset($datoscovid) && !empty($datoscovid->PREGUNTA_4_CHECK_1_Q2_A)?'' : 'selected'}} disabled value="">-Seleccionar-</option>
        <option value="1" {{isset($datoscovid) && $datoscovid->PREGUNTA_4_CHECK_1_Q2_A=='1'?'selected' : ''}}>Sí</option>
        <option value="2" {{isset($datoscovid) && $datoscovid->PREGUNTA_4_CHECK_1_Q2_A=='2'?'selected' : ''}}>No</option>
      </select>
    </div>
     <div class="form-group col-md-1">
    </div>
  </div>

  {{--  Check2  --}}
  <div class="form-row">
    <div class="form-group col-md-12" style="padding-left: 3%;">
     <input type="checkbox" class="custom-control-input" id="chk2" name="chk2" required {{isset($datoscovid) && $datoscovid->PREGUNTA_4_CHECK_2?'checked':''}}>
     <label class="form-check-label" for="Check2"> Cancelación con la liquidez de la empresa.</label>
    </div>
  </div>

  {{--  Check3  --}}
  <div class="form-row">
    <div class="form-group col-md-12" style="padding-left: 3%;">
     <input type="checkbox" class="custom-control-input" id="chk3" name="chk3" required {{isset($datoscovid) && $datoscovid->PREGUNTA_4_CHECK_3?'checked':''}}>
     <label class="form-check-label" for="Check3"> Tomar préstamos de bancos para cancelar a los proveedores.</label>
    </div>
  </div>

  {{--  Pregunta5  --}}
  <div class="form-row">
    <div class="form-group col-md-8">
      <label for="lblQ5">5.¿Qué porcentaje o monto de su costo de ventas y gastos operativos es fijo? </label>
    </div>
    <div class="form-group col-md-2">
      <label>Rpta:</label>
    </div>
    <div class="form-group col-md-1">
       <label style="font-weight: 100;">Porc %</label><input id="porcRpta5"  name="porcRpta5" type="number" class="form-control" oninput="maxLengthCheck(this)" onkeypress="return isNumeric(event)" onchange="validateQuestion5(event,this)" required value="{{isset($datoscovid)?$datoscovid->PREGUNTA_5_CAMPO_1:''}}" min="0" max="100">
    </div>
    <div class="form-group col-md-1">
       <label style="font-weight: 100;">Miles US$</label><input id="solesRpta5" name="solesRpta5" type="number" class="form-control"  onkeypress="return isNumeric(event)" onchange="validateQuestion5(event,this)" required value="{{isset($datoscovid)?$datoscovid->PREGUNTA_5_CAMPO_2:''}}" min="0">
    </div>
  </div>

  {{--  Pregunta6  --}}
  <div class="form-row">
    <div class="form-group col-md-8">
      <label for="lblQ6">6.¿Cuenta con líneas disponible con sus bancos?</label>
    </div>
    <div class="form-group col-md-2">
      <label for="lblRpta6">Rpta:</label>
    </div>
    <div class="form-group col-md-2">
      <select id="slcQ6" name="slcQ6"  class="form-control custom-select is-invalid" onchange="validateQuestion6(event,this)" required>
        <option {{isset($datoscovid) && !empty($datoscovid->PREGUNTA_6)?'' : 'selected'}} disabled value="">-Seleccionar-</option>
        <option value="1" {{isset($datoscovid) && $datoscovid->PREGUNTA_6=='1'?'selected' : ''}}>Sí</option>
        <option value="2" {{isset($datoscovid) && $datoscovid->PREGUNTA_6=='2'?'selected' : ''}}>No</option>
      </select>
    </div>
  </div>

  {{--  Pregunta6-1 --}}
  <div id="divRpta6-1" class="form-row">
    <div class="form-group col-md-8">
      <label style="font-weight: normal">¿Cuál es el monto disponible?  (incluye Interbank y todo el sistema bancario)</label>
    </div>
    <div class="form-group col-md-1">
      <label>Rpta:</label>
    </div>
    <div class="form-group col-md-1">
       <label style="margin-top: 5px;font-weight: normal">Miles US$</label>
    </div>
    <div class="form-group col-md-2">
      <input id="solesRpta6-1" name="solesRpta6-1" type="number" class="form-control" onkeypress="return filterFloat();" value="{{isset($datoscovid)?$datoscovid->PREGUNTA_6_Q1:''}}" min="0">
    </div>
  </div>

    {{--  Pregunta7  --}}
  <div class="form-row">
    <div class="form-group col-md-8">
      <label for="lblQ7">7. Realiza importación / exportación a algún país que se encuentra afectado fuertemente con el COVID19.</label>
    </div>
    <div class="form-group col-md-2">
      <label for="lblRpta7">Rpta:</label>
    </div>
    <div class="form-group col-md-2">
      <select id="slcQ7" name="slcQ7" class="form-control custom-select is-invalid" onchange="validateQuestion7(event,this)" required>
        <option {{isset($datoscovid) && !empty($datoscovid->PREGUNTA_7)?'' : 'selected'}} disabled value="">-Seleccionar-</option>
        <option value="0" {{isset($datoscovid) && $datoscovid->PREGUNTA_7=='0'?'selected' : ''}}>Ninguna</option>
        <option value="1" {{isset($datoscovid) && $datoscovid->PREGUNTA_7=='1'?'selected' : ''}}>Imp</option>
        <option value="2" {{isset($datoscovid) && $datoscovid->PREGUNTA_7=='2'?'selected' : ''}}>Exp</option>
        <option value="3" {{isset($datoscovid) && $datoscovid->PREGUNTA_7=='3'?'selected' : ''}}>Ambas</option>
      </select>
    </div>
  </div>

    {{--  Pregunta7-1 --}}
  <div id="divRpta7-1" class="form-row">
    <div class="form-group col-md-8">
     <label style="font-weight: normal">¿En caso ud se dedique a importar, indique cuántos meses de stock mantiene?</label>
    </div>
    <div class="form-group col-md-2">
      <label>Rpta:</label>
    </div>
    <div class="form-group col-md-1">
      <label style="margin-top: 10px;font-weight: 100;">N° Meses</label>
    </div>
    <div class="form-group col-md-1">
      <input id="solesRpta7-1" name="solesRpta7-1" type="number" class="form-control"  value="{{isset($datoscovid)?$datoscovid->PREGUNTA_7_Q1:''}}" min="0">
    </div>
  </div>

      {{--  Pregunta7-2 --}}
  <div id="divRpta7-2" class="form-row">
    <div class="form-group col-md-8">
     <label style="font-weight: normal">Si marco importación o exportación comentar el monto que importa o exporta al país afectado</label>
    </div>
    <div class="form-group col-md-2">
      <label>Rpta:</label>
    </div>
    <div class="form-group col-md-1">
      <label style="font-weight: 100;">Exp Miles US$</label><input id="ExporRpta7-2" name="ExporRpta7-2" type="number" class="form-control" onkeypress="return isNumeric(event)" value="{{isset($datoscovid)?$datoscovid->PREGUNTA_7_Q2_CAMPO_1:''}}" min="0">
    </div>
     <div class="form-group col-md-1">
      <label style="font-weight: 100;">Imp Miles US$</label><input id="ImporRpta7-2" name="ImporRpta7-2" type="number" class="form-control" onkeypress="return isNumeric(event)" value="{{isset($datoscovid)?$datoscovid->PREGUNTA_7_Q2_CAMPO_2:''}}" min="0">
    </div>
  </div>

        {{--  Pregunta7-3 --}}
  <div id="divRpta7-3" class="form-row">
    <div class="form-group col-md-8">
     <label style="font-weight: normal">¿Qué porcentaje estima que se verá afectado?</label>
    </div>
    <div class="form-group col-md-2">
      <label>Rpta:</label>
    </div>
    <div class="form-group col-md-1">
       <label style="margin-top: 5px;font-weight: 100;">Porc %</label>
    </div>
    <div class="form-group col-md-1">
      <input id="porcRpta7-3" name="porcRpta7-3" type="number" class="form-control" oninput="maxLengthCheck(this)" onkeypress="return isNumeric(event)" value="{{isset($datoscovid)?$datoscovid->PREGUNTA_7_Q3:''}}" min="0" max="100">
    </div>
  </div>

        {{--  Pregunta7-4 --}}
  <div id="divRpta7-4" class="form-row">
    <div class="form-group col-md-8">
     <label style="font-weight: normal">¿Cuenta con proveedores o mercados sustitutos?</label>
    </div>
    <div class="form-group col-md-2">
      <label>Rpta:</label>
    </div>
    <div class="form-group col-md-2">
       <select id="slcRpta7-4" name="slcRpta7-4" class="form-control"class="form-control custom-select is-invalid" >
        <option {{isset($datoscovid) && !empty($datoscovid->PREGUNTA_7_Q4)?'' : 'selected'}} disabled value="">-Seleccionar-</option>
        <option value="1" {{isset($datoscovid) && $datoscovid->PREGUNTA_7_Q4=='1'?'selected' : ''}}>Sí</option>
        <option value="2" {{isset($datoscovid) && $datoscovid->PREGUNTA_7_Q4=='2'?'selected' : ''}}>No</option>
      </select>
    </div>
  </div>

    {{--  Pregunta8 --}}
  
  <div class="form-row">
    <div class="form-group col-md-8">
      <label>8. ¿Qué porcentaje o monto de sus cuentas por cobrar han sido realizadas desde que inicio el COVID19?</label>
    </div>
    <div class="form-group col-md-2">
    </div>
    <div class="form-group col-md-1">
    </div>
    <div class="form-group col-md-1">
  </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-8">
      <p style="color:#FF0000;margin-left:2%;font-size:12px">*Solo para clientes con una gestión financiera consolidada y profesional.</p>
    </div>
    <div class="form-group col-md-2">
      <label>Rpta:</label>
    </div>
    <div class="form-group col-md-1">
     <label style="font-weight: 100;">Porc %</label><input id="txtQ8-porcentaje" name="txtQ8-porcentaje" type="number" class="form-control" oninput="maxLengthCheck(this)" onchange="validateQuestion8(event,this)" onkeypress="return isNumeric(event)" required value="{{isset($datoscovid)?$datoscovid->PREGUNTA_8_CAMPO_1:''}}" min="0" max="100">
    </div>
    <div class="form-group col-md-1">
     <label style="font-weight: 100;">Miles US$</label><input id="txtQ8-monto" name="txtQ8-monto" type="number" class="form-control" onchange="validateQuestion8(event,this)" onkeypress="return isNumeric(event)" required value="{{isset($datoscovid)?$datoscovid->PREGUNTA_8_CAMPO_2:''}}" min="0">
    </div>
  </div>

     {{--  Pregunta9 --}}
  <div class="form-row">
    <div class="form-group col-md-8">
      <label>9.  Indicar si en el 2020, ha realizado inversiones o se encuentra relizando inversiones.</label>
    </div>
    <div class="form-group col-md-2">
      <label>Rpta:</label>
    </div>
    <div class="form-group col-md-2">
       <select id="slcQ9" name="slcQ9" class="form-control custom-select is-invalid" onchange="validateQuestion9()" required>
        <option {{isset($datoscovid) && !empty($datoscovid->PREGUNTA_9)?'' : 'selected'}} disabled value="">-Seleccionar-</option>
        <option value="1" {{isset($datoscovid) && $datoscovid->PREGUNTA_9=='1'?'selected' : ''}}>Sí</option>
        <option value="2" {{isset($datoscovid) && $datoscovid->PREGUNTA_9=='2'?'selected' : ''}}>No</option>
      </select>
    </div>
  </div>

     {{--  Pregunta9-1 --}}
  <div id="divRpta9-1" class="form-row">
    <div class="form-group col-md-8">
     <label style="font-weight: normal">¿Cuál es  el monto de la inversión?</label>
    </div>
    <div class="form-group col-md-2">
      <label>Rpta:</label>
    </div>
    <div class="form-group col-md-2">
        <label style="font-weight: 100;">Miles US$</label><input id="txtQ9monto" name="txtQ9monto" type="number" class="form-control" onchange="validateQuestion9_1()" onkeypress="return isNumeric(event)" value="{{isset($datoscovid)?$datoscovid->PREGUNTA_9_Q1:''}}" min="0">
    </div>
  </div>

     {{--  Pregunta9-2 --}}
  <div id="divRpta9-2" class="form-row">
    <div class="form-group col-md-8">
     <label style="font-weight: normal">¿Cómo lo financío?</label>
    </div>
    <div class="form-group col-md-2">
      <label>Rpta:</label>
    </div>
    <div class="form-group col-md-2">
      <select id="slcQ9-2" name="slcQ9-2" class="form-control custom-select is-invalid" onchange="validateQuestion9_2()">
        <option {{isset($datoscovid) && !empty($datoscovid->PREGUNTA_9_Q2)?'' : 'selected'}} disabled value="">-Seleccionar-</option>
        <option value="Banco">Banco</option>
        <option value="Recursos Propios" {{isset($datoscovid) && $datoscovid->PREGUNTA_9_Q2=='Recursos Propios'?'selected' : ''}}>Recursos Propios</option>
        <option value="Accionistas" {{isset($datoscovid) && $datoscovid->PREGUNTA_9_Q2=='Accionistas'?'selected' : ''}}>Accionistas</option>
      </select>
    </div>
  </div>

       {{--  Pregunta9-2-1 --}}
  <div id="divRpta9-2-1" class="form-row">
    <div class="form-group col-md-5">
    </div>
    <div class="form-group col-md-3">
     <label>¿Qué porcentaje financío?</label>
    </div>
    <div class="form-group col-md-2">
      <label>Rpta:</label>
    </div>
    <div class="form-group col-md-1">
      <label style="margin-top: 5px;font-weight: 100;">Porc %</label>
    </div>
    <div class="form-group col-md-1">
        <input id="txtQ9-Porcentaje" name="txtQ9-Porcentaje" type="number" class="form-control" oninput="maxLengthCheck(this)" onkeypress="return isNumeric(event)" value="{{isset($datoscovid)?$datoscovid->PREGUNTA_9_Q2_Q1:''}}" min="0" max="100">
    </div>
  </div>

         {{--  Pregunta9-2-2 --}}
  <div id="divRpta9-2-2" class="form-row">
    <div class="form-group col-md-1">
    </div>
    <div class="form-group col-md-7">
     <label style="margin-left: 60%;">Plazo de Financiamiento:</label>
    </div>
    <div class="form-group col-md-1">
      <label>Rpta:</label>
    </div>
    <div class="form-group col-md-2">
          <select id="slcQ9-2-2-plazo" name="slcQ9-2-2-plazo" class="form-control custom-select is-invalid">
              <option {{isset($datoscovid) && !empty($datoscovid->PREGUNTA_9_Q2_Q2_CAMPO_A)?'' : 'selected'}} disabled value="">-Seleccionar-</option>
              <option value="días" {{isset($datoscovid) && $datoscovid->PREGUNTA_9_Q2_Q2_CAMPO_A=='días'?'selected' : ''}}>Días</option>
              <option value="meses" {{isset($datoscovid) && $datoscovid->PREGUNTA_9_Q2_Q2_CAMPO_A=='meses'?'selected' : ''}}>Meses</option>
          </select>
    </div>
    <div class="form-group col-md-1">
        <input id="txtQ9anio" name="txtQ9anio" type="number" class="form-control" value="{{isset($datoscovid)?$datoscovid->PREGUNTA_9_Q2_Q2_CAMPO_B:''}}" min="0">
    </div>
  </div>

    {{--  Pregunta10 --}}
  <div class="form-row">
    <div class="form-group col-md-12">
      <label>10. ¿Qué medidas administrativas y financieras están tomando o tomarán para afrontar la coyuntura y poder mitigar los efectos del COVID19 adversos sobre el negocio?(Comentar) * </label>
    </div>
  </div>

  <div class="form-row">
    <div class="form-group col-md-12">
      <textarea id="commentQ10" name="commentQ10" class="form-control is-invalid" placeholder="Ingrese sus comentarios aquí." style="width:100%" required>{{isset($datoscovid)?$datoscovid->PREGUNTA_10:''}}</textarea>
    </div>
  </div>

   {{--  Pregunta11 --}}
  <div class="form-row">
    <div class="form-group col-md-12">
      <label>Conclusión:</label>
    </div>
  </div>

  <div class="form-row">
    <div class="form-group col-md-12">
      <p><strong>11. Definir dentro de las siguientes 3 alternativas el grado de impacto negativo al COVID19:</strong>
      Coloque un <i class="fa fa-check-square-o" aria-hidden="true"></i><strong> (ÚNICA SELECCIÓN)</strong></p>
    </div>
  </div>

    {{--  Check4  --}}
  <div class="form-row">
    <div class="form-group col-md-4" style="padding-left: 3%;">
     <input class="form-check-input" type="checkbox" value="" id="chkQ11Bajo" name="chkQ11Bajo" required {{isset($datoscovid) && $datoscovid->PREGUNTA_11 == 'bajo' ?'checked':''}}>
     <label class="form-check-label"> Bajo 
      <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" 
      title="Aquellos empresas que a pesar de la cuarentena han seguido operando pero con un menor volumen de ingresos."></i>
     </label>
    </div>

    {{--  Check5  --}}
    <div class="form-group col-md-4" style="padding-left: 3%;">
     <input type="checkbox" class="form-check-input" id="chkQ11Medio" name="chkQ11Medio" required {{isset($datoscovid) && $datoscovid->PREGUNTA_11 == 'medio' ?'checked':''}}>
     <label class="form-check-label"> Medio 
        <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" 
        title="Aquellas empresas que no están operando dentro de la cuarentena, sin embargo su salud financiera le ha permitido cubrir obligaciones corrientes o aquellas que siguen operando pero sus ingresos han sido afectados fuertemente."></i>
     </label>
    </div>

    {{--  Check6  --}}
    <div class="form-group col-md-4" style="padding-left: 3%;">
     <input type="checkbox" class="form-check-input" id="chkQ11Alto" name="chkQ11Alto" required {{isset($datoscovid) && $datoscovid->PREGUNTA_11 == 'alto' ?'checked':''}}>
     <label class="form-check-label"> Alto 
      <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" 
        title="Aquellas empresas que no están operando dentro de la cuarentena y han tenido que recordar personal y/o tomar préstamos para poder cubrir las obligaciones corrientes."></i>
     </label>
    </div>
  </div>

  {{--  Pregunta12 --}}
  <div class="form-row">
    <div class="form-group col-md-8">
      <label>12. ¿Cuál es el monto de necesidades de capital de trabajo que tiene el cliente?</label>
    </div>
    <div class="form-group col-md-1">
      <label>Rpta.</label>
    </div>
    <div class="form-group col-md-1">
      <label style="margin-top: 5px;font-weight: 100;">Miles US$</label>
    </div>
    <div class="form-group col-md-2">
     <input id="txtQ12-monto" name="txtQ12-monto" type="number" class="form-control" onchange="validateQuestion12(event,this)" onkeypress="return isNumeric(event)" required value="{{isset($datoscovid)?$datoscovid->PREGUNTA_12:''}}" min="0">
    </div>
  </div>

  <div id="divPreguta12">
    {{--  Pregunta12-SubPregunta1 --}}
    <div class="form-row">
      <div class="form-group col-md-10" style="padding-left:20px">
      <label>¿Cuál es el destino de necesidades indicadas? (Seleccione con <i class="fa fa-check-square-o" aria-hidden="true"></i>)</label>
      </div>
      <div class="form-group col-md-2">
      </div>
    </div>

    {{--  ChecksRow1  --}}
    <div class="form-row">
      <div class="form-group col-md-4" style="padding-left:20px">
          <input type="checkbox" class="form-check-input" id="chkQ12Costo" name="chkQ12Costo"  {{isset($datoscovid) && $datoscovid->PREGUNTA_12_Q1_CHECK_1?'checked':''}}>
          <label class="form-check-label"> Costos fijos</label>
      </div>
      <div class="form-group col-md-4">
          <input type="checkbox" class="form-check-input" id="chkQ12Proveedor" name="chkQ12Proveedor"  {{isset($datoscovid) && $datoscovid->PREGUNTA_12_Q1_CHECK_2?'checked':''}}>
          <label class="form-check-label"> Proveedores</label>
      </div>
      <div class="form-group col-md-4">
          <input type="checkbox" class="form-check-input" id="chkQ12Planilla" name="chkQ12Planilla"  {{isset($datoscovid) && $datoscovid->PREGUNTA_12_Q1_CHECK_3?'checked':''}}>
          <label class="form-check-label"> Planilla</label>
      </div>
    </div>

    {{--  ChecksRow2  --}}
    <div class="form-row">
      <div class="form-group col-md-4" style="padding-left:20px">
          <input type="checkbox" class="form-check-input" id="chkQ12Impuesto" name="chkQ12Impuesto" {{isset($datoscovid) && $datoscovid->PREGUNTA_12_Q1_CHECK_4?'checked':''}}>
          <label class="form-check-label"> Impuesto</label>
      </div>
      <div class="form-group col-md-4">
          <input type="checkbox" class="form-check-input" id="chkQ12Otros" name="chkQ12Otros" {{isset($datoscovid) && $datoscovid->PREGUNTA_12_Q1_CHECK_5?'checked':''}}>
          <label class="form-check-label"> Otros</label>
      </div>
      <div class="form-group col-md-4">
          <label class="form-check-label"> Especifique:</label>
          <input type="text" class="form-control" id="txtQ12Especifique" name="txtQ12Especifique" readonly="readonly" value="{{isset($datoscovid)?$datoscovid->PREGUNTA_12_Q1_CAMPO:''}}">
      </div>
    </div>

    {{--  Pregunta12-SubPregunta2 --}}
    <div class="form-row">
      <div class="form-group col-md-12">
        <label>¿Cuál será el medio de repago?</label>
      </div>
    </div>
    <div class="form-row">
      <div class="form-group col-md-12">
        <textarea id="comment2" name="comment2" class="form-control is-invalid" placeholder="Ingrese sus comentarios aquí." style="width:100%">{{isset($datoscovid)?$datoscovid->PREGUNTA_12_Q2:''}}</textarea>
       <br/>     
      </div>
    </div>

     {{--  Pregunta12-SubPregunta3 --}}
    <div class="form-row">
      <div class="form-group col-md-4">
        <label>Plazo de Financiamiento:</label>
      </div>
      <div class="form-group col-md-4">
      </div>
      <div class="form-group col-md-2">
          <select id="slcQ12-plazo" name="slcQ12-plazo" class="form-control custom-select is-invalid">
              <option {{isset($datoscovid) && !empty($datoscovid->PREGUNTA_12_Q3_CAMPO_A)?'' : 'selected'}} disabled value="">-Seleccionar-</option>
              <option value="días" {{isset($datoscovid) && $datoscovid->PREGUNTA_12_Q3_CAMPO_A=='días'?'selected' : ''}}>Días</option>
              <option value="meses" {{isset($datoscovid) && $datoscovid->PREGUNTA_12_Q3_CAMPO_A=='meses'?'selected' : ''}}>Meses</option>
          </select>
      </div>
      <div class="form-group col-md-2">
        <input id="txtQ12-plazo" name="txtQ12-plazo" type="number" class="form-control" value="{{isset($datoscovid)?$datoscovid->PREGUNTA_12_Q3_CAMPO_B:''}}" min="0">
         <br/> 
         <br/>       
      </div>
    </div>
  </div>

  {{--  Footer  --}}

<div class="form-row">
  <div class="form-group col-md-4">
  <label>EJECUTIVO DE NEGOCIO:</label>
  </div>
   <div class="form-group col-md-1">
        
  </div>
   <div class="form-group col-md-7">
        <label>{{$cliente->EJECUTIVO}}</label>
  </div>
</div>

<div class="form-row">
  <div class="form-group col-md-4">
        <label>USUARIO:</label>
  </div>
   <div class="form-group col-md-1">
        
  </div>
   <div class="form-group col-md-7">
        <label>{{$username}}</label>
  </div>
</div>

<div class="form-row">
  <div class="form-group col-md-4">
        <label>FECHA VISITA:</label>
  </div>
   <div class="form-group col-md-1">
        
  </div>
   <div class="form-group col-md-2">
       <input autocomplete="off" class="form-control dfecha"  type="text" name="fechaVisita" placeholder="Seleccionar fecha" value="{{isset($datoscovid)?$datoscovid->FECHA_VISITA:null}}" required> 
  </div>
     <div class="form-group col-md-5">
  </div>
</div>

<div class="form-row">
  <div class="form-group col-md-8">
  </div>
  <div class="form-group col-md-2">
        
  </div>
  <div class="form-group col-md-2" >
        <button type="submit" class="btn btn-primary btn-lg btn-block guardar">Guardar Ficha</button>
  </div>
</div>
</form>
@stop

@section('js-scripts')
<script>

$(document).ready(function () 
{
    $('.dfecha').each(function () {
        $(this).on('keydown', function () {
            return false;
        });
        $(this).datepicker({
            maxViewMode: 1,
            language: "es",
            autoclose: true,
            startDate: "-365d",
            endDate: "0d",
            format: "yyyy-mm-dd",
        });
    });
    
    var puedeguardar ="{{$puedeguardar}}";
    var seguardo = "{{$seguardo}}";

    if (!puedeguardar || seguardo==1) {
      $("input").attr('disabled','disabled');
      $("textarea").attr('disabled','disabled');
      $("select").attr('disabled','disabled');
      $(".guardar").attr('disabled','disabled');
    }
    
    document.getElementById("divCheck1-1").style.display = "none";
    document.getElementById("divCheck1-2").style.display = "none";
    document.getElementById("divRpta6-1").style.display = "none";
    document.getElementById("divRpta7-1").style.display = "none";
    document.getElementById("divRpta7-2").style.display = "none";
    document.getElementById("divRpta7-3").style.display = "none";
    document.getElementById("divRpta7-4").style.display = "none";

    document.getElementById("divRpta9-1").style.display = "none";
    document.getElementById("divRpta9-2").style.display = "none";
    document.getElementById("divRpta9-2-1").style.display = "none";
    document.getElementById("divRpta9-2-2").style.display = "none";
    
    if($('#txtQ12-monto').val() != "0" && $('#txtQ12-monto').val() != ""){
        document.getElementById("divPreguta12").style.display = "block";
    }
    else{
      document.getElementById("divPreguta12").style.display = "none";
    }

    if ($('select[name="slcQ6"]').val() == "1") {
        document.getElementById("divRpta6-1").style.display = "block";
    } else {
        document.getElementById("divRpta6-1").style.display = "none";
    }

    if ($('select[name="slcQ7"]').val() == "1" || $('select[name="slcQ7"]').val() == "3") {
            document.getElementById("divRpta7-1").style.display = "block";
            document.getElementById("divRpta7-2").style.display = "block";
            document.getElementById("divRpta7-3").style.display = "block";
            document.getElementById("divRpta7-4").style.display = "block";
    } else if ($('select[name="slcQ7"]').val() == "2") {
            document.getElementById("divRpta7-1").style.display = "none";
            document.getElementById("divRpta7-2").style.display = "block";
            document.getElementById("divRpta7-3").style.display = "block";
            document.getElementById("divRpta7-4").style.display = "block";
    } 
    else {
        document.getElementById("divRpta7-1").style.display = "none";
        document.getElementById("divRpta7-2").style.display = "none";
        document.getElementById("divRpta7-3").style.display = "none";
        document.getElementById("divRpta7-4").style.display = "none";
    }
    
      if ($('#chk1').is(":checked")) {
            document.getElementById("divCheck1-1").style.display = "block";
            document.getElementById("divCheck1-2").style.display = "block";
      }else {
            document.getElementById("divCheck1-1").style.display = "none";
            document.getElementById("divCheck1-2").style.display = "none";
      }

      if ($('select[name="slcQ9"]').val() == "1") {
             document.getElementById("divRpta9-1").style.display = "block";
             document.getElementById("divRpta9-2").style.display = "block";
      } else {
            document.getElementById("divRpta9-1").style.display = "none";
            document.getElementById("divRpta9-2").style.display = "none";
      }

      if ($('select[name="slcQ9-2"]').val() == "Banco") {
            document.getElementById("divRpta9-2-1").style.display = "block";
            document.getElementById("divRpta9-2-2").style.display = "block";
      } else {
          document.getElementById("divRpta9-2-1").style.display = "none";
          document.getElementById("divRpta9-2-2").style.display = "none";
      }

    {{--  Evento Check1  --}}
   $('#chk1').change(function (){
      var x = document.getElementById("divCheck1-1");
      var y = document.getElementById("divCheck1-2");
       
      if (this.checked) {
             x.style.display = "block";
             y.style.display = "block";
      }else {
            x.style.display = "none";
            y.style.display = "none";

            $('#slcQ4-plazo').val("");
            $('#slcChk1-2').val("");
            $('#txtQ4-plazo').val(null);
      }
    });
    
    {{--  Evento Check 1  --}}
    $('#chk1').change(function (){
        if (this.checked) {
          document.getElementById("slcQ4-plazo").required=true;
          document.getElementById("txtQ4-plazo").required=true;
          document.getElementById("slcChk1-2").required=true;

          document.getElementById("chk2").required=false;
          document.getElementById("chk3").required=false;
        }
        else if($('#chk3').is(":checked")){
            document.getElementById("slcQ4-plazo").required=false;
            document.getElementById("txtQ4-plazo").required=false;
            document.getElementById("slcChk1-2").required=false;

            document.getElementById("chk1").required=false;
            document.getElementById("chk2").required=false;
          }
        else if($('#chk2').is(":checked")){
            document.getElementById("slcQ4-plazo").required=false;
            document.getElementById("txtQ4-plazo").required=false;
            document.getElementById("slcChk1-2").required=false;

            document.getElementById("chk1").required=false;
            document.getElementById("chk2").required=false;
          }
        else{
          document.getElementById("slcQ4-plazo").required=false;
          document.getElementById("txtQ4-plazo").required=false;
          document.getElementById("slcChk1-2").required=false;

          document.getElementById("chk2").required=true;
          document.getElementById("chk3").required=true;
        }
    });

        {{--  Evento Check 2  --}}
    $('#chk2').change(function (){
        if (this.checked) {
          document.getElementById("chk1").required=false;
          document.getElementById("chk3").required=false;
        }
        else if($('#chk3').is(":checked")){
            document.getElementById("chk1").required=false;
            document.getElementById("chk2").required=false;
          }
        else if($('#chk1').is(":checked")){
            document.getElementById("chk1").required=false;
            document.getElementById("chk3").required=false;
          }
        else{
          document.getElementById("chk1").required=true;
          document.getElementById("chk3").required=true;
        }
    });

       {{--  Evento Check 3  --}}
    $('#chk3').change(function (){
        if (this.checked) {
          document.getElementById("chk1").required=false;
          document.getElementById("chk2").required=false;
        }
        else if($('#chk2').is(":checked")){
            document.getElementById("chk1").required=false;
            document.getElementById("chk3").required=false;
          }
        else if($('#chk1').is(":checked")){
            document.getElementById("chk1").required=false;
            document.getElementById("chk3").required=false;
          }
        else{
          document.getElementById("chk1").required=true;
          document.getElementById("chk2").required=true;
        }
    });

     {{--  Evento Seletec Pregunta6  --}}
    $('#slcQ6').change(function (){
        var x = document.getElementById("divRpta6-1");
       
        if ($('select[name="slcQ6"]').val() == "1") {
             x.style.display = "block";
        } else {
            x.style.display = "none";
            $('#solesRpta6-1').val(null);
        }
    });

     {{--  Evento Select Pregunta7  --}}
    $('#slcQ7').change(function (){
        var x = document.getElementById("divRpta7-1");
        var y = document.getElementById("divRpta7-2");
        var w = document.getElementById("divRpta7-3");
        var z = document.getElementById("divRpta7-4");
       
        if ($('select[name="slcQ7"]').val() == "1" || $('select[name="slcQ7"]').val() == "3") {
                x.style.display = "block";
                y.style.display = "block";
                w.style.display = "block";
                z.style.display = "block";

                $('#solesRpta7-1').val(null);
                $('#ExporRpta7-2').val(null);
                $('#ImporRpta7-2').val(null);
                $('#porcRpta7-3').val(null);
                $('#slcRpta7-4').val("");

        } else if ($('select[name="slcQ7"]').val() == "2") {
                x.style.display = "none";
                y.style.display = "block";
                w.style.display = "block";
                z.style.display = "block";

                $('#solesRpta7-1').val(null);
                $('#ExporRpta7-2').val(null);
                $('#ImporRpta7-2').val(null);
                $('#porcRpta7-3').val(null);
                $('#slcRpta7-4').val("");
        } 
        else {
            x.style.display = "none";
            y.style.display = "none";
            w.style.display = "none";
            z.style.display = "none";

            $('#solesRpta7-1').val(null);
            $('#ExporRpta7-2').val(null);
            $('#ImporRpta7-2').val(null);
            $('#porcRpta7-3').val(null);
            $('#slcRpta7-4').val("");
            
        }
    });

      {{--  Evento Seletec Pregunta9  --}}
    $('#slcQ9').change(function (){
        var x = document.getElementById("divRpta9-1");
        var y = document.getElementById("divRpta9-2");
       
        if ($('select[name="slcQ9"]').val() == "1") {
             x.style.display = "block";
             y.style.display = "block";
        } else {
            x.style.display = "none";
            y.style.display = "none";

             $('#txtQ9monto').val(null);
             $('#slcQ9-2').val("");
             $('#slcQ9-2-2-plazo').val("");
             $('#txtQ9-Porcentaje').val(null);
             $('#txtQ9anio').val(null);
             document.getElementById("divRpta9-2-1").style.display = "none";
             document.getElementById("divRpta9-2-2").style.display = "none";

            document.getElementById("txtQ9-Porcentaje").required=false;
            document.getElementById("txtQ9anio").required=false;
        }
    });

         {{--  Evento Seletec Pregunta9-2 --}}
    $('#slcQ9-2').change(function (){
        var x = document.getElementById("divRpta9-2-1");
        var y = document.getElementById("divRpta9-2-2");
       
        if ($('select[name="slcQ9-2"]').val() == "Banco") {
             x.style.display = "block";
             y.style.display = "block";
        } else {
            x.style.display = "none";
            y.style.display = "none";

            $('#txtQ9-Porcentaje').val(null);
            $('#txtQ9anio').val(null);
            $('#slcQ9-2-2-plazo').val("");
        }
    });

     {{--  Evento Select Pregunta11  --}}
    $("#chkQ11Bajo").on('click', function() {
     var chkQ11Medio = "input:checkbox[name='chkQ11Medio']";
     var chkQ11Alto = "input:checkbox[name='chkQ11Alto']";
     var $box = $(this);
        if ($box.is(":checked")) {
            $(chkQ11Medio).prop("checked", false);
            $(chkQ11Alto).prop("checked", false);

            $box.prop("checked", true);
            $box.prop('required', false);

            $(chkQ11Medio).prop("required", false);
            $(chkQ11Alto).prop("required", false);
        } else {
                 $box.prop("checked", false);
                 $box.prop('required', true);

                 $(chkQ11Medio).prop("checked", false);
                 $(chkQ11Alto).prop("checked", false);

                $(chkQ11Medio).prop("required", false);
                $(chkQ11Alto).prop("required", false);
        }
    });

     {{--  Evento Select Pregunta11  --}}
    $("#chkQ11Medio").on('click', function() {
     var chkQ11Bajo = "input:checkbox[name='chkQ11Bajo']";
     var chkQ11Alto = "input:checkbox[name='chkQ11Alto']";
     var $box = $(this);
        if ($box.is(":checked")) {
            $(chkQ11Bajo).prop("checked", false);
            $(chkQ11Alto).prop("checked", false);

            $box.prop("checked", true);
            $box.prop('required', false);

            $(chkQ11Bajo).prop("required", false);
            $(chkQ11Alto).prop("required", false);
        } else {
                 $box.prop("checked", false);
                 $box.prop('required', true);

                 $(chkQ11Bajo).prop("checked", false);
                 $(chkQ11Alto).prop("checked", false);

                $(chkQ11Bajo).prop("required", false);
                $(chkQ11Alto).prop("required", false);
        }
    });

    {{--  Evento Select Pregunta11  --}}
    $("#chkQ11Alto").on('click', function() {
    
     var chkQ11Bajo = "input:checkbox[name='chkQ11Bajo']";
     var chkQ11Medio = "input:checkbox[name='chkQ11Medio']";
     var $box = $(this);
        if ($box.is(":checked")) {
            $(chkQ11Bajo).prop("checked", false);
            $(chkQ11Medio).prop("checked", false);

            $box.prop("checked", true);
            $box.prop('required', false);

            $(chkQ11Bajo).prop("required", false);
            $(chkQ11Medio).prop("required", false);
        } else {
                 $box.prop("checked", false);
                 $box.prop('required', true);

                 $(chkQ11Bajo).prop("checked", false);
                 $(chkQ11Medio).prop("checked", false);

                $(chkQ11Bajo).prop("required", false);
                $(chkQ11Medio).prop("required", false);
        }
    });

    $('#chkQ12Otros').change(function (){
        var x = document.getElementById("txtQ12Especifique");
       
        if (this.checked) {
            x.readOnly= false;
        } else {
            x.value= "";
            x.readOnly= true;
        }
    });

    $('#chkQ12Otros').change(function (){
        var x = document.getElementById("txtQ12Especifique");
        var $box = $(this);
        
        if ($box.is(":checked")) {
           x.required=true;
        } else {
           x.required=false;
        }
    });


      {{--  Evento Check Costo Fijo Pregunta 12  --}}
    $('#chkQ12Costo').change(function (){
        if (this.checked) {
          document.getElementById("chkQ12Proveedor").required=false;
          document.getElementById("chkQ12Planilla").required=false;
          document.getElementById("chkQ12Impuesto").required=false;
          document.getElementById("chkQ12Otros").required=false;
        }
        else if($('#chkQ12Proveedor').is(":checked")){
            document.getElementById("chkQ12Planilla").required=false;
            document.getElementById("chkQ12Impuesto").required=false;
            document.getElementById("chkQ12Otros").required=false;
            document.getElementById("chkQ12Costo").required=false;
          }
        else if($('#chkQ12Planilla').is(":checked")){
            document.getElementById("chkQ12Proveedor").required=false;
            document.getElementById("chkQ12Impuesto").required=false;
            document.getElementById("chkQ12Otros").required=false;
            document.getElementById("chkQ12Costo").required=false;
          }
        else if($('#chkQ12Impuesto').is(":checked")){
            document.getElementById("chkQ12Proveedor").required=false;
            document.getElementById("chkQ12Planilla").required=false;
            document.getElementById("chkQ12Otros").required=false;
            document.getElementById("chkQ12Costo").required=false;
          }
        else if($('#chkQ12Otros').is(":checked")){
            document.getElementById("chkQ12Proveedor").required=false;
            document.getElementById("chkQ12Planilla").required=false;
            document.getElementById("chkQ12Impuesto").required=false;
            document.getElementById("chkQ12Costo").required=false;
          }
        else{
              document.getElementById("chkQ12Proveedor").required=true;
              document.getElementById("chkQ12Planilla").required=true;
              document.getElementById("chkQ12Impuesto").required=true;
              document.getElementById("chkQ12Otros").required=true;
              document.getElementById("chkQ12Costo").required=true;
          }
    });

          {{--  Evento Check Proveedor Pregunta 12  --}}
    $('#chkQ12Proveedor').change(function (){
        if (this.checked) {
          document.getElementById("chkQ12Costo").required=false;
          document.getElementById("chkQ12Planilla").required=false;
          document.getElementById("chkQ12Impuesto").required=false;
          document.getElementById("chkQ12Otros").required=false;
        }
        else if($('#chkQ12Costo').is(":checked")){
            document.getElementById("chkQ12Planilla").required=false;
            document.getElementById("chkQ12Impuesto").required=false;
            document.getElementById("chkQ12Otros").required=false;
            document.getElementById("chkQ12Proveedor").required=false;
        }
        else if($('#chkQ12Impuesto').is(":checked")){
            document.getElementById("chkQ12Planilla").required=false;
            document.getElementById("chkQ12Proveedor").required=false;
            document.getElementById("chkQ12Impuesto").required=false;
            document.getElementById("chkQ12Costo").required=false;
          }
        else if($('#chkQ12Planilla').is(":checked")){
            document.getElementById("chkQ12Proveedor").required=false;
            document.getElementById("chkQ12Impuesto").required=false;
            document.getElementById("chkQ12Otros").required=false;
            document.getElementById("chkQ12Costo").required=false;
          }
        else if($('#chkQ12Otros').is(":checked")){
            document.getElementById("chkQ12Proveedor").required=false;
            document.getElementById("chkQ12Planilla").required=false;
            document.getElementById("chkQ12Impuesto").required=false;
            document.getElementById("chkQ12Costo").required=false;
          }
        else{
              document.getElementById("chkQ12Proveedor").required=true;
              document.getElementById("chkQ12Planilla").required=true;
              document.getElementById("chkQ12Impuesto").required=true;
              document.getElementById("chkQ12Otros").required=true;
              document.getElementById("chkQ12Costo").required=true;
          }
    });

           {{--  Evento Check Planilla Pregunta 12  --}}
    $('#chkQ12Planilla').change(function (){
        if (this.checked) {
          document.getElementById("chkQ12Costo").required=false;
          document.getElementById("chkQ12Proveedor").required=false;
          document.getElementById("chkQ12Impuesto").required=false;
          document.getElementById("chkQ12Otros").required=false;
        }
        else if($('#chkQ12Costo').is(":checked")){
            document.getElementById("chkQ12Planilla").required=false;
            document.getElementById("chkQ12Impuesto").required=false;
            document.getElementById("chkQ12Otros").required=false;
            document.getElementById("chkQ12Proveedor").required=false;
        }
        else if($('#chkQ12Impuesto').is(":checked")){
            document.getElementById("chkQ12Planilla").required=false;
            document.getElementById("chkQ12Proveedor").required=false;
            document.getElementById("chkQ12Impuesto").required=false;
            document.getElementById("chkQ12Costo").required=false;
          }
        else if($('#chkQ12Proveedor').is(":checked")){
            document.getElementById("chkQ12Otros").required=false;
            document.getElementById("chkQ12Planilla").required=false;
            document.getElementById("chkQ12Impuesto").required=false;
            document.getElementById("chkQ12Costo").required=false;
          }
        else if($('#chkQ12Otros').is(":checked")){
            document.getElementById("chkQ12Proveedor").required=false;
            document.getElementById("chkQ12Planilla").required=false;
            document.getElementById("chkQ12Impuesto").required=false;
            document.getElementById("chkQ12Costo").required=false;
          }
        else{
              document.getElementById("chkQ12Proveedor").required=true;
              document.getElementById("chkQ12Planilla").required=true;
              document.getElementById("chkQ12Impuesto").required=true;
              document.getElementById("chkQ12Otros").required=true;
              document.getElementById("chkQ12Costo").required=true;
          }
    });

        {{--  Evento Check Impuesto Pregunta 12  --}}
    $('#chkQ12Impuesto').change(function (){
        if (this.checked) {
          document.getElementById("chkQ12Costo").required=false;
          document.getElementById("chkQ12Proveedor").required=false;
          document.getElementById("chkQ12Planilla").required=false;
          document.getElementById("chkQ12Otros").required=false;
        }
        else if($('#chkQ12Costo').is(":checked")){
            document.getElementById("chkQ12Planilla").required=false;
            document.getElementById("chkQ12Impuesto").required=false;
            document.getElementById("chkQ12Otros").required=false;
            document.getElementById("chkQ12Proveedor").required=false;
        }
        else if($('#chkQ12Proveedor').is(":checked")){
            document.getElementById("chkQ12Otros").required=false;
            document.getElementById("chkQ12Planilla").required=false;
            document.getElementById("chkQ12Impuesto").required=false;
            document.getElementById("chkQ12Costo").required=false;
          }
        else if($('#chkQ12Otros').is(":checked")){
            document.getElementById("chkQ12Proveedor").required=false;
            document.getElementById("chkQ12Planilla").required=false;
            document.getElementById("chkQ12Impuesto").required=false;
            document.getElementById("chkQ12Costo").required=false;
          }
        else if($('#chkQ12Planilla').is(":checked")){
            document.getElementById("chkQ12Otros").required=false;
            document.getElementById("chkQ12Proveedor").required=false;
            document.getElementById("chkQ12Impuesto").required=false;
            document.getElementById("chkQ12Costo").required=false;
          }
        else{
              document.getElementById("chkQ12Proveedor").required=true;
              document.getElementById("chkQ12Planilla").required=true;
              document.getElementById("chkQ12Impuesto").required=true;
              document.getElementById("chkQ12Otros").required=true;
              document.getElementById("chkQ12Costo").required=true;
          }
    });

        {{--  Evento Check Otros Pregunta 12  --}}
    $('#chkQ12Otros').change(function (){
        if (this.checked) {
          document.getElementById("chkQ12Costo").required=false;
          document.getElementById("chkQ12Proveedor").required=false;
          document.getElementById("chkQ12Planilla").required=false;
          document.getElementById("chkQ12Impuesto").required=false;
        }
        else if($('#chkQ12Costo').is(":checked")){
            document.getElementById("chkQ12Planilla").required=false;
            document.getElementById("chkQ12Impuesto").required=false;
            document.getElementById("chkQ12Otros").required=false;
            document.getElementById("chkQ12Proveedor").required=false;
        }
        else if($('#chkQ12Proveedor').is(":checked")){
            document.getElementById("chkQ12Otros").required=false;
            document.getElementById("chkQ12Planilla").required=false;
            document.getElementById("chkQ12Impuesto").required=false;
            document.getElementById("chkQ12Costo").required=false;
          }
        else if($('#chkQ12Impuesto').is(":checked")){
            document.getElementById("chkQ12Proveedor").required=false;
            document.getElementById("chkQ12Planilla").required=false;
            document.getElementById("chkQ12Otros").required=false;
            document.getElementById("chkQ12Costo").required=false;
          }
        else if($('#chkQ12Planilla').is(":checked")){
            document.getElementById("chkQ12Otros").required=false;
            document.getElementById("chkQ12Proveedor").required=false;
            document.getElementById("chkQ12Impuesto").required=false;
            document.getElementById("chkQ12Costo").required=false;
          }
        else{
              document.getElementById("chkQ12Proveedor").required=true;
              document.getElementById("chkQ12Planilla").required=true;
              document.getElementById("chkQ12Impuesto").required=true;
              document.getElementById("chkQ12Otros").required=true;
              document.getElementById("chkQ12Costo").required=true;
          }
    });
});

function validateQuestion3(evt,input){
  if(input.id == "porcRpta3"){
      if($('#porcRpta3').val() != ""){
      document.getElementById("solesRpta3").required=false;
      }
  }
  else{
      if($('#solesRpta3').val()!= ""){
      document.getElementById("porcRpta3").required=false;
      }
  }
};

function validateQuestion5(evt,input){
  if(input.id == "porcRpta5"){
      if($('#porcRpta5').val() != ""){
      document.getElementById("solesRpta5").required=false;
      }
  }
  else{
      if($('#solesRpta5').val()!= ""){
      document.getElementById("porcRpta5").required=false;
      }
  }
};

function validateQuestion6(){
    if($('#slcQ6').val() != "1"){
      document.getElementById("solesRpta6-1").required=false;
    }
    else{
          document.getElementById("solesRpta6-1").required=true;
    }
};

function validateQuestion7(){
    {{--  IMPORTACION  --}}
    if($('#slcQ7').val() == "1")
    {
      document.getElementById("ExporRpta7-2").required=false;
      document.getElementById("solesRpta7-1").required=true;
      document.getElementById("ImporRpta7-2").required=true;
      document.getElementById("porcRpta7-3").required=true;
      document.getElementById("slcRpta7-4").required=true;
    }
    {{--  EXPORTACION  --}}
    else if($('#slcQ7').val() == "2")
    {
       document.getElementById("ImporRpta7-2").required=false;
      document.getElementById("porcRpta7-3").required=true;
      document.getElementById("slcRpta7-4").required=true;
      document.getElementById("ExporRpta7-2").required=true;
    }
    {{--  AMBAS  --}}
    else if($('#slcQ7').val() == "3")
    {
      document.getElementById("solesRpta7-1").required=true;
      document.getElementById("ImporRpta7-2").required=true;
      document.getElementById("ExporRpta7-2").required=true;
      document.getElementById("porcRpta7-3").required=true;
      document.getElementById("slcRpta7-4").required=true;
    }
    else{
      document.getElementById("solesRpta7-1").required=false;
      document.getElementById("ImporRpta7-2").required=false;
      document.getElementById("ExporRpta7-2").required=false;
      document.getElementById("porcRpta7-3").required=false;
      document.getElementById("slcRpta7-4").required=false;

    }
};

function validateQuestion8(evt,input){
  if(input.id == "txtQ8-porcentaje"){
      if($('#txtQ8-porcentaje').val() != ""){
      document.getElementById("txtQ8-monto").required=false;
      }
  }
  else{
      if($('#txtQ8-monto').val()!= ""){
      document.getElementById("txtQ8-porcentaje").required=false;
      }
  }
};

function validateQuestion9(){
   if($('#slcQ9').val() != "1"){
      document.getElementById("txtQ9monto").required=false;
      document.getElementById("slcQ9-2").required=false;
    }
    else{
          document.getElementById("txtQ9monto").required=true;
          document.getElementById("slcQ9-2").required=true;
    }
};

function validateQuestion9_1(){
  if($('#txtQ9monto').val() != ""){
      document.getElementById("txtQ9monto").required=false;
  }
};

function validateQuestion9_2(){
  if($('#slcQ9-2').val() != "Banco"){
      document.getElementById("txtQ9-Porcentaje").required=false;
      document.getElementById("slcQ9-2-2-plazo").required=false;
      document.getElementById("txtQ9anio").required=false;
  }
  else{ 
       document.getElementById("txtQ9-Porcentaje").required=true;
       document.getElementById("slcQ9-2-2-plazo").required=true;
       document.getElementById("txtQ9anio").required=true;
  }
};

function validateQuestion12(evt,input){
  if($('#txtQ12-monto').val() > 0){
      document.getElementById("divPreguta12").style.display = "block";
      document.getElementById("comment2").required=true;
      document.getElementById("slcQ12-plazo").required=true;
      document.getElementById("txtQ12-plazo").required=true;

      document.getElementById("chkQ12Costo").required=true;
      document.getElementById("chkQ12Proveedor").required = true;
      document.getElementById("chkQ12Planilla").required = true;
      document.getElementById("chkQ12Impuesto").required = true;
      document.getElementById("chkQ12Otros").required = true;
  }
  else{ 
      document.getElementById("divPreguta12").style.display = "none";
      document.getElementById("comment2").required=false;
      document.getElementById("slcQ12-plazo").required=false;
      document.getElementById("txtQ12-plazo").required=false;

      document.getElementById("chkQ12Costo").required=false;
      document.getElementById("chkQ12Proveedor").required = false;
      document.getElementById("chkQ12Planilla").required = false;
      document.getElementById("chkQ12Impuesto").required = false;
      document.getElementById("chkQ12Otros").required = false;

      $('#comment2').val(null);
      $('#slcQ12-plazo').val("");
      $('#txtQ12-plazo').val(null);
      $('#txtQ12Especifique').val(null);

      $(chkQ12Costo).prop("checked", false);
      $(chkQ12Proveedor).prop("checked", false);
      $(chkQ12Planilla).prop("checked", false);
      $(chkQ12Impuesto).prop("checked", false);
      $(chkQ12Otros).prop("checked", false);
      document.getElementById("txtQ12Especifique").readOnly=true;

  }
};

function maxLengthCheck(object) {
  if (object.value > 100){
      object.value = object.value.slice(0, 2)
    }
};

function isNumeric (evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode (key);
    var regex = /[0-9]|\./;
    if ( !regex.test(key) ) {
      theEvent.returnValue = false;
      if(theEvent.preventDefault) theEvent.preventDefault();
    }
};

</script>
@stop