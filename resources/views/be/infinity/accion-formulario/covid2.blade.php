@extends('Layouts.layout')
@section('js-libs')
<script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.es.min.js') }}"></script>
@stop

@section('pageTitle','Ficha Covid')
@section('tituloDerecha')
@stop
@section('content')

<div class="row" style="padding: 20px;">
  <a style="
      border-style: none;
      font-size: 20px;
  " href="{{ route('infinity.me.visita.historia',['id'=>$idHistorica])}}"><i class="icony fa fa-arrow-left"></i> Volver </a>
</div>
<form id="formCovid" class="x_panel">
    <input type="text"style="height:0; visibility:hidden" name="id" id="id" value="{{isset($datoscovid)?$datoscovid->ID:''}}">
  <h2><strong>FICHA: COVID-19</strong></h2>
  <hr>
  {{--  Fila2  --}}
  <div class="form-group row">
    <label class="col-sm-2 col-form-label">Código Único</label>
    <div class="col-sm-4">
      <input id="codunico" name="codunico" type="text" class="form-control" value="{{$code}}" readonly="readonly">
    </div>
  </div>
  {{--  Fila3  --}}
  <div class="form-group row">
    <label class="col-sm-2 col-form-label">Nombre del Cliente:</label>
    <div class="col-sm-4">
      <input id="ClientName" type="text" class="form-control" value="{{$cliente->NOMBRE}}" readonly="readonly"><br/>
    </div>
  </div>
<div style="height:40px;"></div>
  {{--  Pregunta1  --}}
  <div class="form-row row">
    <div class="form-group col-md-8">
      <label>1. Indicar si la pandemia COVID-19, ha tenido un impacto negativo en la gestión:</label>
    </div>
    <div class="form-group col-md-2">
    </div>
    <div class="form-group col-md-2">
      <select id="slcQ1" name="slcQ1" class="form-control custom-select is-invalid cerrado" required>
        <option {{isset($datoscovid) && !empty($datoscovid->PREGUNTA_1)?'' : 'selected'}} disabled value="">-Seleccionar-</option>
        <option value="1" {{isset($datoscovid) && $datoscovid->PREGUNTA_1=='1' ?'selected':''}}>Sí</option>
        <option value="2" {{isset($datoscovid) && $datoscovid->PREGUNTA_1=='2' ?'selected':''}}>No</option>
      </select>
    </div>
  </div>
  <div style="height:40px;"></div>

  {{--  Pregunta2  --}}
  <div class="form-row row">
    <div class="form-group col-md-6">
      <label> 2. ¿Se encuentra actualmente operando?</label>
    </div>
    <div class="form-group col-md-1"></div>
    <div class="form-group col-md-2">
      <select id="slcQ2_1" name="slcQ2_1" class="form-control custom-select is-invalid cerrado" required>
          <option {{isset($datoscovid) && !empty($datoscovid->PREGUNTA_2_1)?'' : 'selected'}} disabled value="">-Seleccionar-</option>
          <option value="1" {{isset($datoscovid) && $datoscovid->PREGUNTA_2_1=='1' ?'selected':''}}>Sí</option>
          <option value="2" {{isset($datoscovid) && $datoscovid->PREGUNTA_2_1=='2' ?'selected':''}}>No</option>
        </select>
    </div>
    <div class="form-group col-md-3">
    </div>
  </div>
  <div class="form-row row">
    <div class="form-group col-md-6">
      <label>&nbsp;&nbsp; ¿Se acogió a la suspensión perfecta de labores?</label>
    </div>
    <div class="form-group col-md-1"></div>
    <div class="form-group col-md-2">
      <select id="slcQ2_2" name="slcQ2_2" class="form-control custom-select is-invalid cerrado" required>
          <option {{isset($datoscovid) && !empty($datoscovid->PREGUNTA_2_2)?'' : 'selected'}} disabled value="">-Seleccionar-</option>
          <option value="1" {{isset($datoscovid) && $datoscovid->PREGUNTA_2_2=='1' ?'selected':''}}>Sí</option>
          <option value="2" {{isset($datoscovid) && $datoscovid->PREGUNTA_2_2=='2' ?'selected':''}}>No</option>
        </select>
    </div>
    <div class="form-group col-md-3">
    </div>
  </div>
  <div class="form-row row">
    <div class="form-group col-md-6">
      <label>&nbsp;&nbsp; ¿Redujo el personal?</label>
    </div>
    <div class="form-group col-md-1"></div>
    <div class="form-group col-md-2">
      <select id="slcQ2_3_1" name="slcQ2_3_1" class="form-control custom-select is-invalid cerrado" required>
          <option {{isset($datoscovid) && !empty($datoscovid->PREGUNTA_2_3_1)?'' : 'selected'}} disabled value="">-Seleccionar-</option>
          <option value="1" {{isset($datoscovid) && $datoscovid->PREGUNTA_2_3_1=='1' ?'selected':''}}>Sí</option>
          <option value="2" {{isset($datoscovid) && $datoscovid->PREGUNTA_2_3_1=='2' ?'selected':''}}>No</option>
        </select>
    </div>
     <div style="{{isset($datoscovid) && $datoscovid->PREGUNTA_2_3_1=='1' ?'':'display:none'}}" identificador="DIV2_1" class="form-group col-md-1 DIV2">
       <label style="font-weight: 100;margin-top: 5px;">Porc %</label>
    </div>
    <div style="{{isset($datoscovid) && $datoscovid->PREGUNTA_2_3_1=='1' ?'':'display:none'}}" identificador="DIV2_1" class="form-group col-md-2 DIV2">
       <input id="slcQ2_3_2" name="slcQ2_3_2" type="number" min="0" max="100" class="form-control cerrado" value="{{isset($datoscovid)?$datoscovid->PREGUNTA_2_3_2:''}}" {{isset($datoscovid) && $datoscovid->PREGUNTA_2_3_1=='1'?'required' : ''}}>
    </div>
  </div>
<div style="height:40px;"></div>
  {{--  Pregunta3  --}}
  <div class="form-row row">
    <div class="form-group col-md-12">
      <label>3. Otras medidas administrativas y financieras que se están toman o tomarán para afrontar la coyuntura y poder mitigar los efectos del COVID-19 adversos sobre el negocio:</label>
    </div>
  </div>

  <div class="form-row row">
    <div class="form-group col-md-12">
      <textarea maxlength="500" id="slcQ3" name="slcQ3" class="form-control is-invalid cerrado" placeholder="Ingrese sus comentarios aquí." style="width:100%; height:81px;">{{isset($datoscovid)?$datoscovid->PREGUNTA_3:''}}</textarea>
    </div>
  </div>
  <div style="height:40px;"></div>
  {{--  Pregunta4  --}}
  <div class="form-row row">
    <div class="form-group col-md-6">
      <label> 4. En qué porcentaje se ha reducido su ingreso mensual en lo que va del 2020</label>
    </div>
    <div class="form-group col-md-1">
    </div>
    <div class="form-group col-md-2">
    </div>
     <div class="form-group col-md-1">
       <label style="font-weight: 100;margin-top: 5px;">Porc %</label>
    </div>
    <div class="form-group col-md-2">
       <input id="slcQ4" name="slcQ4" type="number" min="0" max="100" class="form-control cerrado" value="{{isset($datoscovid)?$datoscovid->PREGUNTA_4:''}}" required>
    </div>
  </div>
  <div style="height:40px;"></div>
  {{--  Pregunta5  --}}
  <div class="form-row row">
    <div class="form-group col-md-6">
      <label>5. En cuánto espera proyectar ventas al cierre del 2020 </label>
      <label>(En el caso de porcentaje, indicar la variación respecto al año anterior)</label>
    </div>
    <div class="form-group col-md-2">
    </div>
    <div class="form-group col-md-2">
       <label style="font-weight: 100;">Porc %</label>
       <input min="-999" max="999" id="slcQ5_1" name="slcQ5_1" type="number" class="form-control cerrado" value="{{isset($datoscovid)?$datoscovid->PREGUNTA_5_1:''}}" required>
    </div>
    <div class="form-group col-md-2">
      <label style="font-weight: 100;">Miles US$</label>
      <input id="slcQ5_2" name="slcQ5_2" type="number" class="form-control cerrado" required value="{{isset($datoscovid)?$datoscovid->PREGUNTA_5_2:''}}" min="0" max="999999">
    </div>
  </div>
  <div style="height:40px;"></div>
  {{--  Pregunta6  --}}
  <div class="form-row row">
    <div class="form-group col-md-6">
      <label for="lblQ5">6. ¿Qué porcentaje o monto de su costo de ventas y gastos operativos es fijo? </label>
    </div>
    <div class="form-group col-md-2">
    </div>
    <div class="form-group col-md-2">
       <label style="font-weight: 100;">Porc %</label><input id="slcQ6_1"  name="slcQ6_1" type="number" class="form-control cerrado" required value="{{isset($datoscovid)?$datoscovid->PREGUNTA_6_1:''}}" min="0" max="100">
    </div>
    <div class="form-group col-md-2">
       <label style="font-weight: 100;">Miles US$</label><input id="slcQ6_2" name="slcQ6_2" type="number" class="form-control cerrado" required value="{{isset($datoscovid)?$datoscovid->PREGUNTA_6_2:''}}" min="0">
    </div>
  </div>
  <div style="height:40px;"></div>
  {{--  Pregunta7  --}}
  <div class="form-row row">
    <div class="form-group col-md-8">
      <label for="slcQ7">7. Realiza importación / exportación a algún país que se encuentra afectado fuertemente con el COVID19.</label>
    </div>
    <div class="form-group col-md-2">
    </div>
    <div class="form-group col-md-2">
      <select onchange="show('DIV7',this)" id="slcQ7" name="slcQ7" class="form-control custom-select is-invalid cerrado" onchange="" required>
        <option {{isset($datoscovid) && !empty($datoscovid->PREGUNTA_7)?'' : 'selected'}} disabled value="">-Seleccionar-</option>
        <option value="0" {{isset($datoscovid) && $datoscovid->PREGUNTA_7=='0'?'selected' : ''}}>Ninguna</option>
        <option value="1" {{isset($datoscovid) && $datoscovid->PREGUNTA_7=='1'?'selected' : ''}}>Imp</option>
        <option value="2" {{isset($datoscovid) && $datoscovid->PREGUNTA_7=='2'?'selected' : ''}}>Exp</option>
        <option value="3" {{isset($datoscovid) && $datoscovid->PREGUNTA_7=='3'?'selected' : ''}}>Ambas</option>
      </select>
    </div>
  </div>

  {{--  Pregunta7-1 --}}
  <div id="divRpta7-1"  style="{{isset($datoscovid) && in_array($datoscovid->PREGUNTA_7,array('1','3'))?'':'display:none'}}"identificador="DIV7_13"  class="form-row DIV7">
    <div class="form-group col-md-6">
     <label style="font-weight: normal">¿En caso ud se dedique a importar, indique cuántos meses de stock mantiene?</label>
    </div>
    <div class="form-group col-md-2">
    </div>
    <div class="form-group col-md-1">
      <label style="margin-top: 10px;font-weight: 100;">N° Meses</label>
    </div>
    <div class="form-group col-md-2">
    </div>
    <div class="form-group col-md-1">
      <input id="slcQ7_1" name="slcQ7_1" type="number" class="form-control cerrado"  value="{{isset($datoscovid)?$datoscovid->PREGUNTA_7_1:''}}" min="0" max="999">
    </div>
  </div>

  {{--  Pregunta7-2 --}}
  <div id="divRpta7-2"  style="{{isset($datoscovid) && in_array($datoscovid->PREGUNTA_7,array('1','2','3'))?'':'display:none'}}"identificador="DIV7_123"  class="form-row DIV7">
    <div class="form-group col-md-6">
     <label style="font-weight: normal">Si marco importación o exportación comentar el monto que importa o exporta al país afectado</label>
    </div>
    <div class="form-group col-md-2">
    </div>
    <div class="form-group col-md-1">
      <label style="font-weight: 100;">Exp Miles US$</label><input id="slcQ7_2_1" name="slcQ7_2_1" type="number" class="form-control cerrado" value="{{isset($datoscovid)?$datoscovid->PREGUNTA_7_2_1:''}}" min="0" max="999999999">
    </div>
    <div class="form-group col-md-2">
    </div>
     <div class="form-group col-md-1">
      <label style="font-weight: 100;">Imp Miles US$</label><input id="slcQ7_2_2" name="slcQ7_2_2" type="number" class="form-control cerrado" value="{{isset($datoscovid)?$datoscovid->PREGUNTA_7_2_2:''}}" min="0" max="999999999">
    </div>
  </div>

  {{--  Pregunta7-3 --}}
  <div id="divRpta7-3"  style="{{isset($datoscovid) && in_array($datoscovid->PREGUNTA_7,array('1','2','3'))?'':'display:none'}}"identificador="DIV7_123"  class="form-row DIV7">
    <div class="form-group col-md-6">
     <label style="font-weight: normal">¿Qué porcentaje estima que se verá afectado?</label>
    </div>
    <div class="form-group col-md-2">
    </div>
    <div class="form-group col-md-1">
       <label style="margin-top: 5px;font-weight: 100;">Porc %</label>
    </div>
    <div class="form-group col-md-2">
    </div>
    <div class="form-group col-md-1">
      <input id="slcQ7_3" name="slcQ7_3" type="number" class="form-control cerrado" value="{{isset($datoscovid)?$datoscovid->PREGUNTA_7_3:''}}" min="0" max="100">
    </div>
  </div>

  {{--  Pregunta7-4 --}}
  <div id="divRpta7-4" style="{{isset($datoscovid) && in_array($datoscovid->PREGUNTA_7,array('1','2','3'))?'':'display:none'}}" identificador="DIV7_123" class="form-row DIV7">
    <div class="form-group col-md-6">
     <label style="font-weight: normal">¿Cuenta con proveedores o mercados sustitutos?</label>
    </div>
    <div class="form-group col-md-2">
    </div>
    <div class="form-group col-md-4">
       <select id="slcQ7_4" name="slcQ7_4" class="form-control custom-select is-invalid cerrado" >
        <option {{isset($datoscovid) && !empty($datoscovid->PREGUNTA_7_4)?'' : 'selected'}} disabled value="">-Seleccionar-</option>
        <option value="1" {{isset($datoscovid) && $datoscovid->PREGUNTA_7_4=='1'?'selected' : ''}}>Sí</option>
        <option value="2" {{isset($datoscovid) && $datoscovid->PREGUNTA_7_4=='2'?'selected' : ''}}>No</option>
      </select>
    </div>
  </div>
  <div style="height:40px;"></div>
  {{--  Pregunta8 Informativa  --}}
  <div class="form-row row">
    <div class="form-group col-md-12">
      <p><strong>8. ¿Qué está haciendo con los vencimientos de obligaciones con sus proveedores? (Próximas a vencer)?</strong></p>
      <p style="padding-left: 1%;"> (Coloque un <i class="fa fa-check-square-o" aria-hidden="true"></i>; en caso seleccionar la primera comentar las preguntas de la primera opción) 
      <strong>(MÚLTIPLE SELECCIÓN)</strong></p>
    </div>
  </div>

  {{--  Check1  --}}
  <div class="form-row row">
    <div class="form-group col-md-12" style="padding-left: 3%;">
     <input type="checkbox" class="custom-control-input cerrado" id="slcQ8_1" name="slcQ8_1" {{isset($datoscovid) && $datoscovid->PREGUNTA_8_1?'checked':''}}>
     <label class="form-check-label" for="Check1"> Reprogramación con los proveedores.</label>
    </div>
  </div>

    {{--  DivCheck1-1-1 --}}
  <div identificador="DIV81_TRUE" style="{{isset($datoscovid) && $datoscovid->PREGUNTA_8_1?'':'display:none'}}" id="divCheck1-1" class="form-row row DIV81">
    <div class="form-group col-md-8" style="padding-left: 3%;">
        <label style="font-weight:normal">¿A qué plazo reprogramó?</label>
    </div>
    <div class="form-group col-md-1">
    </div>
    <div class="form-group col-md-2">
        <select id="slcQ8_1_1_1" name="slcQ8_1_1_1" class="form-control custom-select is-invalid cerrado" >
            <option {{isset($datoscovid) && !empty($datoscovid->PREGUNTA_8_1_1_1)?'' : 'selected'}} disabled value="">-Seleccionar-</option>
            <option value="días" {{isset($datoscovid) && $datoscovid->PREGUNTA_8_1_1_1=='días'?'selected' : ''}}>Días</option>
            <option value="meses" {{isset($datoscovid) && $datoscovid->PREGUNTA_8_1_1_1=='meses'?'selected' : ''}}>Meses</option>
        </select>
    </div>
    <div class="form-group col-md-1">
       <input id="slcQ8_1_1_2" name="slcQ8_1_1_2" type="number" class="form-control cerrado" value="{{isset($datoscovid)?$datoscovid->PREGUNTA_8_1_1_2:''}}" min="0" max="999">
    </div>
  </div>

    {{--  DivCheck1-1-2 --}}
  <div identificador="DIV81_TRUE"  style="{{isset($datoscovid) && $datoscovid->PREGUNTA_8_1?'':'display:none'}}"  id="divCheck1-2" class="form-row row DIV81">
    <div class="form-group col-md-8" style="padding-left: 3%;">
        <label style="font-weight:normal">¿Ha tenido cancelación de sus líneas?</label>
    </div>
    <div class="form-group col-md-1">
    </div>
    <div class="form-group col-md-2">
      <select id="slcQ8_1_2" class="form-control custom-select is-invalid cerrado" name="slcQ8_1_2">
        <option {{isset($datoscovid) && !empty($datoscovid->PREGUNTA_8_1_2)?'' : 'selected'}} disabled value="">-Seleccionar-</option>
        <option value="1" {{isset($datoscovid) && $datoscovid->PREGUNTA_8_1_2=='1'?'selected' : ''}}>Sí</option>
        <option value="2" {{isset($datoscovid) && $datoscovid->PREGUNTA_8_1_2=='2'?'selected' : ''}}>No</option>
      </select>
    </div>
     <div class="form-group col-md-1">
    </div>
  </div>

  {{--  Check2  --}}
  <div class="form-row row">
    <div class="form-group col-md-12" style="padding-left: 3%;">
     <input type="checkbox" class="custom-control-input cerrado" id="slcQ8_2" name="slcQ8_2" {{isset($datoscovid) && $datoscovid->PREGUNTA_8_2?'checked':''}}>
     <label class="form-check-label" for="Check2"> Cancelación con la liquidez de la empresa.</label>
    </div>
  </div>

  {{--  Check3  --}}
  <div class="form-row row">
    <div class="form-group col-md-8" style="padding-left: 3%;">
     <input type="checkbox" class="custom-control-input cerrado" id="slcQ8_3" name="slcQ8_3"  {{isset($datoscovid) && $datoscovid->PREGUNTA_8_3?'checked':''}}>
     <label class="form-check-label" for="Check3"> Tomar préstamos de bancos para cancelar a los proveedores.</label>
    </div>
    <div identificador="DIV8.3_TRUE" style="{{isset($datoscovid) && $datoscovid->PREGUNTA_8_3?'':'display:none'}}"  class="form-group col-md-2 DIV8.3">
      <label>Monto US$</label>
      <input id="slcQ8_3_1"name="slcQ8_3_1"  {{isset($datoscovid) && $datoscovid->PREGUNTA_8_3?'required':''}} value="{{isset($datoscovid) && $datoscovid->PREGUNTA_8_3?$datoscovid->PREGUNTA_8_3_1:''}}" class="form-control cerrado"  type="numeric" min="0" max="999999999">
    </div>
    <div identificador="DIV8.3_TRUE" style="{{isset($datoscovid) && $datoscovid->PREGUNTA_8_3?'':'display:none'}}" class="form-group col-md-2 DIV8.3">
      <label>Plazo (meses)</label>
      <input  id="slcQ8_3_2" name="slcQ8_3_2" class="form-control cerrado" {{isset($datoscovid) && $datoscovid->PREGUNTA_8_3?'required':''}} class="form-control"  type="numeric" min="0" value="{{isset($datoscovid) && $datoscovid->PREGUNTA_8_3?$datoscovid->PREGUNTA_8_3_2:''}}" max="999">
    </div>
  </div>
  <div style="height:40px;"></div>


  {{--  Pregunta9 --}}
  
  <div class="form-row row">
    <div class="form-group col-md-8">
      <label>9. ¿Qué porcentaje o monto de sus cuentas por cobrar han sido realizadas desde que inicio el COVID19?</label>
    </div>
    <div class="form-group col-md-2">
    </div>
    <div class="form-group col-md-1">
    </div>
    <div class="form-group col-md-1">
  </div>
  </div>
  <div class="form-row row">
    <div class="form-group col-md-8">
      <p style="color:#FF0000;margin-left:2%;font-size:12px">*Solo para clientes con una gestión financiera consolidada y profesional.</p>
    </div>
    <div class="form-group col-md-2">
    </div>
    <div class="form-group col-md-1">
     <label style="font-weight: 100;">Porc %</label>
     <input id="slcQ9_1" name="slcQ9_1" type="number" class="form-control cerrado" required value="{{isset($datoscovid)?$datoscovid->PREGUNTA_9_1:''}}" min="0" max="100">
    </div>
    <div class="form-group col-md-1">
     <label style="font-weight: 100;">Miles US$</label>
     <input id="slcQ9_2" name="slcQ9_2" type="number" class="form-control cerrado" required value="{{isset($datoscovid)?$datoscovid->PREGUNTA_9_2:''}}" min="0" max="999999999">
    </div>
  </div>
  <div style="height:40px;"></div>

  {{--  Pregunta10 --}}

  <div class="form-row row">
    <div class="form-group col-md-6">
      <label>10. ¿Cúanto es el monto o porcentaje de cuentas por cobrar en morosidad a la fecha?</label>
    </div>
    <div class="form-group col-md-1">
    </div>
    <div class="form-group col-md-2">
       <label style="font-weight: 100;">Porc %</label>
       <input value="{{isset($datoscovid)?$datoscovid->PREGUNTA_10_1:''}}" id="slcQ10_1"  name="slcQ10_1" type="number" class="form-control cerrado" min="0" max="100" required>
    </div>
    <div class="form-group col-md-1">
    </div>
    <div class="form-group col-md-2">
       <label style="font-weight: 100;">Miles US$</label>
       <input value="{{isset($datoscovid)?$datoscovid->PREGUNTA_10_2:''}}" id="slcQ10_2" name="slcQ10_2" type="number" class="form-control cerrado" min="0" max="999999999" required>
    </div>
  </div>
  <div style="height:40px;"></div>

  {{--  Pregunta11 --}}
  <div class="form-row row">
    <div class="form-group col-md-6">
      <label>11. ¿Cuenta con algún crédito Reactiva en el Sistema financiero (excepto IBK)?</label>
    </div>
    <div class="form-group col-md-2">
    </div>
    <div class="form-group col-md-4">
      <select id="slcQ11" class="form-control custom-select is-invalid cerrado" name="slcQ11" required>
        <option {{isset($datoscovid) && !empty($datoscovid->PREGUNTA_11)?'' : 'selected'}} disabled value="">-Seleccionar-</option>
        <option value="1" {{isset($datoscovid) && $datoscovid->PREGUNTA_11=='1'?'selected' : ''}}>Sí</option>
        <option value="2" {{isset($datoscovid) && $datoscovid->PREGUNTA_11=='2'?'selected' : ''}}>No</option>
      </select>
    </div>
  </div>
  <div identificador="DIV11_1" style="{{isset($datoscovid) && $datoscovid->PREGUNTA_11=='1'?'':'display:none'}}"  class="form-row row DIV11">
    <div class="form-group col-md-2">
     <input type="checkbox" class="custom-control-input cerrado" id="slcQ11_1_1" name="slcQ11_1_1"  {{isset($datoscovid) && $datoscovid->PREGUNTA_11_1_1?'checked':''}}>
     <label class="form-check-label">BCP</label>
    </div>
    <div identificador="DIV11.1.1_TRUE" style="{{isset($datoscovid) && $datoscovid->PREGUNTA_11_1_1?'':'display:none'}}" class="DIV11.1.1 form-group col-md-4">
      <label style="font-weight: 100;">Miles US$</label>
      <input id="slcQ11_1_2" name="slcQ11_1_2" type="number" class="form-control cerrado" min="0" value="{{isset($datoscovid)?$datoscovid->PREGUNTA_11_1_2:''}}" max="999999999">
    </div>
  </div>
  <div identificador="DIV11_1" style="{{isset($datoscovid) && $datoscovid->PREGUNTA_11=='1'?'':'display:none'}}"   class="form-row row DIV11">
    <div class="form-group col-md-2">
      <input type="checkbox" class="custom-control-input cerrado" id="slcQ11_2_1" name="slcQ11_2_1"  {{isset($datoscovid) && $datoscovid->PREGUNTA_11_2_1?'checked':''}}>
      <label class="form-check-label">BBVA</label>
    </div>
    <div identificador="DIV11.1.2_TRUE" style="{{isset($datoscovid) && $datoscovid->PREGUNTA_11_2_1?'':'display:none'}}"  class="DIV11.1.2 form-group col-md-4">
      <label style="font-weight: 100;">Miles US$</label>
      <input id="slcQ11_2_2" name="slcQ11_2_2" type="number" class="form-control cerrado"  value="{{isset($datoscovid)?$datoscovid->PREGUNTA_11_2_2:''}}"  min="0" max="999999999">
    </div>
  </div>
  <div  identificador="DIV11_1" style="{{isset($datoscovid) && $datoscovid->PREGUNTA_11=='1'?'':'display:none'}}"   class="form-row row DIV11">
    <div class="form-group col-md-2">
      <input type="checkbox" class="custom-control-input cerrado" id="slcQ11_3_1" name="slcQ11_3_1"  {{isset($datoscovid) && $datoscovid->PREGUNTA_11_3_1?'checked':''}}>
      <label>SCOTIA</label>
    </div>
    <div identificador="DIV11.1.3_TRUE" style="{{isset($datoscovid) && $datoscovid->PREGUNTA_11_3_1?'':'display:none'}}"  class="DIV11.1.3 form-group col-md-4">
      <label style="font-weight: 100;">Miles US$</label>
      <input id="slcQ11_3_2" name="slcQ11_3_2" type="number" class="form-control cerrado" value="{{isset($datoscovid)?$datoscovid->PREGUNTA_11_3_2:''}}" min="0" max="999999999">
    </div>
  </div>
  <div  identificador="DIV11_1" style="{{isset($datoscovid) && $datoscovid->PREGUNTA_11=='1'?'':'display:none'}}"   class="form-row row DIV11">
    <div class="form-group col-md-2">
      <input type="checkbox" class="custom-control-input cerrado" id="slcQ11_4_1" name="slcQ11_4_1"  {{isset($datoscovid) && $datoscovid->PREGUNTA_11_4_1?'checked':''}}>
      <label>Banco Pichincha</label>
    </div>
    <div identificador="DIV11.1.4_TRUE" style="{{isset($datoscovid) && $datoscovid->PREGUNTA_11_4_1?'':'display:none'}}"  class="DIV11.1.4 form-group col-md-4">
      <label style="font-weight: 100;">Miles US$</label>
      <input id="slcQ11_4_2" name="slcQ11_4_2" type="number" class="form-control cerrado" value="{{isset($datoscovid)?$datoscovid->PREGUNTA_11_4_2:''}}" min="0" max="999999999">
    </div>
  </div>
  <div identificador="DIV11_1" style="{{isset($datoscovid) && $datoscovid->PREGUNTA_11=='1'?'':'display:none'}}"   class="form-row row DIV11">
    <div class="form-group col-md-2">
      <input type="checkbox" class="custom-control-input cerrado" id="slcQ11_5_1" name="slcQ11_5_1"  {{isset($datoscovid) && $datoscovid->PREGUNTA_11_5_1?'checked':''}}>
      <label>BanBif</label>
    </div>
    <div identificador="DIV11.1.5_TRUE" style="{{isset($datoscovid) && $datoscovid->PREGUNTA_11_5_1?'':'display:none'}}" class="DIV11.1.5 form-group col-md-4">
      <label style="font-weight: 100;">Miles US$</label>
      <input id="slcQ11_5_2" name="slcQ11_5_2" type="number" class="form-control cerrado" value="{{isset($datoscovid)?$datoscovid->PREGUNTA_11_5_2:''}}" min="0" max="999999999">
    </div>
  </div>
  <div  identificador="DIV11_1" style="{{isset($datoscovid) && $datoscovid->PREGUNTA_11=='1'?'':'display:none'}}"   class="form-row row DIV11">
    <div class="form-group col-md-2">
      <input type="checkbox" class="custom-control-input cerrado" id="slcQ11_6_1" name="slcQ11_6_1"  {{isset($datoscovid) && $datoscovid->PREGUNTA_11_6_1?'checked':''}}>
      <label>Otros</label>
    </div>
    <div identificador="DIV11.1.6_TRUE" style="{{isset($datoscovid) && $datoscovid->PREGUNTA_11_6_1?'':'display:none'}}" class="DIV11.1.6 form-group col-md-4">
      <label style="font-weight: 100;">Miles US$</label>
      <input id="slcQ11_6_2" name="slcQ11_6_2" type="number" value="{{isset($datoscovid)?$datoscovid->PREGUNTA_11_6_2:''}}"   class="form-control cerrado" min="0" max="999999999">
    </div>
  </div>
  <div style="height:40px;"></div>

  {{--  Pregunta12 --}}
  <div class="form-row row">
    <div class="form-group col-md-8">
      <label>12.  Indicar si en el 2020, ha realizado inversiones o se encuentra relizando inversiones.</label>
    </div>
    <div class="form-group col-md-2">
    </div>
    <div class="form-group col-md-2">
       <select id="slcQ12" name="slcQ12" class="form-control custom-select is-invalid cerrado" onchange="" required>
        <option {{isset($datoscovid) && !empty($datoscovid->PREGUNTA_12)?'' : 'selected'}} disabled value="">-Seleccionar-</option>
        <option value="1" {{isset($datoscovid) && $datoscovid->PREGUNTA_12=='1'?'selected' : ''}}>Sí</option>
        <option value="2" {{isset($datoscovid) && $datoscovid->PREGUNTA_12=='2'?'selected' : ''}}>No</option>
      </select>
    </div>
  </div>

     {{--  Pregunta12-1 --}}
  <div identificador="DIV12_1" style="{{isset($datoscovid) && $datoscovid->PREGUNTA_12=='1'?'':'display:none'}}" id="divRpta12-1" class="DIV12 form-row row">
    <div class="form-group col-md-8">
     <label style="font-weight: normal">¿Cuál es  el monto de la inversión?</label>
    </div>
    <div class="form-group col-md-2">
    </div>
    <div class="form-group col-md-2">
        <label style="font-weight: 100;">Miles US$</label><input id="slcQ12_1" name="slcQ12_1" type="number" class="form-control cerrado" onchange="" value="{{isset($datoscovid)?$datoscovid->PREGUNTA_12_1:''}}" min="0" max="999999999">
    </div>
  </div>

    {{--  Pregunta12-2 --}}
  <div identificador="DIV12_1" style="{{isset($datoscovid) && $datoscovid->PREGUNTA_12=='1'?'':'display:none'}}" id="divRpta12-2" class="DIV12 form-row row">
    <div class="form-group col-md-8">
     <label style="font-weight: normal">¿Cómo lo financió?</label>
    </div>
    <div class="form-group col-md-2">
    </div>
    <div class="form-group col-md-2">
      <select onchange="show('DIV12.2',this)" id="slcQ12_2" name="slcQ12_2" class="form-control custom-select is-invalid cerrado" onchange="">
        <option>-Seleccionar-</option>
        <option value="Banco" {{isset($datoscovid) && $datoscovid->PREGUNTA_12_2=='Banco'?'selected' : ''}}>Banco</option>
        <option value="Recursos Propios" {{isset($datoscovid) && $datoscovid->PREGUNTA_12_2=='Recursos Propios'?'selected' : ''}}>Recursos Propios</option>
        <option value="Accionistas" {{isset($datoscovid) && $datoscovid->PREGUNTA_12_2=='Accionistas'?'selected' : ''}}>Accionistas</option>
      </select>
    </div>
    {{--  Pregunta12-2-1 --}}
  <div identificador="DIV12.2_Banco" style="{{isset($datoscovid) && $datoscovid->PREGUNTA_12_2=='Banco'?'':'display:none'}}"  id="divRpta12-2-1" class="form-row row DIV12.2">
    <div class="form-group col-md-5">
    </div>
    <div class="form-group col-md-3">
     <label>¿Qué porcentaje financío?</label>
    </div>
    <div class="form-group col-md-2">
    </div>
    <div class="form-group col-md-1">
      <label style="margin-top: 5px;font-weight: 100;">Porc %</label>
    </div>
    <div class="form-group col-md-1">
        <input  id="slcQ12_2_1" name="slcQ12_2_1" type="number" class="form-control cerrado" value="{{isset($datoscovid)?$datoscovid->PREGUNTA_12_2_1:''}}" min="0" max="100">
    </div>
  </div>
  
  {{--  Pregunta12-2-2 --}}
  <div  identificador="DIV12.2_Banco" style="{{isset($datoscovid) && $datoscovid->PREGUNTA_12_2=='Banco'?'':'display:none'}}" id="divRpta12-2-2" class="form-row row DIV12.2">
    <div class="form-group col-md-1">
    </div>
    <div class="form-group col-md-7">
     <label style="margin-left: 60%;">Plazo de Financiamiento:</label>
    </div>
    <div class="form-group col-md-1">
    </div>
    <div class="form-group col-md-2">
          <select id="slcQ12_2_2_1" name="slcQ12_2_2_1" class="form-control custom-select is-invalid cerrado">
              <option {{isset($datoscovid) && !empty($datoscovid->PREGUNTA_12_2_2_1)?'' : 'selected'}} disabled value="">-Seleccionar-</option>
              <option value="días" {{isset($datoscovid) && $datoscovid->PREGUNTA_12_2_2_1=='días'?'selected' : ''}}>Días</option>
              <option value="meses" {{isset($datoscovid) && $datoscovid->PREGUNTA_12_2_2_1=='meses'?'selected' : ''}}>Meses</option>
          </select>
    </div>
    <div class="form-group col-md-1">
        <input id="slcQ12_2_2_2" name="slcQ12_2_2_2" type="number" class="form-control cerrado" value="{{isset($datoscovid)?$datoscovid->PREGUNTA_12_2_2_2:''}}" min="0" max="999">
    </div>
  </div>
  </div>
 

  <div style="height:40px;"></div>

  {{--  Pregunta13 --}}

                <div class="form-row row">
                    {{--  Check4  --}}
                    <div class="form-group col-md-3" style="padding-left: 3%;">
                    <input class="form-check-input cerrado slcQ13" type="checkbox" value="bajo" id="slcQ13_0" name="slcQ13_0"  {{isset($datoscovid) && $datoscovid->PREGUNTA_13 =='sin impacto' ?'checked':''}}>
                    <label class="form-check-label"> Sin impacto 
                    <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" 
                    title=""></i>
                    </label>
                    </div>

                    {{--  Check5  --}}
                    <div class="form-group col-md-3" style="padding-left: 3%;">
                    <input class="form-check-input cerrado slcQ13" type="checkbox" value="bajo" id="slcQ13_1" name="slcQ13_1"  {{isset($datoscovid) && $datoscovid->PREGUNTA_13 == 'bajo' ?'checked':''}}>
                    <label class="form-check-label"> Bajo 
                    <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" 
                    title="Aquellos empresas que a pesar de la cuarentena han seguido operando pero con un menor volumen de ingresos."></i>
                    </label>
                    </div>

                    {{--  Check6  --}}
                    <div class="form-group col-md-3" style="padding-left: 3%;">
                    <input type="checkbox" class="form-check-input cerrado slcQ13"  value="medio" id="slcQ13_2" name="slcQ13_2"  {{isset($datoscovid) && $datoscovid->PREGUNTA_13 == 'medio' ?'checked':''}} >
                    <label class="form-check-label"> Medio 
                        <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" 
                        title="Aquellas empresas que no están operando dentro de la cuarentena, sin embargo su salud financiera le ha permitido cubrir obligaciones corrientes o aquellas que siguen operando pero sus ingresos han sido afectados fuertemente."></i>
                    </label>
                    </div>

                    {{--  Check7  --}}
                    <div class="form-group col-md-3" style="padding-left: 3%;">
                    <input type="checkbox" class="form-check-input cerrado slcQ13"  value="alto" id="slcQ13_3" name="slcQ13_3"  {{isset($datoscovid) && $datoscovid->PREGUNTA_13 == 'alto' ?'checked':''}} >
                    <label class="form-check-label"> Alto 
                    <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" 
                        title="Aquellas empresas que no están operando dentro de la cuarentena y han tenido que recordar personal y/o tomar préstamos para poder cubrir las obligaciones corrientes."></i>
                    </label>
                    </div>
                </div>

  <div style="height:40px;"></div>
  {{--  Pregunta14 --}}
  <div class="form-row row">
    <div class="form-group col-md-12"><label>14. Estrategia</label></div>
  </div>
  <div class="form-row row">
                    <div class="form-group col-md-1">Plan Propuesto</div>
                    <div class="form-group col-md-3">
                    <select name="slcQ14_1" id="slcQ14_1" class="form-control custom-select is-invalid cerrado">
                        <option value="">-Seleccionar-</option>
                        <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_1=='Crecer'?'selected' : ''}} value="Crecer">Crecer</option>
                        <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_1=='Mantener'?'selected' : ''}} value="Mantener">Mantener</option>
                        <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_1=='Reprogramar '?'selected' : ''}} value="Reprogramar ">Reprogramar</option>
                        <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_1=='Feve'?'selected' : ''}} value="Feve">Feve</option>
                        <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_1=='Refinanciar'?'selected' : ''}} value="Refinanciar">Refinanciar</option>
                    </select>
                    </div>
                    <div id="crecerWrapper" style="{{isset($datoscovid) && $datoscovid->PREGUNTA_14_1=='Crecer'?'' :'display:none'}}">
                        <div class="form-group col-md-1">Monto</div>
                        <div class="form-group col-md-3">
                            <input {{isset($datoscovid) && $datoscovid->PREGUNTA_14_1=='Crecer'?'required' :''}} type="number" class="form-control custom-select is-invalid cerrado" min="0" max="999999" step="0.01" value="{{isset($datoscovid) && $datoscovid->PREGUNTA_14_1=='Crecer'?$datoscovid->MONTO_CRECER :''}}" name="montoCrecer" id="montoCrecer">
                        </div>
                        <div class="form-group col-md-1">Detalle</div>
                        <div class="form-group col-md-3">
                            <textarea {{isset($datoscovid) && $datoscovid->PREGUNTA_14_1=='Crecer'?'required' :''}}  type="text" class="form-control custom-select is-invalid cerrado" maxLength="30" name="detalleCrecer" id="detalleCrecer">{{trim(isset($datoscovid) && $datoscovid->PREGUNTA_14_1=='Crecer'?$datoscovid->DETALLE_CRECER :'')}}</textarea>
                        </div>
                        </div>
                    <div class="form-group col-md-1">FEVE</div>
                    <div class="form-group col-md-3">
                    <select name="slcQ14_2" id="slcQ14_2" class="form-control custom-select is-invalid cerrado">
                        <option value="">-Seleccionar-</option>
                        <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_2=='Sin FEVE'?'selected' : ''}} value="Sin FEVE">Sin FEVE</option>
                        <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_2=='Seguir COVID'?'selected' : ''}} value="Seguir COVID">Seguir COVID</option>
                        <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_2=='Seguir'?'selected' : ''}} value="Seguir">Seguir</option>
                        <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_2=='Garantizar'?'selected' : ''}} value="Garantizar">Garantizar</option>
                        <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_2=='Reducir'?'selected' : ''}} value="Reducir">Reducir</option>
                        <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_2=='Salir'?'selected' : ''}} value="Salir">Salir</option>
                    </select>
                    </div>
                    <div class="form-group col-md-1">Clasificación</div>
                    <div class="form-group col-md-3">
                    <select name="slcQ14_3" id="slcQ14_3" class="form-control custom-select is-invalid cerrado">
                        <option value="">-Seleccionar-</option>
                        <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_3=='Revisar'?'selected' : ''}} value="Revisar">Revisar</option>
                        <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_3=='No Revisar'?'selected' : ''}} value="No Revisar">No Revisar</option>
                    </select>
                    </div>
                </div>
                <br>
  @if($privilegios ==App\Model\Infinity\Visita::ROL_VALIDADOR && $segmento == 'MEDIANA EMPRESA')
  <div class="form-row row">
    <div class="form-group col-md-8">¿Desea aplicar blindaje de revisión con su rol revisor?</div>
      <div class="form-group col-md-4">
        <select name="slcQ14_4" id="slcQ14_4" class="form-control custom-select is-invalid cerrado">
          <option value="">-Seleccionar-</option>
          <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_4=='1'?'selected' : ''}} value="1">Si</option>
          <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_4=='2'?'selected' : ''}} value="2">No</option>
        </select>
      </div>
    </div>
    <div style="{{isset($datoscovid) && $datoscovid->PREGUNTA_14_4=='1'?'' : 'display:none'}}" identificador="DIV14.4_1" class="form-row row DIV14.4">
      <div class="form-group col-md-8">¿Por cuantos meses desea aplicar el blindaje?</div>
      <div class="form-group col-md-4">
        <select {{isset($datoscovid) && $datoscovid->PREGUNTA_14_4=='1'?'required' : 'disabled'}} name="slcQ14_5" id="slcQ14_5" class="form-control custom-select is-invalid cerrado">
          <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_5=='1'?'selected' : ''}} value="1">1</option>
          <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_5=='2'?'selected' : ''}} value="2">2</option>
          <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_5=='3'?'selected' : ''}} value="3">3</option>
        </select>
      </div>
    </div>
  @endif
  <div style="height:40px;"></div>

  <table id="cumplimientos" style="width: 100%;">
    <tr>
      <th style="text-align: center;">Área de<br> Eliminación </th>
      <th style="text-align: center;">Tipo de <br> compromiso</th>
      <th style="text-align: center;">Detalle de<br> compromiso</th>
      <th style="text-align: center;">Descripcion</th>
      <th style="text-align: center;">Indicador</th>
      <th style="text-align: center;">Fecha Fin de<br>compromiso</th>
      <th style="text-align: center;">Gestor</th>
      <th style="text-align: center;">Status</th>
    </tr>
      @if(count($cumplimientos)>0)
        @foreach(range(0,count($cumplimientos)-1) as $i)
        <input name="ID_CUMPL" id="ID_CUMPL" type="text" style="width:0; visibility:hidden" value="{{$cumplimientos[$i]->ID}}">
        <tr>
          <td>
            <select name="{{'TIPO_GESTION'.$i}}" id="{{'TIPO_GESTION'.$i}}" class="form-control custom-select is-invalid cerrado" required>
              <option value ="">-Seleccionar-</option>
              <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_GESTION=='Comercial'?'selected' : ''}} value="Gestión comercial">Gestión comercial</option>
              <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_GESTION=='Seguimiento comercial'?'selected' : ''}} value="Gestión de seguimiento comercial">Gestión de seguimiento comercial</option>
              <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_GESTION=='GYS'?'selected' : ''}} value="GYS">GYS</option>
              <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_GESTION=='Admisión'?'selected' : ''}} value="Gestión de Admisión">Gestión de Admisión</option>
            </select>
          </td>
          <td>
            <select name="{{'TIPO_COMPROMISO'.$i}}" id="{{'TIPO_COMPROMISO'.$i}}" class="form-control custom-select is-invalid cerrado" required>
              <option value ="">-Seleccionar-</option>
              <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO=='Comerciales'?'selected' : ''}} value="Comerciales">Comerciales</option>
              <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO=='Canalización de flujos IN'?'selected' : ''}}  value="Canalización de flujos IN">Canalización de flujos IN</option>
              <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO=='Incremento de flujos OUT'?'selected' : ''}}  value="Incremento de flujos OUT">Incremento de flujos OUT</option>
              <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO=='Garantías'?'selected' : ''}} value="Garantías">Garantías</option>
              <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO=='Estados Financieros'?'selected' : ''}} value="Estados Financieros">Estados Financieros</option>
              <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO=='Otros'?'selected' : ''}} value="Otros">Otros</option>
            </select>
          </td>
          <td>
          <div style="{{isset($cumplimientos[$i]) 
            && $cumplimientos[$i]->TIPO_COMPROMISO=='Comerciales'?'':'display:none'}}" identificador="{{'DIVCUMPLIMIENTO'.$i.'_Comerciales'}}"  class=" {{'DIVCUMPLIMIENTO'.$i}}">
            <select name="{{'TIPO_COMPROMISO_DETALLE_Comerciales'.$i}}" id="{{'TIPO_COMPROMISO_DETALLE_Comerciales'.$i}}" class="form-control custom-select is-invalid cerrado" required>
              <option value ="">-Seleccionar-</option>
              <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Compra de un activo/inversión'?'selected' : ''}} value="Compra de un activo/inversión">Compra de un activo/inversión</option>
              <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Tipo de cambio'?'selected' : ''}} value="Tipo de cambio">Tipo de cambio</option>
              <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Incremento la utilización de líneas'?'selected' : ''}} value="Incremento la utilización de líneas">Incremento la utilización de líneas</option>
              <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Incrementar nivel de exposición PDM'?'selected' : ''}} value="Incrementar nivel de exposición PDM">Incrementar nivel de exposición PDM</option>
              <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Covenants'?'selected' : ''}} value="Covenants">Covenants</option>
              <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Digitalización de productos'?'selected' : ''}} value="Digitalización de productos">Digitalización de productos</option>
              <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Tarifas especiales'?'selected' : ''}} value="Tarifas especiales">Tarifas especiales</option>
              <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Entrega de documentos - renovación de líneas'?'selected' : ''}} value="Entrega de documentos - renovación de líneas">Entrega de documentos - renovación de líneas</option>
            </select>
          </div>
            <div style="{{isset($cumplimientos[$i]) 
              && $cumplimientos[$i]->TIPO_COMPROMISO=='Canalización de flujos IN'?'':'display:none'}}" identificador="{{'DIVCUMPLIMIENTO'.$i.'_Canalización de flujos IN'}}"  class=" {{'DIVCUMPLIMIENTO'.$i}}">
              <select name="{{'TIPO_COMPROMISO_DETALLE_Canalización_de_flujos_IN'.$i}}" id="{{'TIPO_COMPROMISO_DETALLE_Canalización_de_flujos_IN'.$i}}"  class="form-control custom-select is-invalid cerrado" required>
                <option value ="">-Seleccionar-</option>
                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Proveedores'?'selected' : ''}} value="Proveedores">Proveedores</option>
                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Transferencias'?'selected' : ''}} value="Transferencias">Transferencias</option>
                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Letras/Facturas'?'selected' : ''}} value="Letras/Facturas">Letras/Facturas</option>
                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Recaudación'?'selected' : ''}} value="Recaudación">Recaudación</option>
                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Otros'?'selected' : ''}} value="Otros">Otros</option>
              </select>
            </div>
            <div style="{{isset($cumplimientos[$i]) 
              && $cumplimientos[$i]->TIPO_COMPROMISO=='Incremento de flujos OUT'?'':'display:none'}}" identificador="{{'DIVCUMPLIMIENTO'.$i.'_Incremento de flujos OUT'}}"  class=" {{'DIVCUMPLIMIENTO'.$i}}">
              <select name="{{'TIPO_COMPROMISO_DETALLE_Canalización_de_flujos_OUT'.$i}}" id="{{'TIPO_COMPROMISO_DETALLE_Canalización_de_flujos_OUT'.$i}}"  class="form-control custom-select is-invalid cerrado" required>
                <option value ="">-Seleccionar-</option>
                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Transferencias al exterior'?'selected' : ''}} value="Transferencias al exterior">Transferencias al exterior</option>
                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Pago de proveedores'?'selected' : ''}} value="Pago de proveedores">Pago de proveedores</option>
                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Pago SUNAT'?'selected' : ''}} value="Pago SUNAT">Pago SUNAT</option>
                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Pago AFP'?'selected' : ''}} value="Pago AFP">Pago AFP</option>
                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Proveedores'?'selected' : ''}} value="Proveedores">Proveedores</option>
                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Planilla'?'selected' : ''}} value="Planilla">Planilla</option>
                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Pago varios'?'selected' : ''}} value="Pago varios">Pago varios</option>
              </select>
            </div>
            <div style="{{isset($cumplimientos[$i]) 
              && $cumplimientos[$i]->TIPO_COMPROMISO=='Garantías'?'':'display:none'}}" identificador="{{'DIVCUMPLIMIENTO'.$i.'_Garantías'}}"  class=" {{'DIVCUMPLIMIENTO'.$i}}">
              <select name="{{'TIPO_COMPROMISO_DETALLE_Garantías'.$i}}" id="{{'TIPO_COMPROMISO_DETALLE_Garantías'.$i}}"  class="form-control custom-select is-invalid cerrado" required>
                <option value ="">-Seleccionar-</option>
                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Nueva garantía bien mueble/ inmueble'?'selected' : ''}} value="Nueva garantía bien mueble/ inmueble">Nueva garantía bien mueble/ inmueble</option>
                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Garantía liquida'?'selected' : ''}} value="Garantía liquida">Garantía liquida</option>
                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Incremento del valor de gravamen'?'selected' : ''}} value="Incremento del valor de gravamen">Incremento del valor de gravamen</option>
                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Actualizar tasación'?'selected' : ''}} value="Actualizar tasación">Actualizar tasación</option>
                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Cesión de flujos'?'selected' : ''}} value="Cesión de flujos">Cesión de flujos</option>
                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Fideicomisos'?'selected' : ''}} value="Fideicomisos">Fideicomisos</option>
                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='FEVE'?'selected' : ''}} value="FEVE">FEVE</option>
              </select>
            </div>
            
            <div style="{{isset($cumplimientos[$i]) 
              && $cumplimientos[$i]->TIPO_COMPROMISO=='Estados Financieros'?'':'display:none'}}" identificador="{{'DIVCUMPLIMIENTO'.$i.'_Estados Financieros'}}" class="{{'DIVCUMPLIMIENTO'.$i}}">
              <select name="{{'TIPO_COMPROMISO_DETALLE_Estados_Financieros'.$i}}" id="{{'TIPO_COMPROMISO_DETALLE_Estados_Financieros'.$i}}"  class="form-control custom-select is-invalid cerrado" required>
                <option value ="">-Seleccionar-</option>
                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Actividad'?'selected' : ''}} value="Actividad">Actividad</option>
                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Liquidez'?'selected' : ''}} value="Liquidez">Liquidez</option>
                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Solvencia'?'selected' : ''}} value="Solvencia">Solvencia</option>
                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Cobertura del servicio de deuda'?'selected' : ''}} value="Cobertura del servicio de deuda">Cobertura del servicio de deuda</option>
                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Endeudamiento'?'selected' : ''}} value="Endeudamiento">Endeudamiento</option>
                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Rentabilidad'?'selected' : ''}} value="Rentabilidad">Rentabilidad</option>
              </select>
            </div>
            <div style="{{isset($cumplimientos[$i]) 
              && $cumplimientos[$i]->TIPO_COMPROMISO=='Otros'?'':'display:none'}}" identificador="{{'DIVCUMPLIMIENTO'.$i.'_Otros'}}" class="{{'DIVCUMPLIMIENTO'.$i}}">
              <select name="{{'TIPO_COMPROMISO_DETALLE_Otros'.$i}}" id="{{'TIPO_COMPROMISO_DETALLE_Otros'.$i}}"  class="form-control custom-select is-invalid cerrado" required>
                <option {{!isset($cumplimientos[$i])||$cumplimientos[$i]->TIPO_COMPROMISO_DETALLE==null?'selected':''}}>-Seleccionar-</option>
                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Sustento de deuda en otros bancos'?'selected' : ''}} value="Sustento de deuda en otros bancos">Sustento de deuda en otros bancos</option>
                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Incremento del Capital social'?'selected' : ''}} value="Incremento del Capital social">Incremento del Capital social</option>
              </select>
            </div>
          </td>
          <td>
            <input maxlength="200"  value="{{isset($cumplimientos[$i])? $cumplimientos[$i]->DETALLE_COMPRA:''}}"  name="{{'DETALLE_COMPRA'.$i}}" id="{{'DETALLE_COMPRA'.$i}}"  type="text" class="form-control cerrado" required>
          </td>
          <td>
            <input value="{{isset($cumplimientos[$i])? $cumplimientos[$i]->INDICADOR:''}}" name="{{'INDICADOR'.$i}}" id="{{'INDICADOR'.$i}}"  oninput="maxLengthMontoCheck(this)" max="999999999"  type="number" class="form-control cerrado"  min="0" required>
          </td>

          <td>
            <input autocomplete="off" class="form-control fechaCompromiso cerrado"  type="text" name="{{'FECHA_COMPROMISO'.$i}}" placeholder="Seleccionar fecha" value="{{isset($cumplimientos[$i])? $cumplimientos[$i]->FECHA_COMPROMISO:''}}" required> 
          </td>
          <td>
          <select name="{{'GESTOR'.$i}}" id="{{'GESTOR'.$i}}"  class="form-control custom-select is-invalid cerrado" required>
              <option value ="">-Seleccionar-</option>
              <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->GESTOR=='EJECUTIVO DE NEGOCIO'?'selected' : ''}} value="EJECUTIVO DE NEGOCIO">EJECUTIVO DE NEGOCIO</option>
              <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->GESTOR=='JEFE ZONAL'?'selected' : ''}} value="JEFE ZONAL">JEFE ZONAL</option>
              <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->GESTOR=='EQUIPO SEGUIMIENTO'?'selected' : ''}} value="EQUIPO SEGUIMIENTO">EQUIPO SEGUIMIENTO</option>
            </select>
          </td>
          <td>
            <select fecha="{{isset($cumplimientos[$i])? $cumplimientos[$i]->FECHA_COMPROMISO:''}}" name="{{'STATUS_COMPROMISO'.$i}}" id="{{'STATUS_COMPROMISO'.$i}}"  class="status form-control custom-select is-invalid" {{isset($cumplimientos[$i])? ($now->diffInDays(\Carbon\Carbon::parse($cumplimientos[$i]->FECHA_COMPROMISO)))>0?'':'disabled':''}}>
              <option value ="">-Seleccionar-</option>
              <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->STATUS_COMPROMISO=='CUMPLIO'?'selected' : ''}} value="CUMPLIO">CUMPLIÓ</option>
              <option {{(isset($cumplimientos[$i]) && $cumplimientos[$i]->STATUS_COMPROMISO=='NO CUMPLIO')||isset($cumplimientos[$i]) && $cumplimientos[$i]->FECHA_COMPROMISO<$now?'selected' : ''}} value="NO CUMPLIO">NO CUMPLIÓ</option>
            </select>
          </td>
          <td style="padding-left: 10px;font-size: 20px;">
          </td>
        </tr>
        @endforeach
      @endif
  </table>

  <div style="height:40px;"></div>

  {{--  Footer  --}}

<div class="form-row row">
  <div class="form-group col-md-4">
  <label>EJECUTIVO DE NEGOCIO:</label>
  </div>
   <div class="form-group col-md-1">
        
  </div>
   <div class="form-group col-md-7">
        <label>{{$cliente->EJECUTIVO}}</label>
  </div>
</div>

<div class="form-row row">
  <div class="form-group col-md-4">
        <label>ROL VALIDADOR:</label>
  </div>
   <div class="form-group col-md-1">
        
  </div>
   <div class="form-group col-md-7">
        <label>{{$rolvalidador}}</label>
  </div>
</div>

<!-- <div class="form-row row">
  <div class="form-group col-md-4">
        <label>FECHA VISITA:</label>
  </div>
   <div class="form-group col-md-1">
        
  </div>
   <div class="form-group col-md-2">
       <input autocomplete="off" class="form-control dfecha cerrado"  type="text" name="fechaVisita" placeholder="Seleccionar fecha" value="{{isset($datoscovid)?$datoscovid->FECHA_VISITA:null}}" required> 
  </div>
     <div class="form-group col-md-5">
  </div>
</div> -->
<div class="form-row row" style="visibility:hidden;">
  <input id="cumplimientosCounter" name="cumplimientosCounter" value="{{count($cumplimientos)}}"  type="text">
</div>
</form>
@stop

@section('js-scripts')
<script>

$(document).ready(function () 
{
  $('select').attr('disabled',true);
  $('input').attr('disabled',true);
  $('textarea').attr('disabled',true);
});

</script>
@stop