
<html>
<head>
<meta charset="utf-8">

    <title> SEGUIMIENTO DIARIO</title>
</head>
<style type="text/css">
	
	body {
    	font-family: 'omnes-regular-webfont';
		font-size: 8;
	}
	.table{
	}
	.tbl {background-color:#000;border-color: red;}
	.tbl td,th,caption{background-color:#fff}
</style>
<body>
  <div>Fecha de impresion : {{$date}}</div>
  <br>
  <br>
  <!-- <div>{{$datos}}</div> -->
@if($semanas)
@foreach($semanas as $semana)
	<div>Semana : {{$semana->SEMANA}}</div>
	<table  class="tbl">
	<thead>
		<tr>
		<th style="vertical-align: middle; text-align: center;background:green;color: white;width:70px">FECHA</th>
		<th style="vertical-align: middle; text-align: center;background:green;color: white;width:70px">CU</th>
		<th style="vertical-align: middle; text-align: center;background:green;color: white;width:150px">CLIENTE</th>
		<th style="vertical-align: middle; text-align: center;background:green;color: white;width:220px">ACUERDO</th>
		<th style="vertical-align: middle; text-align: center;background:green;color: white;width:60px">FECHA COMPROMISO</th>
		<th style="vertical-align: middle; text-align: center;background:green;color: white;width:150px">RESPONSABLE</th>							            								       
		</tr>
	</thead>
	<tbody>
		@if($datos)
		@foreach($datos as $elementotabla)
			@if($elementotabla->SEMANA == $semana->SEMANA)
			<tr>
				<td style="vertical-align: middle; text-align: center;">{{$elementotabla->FECHA_REGISTRO}}</td>
				<td style="vertical-align: middle; text-align: center;">{{$elementotabla->CU}}</td>
				<td style="vertical-align: middle; text-align: center;">{{$elementotabla->CLIENTE}}</td>
				<td style="vertical-align: middle; text-align: center;">{{$elementotabla->ACUERDO}}</td>
				<td style="vertical-align: middle; text-align: center;">{{$elementotabla->FECHA_COMPROMISO}}</td>
				<td style="vertical-align: middle; text-align: center;">{{$elementotabla->RESPONSABLE}}</td>					  						
			</tr>
			@endif
		@endforeach
		@endif
	</tbody>
</table>
@endforeach
@endif
</body>
</html>