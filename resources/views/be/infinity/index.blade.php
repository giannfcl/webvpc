
@extends('be.infinity.indexjs')

@section('vista_ac')
<div class="x_panel row" style="padding:30px; box-shadow: 0 4px 8px 3px lightgrey;">
	<div class="row clearfix">
		<div class="form-group col-md-3 {{$vista['bancas'] ? '' : 'hidden'}}">
			<label class="control-label col-md-4" style="margin-bottom: 0">Banca</label>
			<div class="col-md-8" style="margin-bottom: 0">
				<select id="Bancas" class="form-control filtroMain" name="banca" required="">
					{!! $vista['bancas_data'] !!}
				</select>
			</div>
		</div>
		<div class="form-group col-md-3 {{$vista['zonales']? '' : 'hidden'}}">
			<label class="control-label col-md-4" style="margin-bottom: 0">Zonal</label>
			<div class="col-md-8" style="margin-bottom: 0">
				<select id="Zonales" class="form-control filtroMain" name="zonales" required="">
					{!! $vista['zonales_data'] !!}
				</select>
			</div>
		</div>
		<div class="form-group col-md-3 {{$vista['jefaturas'] ? '' : 'hidden'}}">
			<label class="control-label col-md-4" style="margin-bottom: 0">Jefatura</label>
			<div class="col-md-8" style="margin-bottom: 0">
				<select id="Jefaturas" class="form-control filtroMain" name="jefaturas" required="">
					{!! $vista['jefaturas_data'] !!}
				</select>
			</div>
		</div>
		<div class="form-group col-md-3 {{$vista['ejecutivos'] ? '' : 'hidden'}}">
			<label class="control-label col-md-4" style="margin-bottom: 0">Ejecutivos</label>
			<div class="col-md-8" style="margin-bottom: 0">
				<select id="Ejecutivos" class="form-control filtroMain" name="ejecutivos" required="">
					{!! $vista['ejecutivos_data'] !!}
				</select>
			</div>
		</div>
	</div><br>
	<ul class="nav nav-tabs">
		<li class="active"><a id="resumenTab" data-toggle="tab" href="#resumen">Resumen</a></li>
		<li style="display: none;"><a data-toggle="tab" href="#laumaticas">Lineas Automáticas</a></li>
		<li><a data-toggle="tab" href="#segdiario">Seguimiento Diario</a></li>
		<li><a data-toggle="tab" href="#tablaComp">Tabla Compromisos</a></li>
	</ul>

	<div class="tab-content" style="margin-top: 20px;">
		<div id="laumaticas" class="tab-pane fade">
			<div class="row">
				<div class="col-xs-6">
					<div class="x_panel">
						<p><b>CARTERA: <span id="cartera"></span> </b></p>
						<div class="progress">
							<div class="progress-bar" id="barracartera_v" style="min-width: 5%;width: 0%;background-color: #26B99A">

							</div>
							<div class="progress-bar" id="barracartera_r" style="min-width: 5%;width: 0%;background-color: #D9534F">

							</div>
							<div class="progress-bar" id="barracartera_a" style="min-width: 5%;width: 0%;background-color: #F0AD4E">

							</div>
							<div class="progress-bar" id="barracartera_n" style="min-width: 5%;width: 0%;background-color: #000000">

							</div>
						</div>
						<p><b>CLIENTES LINEAS AUTOMATICAS: <span id="clientesla"></span></b></p>
						<div class="progress">
							<div class="progress-bar" id="barraclientesla_v" style="min-width: 5%;width: 0%;background-color: #26B99A">

							</div>
							<div class="progress-bar" id="barraclientesla_r" style="min-width: 5%;width: 0%;background-color: #D9534F">

							</div>
							<div class="progress-bar" id="barraclientesla_a" style="min-width: 5%;width: 0%;background-color: #F0AD4E">

							</div>
							<div class="progress-bar" id="barraclientesla_n" style="min-width: 5%;width: 0%;background-color: #000000">

							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-6">
					<div class="x_panel">
						<div class="row">
							<div class="col-md-6">
								<p>GESTIONES PENDIENTES</p>
								<div class="progress">
									<div class="progress-bar active" role="progressbar" id="barragespendientes" style="min-width: 5%; width: 0%">
										0%
									</div>
								</div>
							</div>

							<div class="col-md-6">
								<p>DOCUMENTACIÓN PENDIENTE</p>
								<div class="progress">
									<div class="progress-bar active" role="progressbar" id="barradocumentacion" style="min-width: 5%; width: 0%">
										0%
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<p>VISITA PENDIENTES</p>
								<div class="progress">
									<div class="progress-bar active" role="progressbar" id="barravisitaspendientes" style="min-width: 10%; width: 0%">
										0%
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<p>RECALCULO PENDIENTES</p>
								<div class="progress">
									<div class="progress-bar active" role="progressbar" id="barrarecalculospendientes" style="min-width: 10%; width: 0%">
										0%
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<p><strong>VENCIDOS IBK:</strong><span id="vencidosibk">0</span> </p>
								<p><strong>VENCIDOS RCC:</strong><span id="vencidosrcc">0</span> </p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Grilla -->
			<div class="row">
				<div class="col-xs-12">
					<div class="x_panel">
						<div class="x_content" style="overflow-x:scroll">
							<table class="table table-striped table-bordered jambo_table" id="datatable1">
								<thead>
									<tr>
										<th>LA</th>
										<th>Ejecutivo</th>
										<th>Cliente</th>
										<th>Deuda</th>
										<th>Vencido</th>
										<th>Semáforo</th>
										<th>Recálc</th>
										<th>Docs</th>
										<th>Visitas</th>
										<th style="text-align: center;">Gestión</th>
										<th style="text-align: center;">Acciones</th>
										<th style="text-align: center;">Status Comité</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

			<!-- Colores de semaforos -->
			@foreach(App\Entity\Infinity\Semaforo::SEMAFOROS_ME_COLORES as $key => $color)
			<input id="semaforo-{{$key}}" type="hidden" value="{{$color}}">
			@endforeach
			<div class="hidden">
				@foreach($iconoAlertas as $key => $icono)
				<div class="alerta-icono-{{$key}}">
					{!! $icono !!}
				</div>
				@endforeach
			</div>
			<input id="icono" type="hidden" value="{{ URL::asset('img/Logo_favi.ico') }}" />
			<input id="icono2" type="hidden" value="{{ URL::asset('img/Logo_favi_2.ico') }}" />
			<input id="logeado" type="hidden" value="{{$registro}}" />
			<div class="modal fade" tabindex="-1" role="dialog" id="modalDocumentacion_Comite">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title">Gestión de Comité</h4>
						</div>
						<div class="modal-body">
							<form id="frmGestion_comite" class="form-horizontal" enctype="multipart/form-data" method="POST" action="{{route('infinity.me.detalle.guardar.gestion')}}">
								<div>
									<label class="control-label col-md-3 col-sm-3 col-xs-12">COD UNICO:</label>
									<div class="input-group col-md-9 col-sm-9 col-xs-12">
										<input class="form-control" name="codUnico" id="codUnico" type="text" value="" readonly="readonly">
									</div>
								</div>
								<div>
									<label class="control-label col-md-3 col-sm-3 col-xs-12">RMG:</label>
									<div class="input-group col-md-9 col-sm-9 col-xs-12">
										<input class="form-control" id="rmg_modal" type="text" value="" readonly="readonly">
									</div>
								</div>
								<div>
									<label class="control-label col-md-3 col-sm-3 col-xs-12">RATING:</label>
									<div class="input-group col-md-9 col-sm-9 col-xs-12">
										<input class="form-control" id="rating_modal" type="text" value="" readonly="readonly">
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12">Estado:</label>
									<div class="input-group col-md-9 col-sm-9 col-xs-12">
										<input readonly="" class="form-control" value="Comité">
										<input name="estadoGestion" value="4" hidden="hidden">
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12">Fecha Comité:</label>
									<div class="input-group col-md-9 col-sm-9 col-xs-12">
										<div class="input-group-addon styleAddOn"><i class="glyphicon glyphicon-calendar fa fa-calendar" for="fechaGestion"></i></div>
										<input autocomplete="off" class="form-control dfecha" type="text" id="fechaGestion" name="fechaGestion" placeholder="Seleccionar fecha" value="">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12">Política:</label>
									<div class="input-group col-md-9 col-sm-9 col-xs-12">
										<div class="input-group-addon styleAddOn"><i class="fa fa-tasks" for="PoliticaAsociada"></i></div>
										<select class="form-control" name="PoliticaAsociada" id="PoliticaAsociada_Comite">
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12">Decisión:</label>
									<div class="input-group col-md-9 col-sm-9 col-xs-12">
										<div class="input-group-addon styleAddOn"><i class="fa fa-tasks" for="DecisionComite"></i></div>
										<select class="form-control" name="DecisionComite" id="DecisionComite">
											<option value="">Seleccionar Aprobacion</option>
											<option value="1">Aprobado</option>
											<option value="0">Desaprobado</option>
										</select>
									</div>
								</div>
								<div class="form-group checkboxtiempos" hidden="hidden">
									<label class="control-label col-md-3 col-sm-3 col-xs-12">Gestión de Espera:</label>
									<div class="input-group col-md-9 col-sm-9 col-xs-12">
										<div class="col-md-4">
											<input type="radio" class="checktiempo" name="tiempo" value="3"> 3 Meses
										</div>
										<div class="col-md-4">
											<input type="radio" class="checktiempo" name="tiempo" value="6"> 6 Meses
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12">Comentario:</label>
									<div class="input-group col-md-9 col-sm-9 col-xs-12">
										<textarea class="form-control" rows="10" style="resize: none" placeholder="Ingresar Comentario..." name="comentarioGestion"></textarea>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12">Adjunto:</label>
									<div class="input-group col-md-9 col-sm-9 col-xs-12">
										<input type="file" name="adjuntoGestion" id="adjuntoGestion" class="form-control image" style="border-style: none">
									</div>
								</div>
								<div class="form-group valid">
									<label id="texto_usuario_permitido" class="control-label col-md-7 col-sm-3 col-xs-12"></label>
									<label id="usuario_permitido" class="control-label col-md-7 col-sm-3 col-xs-12"></label>
									<input id="re_usu_permitido" hidden="hidden">
								</div>
								<center class="guardarGestion"><button class="btn btn-success" type="submit">Guardar</button></center>
							</form>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div><!-- /.modal -->
			</div>

		</div>
		<div id="segdiario" class="tab-pane fade" style="">

			<div id="infosegui">
				<div class="row clearfix">
					<div class="form-group col-md-4">
						<label class="control-label ">TIENE LINEA AUTOMÁTICA</label>
						<select id="es_infinity" class="form-control selectpicker filtroDinamico" name="es_infinity" multiple="multiple" multiple="multiple" title="Seleccione" data-size="4" tabindex="-1" data-selected-text-format="count>1" data-count-selected-text="{0} Seleccione ">
							<option value="1">SI</option>
							<option value="0">NO</option>
						</select>
					</div>
					<div class="form-group col-md-4">
						<label class="control-label">RANGO DE DEUDA VENCIDA</label>
						<select id="rango_deuda" class="form-control selectpicker filtroDinamico" name="rango_deuda" multiple="multiple" multiple="multiple" title="Seleccione" data-size="4" tabindex="-1" data-selected-text-format="count>1" data-count-selected-text="{0} Seleccione ">
							<option value="Menos de 50M">MENOS DE 50M</option>
							<option value="Más de 50M">MÁS DE 50M</option>
						</select>
					</div>

					<div class="form-group col-md-4">
						<label class="control-label">SEMÁFORO</label>
						<select id="semaforo_m0" class="form-control selectpicker filtroDinamico" name="semaforo_m0" multiple="multiple" multiple="multiple" title="Seleccione" data-size="4" tabindex="-1" data-selected-text-format="count>1" data-count-selected-text="{0} Seleccione ">
							<option value="1">VERDE</option>
							<!-- <option value="2">AMARILLO</option> -->
							<option value="3">ROJO</option>
							<option value="99">NEGRO</option>
						</select>
					</div>
				</div>

				<div class="row clearfix">
					<div class="form-group col-md-4">
						<label class="control-label">RANGO DIAS DE ATRASO</label>
						<select id="rango_atraso" class="form-control selectpicker filtroDinamico" name="rango_atraso" multiple="multiple" multiple="multiple" title="Seleccione" data-size="4" tabindex="-1" data-selected-text-format="count>1" data-count-selected-text="{0} Seleccione ">
							<option value="[1-7 dias]">1-7 DIAS</option>
							<option value="[8-15 dias]">8-15 DíAS</option>
							<option value="Más de 15 días">MÁS DE 15 DÍAS</option>
							<option value="Sin Atrasos">SIN ATRASOS</option>
						</select>
					</div>
					<div class="form-group col-md-4">
						<label class="control-label">¿SOLO LEASING?</label>
						<select id="leasing" class="form-control selectpicker filtroDinamico" name="leasing" multiple="multiple" multiple="multiple" title="Seleccione" data-size="4" tabindex="-1" data-selected-text-format="count>1" data-count-selected-text="{0} Seleccione ">
							<option value="SI">SI</option>
							<option value="NO">NO</option>
						</select>
					</div>

					<div class="form-group col-md-4">
						<label class="control-label">ESTADO GESTION ATRASO</label>
						<select id="gestion" class="form-control selectpicker filtroDinamico" name="gestion" multiple="multiple" multiple="multiple" title="Seleccione" data-size="4" tabindex="-1" data-selected-text-format="count>1" data-count-selected-text="{0} Seleccione ">
							<option value="GESTIONADO">GESTIONADO</option>
							<option value="NO GESTIONADO">NO GESTIONADO</option>
						</select>
					</div>
				</div>

				<div class="row clearfix">
					<div class="form-group col-md-4">
						<label class="control-label">FEVE</label>
						<select id="feve" class="form-control selectpicker filtroDinamico" name="feve" multiple="multiple" multiple="multiple" title="Seleccione" data-size="4" tabindex="-1" data-selected-text-format="count>1" data-count-selected-text="{0} Seleccione ">
							<option value="RECUP">RECUP.</option>
							<option value="REDUCIR">REDUCIR</option>
							<option value="N/A">N/A</option>
							<option value="SALIR">SALIR</option>
							<option value="EXFEVE">EXFEVE</option>
							<option value="GARANTIZAR">GARANTIZAR</option>
							<option value="SEGUIR">SEGUIR</option>
						</select>
					</div>
					<div class="form-group col-md-4">
						<label class="control-labe">FRECUENCIA DE ATRASOS TOTALES</label>
						<select id="frecuencia_atraso_tot" class="form-control selectpicker filtroDinamico" name="frecuencia_atraso_tot" multiple="multiple" multiple="multiple" title="Seleccione" data-size="4" tabindex="-1" data-selected-text-format="count>1" data-count-selected-text="{0} Seleccione ">
							<option value="0">0 MESES</option>
							<option value="1">1 MESES</option>
							<option value="2">2 MESES</option>
							<option value="3">3 MESES</option>
							<option value="4">4 MESES</option>
							<option value="5">5 MESES</option>
							<option value="6">6 MESES</option>
						</select>
					</div>

					<div class="form-group col-md-4">
						<label class="control-label">FRECUENCIA DE ATRASOS (SIN DSCTO)</label>
						<select id="frecuencia_atraso_sin_dscto" class="form-control selectpicker filtroDinamico" name="frecuencia_atraso_sin_dscto" multiple="multiple" multiple="multiple" title="Seleccione" data-size="4" tabindex="-1" data-selected-text-format="count>1" data-count-selected-text="{0} Seleccione ">
							<option value="0">0 MESES</option>
							<option value="1">1 MESES</option>
							<option value="2">2 MESES</option>
							<option value="3">3 MESES</option>
							<option value="4">4 MESES</option>
							<option value="5">5 MESES</option>
							<option value="6">6 MESES</option>
						</select>
					</div>
				</div>
				<div class="row clearfix">
					<div class="form-group col-md-4">
						<label class="control-label">Estado ficha covid</label>
						<select id="estado_aprobacion" class="form-control selectpicker filtroDinamico" name="estado_aprobacion" multiple="multiple" multiple="multiple" title="Seleccione" data-size="4" tabindex="-1" data-selected-text-format="count>1" data-count-selected-text="{0} Seleccione ">
							<option value>--</option>
							<option value="1">APROBADO</option>
							<option value="0">GUARDADO</option>
						</select>
					</div>
				</div>
				@if(Auth::user()->ROL=='38')
				<div class="row clearfix">
					<div class="form-group col-md-4">
						<label class="control-label">GESTION GYS</label>
						<select id="gesgys" class="form-control selectpicker filtroDinamico" name="gesgys" multiple="multiple" multiple="multiple" title="Seleccione" data-size="4" tabindex="-1" data-selected-text-format="count>1" data-count-selected-text="{0} Seleccione ">
							<option value="1">SI</option>
							<option value="0">NO</option>
						</select>
					</div>
				</div>
				@endif
			</div>

			<ul class="nav nav-tabs">
				<li class="active"><a data-toggle="tab" href="#resumenseg">Resumen</a></li>
				<li><a data-toggle="tab" href="#tablaseg">Tabla Clientes</a></li>
				{{-- <li><a data-toggle="tab" href="#tablaComp">Tabla Compromisos</a></li> --}}
			</ul>
			<div class="tab-content">
				<div id="resumenseg" class="tab-pane fade in active">
					<div class="row" style="margin-top:30px">
						<h3 style="text-align:center">Gestión de cartera por dias de atraso</h3>
					</div>
					<div class="row" style="margin-top:50px;margin-bottom:50px;height:400px;">
						<div class="col-md-4">
							<div id="unoSieteContainer_2" style="height:330px; cursor:pointer">
								<p class="GraphName" style="font-weight:bold;">1 - 7 días</p>
								<div style="height:200px;display: flex;justify-content: center;align-items: center;">
									<canvas height="200" class="canvasNota" id="unoSiete_2"></canvas>
									<div id="unoSieteScore_2" class="score">0%</div>
								</div>
							</div>
							<h3 style="text-align:center" id="unoSieteCounter_2">Pendientes 0 clientes</h3>
						</div>
						<div class="col-md-4">
							<div id="ochoCatorceContainer_2" style="height:330px; cursor:pointer;">
								<p class="GraphName" style="font-weight:bold;">8 - 14 días</p>
								<div style="height:200px;display: flex;justify-content: center;align-items: center;">
									<canvas height="200" class="canvasNota" id="ochoCatorce_2"></canvas>
									<div id="ochoCatorceScore_2" class="score">0%</div>
								</div>
							</div>
							<h3 style="text-align:center" id="ochoCatorceCounter_2">Pendientes 0 clientes</h3>
						</div>
						<div class="col-md-4 ">
							<div id="quinceContainer_2" style="height:330px; cursor:pointer;">
								<p class="GraphName" style="font-weight:bold;">15+ días</p>
								<div style="height:200px;display: flex;justify-content: center;align-items: center;">
									<canvas height="200" class="canvasNota" id="quince+_2"></canvas>
									<div id="quinceScore_2" class="score">0%</div>
								</div>
							</div>
							<h3 style="text-align:center" id="quinceCounter_2">Pendientes 0 clientes</h3>
						</div>
					</div>
				</div>
				<div id="tablaseg" class="tab-pane fade">
					<a class="btn btn-danger" id="btnAbrirModalMeses" style="background-color: red !important;color: white !important;">PDF COMITE</a>
					<!-- <a class="btn btn-danger" target="_blank" href="{{route('infinity.alertascartera.pdfseguimiento')}}" style="background-color: red !important;color: white !important;">Exportar Ult. Comite</a> -->
					<div class="row">
						<div class="col-xs-12">
							<div class="x_panel">
								<div class="x_content">
									<table class="table table-striped table-bordered jambo_table" id="datatables">
										<thead>
											<tr>
												<th style="width: 20px" align="center">Atraso (Dias)</th>
												<th style="text-align: center;width: 90px">Atrasos (Frecuencia)</th>
												<th style="text-align: center;">Ejecutivo</th>
												<th style="text-align: center;">Cliente</th>
												<th style="text-align: center;">Deuda IBK</th>
												<th style="text-align: center;">Semáforo</th>
												<th style="text-align: center;">Gestión</th>
												<th style="text-align: center;width: 90px">Fecha Compromiso</th>
												<th style="text-align: center;width: 110px">Fichas</th>
												<th style="text-align: center;width: 110px">Estado ficha</th>
												<th style="text-align: center;width: 110px">Blindaje</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="modal fade" tabindex="-1" role="dialog" id="modalMeses">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title">Gestión de Comité</h4>
						</div>
						<div class="modal-body">
							<div class="row">
								<label class="control-label col-md-4">Mes:</label>
								<div class="col-md-8">
									<select id="combomes" class="form-control" >
											<option value="">Seleccionar</option>
										@foreach($meses as $mes)
											<option value="{{$mes->MES}}">{{$mes->MES}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="mostrardescarga" align="center">
									Seleccionar Fecha
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="resumen" class="tab-pane fade in active" style="">
			<div class="row" style="    margin-top: 10px;">
				<h3 id="clientes" style="text-align:center">Clientes cartera: 0</h3>
			</div><br><br>
			<div class="col-md-7" style="">
				<div class="row" style="">
					<h3 id="atrasos" style="text-align:center">Con Atrasos: 0</h3>
				</div>
				<div class="row" style="margin-top: 30px;">
					<div id="more" class="col-md-5 selected">
						<h4 id="moreText" style="text-align:center">Mayor a S/ 50M: 0</h4>
					</div>
					<div class="col-md-1"></div>
					<div id="less" class="col-md-5 unselected">
						<h4 id="lessText" style="text-align:center">Menor a S/ 50M: 0</h4>
					</div>
				</div>
				<div class="row" style="    margin-top: 30px;">
					<div class="col-md-4">
						<div id="unoSieteContainer" style="height:330px; cursor:pointer">
							<p class="GraphName" style="font-weight:bold;">1 - 7 días</p>
							<div style="height:200px;display: flex;justify-content: center;align-items: center;">
								<canvas height="130" class="canvasNota" id="unoSiete"></canvas>
								<div id="unoSieteScore" class="score">0%</div>
							</div>
						</div>
						<h4 style="text-align:center" id="unoSieteCounter">Pendientes 0 clientes</h4>
					</div>
					<div class="col-md-4">
						<div id="ochoCatorceContainer" style="height:330px; cursor:pointer;">
							<p class="GraphName" style="font-weight:bold;">8 - 14 días</p>
							<div style="height:200px;display: flex;justify-content: center;align-items: center;">
								<canvas height="130" class="canvasNota" id="ochoCatorce"></canvas>
								<div id="ochoCatorceScore" class="score">0%</div>
							</div>
						</div>
						<h4 style="text-align:center" id="ochoCatorceCounter">Pendientes 0 clientes</h4>
					</div>
					<div class="col-md-4 ">
						<div id="quinceContainer" style="height:330px; cursor:pointer;">
							<p class="GraphName" style="font-weight:bold;">15+ días</p>
							<div style="height:200px;display: flex;justify-content: center;align-items: center;">
								<canvas height="130" class="canvasNota" id="quince+"></canvas>
								<div id="quinceScore" class="score">0%</div>
							</div>
						</div>
						<h4 style="text-align:center" id="quinceCounter">Pendientes 0 clientes</h4>
					</div>
				</div>
			</div>

			<div class="col-md-5" style="border-left: 2px solid">
				<div class="row" style="">
					<h3 id="vigentes" style="text-align:center">Vigentes: 0</h3>
				</div>
				<div class="row" style="margin-top: 30px;display: flex;justify-content: center;align-items: center;">
					<div class="col-md-7" style="display:flex;align-items:center;flex-direction:column">
						<div id="infinity" class="row selected" style="width:85%; margin
						:20px;">
							<h4 id="infinityText">Con L.A.: 0</h4>
						</div>
						<div id="NoInfinity" class="row unselected" style="width:85%; margin:20px;">
							<h4 id="NoInfinityText">Sin L.A.: 0</h4>
						</div>
					</div>
					<div class="col-md-5" style="margin-top:70px;">
						<div id="gestionVigContainer" style="height:330px;">
							<p class="GraphName" style="font-weight:bold;"></p>
							<div id="gestionVigClick" style="height:200px;display: flex;justify-content: center;align-items: center;cursor:pointer">
								<canvas height="200" class="canvasNota" id="gestionVig"></canvas>
								<div id="gestionVigScore" class="score">0%</div>
							</div>
						</div>
						<h4 style="text-align:center" id="gestionVigCounter">Pendientes 0 / 0</h4>
					</div>
				</div>
			</div>
		</div>
		<div id="tablaComp" class="tab-pane fade">
			<div class="row">
				<div class="col-xs-12">
					<div class="x_panel">
						<div class="x_content">
							<table class="table table-striped table-bordered jambo_table" id="datatablesComp">
								<thead>
									<tr>
										<th style="width: 20px" align="center">Cliente</th>
										<th style="width: 20px" align="center">Ejecutivo</th>
										<th style="width: 20px" align="center">Area de Eliminación</th>
										<th style="text-align: center;width: 90px">Tipo de Compromiso</th>
										<th style="text-align: center;">Detalle de Compromiso</th>
										<th style="text-align: center;">Descripcion</th>
										<th style="text-align: center;">Variable</th>
										<th style="text-align: center;">Fecha Fin de Compromiso</th>
										<th style="text-align: center;">Gestor</th>
										<th style="text-align: center;"></th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="modalGuardar">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <h3 style="text-align:center" id="messageModal"></h3>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="modalGestionar">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="padding: 10px;">
            <div class="modal-header">
                <h4 class="modal-title">Gestiona tu Compromiso</h4>
            </div>
            <form action="javascript:gestionCumplimientoPOST()">
                <input type="hidden" name="IDCUMPL" id="IDCUMPL">
                <input type="hidden" name="CU" id="CU">
                <input type="hidden" name="IDCOVID" id="IDCOVID">
                <div class="modal-body">
					<div style="padding: 10px;" class="row" id="rowFechaCompromiso">
                        <div class="col-md-4">Fecha de Fin de Compromiso</div>
                        <div class="col-md-8">
                            <input  autocomplete="off" class="form-control fechaCompromiso cerrado" type="text" name="fechaCompromisoGestion" id="fechaCompromisoGestion" placeholder="Seleccionar fecha" value="" required>
                        </div>
                    </div>
                    <div style="padding: 10px;" class="row">
                        <div class="col-md-4">Status</div>
                        <div class="col-md-8">
                            <select name="status" id="status" class="status form-control custom-select is-invalid">
                                <option selected value="">-Seleccionar-</option>
                                <option value="ELIMINADO">ELIMINADO</option>
                                <option value="CUMPLIO">CUMPLIÓ</option>
                                <option value="NO CUMPLIO">NO CUMPLIÓ</option>
                            </select>
                        </div>
                    </div>
                    <div style="display: flex;
                        justify-content: center;
                        align-items: center;
                        padding-top: 25px;" class="row">
                        <button id="btnAct" type="submit" class="btn btn-primary btn-lg btn-success">Actualizar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<select style="visibility:hidden" type="hidden" name="optionHolder" id="optionHolder"></select>
@stop
