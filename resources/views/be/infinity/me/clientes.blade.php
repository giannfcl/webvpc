@extends('Layouts.layout')

@section('js-libs')

<link href="{{ URL::asset('css/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css">

<script type="text/javascript" src="{{ URL::asset('js/datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/jszip.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/pdfmake.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/vfs_fonts.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/buttons.flash.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/buttons.print.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/buttons.html5.min.js') }}"></script>

<link href="{{ URL::asset('css/datatables.min.css') }}" rel="stylesheet" type="text/css">

<script type="text/javascript" src="{{ URL::asset('js/numeral.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/chart.bundle.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/utils.js') }}"></script>
<link href="{{ URL::asset('css/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css">

<script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.es.min.js') }}"></script>

<link href="{{ URL::asset('css/formValidation.min.css') }}" rel="stylesheet" type="text/css" > 

<script type="text/javascript" src="{{ URL::asset('js/formvalidation/formValidation.popular.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/formvalidation/language/es_CL.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/formvalidation/framework/bootstrap.min.js') }}"></script>

@stop

@section('content')

@section('pageTitle', 'Líneas Automáticas')

<style>
    .celda-centrada{
        vertical-align: middle !important; 
        text-align: center;
    }
    table a,table a:hover,table a:active,table a:visited{
        text-decoration: underline;
    }


    .table>tbody>tr>td{
        padding: 5px;
    }
</style>

<?php
//Calculos de indicadores
if (isset($GZBE)) {
    $GZ_R=$GZBE->REGISTRO;
    $GZ_N=$GZBE->NOMBRE;
}
if (isset($GDBE)) {
    $GD_R=$GDBE->REGISTRO;
    $GD_N=$GDBE->NOMBRE;
}
if (isset($ARBE)) {
    $AR_R=$ARBE->REGISTRO;
    $AR_N=$ARBE->NOMBRE;
}
if (isset($JR[0])) {
    $JR_R=$JR[0]->REGISTRO;
    $JR_N=$JR[0]->NOMBRE;
}
if (isset($SR)) {
    $SR_R=$SR->REGISTRO;
    $SR_N=$SR->NOMBRE;
}
if (isset($GR)) {
    $GR_R=$GR->REGISTRO;
    $GR_N=$GR->NOMBRE;
}
if ($registro) {
    $registro=$registro;
}
$cartera = 0;
$clientesInfinity = 0;
$documentacion = 0;
$gestionado = 0;
$gestionable = 0;
$vencidoIBK = 0;
$vencidoRCC = 0;
$visitas = 0;
$recalculo_pendientes=0;
foreach ($stats as $stat) {
    $cartera += $stat->TOTAL;
    $clientesInfinity += $stat->INFINITY;
    $documentacion += $stat->DOCUMENTACION_COMPLETA_INFINITY;
    $documentacionV2 = $stat->DOCUMENTACION_COMPLETA_INFINITY_2;
    $gestionado += $stat->GESTIONES_PENDIENTES;
    $gestionable += $stat->GESTIONABLE_INFINITY;
    $vencidoIBK += $stat->VENCIDO_IBK;
    $vencidoRCC += $stat->VENCIDO_RCC;
    $visitas+= $stat->VISITA_INFINITY;
    $recalculo_pendientes+=$stat->RECALCULOS_PENDIENTES;
}
?>

<div class="row">
    <div class="col-xs-6">
        <div class="x_panel">
            <p><b>CARTERA: {{$cartera}}</b></p>
            <div class="progress">
                @foreach($stats as $key => $stat)
                <div class="progress-bar" style="width: {{$stat->TOTAL*100/$cartera}}%; 
                     background-color: {{$key? App\Entity\Infinity\Semaforo::getColor($key) : 'gray'}}">
                    {{$stat->TOTAL}}
                </div>
                @endforeach
            </div>
            <p><b>CLIENTES LINEAS AUTOMATICAS: {{$clientesInfinity}}</b></p>
            <div class="progress">
                @if($clientesInfinity>0)
                    @foreach($stats as $key => $stat)
                    <div class="progress-bar" style="width: {{$stat->INFINITY*100/$clientesInfinity}}%; background-color: {{App\Entity\Infinity\Semaforo::getColor($key)}}">
                        {{$stat->INFINITY}}
                    </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
    <div class="col-xs-6">
        <div class="x_panel">
            <div class="row">
                <div class="col-md-6">
                    <p>GESTIONES PENDIENTES</p>
                    <div class="progress">
                        @if($gestionado>0)
                        <div class="progress-bar active" role="progressbar" style="min-width: 10%; width: {{$gestionado*100/$clientesInfinity}}%">
                            {{number_format($gestionado*100/$clientesInfinity,0)}}%
                        </div>
                        @endif
                    </div>
                </div>

                <div class="col-md-6">
                    <p>DOCUMENTACIÓN</p>
                    <div class="progress">
                        @if($clientesInfinity>0)
                        <div class="progress-bar active" role="progressbar" style="min-width: 10%; width: {{$documentacionV2*100/$clientesInfinity}}%">
                            {{number_format($documentacionV2*100/$clientesInfinity,0)}}%
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3"> 
                    <p>VISITA PENDIENTES</p>
                    <div class="progress">
                        @if($clientesInfinity>0)
                        <div class="progress-bar active" role="progressbar" style="min-width: 10%; width: {{$visitas*100/$clientesInfinity}}%">
                            {{number_format($visitas*100/$clientesInfinity,0)}}%
                        </div>
                        @endif
                    </div>
                </div>
                <div class="col-md-3">
                    <p>RECALCULO PENDIENTES</p>
                    <div class="progress">
                        @if($clientesInfinity>0)
                        <div class="progress-bar active" role="progressbar" style="min-width: 10%; width: {{$recalculo_pendientes*100/$clientesInfinity}}%">
                            {{number_format($recalculo_pendientes*100/$clientesInfinity,0)}}%
                        </div>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <p><strong>VENCIDOS IBK:</strong> {{$vencidoIBK}}</p>
                    <p><strong>VENCIDOS RCC:</strong> {{$vencidoRCC}}</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="x_panel">
            <div class="x_content">
                <table class="table table-striped table-bordered jambo_table" id="data-table">
                    <thead>
                        <tr>
                            <th>LA</th>
                            <th>Ejecutivo</th>
                            <th>Cliente</th>
                            <th>Deuda</th>
                            <th>Vencido</th>
                            <th>Semáforo</th>
                            <th>Recálc</th>
                            <th>Docs</th>
                            <th>Visitas</th>
                            <th>Gestión</th>
                            <th>Acciones</th>
                            <th>Status Comité</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Colores de semaforos -->
@foreach(App\Entity\Infinity\Semaforo::SEMAFOROS_ME_COLORES as $key => $color)
<input id="semaforo-{{$key}}" type="hidden" value="{{$color}}">
@endforeach

<input id="flg-visibilidad-banca" value="{{$flgVisibilidadBanca}}" type="hidden">
<input id="flg-visibilidad-zonal" value="{{$flgVisibilidadZonal}}" type="hidden">
<input id="flg-visibilidad-jefatura" value="{{$flgVisibilidadJefatura}}" type="hidden">
<input id="flg-visibilidad-general" value="{{$flgVisibilidadGeneral}}" type="hidden">
<input id="flg-visibilidad-control" value="{{$flgVisibilidadControl}}" type="hidden">

<div class="hidden">
@foreach($iconoAlertas as $key => $icono)
<div class="alerta-icono-{{$key}}">
{!! $icono !!}
</div>
@endforeach
</div>

<input id="icono" type="hidden" value="{{ URL::asset('img/Logo_favi.ico') }}" />
<input id="icono2" type="hidden" value="{{ URL::asset('img/Logo_favi_2.ico') }}" />


<input id="GZ_R" type="hidden" value="{{$GZ_R}}" />
<input id="GZ" type="hidden" value="{{$GZ_N}}" />
<input id="GD_R" type="hidden" value="{{$GD_R}}" />
<input id="GD" type="hidden" value="{{$GD_N}}" />
<input id="AR_R" type="hidden" value="{{$AR_R}}" />
<input id="AR" type="hidden" value="{{$AR_N}}" />
<input id="JR_R" type="hidden" value="{{$JR_R}}" />
<input id="JR" type="hidden" value="{{$JR_N}}" />
<input id="SR_R" type="hidden" value="{{$SR_R}}" />
<input id="SR" type="hidden" value="{{$SR_N}}" />
<input id="GR_R" type="hidden" value="{{$GR_R}}" />
<input id="GR" type="hidden" value="{{$GR_N}}" />
<input id="logeado" type="hidden" value="{{$registro}}" />



<div class="modal fade" tabindex="-1" role="dialog" id="modalDocumentacion_Comite">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Gestión de Comité</h4>
            </div>
            <div class="modal-body">
                <form id="frmGestion_comite" class="form-horizontal" enctype="multipart/form-data" method="POST" action="{{route('infinity.me.detalle.guardar.gestion')}}">
                    <div>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">COD UNICO:</label>
                        <div class="input-group col-md-9 col-sm-9 col-xs-12">
                            <input class="form-control" name="codUnico" id="codUnico" type="text" value="" readonly="readonly">
                        </div>
                    </div>
                    <div>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">RMG:</label>
                        <div class="input-group col-md-9 col-sm-9 col-xs-12">
                            <input class="form-control" id="rmg_modal" type="text" value="" readonly="readonly">
                        </div>
                    </div>
                    <div>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">RATING:</label>
                        <div class="input-group col-md-9 col-sm-9 col-xs-12">
                            <input class="form-control" id="rating_modal" type="text" value="" readonly="readonly">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Estado:</label>
                        <div class="input-group col-md-9 col-sm-9 col-xs-12">
                            <input readonly="" class="form-control" value="Comité">
                            <input name="estadoGestion" value="4" hidden="hidden">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Fecha Gestión:</label>
                        <div class="input-group col-md-9 col-sm-9 col-xs-12" >               
                            <div class="input-group-addon styleAddOn"><i class="glyphicon glyphicon-calendar fa fa-calendar" for="fechaGestion"></i></div>
                            <input autocomplete="off" class="form-control dfecha"  type="text" id="fechaGestion" name="fechaGestion" placeholder="Seleccionar fecha" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Política:</label>
                        <div class="input-group col-md-9 col-sm-9 col-xs-12" >
                            <div class="input-group-addon styleAddOn"><i class="fa fa-tasks" for="PoliticaAsociada"></i></div>
                            <select class="form-control" name="PoliticaAsociada" id="PoliticaAsociada_Comite">
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Decisión:</label>
                        <div class="input-group col-md-9 col-sm-9 col-xs-12" >
                            <div class="input-group-addon styleAddOn"><i class="fa fa-tasks" for="DecisionComite"></i></div>
                            <select class="form-control" name="DecisionComite">
                                <option value="">Seleccionar Aprobacion</option>
                                <option value="1">Aprobado</option>
                                <option value="0">Desaprobado</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Comentario:</label>
                        <div class="input-group col-md-9 col-sm-9 col-xs-12">
                            <textarea class="form-control" rows="10" style="resize: none" placeholder="Ingresar Comentario..." name="comentarioGestion" ></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Adjunto:</label>
                        <div class="input-group col-md-9 col-sm-9 col-xs-12">
                            <input type="file" name="adjuntoGestion" id = "adjuntoGestion" class="form-control image" style="border-style: none">
                        </div>
                    </div>
                    <div class="form-group valid">
                        <label class="control-label col-md-7 col-sm-3 col-xs-12">Solo puede revisarlo</label>
                        <label id="usuario_permitido" class="control-label col-md-7 col-sm-3 col-xs-12"></label>
                        <input id="re_usu_permitido" hidden="hidden">
                    </div>
                    <center class="guardarGestion"><button class="btn btn-success" type="submit">Guardar</button></center>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>


@stop

@section('js-scripts')
<script>
$('#data-table').DataTable({
    processing: true,
    "bAutoWidth":false,
    rowId: 'staffId',
    dom: 'Blfrtip',
        buttons: [
            { "extend": 'pdf', "text":'PDF',"className": 'btn btn-danger btn-xs' },
            { "extend": 'excel', "text":'EXCEL',"className": 'btn btn-success btn-xs' }
        ],
    serverSide: true,
    language: {"url": "{{ URL::asset('js/Json/Spanish.json') }}"},
    ajax: "{{ route('infinity.me.clientes.get') }}",
    "aLengthMenu": [[25, 50, -1], [25, 50, "Todo"]],
    "iDisplayLength": 25,
    "order": [[9, "desc"]],
    columnDefs: [
        {
            targets: 0,
            data: null,
            searchable: false,
            className: "celda-centrada",
            render: function (data, type, row) {
                if (row.FLG_INFINITY == 1) {
                    return '<img src="'+ $('#icono').val()+'" style="width: 20px"/>';
                } else if (row.FLG_INFINITY == 2) {
                    return '<img src="'+ $('#icono2').val()+'" style="width: 20px"/>';
                }else{
                    return '';
                }
            }
        },
        {
            targets: 1,
            data: null,
            searchable: $('#flg-visibilidad-general').val() == '1' ? true : false,
            visible: $('#flg-visibilidad-general').val() == '1' ? true : false,
            //className: "celda-centrada",
            render: function (data, type, row) {

                html = '';

                if ($('#flg-visibilidad-general').val() == '1')
                    html += ''+row.EJECUTIVO;

                if ($('#flg-visibilidad-zonal').val() == '1')
                    html += '<br/>' + 'JEFE: '+ row.JEFATURA;

                if ($('#flg-visibilidad-banca').val() == '1')
                    html += '<br/>' + 'ZONAL: '+row.ZONAL;

                return html;
            }
        },
        {
            targets: 2,
            data: null,
            render: function (data, type, row) {
                var linea1 = 'CU: ' + row.COD_UNICO;
                var linea2 = '<br/>';
                if($('#flg-visibilidad-control').val() == '0')
                    linea2+='<a target="_blank" href="{{route("infinity.me.cliente.detalle")}}?cu=' + row.COD_UNICO + '">' + row.NOMBRE + '</a>';
                else
                    linea2+='<p>'+row.NOMBRE+'</p>';

                return linea1 + linea2;
            }
        },
        {
            targets: 3,
            data: null,
            searchable: false,
            render: function (data, type, row) {
                return numeral(row.SALDO_RCC / 1000).format('0,0') + 'M';
            }
        },
        {
            targets: 4,
            data: null,
            searchable: false,
            render: function (data, type, row) {
                var linea1 = 'IBK: ' + numeral(row.SALDO_VENCIDO_IBK / 1000).format('0,0') + 'M';
                var linea2 = '<br/>RCC: ' + numeral(row.SALDO_VENCIDO_RCC / 1000).format('0,0') + 'M';
                return linea1 + linea2;
            }
        },
        {
            targets: 5,
            data: null,
            searchable: false,
            className: "celda-centrada",
            render: function (data, type, row) {    
                var color1 = $('#semaforo-' + row.NIVEL_ALERTA).val();
                var color2 = $('#semaforo-' + row.NIVEL_ALERTA_ANTERIOR).val();
                var color3 = $('#semaforo-' + row.NIVEL_ALERTA_ANTERIOR_2).val();
                return '<i class="fa fa-circle fa-lg" style="padding-right : 3px; color:' + color3 + '"></i>' +
                        '<i class="fa fa-circle fa-lg" style="padding-right : 3px; color:' + color2 + '"></i>' +
                        '<i class="fa fa-circle fa-2x" style="color:' + color1 + '"></i>';
            }
        },
        {
            targets: 6,
            data: null,
            searchable: false,
            className: "celda-centrada",
            render: function (data, type, row, meta) {
                if (row.ALERTA_RECALCULO){
                    return $('.alerta-icono-'+row.ALERTA_RECALCULO)[0].outerHTML;
                }else{
                    return '';
                }
            }
        },
        {
            targets: 7,
            data: null,
            searchable: false,
            className: "celda-centrada",
            render: function (data, type, row) {
                if (row.ALERTA_DOCUMENTACION !== null){
                    return $('.alerta-icono-'+row.ALERTA_DOCUMENTACION)[0].outerHTML;
                }else{
                    return '';
                }
            }
        },
        {
            targets: 8,
            data: null,
            searchable: false,
            className: "celda-centrada",
            render: function (data, type, row, meta) {
                if (row.ALERTA_VISITA !== null){
                    return $('.alerta-icono-'+row.ALERTA_VISITA)[0].outerHTML;
                }else{
                    return '';
                }
            }
        },
        {
            targets: 9,
            data: null,
            searchable: false,
            className: "celda-centrada",
            render: function (data, type, row, meta) {
                var html = '';
                var texto = '';
                var color = '';
                if (row.FLG_INFINITY >= 1) {
                    if (row.TIMER < 0){
                        color = '#73879C';
                        texto = 'vence en ' + Math.abs(row.TIMER) + ' día(s)';
                        html += '<div style="color: '+ color + '"><i class="fas fa-clock"></i> ' + texto + '</div>';
                    }
                    if(row.TIMER == 0){
                        color = '#D9534F';
                        texto = 'vence hoy';
                        html += '<div style="color: '+ color + '"><i class="fas fa-clock"></i> ' + texto + '</div>';
                    }
                    if(row.TIMER > 0){
                        color = '#D9534F';
                        texto = 'venció hace '  + Math.abs(row.TIMER) + ' día(s)';
                        html += '<div style="color: '+ color + '"><i class="fas fa-clock"></i> ' + texto + '</div>';
                    }
                }
                return html;
            }
        },
        {
            targets: 10,
            data: null,
            searchable: true,
            visible: $('#flg-visibilidad-control').val() == '0' ? true : false,
            render: function (data, type, row) {
                html = '<a target="_blank" href="{{route("infinity.me.cliente.conoceme")}}?cu=' + row.COD_UNICO + '">Conóceme</a>';
                if (row.CANT_POLI_CUALI == null || row.CANT_POLI_CUALI < 0) {
                    html += ' <a target="_blank" href="{{route("infinity.me.cliente.visita")}}?cu=' + row.COD_UNICO + '">Visita</a>';
                }
                return html;
            }
        },
        {
            targets: 11,
            data: null,
            searchable: false,
            visible: $('#flg-visibilidad-control').val() == '0' ? true : false,
            render: function (data, type, row) {
                html='';
                if ((row.FLG_INFINITY > 1 && row.CANT_GESTIONES_COMITE_PENDIENTES!=null) || (row.FLG_INFINITY == '0' && row.PERMITIDOS==1)) {
                    html = '<a id="click_comite" cu="' + row.COD_UNICO + '" rmg="' + row.RMG + '" rating="' + row.RATING + '">Pendiente</a>';
                }
                return html;
            }
        }
    ],
    columns: [
        {data: 'FLG_INFINITY', name: 'FLG_INFINITY', searchable: false},
        {data: 'NOMBRE', name: 'WU.NOMBRE',searchable: false},
        {data: 'NOMBRE', name: 'WIC.NOMBRE'},
        {data: 'SALDO_RCC', name: 'SALDO_RCC', searchable: false},
        {data: 'SALDO_VENCIDO_RCC', name: 'SALDO_VENCIDO_RCC', searchable: false},
        {data: 'NIVEL_ALERTA', name: 'NIVEL_ALERTA', searchable: false},
        {data: 'FECHA_ULTIMO_RECALCULO_INFINITY', name: 'FECHA_ULTIMO_RECALCULO', searchable: false},
        {data: 'ALERTA_DOCUMENTACION', name: 'ALERTA_DOCUMENTACION', searchable: false},
        {data: 'FECHA_ULTIMA_VISITA_INFINITY', name: 'FECHA_ULTIMA_VISITA', searchable: false}, //Reemplazar con info de resultado
        {data: 'CANT_GESTIONES_PENDIENTES', name: 'CANT_GESTIONES_PENDIENTES',searchable: false},
        {data: 'COD_UNICO', name: 'WIC.COD_UNICO',sortable:false},
        {data: 'CANT_GESTIONES_COMITE_PENDIENTES', name: 'CANT_GESTIONES_COMITE_PENDIENTES',searchable: false}
    ]
});

function decodeHtml(html) {
    var txt = document.createElement("textarea");
    txt.innerHTML = html;
    return txt.value;
};

$(document).on('click','#click_comite',function () {

    $("#usuario_permitido").html('');
    $("#re_usu_permitido").val('');

    $('#rmg_modal').val('');
    $('#rating_modal').val('');

    $('#rmg_modal').val($(this).attr('rmg'));
    $('#rating_modal').val($(this).attr('rating'));

    $('.dfecha').each(function () {
        $(this).datepicker({
            maxViewMode: 1,
            //daysOfWeekDisabled: "0,6",
            language: "es",
            autoclose: true,
            startDate: "-365d",
            endDate: "0d",
            format: "yyyy-mm-dd",
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#frmGestion_comite').formValidation('revalidateField', 'fechaGestion');
        });
    });

    $('#frmGestion_comite').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            'fechaGestion': {
                validators: {
                    notEmpty: {
                        message: 'Debes seleccionar una fecha'
                    },
                    date: {
                        format: 'YYYY-MM-DD',
                        message: 'Ingrese una fecha válida',
                    }
                }
            },
            'PoliticaAsociada': {
                validators: {
                    notEmpty: {
                        message: 'Debes seleccionar una Política'
                    }
                }
            },
            'comentarioGestion': {
                validators: {
                    notEmpty: {
                        message: 'Debes seleccionar un estado'
                    },
                    stringLength: {
                        max: 500,
                        min: 30,
                        message: 'El comentario debe tener entre 30 y 500 caracteres'
                    }
                }
            },
            'DecisionComite': {
                validators: {
                    notEmpty: {
                        message: 'Debes seleccionar una Decisión'
                    }
                }
            }
        },
    });

    var cu = $(this).attr('cu');
    if (parseInt(cu)>0){
        $('#modalDocumentacion_Comite').modal();
        GZ = $("#GZ").val();
        GZ_R = $("#GZ_R").val();
        GD = $("#GD").val();
        GD_R = $("#GD_R").val();
        AR = $("#AR").val();
        AR_R = $("#AR_R").val();
        JR = $("#JR").val();
        JR_R = $("#JR_R").val();
        SR = $("#SR").val();
        SR_R = $("#SR_R").val();
        GR = $("#GR").val();
        GR_R = $("#GR_R").val();

        var per = "";
        var R_per = "";
        var normal =['AAA','AA','A','BBB','BB','B'];
        var muybajo =['CC','C','SIN RATING'];
        var bajo=['CCC'];

        if ($.inArray($('#rating_modal').val(),normal)>=0) {
                if ($('#rmg_modal').val()<150000) {
                    per=GZ;
                    R_per=GZ_R;
                }
                if (150000<=$('#rmg_modal').val()) {
                    per=GZ;
                    R_per=GZ_R;
                    if (200000<=$('#rmg_modal').val()) {
                        per=GD;
                        R_per=GD_R;
                        if (350000<=$('#rmg_modal').val()) {
                            per=JR;
                            R_per=JR_R;
                            if (600000<=$('#rmg_modal').val()) {
                                per=SR;
                                R_per=SR_R;
                                if (1200000<=$('#rmg_modal').val()) {
                                    per=GR;
                                    R_per=GR_R;
                                }
                            }
                        }
                    }
                }
        }
        if ($.inArray($('#rating_modal').val(),bajo)>=0) {
                if ($('#rmg_modal').val()<150000) {
                    per=GZ;
                    R_per=GZ_R;
                }
                if (150000<=$('#rmg_modal').val()) {
                    per=GD;
                    R_per=GD_R;
                    if (250000<=$('#rmg_modal').val()) {
                        per=AR;
                        R_per=AR_R;
                        if (300000<=$('#rmg_modal').val()) {
                            per=JR;
                            R_per=JR_R;
                            if (600000<=$('#rmg_modal').val()) {
                                per=SR;
                                R_per=SR_R;
                                if (1200000<=$('#rmg_modal').val()) {
                                    per=GR;
                                    R_per=GR_R;
                                }
                            }
                        }
                    }
                }
        }
        if ($.inArray($('#rating_modal').val(),muybajo)>=0) {
            if ($('#rmg_modal').val()<300000) {
                per=AR;
                R_per=AR_R;
            }
            if (300000<=$('#rmg_modal').val()) {
                per=JR;
                R_per=JR_R;
                if (600000<=$('#rmg_modal').val()) {
                    per=SR;
                    R_per=SR_R;
                    if (1200000<=$('#rmg_modal').val()) {
                        per=GR;
                        R_per=GR_R;
                    }
                }
            }
        }

        $("#re_usu_permitido").val(R_per);
        $("#usuario_permitido").html(per);
        
        if ($("#logeado").val()!=R_per) {
            $('.guardarGestion').attr('hidden','hidden');
            $('.valid').removeAttr('hidden');
        }
        if ($("#logeado").val()==R_per
            ||($("#logeado").val()==GD_R && 150000<=$('#rmg_modal').val()<200000 && $.inArray($('#rating_modal').val(),normal)>=0) //Gerente de Division puede además realizar acciones con rmg entre 150000 y 200000 , rating normal
            ||($("#logeado").val()==GD_R && $('#rmg_modal').val()<150000 && $.inArray($('#rating_modal').val(),normal.concat(bajo))>=0) //Gerente de Division puede además realizar acciones con menos 150000, rating normal y bajo
            ||($("#logeado").val()==JR_R && $('#rmg_modal').val()<600000) //Jefe de Riesgos puede además realizar acciones con menos 600000
            ||($("#logeado").val()==SR_R && $('#rmg_modal').val()<1200000) //SubGerente de Riesgos puede además realizar acciones con menos 1200000
            ||$("#logeado").val()==GR_R //Gerente de Riesgos puede realizar acciones en todo momento
            ){

            $('.guardarGestion').removeAttr('hidden');
            $('.valid').attr('hidden','hidden');
        }

        $.ajax({
            type: "POST",
            data: {
                cu: cu,
            },
            async: false,
            url: "{{route('infinity.me.cliente.politicas_comite')}}",
            success: function (result) {
                $("#codUnico").val(cu);
                $("#PoliticaAsociada_Comite").html(result);
            }
        });
    }
});

// $(window).bind('resize', function () {
//     $('#data-table').dataTable().fnAdjustColumnSizing();
// } );

</script>
@stop