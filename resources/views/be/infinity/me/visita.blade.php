@extends('Layouts.layout')

@section('js-libs')
<link href="{{ URL::asset('css/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ URL::asset('css/datatables.min.css') }}" rel="stylesheet" type="text/css">

<script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.es.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/jspdf.debug.js') }}"></script>

<!--FORM VALIDATION-->
<link href="{{ URL::asset('css/formValidation.min.css') }}" rel="stylesheet" type="text/css" > 
<script type="text/javascript" src="{{ URL::asset('js/formvalidation/formValidation.popular.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/formvalidation/language/es_CL.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/formvalidation/framework/bootstrap.min.js') }}"></script>

<style type="text/css">
    .formatoCheck{
        font-size: 14px;
    }
    .tooltip-inner{
        min-width: 200px !important;
    }
    .help-block{
        font-size: 1em !important;
    }
    .espaciado{
        padding-left: 15px;
    }

    .has-feedback .form-control {
        padding-right: 10px !important;
    }

    .fv-plugins-icon, .form-control-feedback{
        display: none !important;
    }

</style>
@stop
<?php
//Son los usuarios que no pueden hacer modificaciones en esta ficha
$fechavalida = '2017-01-01';
if ($visita && $visita->getValue('_fechaVisita')) {
    $fechavalida=$visita->getValue('_fechaVisita');
}
?>

@section('content')

@section('pageTitle','Ficha Visita - Covid')
<!--Flag de solo lectura-->
<input type="text" id="fechaValida" value="{{$fechavalida}}" hidden="">

<form id="frmVisita" action="{{route('infinity.me.cliente.visita.guardar')}}" method="POST">
    <input type="hidden" name="codunicoConoceme" value="{{$cliente->getValue('_codunicoConoceme')}}">
    <input type="hidden" name="codunico" value="{{$cliente->getValue('_codunico')}}">
    <input type="hidden" name="flgGuardado" value="{{$flgGuardado}}">
    @if ($flgGuardado=="1" && in_array($privilegios,array(App\Model\Infinity\Visita::LLENADOR_SIN_BLINDAJE)))
    <div class="alert alert-warning" role="alert">La ficha esta a espera de ser validada</div>
    @endif
    @if ($flgGuardado=="0" && $flgNotificado=="1")
    <div style="font-size:1.5em" class="alert alert-warning" role="alert">La ficha anterior ya fue validada. Esta es una nueva ficha</div>
    @endif
    @if ($privilegios == App\Model\Infinity\Visita::SOLO_LECTURA)
    <div class="alert alert-warning" role="alert">Modo Lectura</div>
    @endif
    @if ($privilegios == App\Model\Infinity\Visita::LLENADOR_BLINDAJE && $mesesBlindaje>0)
    <div class="alert alert-warning" role="alert">Te han concedido blindaje por {{$mesesBlindaje}} {{$mesesBlindaje>1?'meses':'mes'}}. Puedes <strong>guardar y aprobar</strong> de manera inmediata si es que no se dispara ninguna alerta.</div>
    @endif
    <!-- DATOS GENERALES DE LA EMPRESA -->
<div id="impresion">
    <div class="row">
        <div class="col-xs-12" >
            <div class="x_panel" style="min-height: 180px">
                <div class="x_title">
                    <h2>Empresa</h2>
                    <ul class="nav navbar-right panel_toolbox">
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-4 form-horizontal form-label-left">
                            <div class="form-group">
                                <label class="control-label col-sm-3 col-xs-12">Empresa</label>
                                <div class="col-sm-9 col-xs-12">
                                    <input type="text" class="form-control" readonly  value="{{$cliente->getValue('_nombre')}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3 col-xs-12">Cod. Unico</label>
                                <div class="col-sm-9 col-xs-12">
                                    <input type="text" class="form-control" readonly value="{{$cliente->getValue('_codunico')}}">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 form-horizontal form-label-left">
                            <div class="form-group">
                                <label class="control-label col-sm-3 col-xs-12">DDJJ Dic</label>
                                <div class="col-sm-8 col-xs-12">
                                    <input type="text" class="form-control" readonly  value="{{$infoCliente->FECHA_ULTIMO_EEFF_1}}">
                                </div>
                                <div class="col-sm-1 col-xs-12">
                                    <?php
                                    echo \App\Entity\Infinity\AlertaDocumentacion::getIcono(\App\Entity\Infinity\AlertaDocumentacion::ID_DDJJ, $infoCliente->FECHA_ULTIMO_EEFF_1,$infoCliente->FLG_INFINITY,$infoCliente->FECHA_INGRESO_INFINITY);
                                    ?>                                    
                                </div>                                        

                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3 col-xs-12">EEFF Jun</label>
                                <div class="col-sm-8 col-xs-12">
                                    <input type="text" class="form-control" readonly value="{{$infoCliente->FECHA_ULTIMO_EEFF_2}}">
                                </div>
                                <div class="col-sm-1 col-xs-12">
                                    <?php
                                    echo \App\Entity\Infinity\AlertaDocumentacion::getIcono(\App\Entity\Infinity\AlertaDocumentacion::ID_EEFF, $infoCliente->FECHA_ULTIMO_EEFF_2,$infoCliente->FLG_INFINITY,$infoCliente->FECHA_INGRESO_INFINITY);
                                    ?>                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 form-horizontal form-label-left">
                            <div class="form-group">
                                <label class="control-label col-sm-3 col-xs-12">F02</label>
                                <div class="col-sm-8 col-xs-12">
                                    <input type="text" class="form-control" readonly  value="{{$infoCliente->FECHA_ULTIMO_F02}}">
                                </div>
                                <div class="col-sm-1 col-xs-12">
                                    <?php
                                    echo \App\Entity\Infinity\AlertaDocumentacion::getIcono(\App\Entity\Infinity\AlertaDocumentacion::ID_F02, $infoCliente->FECHA_ULTIMO_F02,$infoCliente->FLG_INFINITY,$infoCliente->FECHA_INGRESO_INFINITY);
                                    ?>                                    
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3 col-xs-12">IBR</label>
                                <div class="col-sm-8 col-xs-12">
                                    <input type="text" class="form-control" readonly value="{{$infoCliente->FECHA_ULTIMO_IBR}}">
                                </div>
                                <div class="col-sm-1 col-xs-12">
                                    <?php
                                    echo \App\Entity\Infinity\AlertaDocumentacion::getIcono(\App\Entity\Infinity\AlertaDocumentacion::ID_IBR, $infoCliente->FECHA_ULTIMO_IBR,$infoCliente->FLG_INFINITY,$infoCliente->FECHA_INGRESO_INFINITY);
                                    ?>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if (true)

    <!-- ZONA DE CHECKS -->
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="checkbox">
                            <label class="formatoCheck">
                                <input name="flagCambioModelo" id="flagCambioModelo" type="checkbox" {{$visita->getValue('_flagCambioModeloNegocio') == 1?'checked':''}}>¿Cambió el modelo de negocio? 
                            </label>
                        </div>           

                        <div class="checkbox">
                            <label class="formatoCheck">
                                <input name="flagCambioMixVentas" id="flagCambioMixVentas" type="checkbox" {{$visita->getValue('_flagCambioMixVentas') == 1?'checked':''}}>¿Cambió los productos o mix de ventas? 
                            </label>
                        </div>
                        <!--
                            <div class="checkbox">
                                <label class="formatoCheck">
                                    <input name="flagCambioProcesoIntegracion" id="flagCambioProcesoIntegracion" type="checkbox" {{$visita->getValue('_flagCambioProcesoIntegracion')== 1?'checked':''}}>¿Presenta algún cambio en el proceso de integración de la cadena? 
                                </label>
                            </div>
                        -->

                        <div class="checkbox">
                            <label class="formatoCheck">
                                <input name="flagCambioConcentracionProveedores" id="flagCambioConcentracionProveedores" type="checkbox" {{$visita->getValue('_flagCambioConcentracionProveedores')== 1?'checked':''}}>¿Cambió la concentración de sus proveedores?
                                <!-- ¿Cambió la concentración de proveedores de sus clientes? -->
                            </label>
                        </div>

                        <div class="checkbox">
                            <label class="formatoCheck">
                                <input name="flagCambioConcentracionVentas" id="flagCambioConcentracionVentas" type="checkbox" {{$visita->getValue('_flagCambioConcentracionVentas')== 1?'checked':''}}>¿Cambió la concentración de ventas de sus clientes? 
                            </label>
                        </div>

                        <div class="checkbox">
                            <label class="formatoCheck col-sm-4">
                                <input name="flagCambioOperaciones" id="flagCambioOperaciones" type="checkbox" class="check" {{$visita->getValue('_flagCambioOperaciones')== 1?'checked':''}}>¿Cambió la zona de operaciones? 
                            </label>
                            <div class="col-sm-6">
                                <button data-toggle="modal" data-target="#modalZonaOperaciones" type="button" class="btn btn-primary" id="btnZonasOperaciones" disabled>Lista de Zonas de Operacion</button>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="checkbox">
                            <label class="formatoCheck col-sm-4">
                                <input name="flagCambioZonaClientes" id="flagCambioZonaClientes" type="checkbox" class="check" {{$visita->getValue('_flagCambioZonaClientes')== 1?'checked':''}}>¿Cambió la zona de clientes?
                            </label>
                            <div class="col-sm-6">
                                <button data-toggle="modal" data-target="#modalZonaClientes" type="button" class="btn btn-primary" id="btnZonasClientes" disabled>Lista de Zonas de clientes</button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        @if ($cliente->getValue('_backlog') == 'Proyectos')
                        <ul class="list-unstyled msg_list">
                            <li><div class="checkbox grupoCheck">
                                    <label class="formatoCheck col-md-3">
                                        <input name="flagBacklog" id="flagBacklog" type="checkbox" class="check" {{$visita->getValue('_flagCambioBacklog')== 1?'checked':''}}>Backlog Proyectos
                                    </label>
                                    <div class="col-sm-9">
                                        <label class="formatoCheck">¿Cambió la política de compras?
                                            <select class="form-control" name="gestionesCompra" value="{{$cliente->getValue('_gestionesCompra')}}" disabled>
                                                <option value="">--Elegir Opción--</option>
                                                @foreach($gestionesCompra as $b)
                                                <option value="{{$b}}" {{($cliente->getValue('_gestionesCompra')==$b)?'selected=selected':''}}>{{$b}}
                                                </option>
                                                @endforeach
                                            </select>
                                        </label>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        @endif
                    </div>

                    <div class="col-md-6 col-sm-12">
                        <label>¿El cliente ha realizado inversiones de por lo menos 50% del activo fijo o del patrimonio?:</label>
                        <div class="row">
                            <div class="col-sm-2">
                                <select class="form-control" name="flagCambioInversionActivoPatrimonio">
                                    <option value="0">No</option>
                                    <option value="1" {{$visita->getValue('_flagCambioInversionActivoPatrimonio')== 1?'selected':''}}>Sí</option>
                                </select>
                            </div>
                            <div class="col-sm-10">
                                <textarea class="form-control" rows="3" name="cambioInversionActivoPatrimonio">{{$visita->getValue('_cambioInversionActivoPatrimonio')}}</textarea>
                                <p id="lblCambioInversionActivoPatrimonio" class="hidden">Explicar e indicar si esto genero un cambio de modelo de negocio actual</p>
                            </div>
                        </div>

                        <label for="fullname" style="padding-top: 10px;">¿El cliente ha realizado préstamos al accionista o a empresa vinculadas de por lo menos el 15% del activo en el último ejercicio?</label>
                        <div class="row">
                            <div class="col-sm-2">
                                <select class="form-control" name="flagCambioPrestamoDesvio">
                                    <option value="0">No</option>
                                    <option value="1" {{$visita->getValue('_flagCambioPrestamoDesvio')== 1?'selected':''}}>Sí</option>
                                </select>
                            </div>
                            <div class="col-sm-10">
                                <textarea class="form-control" rows="3" name="cambioPrestamoDesvio">{{$visita->getValue('_cambioPrestamoDesvio')}}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ZONA DE MIX Y MODELO DE NEGOCIO -->
    <div class="row">
        <div id="panelModeloNegocio" class="col-sm-6 col-xs-12 hidden">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Modelo de Negocio</h2>
                    <ul class="nav navbar-right panel_toolbox">
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content" style="min-height: 150px" > 
                    <div class="form-group">
                        <label class="control-label col-sm-3 col-xs-12">Modelo de Negocio
                            <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top"  data-original-title="Descripción o detalle del cómo la empresa crea y entrega valor"></i>
                        </label>
                        <div class="col-sm-9 col-xs-12">
                            <textarea class="form-control" rows="3" name="modeloNegocio">{{$cliente->getValue('_modeloNegocio')}}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3 col-xs-12">Ventaja Competitiva
                            <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top"  data-original-title="Señalar la principal ventaja competitiva en su sector"></i>
                        </label>
                        <div class="col-sm-9 col-xs-12">
                            <textarea    class="form-control" rows="3" name="ventajaCompetitiva">{{$cliente->getValue('_ventajaCompetitiva')}}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3 col-xs-12">Fortalezas y Riesgos
                            <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top"  data-original-title="Señalar principales fortalezas y riesgos"></i>
                        </label>
                        <div class="col-sm-9 col-xs-12">
                            <textarea class="form-control" rows="3" name="fortalezasRiesgos">{{$cliente->getValue('_fortalezasRiesgos')}}</textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="panelMixVentas" class="col-sm-6 col-xs-12 hidden">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Mix de Ventas</h2>
                    <ul class="nav navbar-right panel_toolbox">
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content" style="min-height: 150px" > 
                    @for ($i=0;$i<3;$i++)
                    <div class="row">
                        <div class="form-group col-sm-2"></div>
                        <div class="form-group col-sm-4">
                            <input class="form-control inputMixVentas" type="text" name="mixVenta[{{$i}}][productoServicio]" maxlength="50" placeholder="Producto/Servicio" value="{{isset($cliente->getValue('_mixVentas')[$i])?$cliente->getValue('_mixVentas')[$i]->getValue('_productoServicio'):''}}">
                        </div>

                        <div class="form-group col-sm-4"> 
                            <select class="form-control inputMixVentas" name="mixVenta[{{$i}}][participacion]" required>
                                @foreach ($participacion as $key => $par)
                                <!--Añadir la opción de selected-->
                                <option value="{{$key}}"
                                        {{isset($cliente->getValue('_mixVentas')[$i]) && $cliente->getValue('_mixVentas')[$i]->getValue('_participacion') == $key ?'selected':''}}>
                                    {{$par}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    @endfor
                </div>
            </div>
        </div>
    </div>

    <!-- ZONA DE CLIENTES Y PROVEEDORES -->
    <div class="row">
        <div class="col-sm-12">
            <!--PROVEEDORES-->
            <div class="x_panel">
                <div class="x_title">
                    <h2>Proveedores</h2>
                    <ul class="nav navbar-right panel_toolbox">
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-2"><b>RUC</b></div>
                        <div class="col-sm-2"><b>Nombre</b></div>
                        <div class="col-sm-2"><b>Particip.</b></div>
                        <div class="col-sm-1"><b>Exclusiv.</b></div>
                        <div class="col-sm-1"><b>Año</b></div>
                        <div class="col-sm-1"><b>Contrato?</b></div>
                        <div class="col-sm-2"><b>Fec. Vec. Contrato</b></div>
                        <div class="col-sm-1"><b>Adjunto</b></div>
                    </div>
                    @for($i=0;$i<5;$i++)
                    <div class="row">
                        <div class="form-group col-sm-2"> 
                            <input class="form-control inputConcentProv" type="text" name="proveedor[{{$i}}][documento]" maxlength="11" value="{{isset($cliente->getValue('_proveedores')[$i])?$cliente->getValue('_proveedores')[$i]->getValue('_documento'):''}}" disabled>
                        </div>
                        <div class="form-group col-sm-2"> 
                            <input class="form-control inputConcentProv Prokey" type="text" name="proveedor[{{$i}}][nombre]" maxlength="50" value="{{isset($cliente->getValue('_proveedores')[$i])?$cliente->getValue('_proveedores')[$i]->getValue('_nombre'):''}}" disabled>
                        </div>
                        <div class="form-group col-sm-2"> 
                            <select class="form-control inputConcentProv" name="proveedor[{{$i}}][participacion]" disabled>
                                @foreach ($participacion as $key => $par)
                                <option value="{{$key}}"
                                        {{isset($cliente->getValue('_proveedores')[$i]) && $cliente->getValue('_proveedores')[$i]->getValue('_concentracion') == $key ?'selected = "selected"':''}}
                                    >{{$par}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-1">
                            <input class="inputConcentProv" type="checkbox" disabled
                                   name="proveedor[{{$i}}][exclusividad]" 
                                   {{isset($cliente->getValue('_proveedores')[$i]) && $cliente->getValue('_proveedores')[$i]->getValue('_exclusividad') == 1 ?'checked':''}}>
                        </div>
                        <div class="form-group col-sm-1"> 
                            <input class="form-control inputConcentProv" type="text" name="proveedor[{{$i}}][desde]" maxlength="4" value="{{isset($cliente->getValue('_proveedores')[$i])?$cliente->getValue('_proveedores')[$i]->getValue('_desde'):''}}" disabled>
                        </div>
                        <div class="form-group col-sm-1">
                            <input  type="checkbox" class="" 
                                    name="proveedor[{{$i}}][flgContrato]"  
                                    {{isset($cliente->getValue('_proveedores')[$i]) && $cliente->getValue('_proveedores')[$i]->getValue('_flgContrato') == 1 ?'checked':''}}>
                        </div>
                        <div class="form-group col-sm-1">
                            <input class="form-control dfecha" type="text" name="proveedor[{{$i}}][contratofechaVencimiento]" 
                                   value="{{isset($cliente->getValue('_proveedores')[$i])?$cliente->getValue('_proveedores')[$i]->getValue('_contratofechaVencimiento'):''}}">
                        </div>
                        <div class="form-group col-sm-2">
                            @if (isset($cliente->getValue('_proveedores')[$i]) && $cliente->getValue('_proveedores')[$i]->getValue('_contratoAdjunto'))
                            <?php
                                $nombre=$cliente->getValue('_proveedores')[$i]->getValue('_nombre');
                                $codunico=$cliente->getValue('_proveedores')[$i]->getValue('_codunico');
                                $nom_arch=$cliente->getValue('_proveedores')[$i]->getValue('_contratoAdjunto');
                                if ($nombre && $codunico && $nom_arch) {
                                    $ruta="infinity/stakeholders/proveedores/$codunico/$nombre/$nom_arch";
                                }
                                // dd(storage_path('app/'.str_replace('|','/',$ruta)));
                                // dd(file(storage_path('app/'.str_replace('|','/',$ruta))));die();
                            ?>
                            <a href="{{route('download', ['file' => str_replace('/','|',$ruta)])}}" ><i class="fa fa-download fa-2x" ></i></a>
                            @endif
                            <input type="file" name="proveedor[{{$i}}][adjuntodeProveedores]" class="form-control image inputConcentProv" style="border-style: none" value="{{isset($cliente->getValue('_proveedores')[$i])?$cliente->getValue('_proveedores')[$i]->getValue('_contratoAdjunto'):''}}" disabled>
                            <!-- <input type="file" id="exampleInputFile"> !-->
                        </div>
                    </div>
                    @endfor
                </div>
            </div>
        </div>

        <div class="col-sm-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Clientes</h2>
                    <ul class="nav navbar-right panel_toolbox">
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content" style="min-height: 150px">
                    <div class="row">
                        <div class="col-sm-2"><b>RUC</b></div>
                        <div class="col-sm-2"><b>Nombre</b></div>
                        <div class="col-sm-2"><b>Particip.</b></div>
                        <div class="col-sm-1"><b>Año</b></div>
                        <div class="col-sm-1"><b>Contrato?</b></div>
                        <div class="col-sm-2"><b>Fec. Vec. Contrato</b></div>
                        <div class="col-sm-2"><b>Adjunto</b></div>
                    </div>
                    @for($i=0;$i<5;$i++)
                    <div class="row">
                        <div class="form-group col-sm-2">
                            <input class="form-control inputConcentCli" type="text" name="cliente[{{$i}}][documento]" maxlength="11" disabled="disabled" value="{{isset($cliente->getValue('_clientes')[$i])?$cliente->getValue('_clientes')[$i]->getValue('_documento'):''}}">
                        </div>
                        <div class="form-group col-sm-2"> 
                            <input class="form-control inputConcentCli Clikey" type="text" name="cliente[{{$i}}][nombre]" maxlength="50" disabled="disabled" value="{{isset($cliente->getValue('_clientes')[$i])?$cliente->getValue('_clientes')[$i]->getValue('_nombre'):''}}">
                        </div>
                        <div class="form-group col-sm-2"> 
                            <select class="form-control inputConcentCli" name="cliente[{{$i}}][participacion]" disabled="disabled">
                                @foreach ($participacion as $key => $par)
                                <option value="{{$key}}"
                                        {{isset($cliente->getValue('_clientes')[$i]) && $cliente->getValue('_clientes')[$i]->getValue('_concentracion') == $key ?'selected = "selected"':''}}
                                    >{{$par}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-1"> 
                            <input class="form-control inputConcentCli " type="text" name="cliente[{{$i}}][desde]" maxlength="4" disabled="disabled"
                                   value="{{isset($cliente->getValue('_clientes')[$i])?$cliente->getValue('_clientes')[$i]->getValue('_desde'):''}}" >
                        </div>
                        <div class="form-group col-sm-1">
                            <input  type="checkbox"  
                                    name="cliente[{{$i}}][flgContrato]"  {{isset($cliente->getValue('_clientes')[$i]) && $cliente->getValue('_clientes')[$i]->getValue('_flgContrato') == 1 ?'checked':''}}>
                        </div>
                        <div class="form-group col-sm-2">
                            <input class="form-control dfecha" type="text" name="cliente[{{$i}}][contratofechaVencimiento]" value="{{isset($cliente->getValue('_clientes')[$i])?$cliente->getValue('_clientes')[$i]->getValue('_contratofechaVencimiento'):''}}">
                        </div>
                        <div class="form-group col-sm-2">
                            @if (isset($cliente->getValue('_clientes')[$i]) && $cliente->getValue('_clientes')[$i]->getValue('_contratoAdjunto'))
                            <?php
                                $nombre=$cliente->getValue('_clientes')[$i]->getValue('_nombre');
                                $codunico=$cliente->getValue('_clientes')[$i]->getValue('_codunico');
                                $nom_arch=$cliente->getValue('_clientes')[$i]->getValue('_contratoAdjunto');
                                if ($nombre && $codunico && $nom_arch) {
                                    $ruta="infinity/stakeholders/clientes/$codunico/$nombre/$nom_arch";
                                }
                            ?>
                            <a href="{{route('download', ['file' => str_replace('/','|',$ruta)])}}" ><i class="fa fa-download fa-2x" ></i></a>
                            @endif
                            <input type="file" name="cliente[{{$i}}][adjuntodeClientes]" class="form-control image inputConcentCli" style="border-style: none" value="{{isset($cliente->getValue('_clientes')[$i])?$cliente->getValue('_clientes')[$i]->getValue('_contratoAdjunto'):''}}" disabled>
                            <!-- <input type="file" id="exampleInputFile"> !-->
                        </div>
                    </div>
                    @endfor
                </div>
            </div>
        </div>
    </div>

    <!-- Accionistas, FINANCIAMIENTOS y PROYECCION -->
    <div class="row">

        <!-- ACCIONISTAS-->
        <div class="col-md-6">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Accionistas</h2>
                    <ul class="nav navbar-right panel_toolbox">
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content form-horizontal">
                    <div class="grupoCheck">
                        <div class="checkbox col-md-12"  style="padding-bottom: 10px">
                            <label class="formatoCheck col-md-5 col-sm-6">
                                <input name="flagCambioGerenciaGeneral" id="flagCambioGerenciaGeneral" type="checkbox" class="check"
                                       {{$visita->getValue('_flagCambioGerenciaGeneral') == '1'?'checked':''}}
                                >¿Cambió en Gerencia General? 
                            </label>
                            <div class="col-md-4 col-sm-6">
                                <input type="number" min="1900" max="2021" class="form-control"  name="cambioGerenciaGeneralAnnio" placeholder="año" value="{{$cliente->getValue('_cambioGerenciaGeneralAnnio')}}" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3 col-xs-12">G. General</label>
                            <div class="col-sm-9 col-xs-12">
                                <input type="text" class="form-control" name="gerenteGeneral" value="{{$cliente->getValue('_gerenteGeneral')}}" disabled>
                            </div>
                        </div>
                    </div>

                    <div class="grupoCheck">
                        <div class="checkbox"  style="padding-bottom: 10px">
                            <label class="formatoCheck">
                                <input name="flagCambioGestionFinanciera" id="flagCambioGestionFinanciera" class="check" type="checkbox"
                                       {{$visita->getValue('_flagCambioGestionFinanciera') == '1'?'checked':''}}       
                                >¿Cambió en Gestión Financiera? 
                            </label>                    
                        </div>

                        <div class="form-group">
                            <div class="col-sm-3 col-xs-12">
                                <select class="form-control" name="financieroRol" value="{{$cliente->getValue('_financieroRol')}}" disabled>
                                    <option>--Elegir Opción--</option>
                                    @foreach($financieroRol as $b)
                                    <option value="{{$b}}" {{($cliente->getValue('_financieroRol')==$b)?'selected=selected':''}}>{{$b}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-9 col-xs-12">
                                <input type="text" class="form-control" name="financieroNombre" value="{{$cliente->getValue('_financieroNombre')}}" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3 col-xs-12">Contabilidad</label>
                            <div class="col-sm-9 col-xs-12">
                                <select class="form-control" name="tipoContabilidad" value="{{$cliente->getValue('_tipoContabilidad')}}" disabled>
                                    <option >No aplica</option>
                                    <option value="Interna" {{($cliente->getValue('_tipoContabilidad')=='Interna')?'selected=selected':''}}>Interna</option>
                                    <option value="Externa" {{($cliente->getValue('_tipoContabilidad')=='Externa')?'selected=selected':''}}>Externa</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="grupoCheck">
                        <div class="checkbox col-md-12"  style="padding-bottom: 10px">
                            <label class="formatoCheck col-md-5">
                                <input name="flagCambioAccionistas" class="check" id="flagCambioAccionistas" type="checkbox"
                                       {{$visita->getValue('_flagCambioAccionistas') == '1'? 'checked':''}}
                                >¿Cambió en accionistas? 
                            </label>
                            <label style="display: flex;" class="formatoCheck col-md-5">
                                Año:
                                <input style="margin-left:10px;" type="number" min="1900" max="2021" class="form-control"  name="cambioAccionistasAnnio" onkeypress="return isNumeric(event)" oninput="maxLengthCheckYear(this)" value="{{$cliente->getValue('_cambioAccionistasAnnio')}}" disabled>
                            </label>
                        </div> 
                        <div class="row">
                            <div class="col-sm-3"><b>DNI</b></div>
                            <div class="col-sm-4"><b>Accionista</b></div>
                            <div class="col-sm-3"><b>Particip.</b></div>
                            <div class="col-sm-2"><b>Año de Nacimiento</b></div>
                        </div>
                        @for($i=0;$i<5;$i++)
                        <div class="row">
                            <div class="form-group col-sm-3"> 
                                <input class="form-control" type="text" name="accionista[{{$i}}][documento]" maxlength="11" value="{{isset($cliente->getValue('_accionistas')[$i])?$cliente->getValue('_accionistas')[$i]->getValue('_documento'):''}}" disabled>
                            </div>
                            <div class="form-group col-sm-4"> 
                                <input class="form-control" type="text" name="accionista[{{$i}}][nombre]" maxlength="50" value="{{isset($cliente->getValue('_accionistas')[$i])?$cliente->getValue('_accionistas')[$i]->getValue('_nombre'):''}}" disabled>
                            </div>
                            <div class="form-group col-sm-3"> 
                                <select class="form-control" name="accionista[{{$i}}][participacion]" disabled>
                                    @foreach ($participacion as $key => $par)
                                    <option value="{{$key}}"
                                            {{isset($cliente->getValue('_accionistas')[$i]) && $cliente->getValue('_accionistas')[$i]->getValue('_concentracion') == $key ?'selected = "selected"':''}}
                                        >{{$par}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-sm-2"> 
                                <input class="form-control" type="number"  name="accionista[{{$i}}][nacimiento]" max="2021" min="1900" onkeypress="return isNumeric(event)" oninput="maxLengthCheckYear(this)" value="{{isset($cliente->getValue('_accionistas')[$i])?$cliente->getValue('_accionistas')[$i]->getValue('_nacimiento'):''}}" disabled>
                            </div>
                        </div>
                        @endfor
                    </div>

                    <!--
                        <div class="checkbox col-md-12 grupoCheck"  style="padding-bottom: 10px">
                            <label class="formatoCheck col-md-3">
                                <input name="flgLineaSucesion" id="flgLineaSucesion" type="checkbox" class="check">Línea de sucesión 
                            </label>
                            <div class="col-md-4">
                                
                                <select class="form-control" name="lineaSucesion" disabled>
                                    <option>Seleccione una opción</option>
                                    <option value="1">Linea 1</option>
                                    <option value="2">Linea 2</option>
                                    <option value="3">Linea 3</option>
                                </select>
                            </div>
                        </div> 
                    -->
                </div>
            </div>
        </div>

        <!--FINANCIAMIENTOS-->
        <div class="col-md-6">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Financiamientos</h2>
                    <ul class="nav navbar-right panel_toolbox">
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-sm-5 col-xs-12">Monto de Línea con Proveedores (Miles de PEN)</label>
                        <div class="col-sm-3 col-xs-12">
                            <input required max="999999" min="1" type="number" class="form-control" value="{{$cliente->getValue('_montoLineaProveedores')}}" name="montoLineaProveedores">
                        </div>
                    </div>

                    <div class="grupoCheck">
                        <div class="checkbox"  style="padding-bottom: 10px">
                            <label class="formatoCheck">
                                <input name="flagCambioLineas" class="check" id="flagCambioLineas" type="checkbox"
                                       {{$visita->getValue('_flagCambioLineas') == '1'? 'checked':''}}       
                                >¿Cambió en distribución de líneas bancarias? 
                            </label>                    
                        </div>

                        <div class="row">
                            <div class="col-sm-2"><b></b></div>
                            <div class="col-sm-2"><b>Deuda</b></div>
                            <div class="col-sm-3"><b>Líneas</b></div>
                            <div class="col-sm-2"><b>Garantías</b></div>
                            <div class="col-sm-3"><b>Tipo</b></div>
                        </div>

                        @foreach ($bancos as $key => $banco)
                        <div class="row">
                            <div class="form-group col-sm-2"> 
                                <label class="control-label">{{$banco}}</label>
                            </div>
                            <div class="form-group col-sm-2"> 
                                <input type="text" class="form-control" readonly="readonly" value="{{isset($rcc['Deuda'][$key])? number_format($rcc['Deuda'][$key]->MONTO/1000,0,'.',','):0}}">
                            </div>
                            <div class="form-group col-sm-3"> 
                                <input type="number" class="form-control noActualizable" name="linea[{{$key}}][monto]" max="999999" value="{{isset($cliente->getValue('_lineas')[$key])? $cliente->getValue('_lineas')[$key]->getValue('_linea'):''}}">
                            </div>
                            <div class="form-group col-sm-2"> 
                                <input type="text" class="form-control" readonly="readonly" value="{{isset($rcc['Garantia'][$key])? number_format($rcc['Garantia'][$key]->MONTO/1000,0,'.',','):0}}">
                            </div>
                            <div class="form-group col-sm-3"> 
                                <select class="form-control noActualizable" name="linea[{{$key}}][tipoGarantia]">
                                    <option value="">Seleccionar</option>
                                    @foreach ($tiposGarantia as $tipoGarantia)
                                    <option value="{{$tipoGarantia}}"
                                            {{isset($cliente->getValue('_lineas')[$key]) && $cliente->getValue('_lineas')[$key]->getValue('_tipoGarantia') == $tipoGarantia? 'selected':''}}
                                        >{{$tipoGarantia}}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        @endforeach

                        <div class="checkbox"  style="padding-bottom: 10px">
                            <label class="formatoCheck">
                                <input name="flgGravamen" id="flgGravamen" type="checkbox" disabled {{$cliente->getValue('_activoLibreGravamen') == 1?'checked':''}}>¿Cuenta con activos libre de gravamen? 
                            </label>
                        </div> 
                    </div>
                </div>
            </div>
        </div>

        <!--PROYECCION-->
        <div class="col-md-6">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Proyección</h2>
                    <ul class="nav navbar-right panel_toolbox">
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="form-group col-sm-12" >
                        <label class="control-label col-md-6 col-sm-6">Proyección de Inversiones (Miles de PEN) Año 2021</label>
                        <div class="col-md-4 col-sm-4">
                            <input type="number" class="form-control" id="inputProyeccionInversion" name="proyeccionInversion" required min="0" max="999999" value="{{$cliente->getValue('_proyeccionInversion')}}">
                        </div>
                        <div class="col-md-1 col-sm-1">
                            <i class="fa fa-question-circle"></i>
                        </div>
                        <div class="col-md-1 col-sm-1 {{$cliente->getValue('_proyeccionInversion')>0?'':'hidden'}}" id="btnInversion" style="cursor: pointer;"  >
                            <img src = "{{ URL::asset('img/click.gif') }}" style="width: 40px;height: 40px">
                            <input  style="opacity:0;position:absolute" 
                            {{$cliente->getValue('_proyeccionInversion')!=null?$cliente->getValue('_proyeccionInversion')>1?'required':'disabled':'disabled'}} max="8" min="1" type="number" class="form-control" name="tipoInversionV" id="tipoInversionV" value="{{$cliente->getValue('_inversiones')!=null?count($cliente->getValue('_inversiones')):0}}">
                        </div>
                    </div>

                    <!-- <div class="form-group col-sm-12">
                        <label class="control-label col-md-6 col-sm-6">Proyección de Ventas (Miles de PEN) Año 2021</label>
                        <div class="col-md-4 col-sm-4">
                            <input required max="999999" min="1" type="number" class="form-control" name="proyeccionVentas" value="{{$cliente->getValue('_proyeccionVentas')}}">
                        </div>
                        <div class="col-md-2 col-sm-2">
                            <i class="fa fa-question-circle"></i>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>

    </div>
                    
    <div class="row">
        <div class="col-md-12">
              <div class="x_panel">
              <div class="x_title">
                    <h2><strong>FICHA: COVID-19</strong></h2>
                    <ul class="nav navbar-right panel_toolbox">
                    </ul>
                    <div class="clearfix"></div>
                </div>
              <input style="height:0; visibility:hidden" type="text" name="registro" id="registro" value="{{isset($datoscovid)?$datoscovid->ESTADO_APROBACION=='0'?$datoscovid->USUARIO_CREACION:'':''}}"> 
                <div style="height:40px;"></div>
                {{--  Pregunta1  --}}
                <div class="form-row row">
                    <div class="form-group col-md-8">
                    <label>1. Indicar si la pandemia COVID-19, ha tenido un impacto negativo en la gestión:</label>
                    </div>
                    <div class="form-group col-md-2">
                    </div>
                    <div class="form-group col-md-2">
                    <select id="slcQ1" name="slcQ1" class="form-control custom-select is-invalid cerrado" required>
                        <option {{isset($datoscovid) && !empty($datoscovid->PREGUNTA_1)?'' : 'selected'}} disabled value="">-Seleccionar-</option>
                        <option value="1" {{isset($datoscovid) && $datoscovid->PREGUNTA_1=='1' ?'selected':''}}>Sí</option>
                        <option value="2" {{isset($datoscovid) && $datoscovid->PREGUNTA_1=='2' ?'selected':''}}>No</option>
                    </select>
                    </div>
                </div>
                <div style="height:40px;"></div>

                {{--  Pregunta2  --}}
                <div class="form-row row">
                    <div class="form-group col-md-6">
                        <label>2. ¿Cuánto vendiste el último trimestre 2019 vs comparativo del último trimestre 2020? <br>
                        <strong style="color:black">(*) IMPORTANTE: Validar información del 2019 en el reporte tributario</strong></label>
                    </div>
                    <div class="form-group col-md-2">
                        <label style="font-weight: 100;">2019 Miles US$</label><input id="slc2Q2_1" name="slc2Q2_1" type="number" class="form-control cerrado"  onkeypress="return isNumeric(event)" oninput="maxLengthMontoCheck(this)" required value="{{isset($datoscovid)?(isset($datoscovid->PREGUNTA2_2_1)?$datoscovid->PREGUNTA2_2_1:''):''}}" min="0">
                    </div>
                    <div class="form-group col-md-2">
                        <label style="font-weight: 100;">2020 Miles US$</label><input id="slc2Q2_2" name="slc2Q2_2" type="number" class="form-control cerrado"  onkeypress="return isNumeric(event)" oninput="maxLengthMontoCheck(this)" required value="{{isset($datoscovid)?(isset($datoscovid->PREGUNTA2_2_2)?$datoscovid->PREGUNTA2_2_2:''):''}}" min="0">
                    </div>
                    <div class="form-group col-md-2" style="display:flex;flex-direction:column;align-items: center;">
                        <label style="font-weight: 100;">¿Monto real 2020?</label>
                        <input class="form-check-input cerrado slc2Q2_3" type="checkbox" id="slc2Q2_3" name="slc2Q2_3"  
                        {{isset($datoscovid) && isset($datoscovid->PREGUNTA2_2_3) && $datoscovid->PREGUNTA2_2_3 =='1' ?'checked':''}}>
                    </div>
                </div>
                <div style="height:40px;"></div>
                {{--  Pregunta3  --}}
                <div class="form-row row">
                    <div class="form-group col-md-6">
                        <label for="lblQ5">3. ¿Cuánto se redujeron sus costos y gastos fijos para el 2020 y cuál es tu proyectado para este año?</label>
                    </div>
                    <div class="form-group col-md-2">
                    </div>
                    <div class="form-group col-md-2">
                        <label style="font-weight: 100;">2020 Miles US$</label><input id="slc2Q3_1" name="slc2Q3_1" type="number" class="form-control cerrado"  onkeypress="return isNumeric(event)" oninput="maxLengthMontoCheck(this)" required value="{{isset($datoscovid)?(isset($datoscovid->PREGUNTA2_3_1)?$datoscovid->PREGUNTA2_3_1:''):''}}" min="0">
                    </div>
                    <div class="form-group col-md-2">
                        <label style="font-weight: 100;">Proyectado Miles US$</label><input id="slc2Q3_2" name="slc2Q3_2" type="number" class="form-control cerrado"  onkeypress="return isNumeric(event)" oninput="maxLengthMontoCheck(this)" required value="{{isset($datoscovid)?(isset($datoscovid->PREGUNTA2_3_2)?$datoscovid->PREGUNTA2_3_2:''):''}}" min="0">
                    </div>
                </div>

                <!-- <div class="form-row row">
                    <div class="form-group col-md-6">
                    <label> 2. ¿Se encuentra actualmente operando?</label>
                    </div>
                    <div class="form-group col-md-1"></div>
                    <div class="form-group col-md-2">
                    <select id="slcQ2_1" name="slcQ2_1" class="form-control custom-select is-invalid cerrado" required>
                        <option {{isset($datoscovid) && !empty($datoscovid->PREGUNTA_2_1)?'' : 'selected'}} disabled value="">-Seleccionar-</option>
                        <option value="1" {{isset($datoscovid) && $datoscovid->PREGUNTA_2_1=='1' ?'selected':''}}>Sí</option>
                        <option value="2" {{isset($datoscovid) && $datoscovid->PREGUNTA_2_1=='2' ?'selected':''}}>No</option>
                        </select>
                    </div>
                    <div class="form-group col-md-3">
                    </div>
                </div>
                <div class="form-row row">
                    <div class="form-group col-md-6">
                        <label>&nbsp;&nbsp; ¿Se acogió a la suspensión perfecta de labores?</label>
                        </div>
                        <div class="form-group col-md-1"></div>
                        <div class="form-group col-md-2">
                        <select id="slcQ2_2" name="slcQ2_2" class="form-control custom-select is-invalid cerrado" required>
                            <option {{isset($datoscovid) && !empty($datoscovid->PREGUNTA_2_2)?'' : 'selected'}} disabled value="">-Seleccionar-</option>
                            <option value="1" {{isset($datoscovid) && $datoscovid->PREGUNTA_2_2=='1' ?'selected':''}}>Sí</option>
                            <option value="2" {{isset($datoscovid) && $datoscovid->PREGUNTA_2_2=='2' ?'selected':''}}>No</option>
                            </select>
                        </div>
                    <div class="form-group col-md-3">
                    </div>
                </div> -->
                <!-- OBSERVACION: SI INDICA QUE SI SE DEBE ESPECIFICAR EL PORCENTAJE-->
                <!-- <div class="form-row row">
                    <div class="form-group col-md-6">
                    <label>&nbsp;&nbsp; ¿Redujo el personal?</label>
                    </div>
                    <div class="form-group col-md-1"></div>
                    <div class="form-group col-md-2">
                    <select onchange="show('DIV2',this)" id="slcQ2_3_1" name="slcQ2_3_1" class="form-control custom-select is-invalid cerrado" required>
                        <option {{isset($datoscovid) && !empty($datoscovid->PREGUNTA_2_3_1)?'' : 'selected'}} disabled value="">-Seleccionar-</option>
                        <option value="1" {{isset($datoscovid) && $datoscovid->PREGUNTA_2_3_1=='1' ?'selected':''}}>Sí</option>
                        <option value="2" {{isset($datoscovid) && $datoscovid->PREGUNTA_2_3_1=='2' ?'selected':''}}>No</option>
                    </select>
                    </div>
                    <div style="{{isset($datoscovid) && $datoscovid->PREGUNTA_2_3_1=='1' ?'':'display:none'}}" identificador="DIV2_1" class="form-group col-md-1 DIV2">
                        <label style="font-weight: 100;margin-top: 5px;">Porc %</label>
                    </div>
                    <div style="{{isset($datoscovid) && $datoscovid->PREGUNTA_2_3_1=='1' ?'':'display:none'}}" identificador="DIV2_1" class="form-group col-md-2 DIV2">
                        <input id="slcQ2_3_2" name="slcQ2_3_2" onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)" type="number" min="0" max="100" class="form-control cerrado" value="{{isset($datoscovid)?$datoscovid->PREGUNTA_2_3_2:''}}">
                    </div>
                </div> -->
                <div style="height:40px;"></div>
                {{--  Pregunta4  --}}
                <div class="form-row row">
                    <div class="form-group col-md-8">
                        <label>4. ¿Logró tener generación Ebitda Positiva? <br>
                         Si la respuesta es afirmativa, ¿Qué estrategias hizo y/o realizará para continuar con estas eficiencias?:</label>
                    </div>
                    <div class="form-group col-md-2">
                    </div>
                    <div class="form-group col-md-2">
                        <select onchange="show('DIV2.4',this)" id="slc2Q4" name="slc2Q4" class="form-control custom-select is-invalid cerrado" onchange="" required>
                            <option {{isset($datoscovid) && !empty($datoscovid->PREGUNTA2_4) && isset($datoscovid->PREGUNTA2_4)?'' : 'selected'}} disabled value="">-Seleccionar-</option>
                            <option value="0" {{isset($datoscovid) && isset($datoscovid->PREGUNTA2_4) && $datoscovid->PREGUNTA2_4=='0'?'selected' : ''}}>No</option>
                            <option value="1" {{isset($datoscovid) && isset($datoscovid->PREGUNTA2_4) && $datoscovid->PREGUNTA2_4=='1'?'selected' : ''}}>Si</option>
                        </select>
                    </div>
                </div>
                {{--  Pregunta4-1 --}}
                    <div id="divRpta7-1"  style="{{isset($datoscovid) && isset($datoscovid->PREGUNTA2_4_1) && in_array($datoscovid->PREGUNTA2_4,array('1'))?'':'display:none'}}" identificador="DIV2.4_1"  class="form-row DIV2.4">
                        <textarea maxlength="300" id="slc2Q4_1" name="slc2Q4_1" class="form-control is-invalid cerrado" placeholder="Ingrese sus comentarios aquí." style="width:100%; height:81px;">{{isset($datoscovid)?(isset($datoscovid->PREGUNTA2_4_1)?$datoscovid->PREGUNTA2_4_1:''):''}}</textarea>
                    </div>
                <div style="height:40px;"></div>
                {{--  Pregunta5  --}}
                <div class="form-row row">
                    <div class="form-group col-md-12">
                    <label>5. Asociado al nivel de actividad, comentar sobre el Incremento de deuda vs nivel ventas en los últimos doce meses:</label>
                    </div>
                </div>

                <div class="form-row row">
                    <div class="form-group col-md-12">
                    <textarea maxlength="300" id="slc2Q5" name="slc2Q5" class="form-control is-invalid cerrado" placeholder="Ingrese sus comentarios aquí." style="width:100%; height:81px;">{{isset($datoscovid)?(isset($datoscovid->PREGUNTA2_5)?$datoscovid->PREGUNTA2_5:''):''}}</textarea>
                    </div>
                </div>
                <div style="height:40px;"></div>
                {{--  Pregunta6  --}}
                <div class="form-row row">
                    <div class="form-group col-md-8">
                    <label>6.Tuvo reactiva si o no</label>
                    </div>
                    <div class="form-group col-md-2">
                    </div>
                    <div class="form-group col-md-2">
                    <select id="slc2Q6" name="slc2Q6" class="form-control custom-select is-invalid cerrado" required>
                        <option {{isset($datoscovid) && !empty($datoscovid->PREGUNTA2_6)?'' : 'selected'}} disabled value="">-Seleccionar-</option>
                        <option value="1" {{isset($datoscovid) && isset($datoscovid->PREGUNTA2_6) && $datoscovid->PREGUNTA2_6 =='1' ?'selected':''}}>Sí</option>
                        <option value="2" {{isset($datoscovid) && isset($datoscovid->PREGUNTA2_6) && $datoscovid->PREGUNTA2_6 =='2' ?'selected':''}}>No</option>
                    </select>
                    </div>
                </div>
                {{--  Pregunta7  --}}
                <div class="form-row row">
                    <div class="form-group col-md-12">
                    <label>7.Considerando el nivel de deuda que mantiene a la fecha ¿Cómo afrontará los medianos y largos plazos vigentes? Si recibió REACTIVA, ¿Cómo piensa pagar el reactiva?:</label>
                    </div>
                </div>

                <div class="form-row row">
                    <div class="form-group col-md-12">
                    <textarea maxlength="300" id="slc2Q7" name="slc2Q7" class="form-control is-invalid cerrado" placeholder="Ingrese sus comentarios aquí." style="width:100%; height:81px;">{{isset($datoscovid)?(isset($datoscovid->PREGUNTA2_7)?$datoscovid->PREGUNTA2_7:''):''}}</textarea>
                    </div>
                </div>
                <div style="height:40px;"></div>
                {{--  Pregunta8  --}}
                <div class="form-group col-md-8">
                        <label>8. Teniendo en cuenta lo antes mencionado ¿Cuál es la proyección de deuda al cierre del año? <br><strong style="color:black"> (*)Importante: Llenar con información de reporte tributario </strong></label>
                    </div>
                    <div class="form-group col-md-2"></div>
                    <div class="form-group col-md-2">
                        <label style="font-weight: 100;">Proyectado Cierre 2021 Miles US$</label><input id="slc2Q8" name="slc2Q8" type="number" class="form-control cerrado"  onkeypress="return isNumeric(event)" oninput="maxLengthMontoCheck(this)" required value="{{isset($datoscovid)?(isset($datoscovid->PREGUNTA2_8)?$datoscovid->PREGUNTA2_8:''):''}}" min="0">
                    </div>
                {{--  Pregunta9  --}}
                <div class="form-row row">
                    <div class="form-group col-md-6">
                        <label>9. Indicar el promedio mensual de ventas del 2019, 2020 y el proyectado al 2021</label>
                    </div>
                    <div class="form-group col-md-2">
                        <label style="font-weight: 100;">2019 Miles US$</label><input id="slc2Q9_1" name="slc2Q9_1" type="number" class="form-control cerrado"  onkeypress="return isNumeric(event)" oninput="maxLengthMontoCheck(this)" required value="{{isset($datoscovid)?(isset($datoscovid->PREGUNTA2_9_1)?$datoscovid->PREGUNTA2_9_1:''):''}}" min="0">
                    </div>
                    <div class="form-group col-md-2">
                        <label style="font-weight: 100;">2020 Miles US$</label><input id="slc2Q9_2" name="slc2Q9_2" type="number" class="form-control cerrado"  onkeypress="return isNumeric(event)" oninput="maxLengthMontoCheck(this)" required value="{{isset($datoscovid)?(isset($datoscovid->PREGUNTA2_9_2)?$datoscovid->PREGUNTA2_9_2:''):''}}" min="0">
                    </div>
                    <div class="form-group col-md-2">
                        <label style="font-weight: 100;">Proyectado 2021 Miles US$</label><input id="slc2Q9_3" name="slc2Q9_3" type="number" class="form-control cerrado"  onkeypress="return isNumeric(event)" oninput="maxLengthMontoCheck(this)" required value="{{isset($datoscovid)?(isset($datoscovid->PREGUNTA2_9_3)?$datoscovid->PREGUNTA2_9_3:''):''}}" min="0">
                    </div>
                </div>
                {{--  Pregunta10  --}}
                <div class="form-row row">
                    <div class="form-group col-md-12">
                    <label>10. ¿Qué EXPECTATIVAS tiene sobre el nivel de ventas en el primer SEMESTRE del 2021?:</label>
                    </div>
                </div>

                <div class="form-row row">
                    <div class="form-group col-md-12">
                    <textarea maxlength="300" id="slc2Q10" name="slc2Q10" class="form-control is-invalid cerrado" placeholder="Ingrese sus comentarios aquí." style="width:100%; height:81px;">{{isset($datoscovid)?(isset($datoscovid->PREGUNTA2_10)?$datoscovid->PREGUNTA2_10:''):''}}</textarea>
                    </div>
                </div>
                <div style="height:40px;"></div>
                {{--  Pregunta11  --}}
                <div class="form-row row">
                    <div class="form-group col-md-6">
                        <label for="lblQ5">11. ¿En cuánto espera proyectar el EBITDA para el 2021? </label>
                    </div>
                    <div class="form-group col-md-2">
                    </div>
                    <div class="form-group col-md-2">
                        <label style="font-weight: 100;">Margen EBITDA (%)</label><input id="slc2Q11_1"  name="slc2Q11_1" type="number" class="form-control cerrado" oninput="maxLengthCheck(this)" onkeypress="return isNumeric(event)" required value="{{isset($datoscovid)?(isset($datoscovid->PREGUNTA2_11_1)?$datoscovid->PREGUNTA2_11_1:''):''}}" min="0" max="100">
                    </div>
                    <div class="form-group col-md-2">
                        <label style="font-weight: 100;">Miles US$</label><input id="slc2Q11_2" name="slc2Q11_2" type="number" class="form-control cerrado"  onkeypress="return isNumeric(event)" oninput="maxLengthMontoCheck(this)" value="{{isset($datoscovid)?(isset($datoscovid->PREGUNTA2_11_2)?$datoscovid->PREGUNTA2_11_2:''):''}}" min="0">
                    </div>
                </div>
                {{--  Pregunta12  --}}
                <div class="form-row row">
                    <div class="form-group col-md-12">
                    <label>12. ¿En cuánto espera proyectar su deuda para el 2021 en el primer SEMESTRE? <br> 
                   <strong style="color:black">(*) IMPORTANTE: Validar si tiene caja y cómo va a pagar:</strong></label>
                    </div>
                </div>
                <div class="form-row row">
                    <div class="form-group col-md-12">
                    <textarea maxlength="300" id="slc2Q12" name="slc2Q12" class="form-control is-invalid cerrado" placeholder="Ingrese sus comentarios aquí." style="width:100%; height:81px;">{{isset($datoscovid)?(isset($datoscovid->PREGUNTA2_12)?$datoscovid->PREGUNTA2_12:''):''}}</textarea>
                    </div>
                </div>
                <div style="height:40px;"></div>
                {{--  Pregunta13  --}}
                <div class="form-row row">
                    <div class="form-group col-md-8">
                        <label>13. Confirmar si presenta problemas de stock. Si la respuesta es afirmativa, indicar qué medidas ha tomado para contar con stock.</label>
                    </div>
                    <div class="form-group col-md-2">
                    </div>
                    <div class="form-group col-md-2">
                        <select onchange="show('DIV2.13',this)" id="slc2Q13" name="slc2Q13" class="form-control custom-select is-invalid cerrado" onchange="" required>
                            <option {{isset($datoscovid) && !empty($datoscovid->PREGUNTA2_13) && isset($datoscovid->PREGUNTA2_13)?'' : 'selected'}} disabled value="">-Seleccionar-</option>
                            <option value="0" {{isset($datoscovid) && isset($datoscovid->PREGUNTA2_13) && $datoscovid->PREGUNTA2_13=='0'?'selected' : ''}}>No</option>
                            <option value="1" {{isset($datoscovid) && isset($datoscovid->PREGUNTA2_13) && $datoscovid->PREGUNTA2_13=='1'?'selected' : ''}}>Si</option>
                        </select>
                    </div>
                </div>
                {{--  Pregunta13-1 --}}
                    <div id="divRpta7-1"  style="{{isset($datoscovid) && isset($datoscovid->PREGUNTA2_13_1) && in_array($datoscovid->PREGUNTA2_13,array('1'))?'':'display:none'}}" identificador="DIV2.13_1"  class="form-row DIV2.13">
                        <textarea maxlength="300" id="slc2Q13_1" name="slc2Q13_1" class="form-control is-invalid cerrado" placeholder="Ingrese sus comentarios aquí." style="width:100%; height:81px;">{{isset($datoscovid)?(isset($datoscovid->PREGUNTA2_13_1)?$datoscovid->PREGUNTA2_13_1:''):''}}</textarea>
                    </div>
                <div style="height:40px;"></div>
                {{--  Pregunta14  --}}
                <div class="form-row row">
                    <div class="form-group col-md-12">
                    <p><strong>14. ¿Qué está haciendo con los vencimientos de obligaciones con sus proveedores? (Próximas a vencer)?</strong></p>
                    <p style="padding-left: 1%;"> (Coloque un <i class="fa fa-check-square-o" aria-hidden="true"></i>; en caso seleccionar la primera comentar las preguntas de la primera opción) 
                    <strong>(MÚLTIPLE SELECCIÓN)</strong></p>
                    </div>
                </div>

                {{--  Check1  --}}
                <div class="form-row row">
                    <div class="form-group col-md-12" style="padding-left: 3%;">
                    <input onchange="showCheckBox('DIV2.141',this)" type="checkbox" class="custom-control-input cerrado" id="slc2Q14_1" name="slc2Q14_1" {{isset($datoscovid) && isset($datoscovid->PREGUNTA2_14_1) && $datoscovid->PREGUNTA2_14_1?'checked':''}}>
                    <label class="form-check-label" for="Check1"> Actualmente tiene deuda reprogramada con proveedores.</label>
                    </div>
                </div>

                    {{--  DivCheck1-1-1 --}}
                <div identificador="DIV2.141_TRUE" style="{{isset($datoscovid)&& isset($datoscovid->PREGUNTA2_14_1) && $datoscovid->PREGUNTA2_14_1?'':'display:none'}}" id="divCheck1-1" class="form-row row DIV2.141">
                    <div class="form-group col-md-8" style="padding-left: 3%;">
                        <label style="font-weight:normal">¿A qué plazo reprogramó?</label>
                    </div>
                    <div class="form-group col-md-1">
                    </div>
                    <div class="form-group col-md-2">
                        <select id="slc2Q14_1_1_1" name="slc2Q14_1_1_1" class="slc2Q14_1 form-control custom-select is-invalid cerrado" >
                            <option {{isset($datoscovid) && !empty($datoscovid->PREGUNTA2_14_1_1_1)?'' : 'selected'}} disabled value="">-Seleccionar-</option>
                            <option value="días" {{isset($datoscovid) && isset($datoscovid->PREGUNTA2_14_1_1_1) && $datoscovid->PREGUNTA2_14_1_1_1=='días'?'selected' : ''}}>Días</option>
                            <option value="meses" {{isset($datoscovid) && isset($datoscovid->PREGUNTA2_14_1_1_1) && $datoscovid->PREGUNTA2_14_1_1_1=='meses'?'selected' : ''}}>Meses</option>
                        </select>
                    </div>
                    <div class="form-group col-md-1">
                    <input onkeypress="return isNumeric(event)"  oninput="maxLengthPlazoCheck(this)" id="slc2Q14_1_1_2" name="slc2Q14_1_1_2" type="number" class="form-control cerrado slc2Q14_1" value="{{isset($datoscovid)?(isset($datoscovid->PREGUNTA2_14_1_1_2)?$datoscovid->PREGUNTA2_14_1_1_2:''):''}}" min="0" max="999">
                    </div>
                </div>

                    {{--  DivCheck1-1-2 --}}
                <div identificador="DIV2.141_TRUE"  style="{{isset($datoscovid)&& isset($datoscovid->PREGUNTA2_14_1) && $datoscovid->PREGUNTA2_14_1?'':'display:none'}}"  id="divCheck1-2" class="form-row row DIV2.141">
                    <div class="form-group col-md-8" style="padding-left: 3%;">
                        <label style="font-weight:normal">¿Ha tenido cancelación de sus líneas?</label>
                    </div>
                    <div class="form-group col-md-1">
                    </div>
                    <div class="form-group col-md-2">
                    <select id="slc2Q14_1_2" class="form-control custom-select is-invalid cerrado slc2Q14_1" name="slc2Q14_1_2">
                        <option {{isset($datoscovid) && isset($datoscovid->PREGUNTA2_14_1_2) && !empty($datoscovid->PREGUNTA2_14_1_2)?'' : 'selected'}} disabled value="">-Seleccionar-</option>
                        <option value="1" {{isset($datoscovid) && isset($datoscovid->PREGUNTA2_14_1_2) && $datoscovid->PREGUNTA2_14_1_2=='1'?'selected' : ''}}>Sí</option>
                        <option value="2" {{isset($datoscovid) && isset($datoscovid->PREGUNTA2_14_1_2) && $datoscovid->PREGUNTA2_14_1_2=='2'?'selected' : ''}}>No</option>
                    </select>
                    </div>
                    <div class="form-group col-md-1">
                    </div>
                </div>
                {{--  DivCheck1-1-3 --}}
                <div identificador="DIV2.141_TRUE"  style="{{isset($datoscovid)&& isset($datoscovid->PREGUNTA2_14_1) && $datoscovid->PREGUNTA2_14_1?'':'display:none'}}"  id="divCheck1-2" class="form-row row DIV2.141">
                    <div class="form-group col-md-8" style="padding-left: 3%;">
                        <label style="font-weight:normal">Monto</label>
                    </div>
                    <div class="form-group col-md-2">
                        <input onkeypress="return isNumeric(event)"  oninput="maxLengthPlazoCheck(this)" id="slc2Q14_1_3" name="slc2Q14_1_3" type="number" class="form-control cerrado slc2Q14_1" value="{{isset($datoscovid)?(isset($datoscovid->PREGUNTA2_14_1_3)?$datoscovid->PREGUNTA2_14_1_3:''):''}}" min="0" max="999">
                    </div>
                    <div class="form-group col-md-2">
                    </div>
                </div>
                {{--  DivCheck1-1-4 --}}
                <div identificador="DIV2.141_TRUE"  style="{{isset($datoscovid)&& isset($datoscovid->PREGUNTA2_14_1) && $datoscovid->PREGUNTA2_14_1?'':'display:none'}}"  id="divCheck1-2" class="form-row row DIV2.141">
                    <div class="form-group col-md-8" style="padding-left: 3%;">
                        <label style="font-weight:normal">¿Necesitas reprogramar nuevas deudas o volver a reprogramarla las actuales? </label>
                    </div>
                    <div class="form-group col-md-2">
                        <select onchange="show('DIV2.14.1.4',this)" id="slc2Q14_1_4" class="form-control custom-select is-invalid cerrado slc2Q14_1" name="slc2Q14_1_4">
                            <option {{isset($datoscovid) && isset($datoscovid->PREGUNTA2_14_1_4) && !empty($datoscovid->PREGUNTA2_14_1_4)?'' : 'selected'}} disabled value="">-Seleccionar-</option>
                            <option value="1" {{isset($datoscovid) && isset($datoscovid->PREGUNTA2_14_1_4) && $datoscovid->PREGUNTA2_14_1_4=='1'?'selected' : ''}}>Reprogramar más</option>
                            <option value="2" {{isset($datoscovid) && isset($datoscovid->PREGUNTA2_14_1_4) && $datoscovid->PREGUNTA2_14_1_4=='2'?'selected' : ''}}>Volver a reprogramarla</option>
                        </select>
                    </div>
                    <div class="form-group col-md-2">
                        <input style="{{isset($datoscovid)&& isset($datoscovid->PREGUNTA2_14_1_4) && $datoscovid->PREGUNTA2_14_1_4=='1'?'':'display:none'}}" identificador="DIV2.14.1.4_1" onkeypress="return isNumeric(event)"  oninput="maxLengthPlazoCheck(this)" id="slc2Q14_1_4_1" name="slc2Q14_1_4_1" type="number" class="form-control cerrado DIV2.14.1.4" value="{{isset($datoscovid)?(isset($datoscovid->PREGUNTA2_14_1_4_1)?$datoscovid->PREGUNTA2_14_1_4_1:''):''}}" min="0" max="999">
                    </div>
                </div>
                        
                {{--  Check2  --}}
                <div class="form-row row">
                    <div class="form-group col-md-12" style="padding-left: 3%;">
                    <input type="checkbox" class="custom-control-input cerrado slc2Q14" id="slc2Q14_2" name="slc2Q14_2" {{isset($datoscovid) && isset($datoscovid->PREGUNTA2_14_2) && $datoscovid->PREGUNTA2_14_2?'checked':''}}>
                    <label class="form-check-label" for="Check2">Cancelación con la liquidez de la empresa.</label>
                    </div>
                </div>

                {{--  Check3  --}}
                <!-- <div class="form-row row">
                    <div class="form-group col-md-8" style="padding-left: 3%;">
                    <input onchange="showCheckBox('DIV2.123',this)" type="checkbox" class="custom-control-input cerrado slc2Q12" id="slc2Q12_3" name="slc2Q12_3" {{isset($datoscovid) && isset($datoscovid->PREGUNTA2_12_3) && $datoscovid->PREGUNTA2_12_3?'checked':''}}>
                    <label class="form-check-label" for="Check2">Han reducido sus líneas con proveedores, en qué porcentaje.</label>
                    </div>
                    <div class="col-md-2"></div>
                    {{--  DivCheck3-1 --}}
                    <div identificador="DIV2.123_TRUE"  style="{{isset($datoscovid)&& isset($datoscovid->PREGUNTA2_12_3) && $datoscovid->PREGUNTA2_12_3?'':'display:none'}}"  id="divCheck1-2" class="form-group col-md-2 DIV2.123">
                        <label style="font-weight: 100;">Porc %</label>
                        <input id="slc2Q12_3_1"  name="slc2Q12_3_1" type="number" class="form-control cerrado" oninput="maxLengthCheck(this)" onkeypress="return isNumeric(event)" required value="{{isset($datoscovid)?(isset($datoscovid->PREGUNTA2_12_3_1)?$datoscovid->PREGUNTA2_12_3_1:''):''}}" min="0" max="100">
                    </div>
                </div> -->
                
                {{--  Check4  --}}
                <div class="form-row row">
                    <div class="form-group col-md-8" style="padding-left: 3%;">
                    <input onchange="showCheckBox('DIV2.143',this)"  type="checkbox" class="custom-control-input cerrado slc2Q14" id="slc2Q14_3" name="slc2Q14_3"  {{isset($datoscovid) && isset($datoscovid->PREGUNTA2_14_3) && $datoscovid->PREGUNTA2_14_3?'checked':''}}>
                    <label class="form-check-label" for="Check3"> Tomar préstamos de bancos para cancelar a los proveedores.</label>
                    </div>
                    <div identificador="DIV2.143_TRUE" style="{{isset($datoscovid) && isset($datoscovid->PREGUNTA2_14_3) && $datoscovid->PREGUNTA2_14_3?'':'display:none'}}"  class="form-group col-md-2 DIV2.143">
                        <label>Miles de US$</label>
                        <input onkeypress="return isNumeric(event)"  oninput="maxLengthMontoCheck(this)" id="slc2Q14_3_1"name="slc2Q14_3_1" value="{{isset($datoscovid) && isset($datoscovid->PREGUNTA2_14_3_1) && $datoscovid->PREGUNTA2_14_3_1 && isset($datoscovid->PREGUNTA2_14_3_1)?$datoscovid->PREGUNTA2_14_3_1:''}}" class="form-control cerrado slcQ14_3"  type="numeric" min="0" max="999999999">
                        </div>
                        <div identificador="DIV2.143_TRUE" style="{{isset($datoscovid) && isset($datoscovid->PREGUNTA2_14_3) && $datoscovid->PREGUNTA2_14_3?'':'display:none'}}" class="form-group col-md-2 DIV2.143">
                        <label>Plazo (meses)</label>
                        <input onkeypress="return isNumeric(event)"  oninput="maxLengthPlazoCheck(this)" id="slc2Q14_3_2" name="slc2Q14_3_2" class="form-control cerrado slcQ14_3" type="numeric" min="0" value="{{isset($datoscovid) && isset($datoscovid->PREGUNTA2_14_3_2) && $datoscovid->PREGUNTA2_14_3_2?$datoscovid->PREGUNTA2_14_3_2:''}}" max="999">
                    </div>
                </div>
                <div style="height:40px;"></div>
                <!-- {{--  Pregunta4  --}}
                <div class="form-row row">
                    <div class="form-group col-md-6">
                    <label> 4. En qué porcentaje se ha reducido su ingreso mensual en lo que va del 2021</label>
                    </div>
                    <div class="form-group col-md-1">
                    </div>
                    <div class="form-group col-md-2">
                    </div>
                    <div class="form-group col-md-1">
                    <label style="font-weight: 100;margin-top: 5px;">Porc %</label>
                    </div>
                    <div class="form-group col-md-2">
                    <input id="slcQ4" name="slcQ4" onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)" type="number" min="0" max="100" class="form-control cerrado" value="{{isset($datoscovid)?$datoscovid->PREGUNTA_4:''}}" required>
                    </div>
                </div>
                <div style="height:40px;"></div> -->
                <!-- {{--  Pregunta5  --}}
                <div class="form-row row">
                    <div class="form-group col-md-6">
                        <label>5. En cuánto espera proyectar ventas al cierre del 2021 </label>
                        <label>(En el caso de porcentaje, indicar la variación respecto al año anterior)</label>
                    </div>
                    <div class="form-group col-md-2">
                    </div>
                    <div class="form-group col-md-2">
                        <label style="font-weight: 100;">Porc %</label>
                        <input min="-999" max="999" id="slcQ5_1" name="slcQ5_1" type="number" class="form-control cerrado" onchange="validateQuestion5(event,this)"  oninput="maxLengthCheck5(this)" value="{{isset($datoscovid)?$datoscovid->PREGUNTA_5_1:''}}" required>
                    </div>
                    <div class="form-group col-md-2">
                        <label style="font-weight: 100;">Miles US$</label>
                        <input id="slcQ5_2" name="slcQ5_2" type="number" class="form-control cerrado" onchange="validateQuestion5(event,this)" required value="{{isset($datoscovid)?$datoscovid->PREGUNTA_5_2:''}}" oninput="maxLengthMontoCheck(this)" min="0" max="999999">
                    </div>
                </div>
                <div style="height:40px;"></div> -->
                <!-- {{--  Pregunta6  --}}
                <div class="form-row row">
                    <div class="form-group col-md-6">
                        <label for="lblQ5">6. ¿Qué porcentaje o monto de su costo de ventas y gastos operativos es fijo? </label>
                    </div>
                    <div class="form-group col-md-2">
                    </div>
                    <div class="form-group col-md-2">
                        <label style="font-weight: 100;">Porc %</label><input id="slcQ6_1"  name="slcQ6_1" type="number" class="form-control cerrado" oninput="maxLengthCheck(this)" onkeypress="return isNumeric(event)" onchange="validateQuestion6(event,this)" required value="{{isset($datoscovid)?$datoscovid->PREGUNTA_6_1:''}}" min="0" max="100">
                    </div>
                    <div class="form-group col-md-2">
                        <label style="font-weight: 100;">Miles US$</label><input id="slcQ6_2" name="slcQ6_2" type="number" class="form-control cerrado"  onkeypress="return isNumeric(event)" oninput="maxLengthMontoCheck(this)" onchange="validateQuestion6(event,this)" required value="{{isset($datoscovid)?$datoscovid->PREGUNTA_6_2:''}}" min="0">
                    </div>
                </div>
                <div style="height:40px;"></div> -->
                <!-- {{--  Pregunta7  --}}
                <div class="form-row row">
                    <div class="form-group col-md-8">
                        <label for="slcQ7">7. Realiza importación / exportación a algún país que se encuentra afectado fuertemente con el COVID19.</label>
                    </div>
                    <div class="form-group col-md-2">
                    </div>
                    <div class="form-group col-md-2">
                        <select onchange="show('DIV7',this)" id="slcQ7" name="slcQ7" class="form-control custom-select is-invalid cerrado" onchange="" required>
                            <option {{isset($datoscovid) && !empty($datoscovid->PREGUNTA_7)?'' : 'selected'}} disabled value="">-Seleccionar-</option>
                            <option value="0" {{isset($datoscovid) && $datoscovid->PREGUNTA_7=='0'?'selected' : ''}}>Ninguna</option>
                            <option value="1" {{isset($datoscovid) && $datoscovid->PREGUNTA_7=='1'?'selected' : ''}}>Imp</option>
                            <option value="2" {{isset($datoscovid) && $datoscovid->PREGUNTA_7=='2'?'selected' : ''}}>Exp</option>
                            <option value="3" {{isset($datoscovid) && $datoscovid->PREGUNTA_7=='3'?'selected' : ''}}>Ambas</option>
                        </select>
                    </div>
                </div>

                <div class="form-row row"> -->
                <!-- {{--  Pregunta7-1 --}}
                    <div id="divRpta7-1"  style="{{isset($datoscovid) && in_array($datoscovid->PREGUNTA_7,array('1','3'))?'':'display:none'}}"identificador="DIV7_13"  class="form-row DIV7">
                        <div class="form-group col-md-6">
                            <label style="font-weight: normal">¿En caso ud se dedique a importar, indique cuántos meses de stock mantiene?</label>
                        </div>
                        <div class="form-group col-md-2">
                        </div>
                        <div class="form-group col-md-1">
                            <label style="margin-top: 10px;font-weight: 100;">N° Meses</label>
                        </div>
                        <div class="form-group col-md-2">
                        </div>
                        <div class="form-group col-md-1">
                            <input id="slcQ7_1" onkeypress="return isNumeric(event)"  oninput="maxLengthPlazoCheck(this)" name="slcQ7_1" type="number" class="form-control cerrado slcQ7"  value="{{isset($datoscovid)?$datoscovid->PREGUNTA_7_1:''}}" min="0" max="999">
                        </div>
                    </div>

                    {{--  Pregunta7-2 --}}
                    <div id="divRpta7-2"  style="{{isset($datoscovid) && in_array($datoscovid->PREGUNTA_7,array('1','2','3'))?'':'display:none'}}"identificador="DIV7_123"  class="form-row DIV7">
                        <div class="form-group col-md-6">
                            <label style="font-weight: normal">Si marco importación o exportación comentar el monto que importa o exporta al país afectado</label>
                        </div>
                        <div class="form-group col-md-2">
                        </div>
                        <div class="form-group col-md-1">
                            <label style="font-weight: 100;">Exp Miles US$</label>
                            <input id="slcQ7_2_1" name="slcQ7_2_1" type="number" class="form-control cerrado slcQ7" oninput="maxLengthMontoCheck(this)" onkeypress="return isNumeric(event)" value="{{isset($datoscovid)?$datoscovid->PREGUNTA_7_2_1:''}}" min="0" max="999999999">
                        </div>
                        <div class="form-group col-md-2">
                        </div>
                        <div class="form-group col-md-1">
                            <label style="font-weight: 100;">Imp Miles US$</label>
                            <input id="slcQ7_2_2" name="slcQ7_2_2" type="number" class="form-control cerrado slcQ7" oninput="maxLengthMontoCheck(this)" onkeypress="return isNumeric(event)" value="{{isset($datoscovid)?$datoscovid->PREGUNTA_7_2_2:''}}" min="0" max="999999999">
                        </div>
                    </div>

                    {{--  Pregunta7-3 --}}
                    <div id="divRpta7-3"  style="{{isset($datoscovid) && in_array($datoscovid->PREGUNTA_7,array('1','2','3'))?'':'display:none'}}" identificador="DIV7_123"  class="form-row DIV7">
                        <div class="form-group col-md-6">
                            <label style="font-weight: normal">¿Qué porcentaje estima que se verá afectado?</label>
                        </div>
                        <div class="form-group col-md-2">
                        </div>
                        <div class="form-group col-md-1">
                            <label style="margin-top: 5px;font-weight: 100;">Porc %</label>
                        </div>
                        <div class="form-group col-md-2">
                        </div>
                        <div class="form-group col-md-1">
                            <input id="slcQ7_3" name="slcQ7_3" type="number" class="form-control cerrado slcQ7" oninput="maxLengthCheck(this)" onkeypress="return isNumeric(event)" value="{{isset($datoscovid)?$datoscovid->PREGUNTA_7_3:''}}" min="0" max="100">
                        </div>
                    </div>

                    {{--  Pregunta7-4 --}}
                    <div id="divRpta7-4" style="{{isset($datoscovid) && in_array($datoscovid->PREGUNTA_7,array('1','2','3'))?'':'display:none'}}" identificador="DIV7_123" class="form-row DIV7">
                        <div class="form-group col-md-6">
                            <label style="font-weight: normal">¿Cuenta con proveedores o mercados sustitutos?</label>
                        </div>
                        <div class="form-group col-md-2">
                        </div>
                        <div class="form-group col-md-4">
                            <select id="slcQ7_4" name="slcQ7_4" class="slcQ7 form-control custom-select is-invalid cerrado" >
                                <option {{isset($datoscovid) && !empty($datoscovid->PREGUNTA_7_4)?'' : 'selected'}} disabled value="">-Seleccionar-</option>
                                <option value="1" {{isset($datoscovid) && $datoscovid->PREGUNTA_7_4=='1'?'selected' : ''}}>Sí</option>
                                <option value="2" {{isset($datoscovid) && $datoscovid->PREGUNTA_7_4=='2'?'selected' : ''}}>No</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div style="height:40px;"></div> -->
                <!-- {{--  Pregunta8 Informativa  --}}
                <div class="form-row row">
                    <div class="form-group col-md-12">
                    <p><strong>8. ¿Qué está haciendo con los vencimientos de obligaciones con sus proveedores? (Próximas a vencer)?</strong></p>
                    <p style="padding-left: 1%;"> (Coloque un <i class="fa fa-check-square-o" aria-hidden="true"></i>; en caso seleccionar la primera comentar las preguntas de la primera opción) 
                    <strong>(MÚLTIPLE SELECCIÓN)</strong></p>
                    </div>
                </div>

                {{--  Check1  --}}
                <div class="form-row row">
                    <div class="form-group col-md-12" style="padding-left: 3%;">
                    <input onchange="showCheckBox('DIV81',this)" type="checkbox" class="custom-control-input cerrado slcQ8" id="slcQ8_1" name="slcQ8_1" {{isset($datoscovid) && $datoscovid->PREGUNTA_8_1?'checked':''}}>
                    <label class="form-check-label" for="Check1"> Reprogramación con los proveedores.</label>
                    </div>
                </div>

                    {{--  DivCheck1-1-1 --}}
                <div identificador="DIV81_TRUE" style="{{isset($datoscovid) && $datoscovid->PREGUNTA_8_1?'':'display:none'}}" id="divCheck1-1" class="form-row row DIV81">
                    <div class="form-group col-md-8" style="padding-left: 3%;">
                        <label style="font-weight:normal">¿A qué plazo reprogramó?</label>
                    </div>
                    <div class="form-group col-md-1">
                    </div>
                    <div class="form-group col-md-2">
                        <select id="slcQ8_1_1_1" name="slcQ8_1_1_1" class="slcQ8_1 form-control custom-select is-invalid cerrado" >
                            <option {{isset($datoscovid) && !empty($datoscovid->PREGUNTA_8_1_1_1)?'' : 'selected'}} disabled value="">-Seleccionar-</option>
                            <option value="días" {{isset($datoscovid) && $datoscovid->PREGUNTA_8_1_1_1=='días'?'selected' : ''}}>Días</option>
                            <option value="meses" {{isset($datoscovid) && $datoscovid->PREGUNTA_8_1_1_1=='meses'?'selected' : ''}}>Meses</option>
                        </select>
                    </div>
                    <div class="form-group col-md-1">
                    <input onkeypress="return isNumeric(event)"  oninput="maxLengthPlazoCheck(this)" id="slcQ8_1_1_2" name="slcQ8_1_1_2" type="number" class="form-control cerrado slcQ8_1" value="{{isset($datoscovid)?$datoscovid->PREGUNTA_8_1_1_2:''}}" min="0" max="999">
                    </div>
                </div>

                    {{--  DivCheck1-1-2 --}}
                <div identificador="DIV81_TRUE"  style="{{isset($datoscovid) && $datoscovid->PREGUNTA_8_1?'':'display:none'}}"  id="divCheck1-2" class="form-row row DIV81">
                    <div class="form-group col-md-8" style="padding-left: 3%;">
                        <label style="font-weight:normal">¿Ha tenido cancelación de sus líneas?</label>
                    </div>
                    <div class="form-group col-md-1">
                    </div>
                    <div class="form-group col-md-2">
                    <select id="slcQ8_1_2" class="form-control custom-select is-invalid cerrado slcQ8_1" name="slcQ8_1_2">
                        <option {{isset($datoscovid) && !empty($datoscovid->PREGUNTA_8_1_2)?'' : 'selected'}} disabled value="">-Seleccionar-</option>
                        <option value="1" {{isset($datoscovid) && $datoscovid->PREGUNTA_8_1_2=='1'?'selected' : ''}}>Sí</option>
                        <option value="2" {{isset($datoscovid) && $datoscovid->PREGUNTA_8_1_2=='2'?'selected' : ''}}>No</option>
                    </select>
                    </div>
                    <div class="form-group col-md-1">
                    </div>
                </div>
                {{--  Check2  --}}
                <div class="form-row row">
                    <div class="form-group col-md-12" style="padding-left: 3%;">
                    <input type="checkbox" class="custom-control-input cerrado slcQ8" id="slcQ8_2" name="slcQ8_2" {{isset($datoscovid) && $datoscovid->PREGUNTA_8_2?'checked':''}}>
                    <label class="form-check-label" for="Check2"> Cancelación con la liquidez de la empresa.</label>
                    </div>
                </div>

                {{--  Check3  --}}
                <div class="form-row row">
                    <div class="form-group col-md-8" style="padding-left: 3%;">
                    <input onchange="showCheckBox('DIV8.3',this)"  type="checkbox" class="custom-control-input cerrado slcQ8" id="slcQ8_3" name="slcQ8_3"  {{isset($datoscovid) && $datoscovid->PREGUNTA_8_3?'checked':''}}>
                    <label class="form-check-label" for="Check3"> Tomar préstamos de bancos para cancelar a los proveedores.</label>
                    </div>
                    <div identificador="DIV8.3_TRUE" style="{{isset($datoscovid) && $datoscovid->PREGUNTA_8_3?'':'display:none'}}"  class="form-group col-md-2 DIV8.3">
                    <label>Miles de US$</label>
                    <input onkeypress="return isNumeric(event)"  oninput="maxLengthMontoCheck(this)" id="slcQ8_3_1"name="slcQ8_3_1" value="{{isset($datoscovid) && $datoscovid->PREGUNTA_8_3?$datoscovid->PREGUNTA_8_3_1:''}}" class="form-control cerrado slcQ8_3"  type="numeric" min="0" max="999999999">
                    </div>
                    <div identificador="DIV8.3_TRUE" style="{{isset($datoscovid) && $datoscovid->PREGUNTA_8_3?'':'display:none'}}" class="form-group col-md-2 DIV8.3">
                    <label>Plazo (meses)</label>
                    <input onkeypress="return isNumeric(event)"  oninput="maxLengthPlazoCheck(this)" id="slcQ8_3_2" name="slcQ8_3_2" class="form-control cerrado slcQ8_3" type="numeric" min="0" value="{{isset($datoscovid) && $datoscovid->PREGUNTA_8_3?$datoscovid->PREGUNTA_8_3_2:''}}" max="999">
                    </div>
                </div>
                <div style="height:40px;"></div> -->

                <!-- {{--  Pregunta13  --}} -->
                <!-- <div class="form-row row">
                    <div class="form-group col-md-6">
                        <label>13. Indicar el monto de cuentas por cobrar morosas que registró hasta el cierre del 2020.</label>
                    </div>
                    <div class="form-group col-md-2">
                    </div>
                    <div class="form-group col-md-2">
                        <label style="font-weight: 100;">2019 Miles US$</label><input id="slc2Q13_1" name="slc2Q13_1" type="number" class="form-control cerrado"  onkeypress="return isNumeric(event)" oninput="maxLengthMontoCheck(this)" required value="{{isset($datoscovid)?(isset($datoscovid->PREGUNTA2_13_1)?$datoscovid->PREGUNTA2_13_1:''):''}}" min="0">
                    </div>
                    <div class="form-group col-md-2">
                        <label style="font-weight: 100;">2020 Miles US$</label><input id="slc2Q13_2" name="slc2Q13_2" type="number" class="form-control cerrado"  onkeypress="return isNumeric(event)" oninput="maxLengthMontoCheck(this)" required value="{{isset($datoscovid)?(isset($datoscovid->PREGUNTA2_13_2)?$datoscovid->PREGUNTA2_13_2:''):''}}" min="0">
                    </div>
                </div>                 -->

                {{--  Pregunta15 --}}
                <div class="form-row row">
                    <div class="form-group col-md-8">
                        <label>15. Actualmente tiene deuda reprogramada con bancos</label>
                    </div>
                    <div class="form-group col-md-2">
                    </div>
                    <div class="form-group col-md-2">
                        <select onchange="show('DIV2.15',this)" id="slc2Q15" name="slc2Q15" class="form-control custom-select is-invalid cerrado" onchange="" required>
                            <option {{isset($datoscovid) && !empty($datoscovid->PREGUNTA2_15) && isset($datoscovid->PREGUNTA2_15)?'' : 'selected'}} disabled value="">-Seleccionar-</option>
                            <option value="0" {{isset($datoscovid) && isset($datoscovid->PREGUNTA2_15) && $datoscovid->PREGUNTA2_15=='0'?'selected' : ''}}>No</option>
                            <option value="1" {{isset($datoscovid) && isset($datoscovid->PREGUNTA2_15) && $datoscovid->PREGUNTA2_15=='1'?'selected' : ''}}>Si</option>
                        </select>
                    </div>
                </div>
                {{--  DivCheck1-1-3 --}}
                <div identificador="DIV2.15_1"  style="{{isset($datoscovid)&& isset($datoscovid->PREGUNTA2_15) && $datoscovid->PREGUNTA2_15=='1'?'':'display:none'}}"  id="divCheck1-2" class="form-row row DIV2.15">
                    <div class="form-group col-md-8">
                        <label style="font-weight:normal">Monto</label>
                    </div>
                    <div class="form-group col-md-2">
                    </div>
                    <div class="form-group col-md-2">
                    <input onkeypress="return isNumeric(event)"  oninput="maxLengthPlazoCheck(this)" id="slc2Q15_1" name="slc2Q15_1" type="number" class="form-control cerrado slc2Q15_1" value="{{isset($datoscovid)?(isset($datoscovid->PREGUNTA2_15_1)?$datoscovid->PREGUNTA2_15_1:''):''}}" min="0" max="999">
                    </div>
                </div>
                {{--  DivCheck1-1-4 --}}
                <div identificador="DIV2.15_1"  style="{{isset($datoscovid)&& isset($datoscovid->PREGUNTA2_15) && $datoscovid->PREGUNTA2_15?'':'display:none'}}"  id="divCheck1-2" class="form-row row DIV2.15">
                    <div class="form-group col-md-8">
                        <label style="font-weight:normal">¿Necesitas reprogramar nuevas deudas o volver a reprogramarla las actuales?</label>
                    </div>
                    <div class="form-group col-md-2">
                        <select onchange="show('DIV2.15.2',this)" id="slc2Q15_2" class="form-control custom-select is-invalid cerrado slc2Q15_2" name="slc2Q15_2">
                            <option {{isset($datoscovid) && isset($datoscovid->PREGUNTA2_15_2) && !empty($datoscovid->PREGUNTA2_15_2)?'' : 'selected'}} disabled value="">-Seleccionar-</option>
                            <option value="0" {{isset($datoscovid) && isset($datoscovid->PREGUNTA2_15_2) && $datoscovid->PREGUNTA2_15_2=='0'?'selected' : ''}}>No</option>
                            <option value="1" {{isset($datoscovid) && isset($datoscovid->PREGUNTA2_15_2) && $datoscovid->PREGUNTA2_15_2=='1'?'selected' : ''}}>Reprogramar más</option>
                            <option value="2" {{isset($datoscovid) && isset($datoscovid->PREGUNTA2_15_2) && $datoscovid->PREGUNTA2_15_2=='2'?'selected' : ''}}>Volver a reprogramarla</option>
                        </select>
                    </div>
                    <div class="form-group col-md-2">
                        <input style="{{isset($datoscovid)&& isset($datoscovid->PREGUNTA2_15_2_1) && $datoscovid->PREGUNTA2_15_2_1=='1'?'':'display:none'}}" identificador="DIV2.15.2_1" onkeypress="return isNumeric(event)"  oninput="maxLengthPlazoCheck(this)" id="slc2Q15_2_1" name="slc2Q15_2_1" type="number" class="form-control cerrado DIV2.15.2" value="{{isset($datoscovid)?(isset($datoscovid->PREGUNTA2_15_2_1)?$datoscovid->PREGUNTA2_15_2_1:''):''}}" min="0" max="999">
                    </div>
                </div>
                {{--  Pregunta16 --}}
                
                <div class="form-row row">
                    <div class="form-group col-md-8">
                    <label>16. ¿Qué porcentaje o monto de sus cuentas por cobrar han sido realizadas desde que inicio el COVID19?</label>
                    </div>
                    <div class="form-group col-md-2">
                    </div>
                    <div class="form-group col-md-1">
                    </div>
                    <div class="form-group col-md-1">
                </div>
                </div>
                <div class="form-row row">
                    <div class="form-group col-md-8">
                        <p style="color:#FF0000;margin-left:2%;font-size:12px">*Solo para clientes con una gestión financiera consolidada y profesional.</p>
                    </div>
                    <div class="form-group col-md-2">
                    </div>
                    <div class="form-group col-md-1">
                        <label style="font-weight: 100;">Porc %</label>
                        <input id="slc2Q16_1_1" name="slc2Q16_1_1" type="number" class="form-control cerrado" oninput="maxLengthCheck(this)" onchange="validateQuestion16(event,this)" onkeypress="return isNumeric(event)" required value="{{isset($datoscovid)?$datoscovid->PREGUNTA2_16_1_1:''}}" min="0" max="100">
                    </div>
                    <div class="form-group col-md-1">
                        <label style="font-weight: 100;">Miles US$</label>
                        <input id="slc2Q16_1_2" name="slc2Q16_1_2" type="number" class="form-control cerrado" onchange="validateQuestion9(event,this)" 
                    oninput="maxLengthMontoCheck(this)" onkeypress="return isNumeric(event)" required value="{{isset($datoscovid)?$datoscovid->PREGUNTA2_16_1_2:''}}" min="0" max="999999999">
                    </div>
                </div>
                <div style="{{isset($datoscovid) && isset($datoscovid->PREGUNTA2_16_1_1) && $datoscovid->PREGUNTA2_16_1_1<70?'':'display:none'}}" id="slc2Q16_2_wrapper" class="form-row row">
                    <div  class="form-row">
                        <p>Comentar ¿Qué está haciendo para cobrar lo restante?. </p>
                        <textarea maxlength="300" id="slc2Q16_2" name="slc2Q16_2" class="form-control is-invalid cerrado" placeholder="Ingrese sus comentarios aquí." style="width:100%; height:81px;">{{isset($datoscovid)?(isset($datoscovid->PREGUNTA2_16_2)?$datoscovid->PREGUNTA2_16_2:''):''}}</textarea>
                    </div>
                </div>
                <div style="height:40px;"></div>
                
                {{--  Pregunta17 --}}

                <div class="form-row row">
                    <div class="form-group col-md-6">
                        <label>17. ¿Cúanto es el monto o porcentaje de cuentas por cobrar en morosidad a la fecha?</label>
                    </div>
                    <div class="form-group col-md-1">
                    </div>
                    <div class="form-group col-md-2">
                        <label style="font-weight: 100;">Porc %</label>
                        <input value="{{isset($datoscovid)?$datoscovid->PREGUNTA2_17_1_1:''}}" id="slc2Q17_1_1" onchange="validateQuestion17(event,this)" onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)"  name="slc2Q17_1_1" type="number" class="form-control cerrado" min="0" max="100" required>
                    </div>
                    <div class="form-group col-md-1">
                    </div>
                    <div class="form-group col-md-2">
                        <label style="font-weight: 100;">Miles US$</label>
                        <input value="{{isset($datoscovid)?$datoscovid->PREGUNTA2_17_1_2:''}}" id="slc2Q17_1_2" oninput="maxLengthMontoCheck(this)" onkeypress="return isNumeric(event)"   name="slcQ17_1_2" type="number" class="form-control cerrado" min="0" max="999999999" required>
                    </div>
                </div>
                <div id="slc2Q17_2_wrapper" style="{{isset($datoscovid) && isset($datoscovid->PREGUNTA2_17_1_1) && $datoscovid->PREGUNTA2_17_1_1<70?'':'display:none'}}"  class="form-row row">
                    
                    <div  class="form-row">
                        <p>¿Sus políticas de crédito han cambiado? ¿Qué políticas ha cambiado o que estrategias ha seguido para no incrementar la morosidad?¿Qué esta haciendo para poder reducir la morosidad?</p>
                        <textarea  maxlength="300" id="slc2Q17_2" name="slc2Q17_2" class="form-control is-invalid cerrado" placeholder="Ingrese sus comentarios aquí." style="width:100%; height:81px;">{{isset($datoscovid)?(isset($datoscovid->PREGUNTA2_17_2)?$datoscovid->PREGUNTA2_17_2:''):''}}</textarea>
                    </div>
                </div>
                <div style="height:40px;"></div>

<!--    
                {{--  Pregunta11 --}}
                <div class="form-row row">
                    <div class="form-group col-md-6">
                    <label>11. ¿Cuenta con algún crédito Reactiva en el Sistema Financiero (Excepto IBK)?</label>
                    </div>
                    <div class="form-group col-md-2">
                    </div>
                    <div class="form-group col-md-4">
                    <select onchange="show('DIV11',this)" id="slcQ11" class="form-control custom-select is-invalid cerrado" name="slcQ11" required>
                        <option {{isset($datoscovid) && !empty($datoscovid->PREGUNTA_11)?'' : 'selected'}} disabled value="">-Seleccionar-</option>
                        <option value="1" {{isset($datoscovid) && $datoscovid->PREGUNTA_11=='1'?'selected' : ''}}>Sí</option>
                        <option value="2" {{isset($datoscovid) && $datoscovid->PREGUNTA_11=='2'?'selected' : ''}}>No</option>
                    </select>
                    </div>
                </div>
                <div identificador="DIV11_1" style="{{isset($datoscovid) && $datoscovid->PREGUNTA_11=='1'?'':'display:none'}}"  class="form-row row DIV11">
                    <div class="form-group col-md-2">
                    <input onchange="showCheckBox('DIV11.1.1',this)" type="checkbox" class="custom-control-input cerrado slcQ11" id="slcQ11_1_1" name="slcQ11_1_1"  {{isset($datoscovid) && $datoscovid->PREGUNTA_11_1_1?'checked':''}}>
                    <label class="form-check-label">BCP</label>
                    </div>
                    <div identificador="DIV11.1.1_TRUE" style="{{isset($datoscovid) && $datoscovid->PREGUNTA_11_1_1?'':'display:none'}}" class="DIV11.1.1 form-group col-md-4">
                    <label style="font-weight: 100;">Miles US$</label>
                    <input id="slcQ11_1_2" name="slcQ11_1_2" type="number" oninput="maxLengthMontoCheck(this)" checkrelacionado="slcQ11_1_1"  class="form-control cerrado slcQ11_i" onkeypress="return isNumeric(event)" min="0" value="{{isset($datoscovid)?$datoscovid->PREGUNTA_11_1_2:''}}" max="999999999">
                    </div>
                </div>
                <div identificador="DIV11_1" style="{{isset($datoscovid) && $datoscovid->PREGUNTA_11=='1'?'':'display:none'}}"   class="form-row row DIV11">
                    <div class="form-group col-md-2">
                    <input onchange="showCheckBox('DIV11.1.2',this)" type="checkbox" class="custom-control-input cerrado slcQ11" id="slcQ11_2_1" name="slcQ11_2_1"  {{isset($datoscovid) && $datoscovid->PREGUNTA_11_2_1?'checked':''}}>
                    <label class="form-check-label">BBVA</label>
                    </div>
                    <div identificador="DIV11.1.2_TRUE" style="{{isset($datoscovid) && $datoscovid->PREGUNTA_11_2_1?'':'display:none'}}"  class="DIV11.1.2 form-group col-md-4">
                    <label style="font-weight: 100;">Miles US$</label>
                    <input oninput="maxLengthMontoCheck(this)" id="slcQ11_2_2" name="slcQ11_2_2" type="number" checkrelacionado="slcQ11_2_1"  class="form-control cerrado slcQ11_i" onkeypress="return isNumeric(event)" value="{{isset($datoscovid)?$datoscovid->PREGUNTA_11_2_2:''}}"  min="0" max="999999999">
                    </div>
                </div>
                <div  identificador="DIV11_1" style="{{isset($datoscovid) && $datoscovid->PREGUNTA_11=='1'?'':'display:none'}}"   class="form-row row DIV11">
                    <div class="form-group col-md-2">
                    <input onchange="showCheckBox('DIV11.1.3',this)" type="checkbox" class="custom-control-input cerrado slcQ11" id="slcQ11_3_1" name="slcQ11_3_1"  {{isset($datoscovid) && $datoscovid->PREGUNTA_11_3_1?'checked':''}}>
                    <label>SCOTIA</label>
                    </div>
                    <div identificador="DIV11.1.3_TRUE" style="{{isset($datoscovid) && $datoscovid->PREGUNTA_11_3_1?'':'display:none'}}"  class="DIV11.1.3 form-group col-md-4">
                    <label style="font-weight: 100;">Miles US$</label>
                    <input oninput="maxLengthMontoCheck(this)" id="slcQ11_3_2" name="slcQ11_3_2" checkrelacionado="slcQ11_3_1"  type="number" class="form-control cerrado slcQ11_i" onkeypress="return isNumeric(event)" value="{{isset($datoscovid)?$datoscovid->PREGUNTA_11_3_2:''}}" min="0" max="999999999">
                    </div>
                </div>
                <div  identificador="DIV11_1" style="{{isset($datoscovid) && $datoscovid->PREGUNTA_11=='1'?'':'display:none'}}"   class="form-row row DIV11">
                    <div class="form-group col-md-2">
                    <input onchange="showCheckBox('DIV11.1.4',this)" type="checkbox" class="custom-control-input cerrado slcQ11" id="slcQ11_4_1" name="slcQ11_4_1"  {{isset($datoscovid) && $datoscovid->PREGUNTA_11_4_1?'checked':''}}>
                    <label>Banco Pichincha</label>
                    </div>
                    <div identificador="DIV11.1.4_TRUE" style="{{isset($datoscovid) && $datoscovid->PREGUNTA_11_4_1?'':'display:none'}}"  class="DIV11.1.4 form-group col-md-4">
                    <label style="font-weight: 100;">Miles US$</label>
                    <input oninput="maxLengthMontoCheck(this)" id="slcQ11_4_2" name="slcQ11_4_2" checkrelacionado="slcQ11_4_1" type="number" class="form-control cerrado slcQ11_i" onkeypress="return isNumeric(event)" value="{{isset($datoscovid)?$datoscovid->PREGUNTA_11_4_2:''}}" min="0" max="999999999">
                    </div>
                </div>
                <div identificador="DIV11_1" style="{{isset($datoscovid) && $datoscovid->PREGUNTA_11=='1'?'':'display:none'}}"   class="form-row row DIV11">
                    <div class="form-group col-md-2">
                    <input onchange="showCheckBox('DIV11.1.5',this)" type="checkbox" class="custom-control-input cerrado slcQ11" id="slcQ11_5_1" name="slcQ11_5_1"  {{isset($datoscovid) && $datoscovid->PREGUNTA_11_5_1?'checked':''}}>
                    <label>BanBif</label>
                    </div>
                    <div identificador="DIV11.1.5_TRUE" style="{{isset($datoscovid) && $datoscovid->PREGUNTA_11_5_1?'':'display:none'}}" class="DIV11.1.5 form-group col-md-4">
                    <label style="font-weight: 100;">Miles US$</label>
                    <input oninput="maxLengthMontoCheck(this)" id="slcQ11_5_2" name="slcQ11_5_2" type="number" class="form-control cerrado slcQ11_i" checkrelacionado="slcQ11_5_1"  onkeypress="return isNumeric(event)" value="{{isset($datoscovid)?$datoscovid->PREGUNTA_11_5_2:''}}" min="0" max="999999999">
                    </div>
                </div>
                <div  identificador="DIV11_1" style="{{isset($datoscovid) && $datoscovid->PREGUNTA_11=='1'?'':'display:none'}}"   class="form-row row DIV11">
                    <div class="form-group col-md-2">
                    <input  onchange="showCheckBox('DIV11.1.6',this)" type="checkbox" class="custom-control-input cerrado slcQ11" id="slcQ11_6_1" name="slcQ11_6_1"  {{isset($datoscovid) && $datoscovid->PREGUNTA_11_6_1?'checked':''}}>
                    <label>Otros</label>
                    </div>
                    <div identificador="DIV11.1.6_TRUE" style="{{isset($datoscovid) && $datoscovid->PREGUNTA_11_6_1?'':'display:none'}}" class="DIV11.1.6 form-group col-md-4">
                    <label style="font-weight: 100;">Miles US$</label>
                    <input oninput="maxLengthMontoCheck(this)" id="slcQ11_6_2" name="slcQ11_6_2" type="number" value="{{isset($datoscovid)?$datoscovid->PREGUNTA_11_6_2:''}}"  checkrelacionado="slcQ11_6_1"  class="form-control cerrado slcQ11_i" onkeypress="return isNumeric(event)" min="0" max="999999999">
                    </div>
                </div>
                <div style="height:40px;"></div> -->
                {{--  Pregunta18 --}}
                <div class="form-row row">
                    <div class="form-group col-md-8">
                    <label>18.  Indicar si han realizado inversiones en el 2020. <br> Mencionar la fuente de financiamiento; prestamos bancarios, prestamos de accionistas, otros.</label>
                    </div>
                    <div class="form-group col-md-2">
                    </div>
                    <div class="form-group col-md-2">
                    <select onchange="show('DIV18',this)" id="slcQ12" name="slcQ12" class="form-control custom-select is-invalid cerrado" onchange="" required>
                        <option {{isset($datoscovid) && !empty($datoscovid->PREGUNTA_12)?'' : 'selected'}} disabled value="">-Seleccionar-</option>
                        <option value="1" {{isset($datoscovid) && $datoscovid->PREGUNTA_12=='1'?'selected' : ''}}>Sí</option>
                        <option value="2" {{isset($datoscovid) && $datoscovid->PREGUNTA_12=='2'?'selected' : ''}}>No</option>
                    </select>
                    </div>
                </div>

                    {{--  Pregunta16-1 --}}
                <div identificador="DIV18_1" style="{{isset($datoscovid) && $datoscovid->PREGUNTA_12=='1'?'':'display:none'}}" id="divRpta12-1" class="DIV18 form-row row">
                    <div class="form-group col-md-8">
                    <label style="font-weight: normal">¿Cuál es  el monto de la inversión?</label>
                    </div>
                    <div class="form-group col-md-2">
                    </div>
                    <div class="form-group col-md-2">
                        <label style="font-weight: 100;">Miles US$</label><input oninput="maxLengthMontoCheck(this)" id="slcQ12_1" name="slcQ12_1" type="number" class="form-control cerrado slcQ12" onchange="" onkeypress="return isNumeric(event)" value="{{isset($datoscovid)?$datoscovid->PREGUNTA_12_1:''}}" min="0" max="999999999"
                        {{isset($datoscovid)&&$datoscovid->PREGUNTA_12=='1'?'required':''}}>
                    </div>
                </div>

                    {{--  Pregunta16-2 --}}
                <div identificador="DIV18_1" style="{{isset($datoscovid) && $datoscovid->PREGUNTA_12=='1'?'':'display:none'}}" id="divRpta12-2" class="DIV18 form-row row">
                    <div class="form-group col-md-8">
                    <label style="font-weight: normal">¿Cómo lo financió?</label>
                    </div>
                    <div class="form-group col-md-2">
                    </div>
                    <div class="form-group col-md-2">
                    <select onchange="show('DIV18.2',this)" id="slcQ12_2" name="slcQ12_2" class="form-control custom-select is-invalid cerrado slcQ12" {{isset($datoscovid)&&$datoscovid->PREGUNTA_12=='1'?'required':''}}>
                        <option value="">-Seleccionar-</option>
                        <option value="Banco" {{isset($datoscovid) && $datoscovid->PREGUNTA_12_2=='Banco'?'selected' : ''}}>Banco</option>
                        <option value="Recursos Propios" {{isset($datoscovid) && $datoscovid->PREGUNTA_12_2=='Recursos Propios'?'selected' : ''}}>Recursos Propios</option>
                        <option value="Accionistas" {{isset($datoscovid) && $datoscovid->PREGUNTA_12_2=='Accionistas'?'selected' : ''}}>Accionistas</option>
                    </select>
                    </div>
                    {{--  Pregunta16-2-1 --}}
                <div identificador="DIV18.2_Banco" style="{{isset($datoscovid) && $datoscovid->PREGUNTA_12_2=='Banco'?'':'display:none'}}"  id="divRpta12-2-1" class="form-row row DIV18.2">
                    <div class="form-group col-md-5">
                    </div>
                    <div class="form-group col-md-3">
                    <label>¿Qué porcentaje financío?</label>
                    </div>
                    <div class="form-group col-md-2">
                    </div>
                    <div class="form-group col-md-1">
                    <label style="margin-top: 5px;font-weight: 100;">Porc %</label>
                    </div>
                    <div class="form-group col-md-1">
                        <input  id="slcQ12_2_1" name="slcQ12_2_1" type="number" class="form-control cerrado slcQ12_2" oninput="maxLengthCheck(this)" onkeypress="return isNumeric(event)" value="{{isset($datoscovid)?$datoscovid->PREGUNTA_12_2_1:''}}" min="0" max="100"
                        {{isset($datoscovid)&&$datoscovid->PREGUNTA_12_2=='Banco'?'required':''}}>
                    </div>
                </div>
                
                {{--  Pregunta16-2-2 --}}
                <div  identificador="DIV18.2_Banco" style="{{isset($datoscovid) && $datoscovid->PREGUNTA_12_2=='Banco'?'':'display:none'}}" id="divRpta12-2-2" class="form-row row DIV18.2">
                    <div class="form-group col-md-1">
                    </div>
                    <div class="form-group col-md-7">
                    <label style="margin-left: 60%;">Plazo de Financiamiento:</label>
                    </div>
                    <div class="form-group col-md-1">
                    </div>
                    <div class="form-group col-md-2">
                        <select id="slcQ12_2_2_1" name="slcQ12_2_2_1" class="form-control custom-select is-invalid cerrado slcQ12_2" {{isset($datoscovid)&&$datoscovid->PREGUNTA_12_2=='Banco'?'required':''}}>
                            <option {{isset($datoscovid) && !empty($datoscovid->PREGUNTA_12_2_2_1)?'' : 'selected'}} disabled value="">-Seleccionar-</option>
                            <option value="días" {{isset($datoscovid) && $datoscovid->PREGUNTA_12_2_2_1=='días'?'selected' : ''}}>Días</option>
                            <option value="meses" {{isset($datoscovid) && $datoscovid->PREGUNTA_12_2_2_1=='meses'?'selected' : ''}}>Meses</option>
                        </select>
                    </div>
                    <div class="form-group col-md-1">
                        <input onkeypress="return isNumeric(event)" oninput="maxLengthPlazoCheck(this)" id="slcQ12_2_2_2" name="slcQ12_2_2_2" type="number" class="form-control cerrado slcQ12_2" value="{{isset($datoscovid)?$datoscovid->PREGUNTA_12_2_2_2:''}}" min="0" max="999" {{isset($datoscovid)&&$datoscovid->PREGUNTA_12_2=='Banco'?'required':''}}>
                    </div>
                </div>
                </div>
                <div style="height:40px;"></div>

                <div class="form-row row">
                    <div class="form-group col-md-12">
                    <label>19. ¿Cuánto más cree que podría afectarle esta nueva cuarentena o una nueva paralización de actividades?  ¿Cuáles son las expectativas del cliente? ¿Cuál es el impacto que tendría  en sus ventas y cuáles serían medidas de tomaría para mantener liquidez? <br> 
                    </div>
                </div>
                <div class="form-row row">
                    <div class="form-group col-md-12">
                    <textarea maxlength="300" id="slc2Q19" name="slc2Q19" class="form-control is-invalid cerrado" placeholder="Ingrese sus comentarios aquí." style="width:100%; height:81px;">{{isset($datoscovid)?(isset($datoscovid->PREGUNTA2_19)?$datoscovid->PREGUNTA2_19:''):''}}</textarea>
                    </div>
                </div>
                <div style="height:40px;"></div>
                {{--  Pregunta17 --}}

                <div class="form-row row">
                    <div class="form-group col-md-12">
                    <p><strong>20. Qué impacto tiene el covid19 a la situación financiera actual de la empresa:
                    </strong>
                    Coloque un <i class="fa fa-check-square-o" aria-hidden="true"></i><strong> (ÚNICA SELECCIÓN)</strong></p>
                    </div>
                </div>

                <div class="form-row row">
                    {{--  Check4  --}}
                    <div class="form-group col-md-3" style="padding-left: 3%;">
                    <input class="form-check-input cerrado slcQ13" type="checkbox" value="sin impacto" id="slcQ13_0" name="slcQ13_0"  {{isset($datoscovid) && $datoscovid->PREGUNTA_13 =='sin impacto' ?'checked':''}}>
                    <label class="form-check-label"> Sin impacto 
                    </label>
                    </div>

                    {{--  Check5  --}}
                    <div class="form-group col-md-3" style="padding-left: 3%;">
                    <input class="form-check-input cerrado slcQ13" type="checkbox" value="bajo" id="slcQ13_1" name="slcQ13_1"  {{isset($datoscovid) && $datoscovid->PREGUNTA_13 == 'bajo' ?'checked':''}}>
                    <label class="form-check-label"> Bajo 
                    <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" 
                    title="Aquellos empresas que a pesar de la cuarentena han seguido operando pero con un menor volumen de ingresos."></i>
                    </label>
                    </div>

                    {{--  Check6  --}}
                    <div class="form-group col-md-3" style="padding-left: 3%;">
                    <input type="checkbox" class="form-check-input cerrado slcQ13"  value="medio" id="slcQ13_2" name="slcQ13_2"  {{isset($datoscovid) && $datoscovid->PREGUNTA_13 == 'medio' ?'checked':''}} >
                    <label class="form-check-label"> Medio 
                        <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" 
                        title="Aquellas empresas que no están operando dentro de la cuarentena, sin embargo su salud financiera le ha permitido cubrir obligaciones corrientes o aquellas que siguen operando pero sus ingresos han sido afectados fuertemente."></i>
                    </label>
                    </div>

                    {{--  Check7  --}}
                    <div class="form-group col-md-3" style="padding-left: 3%;">
                    <input type="checkbox" class="form-check-input cerrado slcQ13"  value="alto" id="slcQ13_3" name="slcQ13_3"  {{isset($datoscovid) && $datoscovid->PREGUNTA_13 == 'alto' ?'checked':''}} >
                    <label class="form-check-label"> Alto 
                    <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" 
                        title="Aquellas empresas que no están operando dentro de la cuarentena y han tenido que recordar personal y/o tomar préstamos para poder cubrir las obligaciones corrientes."></i>
                    </label>
                    </div>
                </div>
                <div style="height:40px;"></div>
                {{--  Pregunta18 --}}
                <div class="form-row row">
                    <div class="form-group col-md-12"><label>21. Estrategia</label></div>
                </div>
                <div class="form-row row">
                    <div class="form-group col-md-1">Plan Propuesto</div>
                    <div class="form-group col-md-3">
                    <select required name="slcQ14_1" id="slcQ14_1" class="form-control custom-select is-invalid cerrado">
                        <option value="">-Seleccionar-</option>
                        <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_1=='Crecer'?'selected' : ''}} value="Crecer">Crecer</option>
                        <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_1=='Mantener'?'selected' : ''}} value="Mantener">Mantener</option>
                        <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_1=='Reprogramar'?'selected' : ''}} value="Reprogramar">Reprogramar </option>
                        <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_1=='Feve'?'selected' : ''}} value="Feve">Feve</option>
                        <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_1=='Refinanciar'?'selected' : ''}} value="Refinanciar">Refinanciar</option>
                    </select>
                    </div>
                    <div id="crecerWrapper" style="{{isset($datoscovid) && $datoscovid->PREGUNTA_14_1=='Crecer'?'' :'display:none'}}">
                        <div class="form-group col-md-1">Monto</div>
                        <div class="form-group col-md-3">
                            <input {{isset($datoscovid) && $datoscovid->PREGUNTA_14_1=='Crecer'?'required' :''}} type="number" class="form-control custom-select is-invalid cerrado" min="0" max="999999" step="0.01" value="{{isset($datoscovid) && $datoscovid->PREGUNTA_14_1=='Crecer'?$datoscovid->MONTO_CRECER :''}}" name="montoCrecer" id="montoCrecer">
                        </div>
                        <div class="form-group col-md-1">Detalle</div>
                        <div class="form-group col-md-3">
                            <textarea {{isset($datoscovid) && $datoscovid->PREGUNTA_14_1=='Crecer'?'required' :''}}  type="text" class="form-control custom-select is-invalid cerrado" maxLength="30" name="detalleCrecer" id="detalleCrecer">{{trim(isset($datoscovid) && $datoscovid->PREGUNTA_14_1=='Crecer'?$datoscovid->DETALLE_CRECER :'')}}</textarea>
                        </div>
                    </div>
                    <div class="form-group col-md-1">FEVE</div>
                    <div class="form-group col-md-3">
                    <select {{$privilegios == App\Model\Infinity\Visita::LLENADOR_SIN_BLINDAJE?'':'required'}} name="slcQ14_2" id="slcQ14_2" class="form-control custom-select is-invalid cerrado">
                        <option value="">-Seleccionar-</option>
                        <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_2=='Sin FEVE'?'selected' : ''}} value="Sin FEVE">Sin FEVE</option>
                        <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_2=='Seguir COVID'?'selected' : ''}} value="Seguir COVID">Seguir COVID</option>
                        <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_2=='Seguir'?'selected' : ''}} value="Seguir">Seguir</option>
                        <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_2=='Garantizar'?'selected' : ''}} value="Garantizar">Garantizar</option>
                        <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_2=='Reducir'?'selected' : ''}} value="Reducir">Reducir</option>
                        <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_2=='Salir'?'selected' : ''}} value="Salir">Salir</option>
                    </select>
                    </div>
                    <div class="form-group col-md-1">Clasificación</div>
                    <div class="form-group col-md-3">
                    <select {{$privilegios == App\Model\Infinity\Visita::LLENADOR_SIN_BLINDAJE?'':'required'}} name="slcQ14_3" id="slcQ14_3" class="form-control custom-select is-invalid cerrado">
                        <option value="">-Seleccionar-</option>
                        <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_3=='Revisar'?'selected' : ''}} value="Revisar">Revisar</option>
                        <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_3=='No Revisar'?'selected' : ''}} value="No Revisar">No Revisar</option>
                    </select>
                    </div>
                </div>
                <br>
                @if($privilegios ==App\Model\Infinity\Visita::ROL_VALIDADOR && $segmento == 'MEDIANA EMPRESA')
                <div class="form-row row">
                    <div class="form-group col-md-8">¿Desea aplicar blindaje de revisión con su rol revisor?</div>
                    <div class="form-group col-md-4">
                    <select required onchange="show('DIV14.4',this)" name="slcQ14_4" id="slcQ14_4" class="form-control custom-select is-invalid cerrado">
                        <option value="">-Seleccionar-</option>
                        <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_4=='1'?'selected' : ''}}  value="1">Si</option>
                        <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_4=='2'?'selected' : ''}}  value="2">No</option>
                    </select>
                    </div>
                </div>
                <div style="{{isset($datoscovid) && $datoscovid->PREGUNTA_14_4=='1'?'' : 'display:none'}}" identificador="DIV14.4_1" class="form-row row DIV14.4">
                    <div class="form-group col-md-8">¿Por cuantos meses desea aplicar el blindaje?</div>
                    <div class="form-group col-md-4">
                        <select {{isset($datoscovid) && $datoscovid->PREGUNTA_14_4=='1'?'required' : 'disabled'}} name="slcQ14_5" id="slcQ14_5" class="form-control custom-select is-invalid cerrado">
                        <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_5=='1'?'selected' : ''}} value="1">1</option>
                        <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_5=='2'?'selected' : ''}} value="2">2</option>
                        <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_5=='3'?'selected' : ''}} value="3">3</option>
                            @if (
                                (isset($datoscovid) && in_array($datoscovid->PREGUNTA_13,array('bajo','sin impacto'))) &&
                                (isset($datoscovid) && in_array($datoscovid->PREGUNTA_14_2,array('Sin Feve'))) &&
                                (isset($datoscovid) && in_array($datoscovid->PREGUNTA_14_1,array('Crecer','Mantener')))
                            )
                                <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_5=='4'?'selected' : ''}} value="4">4</option>
                                <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_5=='5'?'selected' : ''}} value="5">5</option>
                                <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_5=='6'?'selected' : ''}} value="6">6</option>
                            @endif
                        </select>
                    </div>
                </div>
                @endif
                <div style="height:40px;"></div>

                <table id="cumplimientos" style="width: 100%;">
                    <tr>
                    <th style="text-align: center;">Owner de<br> Compromiso </th>
                    <th style="text-align: center;">Tipo de <br> compromiso</th>
                    <th style="text-align: center;">Detalle de <br> compromiso</th>
                    <th style="text-align: center;">Comentario</th>
                    <th style="text-align: center;">Indicador</th>
                    <th style="text-align: center;">Fecha Fin de<br>compromiso</th>
                    <th style="text-align: center;">Gestor</th>
                    </tr>
                    @if(count($cumplimientos)>0)
                        @foreach(range(0,count($cumplimientos)-1) as $i)
                        <tr>
                        <td>
                            <select name="{{'TIPO_GESTION'.$i}}" id="{{'TIPO_GESTION'.$i}}" class="form-control custom-select is-invalid cerrado" required>
                                @if(count($revision)>0)
                                    @foreach(range(0,count($revision)-1) as $j)
                                    <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_GESTION==$revision[$j]?'selected' : ''}} value="{{$revision[$j]}}">{{$revision[$j]}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </td>
                        <td>
                            <select onchange="show('{{'DIVCUMPLIMIENTO'.$i}}',this)" name="{{'TIPO_COMPROMISO'.$i}}" id="{{'TIPO_COMPROMISO'.$i}}" class="form-control custom-select is-invalid cerrado" required>
                                <option value ="">-Seleccionar-</option>
                                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO=='Comerciales'?'selected' : ''}} value="Comerciales">Comerciales</option>
                                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO=='Canalización de flujos IN'?'selected' : ''}}  value="Canalización de flujos IN">Canalización de flujos IN</option>
                                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO=='Incremento de flujos OUT'?'selected' : ''}}  value="Incremento de flujos OUT">Incremento de flujos OUT</option>
                                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO=='Garantías'?'selected' : ''}} value="Garantías">Garantías</option>
                                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO=='Estados Financieros'?'selected' : ''}} value="Estados Financieros">Estados Financieros</option>
                                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO=='Otros'?'selected' : ''}} value="Otros">Otros</option>
                            </select>
                        </td>
                        <td>
                            <select style="{{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO=='Comerciales'?'':'display:none'}}"
                                {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO=='Comerciales'?'required':'disabled'}}  
                                identificador="{{'DIVCUMPLIMIENTO'.$i.'_Comerciales'}}"
                                name="{{'TIPO_COMPROMISO_DETALLE_Comerciales'.$i}}" id="{{'TIPO_COMPROMISO_DETALLE_Comerciales'.$i}}" 
                                class="form-control custom-select is-invalid cerrado {{'DIVCUMPLIMIENTO'.$i}}">
                                    <option value ="">-Seleccionar-</option>
                                    <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Compra de un activo/inversión'?'selected' : ''}} value="Compra de un activo/inversión">Compra de un activo/inversión</option>
                                    <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Tipo de cambio'?'selected' : ''}} value="Tipo de cambio">Tipo de cambio</option>
                                    <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Incremento la utilización de líneas'?'selected' : ''}} value="Incremento la utilización de líneas">Incremento la utilización de líneas</option>
                                    <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Incrementar nivel de exposición PDM'?'selected' : ''}} value="Incrementar nivel de exposición PDM">Incrementar nivel de exposición PDM</option>
                                    <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Covenants'?'selected' : ''}} value="Covenants">Covenants</option>
                                    <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Digitalización de productos'?'selected' : ''}} value="Digitalización de productos">Digitalización de productos</option>
                                    <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Tarifas especiales'?'selected' : ''}} value="Tarifas especiales">Tarifas especiales</option>
                                    <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Entrega de documentos - renovación de líneas'?'selected' : ''}} value="Entrega de documentos - renovación de líneas">Entrega de documentos - renovación de líneas</option>
                            </select>
                            <select style="{{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO=='Canalización de flujos IN'?'':'display:none'}}" 
                            {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO=='Canalización de flujos IN'?'required':'disabled'}} 
                            identificador="{{'DIVCUMPLIMIENTO'.$i.'_Canalización de flujos IN'}}" 
                            name="{{'TIPO_COMPROMISO_DETALLE_Canalización_de_flujos_IN'.$i}}" id="{{'TIPO_COMPROMISO_DETALLE_Canalización_de_flujos_IN'.$i}}"  class="form-control custom-select is-invalid cerrado {{'DIVCUMPLIMIENTO'.$i}}" >
                                <option value ="">-Seleccionar-</option>
                                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Proveedores'&& $cumplimientos[$i]->TIPO_COMPROMISO=='Canalización de flujos IN'?'selected' : ''}} value="Proveedores">Proveedores</option>
                                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Transferencias'?'selected' : ''}} value="Transferencias">Transferencias</option>
                                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Letras/Facturas'?'selected' : ''}} value="Letras/Facturas">Letras/Facturas</option>
                                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Recaudación'?'selected' : ''}} value="Recaudación">Recaudación</option>
                                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Otros'?'selected' : ''}} value="Otros">Otros</option>
                            </select>
                            <select  style="{{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO=='Incremento de flujos OUT'?'':'display:none'}}" 
                            {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO=='Incremento de flujos OUT'?'required':'disabled'}} identificador="{{'DIVCUMPLIMIENTO'.$i.'_Incremento de flujos OUT'}}"
                            name="{{'TIPO_COMPROMISO_DETALLE_Canalización_de_flujos_OUT'.$i}}" id="{{'TIPO_COMPROMISO_DETALLE_Canalización_de_flujos_OUT'.$i}}"  class="form-control custom-select is-invalid cerrado {{'DIVCUMPLIMIENTO'.$i}}" >
                                <option value ="">-Seleccionar-</option>
                                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Transferencias al exterior'?'selected' : ''}} value="Transferencias al exterior">Transferencias al exterior</option>
                                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Pago de proveedores'?'selected' : ''}} value="Pago de proveedores">Pago de proveedores</option>
                                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Pago SUNAT'?'selected' : ''}} value="Pago SUNAT">Pago SUNAT</option>
                                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Pago AFP'?'selected' : ''}} value="Pago AFP">Pago AFP</option>
                                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Proveedores' && $cumplimientos[$i]->TIPO_COMPROMISO=='Incremento de flujos OUT'?'selected' : ''}} value="Proveedores">Proveedores</option>
                                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Planilla'?'selected' : ''}} value="Planilla">Planilla</option>
                                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Pago varios'?'selected' : ''}} value="Pago varios">Pago varios</option>
                            </select>
        
                            <select style="{{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO=='Garantías'?'':'display:none'}}" 
                            {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO=='Garantías'?'required':'disabled'}} 
                            identificador="{{'DIVCUMPLIMIENTO'.$i.'_Garantías'}}"  class="form-control custom-select is-invalid cerrado {{'DIVCUMPLIMIENTO'.$i}}" name="{{'TIPO_COMPROMISO_DETALLE_Garantías'.$i}}" id="{{'TIPO_COMPROMISO_DETALLE_Garantías'.$i}}">
                                <option value ="">-Seleccionar-</option>
                                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Nueva garantía bien mueble/ inmueble'?'selected' : ''}} value="Nueva garantía bien mueble/ inmueble">Nueva garantía bien mueble/ inmueble</option>
                                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Garantía liquida'?'selected' : ''}} value="Garantía liquida">Garantía liquida</option>
                                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Incremento del valor de gravamen'?'selected' : ''}} value="Incremento del valor de gravamen">Incremento del valor de gravamen</option>
                                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Actualizar tasación'?'selected' : ''}} value="Actualizar tasación">Actualizar tasación</option>
                                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Cesión de flujos'?'selected' : ''}} value="Cesión de flujos">Cesión de flujos</option>
                                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Fideicomisos'?'selected' : ''}} value="Fideicomisos">Fideicomisos</option>
                                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='FEVE'?'selected' : ''}} value="FEVE">FEVE</option>
                            </select>
                            <select  style="{{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO=='Estados Financieros'?'':'display:none'}}" 
                            {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO=='Estados Financieros'?'required':'disabled'}} 
                            identificador="{{'DIVCUMPLIMIENTO'.$i.'_Estados Financieros'}}"
                            name="{{'TIPO_COMPROMISO_DETALLE_Estados_Financieros'.$i}}" id="{{'TIPO_COMPROMISO_DETALLE_Estados_Financieros'.$i}}"  class="form-control custom-select is-invalid cerrado {{'DIVCUMPLIMIENTO'.$i}}" >
                                <option value ="">-Seleccionar-</option>
                                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Actividad'?'selected' : ''}} value="Actividad">Actividad</option>
                                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Liquidez'?'selected' : ''}} value="Liquidez">Liquidez</option>
                                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Solvencia'?'selected' : ''}} value="Solvencia">Solvencia</option>
                                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Cobertura del servicio de deuda'?'selected' : ''}} value="Cobertura del servicio de deuda">Cobertura del servicio de deuda</option>
                                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Endeudamiento'?'selected' : ''}} value="Endeudamiento">Endeudamiento</option>
                                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Rentabilidad'?'selected' : ''}} value="Rentabilidad">Rentabilidad</option>
                            </select>
                            <select  style="{{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO=='Otros'?'':'display:none'}}" 
                            {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO=='Otros'?'required':'disabled'}} 
                            identificador="{{'DIVCUMPLIMIENTO'.$i.'_Otros'}}" name="{{'TIPO_COMPROMISO_DETALLE_Otros'.$i}}" 
                            id="{{'TIPO_COMPROMISO_DETALLE_Otros'.$i}}"  class="form-control custom-select is-invalid cerrado {{'DIVCUMPLIMIENTO'.$i}}">
                                <option value ="">-Seleccionar-</option>
                                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Sustento de deuda en otros bancos'?'selected' : ''}} value="Sustento de deuda en otros bancos">Sustento de deuda en otros bancos</option>
                                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Incremento del Capital social'?'selected' : ''}} value="Incremento del Capital social">Incremento del Capital social</option>
                                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Acuerdos Comite'?'selected' : ''}} value="Acuerdos Comite">Acuerdos Comité</option>
                                <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->TIPO_COMPROMISO_DETALLE=='Otros'?'selected' : ''}} value="Otros">Otros</option>
                            </select>
                        </td>
                        <td>
                            <input maxlength="200"  value="{{isset($cumplimientos[$i])? $cumplimientos[$i]->DETALLE_COMPRA:''}}"  name="{{'DETALLE_COMPRA'.$i}}" id="{{'DETALLE_COMPRA'.$i}}"  type="text" class="form-control cerrado" required>
                        </td>
                        <td>
                            <input step="0.01" value="{{isset($cumplimientos[$i])? $cumplimientos[$i]->INDICADOR:''}}" name="{{'INDICADOR'.$i}}" id="{{'INDICADOR'.$i}}"  oninput="maxLengthMontoCheck(this)" max="999999999"  type="number" class="form-control cerrado"  min="0" required>
                        </td>

                        <td>
                            <input autocomplete="off" class="form-control fechaCompromiso cerrado"  type="text" name="{{'FECHA_COMPROMISO'.$i}}" placeholder="Seleccionar fecha" value="{{isset($cumplimientos[$i])? $cumplimientos[$i]->FECHA_COMPROMISO:''}}" required> 
                        </td>
                        <td>
                        <select name="{{'GESTOR'.$i}}" id="{{'GESTOR'.$i}}"  class="form-control custom-select is-invalid cerrado" required>
                            <option value ="">-Seleccionar-</option>
                            <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->GESTOR=='EJECUTIVO DE NEGOCIO'?'selected' : ''}} value="EJECUTIVO DE NEGOCIO">EJECUTIVO DE NEGOCIO</option>
                            <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->GESTOR=='JEFE ZONAL'?'selected' : ''}} value="JEFE ZONAL">JEFE ZONAL</option>
                            <option {{isset($cumplimientos[$i]) && $cumplimientos[$i]->GESTOR=='EQUIPO SEGUIMIENTO'?'selected' : ''}} value="EQUIPO SEGUIMIENTO">EQUIPO SEGUIMIENTO</option>
                            </select>
                        </td>
                        <td style="padding-left: 10px;font-size: 20px;">
                            <i onclick="{{'del('.$i.')'}}" class="fas fa-times-circle"></i>
                        </td>
                        </tr>
                        @endforeach
                    @endif
                </table>
                @if($vistaHistorica==0 && $privilegios != App\Model\Infinity\Visita::SOLO_LECTURA)
                    <div class="d-flex flex-row-reverse" style="padding-top: 20px;">
                        <button onclick="add(event)"  class="btn btn-primary btn-lg">+ Añadir Nuevo Compromiso</button>
                    </div>
                @endif
                <div style="height:40px;"></div>
                    
              </div>
        </div>
    </div>
    <!-- Cierre de Visita-->

    <!-- <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Rol Validador</h2>
                    <ul class="nav navbar-right panel_toolbox"></ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content form-horizontal">
                    <h2>{{$bucket}}</h2>
                </div>
            </div>
        </div>
    </div> -->

    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Otros</h2>
                    <ul class="nav navbar-right panel_toolbox">
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content form-horizontal">
                    <div class="col-md-6 col-xs-12 col-sm-12">                    
                        <div class="form-group">
                            <label class="control-label col-sm-3 col-xs-12">Fecha Visita</label>
                            <div class="col-sm-6 col-xs-12" >
                                <input required autocomplete="off" class="form-control dfecha"  type="text" name="fechaVisita" placeholder="Seleccionar fecha"
                                       value='{{($visita->getValue('_fechaVisita'))? $visita->getValue('_fechaVisita')->format('Y-m-d'):''}}'>
                            </div>
                        </div>
                        @if($rolesvalidadores!=null)
                            <div class="form-group">
                                <label class="control-label col-sm-3 col-xs-12">Participante IBK 1  <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" 
                                 title="Rol validador acompañante en la llamada. Se le notificará del guardado de la ficha para que pueda validarla."></i></label>
                                <div class="col-sm-6 col-xs-12">
                                    <select required {{($rolesvalidadores->count())==1?'readonly':''}}  class="form-control" name="rolvalidador" id="rolvalidador">
                                            <option {{isset($datoscovid) && $datoscovid->USUARIO_NOTIFICADO==null?'selected':''}} value="">Seleccione a un validador</option>
                                        @foreach($rolesvalidadores as $i => $validadores)
                                            <option {{isset($datoscovid) && $datoscovid->USUARIO_NOTIFICADO==$validadores->REGISTRO?'selected':''}} value="{{$validadores->REGISTRO}}">{{$validadores->NOMBRE}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif
                        <div class="form-group">
                            <label class="control-label col-sm-3 col-xs-12">Nombre del Cliente  <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" 
                        title="Nombre de la persona que entrevistó por parte de la empresa."></i></label>
                            <div class="col-sm-6 col-xs-12">
                                <input type="text" name="nombreVisita" class="form-control" value="{{$visita->getValue('_entrevistadoNombre')}}" required>
                            </div>
                        </div>
                        
                    </div>
                    
                    <div class="col-md-6 col-xs-12 col-sm-12"> 
                        <div class="form-group">
                            <label class="control-label col-sm-3 col-xs-12">Ejecutivo</label>
                            <div class="col-sm-6 col-xs-12">
                                <!-- <input type="text" class="form-control" readonly value="{{in_array($usuario->getValue('_rol'),array(App\Entity\Usuario::ROL_JEFATURA_BE,App\Entity\Usuario::ROL_GERENCIA_ZONAL_BE)) ? $visita->getValue('_registroNombre'):$usuario->getValue('_nombre')}}" > -->
                                <input type="text" class="form-control" readonly value="{{$infoCliente->EJECUTIVO}}" >
                            </div>
                        </div>
                        @if($rolesvalidadores!=null)
                            <div class="form-group">
                                <label class="control-label col-sm-3 col-xs-12">Participante IBK 2  <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" 
                        title="Segundo Rol validador acompañante en la llamada."></i></label>
                                <div class="col-sm-6 col-xs-12">
                                    <select class="form-control" name="rolvalidador2" id="rolvalidador2">
                                        <option {{isset($datoscovid) && $datoscovid->USUARIO_ACOMP==null?'selected':''}} value="">Ninguno</option>
                                        @foreach($rolesvalidadores as $i => $validadores)
                                            <option {{isset($datoscovid) && $datoscovid->USUARIO_ACOMP==$validadores->REGISTRO?'selected':''}} value="{{$validadores->REGISTRO}}">{{$validadores->NOMBRE}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif
                     
                        <div class="form-group">
                            <label class="control-label col-sm-3 col-xs-12">Cargo</label>
                            <div class="col-sm-6 col-xs-12">
                                <select class="form-control" name="cargoVisita" required>
                                    <option value="">Seleccione un cargo</option>
                                    @foreach ($cargosEntrevistado as $cargo)
                                    <option value="{{$cargo}}" {{$visita->getValue('_entrevistadoCargo') == $cargo?'selected':''}}>{{$cargo}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <input type="hidden" name="idHistorico" value="{{$cliente->getValue('_idHistorico')}}">
                    <input type="hidden" name="visita" value="{{$visita->getValue('_id')}}">
                    @if($privilegios==App\Model\Infinity\Visita::LLENADOR_BLINDAJE || ($privilegios == App\Model\Infinity\Visita::ROL_VALIDADOR && $flgGuardado == '0'))
                    @if(($privilegios == App\Model\Infinity\Visita::ROL_VALIDADOR && $flgGuardado == '0'))
                        <div class="col-md-12 col-xs-12 col-sm-12"> 
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12">Comentario de Rol Validador:</label>
                                <div class="input-group col-md-8 col-sm-8 col-xs-12">
                                    <textarea required maxlength="800" minlength="30" class="form-control" rows="10" style="resize: none" placeholder="Ingresar Comentario..." name="comentarioVisitaRV" >{{$visita->getValue('_comentariosRV')}}</textarea>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="col-md-12 col-xs-12 col-sm-12"> 
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12">Comentario:</label>
                                <div class="input-group col-md-8 col-sm-8 col-xs-12">
                                    <textarea required maxlength="800" minlength="30" class="form-control" rows="10" style="resize: none" placeholder="Ingresar Comentario..." name="comentarioVisita" >{{$visita->getValue('_comentarios')}}</textarea>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div style="display:flex;justify-content:center;align-items:center;"  class="col-md-12 col-sm-12 col-xs-12">
                        <button id="continuar" class="btn btn-lg btn-success" type="submit">Guardar y Aprobar</button>
                    </div>
                    @elseif($privilegios == App\Model\Infinity\Visita::ROL_VALIDADOR && $flgGuardado == '1')
                    <div class="col-md-12 col-xs-12 col-sm-12"> 
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Comentario Comercial:</label>
                            <div class="input-group col-md-8 col-sm-8 col-xs-12">
                                <textarea required maxlength="800" minlength="30" class="form-control" rows="10" style="resize: none" placeholder="Ingresar Comentario..." name="comentarioVisita" >{{$visita->getValue('_comentarios')}}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12"> 
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Comentario de Rol Validador:</label>
                            <div class="input-group col-md-8 col-sm-8 col-xs-12">
                                <textarea required maxlength="800" minlength="30" class="form-control" rows="10" style="resize: none" placeholder="Ingresar Comentario..." name="comentarioVisitaRV" >{{$visita->getValue('_comentariosRV')}}</textarea>
                            </div>
                        </div>
                    </div>
                    <div style="display:flex;justify-content:center;align-items:center;"  class="col-md-12 col-sm-12 col-xs-12">
                        <button id="continuar" class="btn btn-lg btn-success" type="submit">Aprobar</button>
                    </div>
                    @elseif($privilegios==App\Model\Infinity\Visita::LLENADOR_SIN_BLINDAJE)
                    <div class="col-md-12 col-xs-12 col-sm-12"> 
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Comentario:</label>
                            <div class="input-group col-md-8 col-sm-8 col-xs-12">
                                <textarea required maxlength="800" minlength="30" class="form-control" rows="10" style="resize: none" placeholder="Ingresar Comentario..." name="comentarioVisita" >{{$visita->getValue('_comentarios')}}</textarea>
                            </div>
                        </div>
                    </div>
                    <div style="display:flex;justify-content:center;align-items:center;"  class="col-md-12 col-sm-12 col-xs-12">
                        <button id="continuar" class="btn btn-primary" type="submit">Guardar</button>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<div class="form-row row" style="visibility:hidden;">
  <input id="cumplimientosCounter" name="cumplimientosCounter" value="{{count($cumplimientos)}}"  type="text">
</div>
    <div id="modalZonaOperaciones" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row">
                        @foreach ($zonas as $zona)
                        <div class="checkbox col-sm-3 col-xs-6" style="margin-top: 0px;">
                            <label>
                                <input type="checkbox" value="{{$zona}}" name="zonaOperacion[]" 
                                       {{isset($cliente->getValue('_zonaOperaciones')[$zona])?'checked':''}}>{{$zona}}
                            </label>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Listo</button>
                </div>
            </div>
        </div>
    </div>

    <div id="modalZonaClientes" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row">
                        @foreach ($zonas as $zona)
                        <div class="checkbox col-sm-3 col-xs-6" style="margin-top: 0px;">
                            <label>
                                <input type="checkbox" value="{{$zona}}" name="zonaCliente[]" 
                                       {{isset($cliente->getValue('_zonaClientes')[$zona])?'checked':''}}>{{$zona}}
                            </label>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Listo</button>
                </div>
            </div>
        </div>
    </div>

    <!-- /.Modal Inversión-->
    <div class="modal fade" tabindex="-1" role="dialog" id="modalInversion">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="min-height: 240px">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4>Seleccione tipo de Inversión</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        @foreach ($inversiones as $inv)
                        <div class="checkbox col-sm-6 col-xs-6" style="margin-top: 0px;">
                            <label>
                                <input type="checkbox" value="{{$inv}}" name="inversionCliente[]" 
                                       {{isset($cliente->getValue('_inversiones')[$inv])?'checked':''}}>{{$inv}}
                            </label>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Listo</button>
                </div>            
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
     <!-- /.Modal Inversión-->
     <div class="modal fade" tabindex="-1" role="dialog" id="modalConfirmar">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="
                            text-align: center;">
                <div class="modal-header" style="
                            border-bottom-style: none;">
                    <h4>¿Confirma el envió de la ficha?</h4>
                </div>
                <div class="modal-footer" style="
                            display: flex;
                            justify-content: center;">
                    <button id="modalConfirmarBtn" data-dismiss="modal"  class="btn btn-primary">Si, confirmo</button>
                </div>            
            </div>
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="modal fade" tabindex="-1" role="dialog" id="modalOk">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="
                            text-align: center;">
                <div class="modal-header" style="
                            border-bottom-style: none;">
                    <h4>¡Listo! ¡Recibimos tu ficha!</h4>
                </div>         
            </div>
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="modal fade" tabindex="-1" role="dialog" id="modalError">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="
                            text-align: center;">
                <div class="modal-header" style="
                            border-bottom-style: none;">
                    <h4>¡Uy! ¡Hubo un error!</h4>
                </div>         
            </div>
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    @endif

</form>


@stop

@section('js-scripts')
<script>
$('select, input').on('change',function(e){
  if(this.id!='slcQ14_5'){
    $('#slcQ14_5 option[value="4"]').remove();
    $('#slcQ14_5 option[value="5"]').remove();
    $('#slcQ14_5 option[value="6"]').remove();
    const feveArray = ['Sin FEVE'];
    const planArray = ['Crecer','Mantener'];
    const impactoArray = ['sin impacto','bajo'];

    const feve = $('#slcQ14_2').val();
    const plan = $('#slcQ14_1').val();
    var slcQ13_3 = "input:checkbox[name='slcQ13_3']";
    var slcQ13_2 = "input:checkbox[name='slcQ13_2']";
    var slcQ13_1 = "input:checkbox[name='slcQ13_1']";
    var slcQ13_0 = "input:checkbox[name='slcQ13_0']";
    var impacto = '';
  if($(slcQ13_0).is(":checked")){
      impacto = $(slcQ13_0).val();
  }
  if($(slcQ13_1).is(":checked")){
    impacto = $(slcQ13_1).val();
  }
  if($(slcQ13_2).is(":checked")){
    impacto = $(slcQ13_2).val();
  }
  if($(slcQ13_3).is(":checked")){
    impacto = $(slcQ13_3).val();
  }
    if(feveArray.includes(feve) && planArray.includes(plan) && impactoArray.includes(impacto)){
        $('#slcQ14_5').append('<option value="4">4</option>');
        $('#slcQ14_5').append('<option value="5">5</option>');
        $('#slcQ14_5').append('<option value="6">6</option>');
    }else{
        $('#slcQ14_5 option[value="4"]').remove();
        $('#slcQ14_5 option[value="5"]').remove();
        $('#slcQ14_5 option[value="6"]').remove();
    }
  }
});
var formValidation = false;
$("#frmVisita").submit(function(e){
    if(!document.formValidation){
        e.preventDefault();  
        $('#modalConfirmar').modal();
        return false;
    }
});
$('#modalConfirmarBtn').click(function(){
    document.formValidation = true;
    $('#frmVisita').submit();
});
$(document).ready(function () {
$('.slcQ11').on('change',function(){
    const bcp = $('#slcQ11_1_1').is(":checked");
    const bbva = $('#slcQ11_2_1').is(":checked");
    const scotia = $('#slcQ11_3_1').is(":checked");
    const pichincha = $('#slcQ11_4_1').is(":checked");
    const banbif = $('#slcQ11_5_1').is(":checked");
    const otros = $('#slcQ11_6_1').is(":checked");
    const suma = bcp+bbva+scotia+pichincha+banbif+otros;
    if(suma==0){
        $('.slcQ11').attr('required',true);
    }else{
        $('.slcQ11').attr('required',false);
    }
});
    $('#slcQ7').on('change',function(){
        var val = $(this).val();
        switch (val) {
            case "1": //Importacion
                $('#slcQ7_2_1').val(0);
                $('#slcQ7_2_1').attr('disabled',true);
                $('#slcQ7_2_2').attr('disabled',false);
            break;
            case "2": //Exportacion
                $('#slcQ7_2_2').val(0);
                $('#slcQ7_2_2').attr('disabled',true);
                $('#slcQ7_2_1').attr('disabled',false);
            break;
            case "3": //AMBAS
                $('#slcQ7_2_2').attr('disabled',false);
                $('#slcQ7_2_1').attr('disabled',false);
            break;
        
            default:
                break;
        }
    });
      //INHABILITAR SI ES QUE EL USUARIO NO TIENE PERMISO EN EDITAR LA VISTA
  @if ($flgGuardado=="1" && $privilegios == App\Model\Infinity\Visita::LLENADOR_SIN_BLINDAJE)
    $('.cerrado').attr('readonly','readonly');
    $('.cerrado').attr('onclick','return false;');
    $(".fa-times-circle").css('display','none');
    $('select').attr('disabled',true);
    $('input').attr('disabled',true);
    $(".btn").attr('disabled',true);
    $('.status').attr('disabled',false);
  @endif
  @if ($privilegios == App\Model\Infinity\Visita::SOLO_LECTURA)
    $('.cerrado').attr('readonly','readonly');
    $('.cerrado').attr('onclick','return false;');
    $(".fa-times-circle").css('display','none');
    $('select').attr('disabled',true);
    $('input').attr('disabled',true);
    $('textarea').attr('disabled',true);
    $(".btn").attr('disabled',true);
    $('.status').attr('disabled',false);
  @endif
  //LINEAS
    @if($flgGuardado=="1" )
    
    @else
    var grupoCheck = $('#flagCambioLineas').closest('.grupoCheck');
    var inputSelect = grupoCheck.find('input, select, textarea');
    if ($('#flagCambioLineas').attr('checked') == undefined) {
        inputSelect.attr('disabled', '');
        $('#flagCambioLineas').removeAttr('disabled');
    } else {
        $('#flagCambioLineas').attr('checked', true);
        inputSelect.removeAttr('disabled');
    }
    @endif
    
    var counter=0;
    var x = document.getElementsByName("inversionCliente[]");
    for (let index = 0; index < x.length; index++) {
        const element = x[index];
        if(element.checked){
            counter++;
        }
    }
    $('#tipoInversionV').val(counter);
    if(counter>0 && counter<(x.length+1)){
        $("#tipoInversionV").get(0).setCustomValidity("");
    }else{
        $('#tipoInversionV').get(0).setCustomValidity('Debes elegir por lo menos un tipo de inversión. Dale click a la imagen del mouse.');
    }
  //validateQuestion10(event,document.getElementById('slcQ10_2'));
  //validateQuestion5(event,document.getElementById('slcQ5_2'));
  //validateQuestion6(event,document.getElementById('slcQ6_2'));
  //validateQuestion9(event,document.getElementById('slcQ9_2'));
  //validateQuestion10(event,document.getElementById('slcQ10_1'));
  //validateQuestion5(event,document.getElementById('slcQ5_1'));
 // validateQuestion6(event,document.getElementById('slcQ6_1'));
  //validateQuestion9(event,document.getElementById('slcQ9_1'));
  var slcQ13_2 = "input:checkbox[name='slcQ13_2']";
  var slcQ13_3 = "input:checkbox[name='slcQ13_3']";
  var slcQ13_1 = "input:checkbox[name='slcQ13_1']";
  if($(slcQ13_1).is(":checked")){
    $(slcQ13_2).prop('required',false);
    $(slcQ13_3).prop('required',false);
  }
  if($(slcQ13_2).is(":checked")){
    $(slcQ13_3).prop('required',false);
    $(slcQ13_1).prop('required',false);
  }
  if($(slcQ13_3).is(":checked")){
    $(slcQ13_2).prop('required',false);
    $(slcQ13_1).prop('required',false);
  }

    $('.dfecha').each(function () {
        $(this).on('keydown', function () {
            return false;
        });
        $(this).datepicker({
            maxViewMode: 1,
            language: "es",
            autoclose: true,
            startDate: "-365d",
            endDate: "0d",
            format: "yyyy-mm-dd",
          });
    });
    $('.fechaCompromiso').each(function () {
        $(this).on('keydown', function () {
            return false;
        });
        const today = new Date();
        const tomorrow = new Date(today);
        tomorrow.setDate(tomorrow.getDate() + 1);
        $(this).datepicker({
            maxViewMode: 1,
            language: "es",
            autoclose: true,
            startDate: tomorrow.toISOString().substring(0, 10),
            format: "yyyy-mm-dd",
        });
    });
});
function show(id,input){
    elements = document.getElementsByClassName(id);
    val = input.value;
    for (let index = 0; index < elements.length; index++) {
        const element = elements[index];
        attribute = element.getAttribute('identificador');
        parteNumero = attribute.split("_")[1];
        console.log(val,attribute,parteNumero);
        if(val==''){
            check = false;
        }else{
            check = parteNumero.includes(val.toString());
        }
        var inputNodes = element.getElementsByTagName('INPUT');
        for(var i = 0; i < inputNodes.length; ++i){
            var inputNode = inputNodes[i];
            var name = inputNode.getAttribute('name');
            if(check){
                var checked = inputNode.checked;
                inputNode.disabled = false;
                if(id=='DIV11'){
                    const bcp = $('#slcQ11_1_1').is(":checked");
                    const bbva = $('#slcQ11_2_1').is(":checked");
                    const scotia = $('#slcQ11_3_1').is(":checked");
                    const pichincha = $('#slcQ11_4_1').is(":checked");
                    const banbif = $('#slcQ11_5_1').is(":checked");
                    const otros = $('#slcQ11_6_1').is(":checked");
                    const suma = bcp+bbva+scotia+pichincha+banbif+otros;
                    if(suma==0){
                        $('.slcQ11').attr('required',true);
                    }
                }else{
                    inputNode.required = true;    
                }
            }else{
                inputNode.required = false;
                inputNode.disabled = true;
            }
        }
        var inputNodes = element.getElementsByTagName('SELECT');
        console.log(check);
        for(var i = 0; i < inputNodes.length; ++i){
            var inputNode = inputNodes[i];
            if(check){
                inputNode.disabled = false;
                inputNode.required = true;
            }else{
                inputNode.required = false;
                inputNode.disabled = true;
            }
        }
        if(inputNodes.length==0){
            if(check){
                element.disabled = false;
                element.required = true;
            }else{
                element.disabled = true;
                element.required = false;
            }
        }
        if(check){
            element.style.display = "block";
        }else{
            element.style.display = "none";
        }
    }
}
    //INHABILITAR SI ES QUE EL USUARIO NO TIENE PERMISO EN EDITAR LA VISTA
@if ($flgGuardado=="1" && $privilegios == App\Model\Infinity\Visita::LLENADOR_SIN_BLINDAJE)
        $('form :input').attr("readonly", true);
        $('btn').attr("disabled", true);
        $('#continuar').attr("disabled",false);
    @endif
@if ($privilegios == App\Model\Infinity\Visita::SOLO_LECTURA)
        $('form :input').attr("readonly", true);
        $('btn').attr("disabled", true);
        $('#continuar').attr("disabled",false);
    @endif
$('[data-toggle="tooltip"]').tooltip();
if ($('#soloLectura').val() == '1') {
        $(':input').attr('disabled', 'true');
        $('#codunico').attr('disabled',false);
    }

$('#inputProyeccionInversion').on('change', function (e) {
        var valorMinimo = 1;
        $('#tipoInversionV').get(0).setCustomValidity('Debes elegir por lo menos un tipo de inversión. Dale click a la imagen del mouse.');
        if ($(this).val() > valorMinimo) {
            $('#btnInversion').removeClass('hidden');
            $('#tipoInversionV').attr('required',true);
            $('#tipoInversionV').attr('disabled',null);
        } else{
            $('#btnInversion').addClass('hidden');
            $('#tipoInversionV').attr('required',false);
            $('#tipoInversionV').attr('disabled','disabled');
            $('#tipoInversionV').val(0);
            $("input[name='inversionCliente[]']").prop('checked',false);
        }
    });


$('#btnInversion').on('click', function (e) {
        $('#modalInversion').modal();
    });

function checkFormStatus() {

        if ($('#flagCambioMixVentas').prop("checked")) {
            $('#panelMixVentas').removeClass('hidden');
        }
        
        if ($('#flagCambioModelo').prop("checked")) {
            $('#panelModeloNegocio').removeClass('hidden');
        }

        if ($('#flagCambioOperaciones').prop("checked")) {
            $('#btnZonasOperaciones').removeAttr('disabled');
        }
        if ($('#flagCambioZonaClientes').prop("checked")) {
            $('#btnZonasClientes').removeAttr('disabled');
        }

        if ($('#flagCambioConcentracionVentas').prop("checked")) {
            $('.inputConcentCli').removeAttr('disabled');
        }

        if ($('#flagCambioConcentracionProveedores').prop("checked")) {
            $('.inputConcentProv').removeAttr('disabled');
        }

        $('.check:checked').closest('.grupoCheck').find(':input').removeAttr('disabled');

    }

checkFormStatus();
$('select[name="flagCambioInversionActivoPatrimonio"]').change(function () {
        $('#lblCambioInversionActivoPatrimonio').toggleClass('hidden');
    });

    /*Checks de los flag de cambiko*/
$('#flagCambioOperaciones').change(function () {
        $('#btnZonasOperaciones').prop('disabled', (_, val) => !val);
    });

$('#flagCambioZonaClientes').change(function () {
        $('#btnZonasClientes').prop('disabled', (_, val) => !val);
    });

$('#flagCambioMixVentas').change(function () {
        $('#panelMixVentas').toggleClass('hidden');
    });

$('#flagCambioModelo').change(function () {
        $('#panelModeloNegocio').toggleClass('hidden');
    });

$('#flagCambioConcentracionVentas').change(function () {
        if ($(this).attr('checked') != undefined) {
            $(this).removeAttr('checked');
            $('.inputConcentCli').attr('disabled', '');
        } else {
            $(this).attr('checked', true);
            $('.inputConcentCli').removeAttr('disabled');
        }
    });

$('#flagCambioConcentracionProveedores').change(function () {
        if ($(this).attr('checked') != undefined) {
            $(this).removeAttr('checked');
            $('.inputConcentProv').attr('disabled', '');
        } else {
            $(this).attr('checked', true);
            $('.inputConcentProv').removeAttr('disabled');
        }
    });

$('.check').change(function () {
        //Debo de conseguir todos los input del grupo
        var grupoCheck = $(this).closest('.grupoCheck');

        var inputSelect = grupoCheck.find('input, select, textarea');

        if ($(this).attr('checked') != undefined) {
            inputSelect.attr('disabled', '');
            $(this).removeAttr('checked');
            $(this).removeAttr('disabled');
        } else {
            $(this).attr('checked', true);
            inputSelect.removeAttr('disabled');
        }
    });
$('.dfecha').each(function () {
        $(this).on('keydown', function () {
            return false;
        });

        $(this).datepicker({
            maxViewMode: 1,
            //daysOfWeekDisabled: "0,6",
            language: "es",
            autoclose: true,
            startDate: "-365d",
            endDate: "0d",
            format: "yyyy-mm-dd",
        });
    });

function daysBetween(one, another) {
  return Math.round(Math.abs((one) - (another))/8.64e7);
}
function add(e){
  e = e || window.event;
  e.preventDefault();
  table = document.getElementById('cumplimientos');
  rows = table.getElementsByTagName('tr');
  index = rows.length - 1;
  counter = document.getElementById('cumplimientosCounter');
  counter.setAttribute("value", index+1);
  
  table.insertAdjacentHTML('beforeend', `
  <tr>
    <td>
      <select name="TIPO_GESTION`+index+`" id="TIPO_GESTION`+index+`" class="form-control" required>
        @if(count($revision)>0)
            @foreach(range(0,count($revision)-1) as $j)
                <option value="{{$revision[$j]}}">{{$revision[$j]}}</option>
            @endforeach
        @endif
      </select>
    </td>
    <td>
      <select onchange="show('DIVCUMPLIMIENTO`+index+`',this)" name="TIPO_COMPROMISO`+index+`" id="TIPO_COMPROMISO`+index+`" class="form-control custom-select is-invalid" required>
        <option value ="">-Seleccionar-</option>
        <option value="Comerciales">Comerciales</option>
        <option value="Canalización de flujos IN">Canalización de flujos IN</option>
        <option value="Incremento de flujos OUT">Incremento de flujos OUT</option>
        <option value="Garantías">Garantías</option>
        <option value="Estados Financieros">Estados Financieros</option>
        <option value="Otros">Otros</option>
      </select>
    </td>
    <td>
      <select style="display:none" identificador="DIVCUMPLIMIENTO`+index+`_Comerciales" name="TIPO_COMPROMISO_DETALLE_Comerciales`+index+`" id="TIPO_COMPROMISO_DETALLE_Comerciales`+index+`"  class="form-control custom-select is-invalid DIVCUMPLIMIENTO`+index+`" required>
        <option value="Compra de un activo/inversión">Compra de un activo/inversión</option>
        <option value="Tipo de cambio">Tipo de cambio</option>
        <option value="Incremento la utilización de líneas">Incremento la utilización de líneas</option>
        <option value="Incrementar nivel de exposición PDM">Incrementar nivel de exposición PDM</option>
        <option value="Covenants">Covenants</option>
        <option value="Digitalización de productos">Digitalización de productos</option>
        <option value="Tarifas especiales">Tarifas especiales</option>
        <option value="Entrega de documentos - renovación de líneas">Entrega de documentos - renovación de líneas</option>
      </select>
      <select style="display:none" identificador="DIVCUMPLIMIENTO`+index+`_Canalización de flujos IN" name="TIPO_COMPROMISO_DETALLE_Canalización_de_flujos_IN`+index+`" id="TIPO_COMPROMISO_DETALLE_Canalización_de_flujos_IN`+index+`"  class="form-control custom-select is-invalid DIVCUMPLIMIENTO`+index+`" required>
        <option value="Proveedores">Proveedores</option>
        <option value="Transferencias">Transferencias</option>
        <option value="Letras/Facturas">Letras/Facturas</option>
        <option value="Recaudación">Recaudación</option>
        <option value="Otros">Otros</option>
      </select>
      <select style="display:none" identificador="DIVCUMPLIMIENTO`+index+`_Incremento de flujos OUT" name="TIPO_COMPROMISO_DETALLE_Canalización_de_flujos_OUT`+index+`" id="TIPO_COMPROMISO_DETALLE_Canalización_de_flujos_OUT`+index+`"  class="form-control custom-select is-invalid DIVCUMPLIMIENTO`+index+`" required>
        <option value="Transferencias al exterior">Transferencias al exterior</option>
        <option value="Pago de proveedores">Pago de proveedores</option>
        <option value="Pago SUNAT">Pago SUNAT</option>
        <option value="Pago AFP">Pago AFP</option>
        <option value="Proveedores">Proveedores</option>
        <option value="Planilla">Planilla</option>
        <option value="Pago varios">Pago varios</option>
      </select>
      <select style="display:none" identificador="DIVCUMPLIMIENTO`+index+`_Garantías" name="TIPO_COMPROMISO_DETALLE_Garantías`+index+`" id="TIPO_COMPROMISO_DETALLE_Garantías`+index+`"  class="form-control custom-select is-invalid DIVCUMPLIMIENTO`+index+`" required>
        <option value="Nueva garantía bien mueble/inmueble">Nueva garantía bien mueble/inmueble</option>
        <option value="Nueva hipoteca">Nueva hipoteca</option>
        <option value="Incremento del valor de gravamen">Incremento del valor de gravamen</option>
        <option value="Actualizar tasación">Actualizar tasación</option>
        <option value="Cesión de flujos">Cesión de flujos</option>
        <option value="Fideicomisos">Fideicomisos</option>
        <option value="FEVE">FEVE</option>
      </select>
      <select style="display:none" identificador="DIVCUMPLIMIENTO`+index+`_Estados Financieros" name="TIPO_COMPROMISO_DETALLE_Estados_Financieros`+index+`" id="TIPO_COMPROMISO_DETALLE_Estados_Financieros`+index+`" class="form-control custom-select is-invalid DIVCUMPLIMIENTO`+index+`" required>
        <option value="Endeudamiento">Endeudamiento</option>
        <option value="Solvencia">Solvencia</option>
        <option value="Cobertura del servicio de deuda">Cobertura del servicio de deuda</option>
        <option value="Liquidez">Liquidez</option>
        <option value="Actividad">Actividad</option>
        <option value="Rentabilidad">Rentabilidad</option>
      </select>
      <select style="display:none" identificador="DIVCUMPLIMIENTO`+index+`_Otros" name="TIPO_COMPROMISO_DETALLE_Otros`+index+`" id="TIPO_COMPROMISO_DETALLE_Otros`+index+`" class="form-control custom-select is-invalid DIVCUMPLIMIENTO`+index+`" required>
        <option value="Sustento de deuda en otros bancos">Sustento de deuda en otros bancos</option>
        <option value="Incremento del Capital social">Incremento del Capital social</option>
        <option value="Acuerdos Comite">Acuerdos Comité</option>
        <option value="Otros">Otros</option>
      </select>
    </td>
    <td>
      <input maxLength="200" name="DETALLE_COMPRA`+index+`" id="DETALLE_COMPRA`+index+`" type="text" class="form-control" required>
    </td>
    <td>
      <input step="0.01" oninput="maxLengthMontoCheck(this)" min="0" max="999999999" name="INDICADOR`+index+`" id="INDICADOR`+index+`" type="number" class="form-control" required>
    </td>
    <td>
      <input autocomplete="off" class="form-control fechaCompromiso"  type="text" name="FECHA_COMPROMISO`+index+`" placeholder="Seleccionar fecha" required> 
    </td>
    <td>
    <select name="GESTOR`+index+`" id="GESTOR`+index+`"  class="form-control custom-select  is-invalid" required>
        <option value ="">-Seleccionar-</option>
        <option value="EJECUTIVO DE NEGOCIO">EJECUTIVO DE NEGOCIO</option>
        <option value="JEFE ZONAL">JEFE ZONAL</option>
        <option value="EQUIPO SEGUIMIENTO">EQUIPO SEGUIMIENTO</option>
      </select>
    </td>
    <td style="padding-left: 10px;font-size: 20px;">
      <i onclick="del('`+index+`')" class="fas fa-times-circle"></i>
    </td>
  </tr>
  `);
  $('.dfecha').each(function () {
        $(this).on('keydown', function () {
            return false;
        });
        $(this).datepicker({
            maxViewMode: 1,
            language: "es",
            autoclose: true,
            startDate: "-365d",
            endDate: "0d",
            format: "yyyy-mm-dd",
          });
        });
        $('.fechaCompromiso').each(function () {
        $(this).on('keydown', function () {
            return false;
        });
        $(this).datepicker({
            maxViewMode: 1,
            language: "es",
            autoclose: true,
            startDate: "0d",
            format: "yyyy-mm-dd",
          });
        });
}
$('#slcQ14_1').on('change',function(){
    const val = $(this).val();
    if(val=='Crecer'){
        $('#montoCrecer').attr('required',true);
        $('#montoCrecer').attr('disabled',false);
        $('#detalleCrecer').attr('required',true);
        $('#detalleCrecer').attr('disabled',false);
        $('#crecerWrapper').css('display','block');
    }else{
        $('#montoCrecer').attr('required',false);
        $('#montoCrecer').attr('disabled',true);
        $('#detalleCrecer').attr('required',false);
        $('#detalleCrecer').attr('disabled',true);
        $('#crecerWrapper').css('display','none');
    }
});
$('[name="inversionCliente[]"]').on('change',function(){
    var counter=0;
    var x = document.getElementsByName("inversionCliente[]");
    for (let index = 0; index < x.length; index++) {
        const element = x[index];
        if(element.checked){
            counter++;
        }
    }
    $('#tipoInversionV').val(counter);
    if(counter>0 && counter<(x.length+1)){
        $("#tipoInversionV").get(0).setCustomValidity("");
    }else{
        $('#tipoInversionV').get(0).setCustomValidity('Debes elegir por lo menos un tipo de inversión. Dale click a la imagen del mouse.');
    }
});
function del(indexOrigi){
  realIndex = parseInt(indexOrigi)+1;
  document.getElementById('cumplimientos').deleteRow(realIndex);
  var element = document.getElementById('ID_CUMPL'+indexOrigi);
  if(element!=null){
      element.remove();
  }
  table = document.getElementById('cumplimientos');
  rows = table.getElementsByTagName('tr');
  i = rows.length - 1;
  counter = document.getElementById('cumplimientosCounter');
  counter.setAttribute("value",realIndex-1);
  for (let j = 1; j < rows.length; j++) {
    const elementRow = rows[j];
    index = j-1;
    cell_1 = elementRow.cells[0];
    input1 = cell_1.getElementsByTagName('SELECT')[0];
    input1.setAttribute('name','TIPO_GESTION'+index);
    input1.setAttribute('id','TIPO_GESTION'+index);
    cell_2 = elementRow.cells[1];
    input2 = cell_2.getElementsByTagName('SELECT')[0];
    input2.setAttribute('onchange',"show('DIVCUMPLIMIENTO"+index+"',this)");
    input2.setAttribute('name','TIPO_COMPROMISO'+index);
    input2.setAttribute('id','TIPO_COMPROMISO'+index);
    cell_3 = elementRow.cells[2];
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    input3 = cell_3.getElementsByTagName('SELECT')[0];
    input3.setAttribute('name','TIPO_COMPROMISO_DETALLE_Comerciales'+index);
    input3.setAttribute('id','TIPO_COMPROMISO_DETALLE_Comerciales'+index);
    input3.setAttribute('identificador','DIVCUMPLIMIENTO'+index+'_Comerciales');
    input3.setAttribute('class','form-control custom-select is-invalid cerrado DIVCUMPLIMIENTO'+index);
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    input3 = cell_3.getElementsByTagName('SELECT')[1];
    input3.setAttribute('name','TIPO_COMPROMISO_DETALLE_Canalización_de_flujos_IN'+index);
    input3.setAttribute('id','TIPO_COMPROMISO_DETALLE_Canalización_de_flujos_IN'+index);
    input3.setAttribute('identificador','DIVCUMPLIMIENTO'+index+'_Canalización de flujos IN');
    input3.setAttribute('class','form-control custom-select is-invalid cerrado DIVCUMPLIMIENTO'+index);
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    input3 = cell_3.getElementsByTagName('SELECT')[2];
    input3.setAttribute('name','TIPO_COMPROMISO_DETALLE_Canalización_de_flujos_OUT'+index);
    input3.setAttribute('id','TIPO_COMPROMISO_DETALLE_Canalización_de_flujos_OUT'+index);
    input3.setAttribute('identificador','DIVCUMPLIMIENTO'+index+'_Incremento de flujos OUT');
    input3.setAttribute('class','form-control custom-select is-invalid cerrado DIVCUMPLIMIENTO'+index);
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    input3 = cell_3.getElementsByTagName('SELECT')[3];
    input3.setAttribute('name','TIPO_COMPROMISO_DETALLE_Garantías'+index);
    input3.setAttribute('id','TIPO_COMPROMISO_DETALLE_Garantías'+index);
    input3.setAttribute('identificador','DIVCUMPLIMIENTO'+index+'_Garantías');
    input3.setAttribute('class','form-control custom-select is-invalid cerrado DIVCUMPLIMIENTO'+index);
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    input3 = cell_3.getElementsByTagName('SELECT')[4];
    input3.setAttribute('name','TIPO_COMPROMISO_DETALLE_Estados_Financieros'+index);
    input3.setAttribute('id','TIPO_COMPROMISO_DETALLE_Estados_Financieros'+index);
    input3.setAttribute('identificador','DIVCUMPLIMIENTO'+index+'_Estados Financieros');
    input3.setAttribute('class','form-control custom-select is-invalid cerrado DIVCUMPLIMIENTO'+index);
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    input3 = cell_3.getElementsByTagName('SELECT')[5];
    input3.setAttribute('name','TIPO_COMPROMISO_DETALLE_Otros'+index);
    input3.setAttribute('id','TIPO_COMPROMISO_DETALLE_Otros'+index);
    input3.setAttribute('identificador','DIVCUMPLIMIENTO'+index+'_Otros');
    input3.setAttribute('class','form-control custom-select is-invalid cerrado DIVCUMPLIMIENTO'+index);
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    cell_4 = elementRow.cells[3];
    input4 = cell_4.getElementsByTagName('INPUT')[0];
    input4.setAttribute('name','DETALLE_COMPRA'+index);
    input4.setAttribute('id','DETALLE_COMPRA'+index);
    cell_5 = elementRow.cells[4];
    input5 = cell_5.getElementsByTagName('INPUT')[0];
    input5.setAttribute('name','INDICADOR'+index);
    input5.setAttribute('id','INDICADOR'+index);
    cell_6 = elementRow.cells[5];
    input6 = cell_6.getElementsByTagName('INPUT')[0];
    input6.setAttribute('name','FECHA_COMPROMISO'+index);
    input6.setAttribute('id','FECHA_COMPROMISO'+index);
    cell_7 = elementRow.cells[6];
    input7 = cell_7.getElementsByTagName('SELECT')[0];
    input7.setAttribute('name','GESTOR'+index);
    input7.setAttribute('id','GESTOR'+index);
    cell_8 = elementRow.cells[7];
    input8 = cell_8.getElementsByTagName('i')[0];
    input8.setAttribute('onclick','del('+index+')');
  }
}

function showCheckBox(id,input){
    elements = document.getElementsByClassName(id);
    val = input.checked;
    for (let index = 0; index < elements.length; index++) {
      const element = elements[index];
      attribute = element.getAttribute('identificador');
      parteBOOL = attribute.split("_")[1];
      if(val){
        check = parteBOOL=="TRUE";
      }else{
        check = parteBOOL=="FALSE";
      }
      var inputNodes = element.getElementsByTagName('INPUT');
      for(var i = 0; i < inputNodes.length; ++i){
          var inputNode = inputNodes[i];
          if(check){
            inputNode.disabled = false;
            inputNode.required = true;
          }else{
            inputNode.required = false;
            inputNode.disabled = true;
          }
      }
      var inputNodes = element.getElementsByTagName('SELECT');
      for(var i = 0; i < inputNodes.length; ++i){
          var inputNode = inputNodes[i];
          if(check){
            inputNode.disabled = false;
            inputNode.required = true;
          }else{
            inputNode.required = false;
            inputNode.disabled = true;
          }
      }
      if(check){
        element.style.display = "block";
      }else{
        element.style.display = "none";
      }
    }
}
function maxLengthCheck(object) {
  if (Math.abs(object.value) > 100){
      if(object.value<0){
        object.value = object.value.slice(0, 3)
      }else{
        object.value = object.value.slice(0, 2)
      }
    }
}
function maxLengthCheckYear(object) {
  if (Math.abs(object.value) >= 2021){
    object.value = object.value.slice(0, 3)
    }else{
        object.value = object.value.slice(0, 4)
    }
}
function maxLengthMontoCheck(object) {
  if (Math.abs(object.value) > 999999999){
      if(object.value<0){
        object.value = object.value.slice(0, 10)
      }else{
        object.value = object.value.slice(0, 9)
      }
    }
}
function maxLengthCheck5(object) {
  if (Math.abs(object.value) > 999){
      if(object.value<0){
        object.value = object.value.slice(0, 4)
      }else{
        object.value = object.value.slice(0, 3)
      }
    }
}
function maxLengthPlazoCheck(object) {
  if (object.value > 999){
        object.value = object.value.slice(0, 3)
  }
}

function isNumeric (evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode (key);
    var regex = /[0-9]|\./;
    if ( !regex.test(key) ) {
      theEvent.returnValue = false;
      if(theEvent.preventDefault) theEvent.preventDefault();
    }
}

function validateQuestion5(evt,input){
  if(input.id == "slcQ5_1"){
      if($('#slcQ5_1').val() != ""){
      document.getElementById("slcQ5_2").required=false;
      document.getElementById("slcQ5_1").required=true;
      }
  }
  else{
      if($('#slcQ5_2').val()!= ""){
      document.getElementById("slcQ5_1").required=false;
      document.getElementById("slcQ5_2").required=true;
      }
  }
};
function validateQuestion16(evt,input){
    console.log(input.value);
    if(input.id == "slc2Q16_1_1"){
        if(input.value!=""){
            if(input.value<70){
                document.getElementById("slc2Q16_2").required=true;
                document.getElementById("slc2Q16_2_wrapper").style.display = "block";
            }else{
                document.getElementById("slc2Q16_2").required=false;
                document.getElementById("slc2Q16_2_wrapper").style.display = "none";
            }
        }else{
            document.getElementById("slc2Q16_2").required=false;
            document.getElementById("slc2Q16_2_wrapper").style.display = "none";
        }
    }
}
function validateQuestion17(evt,input){
    if(input.id == "slc2Q17_1_1"){
      if(input.value>8){
        document.getElementById("slc2Q17_2").required=true;
        document.getElementById("slc2Q17_2_wrapper").style.display = "block";
      }else{
        document.getElementById("slc2Q17_2").required=false;
        document.getElementById("slc2Q17_2_wrapper").style.display = "none";
      }
    }
}
function validateQuestion6(evt,input){
    if(input.id == "slcQ6_1"){
      if($('#slcQ6_1').val() != ""){
      document.getElementById("slcQ6_2").required=false;
      document.getElementById("slcQ6_1").required=true;
      }
  }
  else{
      if($('#slcQ6_2').val()!= ""){
      document.getElementById("slcQ6_1").required=false;
      document.getElementById("slcQ6_2").required=true;
      }
  }
};

function validateQuestion9(evt,input){
    if(input.id == "slcQ9_1"){
        if($('#slcQ9_1').val() != ""){
        document.getElementById("slcQ9_2").required=false;
        document.getElementById("slcQ9_1").required=true;
        }
    }
    else{
        if($('#slcQ9_2').val()!= ""){
        document.getElementById("slcQ9_1").required=false;
        document.getElementById("slcQ9_2").required=true;
        }
    }
};

function validateQuestion10(evt,input){
    if(input.id == "slcQ10_1"){
        if($('#slcQ10_1').val() != ""){
        document.getElementById("slcQ10_2").required=false;
        document.getElementById("slcQ10_1").required=true;
        }
    }
    else{
        if($('#slcQ10_2').val()!= ""){
        document.getElementById("slcQ10_1").required=false;
        document.getElementById("slcQ10_2").required=true;
        }
    }
};



{{--  Evento Select Pregunta11  --}}
    $("#slcQ13_1").on('click', function() {
     var slcQ13_2 = "input:checkbox[name='slcQ13_2']";
     var slcQ13_3 = "input:checkbox[name='slcQ13_3']";
     var slcQ13_0 = "input:checkbox[name='slcQ13_0']";
     var $box = $(this);
        if ($box.is(":checked")) {
            $(slcQ13_2).prop("checked", false);
            $(slcQ13_3).prop("checked", false);
            $(slcQ13_0).prop("checked", false);

            $box.prop("checked", true);
            $box.prop('required', false);

            $(slcQ13_2).prop("required", false);
            $(slcQ13_3).prop("required", false);
            $(slcQ13_0).prop("required", false);
        } else {
                 $box.prop("checked", false);
                 $box.prop('required', true);

                 $(slcQ13_2).prop("checked", false);
                 $(slcQ13_0).prop("checked", false);
                 $(slcQ13_3).prop("checked", false);

                $(slcQ13_2).prop("required", false);
                $(slcQ13_3).prop("required", false);
                $(slcQ13_0).prop("required", false);
        }
    });

     {{--  Evento Select Pregunta11  --}}
    $("#slcQ13_2").on('click', function() {
     var slcQ13_1 = "input:checkbox[name='slcQ13_1']";
     var slcQ13_3 = "input:checkbox[name='slcQ13_3']";
     var slcQ13_0 = "input:checkbox[name='slcQ13_0']";
     var $box = $(this);
        if ($box.is(":checked")) {
            $(slcQ13_1).prop("checked", false);
            $(slcQ13_3).prop("checked", false);
            $(slcQ13_0).prop("checked", false);

            $box.prop("checked", true);
            $box.prop('required', false);

            $(slcQ13_1).prop("required", false);
            $(slcQ13_3).prop("required", false);
            $(slcQ13_0).prop("required", false);
        } else {
                 $box.prop("checked", false);
                 $box.prop('required', true);

                 $(slcQ13_1).prop("checked", false);
                 $(slcQ13_3).prop("checked", false);
                 $(slcQ13_0).prop("checked", false);

                $(slcQ13_1).prop("required", false);
                $(slcQ13_3).prop("required", false);
                $(slcQ13_0).prop("required", false);
        }
    });

    {{--  Evento Select Pregunta11  --}}
    $("#slcQ13_3").on('click', function() {
    
     var slcQ13_1 = "input:checkbox[name='slcQ13_1']";
     var slcQ13_2 = "input:checkbox[name='slcQ13_2']";
     var slcQ13_0 = "input:checkbox[name='slcQ13_0']";
     var $box = $(this);
        if ($box.is(":checked")) {
            $(slcQ13_1).prop("checked", false);
            $(slcQ13_2).prop("checked", false);
            $(slcQ13_0).prop("checked", false);

            $box.prop("checked", true);
            $box.prop('required', false);

            $(slcQ13_1).prop("required", false);
            $(slcQ13_2).prop("required", false);
            $(slcQ13_0).prop("required", false);
        } else {
                 $box.prop("checked", false);
                 $box.prop('required', true);

                 $(slcQ13_1).prop("checked", false);
                 $(slcQ13_2).prop("checked", false);
                 $(slcQ13_0).prop("checked", false);

                $(slcQ13_1).prop("required", false);
                $(slcQ13_2).prop("required", false);
                $(slcQ13_0).prop("required", false);
        }
    });

    {{--  Evento Select Pregunta11  --}}
    $("#slcQ13_0").on('click', function() {
    
     var slcQ13_1 = "input:checkbox[name='slcQ13_1']";
     var slcQ13_2 = "input:checkbox[name='slcQ13_2']";
     var slcQ13_3 = "input:checkbox[name='slcQ13_3']";
     var $box = $(this);
        if ($box.is(":checked")) {
            $(slcQ13_1).prop("checked", false);
            $(slcQ13_2).prop("checked", false);
            $(slcQ13_3).prop("checked", false);

            $box.prop("checked", true);
            $box.prop('required', false);

            $(slcQ13_1).prop("required", false);
            $(slcQ13_2).prop("required", false);
            $(slcQ13_3).prop("required", false);
        } else {
                 $box.prop("checked", false);
                 $box.prop('required', true);

                 $(slcQ13_1).prop("checked", false);
                 $(slcQ13_2).prop("checked", false);
                 $(slcQ13_3).prop("checked", false);

                $(slcQ13_1).prop("required", false);
                $(slcQ13_2).prop("required", false);
                $(slcQ13_3).prop("required", false);
        }
    });


// $(".Prokey").on("keydown", function() {
//     console.log($(this).val());
// });

// $(".Clikey").on("keydown", function() {
//     console.log("2");
// });
</script>
@stop