@extends('Layouts.layout')

@section('js-libs')
<link href="{{ URL::asset('css/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ URL::asset('css/datatables.min.css') }}" rel="stylesheet" type="text/css">

<script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.es.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/chart.bundle.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/utils.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/numeral.min.js') }}"></script>

<!--FORM VALIDATION-->
<link href="{{ URL::asset('css/formValidation.min.css') }}" rel="stylesheet" type="text/css" > 

<script type="text/javascript" src="{{ URL::asset('js/formvalidation/formValidation.popular.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/formvalidation/language/es_CL.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/formvalidation/framework/bootstrap.min.js') }}"></script>

@stop


@section('pageTitle')
{{$cliente->NOMBRE}} <i class="fa fa-circle fa-sm" style ="color: {{\App\Entity\Infinity\Semaforo::getColor($cliente->NIVEL_ALERTA)}}"></i>
@stop

@section('content')

<style>
    .celda-centrada{
        vertical-align: middle; 
        text-align: center;
    }

    .tr-empty{
        background-color: #f9f9f9;
    }

</style>

<div class="row">
    <div class="col-sm-6 col-xs-12">
        <div class="x_panel" style="min-height: 280px">
            <div class="x_title">
                <h2>Cliente</h2>
                <ul class="nav navbar-right panel_toolbox">
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form class="form-horizontal form-label-left input_mask">
                    <div class="form-group">
                        <label class="control-label col-sm-2 col-xs-12">CU</label>
                        <div class="col-sm-4 col-xs-12">
                            <input class="form-control" type="text" value="{{$cliente->COD_UNICO}}" readonly="readonly" id="codUnico">
                        </div>
                        <a target="_blank" href="{{route('infinity.me.cliente.conoceme')}}?cu={{$cliente->COD_UNICO}}" style="text-decoration: underline;font-weight: bold">Conóceme</a>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2 col-xs-12">Ejecutivo</label>
                        <div class="col-sm-4 col-xs-12">
                            <input class="form-control" type="text" value="{{$cliente->EJECUTIVO}}" readonly="readonly">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2 col-xs-12">Deuda RCC</label>
                        <div class="col-sm-4 col-xs-12">
                            <input class="form-control" type="text" value="S/. {{number_format($cliente->SALDO_RCC,0,'.',',')}}" readonly="readonly">
                        </div>
                        <label class="control-label col-sm-2 col-xs-12">SOW</label>
                        <div class="col-sm-4 col-xs-12">
                            <input class="form-control" type="text" value="{{number_format($cliente->SOW,0,'.',',')}}%" readonly="readonly">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2 col-xs-12">Vencido</label>
                        <div class="col-sm-4 col-xs-12">
                            <input class="form-control" type="text" value="S/. {{number_format($cliente->SALDO_VENCIDO_RCC,0,'.',',')}}" readonly="readonly">
                        </div>
                        <label class="control-label col-sm-2 col-xs-12">Rating</label>
                        <div class="col-sm-4 col-xs-12">
                            <input class="form-control" type="text" value="{{$cliente->RATING}}" readonly="readonly">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2 col-xs-12">Clasificación</label>
                        <div class="col-sm-4 col-xs-12">
                            <input class="form-control" type="text" value="{{$cliente->CLASIFICACION}}" readonly="readonly">
                        </div>
                        <label class="control-label col-sm-2 col-xs-12">FEVE</label>
                        <div class="col-sm-4 col-xs-12">
                            <input class="form-control" type="text" value="{{$cliente->FEVE}}" readonly="readonly">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-xs-12">
        <div class="x_panel" style="min-height: 280px">
            <div class="x_title">
                <h2>Status</h2>
                <ul class="nav navbar-right panel_toolbox">
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <form class="form-horizontal form-label-left input_mask">

                    <div class="form-group" style="padding-bottom: 15px">
                        <!-- EEFF1 -->
                        <label class="control-label col-sm-2 col-xs-12">DDJJ</label>
                        <div class="col-sm-3 col-xs-12">
                            <input class="form-control" type="text" value="{{$cliente->FECHA_ULTIMO_EEFF_1}}" readonly="readonly"> 
                        </div>
                        <div class="col-sm-1 col-xs-12">
                            <?php
                            echo \App\Entity\Infinity\AlertaDocumentacion::getIcono(\App\Entity\Infinity\AlertaDocumentacion::ID_DDJJ, $cliente->FECHA_ULTIMO_EEFF_1,$cliente->FLG_INFINITY,$cliente->FECHA_INGRESO_INFINITY);
                            ?>                                        
                        </div>
                        <!-- EEFF2 -->
                        <label class="control-label col-sm-2 col-xs-12">EEFF</label>
                        <div class="col-sm-3 col-xs-12"><input hidden="hidden" value="{{$cliente->FECHA_ULTIMO_EEFF_2}}">
                            <?php if (date('m', strtotime($cliente->FECHA_ULTIMO_EEFF_2))=='06'){ ?>
                                <input class="form-control" type="text" value="{{$cliente->FECHA_ULTIMO_EEFF_2}}" readonly="readonly">
                            <?php }else{ ?>
                                <input class="form-control" type="text" value="" readonly="readonly">
                            <?php } ?>
                        </div>
                        <div class="col-sm-1 col-xs-12">
                            <?php
                                echo \App\Entity\Infinity\AlertaDocumentacion::getIcono(\App\Entity\Infinity\AlertaDocumentacion::ID_EEFF, $cliente->FECHA_ULTIMO_EEFF_2,$cliente->FLG_INFINITY,$cliente->FECHA_INGRESO_INFINITY);
                            ?> 
                        </div>
                    </div>

                    <div class="form-group" style="padding-bottom: 15px">
                        <label class="control-label col-sm-2 col-xs-12">Última Visita</label>
                        <div class="col-sm-3 col-xs-12">
                            <input class="form-control" type="text" value="{{$cliente->FECHA_ULTIMA_VISITA}}" readonly="readonly"> 
                        </div>
                        <div class="col-sm-1 col-xs-12">
                            <?php
                            echo \App\Entity\Infinity\AlertaDocumentacion::getIcono(\App\Entity\Infinity\AlertaDocumentacion::ID_VISITA, $cliente->FECHA_ULTIMA_VISITA,$cliente->FLG_INFINITY,$cliente->FECHA_INGRESO_INFINITY);
                            ?> 
                        </div>
                        @if ($cliente->CANT_POLI_CUALI == null)
                            <a target="_blank" href="{{route('infinity.me.cliente.visita')}}?cu={{$cliente->COD_UNICO}}" style="text-decoration: underline;font-weight: bold">Agregar Visita</a>
                        @endif
                    </div>
                    <div class="form-group" style="padding-bottom: 15px">
                        <label class="control-label col-sm-2 col-xs-12">IBR</label>
                        <div class="col-sm-3 col-xs-12">
                            <input class="form-control" type="text" value="{{$cliente->FECHA_ULTIMO_IBR}}" readonly="readonly"> 
                        </div>
                        <div class="col-sm-1 col-xs-12">
                            <?php
                            echo \App\Entity\Infinity\AlertaDocumentacion::getIcono(\App\Entity\Infinity\AlertaDocumentacion::ID_IBR, $cliente->FECHA_ULTIMO_IBR,$cliente->FLG_INFINITY,$cliente->FECHA_INGRESO_INFINITY);
                            ?> 
                        </div>
                        <label class="control-label col-sm-2 col-xs-12">F02</label>
                        <div class="col-sm-3 col-xs-12">
                            <input class="form-control" type="text" value="{{$cliente->FECHA_ULTIMO_F02}}" readonly="readonly"> 
                        </div>
                        <div class="col-sm-1 col-xs-12">
                            <?php
                            echo \App\Entity\Infinity\AlertaDocumentacion::getIcono(\App\Entity\Infinity\AlertaDocumentacion::ID_F02, $cliente->FECHA_ULTIMO_F02,$cliente->FLG_INFINITY,$cliente->FECHA_INGRESO_INFINITY);
                            ?> 
                        </div>
                    </div>   
                </form>
                <center><button class="btn btn-primary" id="btnDocumentacion">Gestión de Documentación</button></center>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-sm-6 col-xs-12">
        <div class="x_panel" style="min-height: 380px">
            <div class="x_title">
                <h2>Evolución</h2>
                <ul class="nav navbar-right panel_toolbox">
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <center>
                    <div class="chart-container" style="width: 70%">
                        <canvas id="graficoEvolucion"></canvas>
                    </div>                                              
                </center>            
            </div>
        </div>
        <div class="x_panel" style="min-height: 230px">
            <div class="x_title">
                <h2>Recálculo</h2>
                <ul class="nav navbar-right panel_toolbox">
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <form id="frmRecalculo" class="form-horizontal form-label-left" enctype="multipart/form-data" action="{{route('infinity.me.detalle.guardar.recalculo')}}" method="POST">
                    <input name="codUnico" type="text" value="{{$cliente->COD_UNICO}}" hidden="">
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">MDL </label>
                            <div class="input-group col-md-9 col-sm-9 col-xs-12" >              
                                <input autocomplete="off" class="form-control"  type="text" id="mdlRecalculo" name="mdlRecalculo" placeholder="Monto (USD)" 
                                       value="{{$recalculo!=null?$recalculo->MDL:''}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Fecha </label>
                            <div class="input-group col-md-9 col-sm-9 col-xs-12" >               
                                <div class="input-group-addon styleAddOn"><i class="glyphicon glyphicon-calendar fa fa-calendar" for="fechaRecalculo"></i></div>
                                <input autocomplete="off" class="form-control dfecha"  type="text" id="fechaRecalculo" name="fechaRecalculo" placeholder="Seleccionar fecha"
                                       value="{{$recalculo!=null?$recalculo->FECHA_RECALCULO:''}}" 
                                       >
                            </div>
                        </div>          

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Adjunto </label>
                            <div class="input-group col-md-9 col-sm-9 col-xs-12">
                                <input type="file" name="adjuntoRecalculo" id ="adjuntoRecalculo" class="form-control image" style="border-style: none">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <center>
                            <?php
                            echo \App\Entity\Infinity\AlertaDocumentacion::getIcono(\App\Entity\Infinity\AlertaDocumentacion::ID_RECALCULO, $cliente->FECHA_ULTIMO_RECALCULO);
                            ?> 
                            <p style="font-size: 16px;">Recálculo</p>
                            @if($recalculo!=null)
                            <a href="{{route('download', ['file' => str_replace('/','|',\App\Entity\Infinity\ConocemeDocumentacion::RUTA_MDL.$recalculo->ADJUNTO)])}}" ><i class="fa fa-download fa-2x" ></i>
                            </a><br><br>
                            @endif
                            <button class="btn btn-success" type="submit">Recálculo</button>
                        </center>
                    </div>
                </form>         
            </div>
        </div>

        <div class="x_panel" style="min-height: 170px">
            <div class="x_title">
                <h2>Grupo Económico</h2>
                <ul class="nav navbar-right panel_toolbox">
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                    @if($grupoEconomico!=NULL)
                        @if($grupoEconomico['FLG_AUTONOMIA'])
                            <p><i class="fas fa-check-circle fa-2x" style="color:#26B99A"></i> Sí cumple la Política de Líneas Automáticas</p>
                        @else
                            <p><i class="fas fa-exclamation-circle fa-2x" style="color:#D9534F"></i> No cumple la Política de Líneas Automáticas</p> 
                        @endif
                      <center>
                          <button class="btn btn-success" type="button" id="btnGrupoEconomico">Ver Detalles</button>
                      </center>
                    @endif
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-xs-12">
        <div class="x_panel" style="min-height: 800px">
            <div class="x_title">
                <h2>Políticas y Variables</h2>
                <ul class="nav navbar-right panel_toolbox">
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row" style="padding-bottom: 30px">
                    <div class="col-md-12">
                        <table class="table table-striped jambo_table tableHistorico" id="tablePoliticas">
                            <thead>
                                <tr>
                                    <th>Periodo Política</th>
                                    <th>Tipo</th>
                                    <th>Política</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (count($politicas) > 0)
                                @foreach ($politicas as $politica)
                                <tr>
                                    <td>{{$politica->PERIODO}}</td>
                                    <td>{{$politica->TIPO}}</td>
                                    <td>{{$politica->NOMBRE}}</td>
                                    <td>{{$politica->ESTADO_FINAL_GESTION}}</td>
                                </tr>
                                @endforeach
                                @else
                                <tr class="tr-empty"><td colspan="4">El cliente no tiene políticas aplicadas</td><tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                
                <div class="row" style="padding-bottom: 30px">
                    <div class="col-md-12">
                        <table class="table table-striped jambo_table tableHistorico" id="tableVariables">
                            <thead>
                                <tr>
                                    <th>Grupo</th>
                                    <th>Variable</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (count($variables) > 0)
                                    @foreach ($variables as $i => $var)
                                        @foreach ($var as $j => $v)
                                            <tr class="tr-{{$j}} {{$j == config('app.periodoInfinity')? '':'hidden'}}">
                                                <td>{{$i}}</td>
                                                <td>{{implode(',',array_unique($v))}}</td>
                                            </tr>
                                        @endforeach
                                    @endforeach
                                @else
                                <tr class="tr-empty"><td colspan="2">El cliente no tiene variables a revisar</td><tr>
                                    @endif
                            </tbody>
                        </table>
                    </div>
                </div>                
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-7">
        <div class="x_panel" style="min-height: 500px">
            <div class="x_title">
                <h2>Gestiones y Observaciones</h2>
                <ul class="nav navbar-right panel_toolbox">
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" style="max-height: 380px; <?php echo (count($gestiones) > 0 ? 'overflow-y: scroll' : ''); ?>;">

                <table class="table table-striped table-bordered jambo_table">
                    <thead>
                        <tr>
                            <th>Usuario</th>
                            <th>Fecha</th>
                            <th>Comentario</th>
                            <th>Politica Asociada</th>
                            <th>Tipo de Gestión</th>
                            <th>Adjunto</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($gestiones)>0)
                        @foreach($gestiones as $gestion)
                        <tr style="">
                            <td style="width: 15%;padding-bottom: 30px">{{$gestion->NOMBRE}}<br>{{$gestion->CARGO}}</td>
                            <td style="width: 10%;padding-bottom: 30px">{{$gestion->FECHA_GESTION}}</td> 
                            <td style="width: 20%;padding-bottom: 30px;text-align: justify;">{{$gestion->COMENTARIO}}</td>
                            <td style="width: 15%;padding-bottom: 30px;text-align: justify;">{{$gestion->NOMBRE_POLITICA}}</td>
                            <td style="width: 20%;padding-bottom: 30px;text-align: justify;">{{$gestion->NOMBRE_ESTADO}}</td>
                            <td style="width: 4%;padding-bottom: 30px;padding-left: 30px;text-align: center;">
                                @if ($gestion->ADJUNTO)
                                <a href="{{route('download', ['file' => str_replace('/','|',\App\Entity\Infinity\Gestion::RUTA.$gestion->ADJUNTO)])}}"
                                   ><i class="fa fa-download fa-2x" ></i>
                                </a>
                                @endif
                            </td>
                        </tr>   
                        @endforeach   
                        @else
                        <tr style=""><td colspan="6">No se encontraton gestiones para el cliente</td></tr>
                        @endif
                    </tbody>
                </table>
            </div>

        </div>
    </div>

    <div class="col-sm-5">
        <div class="x_panel" style="min-height: 500px">
            <div class="x_title">
                <h2>Nueva Gestión</h2>
                <ul class="nav navbar-right panel_toolbox">                
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form id="frmGestion" class="form-horizontal" enctype="multipart/form-data" method="POST" action="{{route('infinity.me.detalle.guardar.gestion')}}">

                    <input name="codUnico" type="text" value="{{$cliente->COD_UNICO}}" hidden="">

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Estado:</label>
                        <div class="input-group col-md-9 col-sm-9 col-xs-12">
                            <select class="form-control" name="estadoGestion" id="estadoGestion">
                                <option value="">Seleccionar Estado</option>
                                @foreach($estados as $estado)
                                <option value="{{$estado->ID_ESTADO_GESTION}}">{{$estado->NOMBRE_ESTADO}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Fecha Gestión:</label>
                        <div class="input-group col-md-9 col-sm-9 col-xs-12" >               
                            <div class="input-group-addon styleAddOn"><i class="glyphicon glyphicon-calendar fa fa-calendar" for="fechaGestion"></i></div>
                            <input autocomplete="off" class="form-control dfecha"  type="text" id="fechaGestion" name="fechaGestion" placeholder="Seleccionar fecha" value="">
                        </div>
                    </div>
                    <div class="form-group selec_politicas">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Politica:</label>
                        <div class="input-group col-md-9 col-sm-9 col-xs-12" >
                            <div class="input-group-addon styleAddOn"><i class="glyphicon glyphicon-calendar fa fa-calendar" for="PoliticaAsociada"></i></div>
                            <select class="form-control" name="PoliticaAsociada">
                                <option value="">Seleccionar Política</option>
                                @foreach($combopoliticas as $combopoliticas)
                                <option value="{{$combopoliticas->ID_POLITICA_CLIENTE}}">{{$combopoliticas->NOMBRE}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Comentario:</label>
                        <div class="input-group col-md-9 col-sm-9 col-xs-12">
                            <textarea class="form-control" rows="10" style="resize: none" placeholder="Ingresar Comentario..." name="comentarioGestion" ></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Adjunto:</label>
                        <div class="input-group col-md-9 col-sm-9 col-xs-12">
                            <input type="file" name="adjuntoGestion" id = "adjuntoGestion" class="form-control image" style="border-style: none">
                        </div>
                    </div>
                    <center><button class="btn btn-success" type="submit">Guardar</button></center>
                </form>
            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-sm-12">
        <div class="x_panel" style="min-height: 500px">
            <div class="x_title">
                <h2>Historia del Cliente</h2>
                <ul class="nav navbar-right panel_toolbox">
                </ul>
                <div class="clearfix"></div>
            </div>
            <table class="table table-striped table-bordered jambo_table" id="tblHistoricoCliente">
                <thead>
                    <tr>
                        <th>Tipo</th>
                        <th>Usuario</th>
                        <th>Fecha</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- /.Modal Documentación-->
<div class="modal fade" tabindex="-1" role="dialog" id="modalDocumentacion">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Gestión de Documentación</h4>
            </div>
            <div class="modal-body">
                <form id="frmDocumentacion" class="form-horizontal form-label-left" enctype="multipart/form-data" action="{{route('infinity.me.detalle.guardar.documentacion')}}" method="POST">

                    <input name="codUnico" type="text" value="{{$cliente->COD_UNICO}}" hidden="">

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Tipo Documento:</label>
                        <div class="input-group col-md-9 col-sm-9 col-xs-12">
                            <select class="form-control" name="tipoDocumento" id="cboTipoDocumento">
                                <option value="">Seleccionar Tipo</option>
                                <!--DEFINIR-->
                                <option value="1">DDJJ</option>
                                <option value="2">EEFF</option>
                                <option value="3">IBR</option>
                                <option value="4">F02</option>                            
                            </select>
                        </div>
                    </div>

                    <div class="form-group" id="fechaFirmaOcultar" hidden>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Fecha Firma Documento:</label>
                        <div class="input-group col-md-9 col-sm-9 col-xs-12">
                            <div class="input-group-addon styleAddOn"><i class="glyphicon glyphicon-calendar fa fa-calendar" for="fechaFirma"></i></div>
                            <input autocomplete="off" class="form-control dfecha"  type="text" id="fechaFirma" name="fechaFirma" placeholder="Seleccionar fecha" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Adjunto:</label>
                        <div class="input-group col-md-9 col-sm-9 col-xs-12">
                            <input type="file" name="adjuntoDocumento" id ="adjuntoDocumento" class="form-control image" style="border-style: none">
                        </div>
                    </div>

                    <center>
                        <button class="btn btn-success" type="submit">Agregar</button>
                    </center>
                </form>

                <div id="tablaDocumentacion" style="overflow-y: scroll;max-height: 500px" >
                    <table class="table table-striped jambo_table">
                        <thead>                                    
                            <tr class="headings">
                                <th>Documento</th>
                                <th>Fecha Firma</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>

                            @if(count($documentos)>0)
                            @foreach($documentos as $documento)
                            <tr>
                                <td>{{$documento->NOMBRE_DOCUMENTO}}</td>
                                <td>{{$documento->FECHA_FIRMA}}</td>                       
                                <td><a href="{{route('download', ['file' => str_replace('/','|',\App\Entity\Infinity\ConocemeDocumentacion::RUTA_DOCS.$documento->ADJUNTO)])}}">
                                        <i class="fa fa-download"></i></a></td>
                            </tr>    
                            @endforeach                               
                            @else
                            <tr><td colspan="3">No se encontraron resultados</td></tr>
                            @endif


                        </tbody>
                    </table>
                </div>
            </div>
            <!--<div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>-->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<!-- /.Modal Grupo Económico-->
<div class="modal fade" tabindex="-1" role="dialog" id="modalGrupoEconomico">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Grupo Económico</h4>
            </div>
            <div class="modal-body" style="min-height: 420px;max-height: 420px">
                @if($grupoEconomico!=NULL)
                
                <div class="col-sm-12" id="infoResumen" style="padding-bottom:20px">
                    <label style="text-decoration: underline;">Resumen</label><br>
                    <div class="col-sm-4 col-xs-12">
                        <?php
                            echo \App\Entity\Flag::getIconoFlag($grupoEconomico['FLG_CLASIFICACION']);
                        ?> Clasificación
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <?php
                            echo \App\Entity\Flag::getIconoFlag($grupoEconomico['FLG_VENCIDO']);
                        ?> Vencido
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <?php
                            echo \App\Entity\Flag::getIconoFlag($grupoEconomico['FLG_FEVE']);
                        ?> FEVE DURO
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <?php
                            echo \App\Entity\Flag::getIconoFlag($grupoEconomico['FLG_REFINANCIADO']);
                        ?> Refinanciado
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <?php
                            echo \App\Entity\Flag::getIconoFlag($grupoEconomico['FLG_REESTRUCTURADO']);
                        ?> Reestructurado
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <?php
                            echo \App\Entity\Flag::getIconoFlag($grupoEconomico['FLG_JUDICIAL']);
                        ?> Judicial
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <?php
                            echo \App\Entity\Flag::getIconoFlag($grupoEconomico['FLG_COBRANZA']);
                        ?> Cobranza
                    </div>                    
                </div>

                <div class="col-sm-12">
                        @if(count($grupoEconomico['RELACIONADOS'])>0)
                            <label style="text-decoration: underline;">Relacionados</label><br>
                            <div id="infoRelacionados" style="overflow-y: scroll;max-height: 250px" >
                            @foreach($grupoEconomico['RELACIONADOS'] as $relacionado)
                            <div class="col-sm-12" style="padding-bottom:20px">
                                <label style="text-decoration: underline;">{{$relacionado->NOMBRE_REL}} ({{$relacionado->COD_UNICO_REL}})</label><br>
                                <div class="col-sm-4 col-xs-12">
                                    <?php
                                        echo \App\Entity\Flag::getIconoFlag($relacionado->FLG_CLASIFICACION);
                                    ?> Clasificación
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    Vencido: S/. {{number_format($relacionado->MONTO_VENCIDO,0,'.',',')}}
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <?php
                                        echo \App\Entity\Flag::getIconoFlag($relacionado->FLG_FEVE);
                                    ?> FEVE DURO
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    Refinanciado: S/. {{number_format($relacionado->MONTO_REFINANCIADO,0,'.',',')}}
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    Reestructurado: S/. {{number_format($relacionado->MONTO_REESTRUCTURADO,0,'.',',')}}
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    Judicial: S/. {{number_format($relacionado->MONTO_JUDICIAL,0,'.',',')}}
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    Coactiva: S/. {{number_format($relacionado->MONTO_COACTIVA,0,'.',',')}}
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    Laboral: S/. {{number_format($relacionado->MONTO_LABORAL,0,'.',',')}}
                                </div>    
                            </div> 
                            @endforeach
                        </div>
                        @endif
                </div>
                @endif
            </div>            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


@stop

@section('js-scripts')
<script>

$(document).ready(function () {
    generarGrafico($('#codUnico').val());

    /*Combobox Mes de explicación, cambia las filas a mostrar*/
    $('#cboMesExplicacion').change(function () {

        $('.tableHistorico tbody >tr')
                .find('.tr-empty').remove().end()
                .addClass('hidden').end()
                .find('.tr-' + $(this).val()).removeClass('hidden');

        if ($('#tableSalidas tbody>tr').not('.hidden').length == 0) {
            $('#tableSalidas tbody').append('<tr class="tr-empty"><td colspan="2">El cliente se mantuvo en Líneas Automáticas</td><tr>')
        }

        if ($('#tablePoliticas tbody>tr').not('.hidden').length == 0) {
            $('#tablePoliticas tbody').append('<tr class="tr-empty"><td colspan="2">El cliente no tiene políticas aplicadas</td><tr>')
        }
        
        if ($('#tableVariables tbody>tr').not('.hidden').length == 0) {
            $('#tableVariables tbody').append('<tr class="tr-empty"><td colspan="2">No existen variables para mostrar</td><tr>')
        }
    })

    //La fecha no se registra para documentos DDJJ y EEFF
    $('#cboTipoDocumento').change(function () {
        console.log($(this).val());
        if ($(this).val() <= 2)
            $('#fechaFirmaOcultar').attr('hidden', 'true');
        else
            $('#fechaFirmaOcultar').removeAttr('hidden');
    })

    $('#btnDocumentacion').on("click", function () {
        $('#frmDocumentacion').trigger("reset");
        $('#frmDocumentacion').formValidation('destroy', true);
        initializeFormDocumentacion();
        $('#modalDocumentacion').modal();
    })

    $('#btnGrupoEconomico').on("click", function () {
        $('#modalGrupoEconomico').modal();
    })

    $("[data-toggle=popover]").each(function (i, obj) {
        $(this).popover({
            html: true,
            content: function () {
                html = '<b>Rating:</b> ' + $(this).attr('data-rating');
                html += '<br><b>Mora:</b> ' + $(this).attr('data-mora');
                return html;
            }
        });
    });

    $('.dfecha').each(function () {
        $(this).datepicker({
            maxViewMode: 1,
            //daysOfWeekDisabled: "0,6",
            language: "es",
            autoclose: true,
            startDate: "-365d",
            endDate: "0d",
            format: "yyyy-mm-dd",
        })
                .on('changeDate', function (e) {
                    // Revalidate the date field
                    $('#frmGestion').formValidation('revalidateField', 'fechaGestion');
                    $('#frmDocumentacion').formValidation('revalidateField', 'fechaFirma');
                    $('#frmRecalculo').formValidation('revalidateField', 'fechaRecalculo');
                });
    });

    $('#frmGestion').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            'estadoGestion': {
                validators: {
                    notEmpty: {
                        message: 'Debes seleccionar un estado'
                    },
                }
            },
            'fechaGestion': {
                validators: {
                    notEmpty: {
                        message: 'Debes seleccionar una fecha'
                    },
                    date: {
                        format: 'YYYY-MM-DD',
                        message: 'Ingrese una fecha válida',
                    }
                }
            },
            'comentarioGestion': {
                validators: {
                    notEmpty: {
                        message: 'Debes seleccionar un estado'
                    },
                    stringLength: {
                        max: 500,
                        min: 30,
                        message: 'El comentario debe tener entre 30 y 500 caracteres'
                    }
                }
            },
        },
    })
            .off('success.form.fv');

    $('#frmRecalculo').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            'mdlRecalculo': {
                validators: {
                    notEmpty: {
                        message: 'Debes llenar el monto del MDL'
                    },
                    numeric: {
                        message: 'El MDL debe ser un monto numérico',
                        // The default separators
                        thousandsSeparator: '',
                        decimalSeparator: '.'
                    }
                }
            },
            'fechaRecalculo': {
                validators: {
                    notEmpty: {
                        message: 'Debes seleccionar una fecha'
                    },
                    date: {
                        format: 'YYYY-MM-DD',
                        message: 'Ingrese una fecha válida',
                    }
                }
            },
            'adjuntoRecalculo': {
                validators: {
                    notEmpty: {
                        message: 'Debes seleccionar un archivo'
                    },
                }
            },
        },
    })
            .off('success.form.fv');

});

function initializeFormDocumentacion() {

    $('#frmDocumentacion').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            'tipoDocumento': {
                validators: {
                    notEmpty: {
                        message: 'Debes seleccionar un tipo'
                    },
                }
            },
            'fechaFirma': {
                validators: {
                    notEmpty: {
                        message: 'Debes seleccionar una fecha'
                    },
                    date: {
                        format: 'YYYY-MM-DD',
                        message: 'Ingrese una fecha válida',
                    }
                }
            },
            'adjuntoDocumento': {
                validators: {
                    notEmpty: {
                        message: 'Debes seleccionar un archivo'
                    },
                }
            },
        },
    })
            .off('success.form.fv');
}

function generarGrafico(cu) {

    $.ajax({
        type: "GET",
        data: {
            cu: cu,
        },
        async: false,
        url: APP_URL + 'infinity/me/cliente/detalle/grafico',
        success: function (result) {
            //console.log(result);
            var periodos = [];
            var nivelInverso = [];
            var nivel = [];
            var colores = [];

            for (var i = 0; i < result.length; i++) {
                periodos.push(result[i]['PERIODO']);


                switch (result[i]['NIVEL_ALERTA']) {
                    case '1':
                    case '11':
                        nivelInverso.push(4);
                        break;
                    case '2':
                    case '22':
                        nivelInverso.push(3);
                        break;
                    case '3':
                    case '33':
                        nivelInverso.push(2);
                        break;                    
                    case '99':
                    case '999':
                        nivelInverso.push(1);
                        break;
                    default:
                        nivelInverso.push(0);
                        break;
                }

                switch (result[i]['NIVEL_ALERTA']) {
                    case '1'://VERDE
                    case '11':
                        colores.push('green');
                        break;
                    case '2'://AMBAR
                    case '22':
                        colores.push('#fc3');
                        break;
                    case '3'://ROJO
                    case '33':
                        colores.push('red');
                        break;                    
                    case '99'://NEGRO
                    case '999':
                        colores.push('#000000');
                        break;
                    default:
                        colores.push('gray');
                        break;
                }

            }
            //console.log(nivel);
            //console.log(nivelInverso);
            var config = {
                type: 'line',
                data: {
                    labels: periodos,
                    datasets: [{
                            label: 'Evolución Infinity',
                            //backgroundColor: window.chartColors.blue,
                            borderColor: 'gray',
                            data: nivelInverso,
                            fill: false,
                            borderDash: [5],
                            pointRadius: 15,
                            pointHoverRadius: 10,
                            pointBackgroundColor: colores
                        }, ]
                },
                options: {
                    responsive: true,
                    hover: {
                        mode: 'nearest',
                        intersect: true
                    },
                    legend: {
                        display: false,
                    },
                    scales: {
                        xAxes: [{
                                display: true,
                                scaleLabel: {
                                    display: false,
                                    labelString: 'Month'
                                }
                            }],
                        yAxes: [{
                                display: false,
                                ticks: {
                                    //beginAtZero:true,
                                    suggestedMax: 4.5,
                                    suggestedMin: 0.5
                                }
                            }]
                    },
                    tooltips: {
                        callbacks: {
                            title: function (tooltipItem, data) {
                                console.log(result[tooltipItem[0]['index']]);
                                return 'LA: ' + ((result[tooltipItem[0]['index']]['FLG_INFINITY'] == 1) ? 'Sí' : 'No');
                            },
                            label: function (tooltipItem, data) {
                                i = 0;

                                nivelInverso = [];
                                while (periodos[i] != tooltipItem['xLabel'])
                                    i++;

                                nivelInverso.push('Rating: ' + result[i]['RATING']);
                                nivelInverso.push('Deuda IBK: S/.' + numeral(result[i]['SALDO_INTERBANK']).format('0,0'));
                                nivelInverso.push('Deuda RCC: S/.' + numeral(result[i]['SALDO_RCC']).format('0,0'));
                                nivelInverso.push('Días Atraso: ' + result[i]['MAX_DIAS_ATRASO']);
                                return nivelInverso;
                            },

                        },
                        backgroundColor: '#FFF',
                        titleFontSize: 16,
                        titleFontColor: '#000',
                        bodyFontColor: '#000',
                        bodyFontSize: 14,
                        displayColors: false
                    }
                }
            };

            window.onload = function () {
                var ctx = document.getElementById('graficoEvolucion').getContext('2d');
                window.myLine = new Chart(ctx, config);

            };


        }
    });
}

$('#tblHistoricoCliente').DataTable({
    processing: true,
    serverSide: true,
    language: {"url": "{{ URL::asset('js/Json/Spanish.json') }}"},
    ajax: {
        url: '{{ route('infinity.me.cliente.historia') }}',
        data: {
            "cliente": $('#codUnico').val()
        }
    },
    order: [[2, "desc"]],
        
    columnDefs: [
        {
            targets: 3,
            data: null,
            render: function (data, type, row) {
                var url = '';
                if(row.TIPO === 'VISITA'){
                    url = "{{ route('infinity.me.visita.historia') }}" + "?id=" + row.ID;
                }else{
                    url = "{{ route('infinity.me.conoceme.historia') }}" + "?id=" + row.ID;
                }
                return '<a target="_blank" href="'+ url +'">Ver</a>';
            }
        },
    ],
    columns: [
        {data: 'TIPO', name: 'WICON.TIPO'},
        {data: 'USUARIO', name: 'WU.NOMBRE'},
        {data: 'FECHA_ACTUALIZACION', name: 'WICON.FECHA_ACTUALIZACION', searchable: false},
        {data: 'TIPO', name: 'WICON.TIPO', searchable: false},
    ]
});

$('#estadoGestion').change(function() {
    if ($(this).val()==3) {
        $(".selec_politicas").attr('hidden','hidden');
    }else{
        $(".selec_politicas").removeAttr('hidden');
    }
});

</script>
@stop