@extends('Layouts.layout')
@section('js-libs')

<link href="{{ URL::asset('css/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ URL::asset('css/datatables.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ URL::asset('css/formValidation.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ URL::asset('css/multiselect.min.css') }}" rel="stylesheet" type="text/css">

<script type="text/javascript" src="{{ URL::asset('js/jszip.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/pdfmake.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/vfs_fonts.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/buttons.flash.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/buttons.print.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/buttons.html5.min.js') }}"></script>

<script type="text/javascript" src="{{ URL::asset('js/numeral.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/chart.bundle.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/utils.js') }}"></script>

<script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.es.min.js') }}"></script>

<script type="text/javascript" src="{{ URL::asset('js/formvalidation/formValidation.popular.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/formvalidation/language/es_CL.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/formvalidation/framework/bootstrap.min.js') }}"></script>

<script type="text/javascript" src="{{ URL::asset('js/multiselect.min.js') }}"></script>
<!--  ESTILOS DE ESTA VISTA -->
<link href="{{ URL::asset('css/alertascartera/alertas_main.css') }}" rel="stylesheet" type="text/css"></link>
@stop
@section('content')
    @yield('vista_detalle')
@stop
@section('js-scripts')
<script>
    function addCompromiso(event) {
        $('#modalCumplimiento').modal();
    }

    function showOption(value) {
        $('#optionHolder option[value="' + value + '"]').appendTo('#status');
    }

    function hideOption(value) {
        $('select option[value="' + value + '"]').appendTo('#optionHolder');
    }

    function gestionCumplimiento(idCumplimiento) {
        const name = $('input[value="' + idCumplimiento + '"]').attr('name');
        const index = name.replace('ID_CUMPL', '');
        const cu = $('#codUnico').val();
        const fecha = $('#FECHA_COMPROMISO' + index).html();
        const status = $('#STATUS_COMPROMISO' + index).html();
        $.ajax({
            type: 'POST',
            data: {
                'ID': idCumplimiento,
                'cu': cu
            },
            async: true,
            url: "{{route('infinity.alertascartera.validadorEliminar')}}",
            success: function(result) {
                if (result == '1') {
                    showOption('ELIMINADO');
                    $('#fechaCompromisoGestion').attr('disabled',false);
                    $('#fechaCompromisoGestion').css('display','block');
                } else {
                    hideOption('ELIMINADO');
                    $('#fechaCompromisoGestion').attr('disabled',true);
                    $('#fechaCompromisoGestion').css('display','none');
                }
                $('#status').val(status.trim());
                $('#fechaCompromisoGestion').val(fecha.trim());
                $('#IDCUMPL').val(idCumplimiento);
                $('#modalGestionar').modal();
            }
        });
    }

    function gestionCumplimientoPOST() {
        var today = new Date();
        const status = $('#status').val();
        const fechaCompromiso = $('#fechaCompromisoGestion').val();
        const cu = $('#codUnico').val();
        const idcovid = $('#IDCOVID').val();
        const idCumplimiento = $('#IDCUMPL').val();
        const name = $('input[value="' + idCumplimiento + '"]').attr('name');
        const index = name.replace('ID_CUMPL', '');
        $('#btnAct').attr('disabled', true);
        $.ajax({
            type: 'POST',
            data: {
                'ID': idCumplimiento,
                'ID_COVID': idcovid,
                'cu': cu,
                'FECHA_COMPROMISO': fechaCompromiso,
                'STATUS_COMPROMISO': status,
            },
            async: true,
            url: "{{route('infinity.alertascartera.guardarCumplimiento')}}",
            success: function(result) {
                var res = JSON.parse(result);
                const response = res['result'];
                if (response) {
                    if (status == 'ELIMINADO') {
                        del(index);
                    } else {
                        $('#FECHA_COMPROMISO' + index).html(fechaCompromiso);
                        const date = Date.parse(fechaCompromiso);
                        var timeinmilisec = date - today;
                        var days = (Math.ceil(timeinmilisec / (1000 * 60 * 60 * 24)));
                        var color = '';
                        if (days > 30) {
                            color = '#36D152';
                        } else if (days < 30 && days > 0) {
                            color = '#FACA26';
                        } else {
                            color = '#F92222';
                        }
                        var mensaje = '';
                        if (days > 0) {
                            if (days == 1) {
                                mensaje = 'Mañana vence el cumplimiento';
                            } else {
                                mensaje = 'Quedan ' + days + ' días  vence el cumplimiento';
                            }
                        } else {
                            if (days == -1) {
                                mensaje = 'Ayer venció el cumplimiento';
                            } else if (days == 0) {
                                mensaje = 'Hoy día vence el cumplimiento';
                            } else {
                                mensaje = 'Han pasado ' + days + ' días vence el cumplimiento';
                            }
                        }
                        $('#RECT' + index).attr('fill', color);
                        $('#semaforoInfo' + index).attr('data-original-title', mensaje);
                        $('#STATUS_COMPROMISO' + index).html(status);
                    }
                    getCambios();
                    $('#btnAct').attr('disabled', false);
                }
                $('#modalGestionar').hide();
                $('#modalGestionar').modal('toggle');
                $('#messageModal').html(res['message']);
                $('#modalGuardar').modal();
            }
        });
        return false;
    }

    function addCumplimientoPOST() {
        var today = new Date();
        const cu = $('#codUnico').val();
        const idcovid = $('#IDCOVID').val();
        var values = {};
        values['ID'] = idcovid;
        values['cu'] = cu;
        values['cumplimientosCounter'] = $('#cumplimientosCounter').val();
        $("#nuevoCompromiso input, #nuevoCompromiso select").each(function() {
            var isDisabled = $(this).prop('disabled');
            values[$(this).attr("name")] = !isDisabled ? $(this).val() : null;
        });
        var stringData = JSON.stringify(values);
        $('#btnAdd').attr('disabled', true);
        $.ajax({
            type: 'POST',
            data: {
                data: stringData
            },
            async: true,
            url: "{{route('infinity.alertascartera.addCumplimiento')}}",
            success: function(result) {
                var res = JSON.parse(result);
                const response = res['result'];
                const data = res['data'];
                if (response) {
                    $('#btnAdd').attr('disabled', false);
                    const date = Date.parse(data['FECHA_COMPROMISO']);
                    var timeinmilisec = date - today;
                    var days = (Math.ceil(timeinmilisec / (1000 * 60 * 60 * 24)));
                    var color = '';
                    if (days >= 30) {
                        color = '#36D152';
                    } else if (days < 30 && days > 0) {
                        color = '#FACA26';
                    } else {
                        color = '#F92222';
                    }
                    table = document.getElementById('cumplimientos');
                    rows = table.getElementsByTagName('tr');
                    index = rows.length - 1;
                    counter = document.getElementById('cumplimientosCounter');
                    counter.setAttribute("value", index + 1);
                    var mensaje = '';
                    if (days > 0) {
                        if (days == 1) {
                            mensaje = 'Mañana vence el cumplimiento';
                        } else {
                            mensaje = 'Quedan ' + days + ' días  vence el cumplimiento';
                        }
                    } else {
                        if (days == -1) {
                            mensaje = 'Ayer venció el cumplimiento';
                        } else if (days == 0) {
                            mensaje = 'Hoy día vence el cumplimiento';
                        } else {
                            mensaje = 'Han pasado ' + days + ' días vence el cumplimiento';
                        }
                    }
                    table.insertAdjacentHTML('beforeend', `
                    <tr>
                        <td>
                            <p id="ID_CUMPL_LABEL` + index + `">` + data['ID'] + `</p>
                            <input value="` + data['ID'] + `" name="ID_CUMPL` + index + `" id="ID_CUMPL` + index + `" type="hidden" class="form-control">
                        </td>
                        <td>
                            ` + data['TIPO_GESTION'] + `
                        </td>
                        <td>
                            ` + data['TIPO_COMPROMISO'] + `
                        </td>
                        <td>
                            ` + data['TIPO_COMPROMISO_DETALLE'] + `
                        </td>
                        <td>
                            ` + data['DETALLE_COMPRA'] + `
                        </td>
                        <td>
                            ` + data['INDICADOR'] + `
                        </td>
                        <td id="FECHA_COMPROMISO` + index + `">
                            ` + data['FECHA_COMPROMISO'] + ` 
                        </td>
                        <td>
                            ` + data['GESTOR'] + ` 
                        </td>
                        <td id="STATUS_COMPROMISO` + index + `">
                        </td>
                        <td>
                            <i id="semaforoInfo` + index + `" style="position: absolute;
                                    opacity: 0;
                                    height: 20px;
                                    width: 20px;
                                " class="fa fa-info-circle"  aria-hidden="true" data-toggle="tooltip" 
                                title="` + mensaje + `">
                            </i>
                            <svg class="bd-placeholder-img rounded mr-2" width="20" height="20" role="img">
                                <rect fill="` + color + `" width="100%" height="100%"></rect>
                            </svg>
                        </td>
                        <td style="padding-left: 10px;">
                            <div style="cursor:pointer" onclick="gestionCumplimiento('` + data['ID'] + `')">Gestionar</div> 
                        </td>
                    </tr>
                    `);
                    $('[data-toggle="tooltip"]').tooltip();
                    getCambios();
                    $("#nuevoCompromiso input, #nuevoCompromiso select").each(function() {
                        $(this).val(null);
                    });
                }

                $('#modalCumplimiento').hide();
                $('#modalCumplimiento').modal('toggle');
                $('#messageModal').html(res['message']);
                $('#modalGuardar').modal();
            }
        });
        return false;
    }

    function guardarEstrategia(id) {
        var values = {};
        values['ID'] = id;
        values['cu'] = $('#codUnico').val();
        $("#estrategia input, #estrategia select").each(function() {
            var isDisabled = $(this).prop('disabled');
            values[$(this).attr("name")] = !isDisabled ? $(this).val() : null;
        });
        var stringData = JSON.stringify(values);
        $.ajax({
            type: 'POST',
            data: {
                data: stringData
            },
            async: true,
            url: "{{route('infinity.alertascartera.guardarEstrategia')}}",
            success: function(result) {
                var res = JSON.parse(result);
                $('#messageModal').html(res['message']);
                $('#modalGuardar').modal();
                getCambios();
            }
        });
        return false;
    }

    function getCambios() {
        const id = $('#IDCOVID').val();
        $.ajax({
            type: 'POST',
            data: {
                'id': id
            },
            async: true,
            url: "{{route('infinity.alertascartera.getCambios')}}",
            success: function(result) {
                document.getElementById('ultimoscambios').innerHTML = "";
                for (let index = 0; index < result.length; index++) {
                    const element = result[index];
                    document.getElementById('ultimoscambios').insertAdjacentHTML('beforeend',
                        `
                        <li>
                            <div class="cbp_tmicon">
                            </div>
                            <div style="margin: 0 0 20px 34px; cursor: default;" class="col-sm-12">
                                <p style="padding-right: 40px;">` + element['MENSAJE'] + `</p>
                                <small>` + element['NOMBRE_USUARIO'] + `</small>
                                <small>` + element['FECHA_TEXTO'] + `</small>
                            </div>
                        </li>
                    `
                    );
                }
            }
        });
    }
    $('[data-toggle="tooltip"]').tooltip();

    $(document).on("click", "#vergys", function() {
        $("#modalmensajegys").modal();
        $("#data_comentario_gesgys").val($("#mengys").val());
    });
    $(document).on("click", "#agregargys", function() {
        $("#modalGesgys").modal();
    });

    function rendondear(numero) {
        var resultado = Math.trunc(numero * 100) / 100;
        return resultado;
    }

    function show(id, input) {
        elements = document.getElementsByClassName(id);
        val = input.value;
        for (let index = 0; index < elements.length; index++) {
            const element = elements[index];
            attribute = element.getAttribute('identificador');
            parteNumero = attribute.split("_")[1];
            if (val == '') {
                check = false;
            } else {
                check = parteNumero.includes(val.toString());
            }
            if (check) {
                element.style.display = "block";
                element.required = true;
                element.disabled = false;
            } else {
                element.style.display = "none";
                element.required = false;
                element.disabled = true;
            }
        }
    }

    function del(indexOrigi) {
        realIndex = parseInt(indexOrigi) + 1;
        document.getElementById('cumplimientos').deleteRow(realIndex);
        var element = document.getElementById('ID_CUMPL' + indexOrigi);
        if (element != null) {
            element.remove();
        }
        table = document.getElementById('cumplimientos');
        rows = table.getElementsByTagName('tr');
        i = rows.length - 1;
        counter = document.getElementById('cumplimientosCounter');
        counter.setAttribute("value", $('#cumplimientosCounter').val() - 1);

        for (let j = 1; j < rows.length; j++) {
            const elementRow = rows[j];
            index = j - 1;
            cell_0 = elementRow.cells[0];
            input0 = cell_0.getElementsByTagName('INPUT')[0];
            if (input0 != null) {
                input0.setAttribute('name', 'ID_CUMPL' + index);
                input0.setAttribute('id', 'ID_CUMPL' + index);
            }
            cell_6 = elementRow.cells[6];
            cell_6.setAttribute('name', 'FECHA_COMPROMISO' + index);
            cell_6.setAttribute('id', 'FECHA_COMPROMISO' + index);
            cell_8 = elementRow.cells[8];
            cell_8.setAttribute('name', 'STATUS_COMPROMISO' + index);
            cell_8.setAttribute('id', 'STATUS_COMPROMISO' + index);
            cell_9 = elementRow.cells[9];
            input9 = cell_9.getElementsByTagName('rect')[0];
            input9.setAttribute('id', 'RECT' + index);
            cell_10 = elementRow.cells[10];
            input10 = cell_10.getElementsByTagName('DIV')[0];
            if (input10 != null) {
                const idcumpl = $('#ID_CUMPL' + index).val();
                input10.setAttribute('onclick', 'gestionCumplimiento(' + idcumpl + ')');
            }
        }
    }

    $('.dfecha').each(function() {
        $(this).on('keydown', function() {
            return false;
        });
        $(this).datepicker({
            maxViewMode: 1,
            language: "es",
            autoclose: true,
            startDate: "-365d",
            endDate: "0d",
            format: "yyyy-mm-dd",
        });
    });
    $('.fechaCompromiso').each(function() {
        $(this).on('keydown', function() {
            return false;
        });
        const today = new Date();
        const tomorrow = new Date(today);
        tomorrow.setDate(tomorrow.getDate() + 1);
        $(this).datepicker({
            maxViewMode: 1,
            language: "es",
            autoclose: true,
            startDate: tomorrow.toISOString().substring(0, 10),
            format: "yyyy-mm-dd",
        });
    });

    function maxLengthMontoCheck(object) {
        if (Math.abs(object.value) > 999999999) {
            if (object.value < 0) {
                object.value = object.value.slice(0, 10)
            } else {
                object.value = object.value.slice(0, 9)
            }
        }
    }
    $('#slcQ14_1').on('change', function() {
        const val = $(this).val();
        if (val == 'Crecer') {
            $('#montoCrecer').attr('required', true);
            $('#montoCrecer').attr('disabled', false);
            $('#detalleCrecer').attr('required', true);
            $('#detalleCrecer').attr('disabled', false);
            $('#crecerWrapper').css('display', 'block');
        } else {
            $('#montoCrecer').attr('required', false);
            $('#montoCrecer').attr('disabled', true);
            $('#detalleCrecer').attr('required', false);
            $('#detalleCrecer').attr('disabled', true);
            $('#crecerWrapper').css('display', 'none');
        }
    });
    $(document).on("click", "#btnEstrategia", function() {
        $("#infoEstrategia").slideToggle("slow");
        if ($(this).hasClass("fa-chevron-down")) {
            $(this).removeClass('fa-chevron-down');
            $(this).addClass('fa-chevron-up');
        } else {
            $(this).removeClass('fa-chevron-up');
            $(this).addClass('fa-chevron-down');
        }
    });

    $(document).on("click", "#btninfodiaria", function() {
        $("#infodiaria").slideToggle("slow");
        if ($(this).hasClass("fa-chevron-down")) {
            $(this).removeClass('fa-chevron-down');
            $(this).addClass('fa-chevron-up');
        } else {
            $(this).removeClass('fa-chevron-up');
            $(this).addClass('fa-chevron-down');
        }
    });
    $(document).on("click", "#btnsemaforizacion", function() {
        $("#semaforizacion").slideToggle("slow");
        if ($(this).hasClass("fa-chevron-down")) {
            $(this).removeClass('fa-chevron-down');
            $(this).addClass('fa-chevron-up');
        } else {
            $(this).removeClass('fa-chevron-up');
            $(this).addClass('fa-chevron-down');
        }
    });
    $(document).on("click", "#btninfohistorica", function() {
        $("#infohistorica").slideToggle("slow");
        if ($(this).hasClass("fa-chevron-down")) {
            $(this).removeClass('fa-chevron-down');
            $(this).addClass('fa-chevron-up');
        } else {
            $(this).removeClass('fa-chevron-up');
            $(this).addClass('fa-chevron-down');
        }
    });
    $(document).on("click", "#btnpoli_gestion_LA", function() {
        $("#poli_gestion_LA").slideToggle("slow");
        if ($(this).hasClass("fa-chevron-down")) {
            $(this).removeClass('fa-chevron-down');
            $(this).addClass('fa-chevron-up');
        } else {
            $(this).removeClass('fa-chevron-up');
            $(this).addClass('fa-chevron-down');
        }
    });
    $(document).on("click", "#btnpoli_seguidiario_LA", function() {
        $("#poli_seguidiario_LA").slideToggle("slow");
        if ($(this).hasClass("fa-chevron-down")) {
            $(this).removeClass('fa-chevron-down');
            $(this).addClass('fa-chevron-up');
        } else {
            $(this).removeClass('fa-chevron-up');
            $(this).addClass('fa-chevron-down');
        }
    });
    $(document).on("click", "#btnpoli_seguivigente", function() {
        $("#poli_seguivigente").slideToggle("slow");
        if ($(this).hasClass("fa-chevron-down")) {
            $(this).removeClass('fa-chevron-down');
            $(this).addClass('fa-chevron-up');
        } else {
            $(this).removeClass('fa-chevron-up');
            $(this).addClass('fa-chevron-down');
        }
    });
    $(document).on("click", ".ultmen", function() {
        $("#tog" + $(this).attr('id') + "").slideToggle("slow");
    });

    $(document).ready(function() {
        $("#btninfocli").click();
        $("#btninfodiaria").click();
        $("#btnEstrategia").click();
        $("#btnsemaforizacion").click();
        $("#btninfohistorica").click();
        $("#btnpoli_gestion_LA").click();
        $("#btnpoli_seguidiario_LA").click();
        $("#btnpoli_seguivigente").click(); //agregar para que por defecto aparezca primero
        generarGrafico($('#codUnico').val());

        $(document).on("click", ".detalleact", function() {
            $("#modalDetalleActividad").modal();
            var _id = $(this).closest('tr').attr('id');
            if (_id) {
                $.ajax({
                    type: "GET",
                    data: {
                        id: _id,
                    },
                    url: "{{route('infinity.alertascartera.getdetactividad')}}",
                    success: function(result) {
                        // console.log(result[0]);
                        $("#d_id").val(result[0].ID);
                        $("#d_fecha").val(result[0].FECHA_ACTIVIDAD);
                        $("#d_tipo").val(result[0].TIPO);
                        $("#d_accion").val(result[0].ACCION);
                        $("#d_motivo").val(result[0].MOTIVO_IMPAGO);
                        $("#d_compromiso").val(result[0].COMPROMISO_PAGO);
                        $("#d_responsable").val(result[0].RESPONSABLE);
                        $("#d_deudatotal").val(rendondear(result[0].DEUDA_TOTAL));
                        $("#d_deudaatrasada").val(rendondear(result[0].DEUDA_ATRASADA));
                        $("#d_deudavencida").val(rendondear(result[0].DEUDA_VENCIDA));
                        $("#d_pctdeudaproblema").val(rendondear(result[0].PCT_DEUDA_PROBLEMA) * 100);
                        $("#d_dias").val(result[0].DIAS);
                        $("#d_comentario").val(result[0].COMENTARIO);
                        $("#d_cumpliocompromiso").val(result[0].CUMPLE_COMPROMISO);
                    }
                });
            }
        })

        /*Combobox Mes de explicación, cambia las filas a mostrar*/
        $('#cboMesExplicacion').change(function() {

            $('.tableHistorico tbody >tr')
                .find('.tr-empty').remove().end()
                .addClass('hidden').end()
                .find('.tr-' + $(this).val()).removeClass('hidden');

            if ($('#tableSalidas tbody>tr').not('.hidden').length == 0) {
                $('#tableSalidas tbody').append('<tr class="tr-empty"><td colspan="2">El cliente se mantuvo en Líneas Automáticas</td><tr>')
            }

            if ($('#tablePoliticas tbody>tr').not('.hidden').length == 0) {
                $('#tablePoliticas tbody').append('<tr class="tr-empty"><td colspan="2">El cliente no tiene políticas aplicadas</td><tr>')
            }

            if ($('#tableVariables tbody>tr').not('.hidden').length == 0) {
                $('#tableVariables tbody').append('<tr class="tr-empty"><td colspan="2">No existen variables para mostrar</td><tr>')
            }
        })

        //La fecha no se registra para documentos DDJJ y EEFF
        $('#cboTipoDocumento').change(function() {
            // console.log($(this).val());
            if ($(this).val() <= 2)
                $('#fechaFirmaOcultar').attr('hidden', 'true');
            else
                $('#fechaFirmaOcultar').removeAttr('hidden');
        })

        $('#btnDocumentacion').on("click", function() {
            $('#frmDocumentacion').trigger("reset");
            $('#frmDocumentacion').formValidation('destroy', true);
            initializeFormDocumentacion();
            $('#modalDocumentacion').modal();
        })

        $('#btnGrupoEconomico').on("click", function() {
            $('#modalGrupoEconomico').modal();
        })
        $("[data-toggle=popover]").each(function(i, obj) {
            $(this).popover({
                html: true,
                content: function() {
                    html = '<b>Rating:</b> ' + $(this).attr('data-rating');
                    html += '<br><b>Mora:</b> ' + $(this).attr('data-mora');
                    return html;
                }
            });
        });

        $('.dfecha').each(function() {
            $(this).datepicker({
                    maxViewMode: 1,
                    //daysOfWeekDisabled: "0,6",
                    language: "es",
                    autoclose: true,
                    startDate: "-365d",
                    endDate: "0d",
                    format: "yyyy-mm-dd",
                })
                .on('changeDate', function(e) {
                    // Revalidate the date field
                    $('#frmGestion').formValidation('revalidateField', 'fechaGestion');
                    $('#frmDocumentacion').formValidation('revalidateField', 'fechaFirma');
                    $('#frmRecalculo').formValidation('revalidateField', 'fechaRecalculo');
                });
        });


        $('.dfechacp').each(function() {
            $(this).datepicker({
                maxViewMode: 1,
                //daysOfWeekDisabled: "0,6",
                language: "es",
                autoclose: true,
                startDate: "0d",
                format: "yyyy-mm-dd",
            })
        });

        $('#frmGestion').formValidation({
                framework: 'bootstrap',
                icon: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    'estadoGestion': {
                        validators: {
                            notEmpty: {
                                message: 'Debes seleccionar un estado'
                            },
                        }
                    },
                    'fechaGestion': {
                        validators: {
                            notEmpty: {
                                message: 'Debes seleccionar una fecha'
                            },
                            date: {
                                format: 'YYYY-MM-DD',
                                message: 'Ingrese una fecha válida',
                            }
                        }
                    },
                    'comentarioGestion': {
                        validators: {
                            notEmpty: {
                                message: 'Debes seleccionar un estado'
                            },
                            stringLength: {
                                max: 500,
                                min: 30,
                                message: 'El comentario debe tener entre 30 y 500 caracteres'
                            }
                        }
                    },
                },
            })
            .off('success.form.fv');

        $('#frmRecalculo').formValidation({
                framework: 'bootstrap',
                icon: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    'mdlRecalculo': {
                        validators: {
                            notEmpty: {
                                message: 'Debes llenar el monto del MDL'
                            },
                            numeric: {
                                message: 'El MDL debe ser un monto numérico',
                                // The default separators
                                thousandsSeparator: '',
                                decimalSeparator: '.'
                            }
                        }
                    },
                    'fechaRecalculo': {
                        validators: {
                            notEmpty: {
                                message: 'Debes seleccionar una fecha'
                            },
                            date: {
                                format: 'YYYY-MM-DD',
                                message: 'Ingrese una fecha válida',
                            }
                        }
                    },
                    'adjuntoRecalculo': {
                        validators: {
                            notEmpty: {
                                message: 'Debes seleccionar un archivo'
                            },
                        }
                    },
                },
            })
            .off('success.form.fv');

    });

    function initializeFormDocumentacion() {

        $('#frmDocumentacion').formValidation({
                framework: 'bootstrap',
                icon: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    'tipoDocumento': {
                        validators: {
                            notEmpty: {
                                message: 'Debes seleccionar un tipo'
                            },
                        }
                    },
                    'fechaFirma': {
                        validators: {
                            notEmpty: {
                                message: 'Debes seleccionar una fecha'
                            },
                            date: {
                                format: 'YYYY-MM-DD',
                                message: 'Ingrese una fecha válida',
                            }
                        }
                    },
                    'adjuntoDocumento': {
                        validators: {
                            notEmpty: {
                                message: 'Debes seleccionar un archivo'
                            },
                        }
                    },
                },
            })
            .off('success.form.fv');
    }

    function generarGrafico(cu) {

        $.ajax({
            type: "GET",
            data: {
                cu: cu,
            },
            async: false,
            url: APP_URL + 'infinity/me/cliente/detalle/grafico',
            success: function(result) {
                //console.log(result);
                var periodos = [];
                var nivelInverso = [];
                var nivel = [];
                var colores = [];

                for (var i = 0; i < result.length; i++) {
                    periodos.push(result[i]['PERIODO']);


                    switch (result[i]['NIVEL_ALERTA']) {
                        case '1':
                        case '11':
                            nivelInverso.push(4);
                            break;
                        case '2':
                        case '22':
                            nivelInverso.push(3);
                            break;
                        case '3':
                        case '33':
                            nivelInverso.push(2);
                            break;
                        case '99':
                        case '999':
                            nivelInverso.push(1);
                            break;
                        default:
                            nivelInverso.push(0);
                            break;
                    }

                    switch (result[i]['NIVEL_ALERTA']) {
                        case '1': //VERDE
                        case '11':
                            colores.push('green');
                            break;
                        case '2': //AMBAR
                        case '22':
                            colores.push('#fc3');
                            break;
                        case '3': //ROJO
                        case '33':
                            colores.push('red');
                            break;
                        case '99': //NEGRO
                        case '999':
                            colores.push('#000000');
                            break;
                        default:
                            colores.push('gray');
                            break;
                    }

                }
                //console.log(nivel);
                //console.log(nivelInverso);
                var config = {
                    type: 'line',
                    data: {
                        labels: periodos,
                        datasets: [{
                            label: 'Evolución Infinity',
                            //backgroundColor: window.chartColors.blue,
                            borderColor: 'gray',
                            data: nivelInverso,
                            fill: false,
                            borderDash: [5],
                            pointRadius: 15,
                            pointHoverRadius: 10,
                            pointBackgroundColor: colores
                        }, ]
                    },
                    options: {
                        responsive: true,
                        hover: {
                            mode: 'nearest',
                            intersect: true
                        },
                        legend: {
                            display: false,
                        },
                        scales: {
                            xAxes: [{
                                display: true,
                                scaleLabel: {
                                    display: false,
                                    labelString: 'Month'
                                }
                            }],
                            yAxes: [{
                                display: false,
                                ticks: {
                                    suggestedMax: 4.5,
                                    suggestedMin: 0.5
                                }
                            }]
                        },
                        tooltips: {
                            callbacks: {
                                title: function(tooltipItem, data) {
                                    // console.log(result[tooltipItem[0]['index']]);
                                    return 'LA: ' + ((result[tooltipItem[0]['index']]['FLG_INFINITY'] == 1) ? 'Sí' : 'No');
                                },
                                label: function(tooltipItem, data) {
                                    i = 0;

                                    nivelInverso = [];
                                    while (periodos[i] != tooltipItem['xLabel'])
                                        i++;

                                    nivelInverso.push('Rating: ' + result[i]['RATING']);
                                    nivelInverso.push('Deuda IBK: S/.' + numeral(result[i]['SALDO_INTERBANK']).format('0,0'));
                                    nivelInverso.push('Deuda RCC: S/.' + numeral(result[i]['SALDO_RCC']).format('0,0'));
                                    nivelInverso.push('Días Atraso: ' + result[i]['MAX_DIAS_ATRASO']);
                                    return nivelInverso;
                                },

                            },
                            backgroundColor: '#FFF',
                            titleFontSize: 16,
                            titleFontColor: '#000',
                            bodyFontColor: '#000',
                            bodyFontSize: 14,
                            displayColors: false
                        }
                    }
                };

                window.onload = function() {
                    var ctx = document.getElementById('graficoEvolucion').getContext('2d');
                    window.myLine = new Chart(ctx, config);

                };


            }
        });
    }

    $(document).on("click", "#btnVerGestion", function() {
        $('#modalVerGestion').modal();

        $("#fechaen1").html('');
        $("#fechaen2").html('');
        $("#fechaen3").html('');
        $("#accionen1").html('');
        $("#accionen2").html('');
        $("#accionen3").html('');
        $("#resen1").html('');
        $("#comen1").html('');
        $("#resen2").html('');
        $("#comen2").html('');
        $("#resen3").html('');
        $("#comen3").html('');
        $("#en1").html('');
        $("#en2").html('');
        $("#en3").html('');

        $.ajax({
            type: "GET",
            data: {
                id: $(this).attr('id_gest'),
            },
            async: false,
            url: "{{route('infinity.alertascartera.getMensajesAntiguos')}}",
            success: function(result) {
                // console.log(result[0]);
                $("#fechaen1").html(result[0]['FEC_RES1']);
                $("#fechaen2").html(result[0]['FEC_RES2']);
                $("#fechaen3").html(result[0]['FEC_RES3']);
                $("#accionen1").html(result[0]['ACC1_EN']);
                $("#accionen2").html(result[0]['ACC2_EN']);
                $("#accionen3").html(result[0]['ACC3_EN']);
                $("#resen1").html(result[0]['RES1_EN']);
                $("#comen1").html(result[0]['RES1_COM']);
                $("#resen2").html(result[0]['RES2_EN']);
                $("#comen2").html(result[0]['RES2_COM']);
                $("#resen3").html(result[0]['RES3_EN']);
                $("#comen3").html(result[0]['RES3_COM']);
                $verde = "<i class='fas fa-check fa-lg' style='color:#26B99A'></i>";
                $rojo = "<i class='fas fa-times fa-lg' style='color:#D9534F'></i>";
                $("#en1").html(result[0]['RES1_ESTADO'] == '1' ? $verde : (result[0]['RES1_ESTADO'] == '2' ? $rojo : ""));
                $("#en2").html(result[0]['RES2_ESTADO'] == '1' ? $verde : (result[0]['RES2_ESTADO'] == '2' ? $rojo : ""));
                $("#en3").html(result[0]['RES3_ESTADO'] == '1' ? $verde : (result[0]['RES3_ESTADO'] == '2' ? $rojo : ""));

            }
        });
    });

    $('#tblHistoricoCliente').DataTable({
        processing: true,
        "bAutoWidth": false,
        serverSide: true,
        language: {
            "url": "{{ URL::asset('js/Json/Spanish.json') }}"
        },
        ajax: {
            url: "{{ route('infinity.me.cliente.historia') }}",
            data: {
                "cliente": $('#codUnico').val()
            }
        },
        order: [
            [0, "desc"]
        ],

        columnDefs: [{
            targets: 4,
            data: null,
            render: function(data, type, row) {
                var url = '';
                if (row.TIPO === 'VISITA') {
                    url = "{{ route('infinity.me.visita.historia') }}" + "?id=" + row.ID;
                } else {
                    url = "{{ route('infinity.me.conoceme.historia') }}" + "?id=" + row.ID;
                }
                return '<a target="_blank" href="' + url + '">Ver</a>';
            }
        }, ],
        columns: [{
                data: 'ID',
                name: 'WICON.ID'
            },
            {
                data: 'TIPO',
                name: 'WICON.TIPO'
            },
            {
                data: 'USUARIO',
                name: 'WU.NOMBRE'
            },
            {
                data: 'FECHA_ACTUALIZACION',
                name: 'WICON.FECHA_ACTUALIZACION',
                searchable: false
            },
        ]
    });

    $(document).on("change", "#estadoGestion", function() {
        if ($(this).val() == 3) {
            $(".selec_politicas").attr('hidden', 'hidden');
        } else {
            $(".selec_politicas").removeAttr('hidden');
        }
    });

    $(document).on("click", ".aprobacion", function() {
        if ($('input:radio[name=aprobacion]:checked').val() == '1') {
            $(".checkboxtiempoespera").removeAttr('hidden');
            $(".checktiempoespera").removeAttr('disabled');
        } else if ($('input:radio[name=aprobacion]:checked').val() == '2') {
            $(".checkboxtiempoespera").attr('hidden', 'hidden');
            $(".checktiempoespera").attr('disabled', 'disabled');
        }
    });
</script>
@stop