@extends('be.infinity.detallejs')


@section('pageTitle')
{{$cliente->NOMBRE}} <i class="fa fa-circle fa-sm" style="color: {{\App\Entity\Infinity\Semaforo::getColor($cliente->NIVEL_ALERTA)}}"></i>
@stop

@section('vista_detalle')

<style>
    .GraphName {
        text-align: center;
        font-size: 30px;
        padding: 17px;
    }

    .cbp_tmtimeline:before {
        content: '';
        position: absolute;
        top: 0;
        bottom: 0;
        width: 6px;
        background: #afdcf8;
        left: 5%;
        margin-left: -9px;
    }

    .score {
        position: absolute;
        height: 100px;
        width: 100px;
        display: flex;
        align-items: center;
        justify-content: center;
        font-size: 1.5em;
        font-weight: bolder;
    }

    .celda-centrada {
        vertical-align: middle;
        text-align: center;
    }

    .tr-empty {
        background-color: #f9f9f9;
    }

    .panel {
        padding: 0 18px;
        background-color: white;
        max-height: 0;
        overflow: hidden;
        transition: max-height 0.2s ease-out;
    }

    th,
    td {
        border: 1px solid #dddddd;
        text-align: center;
        padding: 8px;
    }

    .cbp_tmtimeline>li .cbp_tmicon {
        width: 10px;
        height: 10px;
        font-family: 'ecoico';
        speak: none;
        font-style: normal;
        font-weight: normal;
        font-variant: normal;
        display: flex !important;
        justify-content: center !important;
        align-items: center !important;
        text-transform: none;
        font-size: 1.4em;
        line-height: 10px;
        -webkit-font-smoothing: antialiased;
        position: absolute;
        color: #fff;
        background: #46a4da;
        border-radius: 50%;
        box-shadow: 0 0 0 5px #afdcf8;
        text-align: center;
        left: 0%;
        top: 0;
        margin: 0px 0 0 7px;
    }
</style>
<!-- <div class="row">
    <div class="col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="col-sm-11 col-xs-12">
                <h2><b>Información General</b></h2>
            </div>
            <div class="col-sm-1 col-xs-12" align="center">
                <h1><i id="btninfocli" class="fa fa-chevron-down" style="width:6; height:6;font-size:25px !important" aria-hidden="true"></i></h1>
            </div>
        </div>
    </div>
</div> -->
<div class="row">
    <div class="x_panel">
        <div class="col-sm-5">
            <h2>{{$cliente->NOMBRE}}</h2>
        </div>
        <div class="col-sm-7" style="padding-left: 100px">
            <div class="col-sm-4">
                <i class="fa fa-address-card-o" aria-hidden="true" style="font-size: 19px;padding-right: 10px;"></i><a target="_blank" href="{{route('infinity.me.cliente.conoceme')}}?cu={{$cliente->COD_UNICO}}" style="font-weight: bold">CONOCEME</a>
            </div>
            <div class="col-sm-5">
                <i class="fas fa-user-friends" aria-hidden="true" style="font-size: 19px;padding-right: 10px;"></i><a target="_blank" href="{{route('infinity.me.cliente.visita')}}?cu={{$cliente->COD_UNICO}}" style="font-weight: bold">VISITA + COVID</a>
            </div>

            <div class="col-sm-3">
                <i class="fa fa-file-text-o" aria-hidden="true" style="font-size: 19px;padding-right: 10px;"></i><a id="btnDocumentacion" style="font-weight: bold;" href="#">DOC</a>
            </div>
        </div>
    </div>
</div>
<div class="row" id="infocli">
    <div class="x_panel">
        <div style="margin-top: 20px;" class="col-sm-6 col-xs-12">
            <div class="x_panel" style="min-height: 200px">
                <div class="x_title">
                    <h2>Cliente</h2>
                    <ul class="nav navbar-right panel_toolbox">
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <form class="form-horizontal form-label-left input_mask">
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class="control-label col-sm-5 col-xs-12">CU</label>
                                <div class="col-sm-7 col-xs-12">
                                    <input class="form-control" type="text" value="{{$cliente->COD_UNICO}}" readonly="readonly" id="codUnico">
                                </div>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="control-label col-sm-4 col-xs-12">Ejecutivo</label>
                                <div class="col-sm-8 col-xs-12">
                                    <input class="form-control" type="text" value="{{$cliente->EJECUTIVO}}" readonly="readonly">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class="control-label col-sm-5 col-xs-12">Deuda RCC</label>
                                <div class="col-sm-7 col-xs-12">
                                    <input class="form-control" type="text" value="S/. {{number_format($cliente->SALDO_RCC,0,'.',',')}}" readonly="readonly">
                                </div>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="control-label col-sm-4 col-xs-12">SOW</label>
                                <div class="col-sm-8 col-xs-12">
                                    <input class="form-control" type="text" value="{{number_format($cliente->SOW,0,'.',',')}}%" readonly="readonly">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="control-label col-sm-5 col-xs-12">Vencido</label>
                                <div class="col-sm-7 col-xs-12">
                                    <input class="form-control" type="text" value="S/. {{number_format($cliente->SALDO_VENCIDO_RCC,0,'.',',')}}" readonly="readonly">
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label col-sm-4 col-xs-12">Rating</label>
                                <div class="col-sm-8 col-xs-12">
                                    <input class="form-control" type="text" value="{{$cliente->RATING}}" readonly="readonly">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="control-label col-sm-5 col-xs-12">Clasificación</label>
                                <div class="col-sm-7 col-xs-12">
                                    <input class="form-control" type="text" value="{{$cliente->CLASIFICACION}}" readonly="readonly">
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label col-sm-4 col-xs-12">FEVE</label>
                                <div class="col-sm-8 col-xs-12">
                                    <input class="form-control" type="text" value="{{$cliente->FEVE}}" readonly="readonly">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div style="margin-top: 20px;" class="col-sm-6 col-xs-12">
            <div class="x_panel" style="min-height: 255px">
                <div class="x_title">
                    <h2>Status</h2>
                    <ul class="nav navbar-right panel_toolbox">
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <form class="form-horizontal form-label-left input_mask">
                        <div class="row">
                            <!-- EEFF1 -->
                            <div class="form-group col-sm-6">
                                <label class="control-label col-sm-3 col-xs-12">DDJJ</label>
                                <div class="col-sm-6 col-xs-12">
                                    <input class="form-control" type="text" value="{{$cliente->FECHA_ULTIMO_EEFF_1}}" readonly="readonly">
                                </div>
                                <div class="col-sm-1 col-xs-12">
                                    <?php
                                    echo \App\Entity\Infinity\AlertaDocumentacion::getIcono(\App\Entity\Infinity\AlertaDocumentacion::ID_DDJJ, $cliente->FECHA_ULTIMO_EEFF_1, $cliente->FLG_INFINITY, $cliente->FECHA_INGRESO_INFINITY);
                                    ?>
                                </div>
                            </div>
                            <!-- EEFF2 -->
                            <div class="form-group col-sm-6">
                                <label class="control-label col-sm-3 col-xs-12">EEFF</label>
                                <div class="col-sm-6 col-xs-12"><input hidden="hidden" value="{{$cliente->FECHA_ULTIMO_EEFF_2}}">
                                    <input class="form-control" type="text" value="{{$cliente->FECHA_ULTIMO_EEFF_2}}" readonly="readonly">
                                </div>
                                <div class="col-sm-1 col-xs-12">
                                    <?php
                                    echo \App\Entity\Infinity\AlertaDocumentacion::getIcono(\App\Entity\Infinity\AlertaDocumentacion::ID_EEFF, $cliente->FECHA_ULTIMO_EEFF_2, $cliente->FLG_INFINITY, $cliente->FECHA_INGRESO_INFINITY);
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class="control-label col-sm-3 col-xs-12">IBR</label>
                                <div class="col-sm-6 col-xs-12">
                                    <input class="form-control" type="text" value="{{$cliente->FECHA_ULTIMO_IBR}}" readonly="readonly">
                                </div>
                                <div class="col-sm-1 col-xs-12">
                                    <?php
                                    echo \App\Entity\Infinity\AlertaDocumentacion::getIcono(\App\Entity\Infinity\AlertaDocumentacion::ID_IBR, $cliente->FECHA_ULTIMO_IBR, $cliente->FLG_INFINITY, $cliente->FECHA_INGRESO_INFINITY);
                                    ?>
                                </div>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="control-label col-sm-3 col-xs-12">F02</label>
                                <div class="col-sm-6 col-xs-12">
                                    <input class="form-control" type="text" value="{{$cliente->FECHA_ULTIMO_F02}}" readonly="readonly">
                                </div>
                                <div class="col-sm-1 col-xs-12">
                                    <?php
                                    echo \App\Entity\Infinity\AlertaDocumentacion::getIcono(\App\Entity\Infinity\AlertaDocumentacion::ID_F02, $cliente->FECHA_ULTIMO_F02, $cliente->FLG_INFINITY, $cliente->FECHA_INGRESO_INFINITY);
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class="control-label col-sm-3 col-xs-12">Última Visita</label>
                                <div class="col-sm-6 col-xs-12">
                                    <input class="form-control" type="text" value="{{$cliente->FECHA_ULTIMA_VISITA}}" readonly="readonly">
                                </div>
                                <div class="col-sm-1 col-xs-12">
                                    <?php
                                    echo \App\Entity\Infinity\AlertaDocumentacion::getIcono(\App\Entity\Infinity\AlertaDocumentacion::ID_VISITA, $cliente->FECHA_ULTIMA_VISITA, $cliente->FLG_INFINITY, $cliente->FECHA_INGRESO_INFINITY);
                                    ?>
                                </div>
                            </div>
                            <div class="form-group col-sm-6">
                                <!-- <label class="control-label col-sm-3 col-xs-12">Grupo Económico</label> -->
                                <label class="control-label col-sm-9 col-xs-12"><i class="fa fa-hand-o-right" aria-hidden="true" style="font-size: 19px; padding-right:10px;"></i><u><a href="#" id="btnGrupoEconomico" style="font-weight: bold;text-align: center;margin-top: 20px">GRUPO ECONÓMICO</a></u></label>
                                <!-- <button class="btn btn-success" type="button" id="btnGrupoEconomico">Ver Detalles</button> -->
                                <div class="col-sm-1 col-xs-12">
                                    @if($grupoEconomico!=NULL)
                                    @if($grupoEconomico['FLG_AUTONOMIA'])
                                    <span><i class="fas fa-check fa-lg" style="color:#26B99A;padding-top: 8px"></i></span>
                                    @else
                                    <span><i class="fas fa-exclamation fa-lg" style="color:#D9534F;padding-top: 8px"></i></span>
                                    @endif
                                    <!-- <button class="btn btn-success" type="button" id="btnGrupoEconomico">Ver Detalles</button> -->
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!-- <div class="row">
                            <div class="form-group col-sm-8">
                                <center><button type="button" class="btn btn-primary" id="btnDocumentacion">Gestión de Documentación</button></center>
                            </div>
                        </div> -->
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="x_panel">
        <div class="col-sm-11 col-xs-12">
            <h2><b>Información Diaria</b></h2>
        </div>
        <div class="col-sm-1 col-xs-12" align="center">
            <h1><i id="btninfodiaria" class="fa fa-chevron-up" style="width:6; height:6;font-size:25px !important" aria-hidden="true"></i></h1>
        </div>
    </div>
</div>
<div class="row" id="infodiaria">
    <div class="x_panel">
        <div style="margin-top: 20px;" class="col-sm-6 col-xs-12">
            <div class="x_panel" style="min-height: 280px">
                <div class="x_title">
                    <h2>Deuda IBK</h2>
                    <ul class="nav navbar-right panel_toolbox">
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4">Colocación Total IBK :</label>
                            <div class="col-sm-8">
                                <input class="form-control" type="text" disabled value="S/. {{$clienteseguimiento ? number_format($clienteseguimiento->SALDO_TOT_IBK,0,'.',',') : ''}}">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4">Colocación IBK no Vigente :</label>
                            <div class="col-sm-8">
                                <input class="form-control" type="text" disabled value="S/. {{$clienteseguimiento ? number_format($clienteseguimiento->SALDO_NO_VIGENTE,0,'.',',') : ''}}">
                            </div>
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4">Colocación sin Atraso IBK :</label>
                            <div class="col-sm-8">
                                <input class="form-control" type="text" disabled value="S/. {{$clienteseguimiento ? number_format($clienteseguimiento->SALDO_IBK_SIN_ATRASO,0,'.',',') : ''}}">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4">Fecha Atraso:</label>
                            <div class="col-sm-8">
                                <input class="form-control" type="text" disabled value="{{$clienteseguimiento ? $clienteseguimiento->FECHA : ''}}">
                            </div>
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4">Colocación con Atraso IBK :</label>
                            <div class="col-sm-8">
                                <input class="form-control" type="text" disabled value="S/. {{$clienteseguimiento ? number_format($clienteseguimiento->SALDO_IBK_CON_ATRASO,0,'.',',') : ''}}">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4">Días Atraso:</label>
                            <div class="col-sm-8">
                                <input class="form-control" type="text" disabled value="{{$clienteseguimiento ? $clienteseguimiento->DIAS : ''}}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="margin-top: 20px;" class="col-sm-6 col-xs-12">
            <div class="x_panel" style="min-height: 280px">
                <div class="x_title">
                    <div class="col-sm-3">
                        <h2>Productos</h2>
                    </div>
                    <div class="col-sm-9" align="center">
                        <label class="control-label col-sm-4 col-xs-12">Fecha :</label>
                        <div class="input-group col-sm-4 col-xs-12">
                            <input style="width: 100px" autocomplete="off" class="form-control" type="text" placeholder="{{$fecha_actividad}}" disabled>
                        </div>
                    </div>
                    <ul class="nav navbar-right panel_toolbox">
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content" style="max-height: 200px; overflow-y: scroll;">
                    <div class="input-group col-md-12 col-sm-12 col-xs-12">
                        <table class="table table-striped jambo_table">
                            <thead>
                                <tr>
                                    <th>Producto</th>
                                    <th>N° Cred</th>
                                    <th>Situación</th>
                                    <th>Fecha Atraso</th>
                                    <th>N° días Atraso</th>
                                    <th>Monto</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($productosdiarios)
                                @foreach($productosdiarios as $productos)
                                <tr>
                                    <td>{{$productos->PRODUCTO}}</td>
                                    <td>{{$productos->NUM_CREDITO}}</td>
                                    <td>{{$productos->SITUACION}}</td>
                                    <td>{{$productos->FECHA_VENCIMIENTO}}</td>
                                    <td>{{$productos->NUM_DIAS_VENCIDOS}}</td>
                                    <td style="vertical-align: middle;">S/. {{number_format($productos->DEUDA,0,'.',',') }}</td>
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td colspan="6" align="center">No hay datos</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="x_panel">
        <div class="col-sm-11 col-xs-12">
            <h2>
                <b>Semaforización</b>
                @if(!empty($cliente) && $cliente->PROTEGIDO=='1')
                <i class='fa fa-shield' aria-hidden='true' style="color: #F5B54D;margin-top: 13px;margin-left: 20px"></i> ({{$cliente->TIEMPO_RESTANTE==1 ? 'Falta '.$cliente->TIEMPO_RESTANTE.' mes' : 'Faltan '.$cliente->TIEMPO_RESTANTE.' meses'}})
                @endif
            </h2>
        </div>
        <div class="col-sm-1 col-xs-12" align="center">
            <h1><i id="btnsemaforizacion" class="fa fa-chevron-up" style="width:6; height:6;font-size:25px !important" aria-hidden="true"></i></h1>
        </div>
    </div>
</div>
<div class="row" id="semaforizacion">
    <div class="x_panel">
        <div class="col-sm-6 col-xs-12">
            <div class="x_panel" style="min-height: 380px">
                <div class="x_title">
                    <h2>Evolución</h2>
                    <ul class="nav navbar-right panel_toolbox">
                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <center>
                        <div class="chart-container">
                            <canvas id="graficoEvolucion" height="90"></canvas>
                        </div>
                    </center>
                </div>
            </div>

        </div>
        <div class="col-sm-6 col-xs-12">
            <div class="x_panel" style="min-height: 380px">
                <div class="x_title">
                    <h2>Explicación</h2>
                    <ul class="nav navbar-right panel_toolbox">
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <table class="table table-striped jambo_table tableHistorico" id="tableVariables">
                            <thead>
                                <tr>
                                    <th>Grupo</th>
                                    <th>Variable</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if ($variables && count($variables) > 0)
                                @foreach ($variables as $i => $var)
                                @foreach ($var as $j => $v)
                                <tr class="tr-{{$j}} {{$j == config('app.periodoInfinity')? '':'hidden'}}">
                                    <td>{{$i}}</td>
                                    <td>{{implode(',',array_unique($v))}}</td>
                                </tr>
                                @endforeach
                                @endforeach
                                @else
                                <tr class="tr-empty">
                                    <td colspan="2">El cliente no tiene variables a revisar</td>
                                <tr>
                                    @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="x_panel">
        <div class="col-sm-11 col-xs-12">
            <h2>
                <b>Políticas y Gestión - Líneas Automáticas</b>
                @if(!empty($cliente) && $cliente->CANT_GESTIONES_PENDIENTES>0)
                <i class='fa fa-exclamation' aria-hidden='true' style="color: #f16b6b;margin-top: 13px;margin-left: 20px"></i>
                @endif
            </h2>
        </div>
        <div class="col-sm-1 col-xs-12" align="center">
            <h1><i id="btnpoli_gestion_LA" class="fa fa-chevron-up" style="width:6; height:6;font-size:25px !important" aria-hidden="true"></i></h1>
        </div>
    </div>
</div>
<div class="row" id="poli_gestion_LA">
    <div class="x_panel">
        <div class="col-md-7">
            <div class="x_panel" style="min-height: 250px">
                <div class="x_title">
                    <h2>Políticas y Variables</h2>
                    <ul class="nav navbar-right panel_toolbox">
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row" style="padding-bottom: 30px">
                        <div class="col-md-12">
                            <table class="table table-striped jambo_table tableHistorico" id="tablePoliticas">
                                <thead>
                                    <tr>
                                        <th>Periodo Política</th>
                                        <th>Tipo</th>
                                        <th>Política</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (count($politicas) > 0)
                                    @foreach ($politicas as $politica)
                                    <tr>
                                        <td>{{$politica->PERIODO}}</td>
                                        <td>{{$politica->TIPO}}</td>
                                        <td>{{$politica->NOMBRE}}</td>
                                        <td>{{$politica->ESTADO_FINAL_GESTION}}</td>
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr class="tr-empty">
                                        <td colspan="4">El cliente no tiene políticas aplicadas</td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="x_panel" style="min-height: 250px">
                <div class="x_title">
                    <h2>Gestiones y Observaciones</h2>
                    <ul class="nav navbar-right panel_toolbox">
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content" style="max-height: 380px; <?php echo (count($gestiones) > 0 ? 'overflow-y: scroll' : ''); ?>;">
                    <div class="row" style="padding-bottom: 30px">
                        <div class="col-md-12">
                            <table class="table table-striped table-bordered jambo_table">
                                <thead>
                                    <tr>
                                        <th>Usuario</th>
                                        <th>Fecha</th>
                                        <th>Comentario</th>
                                        <th>Politica Asociada</th>
                                        <th>Tipo de Gestión</th>
                                        <th>Adjunto</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($gestiones && count($gestiones)>0)
                                    @foreach($gestiones as $gestion)
                                    <tr>
                                        <td style="width: 15%;padding-bottom: 30px">{{$gestion->NOMBRE}}<br>{{$gestion->CARGO}}</td>
                                        <td style="width: 10%;padding-bottom: 30px">{{$gestion->FECHA_GESTION}}</td>
                                        <td style="width: 20%;padding-bottom: 30px;text-align: justify;">{{$gestion->COMENTARIO}}</td>
                                        <td style="width: 15%;padding-bottom: 30px;text-align: justify;">{{$gestion->NOMBRE_POLITICA}}</td>
                                        <td style="width: 20%;padding-bottom: 30px;text-align: justify;">{{$gestion->NOMBRE_ESTADO}}</td>
                                        <td style="width: 4%;padding-bottom: 30px;padding-left: 30px;text-align: center;">
                                            @if ($gestion->ADJUNTO)
                                            <a href="{{route('download', ['file' => str_replace('/','|',\App\Entity\Infinity\Gestion::RUTA.$gestion->ADJUNTO)])}}"><i class="fa fa-download fa-2x"></i>
                                            </a>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr>
                                        <td colspan="6">No se encontraton gestiones para el cliente</td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-5">
            <div class="x_panel" style="min-height: 400px">
                <div class="x_title">
                    <h2>Nueva Gestión</h2>
                    <ul class="nav navbar-right panel_toolbox">
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <form id="frmGestion" class="form-horizontal" enctype="multipart/form-data" method="POST" action="{{route('infinity.me.detalle.guardar.gestion')}}">

                        <input name="codUnico" type="text" value="{{$cliente->COD_UNICO}}" hidden="">

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Estado:</label>
                            <div class="input-group col-md-8 col-sm-9 col-xs-12">
                                <select class="form-control" name="estadoGestion" id="estadoGestion">
                                    <option value="">Seleccionar Estado</option>
                                    @foreach($estados as $estado)
                                    <option value="{{$estado->ID_ESTADO_GESTION}}">{{$estado->NOMBRE_ESTADO}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Fecha Gestión:</label>
                            <div class="input-group col-md-8 col-sm-9 col-xs-12">
                                <div class="input-group-addon styleAddOn"><i class="glyphicon glyphicon-calendar fa fa-calendar" for="fechaGestion"></i></div>
                                <input autocomplete="off" class="form-control dfecha" type="text" id="fechaGestion" name="fechaGestion" placeholder="Seleccionar fecha" value="">
                            </div>
                        </div>
                        <div class="form-group selec_politicas">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Politica:</label>
                            <div class="input-group col-md-8 col-sm-9 col-xs-12">
                                <div class="input-group-addon styleAddOn"><i class="glyphicon glyphicon-calendar fa fa-calendar" for="PoliticaAsociada"></i></div>
                                <select class="form-control" name="PoliticaAsociada">
                                    <option value="">Seleccionar Política</option>
                                    @foreach($combopoliticas as $combopoliticas)
                                    <option value="{{$combopoliticas->ID_POLITICA_CLIENTE}}">{{$combopoliticas->NOMBRE}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Comentario:</label>
                            <div class="input-group col-md-8 col-sm-9 col-xs-12">
                                <textarea class="form-control" rows="10" style="resize: none" placeholder="Ingresar Comentario..." name="comentarioGestion"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Adjunto:</label>
                            <div class="input-group col-md-8 col-sm-9 col-xs-12">
                                <input type="file" name="adjuntoGestion" id="adjuntoGestion" class="form-control image" style="border-style: none">
                            </div>
                        </div>
                        <center><button class="btn btn-success" type="submit">Guardar</button></center>
                    </form>
                </div>
            </div>
            <div class="x_panel" style="min-height: 250px">
                <div class="x_title">
                    <h2>Recálculo</h2>
                    <ul class="nav navbar-right panel_toolbox">
                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <form id="frmRecalculo" class="form-horizontal form-label-left" enctype="multipart/form-data" action="{{route('infinity.me.detalle.guardar.recalculo')}}" method="POST">
                        <input name="codUnico" type="text" value="{{$cliente->COD_UNICO}}" hidden="">
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">MDL </label>
                                <div class="input-group col-md-9 col-sm-9 col-xs-12">
                                    <input autocomplete="off" class="form-control" type="text" id="mdlRecalculo" name="mdlRecalculo" placeholder="Monto (USD)" value="{{$recalculo!=null?$recalculo->MDL:''}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Fecha </label>
                                <div class="input-group col-md-9 col-sm-9 col-xs-12">
                                    <div class="input-group-addon styleAddOn"><i class="glyphicon glyphicon-calendar fa fa-calendar" for="fechaRecalculo"></i></div>
                                    <input autocomplete="off" class="form-control dfecha" type="text" id="fechaRecalculo" name="fechaRecalculo" placeholder="Seleccionar fecha" value="{{$recalculo!=null?$recalculo->FECHA_RECALCULO:''}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Adjunto </label>
                                <div class="input-group col-md-9 col-sm-9 col-xs-12">
                                    <input type="file" name="adjuntoRecalculo" id="adjuntoRecalculo" class="form-control image" style="border-style: none">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <center>
                                <?php
                                echo \App\Entity\Infinity\AlertaDocumentacion::getIcono(\App\Entity\Infinity\AlertaDocumentacion::ID_RECALCULO, $cliente->FECHA_ULTIMO_RECALCULO);
                                ?>
                                <p style="font-size: 16px;">Recálculo</p>
                                @if($recalculo!=null)
                                <a href="{{route('download', ['file' => str_replace('/','|',\App\Entity\Infinity\ConocemeDocumentacion::RUTA_MDL.$recalculo->ADJUNTO)])}}"><i class="fa fa-download fa-2x"></i>
                                </a><br><br>
                                @endif
                                <button class="btn btn-success" type="submit">Recálculo</button>
                            </center>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- SEGUIMIENTOS ATRASOS -->
<div class="row">
    <div class="x_panel">
        <div class="col-sm-11 col-xs-12">
            <h2>
                <b>Políticas y Seguimiento - Atrasos</b>
                @if(!empty($maxdatoatrasos) && $maxdatoatrasos->FLG_GEST_ATRASO=='1')
                <i class="fas fa-exclamation fa-lx" style="color:#f16b6b;margin-top: 13px;margin-left: 20px"></i>
                @endif
            </h2>
        </div>
        <div class="col-sm-1 col-xs-12" align="center">
            <h1><i id="btnpoli_seguidiario_LA" class="fa fa-chevron-up" style="width:6; height:6;font-size:25px !important" aria-hidden="true"></i></h1>
        </div>
    </div>
</div>
<div class="row" id="poli_seguidiario_LA">
    <div class="x_panel">
        <div class="col-md-6">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Actividad</h2>
                    <ul class="nav navbar-right panel_toolbox"></ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content" style="max-height: 240px;">
                    <form id="frmActividad" class="form-horizontal" enctype="multipart/form-data" action="{{route('infinity.alertascartera.actividad')}}">
                        <input class="hidden" type="text" name="cod_unico" value="{{$cliente->COD_UNICO}}">
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Responsable:</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <select class="form-control" name="responsable" required {{in_array(Auth::user()->ROL,array('20','21','23','24','38','52')) ? '' : 'disabled'}}>
                                        <option value="">Seleccionar Responsable</option>
                                        <?php $cant = count($comborespon) ? "selected" : ''; ?>
                                        @foreach($comborespon as $comborespon_)
                                        <option {{$cant}}>{{$comborespon_->OPCION}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="control-label col-md-4 col-sm-3 col-xs-12" style="margin-right: 7px">Fecha:</label>
                                <div class="input-group col-md-7 col-sm-9 col-xs-12" style="margin-left: 10px">
                                    <div class="input-group-addon styleAddOn"><i class="glyphicon glyphicon-calendar fa fa-calendar" for="fechaActividad"></i></div>
                                    <input class="form-control dfecha" type="text" name="fechaActividad" placeholder="Seleccionar fecha" value="" {{in_array(Auth::user()->ROL,array('20','21','23','24','38','52')) ? '' : 'disabled'}}>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Gestión:</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <select class="form-control" name="tipo" required {{in_array(Auth::user()->ROL,array('20','21','23','24','38','52')) ? '' : 'disabled'}}>
                                        <option value="">Seleccionar Tipo</option>
                                        @foreach($comboact as $comboact)
                                        <option>{{$comboact->OPCION}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="control-label col-md-4 col-sm-3 col-xs-12">Comentario:</label>
                                <div class="col-md-8 col-sm-9 col-xs-12">
                                    <textarea class="form-control" name="comentario" required {{in_array(Auth::user()->ROL,array('20','21','23','24','38','52')) ? '' : 'disabled'}}></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Acción:</label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <select class="form-control" name="accion" required {{in_array(Auth::user()->ROL,array('20','21','23','24','38','52')) ? '' : 'disabled'}}>
                                            <option value="">Seleccionar Acción</option>
                                            @foreach($comboacc as $comboacc)
                                            <option>{{$comboacc->OPCION}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Motivo Impago:</label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <select class="form-control" name="motivoimpago" required {{in_array(Auth::user()->ROL,array('20','21','23','24','38','52')) ? '' : 'disabled'}}>
                                            <option value="">Seleccionar Motivo Impago</option>
                                            @foreach($combomotimp as $combomotimp)
                                            <option>{{$combomotimp->OPCION}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label col-md-4 col-sm-3 col-xs-12" style="margin-right: 8px">Compromiso Pago:</label>
                                    <div class="input-group col-md-7 col-sm-9 col-xs-12">
                                        <div class="input-group-addon styleAddOn"><i class="glyphicon glyphicon-calendar fa fa-calendar" for="compromisopago"></i></div>
                                        <input autocomplete="off" class="form-control dfechacp" type="text" name="compromisopago" placeholder="Esto es Fecha" value="" {{in_array(Auth::user()->ROL,array('20','21','23','24','38','52')) ? '' : 'disabled'}}>
                                    </div>
                                </div>
                                @if (in_array(Auth::user()->ROL,array('20','21','23','24','38','52')))
                                <div class="form-group">
                                    <center><button class="btn btn-primary">Guardar</button></center>
                                </div>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Actividades por Revisar</h2>
                    <ul class="nav navbar-right panel_toolbox">
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content" style="max-height: 240px; overflow-y: scroll;">
                    <table class="table table-striped jambo_table">
                        <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Tipo</th>
                                <th>Acción</th>
                                <th>Motivo</th>
                                <th>Compromiso</th>
                                <th>Cumplió</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($segmentoactividades)
                            @foreach($segmentoactividades as $segmentoactividad)
                            <tr id="{{$segmentoactividad->ID}}">
                                <td class="detalleact">{{$segmentoactividad->FECHA_ACTIVIDAD}}</td>
                                <td class="detalleact">{{$segmentoactividad->TIPO}}</td>
                                <td class="detalleact">{{$segmentoactividad->ACCION}}</td>
                                <td class="detalleact">{{$segmentoactividad->MOTIVO_IMPAGO}}</td>
                                <td class="detalleact">{{$segmentoactividad->COMPROMISO_PAGO}}</td>
                                <td class="detalleact">{{$segmentoactividad->CUMPLE_COMPROMISO}}</td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <th colspan="5">No hay Datos</th>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- SEGUIMIENTOS VIGENTES -->
<div class="row">
    <div class="x_panel">
        <div class="col-sm-11 col-xs-12">
            <h2>
                <b>Políticas y Seguimiento - Vigentes</b>
                @if(!empty($maxdatoatrasos) && $maxdatoatrasos->FLG_GEST_VIG=='1')
                <i class="fas fa-exclamation fa-lx" style="color:#f16b6b;margin-top: 13px;margin-left: 11px"></i>
                @endif
            </h2>
        </div>
        <div class="col-sm-1 col-xs-12" align="center">
            <h1><i id="btnpoli_seguivigente" class="fa fa-chevron-up" style="width:6; height:6;font-size:25px !important" aria-hidden="true"></i></h1>
        </div>
    </div>
</div>
<div class="row" id="poli_seguivigente">
    <div class="x_panel">
        <div style="margin-top: 20px;" class="col-sm-6 col-xs-12">
            <div class="x_panel" style="min-height: 435px">
                <div class="x_title">
                    <div class="col-sm-3">
                        <h2>Últimos Mensajes</h2>
                    </div>
                    <ul class="nav navbar-right panel_toolbox">
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="input-group col-md-12 col-sm-12 col-xs-12">
                    @if (!empty($gestactual) && count($gestactual)>0)
                    @if(isset($datoscaso) && !empty($datoscaso) && $datoscaso->SIG_ORDEN<6 && !empty($gestactual[0]->MENSAJE))
                        <input hidden="hidden" id="mengys" value="{{$gestactual[0]->MENSAJE}}">
                        <button class="btn btn-success" type="button" id="vergys">Ver Mensaje GYS</button>
                        @endif
                        @endif
                        <div class="x_content">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#ultimos">Ultimos</a></li>
                                @if (!empty($gestactual) && count($gestactual)>0)
                                @if(isset($datoscaso) && !empty($datoscaso) && $datoscaso->SIG_ORDEN<6) <li><a data-toggle="tab" href="#nuevos">Nuevo</a></li>
                                    @endif
                                    @endif
                            </ul>
                            <div class="tab-content">
                                <div id="ultimos" class="tab-pane in active">
                                    @if(count($gestactual)==0)
                                    <p>El cliente no tiene gestión pendiente este mes.</p>
                                    @if(Auth::user()->ROL=='38' && $clienteseguimiento->FLG_GES_GYS_DET=='1')
                                    <button class="btn btn-success center" type="button" id="agregargys">Agregar Gestión GYS</button>
                                    @endif
                                    @elseif(count($gestactual)>0 && count($ultimosmensajes)==0)
                                    <p>¡Este cliente sigue pendiente de gestión!</p>
                                    @else
                                    @if(!empty($ultimosmensajes))
                                    @foreach($ultimosmensajes as $ultimosmensaje)
                                    <br>
                                    <div class="row">
                                        <div class="ultmen btn" id="{{$ultimosmensaje->ID_CASO}}{{$ultimosmensaje->ORDEN}}" style="width: 95%;height:26px;background-color: #dddddd">
                                            <div class="col-sm-1">#{{$ultimosmensaje->ORDEN}}</div>
                                            <label class="control-label col-sm-11">{{$ultimosmensaje->TIPO=='EN' ? 'EJECUTIVO DE NEGOCIO' : $ultimosmensaje->TIPO}}</label>
                                        </div>
                                        <div class="panelultmen" id="tog{{$ultimosmensaje->ID_CASO}}{{$ultimosmensaje->ORDEN}}" style="display: none">
                                            <div class="row">
                                                <label class="control-label col-sm-3" style="margin-left: 30px">ACCIÓN REALIZADA:</label>
                                                <label class="control-label col-sm-3" style="margin-left: 30px">{{$ultimosmensaje->ACCION}}</label>
                                                @if($ultimosmensaje->TIPO=='COMITE')
                                                @if($ultimosmensaje->ESTADO=='1')
                                                <label class="control-label col-sm-3" style="margin-left: 30px"><i class='fas fa-check fa-lg' style='color:#26B99A'></i></label>
                                                @elseif($ultimosmensaje->ESTADO=='2')
                                                <label class="control-label col-sm-3" style="margin-left: 30px"><i class='fas fa-times fa-lg' style='color:#D9534F'></i></label>
                                                @endif
                                                @endif
                                            </div>

                                            <div class="row">
                                                <label class="control-label col-sm-3" style="margin-left: 30px">COMENTARIO:</label>
                                                <textarea rows="4" class="form-control col-sm-3" style="width: 200px;margin-left: 30px" disabled>{{$ultimosmensaje->MENSAJE}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                    @endif
                                    @endif
                                </div>
                                @if (!empty($gestactual) && count($gestactual)>0)
                                @if(isset($datoscaso) && !empty($datoscaso) && $datoscaso->SIG_ORDEN<6) <div id="nuevos" class="tab-pane"><br>
                                    @if( ($datoscaso->TOCA_RESPONDER=='EN' && in_array(Auth::user()->ROL,array('20','21'))) || ($datoscaso->TOCA_RESPONDER=='COMITE' && in_array(Auth::user()->ROL,array('25','30','34','35','38','42','49','52'))) )
                                    <form id="formNuevoMensaje" action="{{route('alertascartera.nuevomensaje')}}">
                                        <input hidden="hidden" name="noti_idcaso" value="{{!empty($gestactual) && count($gestactual)>0 ? $gestactual[0]->ID : 0}}">
                                        <input hidden="hidden" name="noti_codunico" value="{{$cliente->COD_UNICO}}">
                                        <input hidden="hidden" name="sigorden" value="{{$datoscaso->SIG_ORDEN}}">
                                        <input hidden="hidden" name="tocaresponder" value="{{$datoscaso->TOCA_RESPONDER}}">
                                        <div class="row">
                                            <label class="control-label col-sm-3">Acción</label>
                                            <select class="form-control col-sm-3" name="accion" style="width: 400px" required>
                                                <option value="">Seleccionar</option>
                                                <option value="LLAMADA">LLAMADA</option>
                                                <option value="VISITA">VISITA</option>
                                                <option value="OTROS">OTROS</option>
                                            </select>
                                        </div><br>
                                        <div class="row">
                                            <label class="control-label col-sm-3">Mensaje</label>
                                            <textarea class="form-control col-sm-3" name="mensaje" style="width: 400px" required></textarea>
                                        </div><br>
                                        @if(in_array(Auth::user()->ROL,array('25','30','34','35','38','42','49','52')))
                                        <div class="row" align="right">
                                            <div class="col-md-4">
                                                <input type="radio" class="aprobacion" name="aprobacion" value="1"> Aprobado
                                            </div>
                                            <div class="col-md-4">
                                                <input type="radio" class="aprobacion" name="aprobacion" value="2"> Desaprobado
                                            </div>
                                        </div><br>
                                        <div class="form-group checkboxtiempoespera" hidden="hidden">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Gestión de Espera:</label>
                                            <div class="input-group col-md-9 col-sm-9 col-xs-12">
                                                <div class="col-md-4">
                                                    <input type="radio" class="checktiempoespera" name="tiempoespera" value="3" required> 3 Meses
                                                </div>
                                                <div class="col-md-4">
                                                    <input type="radio" class="checktiempoespera" name="tiempoespera" value="6" required> 6 Meses
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                        <div class="row">
                                            <button class="btn btn-primary">Enviar</button>
                                        </div>
                                    </form>
                                    @else
                                    <p>Falta gestion del {{$datoscaso->TOCA_RESPONDER=='EN' ? 'Ejecutivo.' : 'Comité.'}}</p>
                                    @endif
                            </div>
                            @endif
                            @endif
                        </div>
                </div>
            </div>
        </div>
    </div>
    <div style="margin-top: 20px;" class="col-sm-6 col-xs-12">
        <div class="x_panel" style="min-height: 435px">
            <div class="x_title">
                <h2>Gestión Anterior</h2>
                <ul class="nav navbar-right panel_toolbox">
                </ul>
                <div class="clearfix"></div>
            </div>
            @if(!empty($gestanteriores))
            @if(count($gestanteriores)>0)
            @foreach($gestanteriores as $gestanterior)
            <button id_gest="{{$gestanterior->ID}}" class="btn" type="button" id="btnVerGestion" style="width:550px;height:40px;">{{$gestanterior->TEXTO_ANO}}<i class="fa fa-plus" aria-hidden="true"></i></button>
            @endforeach
            @endif
            <p>Este cliente no tiene gestiones anteriores</p>
            @endif
        </div>

    </div>
</div>
</div>
</div>
<div class="row">
    <div class="x_panel">
        <div class="col-sm-11 col-xs-12">
            <h2><b>Información Histórica</b></h2>
        </div>
        <div class="col-sm-1 col-xs-12" align="center">
            <h1><i id="btninfohistorica" class="fa fa-chevron-up" style="width:6; height:6;font-size:25px !important" aria-hidden="true"></i></h1>
        </div>
    </div>
</div>
<div class="row" id="infohistorica">
    <div class="x_panel">
        <div class="col-sm-12 col-xs-12">
            <div class="row">
                <div class="x_panel" style="min-height: 500px">
                    <div class="x_title">
                        <h2>Historia del Cliente</h2>
                        <ul class="nav navbar-right panel_toolbox">
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <table class="table table-striped table-bordered jambo_table" id="tblHistoricoCliente">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Tipo</th>
                                <th>Usuario</th>
                                <th>Fecha</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- ESTRATEGIA Y COMPROMISOS -->
<div class="row">
    <div class="x_panel">
        <div class="col-sm-11 col-xs-12">
            <h2><b>Estrategia y Compromisos</b></h2>
        </div>
        <div class="col-sm-1 col-xs-12" align="center">
            <h1><i id="btnEstrategia" class="fa fa-chevron-up" style="width:6; height:6;font-size:25px !important" aria-hidden="true"></i></h1>
        </div>
    </div>
</div>

<div class="row" id="infoEstrategia">
    <div class="x_panel">
        @if(!empty($datoscovid))
        <input id="IDCOVID" type="hidden" value="{{$datoscovid->ID}}">
        <div style="margin-top: 20px;" class="col-sm-7 col-xs-12">
            <div class="x_panel" style="min-height: 280px">
                <div class="x_title">
                    <h2>Estrategia Hoy</h2>
                    <ul class="nav navbar-right panel_toolbox">
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <form id="estrategia" class="x_content" action="javascript:guardarEstrategia({{$datoscovid->ID}})">
                    <div class="form-group row">
                        <div class="form-group col-sm-3">Impacto negativo del COVID 19:
                        </div>
                        <div class="form-group col-md-3">
                            @if($cambioEstrategia && (\Carbon\Carbon::today()->diffInDays(\Carbon\Carbon::parse($datoscovid->FECHA_APROBACION)))<=30) <select  {{$privilegios!=App\Model\Infinity\Visita::SOLO_LECTURA?'':'disabled'}}  name="slcQ13" id="slcQ13" class="form-control custom-select is-invalid cerrado">
                                <option {{isset($datoscovid) && $datoscovid->PREGUNTA_13 == 'sin impacto' ?'selected':''}} value="sin impacto">Sin impacto</option>
                                <option {{isset($datoscovid) && $datoscovid->PREGUNTA_13 == 'bajo' ?'selected':''}} value="bajo">Bajo</option>
                                <option {{isset($datoscovid) && $datoscovid->PREGUNTA_13 == 'medio' ?'selected':''}} value="medio">Medio</option>
                                <option {{isset($datoscovid) && $datoscovid->PREGUNTA_13 == 'alto' ?'selected':''}} value="alto">Alto</option>
                                </select>
                                @else
                                <h4 style="
                                text-align: center;
                                text-transform: uppercase;
                            ">{{isset($datoscovid) && $datoscovid->PREGUNTA_13!=''?$datoscovid->PREGUNTA_13: ''}}</h4>
                                @endif
                        </div>
                    </div>
                    <br>
                    <div class="row form-group">
                        <div class="form-group col-md-3">Plan Propuesto</div>
                        <div class="form-group col-md-3">
                            @if($cambioEstrategia  && (\Carbon\Carbon::today()->diffInDays(\Carbon\Carbon::parse($datoscovid->FECHA_APROBACION)))<=30) <select  {{$privilegios!=App\Model\Infinity\Visita::SOLO_LECTURA?'':'disabled'}} name="slcQ14_1" id="slcQ14_1" class="form-control custom-select is-invalid cerrado">
                                <option value="">-Seleccionar-</option>
                                <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_1=='Crecer'?'selected' : ''}} value="Crecer">Crecer</option>
                                <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_1=='Mantener'?'selected' : ''}} value="Mantener">Mantener</option>
                                <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_1=='Reprogramar'?'selected' : ''}} value="Reprogramar ">Reprogramar</option>
                                <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_1=='Feve'?'selected' : ''}} value="Feve">Feve</option>
                                <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_1=='Refinanciar'?'selected' : ''}} value="Refinanciar">Refinanciar</option>
                                </select>
                                @else
                                {{isset($datoscovid) && $datoscovid->PREGUNTA_14_1!=''?$datoscovid->PREGUNTA_14_1: ''}}
                                @endif
                        </div>
                    </div>
                    <div class="form-group row" id="crecerWrapper" style="{{isset($datoscovid) && $datoscovid->PREGUNTA_14_1=='Crecer'?'' :'display:none'}}">
                        <div class="form-group col-md-3">Monto</div>
                        <div class="form-group col-md-3">
                            @if($cambioEstrategia &&  (\Carbon\Carbon::today()->diffInDays(\Carbon\Carbon::parse($datoscovid->FECHA_APROBACION)))<=30) <input  {{$privilegios!=App\Model\Infinity\Visita::SOLO_LECTURA?'':'disabled'}}  {{isset($datoscovid) && $datoscovid->PREGUNTA_14_1=='Crecer'?'required' :'disabled'}} type="number" class="form-control custom-select is-invalid cerrado" min="0" max="999999" step="0.01" value="{{isset($datoscovid) && $datoscovid->PREGUNTA_14_1=='Crecer'?$datoscovid->MONTO_CRECER :''}}" name="montoCrecer" id="montoCrecer">
                                @else
                                {{isset($datoscovid) && $datoscovid->MONTO_CRECER!=''?$datoscovid->MONTO_CRECER: ''}}
                                @endif
                        </div>
                        <div class="form-group col-md-3">Detalle</div>
                        <div class="form-group col-md-3">
                            @if($cambioEstrategia  && (\Carbon\Carbon::today()->diffInDays(\Carbon\Carbon::parse($datoscovid->FECHA_APROBACION)))<=30) <input  {{$privilegios!=App\Model\Infinity\Visita::SOLO_LECTURA?'':'disabled'}} value="{{trim(isset($datoscovid) && $datoscovid->PREGUNTA_14_1=='Crecer'?$datoscovid->DETALLE_CRECER :'')}}" {{isset($datoscovid) && $datoscovid->PREGUNTA_14_1=='Crecer'?'required' :'disabled'}} type="text" class="form-control custom-select is-invalid cerrado" maxLength="30" name="detalleCrecer" id="detalleCrecer">
                                @else
                                {{isset($datoscovid) && $datoscovid->DETALLE_CRECER!=''?$datoscovid->DETALLE_CRECER: ''}}
                                @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="form-group col-md-3">FEVE</div>
                        <div class="form-group col-md-3">
                            @if($cambioEstrategia && (\Carbon\Carbon::today()->diffInDays(\Carbon\Carbon::parse($datoscovid->FECHA_APROBACION)))<=30) 
                            <select  {{$privilegios!=App\Model\Infinity\Visita::SOLO_LECTURA?'':'disabled'}}  name="slcQ14_2" id="slcQ14_2" class="form-control custom-select is-invalid cerrado">
                                <option value="">-Seleccionar-</option>
                                <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_2=='Sin FEVE'?'selected' : ''}} value="Sin FEVE">Sin FEVE</option>
                                <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_2=='Seguir COVID'?'selected' : ''}} value="Seguir COVID">Seguir COVID</option>
                                <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_2=='Seguir'?'selected' : ''}} value="Seguir">Seguir</option>
                                <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_2=='Garantizar'?'selected' : ''}} value="Garantizar">Garantizar</option>
                                <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_2=='Reducir'?'selected' : ''}} value="Reducir">Reducir</option>
                                <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_2=='Salir'?'selected' : ''}} value="Salir">Salir</option>
                                </select>
                                @else
                                {{isset($datoscovid) && $datoscovid->PREGUNTA_14_2!=''?$datoscovid->PREGUNTA_14_2: ''}}
                                @endif
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="form-group col-md-3">Clasificación</div>
                        <div class="form-group col-md-3">
                            @if($cambioEstrategia && ($now->diffInDays(\Carbon\Carbon::parse($datoscovid->FECHA_APROBACION)))<=30) 
                            <select {{$privilegios!=App\Model\Infinity\Visita::SOLO_LECTURA?'':'disabled'}} name="slcQ14_3" id="slcQ14_3" class="form-control custom-select is-invalid cerrado">
                                <option value="">-Seleccionar-</option>
                                <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_3=='Revisar'?'selected' : ''}} value="Revisar">Revisar</option>
                                <option {{isset($datoscovid) && $datoscovid->PREGUNTA_14_3=='No Revisar'?'selected' : ''}} value="No Revisar">No Revisar</option>
                                </select>
                                @else
                                {{isset($datoscovid) && $datoscovid->PREGUNTA_14_3!=''?$datoscovid->PREGUNTA_14_3: ''}}
                                @endif
                        </div>
                    </div>
                    @if($privilegios!=App\Model\Infinity\Visita::SOLO_LECTURA)
                        @if($cambioEstrategia && (\Carbon\Carbon::today()->diffInDays(\Carbon\Carbon::parse($datoscovid->FECHA_APROBACION)))<=30) 
                        <div class="row form-group">
                            <button type="submit" class="btn btn-primary btn-lg">Actualizar Estrategia</button>
                        </div>
                        @endif
                     @endif
            </form>
        </div>
    </div>
    <div style="margin-top: 20px;" class="col-sm-5 col-xs-12">
        <div class="x_panel" style="height: 341px;">
            <div class="x_title">
                <h2>Ultimos Cambios</h2>
                <!-- <i class="fa fa-refresh" aria-hidden="true" onclick="getCambios()"
                        style=" float: right;
                                cursor:pointer;
                                font-size: 19px;
                                margin-top: 5px;
                                color: #4DD094 !important;"></i> -->
                <div class="clearfix"></div>
            </div>

            <div style="
                    overflow-y: scroll;
                    overflow-x: hidden;
                    max-height: 280px;">
                <ul id="ultimoscambios" style="    margin: 0px 0 0 0;
                    width: 100%;
                    padding: 8px 0 0 0;
                    list-style: none;
                    position: relative;" class="nav navbar-right panel_toolbox cbp_tmtimeline">
                    @if(count($cambios)>0)
                    @foreach(range(0,count($cambios)-1) as $j)
                    <li>
                        <div class="cbp_tmicon">
                        </div>
                        <div style="margin: 0 0 20px 34px; cursor: default;" class="col-sm-12">
                            <p style="padding-right: 40px;">{{$cambios[$j]->MENSAJE}}</p>
                            <small>{{$cambios[$j]->NOMBRE_USUARIO}}</small>
                            <small>{{$cambios[$j]->FECHA_TEXTO}}</small>
                        </div>
                    </li>
                    @endforeach
                    @else
                    <div class="x_content">
                        <h3 style="height: 178px;
                            text-align:center;
                            display: flex;
                            align-items: center;
                            justify-content: center;">Todavia no hay ningun cambio registrado</h3>
                    </div>
                    @endif
                </ul>
            </div>
        </div>
    </div>
    <div style="margin-top: 20px;" class="col-sm-12 col-xs-12">
        <div class="x_panel" style="min-height: 280px">
            <div class="x_title">
                <h2>Mis Compromisos</h2>
                <ul class="nav navbar-right panel_toolbox">
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="form-row row" style="visibility:hidden;">
                    <input id="cumplimientosCounter" name="cumplimientosCounter" value="{{count($cumplimientos)}}" type="text">
                </div>
                <table id="cumplimientos" style="width: 100%;">
                    <tr>
                        <th style="text-align: center;"><strong>#</strong></th>
                        <th style="text-align: center;">Área de<br> eliminación </th>
                        <th style="text-align: center;">Tipo de <br> Compromiso</th>
                        <th style="text-align: center;">Detalle de <br> Compromiso</th>
                        <th style="text-align: center;">Descripción</th>
                        <th style="text-align: center;">Indicador</th>
                        <th style="text-align: center;">Fecha Fin de<br>compromiso</th>
                        <th style="text-align: center;">Gestor</th>
                        <th style="text-align: center;">Status</th>
                    </tr>
                    @if(count($cumplimientos)>0)
                    @foreach(range(0,count($cumplimientos)-1) as $i)
                    <tr>
                        <td>
                            <p id="ID_CUMPL_LABEL">{{$cumplimientos[$i]->ID}}</p>
                            <input name="{{'ID_CUMPL'.$i}}" type="hidden" value="{{$cumplimientos[$i]->ID}}">
                        </td>
                        <td>
                            {{$cumplimientos[$i]->TIPO_GESTION}}
                        </td>
                        <td>
                            {{$cumplimientos[$i]->TIPO_COMPROMISO}}
                        </td>
                        <td>
                            {{$cumplimientos[$i]->TIPO_COMPROMISO_DETALLE}}
                        </td>
                        <td>
                            {{$cumplimientos[$i]->DETALLE_COMPRA}}
                        </td>
                        <td>
                            {{$cumplimientos[$i]->INDICADOR}}
                        </td>

                        <td name="{{'FECHA_COMPROMISO'.$i}}" id="{{'FECHA_COMPROMISO'.$i}}">
                            {{ \Carbon\Carbon::parse($cumplimientos[$i]->FECHA_COMPROMISO)->format('Y-m-d')}}
                        </td>
                        <td>
                            {{$cumplimientos[$i]->GESTOR}}
                        </td>
                        <td id="{{'STATUS_COMPROMISO'.$i}}">
                            {{$cumplimientos[$i]->STATUS_COMPROMISO}}
                        </td>
                        <td>
                            <i id="{{'semaforoInfo'.$i}}" style="position: absolute;
                                    opacity: 0;
                                    height: 20px;
                                    width: 20px;
                                }" class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" title="{{App\Entity\Infinity\FichaCovid::diasVencimiento($cumplimientos[$i]->FECHA_COMPROMISO,$cumplimientos[$i]->STATUS_COMPROMISO)}}">
                            </i>
                            <svg class="bd-placeholder-img rounded mr-2" width="20" height="20" role="img">
                                <rect id="{{'RECT'.$i}}" fill="{{App\Entity\Infinity\FichaCovid::semaforoVencimiento($cumplimientos[$i]->FECHA_COMPROMISO,$cumplimientos[$i]->STATUS_COMPROMISO)}}" width="100%" height="100%"></rect>
                            </svg>
                        </td>
                        <td>
                                    @if($cumplimientos[$i]->GESTOR=='EJECUTIVO DE NEGOCIO' && count($revision) > 0)
                                        <div style="cursor:pointer" onclick="gestionCumplimiento({{$cumplimientos[$i]->ID}})">Gestionar</div>
                                    @elseif($cumplimientos[$i]->GESTOR=='JEFE ZONAL' && count($revision) > 0)
                                        @if(!(in_array($rol,array(
                                        App\Entity\Usuario::ROL_EJECUTIVO_FARMER_BE,
                                        App\Entity\Usuario::ROL_EJECUTIVO_HUNTER_BE,
                                        App\Entity\Usuario::ROL_ANALISTA_NEGOCIO_ZONAL_BE))))
                                        <div style="cursor:pointer" onclick="gestionCumplimiento({{$cumplimientos[$i]->ID}})">Gestionar</div>
                                        @endif
                                    @else
                                        @if((in_array('Seguimiento comercial',$revision)))
                                        <div style="cursor:pointer" onclick="gestionCumplimiento({{$cumplimientos[$i]->ID}})">Gestionar</div>
                                        @endif
                                    @endif
                        </td>
                    </tr>
                    @endforeach
                    @endif
                </table>
                <div class="d-flex flex-row-reverse" style="padding-top: 20px;">
                    <button onclick="addCompromiso(event)" class="btn btn-primary btn-lg">+ Añadir Nuevo Compromiso</button>
                </div>
        </div>
    </div>
</div>
    @else
    <h3 style="padding: 30px;
        line-height: 40px;
        text-align: center;">Pendiente registro de
        <a style="font-weight: 700;" target="_blank" href="{{route('infinity.me.cliente.visita')}}?cu={{$cliente->COD_UNICO}}">
            ficha
        </a>
        <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
    </h3>
    @endif
</div>
</div>
<!-- /.Modal Ver Mensaje GyS-->
<div class="modal fade" tabindex="-1" role="dialog" id="modalmensajegys">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <form id="formVerMensajeGys" action="#">
                    <div class="form-group">
                        <label class="control-label col-md-3"> COMENTARIO :</label>
                        <textarea class="form-control" id="data_comentario_gesgys" disabled></textarea>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- /.Modal Agregar Gestión por GYS-->
<div class="modal fade" tabindex="-1" role="dialog" id="modalGesgys">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Agregar Gestión</h4>
            </div>
            <div class="modal-body">
                <form id="formGestionbyGys" action="{{route('alertascartera.agregargestiongys')}}">
                    <input type="text" hidden="hidden" name="codunico_gesgys" value="{{isset($cliente)?$cliente->COD_UNICO:null}}">
                    <div class="form-group">
                        <label class="control-label col-md-3"> COMENTARIO :</label>
                        <textarea class="form-control" name="comentario_gesgys" required></textarea>
                    </div>
                    <center>
                        <button class="btn btn-success" type="submit">Agregar</button>
                    </center>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- /.Modal Ver Gestión Vigentes-->
<div class="modal fade" tabindex="-1" role="dialog" id="modalVerGestion">
    <div class="modal-dialog modal-lg" role="document" style="width: 80%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Primer Mensaje</h4>
            </div>
            <div class="modal-body">
                <form id="formGestionAnterior" action="#">
                    <div class="form-group">
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class="control-label col-md-3"> EN </label>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="control-label col-md-3"> COMITE </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class="col-sm-3">Fecha</label>
                                <label class="col-sm-3" id="fechaen1"></label>
                                <label class="col-sm-3">Accion</label>
                                <label class="col-sm-3" id="accionen1"></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label>Comentario 1</label>
                                <textarea style="width: 450px" id="comen1" disabled></textarea>
                            </div>
                            <div class="form-group col-sm-6">
                                <label>Respuesta 1</label>
                                <textarea style="width: 450px" id="resen1" disabled></textarea>
                                <span id="en1"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class="col-sm-3">Fecha</label>
                                <label class="col-sm-3" id="fechaen2"></label>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-3">Accion</label>
                                <label class="col-sm-3" id="accionen2"></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label>Comentario 2</label>
                                <textarea style="width: 450px" id="comen2" disabled></textarea>
                            </div>
                            <div class="form-group col-sm-6">
                                <label>Respuesta 2</label>
                                <textarea style="width: 450px" id="resen2" disabled></textarea>
                                <span id="en2"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class="col-sm-3">Fecha</label>
                                <label class="col-sm-3" id="fechaen3"></label>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-3">Accion</label>
                                <label class="col-sm-3" id="accionen3"></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label>Comentario 3</label>
                                <textarea style="width: 450px" id="comen3" disabled></textarea>
                            </div>
                            <div class="form-group col-sm-6">
                                <label>Respuesta 3</label>
                                <textarea style="width: 450px" id="resen3" disabled></textarea>
                                <span id="en3"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- /.Modal Documentación-->
<div class="modal fade" tabindex="-1" role="dialog" id="modalGuardar">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <h3 style="text-align:center" id="messageModal"></h3>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="modalCumplimiento">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="padding: 10px;">
            <div class="modal-header">
                <h4 class="modal-title">Añade tu Compromiso</h4>
            </div>
            <form action="javascript:addCumplimientoPOST()">
                <div id="nuevoCompromiso" class="modal-body">
                    <div style="padding: 10px;" class="row">
                        <div class="col-md-4">Área de Eliminación</div>
                        <div class="col-md-8">
                            <select name="TIPO_GESTION" id="TIPO_GESTION" class="form-control custom-select is-invalid cerrado" required>
                                <option value="">-Seleccionar-</option>
                                @if(count($revision)>0)
                                @foreach(range(0,count($revision)-1) as $j)
                                <option value="{{$revision[$j]}}">{{$revision[$j]}}</option>
                                @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div style="padding: 10px;" class="row">
                        <div class="col-md-4">Tipo de Compromiso</div>
                        <div class="col-md-8">
                            <select onchange="show('DIVCUMPLIMIENTO',this)" name="TIPO_COMPROMISO" id="TIPO_COMPROMISO" class="form-control custom-select is-invalid cerrado" required>
                                <option value="">-Seleccionar-</option>
                                <option value="Comerciales">Comerciales</option>
                                <option value="Canalización de flujos IN">Canalización de flujos IN</option>
                                <option value="Incremento de flujos OUT">Incremento de flujos OUT</option>
                                <option value="Garantías">Garantías</option>
                                <option value="Estados Financieros">Estados Financieros</option>
                                <option value="Otros">Otros</option>
                            </select>
                        </div>
                    </div>
                    <div style="padding: 10px;" class="row">
                        <div class="col-md-4">Detalle de Compromiso</div>
                        <div class="col-md-8">
                            <select required identificador="DIVCUMPLIMIENTO_Comerciales" name="TIPO_COMPROMISO_DETALLE_Comerciales" id="TIPO_COMPROMISO_DETALLE_Comerciales" class="form-control custom-select is-invalid cerrado DIVCUMPLIMIENTO">
                                <option value="">-Seleccionar-</option>
                                <option value="Compra de un activo/inversión">Compra de un activo/inversión</option>
                                <option value="Tipo de cambio">Tipo de cambio</option>
                                <option value="Incremento la utilización de líneas">Incremento la utilización de líneas</option>
                                <option value="Incrementar nivel de exposición PDM">Incrementar nivel de exposición PDM</option>
                                <option value="Covenants">Covenants</option>
                                <option value="Digitalización de productos">Digitalización de productos</option>
                                <option value="Tarifas especiales">Tarifas especiales</option>
                                <option value="Entrega de documentos - renovación de líneas">Entrega de documentos - renovación de líneas</option>
                            </select>
                            <select style="display:none" identificador="DIVCUMPLIMIENTO_Canalización de flujos IN" name="TIPO_COMPROMISO_DETALLE_Canalización_de_flujos_IN" id="TIPO_COMPROMISO_DETALLE_Canalización_de_flujos_IN" class="form-control custom-select is-invalid cerrado DIVCUMPLIMIENTO">
                                <option value="">-Seleccionar-</option>
                                <option value="Proveedores">Proveedores</option>
                                <option value="Transferencias">Transferencias</option>
                                <option value="Letras/Facturas">Letras/Facturas</option>
                                <option value="Recaudación">Recaudación</option>
                                <option value="Otros">Otros</option>
                            </select>
                            <select style="display:none" identificador="DIVCUMPLIMIENTO_Incremento de flujos OUT" name="TIPO_COMPROMISO_DETALLE_Canalización_de_flujos_OUT" id="TIPO_COMPROMISO_DETALLE_Canalización_de_flujos_OUT" class="form-control custom-select is-invalid cerrado DIVCUMPLIMIENTO">
                                <option value="">-Seleccionar-</option>
                                <option value="Transferencias al exterior">Transferencias al exterior</option>
                                <option value="Pago de proveedores">Pago de proveedores</option>
                                <option value="Pago SUNAT">Pago SUNAT</option>
                                <option value="Pago AFP">Pago AFP</option>
                                <option value="Proveedores">Proveedores</option>
                                <option value="Planilla">Planilla</option>
                                <option value="Pago varios">Pago varios</option>
                            </select>
                            <select style="display:none" identificador="DIVCUMPLIMIENTO_Garantías" class="form-control custom-select is-invalid cerrado DIVCUMPLIMIENTO" name="TIPO_COMPROMISO_DETALLE_Garantías" id="TIPO_COMPROMISO_DETALLE_Garantías">
                                <option value="">-Seleccionar-</option>
                                <option value="Nueva garantía bien mueble/ inmueble">Nueva garantía bien mueble/ inmueble</option>
                                <option value="Garantía liquida">Garantía liquida</option>
                                <option value="Incremento del valor de gravamen">Incremento del valor de gravamen</option>
                                <option value="Actualizar tasación">Actualizar tasación</option>
                                <option value="Cesión de flujos">Cesión de flujos</option>
                                <option value="Fideicomisos">Fideicomisos</option>
                                <option value="FEVE">FEVE</option>
                            </select>
                            <select style="display:none" identificador="DIVCUMPLIMIENTO_Estados Financieros" name="TIPO_COMPROMISO_DETALLE_Estados_Financieros" id="TIPO_COMPROMISO_DETALLE_Estados_Financieros" class="form-control custom-select is-invalid cerrado DIVCUMPLIMIENTO">
                                <option value="">-Seleccionar-</option>
                                <option value="Actividad">Actividad</option>
                                <option value="Liquidez">Liquidez</option>
                                <option value="Solvencia">Solvencia</option>
                                <option value="Cobertura del servicio de deuda">Cobertura del servicio de deuda</option>
                                <option value="Endeudamiento">Endeudamiento</option>
                                <option value="Rentabilidad">Rentabilidad</option>
                            </select>
                            <select style="display:none" identificador="DIVCUMPLIMIENTO_Otros" name="TIPO_COMPROMISO_DETALLE_Otros" id="TIPO_COMPROMISO_DETALLE_Otros" class="form-control custom-select is-invalid cerrado DIVCUMPLIMIENTO">
                                <option value="">-Seleccionar-</option>
                                <option value="Sustento de deuda en otros bancos">Sustento de deuda en otros bancos</option>
                                <option value="Incremento del Capital social">Incremento del Capital social</option>
                                <option value="Acuerdos Comite">Acuerdos Comité</option>
                                <option value="Otros">Otros</option>
                            </select>
                        </div>
                    </div>
                    <div style="padding: 10px;" class="row">
                        <div class="col-md-4">Descripcion</div>
                        <div class="col-md-8">
                            <input maxlength="200" name="DETALLE_COMPRA" id="DETALLE_COMPRA" type="text" class="form-control cerrado" required>
                        </div>
                    </div>
                    <div style="padding: 10px;" class="row">
                        <div class="col-md-4">Indicador</div>
                        <div class="col-md-8">
                            <input step="0.01" name="INDICADOR" id="INDICADOR" oninput="maxLengthMontoCheck(this)" max="999999999" type="number" class="form-control cerrado" min="0" required>
                        </div>
                    </div>
                    <div style="padding: 10px;" class="row">
                        <div class="col-md-4">Fecha de Fin de Compromiso</div>
                        <div class="col-md-8">
                            <input autocomplete="off" class="form-control fechaCompromiso cerrado" type="text" name="FECHA_COMPROMISO" id="FECHA_COMPROMISO" placeholder="Seleccionar fecha" value="" required>
                        </div>
                    </div>
                    <div style="padding: 10px;" class="row">
                        <div class="col-md-4">Gestor</div>
                        <div class="col-md-8">
                            <select name="GESTOR" id="GESTOR" class="form-control custom-select is-invalid cerrado" required>
                                <option value="">-Seleccionar-</option>
                                <option value="EJECUTIVO DE NEGOCIO">EJECUTIVO DE NEGOCIO</option>
                                <option value="JEFE ZONAL">JEFE ZONAL</option>
                                <option value="EQUIPO SEGUIMIENTO">EQUIPO SEGUIMIENTO</option>
                            </select>
                        </div>
                    </div>
                    <div style="display: flex;
                        justify-content: center;
                        align-items: center;
                        padding-top: 25px;" class="row">
                        <button id="btnAdd" type="submit" class="btn btn-primary btn-lg btn-success">Añadir</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="modalGestionar">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="padding: 10px;">
            <div class="modal-header">
                <h4 class="modal-title">Gestiona tu Compromiso</h4>
            </div>
            <form action="javascript:gestionCumplimientoPOST()">
                <input type="hidden" name="IDCUMPL" id="IDCUMPL">
                <div class="modal-body">
                    <div class="row" id="rowFechaCompromiso">
                        <div class="col-md-4">Fecha de Fin de Compromiso</div>
                        <div class="col-md-8">
                            <input  autocomplete="off" class="form-control fechaCompromiso cerrado" type="text" name="fechaCompromisoGestion" id="fechaCompromisoGestion" placeholder="Seleccionar fecha" value="" required>
                        </div>
                    </div>
                    <div style="padding: 10px;" class="row">
                        <div class="col-md-4">Status</div>
                        <div class="col-md-8">
                            <select name="status" id="status" class="status form-control custom-select is-invalid">
                                <option selected value="">-Seleccionar-</option>
                                <option value="ELIMINADO">ELIMINADO</option>
                                <option value="CUMPLIO">CUMPLIÓ</option>
                                <option value="NO CUMPLIO">NO CUMPLIÓ</option>
                            </select>
                        </div>
                    </div>
                    <div style="display: flex;
                        justify-content: center;
                        align-items: center;
                        padding-top: 25px;" class="row">
                        <button id="btnAct" type="submit" class="btn btn-primary btn-lg btn-success">Actualizar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="modalDocumentacion">
    <div class="modal-dialog" role="document">
        <div class="modal-content
        ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Gestión de Documentación</h4>
            </div>
            <div class="modal-body">
                <form id="frmDocumentacion" class="form-horizontal form-label-left" enctype="multipart/form-data" action="{{route('infinity.me.detalle.guardar.documentacion')}}" method="POST">

                    <input name="codUnico" type="text" value="{{isset($cliente)?$cliente->COD_UNICO:null}}" hidden="">

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Tipo Documento:</label>
                        <div class="input-group col-md-9 col-sm-9 col-xs-12">
                            <select class="form-control" name="tipoDocumento" id="cboTipoDocumento">
                                <option value="">Seleccionar Tipo</option>
                                <!--DEFINIR-->
                                <option value="1">DDJJ</option>
                                <option value="2">EEFF</option>
                                <option value="3">IBR</option>
                                <option value="4">F02</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group" id="fechaFirmaOcultar" hidden>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Fecha Firma Documento:</label>
                        <div class="input-group col-md-9 col-sm-9 col-xs-12">
                            <div class="input-group-addon styleAddOn"><i class="glyphicon glyphicon-calendar fa fa-calendar" for="fechaFirma"></i></div>
                            <input autocomplete="off" class="form-control dfecha" type="text" id="fechaFirma" name="fechaFirma" placeholder="Seleccionar fecha" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Adjunto:</label>
                        <div class="input-group col-md-9 col-sm-9 col-xs-12">
                            <input type="file" name="adjuntoDocumento" id="adjuntoDocumento" class="form-control image" style="border-style: none">
                        </div>
                    </div>

                    <center>
                        <button class="btn btn-success" type="submit">Agregar</button>
                    </center>
                </form>

                <div id="tablaDocumentacion" style="overflow-y: scroll;max-height: 500px">
                    <table class="table table-striped jambo_table">
                        <thead>
                            <tr class="headings">
                                <th>Documento</th>
                                <th>Fecha Firma</th>
                                <th>Fecha Registro</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(isset($documentos))
                            @if(count($documentos)>0)
                            @foreach($documentos as $documento)
                            <tr>
                                <td>{{$documento->NOMBRE_DOCUMENTO}}</td>
                                <td>{{$documento->FECHA_FIRMA}}</td>
                                <td>{{$documento->FECHA_REGISTRO}}</td>
                                <td><a href="{{route('download', ['file' => str_replace('/','|',\App\Entity\Infinity\ConocemeDocumentacion::RUTA_DOCS.$documento->ADJUNTO)])}}">
                                        <i class="fa fa-download"></i></a></td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="3">No se encontraron resultados</td>
                            </tr>
                            @endif
                            @endif

                        </tbody>
                    </table>
                </div>
            </div>
            <!--<div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>-->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- /.Modal Detalle de Actividad Atrasos-->
<div class="modal fade" tabindex="-1" role="dialog" id="modalDetalleActividad">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Detalle Actividad</h4>
            </div>
            <div class="modal-body">
                <form id="formDetalleActividad" action="{{route('infinity.alertascartera.actualizaractividadcompromiso')}}">
                    <input class="form-control hidden" type="text" id="d_id" name="d_id">
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">FECHA ACTIVIDAD:</label>
                            <div class="input-group col-md-9 col-sm-9 col-xs-12">
                                <input type="text" readonly="" id="d_fecha" class="form-control">
                            </div>
                        </div>
                        <div class="form-group col-sm-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">TIPO:</label>
                            <div class="input-group col-md-9 col-sm-9 col-xs-12">
                                <input type="text" readonly="" id="d_tipo" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">ACCION:</label>
                            <div class="input-group col-md-9 col-sm-9 col-xs-12">
                                <input type="text" readonly="" id="d_accion" class="form-control">
                            </div>
                        </div>
                        <div class="form-group col-sm-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">MOTIVO:</label>
                            <div class="input-group col-md-9 col-sm-9 col-xs-12">
                                <input type="text" readonly="" id="d_motivo" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">COMPROMISO:</label>
                            <div class="input-group col-md-9 col-sm-9 col-xs-12">
                                <input type="text" readonly="" id="d_compromiso" class="form-control">
                            </div>
                        </div>
                        <div class="form-group col-sm-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">REPONSABLE:</label>
                            <div class="input-group col-md-9 col-sm-9 col-xs-12">
                                <input type="text" readonly="" id="d_responsable" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">DEUDA TOTAL:</label>
                            <div class="input-group col-md-9 col-sm-9 col-xs-12">
                                <input type="text" readonly="" id="d_deudatotal" class="form-control">
                            </div>
                        </div>
                        <div class="form-group col-sm-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">DEUDA ATRASADA:</label>
                            <div class="input-group col-md-9 col-sm-9 col-xs-12">
                                <input type="text" readonly="" id="d_deudaatrasada" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">DEUDA VENCIDA:</label>
                            <div class="input-group col-md-9 col-sm-9 col-xs-12">
                                <input type="text" readonly="" id="d_deudavencida" class="form-control">
                            </div>
                        </div>
                        <div class="form-group col-sm-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">DIAS:</label>
                            <div class="input-group col-md-9 col-sm-9 col-xs-12">
                                <input type="text" readonly="" id="d_dias" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">% DEUDA ATRASO + VENCIDA:</label>
                            <div class="input-group col-md-9 col-sm-9 col-xs-12">
                                <input type="text" readonly="" id="d_pctdeudaproblema" class="form-control">
                            </div>
                        </div>
                        <div class="form-group col-sm-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">COMENTARIO:</label>
                            <div class="input-group col-md-9 col-sm-9 col-xs-12">
                                <textarea readonly="" id="d_comentario" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        @if (in_array(Auth::user()->ROL,array('38','52')))
                        <div class="form-group col-sm-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">CUMPLIO COMPROMISO:</label>
                            <div class="input-group col-md-9 col-sm-9 col-xs-12">
                                <select class="form-control" name="d_cumpliocompromiso" id="d_cumpliocompromiso" required>
                                    <option value="">Seleccionar Tipo</option>
                                    @foreach($combocumpliorespon as $combocumpliorespon)
                                    <option>{{$combocumpliorespon->OPCION}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-sm-6">
                            <button class="btn btn-primary">Guardar</button>
                        </div>
                        @endif
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<select style="visibility:hidden" type="hidden" name="optionHolder" id="optionHolder"></select>
<!-- /.Modal Grupo Económico-->
<div class="modal fade" tabindex="-1" role="dialog" id="modalGrupoEconomico">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Grupo Económico</h4>
            </div>
            <div class="modal-body" style="min-height: 420px;max-height: 420px">
                @if(isset($grupoEconomico))
                @if($grupoEconomico!=NULL)
                <div class="col-sm-12" id="infoResumen" style="padding-bottom:20px">
                    <label style="text-decoration: underline;">Resumen</label><br>
                    <div class="col-sm-4 col-xs-12">
                        <?php
                        echo \App\Entity\Flag::getIconoFlag($grupoEconomico['FLG_CLASIFICACION']);
                        ?> Clasificación
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <?php
                        echo \App\Entity\Flag::getIconoFlag($grupoEconomico['FLG_VENCIDO']);
                        ?> Vencido
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <?php
                        echo \App\Entity\Flag::getIconoFlag($grupoEconomico['FLG_FEVE']);
                        ?> FEVE DURO
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <?php
                        echo \App\Entity\Flag::getIconoFlag($grupoEconomico['FLG_REFINANCIADO']);
                        ?> Refinanciado
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <?php
                        echo \App\Entity\Flag::getIconoFlag($grupoEconomico['FLG_REESTRUCTURADO']);
                        ?> Reestructurado
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <?php
                        echo \App\Entity\Flag::getIconoFlag($grupoEconomico['FLG_JUDICIAL']);
                        ?> Judicial
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <?php
                        echo \App\Entity\Flag::getIconoFlag($grupoEconomico['FLG_COBRANZA']);
                        ?> Cobranza
                    </div>
                </div>

                <div class="col-sm-12">
                    @if(count($grupoEconomico['RELACIONADOS'])>0)
                    <label style="text-decoration: underline;">Relacionados</label><br>
                    <div id="infoRelacionados" style="overflow-y: scroll;max-height: 250px">
                        @foreach($grupoEconomico['RELACIONADOS'] as $relacionado)
                        <div class="col-sm-12" style="padding-bottom:20px">
                            <label style="text-decoration: underline;">{{$relacionado->NOMBRE_REL}} ({{$relacionado->COD_UNICO_REL}})</label><br>
                            <div class="col-sm-4 col-xs-12">
                                <?php
                                echo \App\Entity\Flag::getIconoFlag($relacionado->FLG_CLASIFICACION);
                                ?> Clasificación
                            </div>
                            <div class="col-sm-4 col-xs-12">
                                Vencido: S/. {{number_format($relacionado->MONTO_VENCIDO,0,'.',',')}}
                            </div>
                            <div class="col-sm-4 col-xs-12">
                                <?php
                                echo \App\Entity\Flag::getIconoFlag($relacionado->FLG_FEVE);
                                ?> FEVE DURO
                            </div>
                            <div class="col-sm-4 col-xs-12">
                                Refinanciado: S/. {{number_format($relacionado->MONTO_REFINANCIADO,0,'.',',')}}
                            </div>
                            <div class="col-sm-4 col-xs-12">
                                Reestructurado: S/. {{number_format($relacionado->MONTO_REESTRUCTURADO,0,'.',',')}}
                            </div>
                            <div class="col-sm-4 col-xs-12">
                                Judicial: S/. {{number_format($relacionado->MONTO_JUDICIAL,0,'.',',')}}
                            </div>
                            <div class="col-sm-4 col-xs-12">
                                Coactiva: S/. {{number_format($relacionado->MONTO_COACTIVA,0,'.',',')}}
                            </div>
                            <div class="col-sm-4 col-xs-12">
                                Laboral: S/. {{number_format($relacionado->MONTO_LABORAL,0,'.',',')}}
                            </div>
                        </div>
                        @endforeach
                    </div>
                    @endif
                </div>
                @endif
                @endif
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@stop
