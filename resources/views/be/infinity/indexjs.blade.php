@extends('Layouts.layout')

@section('js-libs')

<link href="{{ URL::asset('css/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ URL::asset('css/datatables.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ URL::asset('css/formValidation.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ URL::asset('css/multiselect.min.css') }}" rel="stylesheet" type="text/css">

<script type="text/javascript" src="{{ URL::asset('js/jszip.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/pdfmake.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/vfs_fonts.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/buttons.flash.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/buttons.print.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/buttons.html5.min.js') }}"></script>

<script type="text/javascript" src="{{ URL::asset('js/numeral.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/chart.bundle.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/utils.js') }}"></script>

<script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.es.min.js') }}"></script>

<script type="text/javascript" src="{{ URL::asset('js/formvalidation/formValidation.popular.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/formvalidation/language/es_CL.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/formvalidation/framework/bootstrap.min.js') }}"></script>

<script type="text/javascript" src="{{ URL::asset('js/multiselect.min.js') }}"></script>
<!--  ESTILOS DE ESTA VISTA -->
<link href="{{ URL::asset('css/alertascartera/alertas_main.css') }}" rel="stylesheet" type="text/css"></link>
@stop

@section('content')
	@yield('vista_ac')
@stop

@section('js-scripts')
<script>
	var onlyOnce = true;
	function call_tablaCompromisos(params={
		banca:null,
		zonal:null,
		jefe:null,
		ejecutivo:null
		}){
		banca = params.banca==null?$("#Bancas").val():null;
		zonal = params.zonal==null?$("#Zonales").val():null;
		jefe = params.jefe==null?$("#Jefaturas").val():null;
		ejecutivo = params.ejecutivo==null?$("#Ejecutivos").val():null;
		tablaCompromisos(banca, zonal, jefe, ejecutivo);
	}
	function call_tablaSeguimiento(params={
		banca:null,
		zonal:null,
		jefe:null,
		ejecutivo:null,
		es_infinity:null,
		rango_atraso:null,
		frecuencia_atraso_tot:null,
		leasings:null,
		rango_deuda:null,
		semaforo_m0:null,
		feve:null,
		gestion:null,
		frecuencia_atraso_sin_dscto:null,
		gesgys:null,
		estado_aprobacion:null
		}){
		banca = params.banca==null?$("#Bancas").val():null;
		zonal = params.zonal==null?$("#Zonales").val():null;
		jefe = params.jefe==null?$("#Jefaturas").val():null;
		ejecutivo = params.ejecutivo==null?$("#Ejecutivos").val():null;
		es_infinity = params.es_infinity==null?$("#es_infinity").val():null;
		rango_atraso = params.rango_atraso==null?$("#rango_atraso").val():null;
		frecuencia_atraso_tot = params.frecuencia_atraso_tot==null?$("#frecuencia_atraso_tot").val():null;
		leasings = params.leasings==null?$("#leasings").val():null;
		rango_deuda = params.rango_deuda==null?$("#rango_deuda").val():null;
		semaforo_m0 = params.semaforo_m0==null?$("#semaforo_m0").val():null;
		feve = params.feve==null?$("#feve").val():null;
		gestion = params.gestion==null?$("#gestion").val():null;
		frecuencia_atraso_sin_dscto = params.frecuencia_atraso_sin_dscto==null?$("#frecuencia_atraso_sin_dscto").val():null;
		gesgys = params.gesgys==null? ((rol=='38' && $("#gesgys").val()) ? $("#gesgys").val() : null):null;
		estado_aprobacion = params.estado_aprobacion==null?$("#estado_aprobacion").val():null;
		tablaseguimiento(banca, zonal, jefe, ejecutivo, es_infinity, rango_atraso, frecuencia_atraso_tot, leasings, rango_deuda ,semaforo_m0,feve, gestion,frecuencia_atraso_sin_dscto, gesgys,estado_aprobacion);
	}
	function call_chartSeguimiento(params={
		banca:null,
		zonal:null,
		jefe:null,
		ejecutivo:null,
		es_infinity:null,
		rango_atraso:null,
		frecuencia_atraso_tot:null,
		leasings:null,
		rango_deuda:null,
		semaforo_m0:null,
		feve:null,
		gestion:null,
		frecuencia_atraso_sin_dscto:null,
		gesgys:null,
		estado_aprobacion:null
		}){
		banca = params.banca==null?$("#Bancas").val():null;
		zonal = params.zonal==null?$("#Zonales").val():null;
		jefe = params.jefe==null?$("#Jefaturas").val():null;
		ejecutivo = params.ejecutivo==null?$("#Ejecutivos").val():null;
		es_infinity = params.es_infinity==null?$("#es_infinity").val():null;
		rango_atraso = params.rango_atraso==null?$("#rango_atraso").val():null;
		frecuencia_atraso_tot = params.frecuencia_atraso_tot==null?$("#frecuencia_atraso_tot").val():null;
		leasings = params.leasings==null?$("#leasings").val():null;
		rango_deuda = params.rango_deuda==null?$("#rango_deuda").val():null;
		semaforo_m0 = params.semaforo_m0==null?$("#semaforo_m0").val():null;
		feve = params.feve==null?$("#feve").val():null;
		gestion = params.gestion==null?$("#gestion").val():null;
		frecuencia_atraso_sin_dscto = params.frecuencia_atraso_sin_dscto==null?$("#frecuencia_atraso_sin_dscto").val():null;
		gesgys = params.gesgys==null? ((rol=='38' && $("#gesgys").val()) ? $("#gesgys").val() : null):null;
		chartSeguimiento(banca, zonal, jefe, ejecutivo, es_infinity, rango_atraso, frecuencia_atraso_tot, leasings, rango_deuda ,semaforo_m0,feve, gestion,frecuencia_atraso_sin_dscto, gesgys);
	}
	function getFiltroMasMenos(){
		var x = document.getElementsByClassName("selected");
		var filtro = [];
		for (let index = 0; index < x.length; index++) {
			switch (x[index].id) {
				case "more":
					filtro.push('Más de 50M');
				break;
				case "less":
					filtro.push('Menos de 50M');
				break;
				case "todo":
					filtro.push(null);
				break;
			}
		}
		return filtro;
	}
	$(document).on("click","#btnAbrirModalMeses",function() {
		$("#modalMeses").modal();
	});

	$(document).on("change","#combomes",function() {
		//console.log($(this).val());
		if ($(this).val()=="") {
			$(".mostrardescarga").html("<span align='center'>Seleccionar Fecha</span>");
		}else{
			$(".mostrardescarga").html("<center><a id='redireccion' class='btn btn-success' target='_blank' href='{{route('infinity.alertascartera.pdfseguimiento')}}?mes="+$(this).val()+"'>Exportar Ult. Comite</a></center>");
		}
	});

	$(document).on("click","#gestionVigClick",function() {
		if ($("#infinity").hasClass('selected') && $("#NoInfinity").hasClass('unselected')) {
			$('.nav-tabs a[href="#laumaticas"]').tab('show');
		}
		if ($("#NoInfinity").hasClass('selected') && $("#infinity").hasClass('unselected')) {
			$("#es_infinity").selectpicker('val', '0');
			$("#rango_atraso").selectpicker('val', 'Sin Atrasos');
			$('#frecuencia_atraso_tot').selectpicker('val', '');
			$('#leasings').selectpicker('val', '');
			$('#rango_deuda').selectpicker('val', '');
			$("#semaforo_m0").selectpicker('val', ['3','99']);
			$('#feve').selectpicker('val', '');
			$('#gestion').selectpicker('val', '');
			$('#frecuencia_atraso_sin_dscto').selectpicker('val', '');
			if (rol=='38') {
				$('#gesgys').selectpicker('val', '');
			}

			$('.nav-tabs a[href="#segdiario"]').tab('show');
			$('.nav-tabs a[href="#tablaseg"]').tab('show');
			call_tablaSeguimiento();
			call_tablaCompromisos();
		}
	});
	$(document).on("click","#resumenTab",function() {
		$('#es_infinity').selectpicker('val', '');
		$('#rango_atraso').selectpicker('val', '');
		$('#frecuencia_atraso_tot').selectpicker('val', '');
		$('#leasings').selectpicker('val', '');
		$('#rango_deuda').selectpicker('val', '');
		$('#semaforo_m0').selectpicker('val', '');
		$('#feve').selectpicker('val', '');
		$('#gestion').selectpicker('val', '');
		$('#frecuencia_atraso_sin_dscto').selectpicker('val', '');
		if (rol=='38') {
				$('#gesgys').selectpicker('val', '');
		}
	});

	$(document).on('click', "#infinity", function() {
		if ($("#infinity").hasClass('selected')) {
		} else {
			$("#infinity").removeClass('unselected');
			$("#infinity").addClass('selected');
			$("#NoInfinity").addClass('unselected');
			$("#NoInfinity").removeClass('selected');
		}
		
		resumenVigente($("#Bancas").val(), $("#Zonales").val(), $("#Jefaturas").val(), $("#Ejecutivos").val());
	});

	$(document).on('click', "#NoInfinity", function() {
		if ($("#NoInfinity").hasClass('selected')) {
		} else {
			$("#NoInfinity").removeClass('unselected');
			$("#NoInfinity").addClass('selected');
			$("#infinity").addClass('unselected');
			$("#infinity").removeClass('selected');
		}
		resumenVigente($("#Bancas").val(), $("#Zonales").val(), $("#Jefaturas").val(), $("#Ejecutivos").val());
	});

	function iniciargrafico(constante,chartnum) {
		var chartnum= new Chart(constante,{
			type: 'pie',
			data: {
				datasets: [{
					data: [100],
					backgroundColor: [
						'whitesmoke'
					],
					borderWidth: 0.6
				}],
				labels: [
					'Nota',
					'',
				]
			},

			options: {
				legend: {
					display: false
				},
				cutoutPercentage: 70,
				responsive: false,
				mantainRatio: true,
				tooltips: {
					enabled: false
				},
			}
		});
	}

	const ctx1 = document.getElementById("unoSiete").getContext('2d');
	iniciargrafico(ctx1,"Chart1");
	const ctx2 = document.getElementById("ochoCatorce").getContext('2d');
	iniciargrafico(ctx2,"Chart2");
	const ctx3 = document.getElementById("quince+").getContext('2d');
	iniciargrafico(ctx3,"Chart3");
	const ctx4 = document.getElementById("gestionVig").getContext('2d');
	iniciargrafico(ctx4,"Chart4");
	const ctx5 = document.getElementById("unoSiete_2").getContext('2d');
	iniciargrafico(ctx5,"Chart5");
	const ctx6 = document.getElementById("ochoCatorce_2").getContext('2d');
	iniciargrafico(ctx6,"Chart6");
	const ctx7 = document.getElementById("quince+_2").getContext('2d');
	iniciargrafico(ctx7,"Chart7");
	Chart.defaults.global.legend.display = false;

	$(document).on('change','.filtroMain',function(e){
		if(e.originalEvent){
			const idFiltro = this.id;
			switch (idFiltro) {
				case 'Bancas':
					$('#Zonales').html('<option value="">Todos...</option>');
					$('#Jefaturas').html('<option value="">Todos...</option>');
					$('#Ejecutivos').html('<option value="">Todos...</option>');
				break;
				case 'Zonales':
					$('#Jefaturas').html('<option value="">Todos...</option>');
					$('#Ejecutivos').html('<option value="">Todos...</option>');
				break;
				case 'Jefaturas':
					$('#Ejecutivos').html('<option value="">Todos...</option>');
				break;
			}
			if(idFiltro!='Ejecutivos'){
				var parameters = {banca:null,zonal:null,jefatura:null};
				parameters.banca = $('#Bancas').val();
				parameters.zonal = $('#Zonales').val();
				parameters.jefatura = $('#Jefaturas').val();
				$.ajax({type: "GET", data: parameters, async: true, url: '{{route("infinity.alertascartera.combos")}}',
					success: function(result) {
						const data = JSON.parse(result);
						$('#Bancas').html(data[0]);
						$('#Zonales').html(data[1]);
						$("#Jefaturas").html(data[2]);
						$('#Ejecutivos').html(data[3]);
					}
				});
			}
			const filtro = getFiltroMasMenos();		
			resumenVigente($("#Bancas").val(), $("#Zonales").val(), $("#Jefaturas").val(), $("#Ejecutivos").val());
			call_chartSeguimiento({rango_deuda:filtro});
			//tablaLA($("#Bancas").val(), $("#Zonales").val(), $("#Jefaturas").val(), $("#Ejecutivos").val());
			call_tablaSeguimiento({rango_deuda:filtro});
			call_tablaCompromisos({rango_deuda:filtro});
		}
	});

	$(document).on("click", "#more", function() {
		if ($("#more").hasClass('selected')) {
			$("#more").addClass('unselected');
			$("#more").removeClass('selected');
		} else {
			$("#more").removeClass('unselected');
			$("#more").addClass('selected');
		}
		const filtro = getFiltroMasMenos();
		call_chartSeguimiento({rango_deuda:filtro});
	});
	$(document).on("click", "#less", function() {
		if ($("#less").hasClass('selected')) {
			$("#less").addClass('unselected');
			$("#less").removeClass('selected');
		} else {
			$("#less").removeClass('unselected');
			$("#less").addClass('selected');
		}
		const filtro = getFiltroMasMenos();
		call_chartSeguimiento({rango_deuda:filtro});
	});
	
	$(document).on("click", "#btninfosegui", function() {
		$("#infosegui").slideToggle("slow");
	});
	function showOption(value) {
        $('#optionHolder option[value="' + value + '"]').appendTo('#status');
    }
    function hideOption(value) {
        $('select option[value="' + value + '"]').appendTo('#optionHolder');
    }
	function tablaCompromisos(bancas = null, zonales = null, jefaturas = null, ejecutivos = null, es_infinitys = null, rango_atrasos = null, frecuencia_atraso_tots = null, leasings = null, rango_deudas = null, semaforo_m0s = null, feves = null, gestions = null, frecuencia_atraso_sin_dsctos = null, gesgys = null,estado_aprobacion=null) {
		if ($.fn.dataTable.isDataTable('#datatablesComp')) {
			$('#datatablesComp').DataTable().destroy();
		}
		$('#datatablesComp').DataTable({
			processing: true,
			"bAutoWidth": false,
			rowId: 'staffId',
			serverSide: true,
			language: {
				"url": "{{ URL::asset('js/Json/Spanish.Json') }}"
			},
			ajax: {
				type: "GET",
				url: "{{ route('infinity.alertascartera.seguimiento.compromisos') }}",
				data: function(data) {
					data.banca = bancas ? bancas : null;
					data.zonal = zonales ? zonales : null;
					data.jefatura = jefaturas ? jefaturas : null;
					data.ejecutivo = ejecutivos ? ejecutivos : null;
				}
			},
			"aLengthMenu": [
				[25, 50, -1],
				[25, 50, "Todo"]
			],
			"iDisplayLength": 25,
			"order": [
				[7, "asc"]
			],
			columnDefs: [
				{
					targets: 0,
					data: 'NOMBRE',
					name: 'NOMBRE',
					sortable: false,
					searchable: true,
					className: "celda-centrada",
					render: function(data, type, row) {
						var linea1 = 'CU: ' + row.COD_UNICO;
						var linea2 = '<br/>';
						linea2 += '<a target="_blank" href="{{route("infinity.alertascartera.detalle")}}?cu=' + row.COD_UNICO + '">' + row.NOMBRE + '</a>';
						return linea1+linea2;
					}
				},
				{
					targets: 1,
					data: 'ENCARGADO',
					name: 'ENCARGADO',
					sortable: false,
					searchable: true,
					className: "celda-centrada",
					render: function(data, type, row) {
						var linea1 = '<span>' + row.ENCARGADO + '</span>';
						var linea2 = '<br/>';
						if(row.NOMBRE_JEFE!=null && row.NOMBRE_JEFE!=undefined){
							linea2 += '<br/><span>' + row.NOMBRE_JEFE + '</span>';
						}
						return linea1 + linea2;
					}
				},
				{
					targets: 2,
					data: 'TIPO_GESTION',
					name: 'TIPO_GESTION',
					sortable: true,
					searchable: false,
					className: "celda-centrada",
					render: function(data, type, row) {
						return row.TIPO_GESTION;
					}
				},
				{
					targets: 3,
					data: 'TIPO_COMPROMISO',
					name: 'TIPO_COMPROMISO',
					searchable: false,
					sortable: false,
					className: "celda-centrada",
					render: function(data, type, row) {
						return row.TIPO_COMPROMISO;
					}
				},
				{
					targets: 4,
					data: 'TIPO_COMPROMISO_DETALLE',
					name: 'TIPO_COMPROMISO_DETALLE',
					searchable: false,
					className: "celda-centrada",
					sortable: false,
					render: function(data, type, row) {
						return row.TIPO_COMPROMISO_DETALLE;
					}
				},
				{
					targets: 5,
					data: 'DETALLE_COMPRA',
					name: 'DETALLE_COMPRA',
					className: "celda-centrada",
					searchable: true,
					sortable: false,
					render: function(data, type, row) {
						return row.DETALLE_COMPRA;
					}
				},
				{
					targets: 6,
					data: 'INDICADOR',
					name: 'INDICADOR',
					searchable: false,
					className: "celda-centrada",
					sortable: false,
					render: function(data, type, row) {
						return row.INDICADOR;
					}
				},
				{
					targets: 7,
					data: 'FECHA_COMPROMISO',
					name: 'FECHA_COMPROMISO',
					searchable: false,
					className: "celda-centrada",
					sortable: true,
					render: function(data, type, row) {
						return row.FECHA_COMPROMISO;
					}
				},
				{
					targets: 8,
					data: 'GESTOR',
					name: 'GESTOR',
					searchable: false,
					className: "celda-centrada",
					sortable: true,
					render: function(data, type, row) {
						return row.GESTOR;
					}
				},
				{
					targets: 9,
					data: null,
					name: null,
					className: "celda-centrada",
					searchable: false,
					sortable: false,
					render: function(data, type, row) {
						const gestor = row.GESTOR;
						var stringy = '';
						switch (gestor) {
							case 'EJECUTIVO DE NEGOCIO':
								if({{count($revision)}}>0){
									stringy = `<a style="cursor:pointer" onclick="gestionCumplimiento('` + row.ID + `','` + row.COD_UNICO + `','` + row.FECHA_COMPROMISO + `','` + row.ID_COVID + `')">Gestionar</a>`;
								}
							break;
							case 'EQUIPO SEGUIMIENTO':
								if({{count($revision)}}>1){
									stringy = `<a style="cursor:pointer" onclick="gestionCumplimiento('` + row.ID + `','` + row.COD_UNICO + `','` + row.FECHA_COMPROMISO + `','` + row.ID_COVID + `')">Gestionar</a>`;
								}
							break;
							case 'JEFE ZONAL':
								if({{count($revision)}}>2 || {{$esjefe}}|| {{$rol}}== {{App\Entity\Usuario::ROL_GERENCIA_ZONAL_BE}}){
									stringy = `<a style="cursor:pointer" onclick="gestionCumplimiento('` + row.ID + `','` + row.COD_UNICO + `','` + row.FECHA_COMPROMISO + `','` + row.ID_COVID + `')">Gestionar</a>`;
								}
							break;
							default:
							break;
						}
						return stringy;
					}
				}
			]
		});
	}
	function tablaLA(bancas = null, zonales = null, jefaturas = null, ejecutivos = null) {
		$.ajax({
			url: "{{ route('infinity.alertascartera.indicadores') }}",
			type: "GET",
			data: {
				banca: bancas,
				zonal: zonales,
				jefatura: jefaturas,
				ejecutivo: ejecutivos,
			},
			async: true,
			success: function(data) {
				$("#cartera").html(data.cartera);
				$("#clientesla").html(data.clientesla);
				var todos = data.verdecartera+data.ambarcartera+data.rojocartera+data.negrocartera;

				$("#barracartera_v").html(data.verdecartera);
				$("#barracartera_v").css("width", data.verdecartera*100/todos + "%");
				$("#barracartera_r").html(data.rojocartera);
				$("#barracartera_r").css("width", data.rojocartera*100/todos + "%");
				$("#barracartera_n").html(data.negrocartera);
				$("#barracartera_n").css("width", data.negrocartera*100/todos + "%");
				$("#barracartera_a").html(data.ambarcartera);
				$("#barracartera_a").css("width", data.ambarcartera*100/todos + "%");

				var todosla =data.verdela+data.ambarla+data.rojola+data.negrola;

				$("#barraclientesla_v").html(data.verdela);
				$("#barraclientesla_v").css("width", data.verdela*100/todosla + "%");
				$("#barraclientesla_r").html(data.rojola);
				$("#barraclientesla_r").css("width", data.rojola*100/todosla + "%");
				$("#barraclientesla_n").html(data.negrola);
				$("#barraclientesla_n").css("width", data.negrola*100/todosla + "%");
				$("#barraclientesla_a").html(data.ambarla);
				$("#barraclientesla_a").css("width", data.ambarla*100/todosla + "%");

				$("#barragespendientes").html(data.gestionespendientes + "%");
				$("#barragespendientes").css("width", data.gestionespendientes + "%");
				$("#barradocumentacion").html(data.documentacioncompleta + "%");
				$("#barradocumentacion").css("width", data.documentacioncompleta + "%");
				$("#barravisitaspendientes").html(data.visitaspendientes + "%");
				$("#barravisitaspendientes").css("width", data.visitaspendientes + "%");
				$("#barrarecalculospendientes").html(data.recalculospendientes + "%");
				$("#barrarecalculospendientes").css("width", data.recalculospendientes + "%");

				$("#vencidosibk").html(data.vencidosrcc);
				$("#vencidosrcc").html(data.vencidosibk);
			}
		});


		if ($.fn.dataTable.isDataTable('#datatable1')) {
			$('#datatable1').DataTable().destroy();
		}
		$('#datatable1').DataTable({
			processing: true,
			"bAutoWidth": false,
			rowId: 'staffId',
			dom: 'Blfrtip',
			buttons: [{
					"extend": 'pdf',
					"text": 'PDF',
					"className": 'btn btn-danger btn-xs'
				},
				{
					"extend": 'excel',
					"text": 'EXCEL',
					"className": 'btn btn-success btn-xs'
				}
			],
			serverSide: true,
			language: {
				"url": "{{ URL::asset('js/Json/Spanish.json') }}"
			},
			ajax: {
				"type": "GET",
				"url": "{{ route('infinity.alertascartera.getclientes') }}",
				data: function(data) {
					data.banca = bancas ? bancas : null;
					data.zonal = zonales ? zonales : null;
					data.jefatura = jefaturas ? jefaturas : null;
					data.ejecutivo = ejecutivos ? ejecutivos : null;
				}
			},
			"aLengthMenu": [
				[25, 50, -1],
				[25, 50, "Todo"]
			],
			"iDisplayLength": 25,
			"order": [
				[9, "desc"]
			],
			columnDefs: [{
					targets: 0,
					data: 'FLG_INFINITY',
					name: 'FLG_INFINITY',
					searchable:false,
					className: "celda-centrada",
					render: function(data, type, row) {
						if (row.FLG_INFINITY == 1) {
							return '<img src="' + $('#icono').val() + '" style="width: 20px"/>';
						} else if (row.FLG_INFINITY == 2) {
							return '<img src="' + $('#icono2').val() + '" style="width: 20px"/>';
						} else {
							return '';
						}
					}
				},
				{
					targets: 1,
					data: null,
					searchable: false,
					sortable: false,
					render: function(data, type, row) {
						html = '';
						if (row.EJECUTIVO)
							html += '' + row.EJECUTIVO;

						if ($('#flg-visibilidad-zonal').val() == '1')
							html += '<br/>' + 'JEFE: ' + row.JEFATURA;

						if ($('#flg-visibilidad-banca').val() == '1')
							html += '<br/>' + 'ZONAL: ' + row.ZONAL;

						return html;
					}
				},
				{
					targets: 2,
					data: 'WIC.COD_UNICO',
					name: 'WIC.COD_UNICO',
					render: function(data, type, row) {
						var linea1 = "CU: " + row.COD_UNICO;
						if (row.PROTEGIDO == 1) {
							linea1 += "<span style='color:#F5B54D;float: right;margin-top: 3px;margin-right: 15px'><i class='fa fa-shield' aria-hidden='true'></i></span>";
						}
						var linea2 = "<br/><a target='_blank' href='{{route('infinity.alertascartera.detalle')}}?cu=" + row.COD_UNICO + "'>" + row.NOMBRE + "</a>";

						return linea1 + linea2;
					}
				},
				{
					targets: 3,
					data: 'SALDO_RCC',
					name: 'SALDO_RCC',
					searchable: false,
					render: function(data, type, row) {
						if (row.SALDO_RCC==null){
							return "-";	
						}
						return numeral(row.SALDO_RCC / 1000).format('0,0') + 'M';
					}
				},
				{
					targets: 4,
					data: 'SALDO_VENCIDO_RCC',
					name: 'SALDO_VENCIDO_RCC',
					searchable: false,
					render: function(data, type, row) {
						var linea1 = 'IBK: ' + numeral(row.SALDO_VENCIDO_IBK / 1000).format('0,0') + 'M';
						var linea2 = '<br/>RCC: ' + numeral(row.SALDO_VENCIDO_RCC / 1000).format('0,0') + 'M';
						return linea1 + linea2;
					}
				},
				{
					targets: 5,
					data: 'NIVEL_ALERTA',
					name: 'NIVEL_ALERTA',
					searchable: false,
					className: "celda-centrada",
					render: function(data, type, row) {
						var color1 = $('#semaforo-' + row.NIVEL_ALERTA).val();
						var color2 = $('#semaforo-' + row.NIVEL_ALERTA_ANTERIOR).val();
						var color3 = $('#semaforo-' + row.NIVEL_ALERTA_ANTERIOR_2).val();
						return '<i class="fa fa-circle fa-lg" style="padding-right : 3px; color:' + color3 + '"></i>' +
							'<i class="fa fa-circle fa-lg" style="padding-right : 3px; color:' + color2 + '"></i>' +
							'<i class="fa fa-circle fa-2x" style="color:' + color1 + '"></i>';
					}
				},
				{
					targets: 6,
					data: 'FECHA_ULTIMO_RECALCULO_INFINITY',
					name: 'FECHA_ULTIMO_RECALCULO_INFINITY',
					searchable: false,
					className: "celda-centrada",
					render: function(data, type, row, meta) {
						if (row.ALERTA_RECALCULO==null) {
							return '-';
						} else {
							return $('.alerta-icono-' + row.ALERTA_RECALCULO)[0].outerHTML;
						}
					}
				},
				{
					targets: 7,
					data: 'ALERTA_DOCUMENTACION',
					name : 'ALERTA_DOCUMENTACION',
					searchable: false,
					className: "celda-centrada",
					render: function(data, type, row) {
						if (row.ALERTA_DOCUMENTACION == null) {
							return '-';
						} else {
							return $('.alerta-icono-' + row.ALERTA_DOCUMENTACION)[0].outerHTML;
						}
					}
				},
				{
					targets: 8,
					data: 'FECHA_ULTIMA_VISITA_INFINITY',
					name : 'FECHA_ULTIMA_VISITA_INFINITY',
					searchable: false,
					className: "celda-centrada",
					render: function(data, type, row, meta) {
						if (row.ALERTA_VISITA == null) {
							return '';
						} else {
							return $('.alerta-icono-' + row.ALERTA_VISITA)[0].outerHTML;
						}
					}
				},
				{
					targets: 9,
					data: 'CANT_GESTIONES_PENDIENTES',
					name : 'CANT_GESTIONES_PENDIENTES',
					searchable: false,
					className: "celda-centrada",
					render: function(data, type, row, meta) {
						var html = '';
						if (row.FLG_INFINITY >= 1) {
							if (row.TIMER < 0) {
								texto = 'vence en ' + Math.abs(row.TIMER) + ' día(s)';
								html += '<div style="color: #73879C"><i class="fas fa-clock"></i> ' + texto + '</div>';
							}
							if (row.TIMER == 0) {
								html += '<div style="color: #D9534F"><i class="fas fa-clock"></i> vence hoy </div>';
							}
							if (row.TIMER > 0) {
								texto = 'venció hace ' + Math.abs(row.TIMER) + ' día(s)';
								html += '<div style="color: #D9534F"><i class="fas fa-clock"></i> ' + texto + '</div>';
							}
						}
						return html;
					}
				},
				{
					targets: 10,
					data: null,
					searchable: false,
					sortable: false,
					render: function(data, type, row) {
						html = '<a target="_blank" href="{{route("infinity.me.cliente.conoceme")}}?cu=' + row.COD_UNICO + '"><i class="fa fa-user-plus" aria-hidden="true"></i> Conóceme</a><br />';
						if (row.CANT_POLI_CUALI == null || row.CANT_POLI_CUALI < 0) {
							html += ' <a target="_blank" href="{{route("infinity.me.cliente.visita")}}?cu=' + row.COD_UNICO + '"><i class="fa fa-home" aria-hidden="true"></i> Visita</a><br />';
						}
						return html;
					}
				},
				{
					targets: 11,
					data: 'CANT_GESTIONES_COMITE_PENDIENTES',
					name: 'CANT_GESTIONES_COMITE_PENDIENTES',
					searchable: false,
					render: function(data, type, row) {
						html = '';
						if ((row.FLG_INFINITY > 1 && row.CANT_GESTIONES_COMITE_PENDIENTES != null) || (row.FLG_INFINITY == '0' && row.PERMITIDOS == 1) || row.PERMITIDO_RSG == 1) {
							html = '<a id="click_comite" cu="' + row.COD_UNICO + '" rmg="' + row.RMG + '" rating="' + row.RATING + '">Pendiente</a>';
						}
						return html;
					}
				},
				{
					targets: 12,
					data: 'WIC.NOMBRE',
					name: 'WIC.NOMBRE',
					sortable: false,
					visible: false,
					render: function(data, type, row) {
						return '';
					}
				}
			]
		});
	}

	$(document).on("click", "#unoSieteContainer", function() {
		$('#es_infinity').selectpicker('val', '');
		$('#rango_atraso').selectpicker('val', '[1-7 dias]');
		$('#frecuencia_atraso_tot').selectpicker('val', '');
		$('#leasings').selectpicker('val', '');
		$('#rango_deuda').selectpicker('val', '');
		$('#semaforo_m0').selectpicker('val', '');
		$('#feve').selectpicker('val', '');
		$('#gestion').selectpicker('val', '');
		$('#frecuencia_atraso_sin_dscto').selectpicker('val', '');
		if (rol=='38') {
			$('#gesgys').selectpicker('val', '');
		}

		$('.nav-tabs a[href="#segdiario"]').tab('show');
		$('.nav-tabs a[href="#tablaseg"]').tab('show');
		const filtro = getFiltroMasMenos();
		$('#rango_deuda').selectpicker('val', filtro);
		call_tablaSeguimiento({rango_deuda:filtro});
		call_tablaCompromisos({rango_deuda:filtro});
	});
	$(document).on("click", "#ochoCatorceContainer", function() {
		$('#es_infinity').selectpicker('val', '');
		$('#rango_atraso').selectpicker('val', '[8-15 dias]');
		$('#frecuencia_atraso_tot').selectpicker('val', '');
		$('#leasings').selectpicker('val', '');
		$('#rango_deuda').selectpicker('val', '');
		$('#semaforo_m0').selectpicker('val', '');
		$('#feve').selectpicker('val', '');
		$('#gestion').selectpicker('val', '');
		$('#frecuencia_atraso_sin_dscto').selectpicker('val', '');
		if (rol=='38') {
			$('#gesgys').selectpicker('val', '');
		}

		$('.nav-tabs a[href="#segdiario"]').tab('show');
		$('.nav-tabs a[href="#tablaseg"]').tab('show');
		const filtro = getFiltroMasMenos();
		$('#rango_deuda').selectpicker('val', filtro);
		call_tablaSeguimiento({rango_deuda:filtro});
		call_tablaCompromisos({rango_deuda:filtro});
	});
	$(document).on("click", "#quinceContainer", function() {
		$('#es_infinity').selectpicker('val', '');
		$('#rango_atraso').selectpicker('val', 'Más de 15 días');
		$('#frecuencia_atraso_tot').selectpicker('val', '');
		$('#leasings').selectpicker('val', '');
		$('#rango_deuda').selectpicker('val', '');
		$('#semaforo_m0').selectpicker('val', '');
		$('#feve').selectpicker('val', '');
		$('#gestion').selectpicker('val', '');
		$('#frecuencia_atraso_sin_dscto').selectpicker('val', '');
		if (rol=='38') {
			$('#gesgys').selectpicker('val', '');
		}

		$('.nav-tabs a[href="#segdiario"]').tab('show');
		$('.nav-tabs a[href="#tablaseg"]').tab('show');
		const filtro = getFiltroMasMenos();
		$('#rango_deuda').selectpicker('val', filtro);
		call_tablaSeguimiento({rango_deuda:filtro});
		call_tablaCompromisos({rango_deuda:filtro});
	});

	$(document).on("click", "#unoSieteContainer_2", function() {
		$('#es_infinity').selectpicker('val', '');
		$('#rango_atraso').selectpicker('val', '[1-7 dias]');
		$('#frecuencia_atraso_tot').selectpicker('val', '');
		$('#leasings').selectpicker('val', '');
		$('#rango_deuda').selectpicker('val', '');
		$('#semaforo_m0').selectpicker('val', '');
		$('#feve').selectpicker('val', '');
		$('#gestion').selectpicker('val', '');
		$('#frecuencia_atraso_sin_dscto').selectpicker('val', '');
		if (rol=='38') {
			$('#gesgys').selectpicker('val', '');
		}

		$('.nav-tabs a[href="#tablaseg"]').tab('show');
		const filtro = getFiltroMasMenos();
		call_tablaSeguimiento({rango_deuda:filtro});
		call_tablaCompromisos({rango_deuda:filtro});
	});
	$(document).on("click", "#ochoCatorceContainer_2", function() {
		$('#es_infinity').selectpicker('val', '');
		$('#rango_atraso').selectpicker('val', '[8-15 dias]');
		$('#frecuencia_atraso_tot').selectpicker('val', '');
		$('#leasings').selectpicker('val', '');
		$('#rango_deuda').selectpicker('val', '');
		$('#semaforo_m0').selectpicker('val', '');
		$('#feve').selectpicker('val', '');
		$('#gestion').selectpicker('val', '');
		$('#frecuencia_atraso_sin_dscto').selectpicker('val', '');
		if (rol=='38') {
			$('#gesgys').selectpicker('val', '');
		}

		$('.nav-tabs a[href="#tablaseg"]').tab('show');
		const filtro = getFiltroMasMenos();
		$('#rango_deuda').selectpicker('val', filtro);
		call_tablaSeguimiento({rango_deuda:filtro});
		call_tablaCompromisos({rango_deuda:filtro});
	});
	$(document).on("click", "#quinceContainer_2", function() {
		$('#es_infinity').selectpicker('val', '');
		$('#rango_atraso').selectpicker('val', 'Más de 15 días');
		$('#frecuencia_atraso_tot').selectpicker('val', '');
		$('#leasings').selectpicker('val', '');
		$('#rango_deuda').selectpicker('val', '');
		$('#semaforo_m0').selectpicker('val', '');
		$('#feve').selectpicker('val', '');
		$('#gestion').selectpicker('val', '');
		$('#frecuencia_atraso_sin_dscto').selectpicker('val', '');
		
		$('.nav-tabs a[href="#tablaseg"]').tab('show');
		const filtro = getFiltroMasMenos();
		$('#rango_deuda').selectpicker('val', filtro);
		call_tablaSeguimiento({rango_deuda:filtro});
		call_tablaCompromisos({rango_deuda:filtro});
	});

	$(document).on('click', '#click_comite', function() {
		$("#usuario_permitido").html('');
		$("#re_usu_permitido").val('');

		$('#rmg_modal').val('');
		$('#rating_modal').val('');


		$("#fechaGestion").val('');
		$("#PoliticaAsociada_Comite").val('');
		$("#DecisionComite").val('');
		$("#adjuntoGestion").val('');

		$('#rmg_modal').val($(this).attr('rmg'));
		$('#rating_modal').val($(this).attr('rating'));

		$('.dfecha').each(function() {
			$(this).datepicker({
					maxViewMode: 1,
					//daysOfWeekDisabled: "0,6",
					language: "es",
					autoclose: true,
					startDate: "-365d",
					endDate: "0d",
					format: "yyyy-mm-dd",
				})
				.on('changeDate', function(e) {
					// Revalidate the date field
					$('#frmGestion_comite').formValidation('revalidateField', 'fechaGestion');
				});
		});

		$('#frmGestion_comite').formValidation({
			framework: 'bootstrap',
			icon: {
				valid: 'glyphicon glyphicon-ok',
				invalid: 'glyphicon glyphicon-remove',
				validating: 'glyphicon glyphicon-refresh'
			},
			fields: {
				'fechaGestion': {
					validators: {
						notEmpty: {
							message: 'Debes seleccionar una fecha'
						},
						date: {
							format: 'YYYY-MM-DD',
							message: 'Ingrese una fecha válida',
						}
					}
				},
				'PoliticaAsociada': {
					validators: {
						notEmpty: {
							message: 'Debes seleccionar una Política'
						}
					}
				},
				'comentarioGestion': {
					validators: {
						notEmpty: {
							message: 'Debes seleccionar un estado'
						},
						stringLength: {
							max: 500,
							min: 30,
							message: 'El comentario debe tener entre 30 y 500 caracteres'
						}
					}
				},
				'DecisionComite': {
					validators: {
						notEmpty: {
							message: 'Debes seleccionar una Decisión'
						}
					}
				},
				'tiempo': {
					validators: {
						notEmpty: {
							message: 'Debes seleccionar una Decisión'
						}
					}
				}
			},
		});

		var cu = $(this).attr('cu');
		var rmg = $(this).attr('rmg');
		var rating = $(this).attr('rating');
		$(".checkboxtiempos").attr('hidden', 'hidden');
		$(".checktiempo").attr('disabled', 'disabled');
		if (getpoliticaid(this) == '23' && $("#DecisionComite").val() == 1) {
			$(".checktiempo").removeAttr('disabled');
		}
		if (parseInt(cu) > 0) {
			$('#modalDocumentacion_Comite').modal();
			
			$.ajax({
				type: "GET",
				data: {
					cu: cu,
					rmg: rmg,
					rating: rating,
				},
				async: true,
				url: "{{route('infinity.me.cliente.politicas_comite')}}",
				success: function(result) {
					autonomias = [];
					autonomiasnombres = [];
					for (var i = result.autonomias.length - 1; i >= 0; i--) {
						autonomias.push(result.autonomias[i]['REGISTRO']);
						autonomiasnombres.push(result.autonomias[i]['NOMBRE']);
					}

					todos = [];
					for (var i = result.todos.length - 1; i >= 0; i--) {
						todos.push(result.todos[i]['REGISTRO']);
					}

					if ($.inArray($("#logeado").val(),todos)>=0) {
						$('.guardarGestion').removeAttr('hidden');
						$('.valid').attr('hidden', 'hidden');
					}else{
						$('.guardarGestion').attr('hidden', 'hidden');
						$('.valid').removeAttr('hidden');
					}
					if (autonomiasnombres.length>1) {
						$("#texto_usuario_permitido").html('Las personas con menor autonomia son:');
						$span="";
						for (var j = 0; j < autonomiasnombres.length; j++) {
							$span=$span+"<span>"+autonomiasnombres[j]+"</span><br>";
						}
						$("#usuario_permitido").html($span);
					}else{
						$("#texto_usuario_permitido").html('La persona con menor autonomia es:');
						$("#usuario_permitido").html(autonomiasnombres[0]);
					}
					$("#codUnico").val(cu);
					$("#PoliticaAsociada_Comite").html(result.politica_se);
				}
			});
		}
	});
	$(document).on('change', '#PoliticaAsociada_Comite', function() {
		if (getpoliticaid(this) == '23') {
			$(".checkboxtiempos").removeAttr('hidden');
		} else {
			$(".checkboxtiempos").attr('hidden', 'hidden');
		}
		if ($("#DecisionComite").val() == 1) {
			$(".checktiempo").removeAttr('disabled');
		} else {
			$(".checktiempo").attr('disabled', 'disabled');
		}
	});

	$(document).on('change', '#DecisionComite', function() {
		if ($(this).val() == 1) {
			$(".checktiempo").removeAttr('disabled');
		} else {
			$(".checktiempo").attr('disabled', 'disabled');
		}
	});

	function getpoliticaid(select) {
		return $('option:selected', select).attr('politica_id');
	}
    function gestionCumplimiento(idCumplimiento,cu,fecha,idcovid) {
        $.ajax({
            type: 'POST',
            data: {
                'ID': idCumplimiento,
                'cu': cu
            },
            async: true,
            url: "{{route('infinity.alertascartera.validadorEliminar')}}",
            success: function(result) {
                if (result == '1') {
					showOption('ELIMINADO');
					$('#fechaCompromisoGestion').attr('disabled',false);
                    $('#rowFechaCompromiso').css('display','block');
                } else {
					hideOption('ELIMINADO');
					$('#fechaCompromisoGestion').attr('disabled',true);
                    $('#rowFechaCompromiso').css('display','none');
                }
                $('#fechaCompromisoGestion').val(fecha.trim());
                $('#IDCUMPL').val(idCumplimiento);
                $('#IDCOVID').val(idcovid);
                $('#CU').val(cu);
				$('#modalGestionar').modal();
            }
		});
	}
	$('.fechaCompromiso').each(function() {
        $(this).on('keydown', function() {
            return false;
        });
        const today = new Date();
        const tomorrow = new Date(today);
        tomorrow.setDate(tomorrow.getDate() + 1);
        $(this).datepicker({
            maxViewMode: 1,
            language: "es",
            autoclose: true,
            startDate: tomorrow.toISOString().substring(0, 10),
            format: "yyyy-mm-dd",
        });
    });

	function gestionCumplimientoPOST() {
        var today = new Date();
        const status = $('#status').val();
        const fechaCompromiso = $('#fechaCompromisoGestion').val();
        const cu = $('#CU').val();
        const idcovid = $('#IDCOVID').val();
        const idCumplimiento = $('#IDCUMPL').val();
        const name = $('input[value="' + idCumplimiento + '"]').attr('name');
        const index = name.replace('ID_CUMPL', '');
        $('#btnAct').attr('disabled', true);
        $.ajax({
            type: 'POST',
            data: {
                'ID': idCumplimiento,
                'ID_COVID': idcovid,
                'cu': cu,
                'FECHA_COMPROMISO': fechaCompromiso,
                'STATUS_COMPROMISO': status,
            },
            async: true,
            url: "{{route('infinity.alertascartera.guardarCumplimiento')}}",
            success: function(result) {
				var res = JSON.parse(result);
				const response = res['result'];
				if(response){
					$('#btnAct').attr('disabled', false);
					call_tablaCompromisos();
				}
                $('#modalGestionar').hide();
                $('#modalGestionar').modal('toggle');
                $('#messageModal').html(res['message']);
				$('#modalGuardar').modal();
			}
        });
        return false;
    }
	function resumenVigente(bancas = null, zonales = null, jefaturas = null, ejecutivos = null) {
		$.ajax({
			cache: false,
			url: "{{ route('infinity.alertascartera.getResumenAlertas') }}",
			type: "GET",
			data: {
				banca: bancas ? bancas : null,
				zonal: zonales ? zonales : null,
				jefatura: jefaturas ? jefaturas : null,
				ejecutivo: ejecutivos ? ejecutivos : null,
			},
			async: false,
			success: function(data) {
				var menor = 0;
				var mayor = 0;
				var infinity = $('#infinity').hasClass('selected');
				var NoInfinity = $('#NoInfinity').hasClass('selected');
				var infinityCounter = 0;
				var NoInfinityCounter = 0;
				var infinityCounterGes = 0;
				var NoInfinityCounterGes = 0;
				var infinityCounterGesDen = 0;
				var NoInfinityCounterGesDen = 0;
				for (let index = 0; index < data.length; index++) {
					var row = data[index];
					if (row['FLG_MAYOR_50K'] == '1' && (row['FLG_POLITICA_ATRASO'] == '1')) {
						mayor++;
					}
					if (row['FLG_MENOR_50K'] == '1' && (row['FLG_POLITICA_ATRASO'] == '1')) {
						menor++;
					}
					if (row['FLG_INFINITY'] == '1' && (row['FLG_VIGENTE'] == '1')) {
						infinityCounter++;
					}
					if (row['FLG_INFINITY'] == '0' && (row['FLG_VIGENTE'] == '1')) {
						NoInfinityCounter++;
					}

					if (row['FLG_VIGENTE'] == '1' && (row['FLG_INFINITY'] == '1') && (row['FLG_POLITICA_INFINITY '] == '1') && (row['FLG_INIFNITY_PENDIENTE'] == '1')) {
						infinityCounterGes++;
					}
					if (row['FLG_VIGENTE'] == '1' && row['FLG_INFINITY'] == '1' && row['FLG_POLITICA_INFINITY'] == '1') {
						infinityCounterGesDen++;
					}
					if (row['FLG_POLITICA_INFINITY'] == '0' && (row['FLG_POLITICA_VIGENTE'] == '1') && (row['FLG_VIG_GESTIONADO'] == '1')) {
						NoInfinityCounterGes++;
					}
					if (row['FLG_VIGENTE'] == '1' && row['FLG_POLITICA_VIGENTE'] == '1') {
						NoInfinityCounterGesDen++;
					}
				}

				$("#moreText").html("Mayor a S/ 50M: " + mayor.toString());
				$("#lessText").html("Menor a S/ 50M: " + menor.toString());
				$("#infinityText").html("Con L.A.: " + infinityCounter.toString());
				$("#NoInfinityText").html("Sin L.A.: " + NoInfinityCounter.toString());
				var vigentes = infinityCounter + NoInfinityCounter;
				var atrasos = mayor + menor;
				var clientes = atrasos + vigentes;
				$('#vigentes').html('Vigentes: ' + vigentes.toString());
				$('#clientes').html('Clientes Cartera: ' + clientes.toString());
				$("#atrasos").html('Con Atrasos: ' + (atrasos).toString());
				var gestionadosDen = infinityCounterGesDen + NoInfinityCounterGesDen;
				var gestionados = infinityCounterGes + NoInfinityCounterGes;
				var ratio = 0;
				if (infinity && NoInfinity) {
					$('#gestionVigCounter').html('Pendientes ' + (gestionadosDen - gestionados).toString() + ' / ' + gestionadosDen.toString());
					ratio = gestionadosDen != 0 ? ((gestionados) / gestionadosDen) : 0;
				} else if (infinity) {
					$('#gestionVigCounter').html('Pendientes ' + (infinityCounterGesDen - infinityCounterGes).toString() + ' / ' + infinityCounterGesDen.toString());
					ratio = infinityCounterGesDen != 0 ? (infinityCounterGes) / infinityCounterGesDen : 0;
				} else if (NoInfinity) {
					$('#gestionVigCounter').html('Pendientes ' + (NoInfinityCounterGesDen - NoInfinityCounterGes).toString() + ' / ' + NoInfinityCounterGesDen.toString());
					ratio = NoInfinityCounterGesDen != 0 ? (NoInfinityCounterGes) / NoInfinityCounterGesDen : 0;
				}
				if (window.Chart4) window.Chart4.destroy();
				var width = $('#gestionVigContainer').width();
				$("#gestionVigScore").html(funre4((ratio) * 100) + "%");
				nota = funre4((ratio) * 100 > 100.00 ? 100.00 : funre4((ratio) * 100));
				window.Chart4 = new Chart(ctx4, {
					type: 'doughnut',
					data: {
						datasets: [{

							data: [nota, funre4(100.00 - nota)],
							backgroundColor: [
								'#4DD094',
								'whitesmoke',
							],
							borderWidth: 0.6
						}],
						labels: [
							'Nota',
							'',
						]
					},
					options: {
						legend: {
							display: false
						},
						cutoutPercentage: 70,
						responsive: false,
						mantainRatio: true,
						tooltips: {
							enabled: false
						},
					}
				});
			}
		})
	}

	function chartSeguimiento(banca = null, zonal = null, jefatura = null, ejecutivo = null, es_infinity = null, rango_atraso = null, frecuencia_atraso_tot = null, leasing = null, rango_deuda = null, semaforo_m0 = null, feve = null, gestion = null, frecuencia_atraso_sin_dscto = null, gesgys = null) {
		$.ajax({
			url: "{{ route('infinity.alertascartera.seguimiento') }}",
			type: "GET",
			data: {
				banca: banca ? banca : null,
				zonal: zonal ? zonal : null,
				jefatura: jefatura ? jefatura : null,
				ejecutivo: ejecutivo ? ejecutivo : null,
				es_infinity: es_infinity ? es_infinity : null,
				rango_atraso: rango_atraso ? rango_atraso : null,
				frecuencia_atraso_tot: frecuencia_atraso_tot ? frecuencia_atraso_tot : null,
				leasing: leasing ? leasing : null,
				rango_deuda: rango_deuda ? rango_deuda : null,
				semaforo_m0: semaforo_m0 ? semaforo_m0 : null,
				feve: feve ? feve : null,
				gestion: gestion ? gestion : null,
				frecuencia_atraso_sin_dscto: frecuencia_atraso_sin_dscto ? frecuencia_atraso_sin_dscto : null,
				gesgys : gesgys ? gesgys : null
			},
			async: true,
			success: function(data) {
				var arr = data['data'];
				var sum11 = 0;
				var sum12 = 0;
				var sum13 = 0;
				var n11 = 0;
				var n12 = 0;
				var n13 = 0;

				var ratio = 0;
				for (let index = 0; index < arr.length; index++) {
					const element = arr[index];
					if (element['DIAS'] > 0) {
						switch (element['RANGO_DIAS_ATRASO']) {
							case '[1-7 dias]':
								n11++;
								if (element['FLG_GESTIONADO'] == 'GESTIONADO') {
									sum11++;
								}
								break;
							case '[8-15 dias]':
								n12++;
								if (element['FLG_GESTIONADO'] == 'GESTIONADO') {
									sum12++;
								}

								break;
							case 'Más de 15 días':
								n13++;
								if (element['FLG_GESTIONADO'] == 'GESTIONADO') {
									sum13++;
								}

								break;
						}

					}
				}
				if (n11 == 0) {
					ratio = 1;
				} else {
					ratio = sum11 / n11;
				}

				if (es_infinity == null && rango_atraso == null && frecuencia_atraso_tot == null && leasing == null && semaforo_m0 == null && feve == null && gestion == null && frecuencia_atraso_sin_dscto == null) {
					if (window.Chart1) window.Chart1.destroy();
					var width = $('#unoSieteContainer').width();
					$("#unoSieteScore").html(funre4((ratio) * 100) + "%");
					$("#unoSieteCounter").html("Pendientes " + (n11 - sum11).toString() + " / " + n11.toString());
					nota = funre4((ratio) * 100 > 100.00 ? 100.00 : funre4((ratio) * 100));
					window.Chart1 = new Chart(ctx1, {
						type: 'doughnut',
						data: {
							datasets: [{

								data: [nota, funre4(100.00 - nota)],
								backgroundColor: [
									'#4DD094',
									'whitesmoke',
								],
								borderWidth: 0.6
							}],
							labels: [
								'Nota',
								'',
							]
						},
						options: {
							legend: {
								display: false
							},
							cutoutPercentage: 70,
							responsive: false,
							mantainRatio: true,
							tooltips: {
								enabled: false
							},
						}
					});
				}



				if (window.Chart5) window.Chart5.destroy();
				var width = $('#unoSieteContainer_2').width();
				$("#unoSieteScore_2").html(funre4((ratio) * 100) + "%");
				$("#unoSieteCounter_2").html("Pendientes " + (n11 - sum11).toString() + " / " + n11.toString());
				nota = funre4((ratio) * 100 > 100.00 ? 100.00 : funre4((ratio) * 100));
				window.Chart5 = new Chart(ctx5, {
					type: 'doughnut',
					data: {
						datasets: [{

							data: [nota, funre4(100.00 - nota)],
							backgroundColor: [
								'#4DD094',
								'whitesmoke',
							],
							borderWidth: 0.6
						}],
						labels: [
							'Nota',
							'',
						]
					},
					options: {
						legend: {
							display: false
						},
						cutoutPercentage: 70,
						responsive: false,
						mantainRatio: true,
						tooltips: {
							enabled: false
						},
					}
				});

				if (n12 == 0) {
					ratio = 1;
				} else {
					ratio = sum12 / n12;
				}
				if (es_infinity == null && rango_atraso == null && frecuencia_atraso_tot == null && leasing == null && semaforo_m0 == null && feve == null && gestion == null && frecuencia_atraso_sin_dscto == null) {
					if (window.Chart2) window.Chart2.destroy();
					var width = $('#ochoCatorceContainer').width();
					$("#ochoCatorceScore").html(funre4((ratio) * 100) + "%");
					$("#ochoCatorceCounter").html("Pendientes " + (n12 - sum12).toString() + " / " + n12.toString());
					nota = funre4((ratio) * 100 > 100.00 ? 100.00 : funre4((ratio) * 100));
					window.Chart2 = new Chart(ctx2, {
						type: 'doughnut',
						data: {
							datasets: [{
								data: [nota, funre4(100.00 - nota)],
								backgroundColor: [
									'#4DD094',
									'whitesmoke',
								],
								borderWidth: 0.6
							}],
							labels: [
								'Nota',
								'',
							]
						},
						options: {
							legend: {
								display: false
							},
							cutoutPercentage: 70,
							responsive: false,
							mantainRatio: true,
							tooltips: {
								enabled: false
							},
						}
					});
				}

				if (window.Chart6) window.Chart6.destroy();
				var width = $('#ochoCatorceContainer_2').width();
				$("#ochoCatorceScore_2").html(funre4((ratio) * 100) + "%");
				$("#ochoCatorceCounter_2").html("Pendientes " + (n12 - sum12).toString() + " / " + n12.toString());
				nota = funre4((ratio) * 100 > 100.00 ? 100.00 : funre4((ratio) * 100));
				window.Chart6 = new Chart(ctx6, {
					type: 'doughnut',
					data: {
						datasets: [{
							data: [nota, funre4(100.00 - nota)],
							backgroundColor: [
								'#4DD094',
								'whitesmoke',
							],
							borderWidth: 0.6
						}],
						labels: [
							'Nota',
							'',
						]
					},
					options: {
						legend: {
							display: false
						},
						cutoutPercentage: 70,
						responsive: false,
						mantainRatio: true,
						tooltips: {
							enabled: false
						},
					}
				});

				if (n13 == 0) {
					ratio = 1;
				} else {
					ratio = sum13 / n13;
				}
				if (es_infinity == null && rango_atraso == null && frecuencia_atraso_tot == null && leasing == null && semaforo_m0 == null && feve == null && gestion == null && frecuencia_atraso_sin_dscto == null) {
					if (window.Chart3) window.Chart3.destroy();
					var width = $('#quinceContainer').width();
					$("#quinceScore").html(funre4((ratio) * 100) + "%");
					nota = funre4((ratio) * 100 > 100.00 ? 100.00 : funre4((ratio) * 100));
					$("#quinceCounter").html("Pendientes " + (n13 - sum13).toString() + " / " + n13.toString());
					window.Chart3 = new Chart(ctx3, {
						type: 'doughnut',
						data: {
							datasets: [{

								data: [nota, funre4(100.00 - nota)],
								backgroundColor: [
									'#4DD094',
									'whitesmoke',
								],
								borderWidth: 0.6
							}],
							labels: [
								'Nota',
								'',
							]
						},
						options: {
							legend: {
								display: false
							},
							cutoutPercentage: 70,
							responsive: false,
							mantainRatio: true,
							tooltips: {
								enabled: false
							},
						}
					});

				}

				if (window.Chart7) window.Chart7.destroy();
				var width = $('#quinceContainer_2').width();
				$("#quinceScore_2").html(funre4((ratio) * 100) + "%");
				nota = funre4((ratio) * 100 > 100.00 ? 100.00 : funre4((ratio) * 100));
				$("#quinceCounter_2").html("Pendientes " + (n13 - sum13).toString() + " / " + n13.toString());
				window.Chart7 = new Chart(ctx7, {
					type: 'doughnut',
					data: {
						datasets: [{

							data: [nota, funre4(100.00 - nota)],
							backgroundColor: [
								'#4DD094',
								'whitesmoke',
							],
							borderWidth: 0.6
						}],
						labels: [
							'Nota',
							'',
						]
					},
					options: {
						legend: {
							display: false
						},
						cutoutPercentage: 70,
						responsive: false,
						mantainRatio: true,
						tooltips: {
							enabled: false
						},
					}
				});
			}
		});
	}

	function tablaseguimiento(bancas = null, zonales = null, jefaturas = null, ejecutivos = null, es_infinitys = null, rango_atrasos = null, frecuencia_atraso_tots = null, leasings = null, rango_deudas = null, semaforo_m0s = null, feves = null, gestions = null, frecuencia_atraso_sin_dsctos = null, gesgys = null,estado_aprobacion=null) {
		if ($.fn.dataTable.isDataTable('#datatables')) {
			$('#datatables').DataTable().destroy();
		}
		$('#datatables').DataTable({
			processing: true,
			"bAutoWidth": false,
			rowId: 'staffId',
			serverSide: true,
			language: {
				"url": "{{ URL::asset('js/Json/Spanish.Json') }}"
			},
			ajax: {
				type: "GET",
				url: "{{ route('infinity.alertascartera.seguimiento') }}",
				data: function(data) {
					data.banca = bancas ? bancas : null;
					data.zonal = zonales ? zonales : null;
					data.jefatura = jefaturas ? jefaturas : null;
					data.ejecutivo = ejecutivos ? ejecutivos : null;
					data.es_infinity = es_infinitys ? es_infinitys : null;
					data.rango_atraso = rango_atrasos ? rango_atrasos : null;
					data.frecuencia_atraso_tot = frecuencia_atraso_tots ? frecuencia_atraso_tots : null;
					data.leasing = leasings ? leasings : null;
					data.rango_deuda = rango_deudas ? rango_deudas : null;
					data.semaforo_m0 = semaforo_m0s ? semaforo_m0s : null;
					data.feve = feves ? feves : null;
					data.gestion = gestions ? gestions : null;
					data.frecuencia_atraso_sin_dscto = frecuencia_atraso_sin_dsctos ? frecuencia_atraso_sin_dsctos : null;
					data.gesgys = gesgys ? gesgys : null;
					data.estado_aprobacion = estado_aprobacion? estado_aprobacion : null;
				}
			},
			"aLengthMenu": [
				[25, 50, -1],
				[25, 50, "Todo"]
			],
			"iDisplayLength": 25,
			"order": [
				[0, "desc"]
			],
			columnDefs: [
				{
					targets: 0,
					data: 'DIAS',
					name: 'DIAS',
					sortable: false,
					searchable: false,
					className: "celda-centrada",
					render: function(data, type, row) {
						return row.DIAS;
					}
				},
				{
					targets: 1,
					data: null,
					searchable: false,
					sortable: false,
					render: function(data, type, row) {
						var linea1 = "<span>Todos Prod.: " + row.FRECUENCIA_ATRASOS_TOTPROD + "</span>";
						var linea2 = "<br/><span>Sin Desc.: " + row.FRECUENCIA_ATRASOS_SINDSCTOS + "</span>";
						return linea1 + linea2;
					}
				},
				{
					targets: 2,
					data: 'F.NOMBRE_EJECUTIVO',
					name: 'F.NOMBRE_EJECUTIVO',
					render: function(data, type, row, meta) {
						var linea1 = '<span>' + row.NOMBRE_EJECUTIVO + '</span>';
						var linea2 = '<br/>';
						if(row.NOMBRE_JEFE!=null && row.NOMBRE_JEFE!=undefined){
							linea2 += '<span>' + row.NOMBRE_JEFE + '</span>';
						}
						return linea1 + linea2;
					}
				},
				{
					targets: 3,
					data: 'WICA.COD_UNICO',
					name: 'WICA.COD_UNICO',
					render: function(data, type, row) {
						var linea1 = 'CU: ' + row.COD_UNICO;
						var linea2 = '<br/>';
						linea2 += '<a target="_blank" href="{{route("infinity.alertascartera.detalle")}}?cu=' + row.COD_UNICO + '">' + row.NOMBRE_CLIENTE + '</a>';

						return linea1 + linea2;
					}
				},
				{
					targets: 4,
					data: 'SUMADOS',
					name: 'SUMADOS',
					searchable: false,
					sortable: false,
					render: function(data, type, row) {
						var linea0 = "<span class='hidden'>" + row.SUMADOS + "</span>";
						var linea1 = "<span>Total: : S/." + numeral(row.SALDO_TOT_IBK / 1000).format('0.0') + " M</span>";
						var linea2 = "<br/><span>Con Atraso: : S/." + numeral(row.SALDO_IBK_CON_ATRASO / 1000).format('0.0') + " M</span>";
						var linea3 = "<br/><span>No Vigente: S/." + numeral(row.SALDO_NO_VIGENTE / 1000).format('0.0') + " M </span>";
						return linea0 + linea1 + linea2 + linea3;
					}
				},
				{
					targets: 5,
					data:null,
					searchable: false,
					sortable:false,
					render: function(data, type, row) {
						var color1 = $('#semaforo-' + row.NIVEL_ALERTA).val();
						var color2 = $('#semaforo-' + row.NIVEL_ALERTA_ANTERIOR).val();
						var color3 = $('#semaforo-' + row.NIVEL_ALERTA_ANTERIOR_2).val();
						return '<i class="fa fa-circle fa-lg" style="padding-right : 3px; color:' + color3 + '"></i>' +
							'<i class="fa fa-circle fa-lg" style="padding-right : 3px; color:' + color2 + '"></i>' +
							'<i class="fa fa-circle fa-2x" style="color:' + color1 + '"></i>';
					}
				},
				{
					targets: 6,
					data: null,
					searchable: false,
					sortable:false,
					className: "celda-centrada",
					render: function(data, type, row) {
						var l1 = "<br/><span>ATRASO: " + row.GESTION + "</span>";
						var l2 = "<br/><span>VIGENTE: " + row.GESTION_VIG + "</span>";
						return l1+l2;
					}
				},
				{
					targets: 7,
					data: null,
					searchable: false,
					sortable:false,
					className: "celda-centrada",
					render: function(data, type, row, meta) {
						return row.FEC_COMPROMISO;
					}
				},
				{
					targets: 8,
					data: null,
					searchable: false,
					sortable:false,
					className: "celda-centrada",
					render: function(data, type, row, meta) {
						html = '<a target="_blank" href="{{route("infinity.me.cliente.conoceme")}}?cu=' + row.COD_UNICO + '"><i class="fa fa-user-plus" aria-hidden="true"></i> Conóceme</a><br />';
						if (row.CANT_POLI_CUALI == null || row.CANT_POLI_CUALI < 0) {
							html += ' <a target="_blank" href="{{route("infinity.me.cliente.visita")}}?cu=' + row.COD_UNICO + '"><i class="fa fa-home" aria-hidden="true"></i>Visita + Covid</a><br />';
						}
						return html;
					}
				},
				{
					targets: 9,
					data:null,
					searchable:false,
					sortable:false,
					className: "celda-centrada",
					render: function(data, type, row, meta) {
						var linea1 = row.ESTADO_APROBACION;
						return linea1
					}
				},
				{
					targets: 10,
					data:null,
					searchable:false,
					sortable: false,
					className: "celda-centrada",
					render: function(data, type, row, meta) {
						var linea1 = row.BLINDAJE;
						var res = '';
						if(linea1=='NO'){
							res = 'NO';
						}else if(linea1=='BLINDAJE'){
							res = '<i class="icony fa fa-shield" aria-hidden="true"></i>';
						}

						return res;
					}
				},
				{
					targets: 11,
					data: 'NOMBRE_CLIENTE',
					name: 'WICA.NOMBRE',
					visible: false,
					render: function(data, type, row, meta) {
						return row.NOMBRE_CLIENTE;
					}
				}
			]
		});
	}

	$(document).on("change", ".filtroDinamico", function() {
		call_chartSeguimiento();
		call_tablaSeguimiento();
		call_tablaCompromisos();
	});

	function funre4(numero) {
		var flotante = parseFloat(numero);
		var resultado = (Math.round(flotante * 10, 0) / 10);
		return resultado;
	}

	$(document).ready(function() {
		rol = "{{in_array(Auth::user()->ROL,array('20','21')) ? true :false}}";
		banca_twebarbol = "{{$twebarbol && $twebarbol->TIPO_BANCA ? $twebarbol->TIPO_BANCA : 'MEDIANA EMPRESA'}}";
		zonal_twebarbol = "{{$twebarbol && $twebarbol->NOMBRE_ZONAL ? $twebarbol->NOMBRE_ZONAL : null}}";
		jefatura_twebarbol = "{{$twebarbol && $twebarbol->NOMBRE_JEFE ? $twebarbol->NOMBRE_JEFE : null}}";
		ejecutivo_twebarbol = "{{$twebarbol && $twebarbol->ENCARGADO ? $twebarbol->ENCARGADO : null}}";
		registro = "{{$registro ? $registro :null}}";
		resumenVigente($("#Bancas").val(), $("#Zonales").val(), $("#Jefaturas").val(), $("#Ejecutivos").val());
		call_chartSeguimiento();
		//tablaLA($("#Bancas").val(), $("#Zonales").val(), $("#Jefaturas").val(), $("#Ejecutivos").val());
		call_tablaSeguimiento();
		call_tablaCompromisos();
	});
</script>
@stop