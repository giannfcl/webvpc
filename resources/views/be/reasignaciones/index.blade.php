@extends('Layouts.layout')

@section('js-libs')
<script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.es.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/chart.bundle.js') }}"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
@stop

@section('content')

<div class="ui-widget">
  <label for="nombre">Empresa: </label>
  <input id="nombre">
</div>

@stop

@section('js-scripts')
<script>
	$( function() {
		var NOMBRES_LEADS=[];
		$.ajax({
            type: "POST",
            data: {
                nombre: $("#nombre").val(),
            },
            async: false,
            url: APP_URL + 'be/reasignaciones/buscar',
            success: function (result) {
            	// console.log(result);
            	var NOMBRES_LEADS=[];
            	for (var i = 0; i < result.length; i++) {
            		// console.log(result[i]['NOMBRE']);
            		NOMBRES_LEADS.push(result[i]['NOMBRE']);
            	}
				console.log(NOMBRES_LEADS);
            }
        });

        $( "#nombre" ).autocomplete({
	      source: NOMBRES_LEADS
	    });
  } );
</script>
@stop