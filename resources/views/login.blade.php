@section('pageTitle', 'Bienvenida(o)')

@extends('Layouts.layoutlogin')

@section('content')

    <style> 
        .textoLogin {
           color: black;
           font-size: 16px;            
        }
    </style>

<form method="post" action="{{ route('login.attempt') }}" >
    <!-- <img src = "{{ URL::asset('img/vpconnect.png') }}" style="width: 100%" /> -->
    @if(Session::has('message'))
        <p class="alert alert-warning">{{ session('message') }}</p>
    @endif
    <div class="form-group">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input class="form-control textoLogin" placeholder="Registro de Usuario" type="text" name="registro">
    </div>
    <div class="form-group">
        <input class="form-control textoLogin" placeholder="Password" required="" type="password" name="password" autocomplete="off">
    </div>
    <div class="form-group">
        <button class="btn btn-default textoLogin" type="submit" style="font-weight: bold">Ingresar</button>
    </div>

    <div class="clearfix"></div>
    <div class="separator">
        <div>
            <p class="textoLogin"><a href="mailto:gcumpalumb@intercorp.com.pe">Contacto : Gianfranco Cumpa</a></p>
            <!-- <h4 class="textoLogin"> VP Comercial Interbank</h4>
            <p class="textoLogin">Contacto BPE: controlgestcom@intranet.ib</p>
            <p class="textoLogin">Contacto BE: gestioncomercial@intercorp.com.pe</p> -->
        </div>
    </div>


</form>
@stop