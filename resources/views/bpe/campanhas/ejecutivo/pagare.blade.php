<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Banca Pequeña Empresa</title>
    </head>



<body class="nav-md" style="font-size: 8pt;">
  <section id="identi" style="text-align: justify; ">
        <div style="padding-left:10px" class="row">
            <img style="width: 20%" src="{{ URL::asset('img/logoIBK.png') }}" alt="">
        </div>
        <div style=" text-align: center;padding: 5px;font-size: 10pt;">
            <div><strong>Banca Pequeña Empresa </strong></div>
            <div><strong>Pagaré</strong></div>
        </div>
        <div id="content">
            <div id="left">
                <div id="object1" style="text-align: justify;">Importe _______________________________________________________________</div>
            </div>

            <div id="right">
                <div id="object1">Vencimiento ___________________________________________</div>
            </div>
        </div>
        <br>
       <p style="text-align: justify;font-size: 7pt;">
        Pagaré/Pagaremos solidariamente a INTERBANK,a su orden o a quién esté endosado el presente título,en sus oficinas de esta ciudad o donde se presente este título para su cobro, la suma de _________________________________________________________, importe que expresamente declaro/declaramos adeudar a INTERBANK y por el cual me/nos obligo/obligamos a abonar intereses compensatorios a una tasa del ___________________________% , efectivo ___________________________ y durante el período de mora intereses compensatorios e intereses moratorios a las tasas ___________________________ , efectivo ___________________________ y ___________________________% efectivo ___________________________, respectivamente así como penalidades, seguros, gastos notariales, de cobranza judicial y extrajudicial, y en general los gastos y comisiones que pudiéramos adeudar derivados del crédito representado en este Pagaré, y que se pudieran generar desde la fecha de emisión del presente Pagaré hasta la cancelación total de la presente obligación, sin que sea necesario requerimiento alguno de pago para constituirme/constituirnos en mora, pues es entendido que ésta se producirá de modo automático por el solo hecho del vencimiento de este Pagaré.
        </p>
        <p style="text-align: justify;font-size: 7pt;">
        Expresamente acepto/aceptamos toda variación de las tasas de interés, dentro de los límites legales autorizados, las mismas que se aplicarán luego de la comunicación efectuada por INTERBANK conforme a ley.
        </p>
        <p style="text-align: justify;font-size: 7pt;">
        Asimismo,si a su vencimiento este Pagaré no fuese pagado,autorizo/autorizamos irrevocablemente a INTERBANK para que cobre el importe total adeudado por concepto de este Pagaré,incluidos intereses y demás conceptos derivados del mismo, afectando cualquiera de las cuentas, valores y depósitos en general,que mantuviera/mantuviéramos en INTERBANK.
        </p>
        <p style="text-align: justify; font-size: 7pt;" >
        Acepto/Aceptamos y doy/damos por válidas desde ahora todas las renovaciones y prórrogas totales o parciales que se anoten en este documento, aún cuando no estén suscritas por mi/nosotros.
        </p>
        <p style="text-align: justify; font-size: 7pt ;" >
        Este título no está sujeto a protesto por falta de pago, salvo lo dispuesto en el artículo 81.2 de la Ley 27287 y sus normas complementarias y/o modificatorias.
        </p>
        <p style="text-align: justify; font-size: 7pt ;" >
        Queda expresamente establecido que mi/nuestro domicilio es el indicado en el presente título. INTERBANK podrá entablar acción judicial donde lo tuviera por conveniente, confomre a ley.
        </p>
        <p style="text-align: justify; font-size: 7pt ;" >
        Declaro/Declaramos estar plenamente facultado/facultados para suscribir y emitir el presente Pagaré, asumiendo en caso contrario la responsabilidad civil y/o penal a que hubiera lugar.
        </p>
        <p style="text-align: justify; font-size: 7pt;" >
        Se deja constancia que la información proporcionada por el(los) emitente(s)en el presente documento, tiene el carácter de declaración jurada, de acuerdo con el artículo 179 de la Ley N° 26702.
        </p>
        <table  style="width:30%; margin-bottom:4px; font-size: 7pt;" align="right">
            <tr>
                <td class="variable"><span style="padding-right:1px"></span><strong>  </strong></td>
                <td class="space"> de</td>
                <td class="variable"><span style="padding-right:1px"></span><strong>  </strong></td>
                <td class="space"> de</td>
                <td class="variable"><span style="padding-right:1px"></span><strong>  </strong></td>
            </tr>
        </table>

        <!--<table border="0" width="170" align="right" cellspacing="0" cellpadding="0" style="font-size: 7pt ;">
        <tbody>
            <tr>
            <td>_______________</td>
            <td>de </td>
            <td>_______________</td>
            <td>de </td>
            <td>_______________</td>
            </tr>
        </tbody>
        </table> -->
           <br>
            <table style="width:100%; margin-bottom:5px; font-size: 6pt;">
                <tr>
                    <td class="variable"><span style="padding-right:1px">firma(s) </span><strong></strong></td>
                </tr>
            </table>
            <table style="width:100%; margin-bottom:5px; font-size: 6pt;">
                <tr>
                    <td>
                        <p style="margin-bottom: 0px;">(para persona jurídica: representante(s) legal(es) del cliente / para persona natural: cliente y cónyuge)</p>
                    </td>
                </tr>
            </table>
            <table  style="width:100%; margin-bottom:5px; font-size: 6pt;">
                <tr>
                    <td class="variable"><span style="padding-right:1px">razón social / nombre </span>
                    <strong>
                            @if(substr($acuerdos->NUM_RUC,0,2)==20)
                                {{$acuerdos->NOMBRE}}
                            @endif
                    </strong>
                </td>
                </tr>
            </table>
            <table  style="width:100%; margin-bottom:5px; font-size: 6pt;">
                <tr>
                    <td class="variable"><span style="padding-right:1px">d.o.i. </span>
                        <strong>
                            @if(substr($acuerdos->NUM_RUC,0,2)==20)
                                {{$acuerdos->NUM_RUC}}
                            @endif
                        </strong>
                    </td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">estado civil</span>
                        <strong>
                            {{--@if(substr($acuerdos->NUM_RUC,0,2)==10)
                                @if($acuerdos->RRLL_ESTADO_CIVIL == 1)
                                    Casado(a)
                                @endif

                                @if($acuerdos->RRLL_ESTADO_CIVIL == 2)
                                    Conviviente(a)
                                @endif

                                @if($acuerdos->RRLL_ESTADO_CIVIL == 3)
                                    Soltero
                                @endif

                                @if($acuerdos->RRLL_ESTADO_CIVIL == 4)
                                    Viudo(a)
                                @endif

                                 @if($acuerdos->RRLL_ESTADO_CIVIL == 5)
                                    Divorciado(a)
                                @endif

                                 @if($acuerdos->RRLL_ESTADO_CIVIL == 6)
                                    Separado
                                @endif
                            @endif --}}
                        </strong>
                    </td>
                </tr>
            </table>
            <table  style="width:100%; margin-bottom:5px; font-size: 6pt;">
                <tr>
                    <td class="variable"><span style="padding-right:1px">domicilio</span>
                        <strong>
                           <strong>
                                @if(substr($acuerdos->NUM_RUC,0,2)==20)
                                    {{isset($direccion[0]->DIRECCION)? $direccion[0]->DIRECCION:''}}
                                @endif
                            </strong>
                        </strong>
                    </td>
                </tr>
            </table>
            <table  style="width:100%; margin-bottom:5px; font-size: 6pt;">
                <tr>
                    <td class="variable"><span style="padding-right:1px; width:400px">nombre representante legal / cónyuge </span>
                         <strong>
                            @if(substr($acuerdos->NUM_RUC,0,2)==20)
                              {{$acuerdos->RRLL_APELLIDOS}}  {{$acuerdos->RRLL_NOMBRES}}
                            @endif
                        </strong>
                    </td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">d.o.i.</span>
                         <strong>
                            @if(substr($acuerdos->NUM_RUC,0,2)==20)
                                {{$acuerdos->RRLL_DOCUMENTO}}
                            @endif
                        </strong>
                    </td>
                </tr>
            </table>
            <table  style="width:100%; margin-bottom:5px; font-size: 6pt;">
                <tr>
                    <td class="variable"><span style="padding-right:1px; width:400px">nombre representante legal</span>
                        <strong>
                           @if(substr($acuerdos->NUM_RUC,0,2)==20)
                                {{$acuerdos->RRLL_APELLIDOS_2}} {{$acuerdos->RRLL_NOMBRES_2}}
                            @endif
                         </strong>
                    </td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">d.o.i.</span>
                     <strong>
                         @if(substr($acuerdos->NUM_RUC,0,2)==20)
                            {{$acuerdos->RRLL_DOCUMENTO_2}}
                         @endif
                        </strong>
                    </td>
                </tr>
            </table>
        <br>
        <span style="font-size: 7pt ;"><strong>AVAL</strong></span><br><br>
        <p style="text-align: justify; font-size: 7pt ;" >
        Me constituyo en Aval del Emitente de este pagaré, por las obligaciones contraídas en este documento obligándome al pago de la cantidad adeudada, intereses compensatorios y moratorios, así como comisiones, penalidades, seguros, gastos notariales, de cobranza judicial y extrajudicial, que se pudieran devengar desde la fecha de emisión hasta la cancelación total de la presente obligación. Autorizo irrevocablemente a INTERBANK, para que efectúe el débito del importe total adeudado por concepto de este pagaré, incluidos intereses y demás conceptos derivados del mismo, en cualquiera de las cuentas, valores y depósitos en general que mantuviera en INTERBANK.
        </p>
        <p style="text-align: justify; font-size: 7pt ;" >
        Reconozco que en mi condición de avalista, me obligo de igual modo que el Emitente del título, y mi responsabilidad subsistirá aunque la obligación causal del presente título valor fuere nula; excepto si se trata de defecto de forma de dicho título.
        </p>
        <p style="text-align: justify; font-size: 7pt ;" >
        De la misma forma,acepto que en mi condición de aval, ante un incumplimiento de pago de la obligación garantizada, no podré oponer al tenedor del título valor los medios de defensa personales de la persona a quien avalo.
        </p>
        <p style="text-align: justify; font-size: 7pt ;" >
        El presente constituye un Aval indefinido por lo que no será necesaria mi intervención en las renovaciones que acuerde el Deudor Principal y el tenedor del título, aceptando desde ahora las prórrogas y/o renovaciones que se concedan a mi avalado aun cuando no estén suscritas por él y sin que sea necesario me sean comunicadas.
        </p>
        <p style="text-align: justify; font-size: 7pt ;" >
        Acepto desde ahora que el título no está sujeto a protesto por falta de pago, salvo lo dispuesto en el artículo 81.2 de la Ley 27287 y sus normas complementarias y modificatorias.
        </p>
        <p style="text-align: justify; font-size: 7pt ;" >
        Queda expresamente establecido que mi domicilio es el indicado en el presente título.INTERBANK podrá entablar acción judicial donde lo tuviere conveniente conforme a Ley.
        Declaro estar plenamente facultado para avalar el presente pagaré, asumiendo en caso contrario la responsabilidad civil y/o penal que hubiere lugar.
        </p>
        <p style="text-align: justify; font-size: 7pt ;" >
        Se deja constancia que la información proporcionada por el aval en el presente documento, tiene carácter de declaración jurada, de acuerdo con el artículo 179 de la Ley N° 26702.
        </p>
        <table  style="width:30%; margin-bottom:4px; font-size: 6pt;" align="right">
            <tr>
                <td class="variable"><span style="padding-right:1px"></span><strong>  </strong></td>
                <td class="space"> de</td>
                <td class="variable"><span style="padding-right:1px"></span><strong>  </strong></td>
                <td class="space"> de</td>
                <td class="variable"><span style="padding-right:1px"></span><strong>  </strong></td>
            </tr>
        </table>
        </p>
    <br>
            <table style="width:100%; margin-bottom:5px; font-size: 6pt;">
                <tr>
                    <td class="variable"><span style="padding-right:1px">firma(s) </span><strong>  </strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">firma(s) </span><strong>  </strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">firma(s) </span><strong>  </strong></td>
                </tr>
                <tr>
                    <td>
                        <p style="margin-bottom: 0px;">(aval: persona natural / persona jurídica)</p>
                    </td>
                    <td class="space"></td>
                    <td>
                        <p style="margin-bottom: 0px;">(aval: persona natural / persona jurídica)</p>
                    </td>
                    <td class="space"></td>
                    <td>
                        <p style="margin-bottom: 0px;">(aval: persona natural / persona jurídica)</p>
                    </td>
                </tr>
            </table>
            <table style="width:100%; margin-bottom:5px; font-size: 6pt;">
                <tr>
                    <td class="variable"><span style="padding-right:1px">razón soc. / nombre </span><strong style="font-size: 6.5px">{{substr($acuerdos->NUM_RUC,0,2)==20 && isset($avales[0]['NOMBRE'])? $avales[0]['NOMBRE']:'' }}</strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">razón soc. / nombre </span><strong style="font-size: 6.5px">{{substr($acuerdos->NUM_RUC,0,2)==20 && isset($avales[1]['NOMBRE'])? $avales[1]['NOMBRE']:'' }}</strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">razón soc. / nombre </span><strong style="font-size: 6.5px">{{substr($acuerdos->NUM_RUC,0,2)==20 && isset($avales[2]['NOMBRE'])? $avales[2]['NOMBRE']:'' }}</strong></td>
                </tr>
            </table>
            <table style="width:100%; margin-bottom:5px; font-size: 6pt;">
                <tr>
                    <td class="variable"><span style="padding-right:1px">d.o.i.</span><strong style="font-size: 6.5px">{{substr($acuerdos->NUM_RUC,0,2)==20 && isset($avales[0]['NUM_DOC_AVAL'])? $avales[0]['NUM_DOC_AVAL']:'' }}</strong></td>
                    <td class="space"></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">estado civil </span><strong style="font-size: 6.5px">{{substr($acuerdos->NUM_RUC,0,2)==20 && isset($avales[0]['ESTADO_CIVIL'])? $avales[0]['ESTADO_CIVIL']:'' }}</strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">d.o.i.</span><strong style="font-size: 6.5px">{{substr($acuerdos->NUM_RUC,0,2)==20 && isset($avales[1]['NUM_DOC_AVAL'])? $avales[1]['NUM_DOC_AVAL']:'' }}</strong></td>
                    <td class="space"></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">estado civil</span><strong style="font-size: 6.5px">{{substr($acuerdos->NUM_RUC,0,2)==20 && isset($avales[1]['ESTADO_CIVIL'])? $avales[1]['ESTADO_CIVIL']:'' }}</strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">d.o.i.</span><strong style="font-size: 6.5px">{{substr($acuerdos->NUM_RUC,0,2)==20 && isset($avales[2]['NUM_DOC_AVAL'])? $avales[2]['NUM_DOC_AVAL']:'' }}</strong></td>
                    <td class="space"></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">estado civil</span><strong style="font-size: 6.5px">{{substr($acuerdos->NUM_RUC,0,2)==20 && isset($avales[2]['ESTADO_CIVIL'])? $avales[2]['ESTADO_CIVIL']:'' }}</strong></td>
                </tr>
            </table>
            <table style="width:100%; margin-bottom:5px; font-size: 6pt;">
                <tr>
                    <td class="variable"><span style="padding-right:1px">domicilio </span><strong style="font-size: 6.5px">{{substr($acuerdos->NUM_RUC,0,2)==20 && isset($avales[0]['DIRECCION'])? $avales[0]['DIRECCION']:'' }}</strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">domicilio </span><strong style="font-size: 6.5px">{{substr($acuerdos->NUM_RUC,0,2)==20 && isset($avales[1]['DIRECCION'])? $avales[1]['DIRECCION']:'' }}</strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">domicilio </span><strong style="font-size: 6.5px">{{substr($acuerdos->NUM_RUC,0,2)==20 && isset($avales[2]['DIRECCION'])? $avales[2]['DIRECCION']:'' }}</strong></td>
                </tr>
            </table>
            <table style="width:100%; margin-bottom:5px; font-size: 6pt;">
                <tr>
                    <td class="variable"><span style="padding-right:1px">rep. legal / cónyuge </span><strong> </strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">rep. legal / cónyuge </span><strong></strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">rep. legal / cónyuge </span><strong>  </strong></td>
                </tr>
            </table>
            <table style="width:100%; margin-bottom:5px; font-size: 6pt;">
                <tr>
                    <td class="variable"><span style="padding-right:1px">d.o.i. </span><strong>  </strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">d.o.i. </span><strong> </strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">d.o.i. </span><strong>  </strong></td>
                </tr>
            </table>
            <table style="width:100%; margin-bottom:5px; font-size: 6pt;">
                <tr>
                    <td class="variable"><span style="padding-right:1px">rep. Legal </span><strong>  </strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">rep. Legal </span><strong>  </strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">rep. Legal </span><strong>  </strong></td>
                </tr>
            </table>
            <table style="width:100%; margin-bottom:5px; font-size: 6pt;">
                <tr>
                    <td class="variable"><span style="padding-right:1px">d.o.i. </span><strong> </strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">d.o.i. </span><strong></strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">d.o.i. </span><strong>  </strong></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style="text-align: right;">
                      <div style="margin-bottom: 0px; font-size: 6px">COPIA-BANCO</div>
                    </td>
                </tr>
            </table>
        </section>
     </body>
</html>


<style>
   table{
    border-collapse:collapse;

    }
   body{

        margin:0;
        font-family:-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";
        font-size:1rem;
        font-weight:400;line-height:1.5;color:#212529;text-align:left;
        background-color:#fff;
        margin-left: 90px;
        margin-right: 90px;
        margin-top: 8px;
        margin-bottom: 8px;
        margin-left: 0px;
        margin-right: 0px;
        margin-top: -30px;
        margin-bottom: -30px;
        }

    p {
        margin-top:0.10;
        margin-bottom:0.25em;
        /*margin-bottom: 0.25em; */

        word-spacing: 0.25em;
        line-height: 9px;
    }

    .page_break{
        page-break-before: always;
    }

    .underline{
        border-bottom-style: solid !important;
        border-bottom-width: 0.05px !important;
        padding-left:5px;
        padding-right: 5px;
    }

    .variable{
        border-bottom-style: solid;
        border-left-style: solid;
        border-right-style: solid;
        border-width: 0.5px;
        padding-left: 10px;
        padding-right: 10px;
    }

    .space{
        width:10px;
    }

    #identi{
        margin: 3px 0px 2px;
    };

    #content {
        overflow:auto;
        width: 840px;
        background: gray;
    }
    #left  { float:left;  }
    #right { float:right; }

</style>
