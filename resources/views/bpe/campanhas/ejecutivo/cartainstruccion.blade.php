<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Carta de instrucción para cancelación de 2 o más créditos</title>

    </head>
    <style>
        .page_break{
            page-break-before: always;
        }

        .underline{
            border-bottom-style: solid !important;
            border-bottom-width: 0.05px !important;
            margin-bottom: 5px;
            padding-left:5px;
            padding-right: 5px;
        }

        .variable{
                border-bottom-style: solid;
            border-left-style: solid;
            border-right-style: solid;
            border-width: 0.5px;
            padding-left: 10px;
            margin-bottom:0;
            padding-right: 10px;
        }

        .space{
            width:10px;
        }


        body
            {

            margin:0;
            font-family:-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";
            font-size:1rem;
            font-weight:400;line-height:1.5;color:#212529;text-align:left;
            background-color:#fff;
            margin-left: 90px;
            margin-right: 90px;
            margin-top: 8px;
            margin-bottom: 8px;
            margin-left: 0px;
            margin-right: 0px;
            margin-top: -30px;
            margin-bottom: -30px;
        }

        p {
            margin-top:0.25;
            margin-bottom:0.25em;
            margin-bottom: 0.25em;

            /*word-spacing: 0.3em;
            line-height: 9px;*/
        }

        table{
        border-collapse:collapse;

        }
        .tablaTexto table {
            table-layout: auto !important;
            width: 100%;
        }
        .tablaTexto th, .myclass td, .myclass thead th, .myclass tbody td, .myclass tfoot td, .myclass tfoot th {
            width: auto !important;
            font-size: 6pt !important;
        }
    </style>
    <body class="nav-md" style="font-size: 8.5pt; margin: 0 !important;">
        <div style="
            max-width: 100vw;
            padding: 50px 100px 0px 100px;">
            <div class="row">
                <img style="width: 25%" src="{{ URL::asset('img/logoIBK.png') }}">
            </div>
            <br><br><br><br>
            <div style="text-align: right;">Lima, {{$fecha['dia']}} de {{$fecha['mes']}} de {{$fecha['anio']}}</div><br>
            <div style="text-align: left;">
                <p>Sr(es).</p>
                <p>Banco Internacional del Perú S.A.A-  Interbank</p>
                <p>Presente.-</p>
            </div>
            <br>
            <div style="text-align: right;">Referencia: Cancelación de créditos.</div><br>
            <div style="text-align: justify;">
                Mediante la presente tenemos el gusto de saludarlos y a su vez solicitarle se sirvan realizar la cancelación total de los siguientes créditos:<br>
            </div>
            <br>
            <div style="padding-left: 50px">
                @if (is_array($productos) || is_object($productos))
                    @foreach($productos as $producto)
                    <li>Crédito N° <i class="underline">{{$producto->COD_CREDITO}}</i></li>
                    @endforeach
                @endif
            </div>
            <br><br>
            <div style="text-align: justify;">
                Mediante la presente declaro conocer que la cancelación de los créditos antes mencionados, se realizará dentro de los 15 días de haber entregado la presente comunicación, asumiendo hasta esa fecha los intereses que pudieran generarse. <br>

                Sin otro particular. <br>

                Atentamente,
            </div>
        </div>
        <br><br>

        <div style="width:600px; padding-left:100px; text-align:left;font-size:6.5pt;">
        ________________________________________________________________________________
        <p>Nombre del cliente o Razón Social:
          {{$datos->NOMBRE}}
        </p>
        <p>DNI o RUC:
          {{substr($datos->NUM_RUC,0,2)==20 ? $datos->NUM_RUC : $datos->RRLL_DOCUMENTO}}
        </p>
        <p>Nombre del Representante Legal:
          @if(substr($datos->NUM_RUC,0,2)==20)
            {{$datos->RRLL_APELLIDOS}} {{$datos->RRLL_NOMBRES}}
          @endif
        </p>
        </div>
        <br>

        <div style="width:600px; padding-left:100px; text-align:left;font-size:6.5pt;">
        ________________________________________________________________________________
        <p>Nombre del cliente o Razón Social:
            @if(substr($datos->NUM_RUC,0,2)==20 && $datos->RRLL_DOCUMENTO_2)
                {{$datos->NOMBRE}}
            @endif
        </p>
        <p>DNI o RUC:
            @if(substr($datos->NUM_RUC,0,2)==20 && $datos->RRLL_DOCUMENTO_2)
                {{$datos->NUM_RUC}}
            @endif
        </p>
        <p>Nombre del Representante Legal:
            @if(substr($datos->NUM_RUC,0,2)==20 && $datos->RRLL_DOCUMENTO_2)
                {{$datos->RRLL_APELLIDOS_2}} {{$datos->RRLL_NOMBRES_2}}
            @endif
        </p>
        </div>



    </body>
</html>
