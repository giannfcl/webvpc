<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>DECLARACIÓN JURADA FONDO CRECER PN Y PJ</title>

    </head>
    <style>
        .page_break{
            page-break-before: always;
        }

        .underline{
            border-bottom-style: solid !important;
            border-bottom-width: 0.05px !important;
            margin-bottom: 5px;
            padding-left:5px;
            padding-right: 5px;
        }

        .variable{
                border-bottom-style: solid;
            border-left-style: solid;
            border-right-style: solid;
            border-width: 0.5px;
            padding-left: 10px;
            margin-bottom:0;
            padding-right: 10px;
        }

        .space{
            width:10px;
        }


        body
            {

            margin:0;
            font-family:-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";
            font-size:1rem;
            font-weight:400;line-height:1.5;color:#212529;text-align:left;
            background-color:#fff;
            margin-left: 90px;
            margin-right: 90px;
            margin-top: 8px;
            margin-bottom: 8px;
            margin-left: 0px;
            margin-right: 0px;
            margin-top: -30px;
            margin-bottom: -30px;
        }

        p {
            margin-top:0.25;
            margin-bottom:0.25em;
            margin-bottom: 0.25em;

            /*word-spacing: 0.3em;
            line-height: 9px;*/
        }

        table{
        border-collapse:collapse;

        }
        .tablaTexto table {
            table-layout: auto !important;
            width: 100%;
        }
        .tablaTexto th, .myclass td, .myclass thead th, .myclass tbody td, .myclass tfoot td, .myclass tfoot th {
            width: auto !important;
            font-size: 6pt !important;
        }
    </style>
    <body class="nav-md" style="font-size: 8.5pt; margin: 0 !important;">
        <div style="
            max-width: 100vw;
            padding: 0px 100px 0px 100px;">
            <div style="padding-left:20px" class="row">
                <img style="width: 15%" src="{{ URL::asset('img/logoIBK.png') }}" alt="">
            </div>
            <div style=" text-align: center; font-size: 8pt;margin-bottom:5px;">
                <strong>Declaración Jurada</strong>
            </div>
            <p style="margin-bottom:0;text-align: justify;"><span class="underline"> {{$datos->NOMBRE}}</span>(en adelante el “Cliente”) me comprometo y cumplo con declarar bajo juramento lo siguiente:</p><br>
            <div style="text-align: justify;">
                a.  Que a la fecha de la presente Declaración Jurada no me encuentro inhabilitado para contratar con el Estado en el marco de la normativa de contratación pública vigente.<br>

                b.  Que a la fecha de la presente Declaración Jurada no me encuentro en ningún procedimiento concursal y/o no he sido declarado insolvente por la autoridad competente.<br>

                c.  Que a la fecha de la presente Declaración Jurada no he sido beneficiario final de coberturas cuyas operaciones de crédito hayan sido honradas con recursos de Fondo Crecer.<br>

                d.  Que cumplo con los criterios de elegibilidad referidos a la clasificación en la Central de Riesgos de la SBS, de conformidad con lo dispuesto en el Reglamento Operativo de Fondo Crecer, establecido por Decreto Supremo N° 007-2019- EF.<br>

                e.  Que a la fecha de la presente Declaración Jurada no participo y me obligo a no participar durante la vigencia del Crédito en actividades económicas, de producción o comercio de cualquier producto o actividad que se considere ilegal bajo las leyes o la normativa del país o bajo convenios y acuerdos internacionales ratificados, incluyendo las convenciones y/o legislación relativa a la protección de los recursos de biodiversidad o patrimonio cultura, señalados en el Anexo 4 del Manual Operativo de Garantías del Fondo Crecer, como son: Existencia de un incumplimiento de los principios y derechos fundamentales de los trabajadores, Sectores con percepción social negativa, Producción o comercio de productos peligrosos para la salud humana y de los ecosistemas, Actividades que vulneren la salud de los ecosistemas naturales, Actividades que atenten contra las voluntades de la población, Actividades que atenten contra el patrimonio.<br>
            </div>
        </div><br>
        <div style="max-width: 100vw;padding: 0px 100px 0px 95px;text-align: justify;">
            En el caso que el crédito solicitado sea garantizado por una Entidad Estatal,  mis datos personales brindados a Interbank podrán ser transferidos a dicha empresa para las finalidades pertinentes. <br><br>
            Las declaraciones y compromisos antes descritos se mantendrán vigentes desde la fecha de esta Declaración Jurada y hasta que se haya cancelado la totalidad del Crédito. El Cliente faculta a Interbank a verificar en cualquier momento durante la vigencia del Crédito la veracidad y/o exactitud de estas declaraciones y de verificarse la falsedad y/o inexactitud de alguna de ellas, así como el incumplimiento de alguno de los compromisos asumidos, se producirá una causal de incumplimiento, quedando Interbank facultado a dar por vencidos todos los plazos y a proceder a la aceleración del Crédito de forma automática, sin necesidad de requerimiento previo o intimación en mora, así como a la ejecución de los colaterales, títulos valores y/o garantías personales otorgadas para garantizar el cumplimiento de pago del Crédito. El Cliente declara conocer y aceptar que Interbank estará facultado para tal aceleración y no podrá otorgar dispensas a ningún incumplimiento, procediendo a completar el Pagaré, por haberse configurado los supuestos d) y e) del Acuerdo de Llenado de Pagaré.  <br><br>
            El Cliente adjunta a la presente Declaración Jurada, (i) Solicitud de Crédito, Pagaré y (ii) Acuerdo de Llenado de Pagaré, debidamente suscritos por apoderados facultados que, en conjunto con los demás documentos requeridos por Interbank conforman los documentos de crédito.
        </div>

        <div style="position: absolute; bottom: -30px; width:600px; padding-left:100px; text-align:left;font-ssize:6.5pt;">
        ________________________________________________________________________________
        <p>Solo aplica para cliente Persona Natural con Negocio.</p>
        </div>

        <div style="position: absolute; bottom: -40px; width:600px; padding-right:100px; text-align:right;font-size:6.5pt;">
        <p>Pag 1</p>
        </div>

        <div class="page_break"></div>

        <div style="
            max-width: 100vw;
            padding: 0px 100px 0px 100px;">
            <div style="text-align: justify;">
                Se deja expresa constancia que la presente declaración, se efectúa en tenor a lo dispuesto por el artículo 179° de la Ley General del Sistema Financiero y del Sistema de Seguros y Orgánica de la Superintendencia de Banca y Seguros N° 26702. <br>

            <div style="text-align: right;">{{$fecha['dia']}} de {{$fecha['mes']}} del {{$fecha['anio']}}</div>
            </div><br>

            <!------------------------------------------------------------------------------>
            <div style="text-align: justify;">
                Si eres <strong>Persona Jurídica</strong>, firma aquí:
            </div>
            <div style="font-size:8pt; width:100%; border-style: solid; border-width:0.5px;padding:5px;padding-top:5px; padding-bottom:10px;">
                <table>
                    <tr>
                        <td>Los abajo firmantes, representantes legales de</td>
                        <td colspan="2" style="border-bottom-style:solid;border-bottom-width:0.5px;width: 250px">&nbsp;{{substr($datos->NUM_RUC,0,2)==20 ? $datos->NOMBRE : ''}}</td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td>con RUC No.</td>
                        <td style="border-bottom-style:solid;border-bottom-width:0.5px;width: 330px">&nbsp;{{substr($datos->NUM_RUC,0,2)==20 ? $datos->NUM_RUC : ''}}</td>
                        <td>con domicilio en</td>
                    </tr>
                </table>
                <?php
                  $tamaño='';
                  if (count($direccion)>0) {
                    if (strlen($direccion[0]->DIRECCION)>80) {
                      $tamaño = 'font-size:6px';
                    }
                  }
                ?>
                <table>
                    <tr>
                        $tamaño = font-size:6px;
                        <td colspan="2" style="border-bottom-style:solid;border-bottom-width:0.5px;width: 310px;{{$tamaño}}">&nbsp;{{substr($datos->NUM_RUC,0,2)==20 && count($direccion)>0 ? $direccion[0]->DIRECCION : ''}}</td>
                        <td>, con facultades suficientes para</td>
                    </tr>
                    <tr>
                        <td colspan="3">suscribir el presente Acuerdo de Crédito - Solicitud de Desembolso y Declaración Jurada, firmamos</td>
                    </tr>
                    <tr>
                        <td colspan="3">la misma en señal de conformidad.</td>
                    </tr>
                </table><br>

                <table>
                    <tr>
                        <td>Firma:</td>
                        <td style="border-bottom-style:solid;border-bottom-width:0.5px;width: 200px">&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>Firma:</td>
                        <td style="border-bottom-style:solid;border-bottom-width:0.5px;width: 200px">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Nombre:</td>
                        <td style="border-bottom-style:solid;border-bottom-width:0.5px;width: 200px">{{substr($datos->NUM_RUC,0,2)==20 ? $datos->RRLL_APELLIDOS.' '.$datos->RRLL_NOMBRES : ''}}</td>
                        <td>&nbsp;</td>
                        <td>Nombre:</td>
                        <td style="border-bottom-style:solid;border-bottom-width:0.5px;width: 200px">{{substr($datos->NUM_RUC,0,2)==20 ? $datos->RRLL_APELLIDOS_2.' '.$datos->RRLL_NOMBRES_2 : ''}}</td>
                    </tr>
                    <tr>
                        <td>DNI No:</td>
                        <td style="border-bottom-style:solid;border-bottom-width:0.5px;width: 200px">{{substr($datos->NUM_RUC,0,2)==20 ? $datos->RRLL_DOCUMENTO : ''}}</td>
                        <td>&nbsp;</td>
                        <td>DNI No:</td>
                        <td style="border-bottom-style:solid;border-bottom-width:0.5px;width: 200px">{{substr($datos->NUM_RUC,0,2)==20 ? $datos->RRLL_DOCUMENTO_2 : ''}}</td>
                    </tr>
                    <tr>
                        <td>Cargo:</td>
                        <td style="border-bottom-style:solid;border-bottom-width:0.5px;width: 200px">{{substr($datos->NUM_RUC,0,2)==20 ? 'Representante Legal' : ''}}</td>
                        <td>&nbsp;</td>
                        <td>Cargo:</td>
                        <td style="border-bottom-style:solid;border-bottom-width:0.5px;width: 200px">{{substr($datos->NUM_RUC,0,2)==20  && strlen($datos->RRLL_DOCUMENTO_2) > 0 ? 'Representante Legal' : ''}}</td>
                    </tr>
                </table>
            </div><br>

            <!------------------------------------------------------------------------------>
            <div style="text-align: justify;">
                Si eres <strong>Persona Natural con Negocio</strong>, firma aquí:
            </div>
            <div style="font-size:8pt; width:100%; border-style: solid; border-width:0.5px;padding:5px;padding-top:5px; padding-bottom:10px;">
                <table>
                    <tr>
                        <td>Yo, </td>
                        <td style="border-bottom-style:solid;border-bottom-width:0.5px;width: 300px">&nbsp;@if(substr($datos->NUM_RUC,0,2)==10) {{ $datos->NOMBRE}} @endif</td>
                        <td>identificado con Documento de</td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td>Identidad N°</td>
                        <td style="border-bottom-style:solid;border-bottom-width:0.5px;width: 100px">&nbsp;@if(substr($datos->NUM_RUC,0,2)==10) {{ $datos->RRLL_DOCUMENTO }} @endif</td>
                        <td>de estado civil </td>
                        <td style="border-bottom-style:solid;border-bottom-width:0.5px;width: 100px">&nbsp;
                                @if(substr($datos->NUM_RUC,0,2)==10)
                                    @if($datos->RRLL_ESTADO_CIVIL == 1)
                                        Casado(a)
                                    @endif

                                    @if($datos->RRLL_ESTADO_CIVIL == 2)
                                        Conviviente(a)
                                    @endif

                                    @if($datos->RRLL_ESTADO_CIVIL == 3)
                                        Soltero
                                    @endif

                                    @if($datos->RRLL_ESTADO_CIVIL == 4)
                                        Viudo(a)
                                    @endif

                                    @if($datos->RRLL_ESTADO_CIVIL == 5)
                                        Divorciado(a)
                                    @endif

                                    @if($datos->RRLL_ESTADO_CIVIL == 6)
                                        Separado
                                    @endif
                                @endif
                        </td>
                        <td>con domicilio en</td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td colspan="2" style="border-bottom-style:solid;border-bottom-width:0.5px;width: 100%; font-size: 6.5pt">&nbsp;
                            @if(substr($datos->NUM_RUC,0,2)==10)
                                {{isset($direccion[0]->DIRECCION)? $direccion[0]->DIRECCION:''}}
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">, firmo la misma en señal de conformidad.</td>
                    </tr>
                </table><br>
                <table>
                    <tr>
                        <td>Firma Titular:</td>
                        <td style="border-bottom-style:solid;border-bottom-width:0.5px;width: 200px">&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>Firma Cónyuge:</td>
                        <td style="border-bottom-style:solid;border-bottom-width:0.5px;width: 200px">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Nombre:</td>
                        <td style="border-bottom-style:solid;border-bottom-width:0.5px;width: 200px">{{substr($datos->NUM_RUC,0,2)==10 ? $datos->RRLL_APELLIDOS.' '.$datos->RRLL_NOMBRES : ''}}</td>
                        <td>&nbsp;</td>
                        <td>Nombre:</td>
                        <td style="border-bottom-style:solid;border-bottom-width:0.5px;width: 200px">{{substr($datos->NUM_RUC,0,2)==10 && in_array($datos->RRLL_ESTADO_CIVIL,[1,2])? $datos->RRLL_APELLIDOS_2.' '.$datos->RRLL_NOMBRES_2 : ''}}</td>
                    </tr>
                    <tr>
                        <td>DNI No:</td>
                        <td style="border-bottom-style:solid;border-bottom-width:0.5px;width: 200px">{{substr($datos->NUM_RUC,0,2)==10 ? $datos->RRLL_DOCUMENTO : ''}}</td>
                        <td>&nbsp;</td>
                        <td>DNI No:</td>
                        <td style="border-bottom-style:solid;border-bottom-width:0.5px;width: 200px">{{substr($datos->NUM_RUC,0,2)==10 && in_array($datos->RRLL_ESTADO_CIVIL,[1,2]) ? $datos->RRLL_DOCUMENTO_2 : ''}}</td>
                    </tr>
                </table>
            </div><br>

            <!------------------------------------------------------------------------------>
            <div style="text-align: justify;">
                Aceptado por <strong>INTERBANK</strong>:
            </div>
            <div style="font-size:8pt; width:100%; border-style: solid; border-width:0.5px;padding:5px;padding-top:5px; padding-bottom:10px;">
                <p style="text-align: justify;">Los abajo firmantes, representantes legales de Banco Internacional del Perú S.A.A-Interbank, con RUC No. 20100053455, con facultades suficientes para suscribir el presente documento, según poderes debidamente inscritos en la Partida No.11009129, del Registro de Personas Jurídicas de Lima, firmamos el mismo en señal de aceptación, con lo cual queda debidamente formalizado el presente Acuerdo de Crédito – Solicitud de Desembolso y Declaración Jurada</p> <br>
                <table>
                    <tr>
                        <td>Firma:</td>
                        <td style="border-bottom-style:solid;border-bottom-width:0.5px;width: 200px">&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>Firma:</td>
                        <td style="border-bottom-style:solid;border-bottom-width:0.5px;width: 200px">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Nombre:</td>
                        <td style="border-bottom-style:solid;border-bottom-width:0.5px;width: 200px">&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>Nombre:</td>
                        <td style="border-bottom-style:solid;border-bottom-width:0.5px;width: 200px">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>DNI No:</td>
                        <td style="border-bottom-style:solid;border-bottom-width:0.5px;width: 200px">&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>DNI No:</td>
                        <td style="border-bottom-style:solid;border-bottom-width:0.5px;width: 200px">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Cargo:</td>
                        <td style="border-bottom-style:solid;border-bottom-width:0.5px;width: 200px">&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>Cargo:</td>
                        <td style="border-bottom-style:solid;border-bottom-width:0.5px;width: 200px">&nbsp;</td>
                    </tr>
                </table>
            </div>

            <div style="position: absolute; bottom: -40px; width:600px; padding-right:100px; text-align:right;font-size:6.5pt;">
            <p>Pag 2</p>
            </div>
        </div>


    </body>
</html>
