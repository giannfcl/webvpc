@extends('Layouts.layout')

@section('js-libs')


@stop

@section('content')

@section('pageTitle', 'Empresas')

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Búsqueda</h2>
        <ul class="nav navbar-right panel_toolbox">
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <form action="" class="form-horizontal" method="GET">
            <div class="row">
                <div class="form-group col-md-4">
                    <label for="" class="control-label col-md-4">Documento:</label>
                    <div class="col-md-8">
                        <input class="form-control" name="documento_b" onkeypress="return inputLimiter(event,'custom')" value="{{ $busqueda['documento'] ? $busqueda['documento'] :''}}">
                    </div>
                </div>
                <div class="form-group col-md-4">
                	<button type="submit" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i> Buscar</button>
                </div>
            </div>
        </form>
    </div>
</div>
</div>
</div>

<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
      		<div class="x_title">
	      		<h2>Lista</h2>
	        	<ul class="nav navbar-right panel_toolbox">
	        	</ul>
	        	<div class="clearfix"></div>
    		</div>
			<div>
				<a id="nuevaempresa">
					<button class="btn btn-primary"><i class="fa fa-plus" ></i>Agregar</button>
				</a>
			</div>
    		<div class="x_content">
    			<table class="table table-striped jambo_table">
    				<thead >
    					<tr class="headings">
    						<th>TipoDoc</th>
    						<th>NumDoc</th>
    						<th>Teléfonos</th>
    						<th>Acciones</th>
    					</tr>
    				</thead>
    				<tbody>
    				@if(count($todos)>0)
	    				@foreach($todos as $datos)
	    				<tr>
	    					<td>{{$datos->TIPO_DOC}}</td>
	    					<td>{{$datos->NUM_DOC}}</td>
	    					<td>{{$datos->NUMEROS}}</td>
	    					<td documento="{{$datos->NUM_DOC}}" tipodoc="{{$datos->TD}}">
	    						<a id="nuevotelefono">
	    							<i class="fa fa-plus" data-toggle="tooltip" title="" data-placement="bottom"  aria-hidden="true"  data-original-title="Teléfonos" ></i>
	    						</a>
	    					</td>
	    				</tr>
	    				@endforeach
	                @else
	                <tr>
	                    <td colspan="4">No se encontraron resultados</td>
	                </tr>
	                @endif
    				</tbody>
    			</table>
    		{{ $todos->appends(array_merge($busqueda,$orden))->links()}}
    		</div>
    	</div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modalNuevoTelefono">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Nuevo Telefono</h4>
            </div>
            <form id="frmNuevoTelefono" class="form-horizontal form-label-left" action="{{ route('bpe.blacklist.nuevotelefonos') }}">
                <div class="modal-body">
                	<div id="tipodocumento">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">TIPO DOCUMENTO:</label>
                        <div class="input-group col-md-9 col-sm-9 col-xs-12">
                            <select class="form-control" name="tipodoc" id="tipodoc" readonly="" required>
                            	<option value="1">DNI</option>
                            	<option value="2">RUC</option>
                            </select>
                        </div>
                    </div><br>
                    <div>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">DOCUMENTO:</label>
                        <div class="input-group col-md-9 col-sm-9 col-xs-12">
                            <input class="form-control" name="documento" id="documento" onkeypress="return inputLimiter(event,'custom')" minlength="8" maxlength="11" readonly="" required>
                        </div>
                    </div>
                	<div>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">NUMERO:</label>
                        <div class="input-group col-md-9 col-sm-9 col-xs-12">
                            <input class="form-control" name="numero" id="numero" onkeypress="return inputLimiter(event,'custom')" minlength="6" maxlength="10" required>
                        </div>
                    </div>
                    <div>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">MOTIVO:</label>
                        <div class="input-group col-md-9 col-sm-9 col-xs-12">
                            <input class="form-control" name="motivo" value="SOLICITUD CLIENTE">
                        </div>
                    </div>
                    <div>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">MOTIVO DETALLE:</label>
                        <div class="input-group col-md-9 col-sm-9 col-xs-12">
                            <textarea class="form-control" name="motivodetalle" id="motivodetalle"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-success">Guardar</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@stop

@section('js-scripts')
<script type="text/javascript">
	$(document).on('click','#nuevotelefono',function() {
		$("#documento").val("");
		$("#tipodoc").val("");
		$("#numero").val("");
		$("#motivodetalle").val("");
		$("#tipodocumento").addClass("hidden");
		$("#documento").attr("readonly","readonly");
		$("#tipodoc").attr("readonly","readonly");
		$("#motivo").val("SOLICITUD CLIENTE");
		$("#documento").val($(this).closest("td").attr('documento'));
		$("#tipodoc").val($(this).closest("td").attr('tipodoc'));
		$("#modalNuevoTelefono").modal();
	});

	$(document).on('click','#nuevaempresa',function() {
		$("#documento").val("");
		$("#numero").val("");
		$("#motivodetalle").val("");
		$("#tipodocumento").removeClass("hidden");
		$("#documento").removeAttr("readonly");
		$("#tipodoc").removeAttr("readonly");
		$("#tipodoc").val("1");
		$("#motivo").val("SOLICITUD CLIENTE");
		$("#modalNuevoTelefono").modal();
	});

    function inputLimiter(e, allow) {
        var AllowableCharacters = '';

        if (allow == 'custom') {
            AllowableCharacters = ' 1234567890.';
        }


        var k = document.all ? parseInt(e.keyCode) : parseInt(e.which);
        if (k != 13 && k != 8 && k != 0) {
            if ((e.ctrlKey == false) && (e.altKey == false)) {
                return (AllowableCharacters.indexOf(String.fromCharCode(k)) != -1);
            } else {
                return true;
            }
        } else {
            return true;
        }

    }
</script>
@stop