<?php

use \App\Http\Controllers\GestioncarteraController; ?>
@extends('Layouts.layout')

@section('js-libs')
<script type="text/javascript" src="{{ URL::asset('js/datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.es.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/chart.bundle.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/jszip.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/pdfmake.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/vfs_fonts.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/buttons.flash.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/buttons.print.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/buttons.html5.min.js') }}"></script>

<link href="{{ URL::asset('css/datatables.min.css') }}" rel="stylesheet" type="text/css">
@stop

@section('content')

<div class="row space"></div>
@if(isset($jefes))
<div id="filtroNav" class="section-dashboard row justify-content-md-center">
    <div class="col-md-2 col-sm-2 col-xs-2 justify-content-center">
        <label>JEFATURA</label>
        <div class="form-group filtro">
            <select class="form-control" id="filtroJefes">
                <option selected>Todos</option>
                @if($jefes!=null && count($jefes)>=1)
                @foreach($jefes as $j)
                @if($j['ID_CENTRO'] != null)
                <option value="{{$j['ID_CENTRO']}}"><?php echo $j['NOMBRE'] ?></option>
                @endif
                @endforeach
                @endif
            </select>
        </div>
    </div>
    <div class="col-md-2 col-sm-2 col-xs-2">
        <button class="NxtBtn" onclick="ActualizarData()">Buscar</button>
    </div>
</div>
<div class="space row"></div>
@endif
<div id="graphics" style="flex-direction:column" class="section-dashboard row justify-content-md-center">
    <div style="height:20px" class="col-md-12 hidebar">
        <div></div><i id="TophideBtn" class="fa-chevron-up hidebtn fas"></i>
    </div>
    <div style="padding:0;" class="col-md-12 graphic-top parentWrapper">
        <div id="TopSection" style="padding:0; transition: 1s margin; margin-top:0;" class="col-md-12 graphic-top">
            <div style="height:40px; text-align:center" class="col-md-12">
                <div style="height:40px" class="col-md-1" style="text-align: center; ">
                    <h3></h3>
                </div>
                <div style="height:40px" class="col-md-3" style="text-align: center;">
                    <h4>Gestión de Leads</h4>
                </div>
                <div style="height:40px" class="col-md-3" style="text-align: center">
                    <h4>Performance de la gestión</h4>
                </div>
                <div style="height:40px" class="col-md-5" style="text-align: center">
                    <h4>Antiguedad de la última gestión</h4>
                </div>
            </div>
            <div class="space"></div>
            <div style="height:380px" class="col-md-12 justify-content-md-center" style="padding: 0;">
                <div style="height:380px;text-align: center; " class="col-md-1">
                    <div class="row justify-content-md-center">
                        <h4 class="titleScore">Cant.<br> Leads</h4>
                        <a style="cursor:pointer;font-size:20px" onclick="exportLeads(this)" id="canLeads">0</a>
                    </div>
                    <div class="row">
                        <h4 class="titleScore">Col.<br> Directas SF</h4>
                        <h4 id="colDirectas">0</h4>
                    </div>
                    <div class="row">
                        <h4 class="titleScore">Col.<br> Indirectas SF</h4>
                        <h4 id="colIndirectas">0</h4>
                    </div>
                    <div class="row">
                        <h4 class="titleScore">Cant.<br> Visitas</h4>
                        <h4 id="cantVisitas">0</h4>
                    </div>
                </div>
                <div style="height: 380px;display: flex;flex-direction: column;justify-content: center;align-items: center;" class="col-md-3 align-self-center justify-content-md-center" style="">
                    <div class="row  align-self-center justify-content-md-center" style="margin-top:-90px;">
                        <canvas class="canvasNota" id="gestionLeads"></canvas>
                        <div id="notaScore" class="score">0%</div>
                    </div>
                </div>
                <div style="margin-top: 40px;" class="col-md-3">
                    <div class="row barHorizontal" style="display: flex;justify-content: center;align-items: center;">
                        <div class="col-md-4">
                            <div class="row">Pendiente</div>

                        </div>
                        <div class="col-md-6 backgroundBarContainer" style="height:100%">
                            <div id="pendienteBackground" class="row backgroundBar">

                                <div id="pendiente" class="barHorizontalGreen">
                                    <div id="pendienteTooltip" class="tooltip"></div>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-2 " style="text-align:right">
                            <div id="pendienteCounter" class="row">0 k</div>
                        </div>
                    </div>
                    <div class="row barHorizontal" style="display: flex;justify-content: center;align-items: center;">
                        <div class="col-md-4">
                            <div class="row">Me Interesa</div>

                        </div>
                        <div class="col-md-6 backgroundBarContainer" style="height:100%">
                            <div id="interesaBackground" class="row backgroundBar">
                                <div id="interesa" class="barHorizontalGreen">
                                    <div id="interesaTooltip" class="tooltip"></div>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-2" style="text-align:right">
                            <div id="interesaCounter" class="row">0 k</div>
                        </div>
                    </div>
                    <div class="row barHorizontal" style="display: flex;justify-content: center;align-items: center;">
                        <div class="col-md-4">
                            <div class="row">Contactado</div>

                        </div>
                        <div class="col-md-6 backgroundBarContainer" style="height:100%">
                            <div id="contactadoBackground" class="row backgroundBar">
                                <div id="contactado" class="barHorizontalGreen">
                                    <div id="contactadoTooltip" class="tooltip"></div>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-2" style="text-align:right">
                            <div id="contactadoCounter" class="row">0 k</div>
                        </div>
                    </div>
                    <div class="row barHorizontal" style="display: flex;justify-content: center;align-items: center;">
                        <div class="col-md-4">
                            <div class="row">En Evaluación</div>

                        </div>
                        <div class="col-md-6 backgroundBarContainer" style="height:100%">
                            <div id="evaluacionBackground" class="row backgroundBar">
                                <div id="evaluacion" class="barHorizontalGreen">
                                    <div id="evaluacionTooltip" class="tooltip"></div>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-2" style="text-align:right">
                            <div id="evaluacionCounter" class="row">0 k</div>
                        </div>
                    </div>
                    <div class="row barHorizontal" style="display: flex;justify-content: center;align-items: center;">
                        <div class="col-md-4">
                            <div class="row">Aprobado</div>

                        </div>
                        <div class="col-md-6 backgroundBarContainer" style="height:100%">
                            <div id="aprobadoBackground" class="row backgroundBar">
                                <div id="aprobado" class="barHorizontalGreen">
                                    <div id="aprobadoTooltip" class="tooltip"></div>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-2" style="text-align:right">
                            <div id="aprobadoCounter" class="row">0 k</div>
                        </div>
                    </div>
                    <div class="row barHorizontal" style="display: flex;justify-content: center;align-items: center;">
                        <div class="col-md-4">
                            <div class="row">No Aprobado</div>

                        </div>
                        <div class="col-md-6 backgroundBarContainer" style="height:100%">
                            <div id="noAprobadoBackground" class="row backgroundBar">
                                <div id="noAprobadoTooltip" class="tooltip"></div>
                            </div>
                        </div>
                        <div class="col-md-2" style="text-align:right">
                            <div id="noAprobadoCounter" class="row">0 k</div>
                        </div>
                    </div>

                </div>
                <div style="height:380px" id="Antiguedad" class="col-md-5" style="">
                    <div id="barChartBody" class="row">

                        <div class="colGraph bars">
                            <div id="unMes" class="bar">
                                <div id="unMesTooltip" class="tooltip">0MM</div>
                            </div>
                        </div>
                        <div class="colGraph bars">
                            <div id="dosMes" class="bar">
                                <div id="dosMesTooltip" class="tooltip">0MM</div>
                            </div>
                        </div>
                        <div class="colGraph bars">
                            <div id="tresMes" class="bar">
                                <div id="tresMesTooltip" class="tooltip">0MM</div>
                            </div>
                        </div>
                        <div class="colGraph bars">
                            <div id="cuatroMes" class="bar">
                                <div id="cuatroMesTooltip" class="tooltip">0MM</div>
                            </div>
                        </div>
                        <div class="colGraph bars">
                            <div id="cincoMes" class="bar">
                                <div id="cincoMesTooltip" class="tooltip">0MM</div>
                            </div>
                        </div>
                        <div class="colGraph bars">
                            <div id="seisMes" class="bar">
                                <div id="seisMesTooltip" class="tooltip">0MM</div>
                            </div>
                        </div>
                        <div class="colGraph bars">
                            <div id="seisMasMes" class="bar">
                                <div id="seisMasMesTooltip" class="tooltip">0MM</div>
                            </div>
                        </div>
                    </div>
                    <div style="display:flex;justify-content: space-between;">

                        <div class="colGraph ">
                            <h5>Hace 1 <br> Mes</h5>
                        </div>
                        <div class="colGraph">
                            <h5>Hace 2 <br> Meses</h5>
                        </div>
                        <div class="colGraph">
                            <h5>Hace 3 <br> Meses</h5>
                        </div>
                        <div class="colGraph">
                            <h5>Hace 4 <br> Meses</h5>
                        </div>
                        <div class="colGraph">
                            <h5>Hace 5 <br> Meses</h5>
                        </div>
                        <div class="colGraph">
                            <h5>Hace 6 <br> Meses</h5>
                        </div>
                        <div class="colGraph">
                            <h5>6+ <br> Meses</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="space row"></div>
<div id="navCartera" class="row tabs">
    <ul class="nav nav-tabs" style="border-bottom: 0px !important; font-size:1.35em !important">
        <li class="nav-item">
            <a id="visitas-nav" class="nav-link nav-active">Visitas</a>
        </li>
        <li class="nav-item">
            <a id="eliminados-nav" class="nav-link">Eliminados</a>
        </li>
        <li class="nav-item">
            <a id="otros-nav" class="nav-link">Otros</a>
        </li>
    </ul>
</div>
<div id="rankingVisitas" class="row">
    <div class="col-md-8" style="padding-right:10px;">
        <div style="flex-direction:column" class="section-dashboard row justify-content-md-center">
            <div class="col-md-12 hidebar">
                <div></div><i id="hideBtn1" class="fa-chevron-up hidebtn fas"></i>
            </div>
            <div style="padding:0;" class="col-md-12 graphic-top parentWrapper">
                <div id="rankingEjecutivos" style="padding:0; transition: 1s margin; margin-top:0;" class="col-md-12 graphic-top">
                    <h5 class="row" style="padding-left:30px">
                        <bold style="font-weight:bold">Actualizado al: </bold>{{ $fecha}}
                    </h5>
                    <div class="col-md-7">
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-9">
                                <h4 style="text-align:center">Mes en curso</h4>
                            </div>
                        </div>
                        <div class="row">
                            <table id="resumenVisitasTable" style="width: calc(100% - 20px);margin: 10px;">
                                <thead>
                                    <th></th>
                                    <th style="text-align:center;">
                                        <div># Visitas</div>
                                        <div style="display: flex;flex-direction: column;">
                                            <i id="BtnVisitasUp" class="fa-sort-up sortBtn fas"></i>
                                            <i id="BtnVisitasDown" class="fa-sort-down sortBtn fas"></i>
                                        </div>
                                    </th>
                                    <th style="text-align:center;">
                                        <div>Clientes<br> Visitados</div>
                                        <div style="display: flex;flex-direction: column;">
                                            <i id="BtnClientesUp" class="fa-sort-up sortBtn fas"></i>
                                            <i id="BtnClientesDown" class="fa-sort-down sortBtn fas"></i>
                                        </div>
                                    </th>
                                    <th style="text-align:center;">
                                        <div>Primeras<br> Visitas </div>
                                        <div style="display: flex;flex-direction: column;">
                                            <i id="BtnPrimerasUp" class="fa-sort-up sortBtn fas"></i>
                                            <i id="BtnPrimerasDown" class="fa-sort-down sortBtn fas"></i>
                                        </div>
                                    </th>
                                </thead>
                                <tbody id="rankingVisitasBody">
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="row">
                            <h4 style="text-align:center">Acumulados</h4>
                        </div>
                        <div class="row">
                            <table style="width: calc(100% - 20px);margin: 10px;">
                                <thead>
                                    <th style="text-align:center;">
                                        <div># Visitas</div>
                                        <div style="display: flex;flex-direction: column;">
                                            <i id="BtnVisitasAcuUp" class="fa-sort-up sortBtn fas"></i>
                                            <i id="BtnVisitasAcuDown" class="fa-sort-down sortBtn fas"></i>
                                        </div>
                                    </th>
                                    <th style="text-align:center;">
                                        <div>Clientes<br> Visitados</div>
                                        <div style="display: flex;flex-direction: column;">
                                            <i id="BtnClientesAcuUp" class="fa-sort-up sortBtn fas"></i>
                                            <i id="BtnClientesAcuDown" class="fa-sort-down sortBtn fas"></i>
                                        </div>
                                    </th>
                                    <th style="text-align:center;">
                                        <div>Primeras<br> Visitas </div>
                                        <div style="display: flex;flex-direction: column;">
                                            <i id="BtnPrimerasAcuUp" class="fa-sort-up sortBtn fas"></i>
                                            <i id="BtnPrimerasAcuDown" class="fa-sort-down sortBtn fas"></i>
                                        </div>
                                    </th>
                                </thead>
                                <tbody id="rankingVisitasBodyAcumulado">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4" style="padding-left:20px;">
        <div style="flex-direction:column" class="section-dashboard row justify-content-md-center">
            <div class="col-md-12 hidebar">
                <h4 id="topVisitas">Últimas 5 Visitas <a id="downloadLink" onclick="exportF(this)" style="
    padding: 7px;
    background: #4DD094;
    cursor:pointer;
    color: white;
    font-size: 14px;
    padding-left: 15px;
    border-radius: 10px;
    margin-left: 20px;
    padding-right: 15px;
">Exportar</a></h4><i id="hideBtn2" class="fa-chevron-up hidebtn fas"></i>
            </div>
            <div class="col-md-12 hidebar">
                <div class="form-group filtro">
                    <select class="form-control" id="filtroTop">
                        <option value="5" selected>5</option>
                        <option value="10">10</option>
                        <option value="15">15</option>
                    </select>
                </div>
            </div <div style="padding:0;" class="col-md-12 graphic-top parentWrapper">
            <div id="rankingEmpresas" style="padding:0; transition: 1s margin; margin-top:0;" class="col-md-12 graphic-top">
            </div>
        </div>
    </div>
</div>

</div>
<div id="rankingEliminados" class="row disappear">
    <div class="col-md-8" style="padding-right:10px;">
        <div style="flex-direction:column" class="section-dashboard row justify-content-md-center">
            <div class="col-md-12 hidebar">
                <div></div><i id="hideBtn3" class="fa-chevron-up hidebtn fas"></i>
            </div>
            <div style="padding:0;" class="col-md-12 graphic-top parentWrapper">
                <div id="rankingEjecutivosEliminado" style="padding:0; transition: 1s margin; margin-top:0;" class="col-md-12 graphic-top">
                    <h5 class="row" style="padding-left:30px">
                        <bold style="font-weight:bold">Actualizado al: </bold>{{ $fecha}}
                    </h5>
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-9">
                            </div>
                        </div>
                        <div class="row">
                            <table id="resumenVisitasTableEliminado" style="width: calc(100% - 20px);margin: 10px;">
                                <thead>
                                    <th></th>
                                    <th style="text-align:center;">
                                        <div>Empresas <br> Eliminadas</div>
                                        <div style="display: flex;flex-direction: column;">
                                            <i id="BtnEmpresasEliminadasUp" class="fa-sort-up sortBtn fas"></i>
                                            <i id="BtnEmpresasEliminadasDown" class="fa-sort-down sortBtn fas"></i>
                                        </div>
                                    </th>

                                </thead>
                                <tbody id="rankingVisitasBodyEliminado">
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="row">
                            <table style="width: calc(100% - 20px);margin: 10px;">
                                <thead>
                                    <th style="text-align:center;">
                                        <div>No Contacto</div>
                                        <div style="display: flex;flex-direction: column;">
                                            <i id="BtnNoContactoUp" class="fa-sort-up sortBtn fas"></i>
                                            <i id="BtnNoContactoDown" class="fa-sort-down sortBtn fas"></i>
                                        </div>
                                    </th>
                                    <th style="text-align:center;">
                                        <div>No Perfil </div>
                                        <div style="display: flex;flex-direction: column;">
                                            <i id="BtnNoPerfilUp" class="fa-sort-up sortBtn fas"></i>
                                            <i id="BtnNoPerfilDown" class="fa-sort-down sortBtn fas"></i>
                                        </div>
                                    </th>
                                    <th style="text-align:center;">
                                        <div>No Interesado</div>
                                        <div style="display: flex;flex-direction: column;">
                                            <i id="BtnNoInteresadoUp" class="fa-sort-up sortBtn fas"></i>
                                            <i id="BtnNoInteresadoDown" class="fa-sort-down sortBtn fas"></i>
                                        </div>
                                    </th>
                                    <th style="text-align:center;">
                                        <div>No Califica</div>
                                        <div style="display: flex;flex-direction: column;">
                                            <i id="BtnNoCalificaUp" class="fa-sort-up sortBtn fas"></i>
                                            <i id="BtnNoCalificaDown" class="fa-sort-down sortBtn fas"></i>
                                        </div>
                                    </th>
                                    <th style="text-align:center;">
                                        <div>Otros<br> Motivos </div>
                                        <div style="display: flex;flex-direction: column;">
                                            <i id="BtnOtrosMotivosUp" class="fa-sort-up sortBtn fas"></i>
                                            <i id="BtnOtrosMotivosDOwn" class="fa-sort-down sortBtn fas"></i>
                                        </div>
                                    </th>
                                </thead>
                                <tbody id="rankingVisitasBodyEliminadoAcumulado">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4" style="padding-left:20px;">
        <div style="flex-direction:column" class="section-dashboard row justify-content-md-center">
            <div class="col-md-12 hidebar">
                <h4 id="topVisitasEliminado">Últimas 5 Eliminados <a id="downloadLink" onclick="exportF2(this)" style="
    padding: 7px;
    background: #4DD094;
    cursor:pointer;
    color: white;
    font-size: 14px;
    padding-left: 15px;
    border-radius: 10px;
    margin-left: 20px;
    padding-right: 15px;
">Exportar</a></h4><i id="hideBtn4" class="fa-chevron-up hidebtn fas"></i>
            </div>
            <div class="col-md-12 hidebar">
                <div class="form-group filtro">
                    <select class="form-control" id="filtroTopEliminado">
                        <option value="5" selected>5</option>
                        <option value="10">10</option>
                        <option value="15">15</option>
                    </select>
                </div>
            </div>
            <div style="padding:0;" class="col-md-12 graphic-top parentWrapper">
                <div id="rankingEmpresasEliminado" style="padding:0; transition: 1s margin; margin-top:0;" class="col-md-12 graphic-top">
                </div>
            </div>
        </div>
    </div>
</div>
<div id="rankingOtros" class="row disappear">
    <div class="col-md-4" style="padding-right:20px;">
        <div style="flex-direction:column" class="section-dashboard row justify-content-md-center">
            <div style="height:20px" class="col-md-12 hidebar"><i id="hideBtn1" class="fa-chevron-up hidebtn fas"></i></div>
            <div style="padding:0;" class="col-md-12 graphic-top parentWrapper">
                <div id="Ranking1" style="padding:0; transition: 1s margin; margin-top:0;" class="col-md-12 graphic-top">
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4" style="padding-right:20px;padding-left:20px;">
        <div style="flex-direction:column" class="section-dashboard row justify-content-md-center">
            <div style="height:20px" class="col-md-12 hidebar"><i id="hideBtn2" class="fa-chevron-up hidebtn fas"></i></div>
            <div style="padding:0;" class="col-md-12 graphic-top parentWrapper">
                <div id="Ranking2" style="padding:0; transition: 1s margin; margin-top:0;" class="col-md-12 graphic-top">
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4" style="padding-left:20px;">
        <div style="flex-direction:column" class="section-dashboard row justify-content-md-center">
            <div style="height:20px" class="col-md-12 hidebar"><i id="hideBtn3" class="fa-chevron-up hidebtn fas"></i></div>
            <div style="padding:0;" class="col-md-12 graphic-top parentWrapper">
                <div id="Ranking3" style="padding:0; transition: 1s margin; margin-top:0;" class="col-md-12 graphic-top">
                </div>
            </div>
        </div>
    </div>
</div>
<div id="maskWaiting">
    <div id="loading" class="waitingwraper">
        <img style="height: 100px;" src="../public/img/waiting.gif" alt="">
        <p style="margin-top: 15px;font-size: 1.3em;">Cargando...</p>
    </div>
    <div id="tableLeadsWrapper" class="waitingwraper" style="width: 350px;">
<div class="row" style="    width: 100%;
    padding-bottom: 20px;
    display: flex;
    justify-content: center;
    align-items: center;">
    <h3 style="float: left;
    padding-right: 25px;">¡Descarga tus leads aqui!</h3>
<i style="font-size: 25px;
    float: right; cursor:pointer;" class="fas fa-times" onclick="closeLeads()"></i>
</div>
       <table  style="width:100%; height:100%;" id='tableLead'>
       <thead>
            <th>ID<br>CENTRO</th>
            <th>ID<br>ZONA</th>
            <th>JEFATURA</th>
            <th>REGISTRO</th>
            <th>ENCARGADO</th>
            <th>NUM<br>DOC</th>
            <th>NOMBRE<br>EMPRESA</th>
            <th>CO<br>UNICO</th>
            <th>DEUDA<br>DIRECTA</th>
            <th>DEUDA<br>INDIRECTA</th>
            <th>BANCO<br>PRINCIPAL</th>
            <th>TIPO<br>PROSPECTO</th>
            <th>DEPARTAMENTO</th>
            <th>PROVINCIA</th>
            <th>DISTRITO</th>
            <th>DIRECCION</th>
            <th>GRUPO<br>ECONOMICO</th>
            <th>FACTURACION</th>
            <th>ESTRATEGIA</th>
            <th>ETAPA</th>
            <th>FECHA<br>ETAPA</th>
            <th>VISITADO</th>
            <th>ULTIMA<br>VISITA</th>
            <th>NIVEL<br>RIESGO</th>
            <th>RATING</th>
    </thead>
    <tbody></tbody>
       </table>
    </div>
</div>
<style>
    .sortBtn {
        padding-top: 5px;
        height: 1px;
        cursor: pointer;
        font-size: 1em !important;
        text-align: right !important;
    }

    table>thead>tr>th {
        padding-bottom: 10px;
    }

    table>thead>tr>th>div {
        height: 50px;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    table>tbody>tr td {
        height: 80px;
        font-size: 14px;
        flex-direction: row;
        text-align: center;
        width: 25%;
        padding-bottom: 10px;
        padding-top: 10px;
        justify-content: center;
    }

    /* table>tbody>tr {
        border-top-style: solid;
        border-top-width: 1.5px;
    } */
    .grey {
        background: whitesmoke;
    }

    .nombre {
        text-align: left !important;
        padding-left: 20px;
        font-size: 12px;
    }

    .maskAppear {
        display: flex !important;

    }
    .appear {
        display: flex !important;
    }
    .disappear {
        display: none !important;
    }

    .nav-tabs>li>a {
        line-height: 1.42857143;
        border: 2px solid transparent;
        border-radius: 4px 4px 0 0;
        cursor: pointer;
        border-bottom: 0px !important;
    }
    }

    .nav-link {
        background: whitesmoke;
    }

    .nav-active {
        background: white !important;

    }

    .waitingwraper {
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        background: white;
        padding: 20px;
        border-radius: 27px;
    }

    #maskWaiting {
        align-items: center;
        justify-content: center;
        position: fixed;
        background: rgba(0, 0, 0, 0.3);
        z-index: 999999999999999;
        top: 0;
        right: 0;
        width: 100vw;
        height: 100vh;
        display: none;
    }

    .parentWrapper {
        overflow: hidden;

    }

    .barHorizontalGreen {
        height: 13px;
        background-color: #4DD094;
        transition: 0.8s width;
        width: 0;
        display: flex;
        justify-content: flex-end;
    }

    #content {
        min-width: 1000 !important;
    }

    .barHorizontal {
        height: 40px;
    }

    .backgroundBarContainer {
        height: 100%;
        display: flex;
        justify-content: flex-start;
        align-items: center;
    }

    .colGraph {
        width: 12.5%;
        text-align: center;
    }

    .backgroundBar {
        height: 13px;
        padding: 0;
        width: 100%;
        display: flex;
        justify-content: flex-start;
        background: whitesmoke;
    }

    .bar {
        width: 100%;
        background-color: #4DD094;
        height: 0px;
        justify-content: center;
        align-items: flex-start;
        display: flex;
        transition: 1s height;
    }

    .bar:hover {
        opacity: 0.8;

    }

    .tooltip {
        opacity: 0;
        transition: 0.3s opacity;
        color: white;
        font-weight: bold;
        background: black;
        min-width: 40px;
        padding-left: 8px;
        text-align: center;
        padding-right: 8px;
        padding-bottom: 3px;
        padding-top: 3px;
        margin-top: -30px;
        border-radius: 8px;
    }

    .bar:hover .tooltip {
        opacity: 1;
    }

    .barHorizontalGreen:hover .tooltip {
        opacity: 1;
    }

    .bars {
        display: flex;
        align-items: flex-end;
        justify-content: center;
        padding: 5px;
    }

    #barChartBody {
        height: 270px;
        display: flex;

        justify-content: space-between;
    }

    @font-face {
        font-family: 'Omnes';
        src: url('../public/fonts/omnes-light-webfont.woff2') format('woff2'),
            url('../public/fonts/omnes-light-webfont.woff') format('woff');
        font-weight: normal;
        font-style: normal;

    }

    @font-face {
        font-family: 'OmnesRegular';
        src: url('../public/fonts/omnes-regular-webfont.woff2') format('woff2'),
            url('../public/fonts/omnes-regular-webfont.woff') format('woff');
        font-weight: normal;
        font-style: normal;

    }

    @font-face {
        font-family: 'OmnesBold';
        src: url('../public/fonts/omnes-medium-webfont.woff2') format('woff2'),
            url('../public/fonts/omnes-medium-webfont.woff') format('woff');
        font-weight: normal;
        font-style: normal;

    }

    .score {
        position: absolute;
        height: 100px;
        width: 100px;
        top: calc(50% - 95px);
        right: calc(50% - 50px);
        display: flex;
        align-items: center;
        justify-content: center;
        font-size: 1.5em;
        font-weight: bolder;
    }

    .GraphName {
        text-align: center;
    }

    #Pergest>div>div {
        padding: 4px;
    }

    .canvasNota {
        width: 100% !important;
        height: auto !important;

    }
    #tableLead_paginate{
        display: none;
    }
    #tableLead{
        display: none;
    }
    #tableLead_length{
        display: none;
    }
    #tableLead_filter{
        display: none;
    }
    #tableLead_info{
        display: none;
    }
    #tableLead_previous{
        display: none;
    }
    .dt-button{
        margin: 0 !important;
        font-size: 1.2em;
        padding-right: 20px;
        padding-left: 20px;
        padding-top: 5px;
        border-style: solid;
        border-radius: 9px;
        background: #4DD094;
        font-weight: bold;
        padding-bottom: 5px;
        color: white;
        transition: 0.25s all;
        border-color: transparent;
    }
    table.dataTable thead .sorting:after {
        display: none;
    }
        table.dataTable thead .sorting {
        pointer-events: none;
    cursor: default;}
    table.dataTable thead .sorting_asc {
        pointer-events: none;
    cursor: default;}
    table.dataTable thead .sorting_asc:after {
        display: none;
    }
    body {
        font-family: Omnes !important;
    }


    #Antiguedad {
        height: 100% !important;
        padding-right: 10px;
        padding-left: 30px;
    }

    .titleScore {
        font-family: OmnesBold !important;
        font-size: 1.2em;
    }

    .NxtBtn {
        margin-top: 8px;
        font-size: 1.2em;
        padding-right: 20px;
        padding-left: 20px;
        padding-top: 10px;
        border-style: solid;
        border-radius: 9px;
        background: #4DD094;
        margin-left: 25px;
        font-weight: bold;
        padding-bottom: 10px;
        color: white;
        transition: 0.25s all;
        border-color: transparent;

    }

    .avatar-icon {
        font-size: 2.2em;
    }

    .avatarWrapper {
        cursor: pointer;
    }

    .avatarCircle {
        background: white;
        display: flex;
        height: 65px;
        align-items: center;
        justify-content: center;
        border-radius: 32.5px;
        width: 65px;
    }

    .hidebtn {
        font-size: 1em;
        cursor: pointer;
        font-size: 1em !important;
        text-align: right !important;

    }

    .hidebar {
        display: flex;
        justify-content: space-between;
        align-items: center;
        height: 50px;
    }

    #graphics {
        display: flex;
        padding: 15px !important;
    }

    .space {
        height: 20px;
    }

    .avatar-container2 {
        background: whitesmoke;
        padding-top: 10px;
        display: flex;
        align-items: center;
        padding-bottom: 10px;
        margin-left: 0px !important;
        margin-right: 0px !important;
        padding-left: 15px;
    }

    .avatar-container {
        background: white;
        padding-top: 10px;
        display: flex;
        align-items: center;
        padding-bottom: 10px;
        margin-left: 0px !important;
        margin-right: 0px !important;
        padding-left: 15px;
    }

    .NxtBtn:hover {
        border-color: #4DD094 !important;
        background: transparent !important;
        color: #4DD094 !important;
    }
    table>thead>tr>th {
        padding: 8px !important;
    }
    .section-dashboard {
        background: white;
        padding: 10px !important;
        border-radius: 5px;
        box-shadow: 0 3px 11px 0px rgb(220, 220, 220);
    }

    .filtro {
        display: flex;
        justify-content: center;
    }

    .hideTop {
        margin-top: -500px !important;
    }
</style>
@stop
@section('js-scripts')
<script>
    function closeLeads(){
        document.table.destroy();
        $('#maskWaiting').removeClass('appear');
    }
    var table = null;
    var data = [];
    var data2 = [];
    var id_centro = "";
    const gestionLeads = document.getElementById('gestionLeads').getContext('2d');
    const gestionLeadsChart = new Chart(gestionLeads, {
        type: 'pie',
        data: {
            datasets: [{
                data: [100],
                backgroundColor: [
                    'whitesmoke'
                ],
                borderWidth: 0.6
            }],
            labels: [
                'Nota',
                '',
            ]
        },

        options: {
            legend: {
                display: false
            },
            cutoutPercentage: 70,
            responsive: true,
            mantainRatio: true,
            tooltips: {
                enabled: false
            },
        }
    });
    $(".nav-link").on("click", function() {
        var id = this.id;
        switch (id) {
            case 'visitas-nav':
                ActualizarResumen();
                break;
            case 'eliminados-nav':
                ActualizarResumenEliminados();
                break;
        }
    });

    function ActualizarData() {
        const id = $(".nav-active").first().attr('id');
        switch (id) {
            case 'visitas-nav':
                ActualizarResumen();
                break;
            case 'eliminados-nav':
                ActualizarResumenEliminados();
                break;
        }
    }
    function exportLeads(elem) {
        var finalVal = '';
        var jefe;
        if ($('#filtroJefes').length == 0) {
            jefe = document.id_centro;
        } else {
            jefe = document.getElementById('filtroJefes').options[document.getElementById('filtroJefes').selectedIndex].value;
        }
        if (jefe == 'Todos') {
            jefe = null;
        }
        $('#loading').addClass('disappear');
        setTimeout(function(){$('#maskWaiting').addClass('appear');; }, 1000);
        $('#tableLeadsWrapper').addClass('appear');
        $.ajax({
            type: 'GET',
            data: {
                "ID_CENTRO": jefe,
            },
            async: false,
            url: '{{route("resumen.leads")}}',
            success: function(result) {
                var data = JSON.parse(result);
                if (typeof data === 'object' && data !== null) {
                    data = Object.entries(JSON.parse(result));
                    var realData = [];
                    for (let index = 0; index < data.length; index++) {
                        const element = data[index][1];
                        realData.push(element);
                    }
                    data = realData;
                }
                var table = document.getElementById('tableLead').getElementsByTagName('tbody')[0];
                for (let index = 0; index < data.length; index++) {
                    const element = data[index];
                    var row = table.insertRow(index);
                    var ID_CENTRO = row.insertCell(0);
                    var ID_ZONA = row.insertCell(1);
                    var JEFATURA = row.insertCell(2);
                    var REGISTRO	 = row.insertCell(3);
                    var ENCARGADO	 = row.insertCell(4);
                    var NUM_DOC	 = row.insertCell(5);
                    var NOMBRE_EMPRESA	 = row.insertCell(6);
                    var COD_UNICO		 = row.insertCell(7);
                    var DEUDA_DIRECTA	 = row.insertCell(8);
                    var DEUDA_INDIRECTA	 = row.insertCell(9);
                    var BANCO_PRINCIPAL	 = row.insertCell(10);
                    var TIPO_PROSPECTO	 = row.insertCell(11);
                    var DEPARTAMENTO	 = row.insertCell(12);
                    var PROVINCIA	 = row.insertCell(13);
                    var DISTRITO	 = row.insertCell(14);
                    var DIRECCION	 = row.insertCell(15);
                    var GRUPO_ECONOMICO	 = row.insertCell(16);
                    var FACTURACION	 = row.insertCell(17);
                    var ESTRATEGIA	 = row.insertCell(18);
                    var ETAPA	 = row.insertCell(19);
                    var FECHA_ETAPA	 = row.insertCell(20);
                    var VISITADO	 = row.insertCell(21);
                    var FECHA_ULTIMA_VISITA	 = row.insertCell(22);
                    var NIVEL_RIESGO	     = row.insertCell(23);
                    var RATING = row.insertCell(24);
                    ID_CENTRO.innerHTML = element['ID_CENTRO'];
                    ID_ZONA.innerHTML = element['ID_ZONA'];
                    JEFATURA.innerHTML = element['JEFATURA'];
                    REGISTRO.innerHTML = element['REGISTRO'];
                    ENCARGADO.innerHTML = element['ENCARGADO'];
                    NUM_DOC.innerHTML = element['NUM_DOC'];
                    NOMBRE_EMPRESA.innerHTML = element['NOMBRE_EMPRESA'];
                    COD_UNICO.innerHTML = element['COD_UNICO'];
                    DEUDA_DIRECTA.innerHTML = element['DEUDA_DIRECTA'];
                    DEUDA_INDIRECTA.innerHTML = element['DEUDA_INDIRECTA'];
                    BANCO_PRINCIPAL.innerHTML = element['BANCO_PRINCIPAL'];
                    TIPO_PROSPECTO.innerHTML = element['TIPO_PROSPECTO'];
                    DEPARTAMENTO.innerHTML = element['DEPARTAMENTO'];
                    PROVINCIA.innerHTML = element['PROVINCIA'];
                    DISTRITO.innerHTML = element['DISTRITO'];
                    DIRECCION.innerHTML = element['DIRECCION'];
                    GRUPO_ECONOMICO.innerHTML = element['GRUPO_ECONOMICO'];
                    FACTURACION.innerHTML = element['FACTURACION'];
                    ESTRATEGIA.innerHTML = element['ESTRATEGIA'];
                    ETAPA.innerHTML = element['ETAPA'];
                    FECHA_ETAPA.innerHTML = element['FECHA_ETAPA'];
                    VISITADO.innerHTML = element['VISITADO'];
                    FECHA_ULTIMA_VISITA.innerHTML = element['FECHA_ULTIMA_VISITA'];
                    NIVEL_RIESGO.innerHTML = element['NIVEL_RIESGO'];
                    RATING.innerHTML = element['RATING'];
                }
                document.table = $('#tableLead').DataTable({
                    processing: true,
                    "bAutoWidth": false,
                    "bSortable": false,
                    rowId: 'staffId',
                    dom: 'Blfrtip',
                    buttons: [
                        {
                            extend: 'excel',
                            filename: 'DetalleLeads'
                        }
                    ],
                    language: {
                        "url": "{{ URL::asset('js/Json/Spanish.Json') }}"
                    } ,
                    fixedColumns: true
                });
            }
        });
        return false;
    }

    function exportF(elem) {
        var finalVal = '';
        var top = document.getElementById('filtroTop').options[document.getElementById('filtroTop').selectedIndex].value;
        $.ajax({
            type: 'POST',
            data: {
                "ID_CENTRO": null,
                "LOGIN": true,
                "TOP": '100000',
            },
            async: false,
            url: '{{route("resumen.ranking.empresas")}}',
            success: function(result) {
                var data = JSON.parse(result);
                if (typeof data === 'object' && data !== null) {
                    data = Object.entries(JSON.parse(result));
                    var realData = [];
                    for (let index = 0; index < data.length; index++) {
                        const element = data[index][1];
                        realData.push(element);
                    }
                    data = realData;
                }
                if (data.length > 0) {
                    var content = [];
                    var contentRow = [];
                    for (let index = 0; index < data.length; index++) {
                        const element = data[index];
                        if (index == 0) {
                            content.push(Object.keys(element))
                        }
                        const values = Object.values(element);
                        content.push(values);
                    }
                    for (var i = 0; i < content.length; i++) {
                        var value = content[i];
                        for (var j = 0; j < value.length; j++) {
                            var innerValue = value[j] === null ? '' : value[j].toString();
                            var result = innerValue.replace(/"/g, '""');
                            if (result.search(/("|,|\n)/g) >= 0)
                                result = '"' + result + '"';
                            if (j > 0)
                                finalVal += ';';
                            finalVal += result;
                        }

                        finalVal += '\n';
                    }
                    var url = 'data:text/csv;charset=utf-8,' + encodeURIComponent(finalVal);
                    elem.setAttribute("href", url);
                    elem.setAttribute("download", "LeadVisitados.csv");
                }
            }
        });
        return false;
    }

    function exportF2(elem) {
        var finalVal = '';
        var top = document.getElementById('filtroTopEliminado').options[document.getElementById('filtroTopEliminado').selectedIndex].value;
        $.ajax({
            type: 'POST',
            data: {
                "ID_CENTRO": null,
                "LOGIN": true,
                "TOP": '100000',
            },
            async: false,
            url: '{{route("resumen.ranking.empresas.eliminados")}}',
            success: function(result) {
                var data = JSON.parse(result);
                if (typeof data === 'object' && data !== null) {
                    data = Object.entries(JSON.parse(result));
                    var realData = [];
                    for (let index = 0; index < data.length; index++) {
                        const element = data[index][1];
                        realData.push(element);
                    }
                    data = realData;
                }
                if (data.length > 0) {
                    var content = [];
                    var contentRow = [];
                    for (let index = 0; index < data.length; index++) {
                        const element = data[index];
                        if (index == 0) {
                            content.push(Object.keys(element))
                        }
                        const values = Object.values(element);
                        content.push(values);
                    }
                    for (var i = 0; i < content.length; i++) {
                        var value = content[i];
                        for (var j = 0; j < value.length; j++) {
                            var innerValue = value[j] === null ? '' : value[j].toString();
                            var result = innerValue.replace(/"/g, '""');
                            if (result.search(/("|,|\n)/g) >= 0)
                                result = '"' + result + '"';
                            if (j > 0)
                                finalVal += ';';
                            finalVal += result;
                        }

                        finalVal += '\n';
                    }
                    var url = 'data:text/csv;charset=utf-8,' + encodeURIComponent(finalVal);
                    elem.setAttribute("href", url);
                    elem.setAttribute("download", "LeadEliminados.csv");
                }
            }
        });
        return false;
    }
    $('#hideBtn1').click(function() {
        var height = $('#rankingEjecutivos').height();
        if ($("#" + this.id).hasClass('fa-chevron-up')) {
            $("#" + this.id).removeClass('fa-chevron-up');
            $("#" + this.id).addClass('fa-chevron-down');
            $('#rankingEjecutivos').css('margin-top', '-' + height + 'px');

        } else {
            $("#" + this.id).addClass('fa-chevron-up');
            $("#" + this.id).removeClass('fa-chevron-down');
            $('#rankingEjecutivos').css('margin-top', '0px');
        }
    });
    $('#hideBtn2').click(function() {
        var height = $('#rankingEmpresas').height();
        if ($("#" + this.id).hasClass('fa-chevron-up')) {
            $("#" + this.id).removeClass('fa-chevron-up');
            $("#" + this.id).addClass('fa-chevron-down');
            $('#rankingEmpresas').css('margin-top', '-' + height + 'px');

        } else {
            $("#" + this.id).addClass('fa-chevron-up');
            $("#" + this.id).removeClass('fa-chevron-down');
            $('#rankingEmpresas').css('margin-top', '0px');
        }
    });
    $('#hideBtn3').click(function() {
        var height = $('#rankingEjecutivosEliminado').height();
        if ($("#" + this.id).hasClass('fa-chevron-up')) {
            $("#" + this.id).removeClass('fa-chevron-up');
            $("#" + this.id).addClass('fa-chevron-down');
            $('#rankingEjecutivosEliminado').css('margin-top', '-' + height + 'px');

        } else {
            $("#" + this.id).addClass('fa-chevron-up');
            $("#" + this.id).removeClass('fa-chevron-down');
            $('#rankingEjecutivosEliminado').css('margin-top', '0px');
        }
    });
    $('#hideBtn4').click(function() {
        var height = $('#rankingEmpresasEliminado').height();
        if ($("#" + this.id).hasClass('fa-chevron-up')) {
            $("#" + this.id).removeClass('fa-chevron-up');
            $("#" + this.id).addClass('fa-chevron-down');
            $('#rankingEmpresasEliminado').css('margin-top', '-' + height + 'px');

        } else {
            $("#" + this.id).addClass('fa-chevron-up');
            $("#" + this.id).removeClass('fa-chevron-down');
            $('#rankingEmpresasEliminado').css('margin-top', '0px');
        }
    });
    $(".sortBtn").on('click', function(event) {
        event.stopPropagation();
        event.stopImmediatePropagation();
        if ($("#" + this.id).hasClass('fa-sort-up')) {
            switch (this.id) {
                case 'BtnVisitasUp':
                    var data = document.data.sort(function(b, a) {
                        return a['CTD_VISITAS'] - b['CTD_VISITAS'];
                    });
                    fillRankingVisitas(data);
                    break;
                case 'BtnClientesUp':
                    var data = document.data.sort(function(b, a) {
                        return a['CTD_CLIENTES_UNICOS'] - b['CTD_CLIENTES_UNICOS'];
                    });
                    fillRankingVisitas(data);
                    break;
                case 'BtnPrimerasUp':
                    var data = document.data.sort(function(b, a) {
                        return a['CTD_PRIMERAS_VISITAS'] - b['CTD_PRIMERAS_VISITAS'];
                    });
                    fillRankingVisitas(data);
                    break;
                case 'BtnVisitasAcuUp':
                    var data = document.data.sort(function(b, a) {
                        return a['CTD_VISITAS_YTD'] - b['CTD_VISITAS_YTD'];
                    });
                    fillRankingVisitas(data);
                    break;
                case 'BtnClientesAcuUp':
                    var data = document.data.sort(function(b, a) {
                        return a['CTD_CLIENTES_UNICOS_YTD'] - b['CTD_CLIENTES_UNICOS_YTD'];
                    });
                    fillRankingVisitas(data);
                    break;
                case 'BtnPrimerasAcuUp':
                    var data = document.data.sort(function(b, a) {
                        return a['CTD_PRIMERAS_VISITAS_YTD'] - b['CTD_PRIMERAS_VISITAS_YTD'];
                    });
                    fillRankingVisitas(data);
                    break;
                case 'BtnEmpresasEliminadasUp':
                    var data = document.data.sort(function(b, a) {
                        return a['CTD_ELIMINACION_STOCK'] - b['CTD_ELIMINACION_STOCK'];
                    });
                    fillRankingVisitasEliminados(data);
                    break;
                case 'BtnNoContactoUp':
                    var data = document.data.sort(function(b, a) {
                        return a['CTD_ELIM_NCON_STOCK'] - b['CTD_ELIM_NCON_STOCK'];
                    });
                    fillRankingVisitasEliminados(data);
                    break;
                case 'BtnNoPerfilUp':
                    var data = document.data.sort(function(b, a) {
                        return a['CTD_ELIM_NPER_STOCK'] - b['CTD_ELIM_NPER_STOCK'];
                    });
                    fillRankingVisitasEliminados(data);
                    break;
                case 'BtnNoInteresadoUp':
                    var data = document.data.sort(function(b, a) {
                        return a['CTD_ELIM_NINT_STOCK'] - b['CTD_ELIM_NINT_STOCK'];
                    });
                    fillRankingVisitasEliminados(data);
                    break;
                case 'BtnNoCalificaUp':
                    var data = document.data.sort(function(b, a) {
                        return a['CTD_ELIM_NCAL_STOCK'] - b['CTD_ELIM_NCAL_STOCK'];
                    });
                    fillRankingVisitasEliminados(data);
                    break;
                case 'BtnOtrosMotivosUp':
                    var data = document.data.sort(function(b, a) {
                        return a['CTD_ELIM_OTRO_STOCK'] - b['CTD_ELIM_OTRO_STOCK'];
                    });
                    fillRankingVisitasEliminados(data);
                    break;
            }
        } else {
            switch (this.id) {
                case 'BtnVisitasDown':
                    var data = document.data.sort(function(a, b) {
                        return a['CTD_VISITAS'] - b['CTD_VISITAS'];
                    });
                    fillRankingVisitas(data);
                    break;
                case 'BtnClientesDown':
                    var data = document.data.sort(function(a, b) {
                        return a['CTD_CLIENTES_UNICOS'] - b['CTD_CLIENTES_UNICOS'];
                    });
                    fillRankingVisitas(data);
                    break;
                case 'BtnPrimerasDown':
                    var data = document.data.sort(function(a, b) {
                        return a['CTD_PRIMERAS_VISITAS'] - b['CTD_PRIMERAS_VISITAS'];
                    });
                    fillRankingVisitas(data);
                    break;
                case 'BtnVisitasAcuDown':
                    var data = document.data.sort(function(a, b) {
                        return a['CTD_VISITAS_YTD'] - b['CTD_VISITAS_YTD'];
                    });
                    fillRankingVisitas(data);
                    break;
                case 'BtnClientesAcuDown':
                    var data = document.data.sort(function(a, b) {
                        return a['CTD_CLIENTES_UNICOS_YTD'] - b['CTD_CLIENTES_UNICOS_YTD'];
                    });
                    fillRankingVisitas(data);
                    break;
                case 'BtnPrimerasAcuDown':
                    var data = document.data.sort(function(a, b) {
                        return a['CTD_PRIMERAS_VISITAS_YTD'] - b['CTD_PRIMERAS_VISITAS_YTD'];
                    });
                    fillRankingVisitas(data);
                    break;
                case 'BtnEmpresasEliminadasDown':
                    var data = document.data.sort(function(a, b) {
                        return a['CTD_ELIMINACION_STOCK'] - b['CTD_ELIMINACION_STOCK'];
                    });
                    fillRankingVisitasEliminados(data);
                    break;
                case 'BtnNoContactoDown':
                    var data = document.data.sort(function(a, b) {
                        return a['CTD_ELIM_NCON_STOCK'] - b['CTD_ELIM_NCON_STOCK'];
                    });
                    fillRankingVisitasEliminados(data);
                    break;
                case 'BtnNoPerfilDown':
                    var data = document.data.sort(function(a, b) {
                        return a['CTD_ELIM_NPER_STOCK'] - b['CTD_ELIM_NPER_STOCK'];
                    });
                    fillRankingVisitasEliminados(data);
                    break;
                case 'BtnNoInteresadoDown':
                    var data = document.data.sort(function(a, b) {
                        return a['CTD_ELIM_NINT_STOCK'] - b['CTD_ELIM_NINT_STOCK'];
                    });
                    fillRankingVisitasEliminados(data);
                    break;
                case 'BtnNoCalificaDown':
                    var data = document.data.sort(function(a, b) {
                        return a['CTD_ELIM_NCAL_STOCK'] - b['CTD_ELIM_NCAL_STOCK'];
                    });
                    fillRankingVisitasEliminados(data);
                    break;
                case 'BtnOtrosMotivosDown':
                    var data = document.data.sort(function(a, b) {
                        return a['CTD_ELIM_OTRO_STOCK'] - b['CTD_ELIM_OTRO_STOCK'];
                    });
                    fillRankingVisitasEliminados(data);
                    break;
            }
        }
    });
    document.addEventListener('DOMContentLoaded', (event) => {
        document.id_centro = "<?php echo isset($id_centro) ? $id_centro : null ?>";
        if (id_centro == "null") {
            idcdocument.id_centro = null;
        }
        var top = document.getElementById('filtroTop').options[document.getElementById('filtroTop').selectedIndex].value;
        $.ajax({
            type: 'POST',
            data: {
                "ID_CENTRO": document.id_centro
            },
            async: true,
            url: '{{route("resumen.top.graphics")}}',
            success: function(result) {
                var data = JSON.parse(result);
                resumenTopGraphic(data);
            }
        });
        $.ajax({
            type: 'POST',
            data: {
                "ID_CENTRO": document.id_centro
            },
            async: true,
            url: '{{route("resumen.ranking.visitas")}}',
            success: function(result) {
                var data = JSON.parse(result);
                if (typeof data === 'object' && data !== null) {
                    data = Object.entries(JSON.parse(result));
                    var realData = [];
                    for (let index = 0; index < data.length; index++) {
                        const element = data[index][1];
                        realData.push(element);
                    }
                    data = realData;
                }
                data = data.sort(function(b, a) {
                    return a['CTD_CLIENTES_UNICOS_YTD'] - b['CTD_CLIENTES_UNICOS_YTD'];
                });
                document.data = data;
                fillRankingVisitas(data);
            }
        });
        $.ajax({
            type: 'POST',
            data: {
                "ID_CENTRO": document.id_centro,
                "LOGIN": true,
                'TOP': top
            },
            async: true,
            url: '{{route("resumen.ranking.empresas")}}',
            success: function(result) {
                var data = JSON.parse(result);
                if (typeof data === 'object' && data !== null) {
                    data = Object.entries(JSON.parse(result));
                    var realData = [];
                    for (let index = 0; index < data.length; index++) {
                        const element = data[index][1];
                        realData.push(element);
                    }
                    data = realData;
                }
                document.data2 = data;
                fillRankingEmpresas(data);
                $('#maskWaiting').removeClass('maskAppear');
            }
        });
    });
    $("#visitas-nav").click(function() {
        $("#eliminados-nav").removeClass("nav-active");
        $("#otros-nav").removeClass("nav-active");
        $("#rankingVisitas").removeClass("disappear");
        $("#rankingEliminados").addClass("disappear");
        $("#visitas-nav").addClass("nav-active");
        $("#rankingOtros").addClass("disappear");
    });
    $("#eliminados-nav").click(function() {
        $("#otros-nav").removeClass("nav-active");
        $("#visitas-nav").removeClass("nav-active");
        $("#rankingVisitas").addClass("disappear");
        $("#rankingEliminados").removeClass("disappear");
        $("#eliminados-nav").addClass("nav-active");
        $("#rankingOtros").addClass("disappear");
    });
    $("#otros-nav").click(function() {
        $("#eliminados-nav").removeClass("nav-active");
        $("#visitas-nav").removeClass("nav-active");
        $("#rankingVisitas").addClass("disappear");
        $("#rankingEliminados").addClass("disappear");
        $("#otros-nav").addClass("nav-active");
        $("#rankingOtros").removeClass("disappear");
    });
    $('#TophideBtn').click(function() {
        var element = $('#TopSection');
        if (element.hasClass('hideTop')) {
            element.removeClass('hideTop');
            $('#TophideBtn').removeClass('fa-chevron-down');
            $('#TophideBtn').addClass('fa-chevron-up');
        } else {
            element.addClass('hideTop');
            $('#TophideBtn').addClass('fa-chevron-down');
            $('#TophideBtn').removeClass('fa-chevron-up');
        }
    });

    $(document).on("change", "#filtroTop", function() {
        var top = $("#filtroTop").val();
        var jefe;
        if ($('#filtroJefes').length == 0) {
            jefe = document.id_centro;
        } else {
            jefe = document.getElementById('filtroJefes').options[document.getElementById('filtroJefes').selectedIndex].value;
        }
        if (jefe == 'Todos') {
            jefe = null;
        }
        $('#topVisitas').html("Top " + top + ` Visitas <a id="downloadLink" onclick="exportF(this)" style="
                            padding: 7px;
                            background: #4DD094;
                            cursor:pointer;
                            color: white;
                            font-size: 14px;
                            padding-left: 15px;
                            border-radius: 10px;
                            margin-left: 20px;
                            padding-right: 15px;
        ">Exportar</a>`);
        $.ajax({
            type: 'POST',
            data: {
                "ID_CENTRO": jefe,
                "LOGIN": false,
                'TOP': top
            },
            async: true,
            url: '{{route("resumen.ranking.empresas")}}',
            success: function(result) {
                var data = Object.entries(JSON.parse(result));
                var realData = [];
                for (let index = 0; index < data.length; index++) {
                    const element = data[index][1];
                    realData.push(element);
                }
                document.data2 = realData;
                fillRankingEmpresas(realData);
                $('#maskWaiting').removeClass('maskAppear');
            }
        });

    });
    $(document).on("change", "#filtroTopEliminado", function() {
        var top = $("#filtroTopEliminado").val();
        var jefe;
        if ($('#filtroJefes').length == 0) {
            jefe = document.id_centro;
        } else {
            jefe = document.getElementById('filtroJefes').options[document.getElementById('filtroJefes').selectedIndex].value;
        }
        if (jefe == 'Todos') {
            jefe = null;
        }
        $('#topVisitasEliminado').html("Top " + top + ` Eliminados <a id="downloadLink" onclick="exportF2(this)" style="
                    padding: 7px;
                    background: #4DD094;
                    cursor:pointer;
                    color: white;
                    font-size: 14px;
                    padding-left: 15px;
                    border-radius: 10px;
                    margin-left: 20px;
                    padding-right: 15px;
">Exportar</a>`);
        $.ajax({
            type: 'POST',
            data: {
                "ID_CENTRO": jefe,
                "LOGIN": false,
                'TOP': top
            },
            async: true,
            url: '{{route("resumen.ranking.empresas.eliminados")}}',
            success: function(result) {
                var data = Object.entries(JSON.parse(result));
                var realData = [];
                for (let index = 0; index < data.length; index++) {
                    const element = data[index][1];
                    realData.push(element);
                }
                document.data2 = realData;
                fillRankingEmpresasEliminados(realData);
                $('#maskWaiting').removeClass('maskAppear');
            }
        });

    });

    function ActualizarResumen() {
        var jefe;
        if ($('#filtroJefes').length == 0) {
            jefe = document.id_centro;
        } else {
            jefe = document.getElementById('filtroJefes').options[document.getElementById('filtroJefes').selectedIndex].value;
        }
        if (jefe == 'Todos') {
            jefe = null;
        }
        var top = document.getElementById('filtroTop').options[document.getElementById('filtroTop').selectedIndex].value;
        $('#maskWaiting').addClass('maskAppear');
        $.ajax({
            type: 'POST',
            data: {
                "ID_CENTRO": jefe
            },
            async: true,
            url: '{{route("resumen.top.graphics")}}',
            success: function(result) {
                var data = JSON.parse(result);
                resumenTopGraphic(data);
            }
        });
        $.ajax({
            type: 'POST',
            data: {
                "ID_CENTRO": jefe
            },
            async: true,
            url: '{{route("resumen.ranking.visitas")}}',
            success: function(result) {
                var data = Object.entries(JSON.parse(result));
                var realData = [];
                for (let index = 0; index < data.length; index++) {
                    const element = data[index][1];
                    realData.push(element);
                }
                realData = realData.sort(function(b, a) {
                    return a['CTD_CLIENTES_UNICOS_YTD'] - b['CTD_CLIENTES_UNICOS_YTD'];
                });
                document.data = realData;

                fillRankingVisitas(realData)
            }
        });
        $.ajax({
            type: 'POST',
            data: {
                "ID_CENTRO": jefe,
                "LOGIN": false,
                'TOP': top
            },
            async: true,
            url: '{{route("resumen.ranking.empresas")}}',
            success: function(result) {
                var data = Object.entries(JSON.parse(result));
                var realData = [];
                for (let index = 0; index < data.length; index++) {
                    const element = data[index][1];
                    realData.push(element);
                }

                document.data2 = realData;
                fillRankingEmpresas(realData);
                $('#maskWaiting').removeClass('maskAppear');
            }
        });
    }


    function ActualizarResumenEliminados() {
        var jefe;
        if ($('#filtroJefes').length == 0) {
            jefe = document.id_centro;
        } else {
            jefe = document.getElementById('filtroJefes').options[document.getElementById('filtroJefes').selectedIndex].value;
        }
        if (jefe == 'Todos') {
            jefe = null;
        }
        $('#maskWaiting').addClass('maskAppear');
        var top = document.getElementById('filtroTop').options[document.getElementById('filtroTop').selectedIndex].value;
        $.ajax({
            type: 'POST',
            data: {
                "ID_CENTRO": jefe
            },
            async: true,
            url: '{{route("resumen.top.graphics")}}',
            success: function(result) {
                var data = JSON.parse(result);
                resumenTopGraphic(data);
            }
        });
        $.ajax({
            type: 'POST',
            data: {
                "ID_CENTRO": jefe
            },
            async: true,
            url: '{{route("resumen.ranking.visitas.eliminados")}}',
            success: function(result) {
                var data = Object.entries(JSON.parse(result));
                var realData = [];
                for (let index = 0; index < data.length; index++) {
                    const element = data[index][1];
                    realData.push(element);
                }
                document.data = realData;
                realData = realData.sort(function(b, a) {
                    return a['CTD_ELIMINACION_STOCK'] - b['CTD_ELIMINACION_STOCK'];
                });
                fillRankingVisitasEliminados(realData)
            }
        });
        $.ajax({
            type: 'POST',
            data: {
                "ID_CENTRO": jefe,
                "LOGIN": false,
                'TOP': top
            },
            async: true,
            url: '{{route("resumen.ranking.empresas.eliminados")}}',
            success: function(result) {
                var data = Object.entries(JSON.parse(result));
                var realData = [];
                for (let index = 0; index < data.length; index++) {
                    const element = data[index][1];
                    realData.push(element);
                }
                document.data2 = realData;
                fillRankingEmpresasEliminados(realData);
                $('#maskWaiting').removeClass('maskAppear');
            }
        });
    }

    function fillRankingEmpresasEliminados(data) {
        document.getElementById('rankingEmpresasEliminado').innerHTML = '';
        var length = data.length > 10 ? 10 : data.length;
        for (let index = 0; index < length; index++) {
            const element = data[index];
            if (index % 2) {
                var str = '';
            } else {
                var str = '';
            }
            document.getElementById('rankingEmpresasEliminado').insertAdjacentHTML('beforeend', str);
        }
    }

    function fillRankingVisitasEliminados(data) {
        document.getElementById('rankingVisitasBodyEliminado').innerHTML = '';
        document.getElementById('rankingVisitasBodyEliminadoAcumulado').innerHTML = '';
        for (let index = 0; index < data.length; index++) {
            const element = data[index];
            if (index % 2) {
                document.getElementById('rankingVisitasBodyEliminado').insertAdjacentHTML('beforeend', `
                        <tr>
                            <td class="nombre">` + element['ENCARGADO'] + `</td>
                            <td>` + element['CTD_ELIMINACION_STOCK'] + `</td>
                        </tr>
                        `);
                document.getElementById('rankingVisitasBodyEliminadoAcumulado').insertAdjacentHTML('beforeend', `
                        <tr>
                            <td>` + element['CTD_ELIM_NCON_STOCK'] + `</td>
                            <td>` + element['CTD_ELIM_NPER_STOCK'] + `</td>
                            <td>` + element['CTD_ELIM_NINT_STOCK'] + `</td>
                            <td>` + element['CTD_ELIM_NCAL_STOCK'] + `</td>
                            <td>` + element['CTD_ELIM_OTRO_STOCK'] + `</td>
                        </tr>
                        `);
            } else {
                document.getElementById('rankingVisitasBodyEliminado').insertAdjacentHTML('beforeend', `
                            <tr class="grey">
                                <td class="nombre">` + element['ENCARGADO'] + `</td>
                                <td>` + element['CTD_ELIMINACION_STOCK'] + `</td>
                            </tr>
                            `);
                document.getElementById('rankingVisitasBodyEliminadoAcumulado').insertAdjacentHTML('beforeend', `
                        <tr class="grey">
                            <td>` + element['CTD_ELIM_NCON_STOCK'] + `</td>
                            <td>` + element['CTD_ELIM_NPER_STOCK'] + `</td>
                            <td>` + element['CTD_ELIM_NINT_STOCK'] + `</td>
                            <td>` + element['CTD_ELIM_NCAL_STOCK'] + `</td>
                            <td>` + element['CTD_ELIM_OTRO_STOCK'] + `</td>
                        </tr>
                        `);
            }
        }
    }

    function fillRankingEmpresas(data) {
        document.getElementById('rankingEmpresas').innerHTML = '';
        var length = data.length > 10 ? 10 : data.length;
        for (let index = 0; index < length; index++) {
            const element = data[index];
            if (index % 2) {
                var str = '';
            } else {
                var str = '';
            }
            document.getElementById('rankingEmpresas').insertAdjacentHTML('beforeend', str);
        }
    }

    function fillRankingVisitas(data) {
        document.getElementById('rankingVisitasBody').innerHTML = '';
        document.getElementById('rankingVisitasBodyAcumulado').innerHTML = '';
        for (let index = 0; index < data.length; index++) {
            const element = data[index];
            if (index % 2) {
                document.getElementById('rankingVisitasBody').insertAdjacentHTML('beforeend', `
                        <tr>
                            <td class="nombre">` + element['ENCARGADO'] + `</td>
                            <td>` + element['CTD_VISITAS'] + `</td>
                            <td>` + element['CTD_CLIENTES_UNICOS'] + `</td>
                            <td>` + element['CTD_PRIMERAS_VISITAS'] + `</td>
                        </tr>
                        `);
                document.getElementById('rankingVisitasBodyAcumulado').insertAdjacentHTML('beforeend', `
                        <tr>
                            <td>` + element['CTD_VISITAS_YTD'] + `</td>
                            <td>` + element['CTD_CLIENTES_UNICOS_YTD'] + `</td>
                            <td>` + element['CTD_PRIMERAS_VISITAS_YTD'] + `</td>
                        </tr>
                        `);
            } else {
                document.getElementById('rankingVisitasBody').insertAdjacentHTML('beforeend', `
                            <tr class="grey">
                                <td class="nombre">` + element['ENCARGADO'] + `</td>
                                <td>` + element['CTD_VISITAS'] + `</td>
                                <td>` + element['CTD_CLIENTES_UNICOS'] + `</td>
                                <td>` + element['CTD_PRIMERAS_VISITAS'] + `</td>
                            </tr>
                            `);
                document.getElementById('rankingVisitasBodyAcumulado').insertAdjacentHTML('beforeend', `
                        <tr class="grey">
                            <td>` + element['CTD_VISITAS_YTD'] + `</td>
                            <td>` + element['CTD_CLIENTES_UNICOS_YTD'] + `</td>
                            <td>` + element['CTD_PRIMERAS_VISITAS_YTD'] + `</td>
                        </tr>
                        `);
            }
        }
    }

    function resumenTopGraphic(data) {
        $('#canLeads').html(numberWithCommas(data['CTD_LEADS']));
        $('#colDirectas').html(milmillones(data['DEUDA_DIRECTA']));
        $('#colIndirectas').html(milmillones(data['DEUDA_INDIRECTA']));
        $('#cantVisitas').html(numberWithCommas(data['CTD_VISITAS']));
        if (window.gestionLeadsChart) window.gestionLeadsChart.destroy();
        $("#notaScore").html(funre4(data['PCT_GESTION'] * 100) + "%");
        nota = funre4(data['PCT_GESTION'] * 100) > 100.00 ? 100.00 : funre4(data['PCT_GESTION'] * 100);
        window.gestionLeadsChart = new Chart(gestionLeads, {
            type: 'doughnut',
            data: {
                datasets: [{

                    data: [nota, funre4(100.00 - nota)],
                    backgroundColor: [
                        '#4DD094',
                        'whitesmoke',
                    ],
                    borderWidth: 0.6
                }],
                labels: [
                    'Nota',
                    '',
                ]
            },
            options: {
                legend: {
                    display: false
                },
                cutoutPercentage: 70,
                responsive: true,
                tooltips: {
                    enabled: false
                },
            }
        });
        var pendiente = data['CTD_PEND'];
        var interesa = data['CTD_MEINT'];
        var contactado = data['CTD_CONTAC'];
        var evaluacion = data['EVAL'];
        var aprobado = data['CTD_APROB'];
        var noAprobado = data['CTD_DENEG'];
        var list = [{
            'valor': pendiente,
            'id': 'pendiente'
        }, {
            'valor': interesa,
            'id': 'interesa'
        }, {
            'valor': contactado,
            'id': 'contactado'
        }, {
            'valor': evaluacion,
            'id': 'evaluacion'
        }, {
            'valor': aprobado,
            'id': 'aprobado'
        }, {
            'valor': noAprobado,
            'id': 'noAprobado'
        }];
        var maxValue = Math.max(pendiente, interesa, contactado, evaluacion, aprobado, noAprobado);
        for (let index = 0; index < list.length; index++) {
            const element = list[index];
            const widthContainer = $('#' + element['id'] + 'Background').width();
            const width = (widthContainer / maxValue) * element['valor'];
            $('#' + element['id']).css('width', width + 'px');
            $('#' + element['id'] + 'Counter').html(element['valor'] > 999 ? milmillones(element['valor']) : element['valor']);
            $('#' + element['id'] + 'Tooltip').html(numberWithCommas(element['valor']));
        }
        var unMes = data['CTD_PERMA_1'];
        var dosMes = data['CTD_PERMA_2'];
        var tresMes = data['CTD_PERMA_3'];
        var cuatroMes = data['CTD_PERMA_4'];
        var cincoMes = data['CTD_PERMA_5'];
        var seisMes = data['CTD_PERMA_6'];
        var seisMasMes = data['CTD_PERMA_7'];
        var list = [{
            'valor': unMes,
            'id': 'unMes'
        }, {
            'valor': dosMes,
            'id': 'dosMes'
        }, {
            'valor': tresMes,
            'id': 'tresMes'
        }, {
            'valor': cuatroMes,
            'id': 'cuatroMes'
        }, {
            'valor': cincoMes,
            'id': 'cincoMes'
        }, {
            'valor': seisMes,
            'id': 'seisMes'
        }, {
            'valor': seisMasMes,
            'id': 'seisMasMes'
        }];
        var maxValue = Math.max(unMes, dosMes, tresMes, cuatroMes, cincoMes, seisMes, seisMasMes);
        for (let index = 0; index < list.length; index++) {
            const element = list[index];
            const heightContainer = 220;
            const height = (heightContainer / maxValue) * element['valor'];
            $('#' + element['id']).css('height', height + 'px');
            $('#' + element['id'] + 'Tooltip').html(numberWithCommas(element['valor']));
        }
    }

    function funre4(numero) {
        var flotante = parseFloat(numero);
        var resultado = (Math.round(flotante * 10, 0) / 10);
        return resultado;
    }

    function numberWithCommas(x) {
        if (x) {
            if (x != 0) {
                var parts = x.toString().split(".");
                parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                return parts.join(".");
            } else {
                return '0';
            }
        } else {
            return '0';
        }
    }

    function milmillones(number) {
        str = '';
        number = funre(number);
        if (Math.abs(number / 1000) >= 1) {
            str = numberWithCommas(Math.round(number / 1000)) + " MM";
        } else {
            str = numberWithCommas(Math.round(number)) + " M";
        }
        return str;
    }

    function funre(numero) { //Miles redondeado entero
        var flotante = parseFloat(numero);
        var resultado = Math.round(flotante / 1000);
        return resultado;
    }
</script>
@stop