<?php

use \App\Http\Controllers\GestioncarteraController; ?>
@extends('Layouts.layout')

@section('js-libs')
<script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.es.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/chart.bundle.js') }}"></script>

@stop

@section('content')
<?php
if (isset($usuario)) {
    $usu = $usuario->ID;
}
?>
<input type="text" hidden="hidden" id="id_usuario" value='{{$usu}}'>

<div class="row space"></div>
<div id="filtroNav" class=" section-dashboard row justify-content-md-center">
    @if($usu=='VPC')
    <div class="col-md-2 col-sm-2 col-xs-2 justify-content-center">
        <label>BANCA</label>
        <div class="form-group filtro">
            <select class="form-control" id="filtro1">
                @if(count($bancas)>=1)
                @foreach($bancas as $b)
                @if($b['BANCA'] != null)
                @if($b['BANCA'] == 'BC')
                <option value="{{$b['BANCA']}}" selected><?php echo $b['BANCA'] ?></option>
                @else
                <option value="{{$b['BANCA']}}"><?php echo $b['BANCA'] ?></option>
                @endif
                @endif
                @endforeach
                @endif
            </select>
        </div>
    </div>
    @endif
    @if(!isset($zonal))
    <div class="col-md-2 col-sm-2 col-xs-2 justify-content-center">
        <label>ZONAL</label>
        <div class="form-group filtro">
            <select class="form-control" id="filtro2">
                <option value="null" selected>Todos</option>
                @if(count($zonales)>=1)
                @foreach($zonales as $z)
                @if($z['NOMBRE_ZONAL'] != null)
                <option value="{{$z['NOMBRE_ZONAL']}}"><?php echo $z['NOMBRE_ZONAL'] ?></option>
                @endif
                @endforeach
                @endif
            </select>
        </div>
    </div>
    @endif
    @if(!isset($jefe) && !in_array($ROL,array('21','20')) && $jefes!=null)
    <div class="col-md-2 col-sm-2 col-xs-2 justify-content-center">
        <label>JEFATURA</label>
        <div class="form-group filtro">
            <select class="form-control" id="filtro3">
                <option value="null" selected>Todos</option>
                @if($jefes!=null && count($jefes)>=1)
                @foreach($jefes as $j)
                @if($j['NOMBRE_JEFE'] != null)
                <option value="{{$j['NOMBRE_JEFE']}}"><?php echo $j['NOMBRE_JEFE'] ?></option>
                @endif
                @endforeach
                @endif
            </select>
        </div>
    </div>
    @endif
    @if(!isset($ejecutivo) && $ejecutivos!=null)
    <div class="col-md-2 col-sm-2 col-xs-2 justify-content-center">
        <label>ENCARGADO</label>
        <div class="form-group filtro">
            <select class="form-control" id="filtro4">
                <option value="null" selected>Todos</option>
                @if($ejecutivos!=null && count($ejecutivos)>=1)
                @foreach($ejecutivos as $e)
                @if($e['ENCARGADO'] != null)
                <option value="{{$e['ENCARGADO']}}"><?php echo $e['ENCARGADO'] ?></option>
                @endif
                @endforeach
                @endif
            </select>
        </div>
    </div>
    <div class="col-md-2 col-sm-2 col-xs-2">
        <button class="NxtBtn" onclick="ActualizarFiltro()">Buscar</button>
    </div>
    @endif
</div>

<div class="row space"></div>
<div class="row section-dashboard">
    <div class="row hidebar"><i id="TableroNota" class="heatSensorSection  {{($usuario->ID == 'VPC') ? 'fa-chevron-down' :'fa-chevron-up'}}  hidebtn fas"></i></div>
    <div class="row graphic-top parentWrapper">
        <div id="TopSection" class="wrapper {{($usuario->ID == 'VPC') ? 'hideSectionUp' :''}} " style="padding-bottom:50px; width:100%;">

            <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="row" style="display: flex;">
                    <div class="col-md-4 col-sm-4 col-xs-4 align-self-center justify-content-center canClnt">
                        <p style="margin-bottom: 5px; font-size:1.5em; text-align: center;">Cant. Clientes<i style="font-size: 0.8em;margin-left: 8px;" data-toggle="tooltip" data-placement="top" title="Clientes con Saldos/Ingresos en los ult. 12 meses" id="CantCliInfo" class="fas fa-question-circle"></i></p>
                        <h3 id="cantCli" style="font-weight:bold; margin-top:0; font-size:2em;">0</h3>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-8 align-self-center justify-content-center">
                        <p class="GraphName">Nota</p>
                        <canvas class="canvasNota" id="nota"></canvas>
                        <div id="notaScore" class="score">0%</div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="row graphic-top">
                    <div class="col-md-6 col-sm-6 col-xs-6 align-self-center justify-content-center">
                        <div>
                            <p class="GraphName" style="font-weight:bold;">Financiera</p>
                            <div><canvas class="canvasNota" id="nota_financiera"></canvas></div>
                            <div id="notaFinan" class="score">0%</div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6 align-self-center justify-content-center">
                        <p class="GraphName" style="font-weight:bold;">Ecosistema</p>
                        <div><canvas class="canvasNota" id="nota_ecosistema"></canvas></div>
                        <div id="notaEco" class="score">0%</div>
                    </div>
                </div>
            </div>
        </div>
        <div style="width: 4.16%;
                min-height: 1px;
                float: left;"></div>
    </div>
</div>
</div>

<div id="navCartera" class="row tabs">
    <ul class="nav nav-tabs" style="border-bottom: 0px !important; font-size:1.35em !important">
        <li class="nav-item">
            <a id="finan-nav" class="nav-link nav-active">Financiera</a>
        </li>
        <li class="nav-item">
            <a id="eco-nav" class="nav-link">Ecosistema</a>
        </li>
    </ul>
</div>
<div id="Financiera" class="row graphic-btm">

    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-6" style="padding: 0;">
            <div class="row section-dashboard">
                <div class="row Title">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div style="font-weight: bold;">Actividades</div>
                        <span class="actualizaciones">Actualizado a: <span id="actualizacionActividades"></span></span>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="row hidebar"><i id="ActividadesFinanciera" class="heatSensorSection hidebtn fas fa-chevron-up"></i></div>
                    </div>
                </div>
                <div class="row parentWrapper" style="padding-top: 30px;">
                    <div id="TopSection2" class="wrapper">
                        <div class="table-responsive" style="width: 100%;">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th class="center" scope="col" style="width:100px">Actividades</th>
                                        <th class="center" scope="col" colspan="2">Volumen
                                        </th>
                                        <th class="center" scope="col">Var. Mes
                                        </th>
                                        <th class="center" scope="col">Ppto. Mes
                                        </th>
                                        <th class="center" scope="col">Ppto. Anual
                                        </th>
                                    </tr>
                                </thead>
                                <thead>
                                    <tr>
                                        <th class="center" scope="col" style="width:100px"></th>
                                        <th class="center" scope="col">Punta</th>
                                        <th class="center" scope="col">Prom</th>
                                        <th class="center" scope="col"></th>
                                        <th class="center" scope="col"></th>
                                        <th class="center" scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr style="background: whitesmoke">
                                        <th scope="row" style="width:100px">
                                            <div class="row rowTable semaforoLegend">
                                                <div style="text-align: left;">Colocaciones Directas</div>
                                            </div>
                                        </th>
                                        <td scope="col" colspan="2" class="center">
                                            <div class="space row"></div>
                                            <div class="row">
                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                    <label class="chart_label" id="coldfinal"></label>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                    <label class="chart_label" id="coldfinalPROM"></label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <canvas class="lil_chart" id="coldirectas"></canvas>
                                            </div>
                                        </td>
                                        <td class="center">
                                            <div class="space row"></div>
                                            <div class="row"><label class="table_label" id="varcoldirectas"></label></div>
                                            <div class="row"><label class="porcVar" id="porcVarDir"></label></div>
                                        </td>
                                        <td class="center">
                                            <div class="space row"></div>
                                            <label class="table_label" id="pptocoldirectas"></label>
                                            <div class="semaforoWrapper">
                                                <div class="semaforo" id="cumplimientocoldirectasSEM"></div>
                                                <label class="cumplimiento_label" id="cumplimientocoldirectas" style="color: #73879C !important"></label>
                                            </div>
                                        </td>
                                        <td class="center">
                                            <div class="space row"></div>
                                            <label class="table_label" id="pptocoldirectasANUAL"></label>
                                            <div class="semaforoWrapper">
                                                <div class="semaforo" id="avancecoldirectasSEM"></div>
                                                <label class="cumplimiento_label" id="avancecoldirectas" style="color: #73879C !important"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="width:100px" class="align-middle" scope="row">
                                            <div class="row rowTable semaforoLegend">
                                                <div style="text-align: left;">Colocaciones Indirectas</div>
                                            </div>
                                        </th>
                                        <td scope="col" colspan="2" class="center">
                                            <div class="space row"></div>
                                            <div class="row">
                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                    <label class="chart_label" id="colindfinal"></label>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                    <label class="chart_label" id="colindfinalPROM"></label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <canvas class="lil_chart" id="colindirectas"></canvas>
                                            </div>
                                        </td>
                                        <td class="center">
                                            <div class="space row"></div>
                                            <div class="row"><label class="table_label row" id="varcolindirectas"></label></div>
                                            <div class="row"><label class="porcVar row" id="porcVarInd"></label></div>
                                        </td>
                                        <td class="center">
                                            <div class="space row"></div>
                                            <label class="table_label row" id="pptocolindirectas"></label>
                                            <div class="semaforoWrapper">
                                                <div class="semaforo" id="cumplimientocolindirectasSEM"></div>
                                                <label class="cumplimiento_label" id="cumplimientocolindirectas" style="color: #73879C !important"></label>
                                            </div>
                                        </td>
                                        <td class="center">
                                            <div class="space row"></div>
                                            <label class="table_label row" id="pptocolindirectasANUAL"></label>
                                            <div class="semaforoWrapper">
                                                <div class="semaforo" id="avancecolindirectasSEM"></div>
                                                <label class="cumplimiento_label" id="avancecolindirectas" style="color: #73879C !important"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr style="background: whitesmoke;">
                                        <th style="width:100px" class="align-middle" scope="row">
                                            <div class="row rowTable semaforoLegend">
                                                <div style="text-align: left;">Depósitos</div>
                                            </div>
                                        </th>
                                        <td scope="col" colspan="2" class="center">
                                            <div class="space row"></div>
                                            <div class="row">
                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                    <label class="chart_label" id="depfinal"></label>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                    <label class="chart_label" id="depfinalPROM"></label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <canvas class="lil_chart" id="depositos"></canvas>
                                            </div>
                                        </td>
                                        <td class="center">
                                            <div class="space row"></div>
                                            <div class="row"><label class="table_label row" id="vardeposito"></label></div>
                                            <div class="row"><label class="porcVar row" id="porcVarDep"></label></div>
                                        </td>
                                        <td class="center">
                                            <div class="space row"></div>
                                            <label class="table_label row" id="pptodeposito"></label>
                                            <div class="semaforoWrapper">
                                                <div class="semaforo" id="cumplimientodepSEM"></div>
                                                <label class="cumplimiento_label" id="cumplimientodep" style="color: #73879C !important"></label>
                                            </div>
                                        </td>
                                        <td class="center">
                                            <div class="space row"></div>
                                            <label class="table_label row" id="pptodepositoANUAL"></label>
                                            <div class="semaforoWrapper">
                                                <div class="semaforo" id="avancedepSEM"></div>
                                                <label class="cumplimiento_label" id="avancedep" style="color: #73879C !important"></label>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row space"></div>
            <div class="row space"></div>
            <div class="row space"></div>

            <div class="row section-dashboard">
                <div class="row Title">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div style="font-weight: bold;">Lineas</div>
                        <span class="actualizaciones">Actualizado a: <span id="actualizacionLineas"></span></span>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="row hidebar"><i id="LineasFinanciera" class="heatSensorSection hidebtn fas fa-chevron-up"></i></div>
                    </div>
                </div>
                <div class="row parentWrapper">
                    <div id="TopSection3" class="wrapper">
                        <div class="row space"></div>
                        <div class="row datosLinea" style="display: flex;">
                            <div style=" width: 50px; display: flex;flex-direction:column;align-items: flex-end;font-size: 0.9em;justify-content: flex-end;padding-bottom: 15px;font-weight: bold;">
                                <p>Ult. Valor</p>
                                <p>Var. Mes</p>
                            </div>
                            <div style="width:calc(100% - 50px)">
                                <div class="col-md-2 col-sm-2 col-xs-2 text-center celda">
                                    <div class="titleNumero" style="font-weight: bold;">Cantidad</div>
                                    <h3 id="CL" class="scorish" style="font-weight: bold;">0</h3>
                                    <label id="varCL">0</label>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-2 text-center celda">
                                    <div class="titleNumero" style="font-weight: bold;">% Vigencia</div>
                                    <h3 id="PORV" class="scorish" style="font-weight: bold;">0</h3>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-3 text-center celda">
                                    <div class="titleNumero" style="font-weight: bold;">A vencer<i data-toggle="tooltip" data-placement="top" title="Hasta el cierre del siguiente mes" id="avanceInfo" class="fas fa-question-circle"></i></div>
                                    <h3 id="LVE" class="scorish" style="font-weight: bold;">0</h3>
                                </div>

                                <div class="col-md-2 col-sm-2 col-xs-2 text-center celda">
                                    <div class="titleNumero" style="font-weight: bold;">Riesgo Max</div>
                                    <h3 id="RM" class="scorish" style="font-weight: bold;">0</h3>
                                    <label id="varRM">0</label>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-3 text-center celda">
                                    <div class="titleNumero" style="font-weight: bold;">Utilización</div>
                                    <h3 id="U" class="scorish" style="font-weight: bold;">0</h3>
                                    <label id="varU">0</label>
                                </div>
                            </div>
                        </div>
                        <div class="row space"></div>
                    </div>
                </div>
            </div>
            <div class="row space"></div>
            <div class="row space"></div>
            <div class="row space"></div>
            <div class="row space"></div>


        </div>

        <div class="col-md-6 col-sm-6 col-xs-6">
            <div class="row section-dashboard">
                <div class="row Title">
                    <div class="col-md-7 col-sm-7 col-xs-7">
                        <div style="font-weight: bold;">Resumen de Variaciones</div>
                    </div>
                    <div class="menu col-md-5 col-sm-5 col-xs-5">
                        <div style="width: 120px;" class="form-group filtro">
                            <select class="form-control" id="filtroProducto">
                                @foreach($tipoProductos as $t)
                                @if($t['TIPOPRODUCTO'] != null)
                                <option value="{{$t['TIPOPRODUCTO']}}" <?php if ($t['TIPOPRODUCTO'] == 'Col. Directas') {
                                                                            echo 'selected';
                                                                        } ?>><?php echo $t['TIPOPRODUCTO'] ?></option>
                                @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div style="margin-left:8.333%">
                        <div class="row hidebar"><i id="VariacionesActividades" class="heatSensorSection hidebtn fas fa-chevron-up"></i></div>
                    </div>
                </div>
                <div class="space row"></div>
                <div class="parentWrapper">
                    <div id="TopSection4" class="wrapper">
                        <div class="row">

                            <canvas class="heatSensorGrafico" id="VarXProducto"></canvas>
                        </div>
                        <div class="row text-center" style="padding-top: 20px;">
                            <span style="font-size:1.3em;">Var. diaria:</span>
                            <span id="ultvar" style="font-size:1.3em; font-weight:bold;">0</span>
                            <span style="margin:30px;"></span>
                            <span style="font-size:1.3em;">Var. Ppt:</span>
                            <span id="ultvarPpt" style="font-size:1.3em; font-weight:bold;">0</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row space"></div>
            <div class="row section-dashboard">
                <div class="row Title">
                    <div class="col-md-5 col-sm-5 col-xs-5" style="display:flex;align-items: center;">
                        <div id="dynamicTitle" style="font-weight: bold; width: 100%;">Principales Variaciones<br> Col. Directas<br> TOP 15</div>
                        <i id="topBtm" class="menuItem fas fa-arrow-down" style="
                        font-size: 1em;
                        margin-right: 20px;"></i>
                    </div>
                    <div class="menu col-md-6 col-sm-6 col-xs-6">
                        <div style="width: 120px;" class="form-group filtro">
                            <select class="form-control" id="filtroFrecuencia">
                                @foreach($tipoFrecuencia as $t)
                                @if($t['TIPO_VAR'] != null)
                                <option value="{{$t['TIPO_VAR']}}" <?php if ($t['TIPO_VAR'] == 'Diaria') {
                                                                        echo 'selected';
                                                                    } ?>><?php echo $t['TIPO_VAR'] ?></option>
                                @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div style="margin-left:8.333%">
                        <div class="row hidebar"><i id="VariacionesClientesFinan" class="heatSensorSection hidebtn fas fa-chevron-up"></i></div>
                    </div>
                </div>
                <div class="parentWrapper">

                    <div style="height: 348px; overflow-y:scroll;" id="TopSection5" class="wrapper">

                        <div id="AvatarList" class="row">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row space"></div>
    <div class="row Title">
        <h1 class="scorish" style="font-weight: bold;font-size: 1.5em;">Sistema Financiero</h1>
    </div>

    <div class="row">
        <div class="col-md-6 col-xs-6 col-sm-6" style="padding:0">
            <div class="row section-dashboard">
                <div class="row Title">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div style="font-weight: bold;">Mercado</div>
                        <span class="actualizaciones">Actualizado a: <span id="actualizacionMercado"></span></span>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="row hidebar"><i id="MercadoResumen" class="heatSensorSection hidebtn fas fa-chevron-up"></i></div>
                    </div>
                </div>
                <div class="row parentWrapper">
                    <div id="TopSection6" class="wrapper">
                        <div class="row space"></div>
                        <div class="row datosLinea" style="display:flex;">
                            <div style=" width: 50px; display: flex;flex-direction:column;align-items: flex-end;font-size: 0.9em;justify-content: flex-end;padding-bottom: 15px;font-weight: bold;">
                                <p>Ult. Valor</p>
                                <p>Var. Mes</p>
                            </div>
                            <div style="width:calc(100% - 50px)">
                                <div class="col-md-3 col-sm-3 col-xs-3 text-center celda">
                                    <div class="titleNumero" style="font-weight: bold;">Clientes Col SF</div>
                                    <h3 id="CLI" class="scorish" style="font-weight: bold;">0</h3>
                                    <label id="varCLI">0</label>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-3 text-center celda">
                                    <div class="titleNumero" style="font-weight: bold;">Col. Directas SF</div>
                                    <h3 id="CD" class="scorish" style="font-weight: bold;">0</h3>
                                    <label id="varCD">0</label>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-3 text-center celda">
                                    <div class="titleNumero" style="font-weight: bold;">Col. Indirectas SF</div>
                                    <h3 id="CI" class="scorish" style="font-weight: bold;">0</h3>
                                    <label id="varCI">0</label>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-3 text-center celda">
                                    <div class="titleNumero" style="font-weight: bold;">Garantías SF</div>
                                    <h3 id="GAR" class="scorish" style="font-weight: bold;">0</h3>
                                    <label id="varGAR">0</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row space"></div>
            <div class="row section-dashboard">
                <div class="row Title" style="padding-right: 10px;">
                    <div class="col-md-5 col-sm-5 col-xs-5" style="display:flex;align-items: center;">
                        <div style="font-weight: bold;">Participación por bancos</div>
                    </div>
                    <div class="menu col-md-6 col-sm-6 col-xs-6">
                        <div style="width: 120px;" class="form-group filtro">
                            <select class="form-control" id="filtroProductoBtn">
                                @foreach($tipoProductosBtm as $t)
                                @if($t['TIPOPRODUCTO'] != null)
                                <option value="{{$t['TIPOPRODUCTO']}}" <?php if ($t['TIPOPRODUCTO'] == 'Col. Directas') {
                                                                            echo 'selected';
                                                                        } ?>><?php echo $t['TIPOPRODUCTO'] ?></option>
                                @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div style="margin-left:8.333%">
                        <div class="row hidebar"><i id="ParticipacionBanco" class="heatSensorSection hidebtn fas fa-chevron-up"></i></div>
                    </div>
                </div>
                <div class="row graficos parentWrapper">
                    <div id="TopSection7" class="wrapper">
                        <div class="space row"></div>

                        <div class="row">
                            <div class="col-md-8 col-sm-8 col-xs-8" style="display: flex;align-items: center;justify-content: center; flex-direction:column;">
                                <label style="font-size: 1.3em">Evolutivo de Saldos</label>
                                <canvas class="heatSensorGrafico" style="width:100%" id="LineChartBtnDiv"></canvas>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4" style="display: flex;align-items: center;justify-content: center; flex-direction:column;">
                                <label style="font-size: 1.3em">SOW</label>
                                <canvas style="width: 100% !important" class="heatSensorGrafico" id="PieChartBtmDiv"></canvas>
                            </div>
                        </div>
                        <div class="space row"></div>
                        <div class="space row"></div>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">Bancos</th>
                                        <th scope="col">Saldo Act.</td>
                                        <th scope="col" colspan="2">Var. mes</td>
                                        <th scope="col" colspan="2">Year to date</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">
                                            <div class="row rowTable semaforoLegend">
                                                <div class="semaforo" style="margin-right: 5px; background: rgb(77, 196, 135)"></div>
                                                <div>IBK</div>
                                            </div>
                                        </th>
                                        <td><label id="IBKULT"></label></td>
                                        <td><label id="IBK"></label></td>
                                        <td><label id="IBKPor"></label></td>
                                        <td><label id="IBKY2D"></label></td>
                                        <td><label id="IBKPorY2D"></label></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">
                                            <div class="row rowTable semaforoLegend">
                                                <div class="semaforo" style="margin-right: 5px; background: rgb(197, 90, 17)"></div>
                                                <div>BCP</div>
                                            </div>
                                        </th>
                                        <td><label id="BCPULT"></label></td>
                                        <td><label id="BCP"></label></td>
                                        <td><label id="BCPPor"></label></td>
                                        <td><label id="BCPY2D"></label></td>
                                        <td><label id="BCPPorY2D"></label></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">
                                            <div class="row rowTable semaforoLegend">
                                                <div class="semaforo" style="margin-right: 5px; background:rgb(0, 112, 192)"></div>
                                                <div>BBVA</div>
                                            </div>
                                        </th>
                                        <td><label id="BBVAULT"></label></td>
                                        <td><label id="BBVA"></label></td>
                                        <td><label id="BBVAPor"></label></td>
                                        <td><label id="BBVAY2D"></label></td>
                                        <td><label id="BBVAPorY2D"></label></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">
                                            <div class="row rowTable semaforoLegend">
                                                <div class="semaforo" style="margin-right: 5px; background:rgb(255, 0, 0)"></div>
                                                <div>SCOTIA</div>
                                            </div>
                                        </th>
                                        <td><label id="SCOTIAULT"></label></td>
                                        <td><label id="SCOTIA"></label></td>
                                        <td><label id="SCOTIAPor"></label></td>
                                        <td><label id="SCOTIAY2D"></label></td>
                                        <td><label id="SCOTIAPorY2D"></label></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">
                                            <div class="row rowTable semaforoLegend">
                                                <div class="semaforo" style="margin-right: 5px; background:rgb(0, 176, 240)"></div>
                                                <div>BIF</div>
                                            </div>
                                        </th>
                                        <td><label id="BIFULT"></label></td>
                                        <td><label id="BIF"></label></td>
                                        <td><label id="BIFPor"></label></td>
                                        <td><label id="BIFY2D"></label></td>
                                        <td><label id="BIFPorY2D"></label></td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xs-6 col-sm-6">
            <div class="row section-dashboard">
                <div class="row Title">
                    <div class="col-md-5 col-sm-5 col-xs-5" style="display:flex;align-items: center;">
                        <div id="dynamicTitleMERCADO" style="font-weight: bold; width: 100%;">Principales Variaciones<br> Col. Directas<br> TOP 15</div>
                        <i id="topBtmMERCADO" class="menuItem fas fa-arrow-down" style="
                        font-size: 1em;
                        margin-right: 20px;"></i>
                    </div>
                    <div class="menu col-md-3 col-sm-3 col-xs-3">
                        <div style="width: 120px;" class="form-group filtro">
                            <select class="form-control" id="filtroFrecuenciaMERCADO">
                                @foreach($tipoFrecuenciaMERCADO as $t)
                                @if($t['TIPO_VARIACION'] != null)
                                <option value="{{$t['TIPO_VARIACION']}}" <?php if ($t['TIPO_VARIACION'] == 'Mensual') {
                                                                                echo 'selected';
                                                                            } ?>><?php echo $t['TIPO_VARIACION'] ?></option>
                                @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="menu col-md-3 col-sm-3 col-xs-3">
                        <div style="width: 120px;" class="form-group filtro">
                            <select class="form-control" id="filtroBANCOMERCADO">
                                <option value="TODOS" selected>TODOS</option>
                                @foreach($tipoBANCOMERCADO as $t)
                                @if($t['BANCO'] != null)
                                @if($t['BANCO'] != 'TODOS')
                                <option value="{{$t['BANCO']}}"><?php echo $t['BANCO'] ?></option>
                                @endif
                                @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div style="margin-left:8.333%">
                        <div class="row hidebar"><i id="VarcionesSF" class="heatSensorSection hidebtn fas fa-chevron-up"></i></div>
                    </div>
                </div>
                <div class="parentWrapper">

                    <div style="height: 608px; overflow-y:scroll;" id="TopSection23" class="wrapper">

                        <div id="AvatarListMERCADO" class="row">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="Ecosistema" class="row graphic-btm disappear">
    <div class="col-md-6 col-sm-6 col-xs-6">
        <div class="row section-dashboard">
            <div class="row Title">
                <div class="col-md-6 col-sm-6 col-xs-6" style="padding:0">
                    <div style="font-weight: bold;">Actividades</div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <div class="row hidebar"><i id="TophideBtn12" class=" hidebtn fas fa-chevron-up"></i></div>
                </div>
            </div>
            <div class="row parentWrapper" style="padding-top: 30px;">
                <div id="TopSection12" class="wrapper">
                    <div class="table-responsive" style="width: 100%;">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="center" scope="col">Actividades</th>
                                    <th class="center" scope="col" colspan="2">Volumen
                                    </th>
                                    <th class="center" scope="col">Var. Mes
                                    </th>
                                    <th class="center" scope="col">Ppto. Mes
                                    </th>
                                    <th class="center" scope="col">Ppto. Anual
                                    </th>
                                </tr>
                            </thead>
                            <thead>
                                <tr>
                                    <th class="center" scope="col"></th>
                                    <th class="center" scope="col">Mes</th>
                                    <th class="center" scope="col">Acumulado</th>
                                    <th class="center" scope="col"></th>
                                    <th class="center" scope="col"></th>
                                    <th class="center" scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr style="background: whitesmoke">
                                    <th scope="row">
                                        <div class="row rowTable semaforoLegend">
                                            <div style="text-align: left;">Descuentos y Factoring</div>
                                        </div>
                                    </th>
                                    <td scope="col" colspan="2" class="center">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <label class="chart_label" id="dfactoringULT"></label>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <label class="chart_label" id="dfactoringACUM"></label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <canvas class="lil_chart" id="DFactoring"></canvas>
                                        </div>
                                    </td>
                                    <td class="center" style="display: flex;flex-direction: column;">
                                        <label style="display:block;" class="table_label" id="dfactoringVAR"></label>
                                        <label class="porcVar" id="dfactoringVARPCT"></label>
                                    </td>
                                    <td class="center">
                                        <label style="display:block;" class="table_label" id="dfactoringPPTO"></label>
                                        <div class="semaforoWrapper">
                                            <div class="semaforo" style="margin-right: 5px;" id="dfactoringPPTOPCTSEM"></div>
                                            <label style="font-size:0.8em; margin:0 !important" id="dfactoringPPTOPCT"></label>
                                        </div>
                                    </td>
                                    <td class="center">
                                        <label style="display:block;" class="table_label" id="dfactoringPPTOACUM"></label>

                                        <div class="semaforoWrapper">
                                            <div class="semaforo" style="margin-right: 5px;" id="dfactoringPPTOACUMPCTSEM"></div>
                                            <label style="font-size:0.8em; margin:0 !important" id="dfactoringPPTOACUMPCT"></label>

                                        </div>
                                    </td>
                                </tr>
                                <tr style="border-bottom-style: ridge;border-bottom-width: 0.1px;border-bottom-color: lightgray;">
                                    <th class="align-middle" scope="row">
                                        <div class="row rowTable semaforoLegend">
                                            <div style="text-align: left;">Volumen Pago Proveedores</div>
                                        </div>
                                    </th>
                                    <td scope="col" colspan="2" class="center">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <label class="chart_label" id="vproveedoresULT"></label>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <label class="chart_label" id="vproveedoresACUM"></label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <canvas class="lil_chart" id="VProveedores"></canvas>
                                        </div>
                                    </td>
                                    <td class="center">
                                        <label class="table_label row" id="vproveedoresVAR"></label>
                                        <label class="porcVar row" id="vproveedoresVARPCT"></label>
                                    </td>
                                    <td class="center">
                                        <label class="table_label row" id="vproveedoresPPTO"></label>
                                        <div class="semaforoWrapper">
                                            <div class="semaforo" style="margin-right: 5px;" id="vproveedoresPPTOPCTSEM"></div>
                                            <label style="font-size:0.8em; margin: 0 !important;" id="vproveedoresPPTOPCT"></label>
                                        </div>
                                    </td>
                                    <td class="center">
                                        <label class="table_label row" id="vproveedoresPPTOACUM"></label>
                                        <div class="semaforoWrapper">
                                            <div class="semaforo" style="margin-right: 5px;" id="vproveedoresPPTOACUMPCTSEM"></div>
                                            <label style="font-size:0.8em ;margin: 0 !important;" id="vproveedoresPPTOACUMPCT"></label>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="row depositos disappear">
                        <div class="col-md-2 col-sm-2 col-xs-2">
                            <div style="font-weight: bold;">Cuenta Sueldo</div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-3">
                            <label class="chart_label" id="csueldoULT"></label>
                            <canvas class="lil_chart" id="CSueldo"></canvas>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-2 collabel">
                            <label class="table_label" id="csueldoPPTO"></label>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-2 collabel">
                            <label class="table_label" id="csueldoVAR"></label>
                            <label class="porcVar" id="csueldoVARPCT"></label>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-3">
                            <div class="CumplWrapper">
                                <div class="Cumpl" id="csueldoCUMPLBAR"></div>
                            </div>
                            <label class="cumplimiento_label" id="csueldoCUMPL"></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row space"></div>

        <div class="row section-dashboard">
            <div class="row Title" style="padding-right: 10px;">
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <div style="font-weight: bold;">Descuentos</div>
                    <span class="actualizaciones">Actualizado a: <span id="actualizacionDES"></span></span>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <div class="row hidebar"><i id="TophideBtn10" class=" hidebtn fas fa-chevron-up"></i></div>
                </div>
            </div>
            <div class="row graficos parentWrapper">
                <div id="TopSection10" class="wrapper">
                    <div class="space row"></div>
                    <div class="row">
                        <div class="row" style="display: flex;">
                            <div style=" width: 50px; display: flex;flex-direction:column;align-items: flex-end;font-size: 0.9em;justify-content: flex-end;padding-bottom: 15px;font-weight: bold;">
                                <p>Ult. Valor</p>
                                <p>Var. Mes</p>
                            </div>
                            <div style="width: calc(100% - 50px)">
                                <div class="row datosLinea">
                                    <div class="col-md-3 col-sm-3 col-xs-3 text-center celda">
                                        <div style="font-weight: bold;">Volumen a vencer</div>
                                        <h3 id="VV" class="scorish" style="font-weight: bold;">0</h3>
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-3 text-center celda">
                                        <div style="font-weight: bold;">Incrementos</div>
                                        <h3 id="Inc" class="scorish" style="font-weight: bold;">0</h3>
                                        <label id="VarInc">0</label>
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-3 text-center celda">
                                        <div style="font-weight: bold;">Caidas</div>
                                        <h3 id="Cai" class="scorish" style="font-weight: bold;">0</h3>
                                        <label id="varCai">0</label>
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-3 text-center celda">
                                        <div style="font-weight: bold;">Dsctos SF</div>
                                        <h3 id="DsctSF" class="scorish" style="font-weight: bold;">0</h3>
                                        <label id="varDsctSF">0</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="space row"></div>
                        <div class="row">
                            <label>Descuentos por producto</label>
                            <div class="space row"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12" style="display: flex;align-items: center;justify-content: center;">
                                <canvas class="heatSensorGrafico" style="width:100%" id="Descuentos"></canvas>
                            </div>
                        </div>
                        <div class="row space"></div>
                        <div class="row" style="display: flex;">
                            <div class="table-responsive" style="width: 100%;">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th class="center" scope="col"></th>
                                            <th class="center" scope="col">Saldos</th>
                                            <th class="center" scope="col" colspan="2">Var. Mes</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row">
                                                <div class="row rowTable semaforoLegend">
                                                    <div>F. Negociable:</div>
                                                </div>
                                            </th>
                                            <td class="center">
                                                <label style="text-align: end;" id="SaldoFNULT">0</label>
                                            </td>
                                            <td class="center">
                                                <label style="text-align: end;" id="SaldoFN">0</label>
                                            </td>
                                            <td class="center">
                                                <label style="text-align: end;" id="VarSaldoFN">0</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">
                                                <div class="row rowTable semaforoLegend">
                                                    <div>Dcto. Elec.:</div>
                                                </div>
                                            </th>
                                            <td class="center">
                                                <label style="text-align: end;" id="SaldoDEULT">0</label>
                                            </td>
                                            <td class="center">
                                                <label style="text-align: end;" id="SaldoDE">0</label>
                                            </td>
                                            <td class="center">
                                                <label style="text-align: end;" id="VarSaldoDE">0</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">
                                                <div class="row rowTable semaforoLegend">
                                                    <div>Dcto. Físico:</div>
                                                </div>
                                            </th>
                                            <td class="center">
                                                <label style="text-align: end;" id="SaldoDFFULT">0</label>
                                            </td>
                                            <td class="center">
                                                <label style="text-align: end;" id="SaldoDF">0</label>
                                            </td>
                                            <td class="center">
                                                <label style="text-align: end;" id="VarSaldoDFF">0</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">
                                                <div class="row rowTable semaforoLegend">
                                                    <div>Dcto. Letras:</div>
                                                </div>
                                            </th>
                                            <td class="center">
                                                <label style="text-align: end;" id="SaldoDLULT">0</label>
                                            </td>
                                            <td class="center">
                                                <label style="text-align: end;" id="SaldoDL">0</label>
                                            </td>
                                            <td class="center">
                                                <label style="text-align: end;" id="VarSaldoDL">0</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">
                                                <div class="row rowTable semaforoLegend">
                                                    <div>Dcto. Pagarés:</div>
                                                </div>
                                            </th>
                                            <td class="center">
                                                <label style="text-align: end;" id="SaldoDPULT">0</label>
                                            </td>
                                            <td class="center">
                                                <label style="text-align: end;" id="SaldoDP">0</label>
                                            </td>
                                            <td class="center">
                                                <label style="text-align: end;" id="VarSaldoDP">0</label>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="space row"></div>
                </div>
            </div>
        </div>
        <div class="row space"></div>
    </div>

    <div style="width: 3%;
                min-height: 1px;
                float: left;"></div>

    <div class="col-md-6 col-sm-6 col-xs-6">
        <div class="row section-dashboard">

            <div class="row Title" style="padding-right: 10px;">
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <div style="font-weight: bold;">Pago Proveedores</div>
                    <span class="actualizaciones">Actualizado a: <span id="actualizacionPAP"></span></span>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <div class="row hidebar"><i id="TophideBtn8" class=" hidebtn fas fa-chevron-up"></i></div>
                </div>
            </div>
            <div class="parentWrapper">

                <div id="TopSection8" class="wrapper">

                    <div class="space row"></div>
                    <div class="row">
                        <div class="row" style="display: flex;">
                            <div class="table-responsive" style="width: 100%;">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th class="center" scope="col"></th>
                                            <th class="center" scope="col">Ordenantes</td>
                                            <th class="center" scope="col">Beneficiarios</td>
                                            <th class="center" scope="col">Nuevos beneficiarios</td>
                                            <th class="center" scope="col">Con 1 y 2 Pagos</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row">
                                                <div class="row rowTable semaforoLegend">
                                                    <div>Ult. Valor</div>
                                                </div>
                                            </th>
                                            <td class="center">
                                                <label id="ORD" style="font-weight: bold;">0</label>
                                            </td>
                                            <td class="center">
                                                <label id="BEN" style="font-weight: bold;">0</label>
                                            </td>
                                            <td class="center">
                                                <label id="CP12" style="font-weight: bold;">0</label>
                                            </td>
                                            <td class="center">
                                                <label id="POT" style="font-weight: bold;">0</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">
                                                <div class="row rowTable semaforoLegend">
                                                    <div>Var. Mes</div>
                                                </div>
                                            </th>
                                            <td class="center">
                                                <h3 id="varORD" class="scorish" style="font-weight: bold;">0</h3>
                                            </td>
                                            <td class="center">
                                                <h3 id="varBEN" class="scorish" style="font-weight: bold;">0</h3>
                                            </td>
                                            <td class="center">
                                                <h3 id="varCP12" class="scorish" style="font-weight: bold;">0</h3>
                                            </td>
                                            <td class="center">
                                                <h3 id="varPOT" class="scorish" style="font-weight: bold;">0</h3>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row" style="display: flex;align-items: center;justify-content: center;">
                            <canvas class="heatSensorGrafico" id="PagoProveedores" style="width:100%"></canvas>
                        </div>
                        <div class="space row"></div>
                        <div class="row" style="display: flex;">
                            <div class="table-responsive" style="width: 100%;">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th class="center" scope="col"></th>
                                            <th class="center" scope="col">Volumen Mes</td>
                                            <th class="center" scope="col" colspan="2">Var. Mes</td>
                                            <th class="center" scope="col">Acum. YTD</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row">
                                                <div>Abono en cuentas:</div>
                                            </th>
                                            <td class="center">
                                                <label style="text-align: end;" id="IBC">0</label>
                                            </td>
                                            <td class="center">
                                                <label style="text-align: end;" id="varIBC">0</label>
                                            </td>
                                            <td class="center">
                                                <label style="text-align: end;" id="PCTvarIBC">0</label>
                                            </td>
                                            <td class="center">
                                                <label style="text-align: end;" id="IBCACUM">0</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">
                                                <div>Cheque + interbancario (Con cuenta):</div>
                                            </th>
                                            <td class="center">
                                                <label style="text-align: end;" id="IBCN">0</label>
                                            </td>
                                            <td class="center">
                                                <label style="text-align: end;" id="varIBCN">0</label>
                                            </td>
                                            <td class="center">
                                                <label style="text-align: end;" id="PCTvarIBCN">0</label>
                                            </td>
                                            <td class="center">
                                                <label style="text-align: end;" id="IBCNACUM">0</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">
                                                <div>Cheque + interbancario (Sin cuenta):</div>
                                            </th>
                                            <td class="center">
                                                <label style="text-align: end;" id="CIBK">0</label>
                                            </td>
                                            <td class="center">
                                                <label style="text-align: end;" id="varCIBK">0</label>
                                            </td>
                                            <td class="center">
                                                <label style="text-align: end;" id="PCTvarCIBK">0</label>
                                            </td>
                                            <td class="center">
                                                <label style="text-align: end;" id="CIBKACUM">0</label>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="space row"></div>
                </div>
            </div>
        </div>
        <div class="row space"></div>
        <div class="row Title disappear" style="padding-right: 10px;">
            <div class="col-md-6 col-sm-6 col-xs-6">
                <div style="font-weight: bold;">Cuenta Sueldo (Abono Foco)</div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="row hidebar"><i id="TophideBtn9" class=" hidebtn fas fa-chevron-up"></i></div>
            </div>
        </div>
        <div class="parentWrapper disappear">
            <div id="TopSection9" class="wrapper">
                <div class="row">
                    <canvas class="heatSensorGrafico" style=" height: 300px;width: 100%;" id="CuentaSueldo"></canvas>
                </div>
                <div class="row text-center" style="padding-top: 20px;">
                    <span style="font-size:1.3em;">Var. Mensual:</span>
                    <span id="ultVarEco" style="font-size:1.3em; font-weight:bold;">0</span>
                    <span style="margin:30px;"></span>
                    <span style="font-size:1.3em;">Var. PPT:</span>
                    <span id="ultVarPptoEco" style="font-size:1.3em; font-weight:bold;">0</span>
                </div>
            </div>
        </div>
        <div class="row space"></div>
    </div>
</div>
<div class="row space"></div>
<div class="row space"></div>
<div id="maskModal">
    <div id="maskModalWrapper" style="position: absolute; z-index: -9999999;">
        <div class="row" style="display: flex; flex-direction: row;margin-top: 90px !important; ">
            <div id="Mercado" class="BtnModalTab activeTab" style="margin-left: 12px;"><label>Mercado</label></div>
            <div style="left: 300px !important" id="Interbank" class="BtnModalTab"><label>Interbank</label></div>
        </div>
        <div id="maskOverflowWrapper">

            <div class="row">
                <div id="InterbankBody" class="col-md-12 col-sm-12 col-xs-12 disappear" style="padding-left: 20px;">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Bancos</th>
                                    <th scope="col">Acum. 2019</td>
                                    <th scope="col" colspan="2">Var Acum. 2018</td>
                                    <th scope="col" colspan="2">Var Acum. cierre 2018</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">
                                        <div class="row rowTable semaforoLegend">
                                            <div>MF. Directas</div>
                                        </div>
                                    </th>
                                    <td><label id="SALDOACTDIR" class="row rowTable"></label></td>
                                    <td><label id="Y2DDIR" class="row rowTable"></label></td>
                                    <td><label id="PCTY2DDIR" class="row rowTable"></label></td>
                                    <td><label id="VADIR" class="row rowTable"></label></td>
                                    <td><label id="PCTVADIR" class="row rowTable"></label></td>
                                </tr>
                                <tr>
                                    <th scope="row">
                                        <div class="row rowTable semaforoLegend">
                                            <div>MF. Depósitos</div>
                                        </div>
                                    </th>
                                    <td><label id="SALDOACTDEP" class="row rowTable"></label></td>
                                    <td><label id="Y2DDEP" class="row rowTable"></label></td>
                                    <td><label id="PCTY2DDEP" class="row rowTable"></label></td>
                                    <td><label id="VADEP" class="row rowTable"></label></td>
                                    <td><label id="PCTVADEP" class="row rowTable"></label></td>
                                </tr>
                                <tr>
                                    <th scope="row">
                                        <div class="row rowTable semaforoLegend">
                                            <div>INOF</div>
                                        </div>
                                    </th>
                                    <td><label id="SALDOACTINOFF" class="row rowTable"></label></td>
                                    <td><label id="Y2DINOFF" class="row rowTable"></label></td>
                                    <td><label id="PCTY2DINOFF" class="row rowTable"></label></td>
                                    <td><label id="VAINOFF" class="row rowTable"></label></td>
                                    <td><label id="PCTVAINOFF" class="row rowTable"></label></td>
                                </tr>
                                <tr>
                                    <th scope="row">
                                        <div class="row rowTable semaforoLegend">
                                            <div>- INDIRECTAS</div>
                                        </div>
                                    </th>
                                    <td><label id="SALDOACTIND" class="row rowTable"></label></td>
                                    <td><label id="Y2DIND" class="row rowTable"></label></td>
                                    <td><label id="PCTY2DIND" class="row rowTable"></label></td>
                                    <td><label id="VAIND" class="row rowTable"></label></td>
                                    <td><label id="PCTVAIND" class="row rowTable"></label></td>
                                </tr>
                                <tr>
                                    <th scope="row">
                                        <div class="row rowTable semaforoLegend">
                                            <div>- CAMBIOS</div>
                                        </div>
                                    </th>
                                    <td><label id="SALDOACTCAM" class="row rowTable"></label></td>
                                    <td><label id="Y2DCAM" class="row rowTable"></label></td>
                                    <td><label id="PCTY2DCAM" class="row rowTable"></label></td>
                                    <td><label id="VACAM" class="row rowTable"></label></td>
                                    <td><label id="PCTVACAM" class="row rowTable"></label></td>
                                </tr>
                                <tr>
                                    <th scope="row">
                                        <div class="row rowTable semaforoLegend">
                                            <div>- OTROS</div>
                                        </div>
                                    </th>
                                    <td><label id="SALDOACTOTROS2" class="row rowTable"></label></td>
                                    <td><label id="Y2DOTROS2" class="row rowTable"></label></td>
                                    <td><label id="PCTY2DOTROS2" class="row rowTable"></label></td>
                                    <td><label id="VAOTROS2" class="row rowTable"></label></td>
                                    <td><label id="PCTVAOTROS2" class="row rowTable"></label></td>
                                </tr>
                                <tr>
                                    <th scope="row">
                                        <div class="row rowTable semaforoLegend">
                                            <div class="scorish">TOTAL</div>
                                        </div>
                                    </th>
                                    <td><label id="SALDOACTTOT2" class="row rowTable"></label></td>
                                    <td><label id="Y2DTOT2" class="row rowTable"></label></td>
                                    <td><label id="PCTY2DTOT2" class="row rowTable"></label></td>
                                    <td><label id="VATOT2" class="row rowTable"></label></td>
                                    <td><label id="PCTVATOT2" class="row rowTable"></label></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div style="padding-top: 50px;" class="row disappear">
                        <div class="col-md-6 col-sm-6 col-xs-6 text-center">
                            <div class="row"><label class="scorish">SPREAD</label></div>
                            <div id="SPREADMM" class="row">0</div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 text-center">
                            <div class="row"><label class="scorish">YIELD</label></div>
                            <div id="YIELDDMM" class="row">0</div>
                        </div>
                    </div>
                </div>
                <div id="MercadoBody" class="col-md-12 col-sm-12 col-xs-12">
                    <div style="width: 120px;" class="form-group filtro">
                        <select class="form-control" id="filtroProductoBtnModal">
                            @foreach($tipoProductosBtm as $t)
                            @if($t['TIPOPRODUCTO'] != null)
                            <option value="{{$t['TIPOPRODUCTO']}}" <?php if ($t['TIPOPRODUCTO'] == 'Col. Directas') {
                                                                        echo 'selected';
                                                                    } ?>><?php echo $t['TIPOPRODUCTO'] ?></option>
                            @endif
                            @endforeach
                        </select>
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-6 graficosAtEnd" style="height: 200px">
                            <div class="row rowTtitle text-left"><label>Variacion x Banco</label></div>
                            <canvas style="width: 100%;" id="MercadoProdBancoEvolutivo"></canvas>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 graficosAtEnd">
                            <div class="row rowTtitle text-left"><label>Deuda x Producto</label></div>

                            <canvas style="height: 170px" id="MercadoProdBancoFOTO"></canvas>
                            <div class="space row"></div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Bancos</th>
                                    <th scope="col">Saldo Act.</td>
                                    <th scope="col" colspan="2">Var. mes</td>
                                    <th scope="col" colspan="2">Year to date</td>
                                    <th scope="col" colspan="2">Var. anual</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">
                                        <div class="row rowTable semaforoLegend">
                                            <div class="semaforo" style="margin-right: 5px; background: rgb(77, 196, 135)"></div>
                                            <div>IBK</div>
                                        </div>
                                    </th>
                                    <td><label id="SALDOACTIBK"></label></td>
                                    <td><label id="VMIBK"></label></td>
                                    <td><label id="PCTVMIBK"></label></td>
                                    <td><label id="Y2DIBK"></label></td>
                                    <td><label id="PCTY2DIBK"></label></td>
                                    <td><label id="VAIBK"></label></td>
                                    <td><label id="PCTVAIBK"></label></td>
                                </tr>
                                <tr>
                                    <th scope="row">
                                        <div class="row rowTable semaforoLegend">
                                            <div class="semaforo" style="margin-right: 5px; background: rgb(197, 90, 17)"></div>
                                            <div>BCP</div>
                                        </div>
                                    </th>
                                    <td><label id="SALDOACTBCP"></label></td>
                                    <td><label id="VMBCP"></label></td>
                                    <td><label id="PCTVMBCP"></label></td>
                                    <td><label id="Y2DBCP"></label></td>
                                    <td><label id="PCTY2DBCP"></label></td>
                                    <td><label id="VABCP"></label></td>
                                    <td><label id="PCTVABCP"></label></td>
                                </tr>
                                <tr>
                                    <th scope="row">
                                        <div class="row rowTable semaforoLegend">
                                            <div class="semaforo" style="margin-right: 5px; background:rgb(0, 112, 192)"></div>
                                            <div>BBVA</div>
                                        </div>
                                    </th>
                                    <td><label id="SALDOACTBBVA"></label></td>
                                    <td><label id="VMBBVA"></label></td>
                                    <td><label id="PCTVMBBVA"></label></td>
                                    <td><label id="Y2DBBVA"></label></td>
                                    <td><label id="PCTY2DBBVA"></label></td>
                                    <td><label id="VABBVA"></label></td>
                                    <td><label id="PCTVABBVA"></label></td>
                                </tr>
                                <tr>
                                    <th scope="row">
                                        <div class="row rowTable semaforoLegend">
                                            <div class="semaforo" style="margin-right: 5px; background:rgb(255, 0, 0)"></div>
                                            <div>SCOTIA</div>
                                        </div>
                                    </th>
                                    <td><label id="SALDOACTSCOTIA"></label></td>
                                    <td><label id="VMSCOTIA"></label></td>
                                    <td><label id="PCTVMSCOTIA"></label></td>
                                    <td><label id="Y2DSCOTIA"></label></td>
                                    <td><label id="PCTY2DSCOTIA"></label></td>
                                    <td><label id="VASCOTIA"></label></td>
                                    <td><label id="PCTVASCOTIA"></label></td>
                                </tr>
                                <tr>
                                    <th scope="row">
                                        <div class="row rowTable semaforoLegend">
                                            <div class="semaforo" style="margin-right: 5px; background:rgb(0, 176, 240)"></div>
                                            <div>BIF</div>
                                        </div>
                                    </th>
                                    <td><label id="SALDOACTBIF"></label></td>
                                    <td><label id="VMBIF"></label></td>
                                    <td><label id="PCTVMBIF"></label></td>
                                    <td><label id="Y2DBIF"></label></td>
                                    <td><label id="PCTY2DBIF"></label></td>
                                    <td><label id="VABIF"></label></td>
                                    <td><label id="PCTVABIF"></label></td>
                                </tr>
                                <tr>
                                    <th scope="row">
                                        <div class="row rowTable semaforoLegend">
                                            <div class="semaforo" style="margin-right: 5px; background:gray"></div>
                                            <div>OTROS</div>
                                        </div>
                                    </th>
                                    <td><label id="SALDOACTOTROS"></label></td>
                                    <td><label id="VMOTROS"></label></td>
                                    <td><label id="PCTVMOTROS"></label></td>
                                    <td><label id="Y2DOTROS"></label></td>
                                    <td><label id="PCTY2DOTROS"></label></td>
                                    <td><label id="VAOTROS"></label></td>
                                    <td><label id="PCTVAOTROS"></label></td>
                                </tr>
                                <tr>
                                    <th scope="row">
                                        <div class="row rowTable semaforoLegend">
                                            <div class="semaforo" style="margin-right: 5px; background:white"></div>

                                            <div>TOTAL</div>
                                        </div>
                                    </th>
                                    <td><label id="SALDOACTTOT"></label></td>
                                    <td><label id="VMTOT"></label></td>
                                    <td><label id="PCTVMTOT"></label></td>
                                    <td><label id="Y2DTOT"></label></td>
                                    <td><label id="PCTY2DTOT"></label></td>
                                    <td><label id="VATOT"></label></td>
                                    <td><label id="PCTVATOT"></label></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="maskWaiting">
    <div class="waitingwraper">
        <img style="height: 100px;" src="../public/img/waiting.gif" alt="">
        <p style="margin-top: 15px;font-size: 1.3em;">Cargando...</p>
    </div>
</div>
@stop

<style>
    .activeTab {
        border-bottom: 30px solid white !important;
        z-index: 0 !important;
    }

    .rowTable {
        height: 30px;
        text-align: right;
    }

    .rowTtitle {
        font-family: 'OmnesBold' !important;
        height: 50px;
        font-size: 1.2em;
        display: flex;
        text-align: right;
        align-items: center;
        justify-content: center;

    }

    .scorish {
        font-family: 'OmnesBold' !important;
        font-size: 1.2em;
    }

    .celda {
        height: 100px;
    }

    .minirow {
        padding-left: 15px;
    }

    .score {
        position: absolute;
        height: 100px;
        width: 100px;
        top: calc(50% - 35px);
        right: calc(50% - 50px);
        display: flex;
        align-items: center;
        justify-content: center;
        font-size: 1.5em;
        font-weight: bolder;
    }

    .semaforoWrapper {
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .datosLinea {
        height: 100px;
    }

    .titleNumero {
        font-size: 1em;
    }

    .hideSectionUp {
        margin-top: -500px !important;
    }

    table {
        table-layout: fixed;
        word-wrap: break-word;
    }

    .BtnModalTab {
        position: absolute;
        top: 50px;
        z-index: -9;
        left: 388px;
        border-bottom: 30px solid whitesmoke;
        border-left: 10px solid transparent;
        border-right: 10px solid transparent;
        height: 0;
        width: 120px;
        padding-right: 15px;
        padding-bottom: 5px;
        padding-top: 5px;
        margin-bottom: 0 !important;
        font-size: 1.3em;
        cursor: pointer;
        margin-right: 5px;
        transition: 0.5s all;
    }

    .BtnModalTab label {
        height: 30px;
        padding: 8px;
        cursor: pointer;
    }

    .hideSectionUp2 {
        margin-top: -550px !important;
    }

    .hideSectionUp3 {
        margin-top: -200px !important;
    }

    .hideSectionUp4 {
        margin-top: -550px !important;
    }

    .hideSectionUp23 {
        margin-top: -750px !important;
    }

    .hideSectionUp25 {
        margin-top: -700px !important;
    }

    .hideSectionUp5 {
        margin-top: -600px !important;
    }

    .hideSectionUp7 {
        margin-top: -800px !important;
    }

    .hidebar {
        display: flex;
        justify-content: flex-end;
    }

    th {
        text-align: center !important;
    }

    td {
        text-align: right;
    }

    .actualizaciones {
        font-size: 0.8em;
    }

    .hidebtn {
        font-size: 1em;
        cursor: pointer;
    }

    .wrapper {
        transition: 1s all;
    }

    .parentWrapper {
        overflow: hidden;

    }

    .waitingwraper {
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        background: white;
        padding: 40px;
        border-radius: 27px;
    }

    .maskAppear {
        display: flex !important;
        align-items: center;
        justify-content: center;
    }

    th {
        vertical-align: middle !important;
    }

    #maskModalWrapper {
        max-width: 800px;
        height: calc(100% + 90px);
        width: 90%;
        overflow-x: scroll;
    }

    #maskOverflowWrapper {
        min-width: 700px;
        padding: 20px;
        background-color: white;
    }

    #maskModal {
        position: fixed;
        background: rgba(0, 0, 0, 0.3);
        z-index: 999999999999999;
        top: 0;
        right: 0;
        width: 100vw;
        height: 100vh;
        display: none;
    }

    #maskWaiting {
        position: fixed;
        background: rgba(0, 0, 0, 0.3);
        z-index: 999999999999999;
        top: 0;
        right: 0;
        width: 100vw;
        height: 100vh;
        display: none;
    }

    .NxtBtn {
        margin-top: 8px;
        font-size: 1.2em;
        padding-right: 20px;
        padding-left: 20px;
        padding-top: 10px;
        border-style: solid;
        border-radius: 9px;
        background: #4DD094;
        margin-left: 25px;
        font-weight: bold;
        padding-bottom: 10px;
        color: white;
        transition: 0.25s all;
        border-color: transparent;

    }

    .NxtBtn:hover {
        border-color: #4DD094 !important;
        background: transparent !important;
        color: #4DD094 !important;
    }

    .GraphName {
        text-align: center;
    }

    .canClnt {
        display: flex;
        flex-direction: column;
        justify-content: center;
        /* height: 100%; */
        align-items: center;
    }

    .canvasNota {
        width: 100% !important;
        height: auto !important;
    }

    #VarXProducto {
        width: 100%;
    }

    .table_label {
        margin-bottom: 0 !important;
    }

    .porcVar {
        font-size: 0.8em;
        color: #4dc487;
    }

    .collabel {
        align-self: center;
        justify-content: center;
        display: flex;
        flex-direction: column;
        margin-top: 3px;
        font-size: 1.3em;
    }

    .Cumpl {
        background: #e6b31c;
        height: 100%;
        width: 0;
        transition: 1s all;
    }

    .CumplWrapper {
        height: 12px;
        margin-bottom: 5px;
        width: 100%;
        overflow: hidden;
        background: lightgray;
    }

    .cumplimiento_label {
        font-size: 10pt;
        margin-left: 5px;
        height: 15pt;
        margin-bottom: 0px !important;
    }

    .chart_label {
        font-size: 0.9em;
    }

    .lil_chart {
        width: 100% !important;

    }

    .score {
        font-family: 'OmnesBold' !important;
    }

    .score:hover {
        color: #4DD094;
    }

    .hideSectionUp40 {
        margin-bottom: -900px !important;
    }

    body {
        min-width: 1400px !important;
    }

    #avanceInfo {
        font-size: 1.1em;
        padding-left: 6px;
        cursor: pointer;
    }

    @font-face {
        font-family: 'Omnes';
        src: url('../public/fonts/omnes-light-webfont.woff2') format('woff2'),
            url('../public/fonts/omnes-light-webfont.woff') format('woff');
        font-weight: normal;
        font-style: normal;

    }

    @font-face {
        font-family: 'OmnesRegular';
        src: url('../public/fonts/omnes-regular-webfont.woff2') format('woff2'),
            url('../public/fonts/omnes-regular-webfont.woff') format('woff');
        font-weight: normal;
        font-style: normal;

    }

    @font-face {
        font-family: 'OmnesBold';
        src: url('../public/fonts/omnes-medium-webfont.woff2') format('woff2'),
            url('../public/fonts/omnes-medium-webfont.woff') format('woff');
        font-weight: normal;
        font-style: normal;

    }

    .menuItem {
        font-size: 1.2em;
        margin-left: 0.8em;
        cursor: pointer;
    }

    .menu {
        display: flex;
        justify-content: flex-end;
    }

    .Title {
        display: flex;
        align-items: center;
        margin-left: -20px;
        font-size: 1.4em;
        margin-right: -20px;
        padding-bottom: 20px;
        margin-bottom: 15px;
        border-bottom: 2px solid whitesmoke;
    }

    body {
        font-family: Omnes !important;
    }

    .avatar-icon {
        font-size: 2.2em;
    }

    .avatarWrapper {
        cursor: pointer;
    }

    .avatar {
        height: 65px;
        background: white;
        width: 65px !important;
        display: flex;
        align-items: center;
        justify-content: center;
        border-radius: 32.5px;
    }

    .semaforoLegend {
        display: flex !important;
        align-items: center;
    }

    .semaforo {
        height: 10px;
        width: 10px;
        border-radius: 5px;

    }

    .avatar-container2 {
        background: whitesmoke;
        padding-top: 10px;
        display: flex;
        align-items: center;
        padding-bottom: 10px;
        margin-left: 0px !important;
        margin-right: 0px !important;
        padding-left: 15px;
    }

    .section-dashboard {
        background: white;
        padding: 20px;
        border-radius: 5px;
        box-shadow: 0 3px 11px 0px rgb(220, 220, 220);
    }

    .avatar-container {
        background: white;
        padding-top: 10px;
        display: flex;
        align-items: center;
        padding-bottom: 10px;
        margin-left: 0px !important;
        margin-right: 0px !important;
        padding-left: 15px;
    }

    .center {
        text-align: center;
        padding-bottom: 0 !important;
        padding-top: 0 !important;

    }

    #navCartera {
        border-bottom: 0px !important;
        padding-left: 4.16%;
        padding-right: 4.16%;
        margin-top: 25px !important
    }

    .disappear {
        display: none !important;
    }

    .title {
        display: flex;
        align-items: center;
        margin-left: -20px;
        margin-right: -20px;
        padding-bottom: 20px;
        margin-bottom: 15px;
        border-bottom: 2px solid whitesmoke;
    }

    .directas {
        /* padding-top: 10px; */
        background-color: whitesmoke;
        display: flex;
        text-align: right;
        align-items: flex-end;
    }

    .indirectas {
        /* padding-top: 10px; */
        display: flex;
        text-align: right;
        align-items: flex-end;
    }

    .depositos {
        /* padding-top: 10px; */
        background-color: whitesmoke;
        text-align: right;
        display: flex;
        align-items: flex-end;
    }

    .nav-tabs>li>a {
        margin-right: 2px;
        line-height: 1.42857143;
        border: 2px solid transparent;
        border-radius: 4px 4px 0 0;
        cursor: pointer;
        border-bottom: 0px !important;
    }

    .nav-link {
        background: whitesmoke;
    }

    .nav-active {
        background: white !important;

    }

    .graphic-top {
        display: flex;
        align-items: center;
    }

    .space {
        height: 20px;
    }

    .row {
        width: 100% !important;
        margin: 0 !important;
    }

    .filtro {
        display: flex;
        justify-content: center;
    }
</style>
@section('js-scripts')

<script>
    ////////////////////////////////////////////////// Configuracion inicial - Filtros y Graficas ///////////////////////////////////////////////
    var filtro1;
    var filtro2;
    var filtro3;
    var filtro4;
    var filtroProducto;

    window.filtro1 = "<?php echo isset($banca) ? $banca : 'BC' ?>";
    window.filtro2 = "<?php echo isset($zonal) ? $zonal : 'null' ?>";
    window.filtro3 = "<?php echo isset($jefe) ? $jefe : 'null' ?>";
    window.filtro4 = "<?php echo isset($ejecutivo) ? $ejecutivo : 'null' ?>";
    window.filtroProducto = "Col. Directas";
    window.filtroProductoBtnModal = "Col. Directas";
    window.id = '';
    ActualizarFiltro();

    const ctxLineBtm = document.getElementById('LineChartBtnDiv').getContext('2d');
    const ctxPieBtm = document.getElementById('PieChartBtmDiv').getContext('2d');
    const PieChartBtm = new Chart(ctxPieBtm, {
        data: {
            labels: ["IBK", "BCP", "BBVA", "SCOTIA", "BIF"],
            datasets: [{
                data: [0, 0, 0, 0, 0],
                backgroundColor: [
                    "rgba(0, 176, 80, 0.85)",
                    "rgba(228, 108, 10, 0.85)",
                    "rgba(0, 112, 192, 0.85)",
                    "rgba(255, 0, 0, 0.85)",
                    "rgba(0, 176, 240, 0.85)"
                ],
                borderColor: "transparent"
            }]
        },
        type: 'doughnut',
        options: {
            startAngle: -Math.PI / 4,
            legend: {
                display: false
            },
            animation: {
                animateRotate: true
            }
        }
    });
    const LineChartBtm = new Chart(ctxLineBtm, {
        type: 'line',

        options: {
            responsive: false,
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        display: false
                    }
                }]
            }
        }
    });
    const ctxProd = document.getElementById('VarXProducto').getContext('2d');
    const LineChart = new Chart(ctxProd, {
        type: 'line',
        data: {
            datasets: [{
                label: 'Saldos',
                borderColor: '#4dc487',
                fill: false
            }]
        },
        options: {
            responsive: false,
            legend: {
                display: false
            },
            scales: {

            }
        }
    });
    const ctxNota = document.getElementById("nota").getContext('2d');
    const ChartNota = new Chart(ctxNota, {
        type: 'pie',
        data: {
            datasets: [{
                data: [100],
                backgroundColor: [
                    'whitesmoke'
                ],
                borderWidth: 0.6
            }],
            labels: [
                'Nota',
                '',
            ]
        },

        options: {
            legend: {
                display: false
            },
            cutoutPercentage: 70,
            responsive: true,
            mantainRatio: false,
            tooltips: {
                enabled: false
            },
        }
    });
    const ctxFinan = document.getElementById("nota_financiera").getContext('2d');
    const ChartFinan = new Chart(ctxFinan, {
        type: 'doughnut',
        data: {
            datasets: [{
                data: [100],
                backgroundColor: [
                    'whitesmoke'
                ],
                borderWidth: 0.6
            }],
            labels: [
                'Nota Financiera',
                '',
            ]
        },
        options: {
            legend: {
                display: false
            },
            cutoutPercentage: 70,
            responsive: true,
            tooltips: {
                enabled: false
            },
        }
    });
    const ctxEco = document.getElementById("nota_ecosistema").getContext('2d');
    const ChartEco = new Chart(ctxEco, {
        type: 'doughnut',
        data: {
            datasets: [{
                data: [100],
                backgroundColor: [
                    'whitesmoke'
                ],
                borderWidth: 0.6
            }],
            labels: [
                'Nota Ecosistema',
                '',
            ]
        },
        options: {
            legend: {
                display: false
            },
            cutoutPercentage: 70,
            responsive: true,
            tooltips: {
                enabled: false
            },
        }
    });
    const ctxCD = document.getElementById('coldirectas').getContext('2d');
    const chartCD = new Chart(ctxCD, {
        type: 'line',
        options: {
            responsive: true,
            legend: {
                display: false
            },
            elements: {
                line: {
                    borderColor: '#4dc487',
                    borderWidth: 2
                },
                point: {
                    radius: 4
                }
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem, chart) {
                        var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label;
                        return datasetLabel + ': S/ ' + milmillones(tooltipItem.yLabel);
                    }
                }
            },
            scales: {
                yAxes: [{
                    display: false
                }],
                xAxes: [{
                    display: false
                }]
            }
        }
    });
    const ctxCI = document.getElementById('colindirectas').getContext('2d');
    const chartCI = new Chart(ctxCI, {
        type: 'line',
        options: {
            responsive: true,
            legend: {
                display: false
            },
            elements: {
                line: {
                    borderColor: '#4dc487',
                    borderWidth: 2
                },
                point: {
                    radius: 4
                }
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem, chart) {
                        var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label;
                        return datasetLabel + ': S/ ' + milmillones(tooltipItem.yLabel);
                    }
                }
            },
            scales: {
                yAxes: [{
                    display: false
                }],
                xAxes: [{
                    display: false
                }]
            }
        }
    });
    const ctxD = document.getElementById('depositos').getContext('2d');
    const chartD = new Chart(ctxD, {
        type: 'line',
        options: {
            responsive: true,
            legend: {
                display: false
            },
            elements: {
                line: {
                    borderColor: '#4dc487',
                    borderWidth: 2
                },
                point: {
                    radius: 4
                }
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem, chart) {
                        var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label;
                        return datasetLabel + ': S/ ' + milmillones(tooltipItem.yLabel);
                    }
                }
            },
            scales: {
                yAxes: [{
                    display: false
                }],
                xAxes: [{
                    display: false
                }]
            }
        }
    });
    const ctxCS = document.getElementById('CSueldo').getContext('2d');
    const chartCS = new Chart(ctxCS, {
        type: 'line',
        options: {
            responsive: false,
            legend: {
                display: false
            },
            elements: {
                line: {
                    borderColor: '#4dc487',
                    borderWidth: 2
                },
                point: {
                    radius: 0
                }
            },
            tooltips: {
                enabled: false
            },
            scales: {
                yAxes: [{
                    display: false
                }],
                xAxes: [{
                    display: false
                }]
            }
        }
    });
    const ctxDF = document.getElementById('DFactoring').getContext('2d');
    const chartDF = new Chart(ctxDF, {
        type: 'line',
        options: {
            responsive: true,
            legend: {
                display: false
            },
            elements: {
                line: {
                    borderColor: '#4dc487',
                    borderWidth: 2
                },
                point: {
                    radius: 4
                }
            },
            tooltips: {
                enabled: false
            },
            scales: {
                yAxes: [{
                    display: false
                }],
                xAxes: [{
                    display: false
                }]
            }
        }
    });
    const ctxVP = document.getElementById('VProveedores').getContext('2d');
    const chartVP = new Chart(ctxVP, {
        type: 'line',
        options: {
            responsive: true,
            legend: {
                display: false
            },
            elements: {
                line: {
                    borderColor: '#4dc487',
                    borderWidth: 2
                },
                point: {
                    radius: 4
                }
            },
            tooltips: {
                enabled: false
            },
            scales: {
                yAxes: [{
                    display: false
                }],
                xAxes: [{
                    display: false
                }]
            }
        }
    });

    const ctxDG = document.getElementById('Descuentos').getContext('2d');
    const LineChartDG = new Chart(ctxDG, {
        type: 'line',
        options: {
            responsive: true,
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                    ticks: {
                        display: false
                    }
                }]
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem, chart) {
                        var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label;
                        return datasetLabel + ': S/ ' + milmillones(tooltipItem.yLabel);
                    }
                }
            },
        }
    });
    const ctxCSG = document.getElementById('CuentaSueldo').getContext('2d');
    const LineChartCSG = new Chart(ctxCSG, {
        type: 'line',
        data: {
            datasets: [{
                label: 'Saldos',
                borderColor: '#4dc487',
                fill: false
            }]
        },
        options: {
            responsive: true,
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                    ticks: {
                        display: false
                    }
                }]
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem, chart) {
                        var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label;
                        return datasetLabel + ': S/ ' + milmillones(tooltipItem.yLabel);
                    }
                }
            },
        }
    });
    const ctxPP = document.getElementById('PagoProveedores').getContext('2d');
    const LineChartPP = new Chart(ctxPP, {
        type: 'line',
        options: {
            responsive: true,
            legend: {
                display: false
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem, chart) {
                        var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label;
                        return datasetLabel + ': S/ ' + milmillones(tooltipItem.yLabel);
                    }
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        display: false
                    }
                }]
            }
        }
    });
    const ctxVPB = document.getElementById('MercadoProdBancoEvolutivo').getContext('2d');
    const chartVPB = new Chart(ctxVPB, {
        type: 'line',
        options: {
            responsive: false,
            legend: {
                display: false
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem, chart) {
                        var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label;
                        return datasetLabel + ': S/ ' + milmillones(tooltipItem.yLabel);
                    }
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        callback: function(value) {
                            return milmillones(value);
                        }
                    }
                }]
            }
        }
    });
    const ctxDCF = document.getElementById('MercadoProdBancoFOTO').getContext('2d');
    const BarChartDCF = new Chart(ctxDCF, {
        type: 'horizontalBar',
        options: {
            responsive: false,
            legend: {
                display: false
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem, data) {
                        var xLabel = tooltipItem.xLabel;
                        return ' S/ ' + milmillones(Math.abs(xLabel));
                    }
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        callback: function(value, index, values, chart) {
                            return value;
                        }
                    }
                }],
                xAxes: [{
                    ticks: {
                        callback: function(value) {
                            return milmillones(Math.abs(value));
                        }
                    }
                }]

            }
        }
    });

    ////////////////////////////////////////////////// Mantenimiento del Filtro ///////////////////////////////////////////////

    $(document).on("change", "#filtro1", function() {
        window.filtro1 = $("#filtro1").val();
        getZonales([window.filtro1]);
    });

    $(document).on("change", "#filtro2", function() {
        window.filtro2 = $("#filtro2").val();
        getJefes([window.filtro1, window.filtro2]);
    });
    $(document).on("change", "#filtro3", function() {
        window.filtro3 = $("#filtro3").val();
        getEjecutivos([window.filtro1, window.filtro2, window.filtro3]);

    });
    $(document).on("change", "#filtro4", function() {
        window.filtro4 = $("#filtro4").val();
    });
    $(document).on("change", "#filtroProductoBtnModal", function() {
        $('#maskModal').addClass('maskAppear');
        DetalleClienteEvolutivo();
        DetalleClienteFOTO();
    });
    $(document).on("change", "#filtroProducto", function() {
        window.filtroProducto = $("#filtroProducto").val();
        VarXProducto();
        if ($("#topBtm").hasClass("fa-arrow-up")) {
            $('#dynamicTitle').html('Principales Variaciones<br>' + window.filtroProducto + '<br> TOP 15');
        } else {
            $('#dynamicTitle').html('Principales Variaciones<br>' + window.filtroProducto + '<br>BOTTOM 15');
        }
        getTopBtm();
    });

    $("#topBtmMERCADO").click(function() {
        var tipoProducto = $("#filtroProductoBtn").val();
        if ($("#topBtmMERCADO").hasClass("fa-arrow-up")) {
            $('#dynamicTitleMERCADO').html('Principales Variaciones<br>' + tipoProducto + '<br> TOP 15');
            $("#topBtmMERCADO").removeClass("fa-arrow-up");
            $("#topBtmMERCADO").addClass("fa-arrow-down");
        } else {
            $('#dynamicTitleMERCADO').html('Principales Variaciones<br>' + tipoProducto + '<br> BOTTOM 15');
            $("#topBtmMERCADO").addClass("fa-arrow-up");
            $("#topBtmMERCADO").removeClass("fa-arrow-down");
        }
        getTopBtmMERCADO();
    });

    $("#topBtm").click(function() {
        var tipoProducto = $("#filtroProducto").val();
        if ($("#topBtm").hasClass("fa-arrow-up")) {
            $('#dynamicTitle').html('Principales Variaciones<br>' + tipoProducto + '<br> TOP 15');
            $("#topBtm").removeClass("fa-arrow-up");
            $("#topBtm").addClass("fa-arrow-down");
        } else {
            $('#dynamicTitle').html('Principales Variaciones<br>' + tipoProducto + '<br> BOTTOM 15');
            $("#topBtm").addClass("fa-arrow-up");
            $("#topBtm").removeClass("fa-arrow-down");
        }
        getTopBtm();
    });
    $("#Mercado").click(function() {
        $("#Interbank").removeClass("activeTab");
        $("#InterbankBody").addClass("disappear");
        $("#MercadoBody").removeClass("disappear");
        $("#Mercado").addClass("activeTab");
    });
    $("#Interbank").click(function() {
        $("#Mercado").removeClass("activeTab");
        $("#MercadoBody").addClass("disappear");
        $("#InterbankBody").removeClass("disappear");
        $("#Interbank").addClass("activeTab");
    });
    $("#finan-nav").click(function() {
        $("#eco-nav").removeClass("nav-active");
        $("#Financiera").removeClass("disappear");
        $("#finan-nav").addClass("nav-active");
        $("#Ecosistema").addClass("disappear");
        ActualizarFiltro();
    });
    $("#eco-nav").click(function() {
        $("#finan-nav").removeClass("nav-active");
        $("#Ecosistema").removeClass("disappear");
        $("#eco-nav").addClass("nav-active");
        $("#Financiera").addClass("disappear");
        ActualizarFiltro();
    });

    $("#TableroNota").click(function() {
        if ($("#TableroNota").hasClass("fa-chevron-up")) {
            $("#TableroNota").removeClass("fa-chevron-up");
            $("#TableroNota").addClass("fa-chevron-down");
            $("#TopSection").addClass("hideSectionUp");
        } else {
            $("#TableroNota").addClass("fa-chevron-up");
            $("#TableroNota").removeClass("fa-chevron-down");
            $("#TopSection").removeClass("hideSectionUp");
        }
    });
    $("#VarcionesSF").click(function() {
        if ($("#VarcionesSF").hasClass("fa-chevron-up")) {
            $("#VarcionesSF").removeClass("fa-chevron-up");
            $("#VarcionesSF").addClass("fa-chevron-down");
            $("#TopSection23").addClass("hideSectionUp23");
        } else {
            $("#VarcionesSF").addClass("fa-chevron-up");
            $("#VarcionesSF").removeClass("fa-chevron-down");
            $("#TopSection23").removeClass("hideSectionUp23");
        }
    });
    $("#ActividadesFinanciera").click(function() {
        if ($("#ActividadesFinanciera").hasClass("fa-chevron-up")) {
            $("#ActividadesFinanciera").removeClass("fa-chevron-up");
            $("#ActividadesFinanciera").addClass("fa-chevron-down");
            $("#TopSection2").addClass("hideSectionUp2");
        } else {
            $("#ActividadesFinanciera").addClass("fa-chevron-up");
            $("#ActividadesFinanciera").removeClass("fa-chevron-down");
            $("#TopSection2").removeClass("hideSectionUp2");
        }
    });
    $("#LineasFinanciera").click(function() {
        if ($("#LineasFinanciera").hasClass("fa-chevron-up")) {
            $("#LineasFinanciera").removeClass("fa-chevron-up");
            $("#LineasFinanciera").addClass("fa-chevron-down");
            $("#TopSection3").addClass("hideSectionUp3");
        } else {
            $("#LineasFinanciera").addClass("fa-chevron-up");
            $("#LineasFinanciera").removeClass("fa-chevron-down");
            $("#TopSection3").removeClass("hideSectionUp3");
        }
    });
    $("#VariacionesActividades").click(function() {
        if ($("#VariacionesActividades").hasClass("fa-chevron-up")) {
            $("#VariacionesActividades").removeClass("fa-chevron-up");
            $("#VariacionesActividades").addClass("fa-chevron-down");
            $("#TopSection4").addClass("hideSectionUp7");
        } else {
            $("#VariacionesActividades").addClass("fa-chevron-up");
            $("#VariacionesActividades").removeClass("fa-chevron-down");
            $("#TopSection4").removeClass("hideSectionUp7");
        }
    });
    $("#VariacionesClientesFinan").click(function() {
        if ($("#VariacionesClientesFinan").hasClass("fa-chevron-up")) {
            $("#VariacionesClientesFinan").removeClass("fa-chevron-up");
            $("#VariacionesClientesFinan").addClass("fa-chevron-down");
            $("#TopSection5").addClass("hideSectionUp25");
        } else {
            $("#VariacionesClientesFinan").addClass("fa-chevron-up");
            $("#VariacionesClientesFinan").removeClass("fa-chevron-down");
            $("#TopSection5").removeClass("hideSectionUp25");
        }
    });
    $('.heatSensorSection').click(function() {
        const id = this.id;
        if ($(id).hasClass("fa-chevron-up")) {
            heatSensor(id, '0');
        } else {
            heatSensor(id, '0', 'Section');
        }
    });
    $("#MercadoResumen").click(function() {
        if ($("#MercadoResumen").hasClass("fa-chevron-up")) {
            $("#MercadoResumen").removeClass("fa-chevron-up");
            $("#MercadoResumen").addClass("fa-chevron-down");
            $("#TopSection6").addClass("hideSectionUp3");
        } else {
            $("#MercadoResumen").addClass("fa-chevron-up");
            $("#MercadoResumen").removeClass("fa-chevron-down");
            $("#TopSection6").removeClass("hideSectionUp3");
        }
    });
    $("#ParticipacionBanco").click(function() {
        if ($("#ParticipacionBanco").hasClass("fa-chevron-up")) {
            $("#ParticipacionBanco").removeClass("fa-chevron-up");
            $("#ParticipacionBanco").addClass("fa-chevron-down");
            $("#TopSection7").addClass("hideSectionUp5");
        } else {
            $("#ParticipacionBanco").addClass("fa-chevron-up");
            $("#ParticipacionBanco").removeClass("fa-chevron-down");
            $("#TopSection7").removeClass("hideSectionUp5");
        }
    });
    
    $(document).on('click', '.avatarWrapper', function(event) {
        event.stopPropagation();
        event.stopImmediatePropagation();
        $('#maskModal').addClass('maskAppear');
        window.id = this.id;
        DetalleClienteEvolutivo();
        DetalleClienteFOTO();
    });
    $(document).on('click', '.searchBtn', function(event) {
        event.stopPropagation();
        event.stopImmediatePropagation();
        const cod = this.getAttribute("value");
    });
    $("#maskModal").click(function() {
        $('#maskModal').removeClass('maskAppear');
    });
    $('#maskModalWrapper').click(function() {
        event.stopPropagation();
        event.stopImmediatePropagation();
    })
    $(".heatSensorGrafico").hover(function() {
        event.stopPropagation();
        event.stopImmediatePropagation();
        $.ajax({
            type: 'GET',
            data: {
                "IDHTML": this.id,
                "TIPO": 'Graficos'
            },
            async: true,
            url: "{{route('heatAnalytic')}}",
            success: function(result) {

            }
        });
    });

    $(document).on('click', '.heatSensorSection', function(event) {
        event.stopPropagation();
        event.stopImmediatePropagation();
        var tipo = $("#" + this.id).hasClass('fa-chevron-up') ? 'Secciones Mostrar' : 'Secciones Ocultar';
        $.ajax({
            type: 'GET',
            data: {
                "IDHTML": this.id,
                "TIPO": tipo
            },
            async: true,
            url: "{{route('heatAnalytic')}}",
            success: function(result) {

            }
        });
    });

    function getZonales(datos) {
        $.ajax({
            type: 'GET',
            data: {
                "banca": datos[0]
            },
            async: false,
            url: "{{route('getZonales')}}",
            success: function(result) {
                var arr = JSON.parse(result);
                $("#filtro2").empty();
                window.filtro2 = "null";
                $("#filtro3").empty();
                window.filtro3 = "null";
                $("#filtro4").empty();
                window.filtro4 = "null";
                const x = document.getElementById("filtro2");
                const y = document.getElementById("filtro3");
                const z = document.getElementById("filtro4");
                var option = document.createElement("option");
                option.appendChild(document.createTextNode("Todos"));
                option.value = null;
                y.appendChild(option);
                z.appendChild(option);
                if (arr.length == 1) {
                    datos.push(null);
                    x.appendChild(option);
                    getJefes(datos);
                } else {
                    arr.forEach(element => {
                        var option = document.createElement("option");
                        if (!element['NOMBRE_ZONAL']) {
                            option.appendChild(document.createTextNode("Todos"));
                        } else {
                            option.appendChild(document.createTextNode(element['NOMBRE_ZONAL']));
                        }
                        option.value = element['NOMBRE_ZONAL'];
                        x.appendChild(option);
                    });
                }
            }
        });
    }

    function getJefes(datos) {
        $.ajax({
            type: 'GET',
            data: {
                "banca": datos[0],
                "zonal": datos[1]
            },
            async: false,
            url: "{{route('getJefes')}}",
            success: function(result) {
                var arr = JSON.parse(result);
                $("#filtro3").empty();
                window.filtro3 = "null";
                $("#filtro4").empty();
                window.filtro4 = "null";
                const x = document.getElementById("filtro3");
                const y = document.getElementById("filtro4");
                var option = document.createElement("option");
                option.appendChild(document.createTextNode("Todos"));
                option.value = null;
                y.appendChild(option);
                if (arr.length == 1) {
                    datos.push(null);
                    x.appendChild(option);
                    getEjecutivos(datos);
                } else {
                    arr.forEach(element => {
                        var option = document.createElement("option");
                        if (!element['NOMBRE_JEFE']) {
                            option.appendChild(document.createTextNode("Todos"));
                        } else {
                            option.appendChild(document.createTextNode(element['NOMBRE_JEFE']));
                        }
                        option.value = element['NOMBRE_JEFE'];
                        x.appendChild(option);
                    });
                }
            }
        });
    }

    function getEjecutivos(datos) {
        $.ajax({
            type: 'GET',
            data: {
                "banca": datos[0],
                "zonal": datos[1],
                "jefe": datos[2]
            },
            async: false,
            url: "{{route('getEjecutivos')}}",
            success: function(result) {
                var arr = JSON.parse(result);
                $("#filtro4").empty();
                window.filtro4 = "null";
                const x = document.getElementById("filtro4");
                if (arr.length == 1) {
                    ActualizarFiltro();
                } else {
                    arr.forEach(element => {
                        var option = document.createElement("option");
                        if (!element['ENCARGADO']) {
                            option.appendChild(document.createTextNode("Todos"));
                        } else {
                            option.appendChild(document.createTextNode(element['ENCARGADO']));
                        }
                        option.value = element['ENCARGADO']
                        x.appendChild(option);
                    });
                }
            }
        });
    }

    function heatSensor(idhtml, tipo_seccion, tipo) {
        const now = new Date()
        const nowInt = now.getTime();
        $.ajax({
            type: 'POST',
            async: true,
            url: "{{route('heatAnalytic')}}",
            data: {
                "timestamp": nowInt.toString(),
                "idhtml": idhtml,
                "modulo": 'Gestion Cartera',
                "fecha": now,
                'tipo': tipo,
                'tipo_seccion': tipo_seccion

            },
            success: function(result) {
                console.log('C LOGRO:' + tipo_seccion);
            }
        });
    }

    function ActualizarFiltro() {
        $('#maskWaiting').addClass('maskAppear');
        banca = window.filtro1;
        zonal = window.filtro2;
        jefe = window.filtro3;
        ejecutivo = window.filtro4;
        $.ajax({
            type: 'GET',
            data: {
                "banca": banca,
                "zonal": zonal,
                "jefe": jefe,
                "ejecutivo": ejecutivo,
            },
            async: true,
            url: "{{route('getUsuario')}}",
            success: function(result) {
                if (JSON.parse(result)[0]) {
                    $("#id_usuario").val(JSON.parse(result)[0]['ID']);
                }
                Cumplimiento();
                if ($("#Financiera").hasClass("disappear")) {
                    PAP();
                    Descuentos();
                } else {
                    coldirectas();
                    VarXProducto();
                    LineaMercadoTabla();
                    GraficosBottom();
                    getTopBtm();
                    getTopBtmMERCADO();
                }
            }
        });
    }
    $(document).on("change", "#filtroFrecuenciaMERCADO", function() {
        getTopBtmMERCADO();
    });
    $(document).on("change", "#filtroBANCOMERCADO", function() {
        getTopBtmMERCADO();
    });
    $(document).on("change", "#filtroFrecuencia", function() {
        getTopBtm();
    });
    $(document).on("change", "#filtroProductoBtn", function() {

        GraficosBottom();
        var tipoProducto = $("#filtroProductoBtn").val();
        if ($("#topBtmMERCADO").hasClass("fa-arrow-up")) {
            $('#dynamicTitleMERCADO').html('Principales Variaciones<br>' + tipoProducto + '<br> TOP 15');
        } else {
            $('#dynamicTitleMERCADO').html('Principales Variaciones<br>' + tipoProducto + '<br>BOTTOM 15');
        }
        getTopBtmMERCADO();
    });
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function DetalleClienteEvolutivo() {
        var COD = window.id.replace(/\D/g, '');
        var TIPO = $('#filtroProductoBtnModal').val();
        $.ajax({
            type: "GET",
            data: {
                "CODUNICOCLI": COD,
                "TIPOPRODUCTO": TIPO
            },
            async: false,
            url: "{{route('DetalleClienteEvolutivo')}}",
            success: function(result) {
                var arr = JSON.parse(result);
                var arrIBK = JSON.parse(arr['IBK'])[0];
                var arrBCP = JSON.parse(arr['BCP'])[0];
                var arrBBVA = JSON.parse(arr['BBVA'])[0];
                var arrSCOTIA = JSON.parse(arr['SCOTIA'])[0];
                var arrBIF = JSON.parse(arr['BIF'])[0];
                var arrOTROS = JSON.parse(arr['OTROS'])[0];
                vectorIBK = [];
                vectorBCP = [];
                vectorBBVA = [];
                vectorSCOTIA = [];
                vectorBIF = [];
                vectorOTROS = [];
                if (arrIBK != null && arrIBK !== undefined) {
                    vectorIBK = [arrIBK['M00'], arrIBK['M01'], arrIBK['M02'], arrIBK['M03'], arrIBK['M04'],
                        arrIBK['M05'], arrIBK['M06'], arrIBK['M07'], arrIBK['M08'],
                        arrIBK['M09'], arrIBK['M10'], arrIBK['M11'], arrIBK['M12']
                    ];
                }
                if (arrBCP != null && arrBCP !== undefined) {
                    vectorBCP = [arrBCP['M00'], arrBCP['M01'], arrBCP['M02'], arrBCP['M03'], arrBCP['M04'],
                        arrBCP['M05'], arrBCP['M06'], arrBCP['M07'], arrBCP['M08'],
                        arrBCP['M09'], arrBCP['M10'], arrBCP['M11'], arrBCP['M12']
                    ];
                }
                if (arrBBVA != null && arrBBVA !== undefined) {
                    vectorBBVA = [arrBBVA['M00'], arrBBVA['M01'], arrBBVA['M02'], arrBBVA['M03'], arrBBVA['M04'],
                        arrBBVA['M05'], arrBBVA['M06'], arrBBVA['M07'], arrBBVA['M08'],
                        arrBBVA['M09'], arrBBVA['M10'], arrBBVA['M11'], arrBBVA['M12']
                    ];
                }
                if (arrSCOTIA != null && arrSCOTIA !== undefined) {

                    vectorSCOTIA = [arrSCOTIA['M00'], arrSCOTIA['M01'], arrSCOTIA['M02'], arrSCOTIA['M03'], arrSCOTIA['M04'],
                        arrSCOTIA['M05'], arrSCOTIA['M06'], arrSCOTIA['M07'], arrSCOTIA['M08'],
                        arrSCOTIA['M09'], arrSCOTIA['M10'], arrSCOTIA['M11'], arrSCOTIA['M12']
                    ];
                }
                if (arrBIF != null && arrBIF !== undefined) {

                    vectorBIF = [arrBIF['M00'], arrBIF['M01'], arrBIF['M02'], arrBIF['M03'], arrBIF['M04'],
                        arrBIF['M05'], arrBIF['M06'], arrBIF['M07'], arrBIF['M08'],
                        arrBIF['M09'], arrBIF['M10'], arrBIF['M11'], arrBIF['M12']
                    ];
                }
                if (arrOTROS != null && arrOTROS !== undefined) {

                    vectorOTROS = [arrOTROS['M00'], arrOTROS['M01'], arrOTROS['M02'], arrOTROS['M03'], arrOTROS['M04'],
                        arrOTROS['M05'], arrOTROS['M06'], arrOTROS['M07'], arrOTROS['M08'],
                        arrOTROS['M09'], arrOTROS['M10'], arrOTROS['M11'], arrOTROS['M12']
                    ];
                }
                rVECTORIBK = [];
                rVECTORBCP = [];
                rVECTORBBVA = [];
                rVECTORSCOTIA = [];
                rVECTORBIF = [];
                rVECTOROTROS = [];
                var mesesColtot = ['Diciembre', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'];
                var mesesCol = [];
                for (let index = 0; index < vectorIBK.length; index++) {
                    if (vectorIBK[index] != null) {
                        mesesCol.push(mesesColtot[index].substring(0, 3));
                        rVECTORIBK.push(vectorIBK[index]);
                        rVECTORBCP.push(vectorBCP[index]);
                        rVECTORBBVA.push(vectorBBVA[index]);
                        rVECTORSCOTIA.push(vectorSCOTIA[index]);
                        rVECTORBIF.push(vectorBIF[index]);
                        rVECTOROTROS.push(vectorOTROS[index]);
                    }
                }
                if (window.chartVPB) window.chartVPB.destroy();
                window.chartVPB = new Chart(ctxVPB, {
                    type: 'line',
                    data: {
                        labels: mesesCol,
                        datasets: [{
                            data: rVECTORIBK,
                            label: 'Saldos IBK S/',
                            fill: false,
                            borderColor: 'rgb(77, 196, 135)',
                            backgroundColor: "rgb(77, 196, 135)",
                            borderWidth: 1
                        }, {
                            data: rVECTORBCP,
                            fill: false,
                            label: 'Saldos BCP S/',
                            borderColor: 'rgb(197, 90, 17)',
                            backgroundColor: 'rgb(197, 90, 17)',
                            borderWidth: 1
                        }, {
                            data: rVECTORBBVA,
                            fill: false,
                            label: 'Saldos BBVA S/',
                            borderColor: 'rgb(0, 112, 192)',
                            backgroundColor: 'rgb(0, 112, 192)',
                            borderWidth: 1
                        }, {
                            data: rVECTORSCOTIA,
                            label: 'Saldos SCOTIA S/',
                            fill: false,
                            borderColor: 'rgb(255, 0, 0)',
                            backgroundColor: 'rgb(255, 0, 0)',
                            borderWidth: 1
                        }, {
                            data: rVECTORBIF,
                            fill: false,
                            label: 'Saldos BIF S/',
                            borderColor: 'rgb(0, 176, 240)',
                            backgroundColor: 'rgb(0, 176, 240)',
                            borderWidth: 1
                        }, {
                            fill: false,
                            data: rVECTOROTROS,
                            label: 'Saldos OTROS S/',
                            borderColor: 'gray',
                            backgroundColor: 'gray',
                            borderWidth: 1
                        }]
                    },
                    options: {
                        responsive: true,
                        legend: {
                            display: false
                        },
                        tooltips: {
                            callbacks: {
                                label: function(tooltipItem, chart) {
                                    var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label;
                                    return datasetLabel + ': S/ ' + milmillones(tooltipItem.yLabel);
                                }
                            }
                        },
                        scales: {
                            yAxes: [{
                                ticks: {
                                    callback: function(value) {
                                        return milmillones(value);
                                    }
                                }
                            }]
                        }
                    }
                });
            }
        });

    }

    function DetalleClienteFOTO() {
        var COD = window.id.replace(/\D/g, '');
        var TIPO = $('#filtroProductoBtnModal').val();
        $.ajax({
            type: "GET",
            data: {
                "CODUNICOCLI": COD,
                "TIPOPRODUCTO": TIPO
            },
            async: false,
            url: "{{route('DetalleClienteFOTO')}}",
            success: function(result) {
                var arr = JSON.parse(JSON.parse(result)['FOTO1'])[0];
                var arr2 = JSON.parse(JSON.parse(result)['FOTO2'])[0];
                if (TIPO == 'Col. Directas') {
                    if (window.BarChartDCF) window.BarChartDCF.destroy();
                    window.BarChartDCF = new Chart(ctxDCF, {
                        type: 'horizontalBar',
                        data: {
                            labels: ['Tarjeta de Credito', 'Comex', 'Descuento', 'Factoring', 'Leasing', 'Otros', 'Prestamos', 'Sobregiro'],
                            datasets: [{
                                backgroundColor: "#00b0f0",
                                data: [-parseFloat(arr['MTOSALDO_TC_ACT']), -parseFloat(arr['MTOSALDO_COM_ACT']), -parseFloat(arr['MTOSALDO_DSCTO_ACT']),
                                    -parseFloat(arr['MTOSALDO_FACT_ACT']), -parseFloat(arr['MTOSALDO_LEAS_ACT']), -parseFloat(arr['MTOSALDO_OTRO_ACT']),
                                    -parseFloat(arr['MTOSALDO_PREST_ACT']), -parseFloat(arr['MTOSALDO_SOBR_ACT'])
                                ]
                            }]
                        },
                        options: {
                            responsive: false,
                            legend: {
                                display: false
                            },
                            tooltips: {
                                callbacks: {
                                    label: function(tooltipItem, data) {
                                        var xLabel = tooltipItem.xLabel;
                                        return ' S/ ' + milmillones(Math.abs(xLabel));
                                    }
                                }
                            },
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero: true,
                                        callback: function(value, index, values, chart) {
                                            return value;
                                        }
                                    }
                                }],
                                xAxes: [{
                                    ticks: {
                                        callback: function(value) {
                                            return milmillones(Math.abs(value));
                                        }
                                    }
                                }]

                            }
                        }
                    });

                } else if (TIPO == 'Col. Indirectas') {
                    if (window.BarChartDCF) window.BarChartDCF.destroy();
                    $('#MercadoProdBancoFOTO')[0].height = 200;
                    window.BarChartDCF = new Chart(ctxDCF, {
                        type: 'horizontalBar',
                        data: {
                            labels: ['Carta Fianza', 'Carta de Crédito'],
                            datasets: [{
                                backgroundColor: "#00b0f0",
                                data: [-parseFloat(arr['MTOSALDO_CF_ACT']), -parseFloat(arr['MTOSALDO_CC_ACT'])]
                            }]
                        },
                        options: {
                            responsive: false,
                            legend: {
                                display: false
                            },
                            tooltips: {
                                callbacks: {
                                    label: function(tooltipItem, data) {
                                        var xLabel = tooltipItem.xLabel;
                                        return ' S/ ' + milmillones(Math.abs(xLabel));
                                    }
                                }
                            },
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero: true,
                                        callback: function(value, index, values) {
                                            return value;
                                        }
                                    }
                                }],
                                xAxes: [{
                                    ticks: {
                                        callback: function(value) {
                                            return milmillones(Math.abs(value));
                                        }
                                    }
                                }]
                            }
                        }
                    });
                }
                var str = '';
                $('#SALDOACTIBK').html(milmillones(parseFloat(arr['MTOSALDO_IBK_ACT'])));
                $('#SALDOACTBCP').html(milmillones(parseFloat(arr['MTOSALDO_BCP_ACT'])));
                $('#SALDOACTBBVA').html(milmillones(parseFloat(arr['MTOSALDO_BBVA_ACT'])));
                $('#SALDOACTSCOTIA').html(milmillones(parseFloat(arr['MTOSALDO_SCOT_ACT'])));
                $('#SALDOACTBIF').html(milmillones(parseFloat(arr['MTOSALDO_BIF_ACT'])));
                $('#SALDOACTOTROS').html(milmillones(parseFloat(arr['MTOSALDO_OTROS_ACT'])));
                $('#SALDOACTTOT').html(milmillones(parseFloat(arr['MTOSALDO_SF_ACT'])));

                $('#VMIBK').html(milmillones(parseFloat(arr['VAR_MTOSALDO_IBK_MES'])));
                $('#VMBCP').html(milmillones(parseFloat(arr['VAR_MTOSALDO_BCP_MES'])));
                $('#VMBBVA').html(milmillones(parseFloat(arr['VAR_MTOSALDO_BBVA_MES'])));
                $('#VMSCOTIA').html(milmillones(parseFloat(arr['VAR_MTOSALDO_SCOT_MES'])));
                $('#VMBIF').html(milmillones(parseFloat(arr['VAR_MTOSALDO_BIF_MES'])));
                $('#VMOTROS').html(milmillones(parseFloat(arr['VAR_MTOSALDO_OTROS_MES'])));
                $('#VMTOT').html(milmillones(parseFloat(arr['VAR_MTOSALDO_SF_MES'])));

                str = PositivoNegativo(parseFloat(arr['VAR_MTOSALDO_IBK_MES']), 'VMIBK');
                str = PositivoNegativo(parseFloat(arr['VAR_MTOSALDO_BCP_MES']), 'VMBCP');
                str = PositivoNegativo(parseFloat(arr['VAR_MTOSALDO_BBVA_MES']), 'VMBBVA');
                str = PositivoNegativo(parseFloat(arr['VAR_MTOSALDO_SCOT_MES']), 'VMSCOTIA');
                str = PositivoNegativo(parseFloat(arr['VAR_MTOSALDO_BIF_MES']), 'VMBIF');
                str = PositivoNegativo(parseFloat(arr['VAR_MTOSALDO_OTROS_MES']), 'VMOTROS');
                str = PositivoNegativo(parseFloat(arr['VAR_MTOSALDO_SF_MES']), 'VMTOT');
                $('#PCTVMIBK').html(PositivoNegativo(parseFloat(arr['PCT_MTOSALDO_IBK_MES']), 'PCTVMIBK'));
                $('#PCTVMBCP').html(PositivoNegativo(parseFloat(arr['PCT_MTOSALDO_BCP_MES']), 'PCTVMBCP'));
                $('#PCTVMBBVA').html(PositivoNegativo(parseFloat(arr['PCT_MTOSALDO_BBVA_MES']), 'PCTVMBBVA'));
                $('#PCTVMSCOTIA').html(PositivoNegativo(parseFloat(arr['PCT_MTOSALDO_SCOTIA_MES']), 'PCTVMSCOTIA'));
                $('#PCTVMBIF').html(PositivoNegativo(parseFloat(arr['PCT_MTOSALDO_BIF_MES']), 'PCTVMBIF'));
                $('#PCTVMOTROS').html(PositivoNegativo(parseFloat(arr['PCT_MTOSALDO_OTROS_MES']), 'PCTVMOTROS'));
                $('#PCTVMTOT').html(PositivoNegativo(parseFloat(arr['PCT_MTOSALDO_SF_MES']), 'PCTVMTOT'));

                $('#Y2DIBK').html(milmillones(parseFloat(arr['VAR_MTOSALDO_IBK_CIERRE'])));
                $('#Y2DBCP').html(milmillones(parseFloat(arr['VAR_MTOSALDO_BCP_CIERRE'])));
                $('#Y2DBBVA').html(milmillones(parseFloat(arr['VAR_MTOSALDO_BBVA_CIERRE'])));
                $('#Y2DSCOTIA').html(milmillones(parseFloat(arr['VAR_MTOSALDO_SCOTIA_CIERRE'])));
                $('#Y2DBIF').html(milmillones(parseFloat(arr['VAR_MTOSALDO_BIF_CIERRE'])));
                $('#Y2DOTROS').html(milmillones(parseFloat(arr['VAR_MTOSALDO_OTROS_CIERRE'])));
                $('#Y2DTOT').html(milmillones(parseFloat(arr['VAR_MTOSALDO_SF_CIERRE'])));

                str = PositivoNegativo(parseFloat(arr['VAR_MTOSALDO_IBK_CIERRE']), 'Y2DIBK');
                str = PositivoNegativo(parseFloat(arr['VAR_MTOSALDO_BCP_CIERRE']), 'Y2DBCP');
                str = PositivoNegativo(parseFloat(arr['VAR_MTOSALDO_BBVA_CIERRE']), 'Y2DBBVA');
                str = PositivoNegativo(parseFloat(arr['VAR_MTOSALDO_SCOT_CIERRE']), 'Y2DSCOTIA');
                str = PositivoNegativo(parseFloat(arr['VAR_MTOSALDO_BIF_CIERRE']), 'Y2DBIF');
                str = PositivoNegativo(parseFloat(arr['VAR_MTOSALDO_OTROS_CIERRE']), 'Y2DOTROS');
                str = PositivoNegativo(parseFloat(arr['VAR_MTOSALDO_SF_CIERRE']), 'Y2DTOT');

                $('#PCTY2DIBK').html(PositivoNegativo(parseFloat(arr['PCT_MTOSALDO_IBK_CIERRE']), 'PCTY2DIBK'));
                $('#PCTY2DBCP').html(PositivoNegativo(parseFloat(arr['PCT_MTOSALDO_BCP_CIERRE']), 'PCTY2DBCP'));
                $('#PCTY2DBBVA').html(PositivoNegativo(parseFloat(arr['PCT_MTOSALDO_BBVA_CIERRE']), 'PCTY2DBBVA'));
                $('#PCTY2DSCOTIA').html(PositivoNegativo(parseFloat(arr['PCT_MTOSALDO_SCOT_CIERRE']), 'PCTY2DSCOTIA'));
                $('#PCTY2DBIF').html(PositivoNegativo(parseFloat(arr['PCT_MTOSALDO_BIF_CIERRE']), 'PCTY2DBIF'));
                $('#PCTY2DOTROS').html(PositivoNegativo(parseFloat(arr['PCT_MTOSALDO_OTROS_CIERRE']), 'PCTY2DOTROS'));
                $('#PCTY2DTOT').html(PositivoNegativo(parseFloat(arr['PCT_MTOSALDO_SF_CIERRE']), 'PCTY2DTOT'));

                $('#VAIBK').html(milmillones(parseFloat(arr['VAR_MTOSALDO_IBK_ANUAL'])));
                $('#VABCP').html(milmillones(parseFloat(arr['VAR_MTOSALDO_BCP_ANUAL'])));
                $('#VABBVA').html(milmillones(parseFloat(arr['VAR_MTOSALDO_BBVA_ANUAL'])));
                $('#VASCOTIA').html(milmillones(parseFloat(arr['VAR_MTOSALDO_SCOT_ANUAL'])));
                $('#VABIF').html(milmillones(parseFloat(arr['VAR_MTOSALDO_BIF_ANUAL'])));
                $('#VAOTROS').html(milmillones(parseFloat(arr['VAR_MTOSALDO_OTROS_ANUAL'])));
                $('#VATOT').html(milmillones(parseFloat(arr['VAR_MTOSALDO_SF_ANUAL'])));

                str = PositivoNegativo(parseFloat(arr['VAR_MTOSALDO_IBK_ANUAL']), 'VAIBK');
                str = PositivoNegativo(parseFloat(arr['VAR_MTOSALDO_BCP_ANUAL']), 'VABCP');
                str = PositivoNegativo(parseFloat(arr['VAR_MTOSALDO_BBVA_ANUAL']), 'VABBVA');
                str = PositivoNegativo(parseFloat(arr['VAR_MTOSALDO_SCOT_ANUAL']), 'VASCOTIA');
                str = PositivoNegativo(parseFloat(arr['VAR_MTOSALDO_BIF_ANUAL']), 'VABIF');
                str = PositivoNegativo(parseFloat(arr['VAR_MTOSALDO_OTROS_ANUAL']), 'VAOTROS');
                str = PositivoNegativo(parseFloat(arr['VAR_MTOSALDO_SF_ANUAL']), 'VATOT');
                $('#PCTVAIBK').html(PositivoNegativo(parseFloat(arr['PCT_MTOSALDO_IBK_ANUAL']), 'PCTVAIBK'));
                $('#PCTVABCP').html(PositivoNegativo(parseFloat(arr['PCT_MTOSALDO_BCP_ANUAL']), 'PCTVABCP'));
                $('#PCTVABBVA').html(PositivoNegativo(parseFloat(arr['PCT_MTOSALDO_BBVA_ANUAL']), 'PCTVABBVA'));
                $('#PCTVASCOTIA').html(PositivoNegativo(parseFloat(arr['PCT_MTOSALDO_SCOT_ANUAL']), 'PCTVASCOTIA'));
                $('#PCTVABIF').html(PositivoNegativo(parseFloat(arr['PCT_MTOSALDO_BIF_ANUAL']), 'PCTVABIF'));
                $('#PCTVAOTROS').html(PositivoNegativo(parseFloat(arr['PCT_MTOSALDO_OTROS_ANUAL']), 'PCTVAOTROS'));
                $('#PCTVATOT').html(PositivoNegativo(parseFloat(arr['PCT_MTOSALDO_SF_ANUAL']), 'PCTVATOT'));
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                $('#SALDOACTDIR').html(milmillones(parseFloat(arr2['MF_DIR_YTD_ACT'])));
                $('#SALDOACTDEP').html(milmillones(parseFloat(arr2['MF_DEP_YTD_ACT'])));
                $('#SALDOACTINOFF').html(milmillones(parseFloat(arr2['INOF_YTD_ACT'])));
                $('#SALDOACTIND').html(milmillones(parseFloat(arr2['INOF_IND_YTD_ACT'])));
                $('#SALDOACTCAM').html(milmillones(parseFloat(arr2['INOF_CAMB_YTD_ACT'])));
                $('#SALDOACTOTROS2').html(milmillones(parseFloat(arr2['INOF_OTROS_YTD_ACT'])));
                $('#SALDOACTTOT2').html(milmillones(parseFloat(arr2['TOTAL_ING_YTD_ACT'])));

                $('#Y2DDIR').html(milmillones(parseFloat(arr2['VAR_MF_DIR_YTD_ANT'])));
                $('#Y2DDEP').html(milmillones(parseFloat(arr2['VAR_MF_DEP_YTD_ANT'])));
                $('#Y2DINOFF').html(milmillones(parseFloat(arr2['VAR_INOF_YTD_ANT'])));
                $('#Y2DIND').html(milmillones(parseFloat(arr2['VAR_INOF_IND_YTD_ANT'])));
                $('#Y2DCAM').html(milmillones(parseFloat(arr2['VAR_INOF_CAMB_YTD_ANT'])));
                $('#Y2DOTROS2').html(milmillones(parseFloat(arr2['VAR_INOF_OTROS_YTD_ANT'])));
                $('#Y2DTOT2').html(milmillones(parseFloat(arr2['VAR_TOTAL_ING_YTD_ANT'])));

                str = PositivoNegativo(parseFloat(arr2['VAR_MF_DIR_YTD_ANT']), 'Y2DDIR');
                str = PositivoNegativo(parseFloat(arr2['VAR_MF_DEP_YTD_ANT']), 'Y2DDEP');
                str = PositivoNegativo(parseFloat(arr2['VAR_INOF_YTD_ANT']), 'Y2DINOFF');
                str = PositivoNegativo(parseFloat(arr2['VAR_INOF_IND_YTD_ANT']), 'Y2DIND');
                str = PositivoNegativo(parseFloat(arr2['VAR_INOF_CAMB_YTD_ANT']), 'Y2DCAM');
                str = PositivoNegativo(parseFloat(arr2['VAR_INOF_OTROS_YTD_ANT']), 'Y2DOTROS2');
                str = PositivoNegativo(parseFloat(arr2['VAR_TOTAL_ING_YTD_ANT']), 'Y2DTOT2');

                $('#PCTY2DDIR').html(PositivoNegativo(parseFloat(arr2['PCT_MF_DIR_YTD_ANT']), 'PCTY2DDIR'));
                $('#PCTY2DDEP').html(PositivoNegativo(parseFloat(arr2['PCT_MF_DEP_YTD_ANT']), 'PCTY2DDEP'));
                $('#PCTY2DINOFF').html(PositivoNegativo(parseFloat(arr2['PCT_INOF_YTD_ANT']), 'PCTY2DINOFF'));
                $('#PCTY2DIND').html(PositivoNegativo(parseFloat(arr2['PCT_INOF_IND_YTD_ANT']), 'PCTY2DIND'));
                $('#PCTY2DCAM').html(PositivoNegativo(parseFloat(arr2['PCT_INOF_CAMB_YTD_ANT']), 'PCTY2DCAM'));
                $('#PCTY2DOTROS2').html(PositivoNegativo(parseFloat(arr2['PCT_INOF_OTROS_YTD_ANT']), 'PCTY2DOTROS2'));
                $('#PCTY2DTOT2').html(PositivoNegativo(parseFloat(arr2['PCT_TOTAL_ING_YTD_ANT']), 'PCTY2DTOT2'));

                $('#VADIR').html(milmillones(parseFloat(arr2['VAR_MF_DIR_ACUM_ANUAL'])));
                $('#VADEP').html(milmillones(parseFloat(arr2['VAR_MF_DEP_ACUM_ANUAL'])));
                $('#VAINOFF').html(milmillones(parseFloat(arr2['VAR_INOF_ACUM_ANUAL'])));
                $('#VAIND').html(milmillones(parseFloat(arr2['VAR_INOF_CAMB_ACUM_ANUAL'])));
                $('#VACAM').html(milmillones(parseFloat(arr2['VAR_INOF_IND_ACUM_ANUAL'])));
                $('#VAOTROS2').html(milmillones(parseFloat(arr2['VAR_INOF_OTROS_ACUM_ANUAL'])));
                $('#VATOT2').html(milmillones(parseFloat(arr2['VAR_TOTAL_ING_ACUM_ANUAL'])));
                str = PositivoNegativo(parseFloat(arr2['VAR_MF_DIR_ACUM_ANUAL']), 'VADIR');
                str = PositivoNegativo(parseFloat(arr2['VAR_MF_DEP_ACUM_ANUAL']), 'VADEP');
                str = PositivoNegativo(parseFloat(arr2['VAR_INOF_ACUM_ANUAL']), 'VAINOFF');
                str = PositivoNegativo(parseFloat(arr2['VAR_INOF_CAMB_ACUM_ANUAL']), 'VAIND');
                str = PositivoNegativo(parseFloat(arr2['VAR_INOF_IND_ACUM_ANUAL']), 'VACAM');
                str = PositivoNegativo(parseFloat(arr2['VAR_INOF_OTROS_ACUM_ANUAL']), 'VAOTROS2');
                str = PositivoNegativo(parseFloat(arr2['VAR_TOTAL_ING_ACUM_ANUAL']), 'VATOT2');

                $('#PCTVADIR').html(PositivoNegativo(parseFloat(arr2['PCT_MF_DIR_ACUM_ANUAL']), 'PCTVADIR'));
                $('#PCTVADEP').html(PositivoNegativo(parseFloat(arr2['PCT_MF_DEP_ACUM_ANUAL']), 'PCTVADEP'));
                $('#PCTVAINOFF').html(PositivoNegativo(parseFloat(arr2['PCT_INOF_ACUM_ANUAL']), 'PCTVAINOFF'));
                $('#PCTVAIND').html(PositivoNegativo(parseFloat(arr2['PCT_INOF_CAMB_ACUM_ANUAL']), 'PCTVAIND'));
                $('#PCTVACAM').html(PositivoNegativo(parseFloat(arr2['PCT_INOF_IND_ACUM_ANUAL']), 'PCTVACAM'));
                $('#PCTVAOTROS2').html(PositivoNegativo(parseFloat(arr2['PCT_INOF_OTROS_ACUM_ANUAL']), 'PCTVAOTROS2'));
                $('#PCTVATOT2').html(PositivoNegativo(parseFloat(arr2['PCT_TOTAL_ING_ACUM_ANUAL']), 'PCTVATOT2'));
            }
        });

    }

    function PAPACT(arr) {
        if (arr !== undefined) {
            var VAR = arr['VAR_VOLUMEN_PAP'];
            var VARP = arr['PCT_VAR_VOLUMEN_PAP'];
            var CUMP = arr['PCT_CUMPLIMIENTO'];
            var ALCANCE = arr['PCT_ALCANCE'];
            var PPTO = arr['META_ACT'];
            var PPTOANUAL = arr['META_ACUM'];
            var ULT = arr['VOLUMEN'];
            var ULTACUM = arr['VOLUMEN_ACUM_ACT'];

            var vector = [arr['M0'], arr['M1'], arr['M2'], arr['M3'], arr['M4'],
                arr['M5'], arr['M6'], arr['M7'], arr['M8'],
                arr['M9'], arr['M10'], arr['M11'], arr['M12']
            ];
            var mesesCol = [];
            var fechas = ["Diciembre", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Setiembre", "Octubre", "Noviembre", "Diciembre"]
            var labelsF = [];
            for (let index = 0; index < vector.length; index++) {
                if (vector[index] != null) {
                    mesesCol.push(vector[index]);
                    labelsF.push(fechas[index]);

                }
            }
            if (window.chartVP) window.chartVP.destroy();
            window.chartVP = new Chart(ctxVP, {
                type: 'line',
                data: {
                    labels: labelsF,
                    datasets: [{
                        data: mesesCol
                    }]
                },
                options: {
                    responsive: true,
                    legend: {
                        display: false
                    },
                    elements: {
                        line: {
                            borderColor: '#4dc487',
                            borderWidth: 2
                        },
                        point: {
                            radius: 4
                        }
                    },
                    tooltips: {
                        callbacks: {
                            label: function(tooltipItem, chart) {
                                return 'S/ ' + milmillones(tooltipItem.yLabel);
                            }
                        }
                    },
                    scales: {
                        yAxes: [{
                            display: false
                        }],
                        xAxes: [{
                            display: false
                        }]
                    }
                }
            });
            $('#vproveedoresVARPCT').html(PositivoNegativo(VARP, 'vproveedoresVARPCT'));
            $('#vproveedoresPPTOPCT').html(funre2(CUMP) + '%');
            $('#vproveedoresPPTOACUM').html(milmillones(PPTOANUAL));
            $('#vproveedoresPPTOACUMPCT').html(funre2(ALCANCE) + '%');
            $("#vproveedoresVAR").html(milmillones(VAR));
            $("#vproveedoresPPTO").html(milmillones(PPTO));
            $("#vproveedoresULT").html(milmillones(ULT));
            $("#vproveedoresACUM").html(milmillones(ULTACUM));
            if (CUMP <= 0.8) {
                $("#vproveedoresPPTOPCTSEM").css({
                    'background-color': '#e04f31'
                });
            } else if (CUMP >= 1) {
                $("#vproveedoresPPTOPCTSEM").css({
                    'background-color': '#4dc487'
                });
            } else {
                $("#vproveedoresPPTOPCTSEM").css({
                    'background-color': 'rgb(255, 192, 0)'
                });
            }
            if (ALCANCE <= 0.8) {
                $("#vproveedoresPPTOACUMPCTSEM").css({
                    'background-color': '#e04f31'
                });
            } else if (ALCANCE >= 1) {
                $("#vproveedoresPPTOACUMPCTSEM").css({
                    'background-color': '#4dc487'
                });
            } else {
                $("#vproveedoresPPTOACUMPCTSEM").css({
                    'background-color': 'rgb(255, 192, 0)'
                });
            }
        } else {
            if (window.chartVP) window.chartVP.destroy();
            $("#vproveedoresVAR").html('');
            $("#vproveedoresPPTO").html('');
            $("#vproveedoresCUMPL").html('');
            $("#vproveedoresULT").html('');
            $("#vproveedoresCUMPLBAR").css({
                'background-color': '#d13913',
                'width': '0%'
            });
            $('#vproveedoresVARPCT').html('');
        }
        $('#maskWaiting').removeClass('maskAppear');

    }

    function DCTFCT(arr) {
        if (arr !== undefined) {
            var VAR = arr['VAR'];
            var VARP = arr['VAR_P'];
            var PCT_AVANE = arr['PCT_AVANCE'];
            var PCT_CUMPLI = arr['PCT_CUMPLIMIENTO'];
            var PPTO = arr['META_ACTUAL'];
            var PPTOA = arr['META_ANUAL'];
            var ULT = arr['SALDO_ACT'];

            var vector = [arr['M1'], arr['M2'], arr['M3'], arr['M4'],
                arr['M5'], arr['M6'], arr['M7'], arr['M8'],
                arr['M9'], arr['M10'], arr['M11'], arr['M12']
            ];
            var mesesCol = ['Diciembre', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'];
            var mesesCol2 = [];
            var vector2 = [];
            for (let index = 0; index < vector.length; index++) {
                if (vector[index] != null) {
                    vector2.push(vector[index]);
                    mesesCol2.push(mesesCol[index]);
                }
            }
            if (window.chartDF) window.chartDF.destroy();
            window.chartDF = new Chart(ctxDF, {
                type: 'line',
                data: {
                    labels: mesesCol2,
                    datasets: [{
                        data: vector2
                    }]
                },
                options: {
                    responsive: false,
                    legend: {
                        display: false
                    },
                    elements: {
                        line: {
                            borderColor: '#4dc487',
                            borderWidth: 2
                        },
                        point: {
                            radius: 4
                        }
                    },
                    tooltips: {
                        callbacks: {
                            label: function(tooltipItem, chart) {
                                return 'S/ ' + milmillones(tooltipItem.yLabel);
                            }
                        }
                    },
                    scales: {
                        yAxes: [{
                            display: false
                        }],
                        xAxes: [{
                            display: false
                        }]
                    }
                }
            });
            $('#dfactoringVARPCT').html(PositivoNegativo(VARP, 'dfactoringVARPCT'))
            $("#dfactoringVAR").html(milmillones(VAR));
            $("#dfactoringPPTO").html(milmillones(PPTO));
            $("#dfactoringPPTOPCT").html(funre2(PCT_CUMPLI) + '%');
            $("#dfactoringPPTOACUM").html(milmillones(PPTOA));
            $("#dfactoringPPTOACUMPCT").html(funre2(PCT_AVANE) + '%');
            $("#dfactoringULT").html(milmillones(ULT));
            if (PCT_CUMPLI <= 0.8) {
                $("#dfactoringPPTOPCTSEM").css({
                    'background-color': '#e04f31'
                });
            } else if (PCT_CUMPLI >= 1) {
                $("#dfactoringPPTOPCTSEM").css({
                    'background-color': '#4dc487'
                });
            } else {
                $("#dfactoringPPTOPCTSEM").css({
                    'background-color': 'rgb(255, 192, 0)'
                });
            }
            if (PCT_AVANE <= 0.8) {
                $("#dfactoringPPTOACUMPCTSEM").css({
                    'background-color': '#e04f31'
                });
            } else if (PCT_AVANE >= 1) {
                $("#dfactoringPPTOACUMPCTSEM").css({
                    'background-color': '#4dc487'
                });
            } else {
                $("#dfactoringPPTOACUMPCTSEM").css({
                    'background-color': 'rgb(255, 192, 0)'
                });
            }
        } else {
            if (window.chartDF) window.chartDF.destroy();
            $("#dfactoringVAR").html('');
            $("#dfactoringPPTO").html('');
            $("#dfactoringCUMPL").html('');
            $("#dfactoringULT").html('');
            $("#dfactoringCUMPLBAR").css({
                'background-color': '#d13913',
                'width': '0%'
            });
            $('#dfactoringVARPCT').html('');
        }
    }

    function CUENTASUELDO() {
        var id = $("#id_usuario").val();
        $.ajax({
            type: "GET",
            data: {
                "id": id
            },
            async: false,
            url: "{{route('CUENTASUELDO')}}",
            success: function(result) {
                var arrTot = JSON.parse(result);
                var arr = arrTot["DESCTOS Y FACTORING"][0];
                DCTFCT(arr);
                var arr = arrTot["PAP"][0];
                var fechaPAP = arrTot["FECHAPAP"][0];
                var fechaDES = arrTot["FECHADES"][0];
                $("#actualizacionPAP").html(fechaPAP["FECHA_ACTUALIZACION"]);
                $("#actualizacionDES").html(fechaDES["FECHA_ACTUALIZACION"]);
                var arr = arrTot["CSUELDO"][0];
                if (arr !== undefined) {
                    var VAR = arr['VAR'];
                    var VARP = arr['VAR_P'];
                    var CUMP = arr['CUMP_P'];
                    var PPTO = arr['META'];
                    var ULT = arr['MONTO_FOCO'];

                    var vector = [arr['M1'], arr['M2'], arr['M3'], arr['M4'],
                        arr['M5'], arr['M6'], arr['M7'], arr['M8'],
                        arr['M9'], arr['M10'], arr['M11'], arr['M12']
                    ];
                    var mesesCol = [];
                    for (let index = 0; index < vector.length; index++) {
                        if (vector[index] != null) {
                            mesesCol.push(vector[index]);
                        }
                    }
                    if (window.chartCS) window.chartCS.destroy();
                    window.chartCS = new Chart(ctxCS, {
                        type: 'line',
                        data: {
                            labels: mesesCol,
                            datasets: [{
                                data: mesesCol
                            }]
                        },
                        options: {
                            responsive: false,
                            legend: {
                                display: false
                            },
                            elements: {
                                line: {
                                    borderColor: '#4dc487',
                                    borderWidth: 2
                                },
                                point: {
                                    radius: 0
                                }
                            },
                            tooltips: {
                                enabled: false
                            },
                            scales: {
                                yAxes: [{
                                    display: false
                                }],
                                xAxes: [{
                                    display: false
                                }]
                            }
                        }
                    });
                    $('#csueldoVARPCT').html(PositivoNegativo(VARP, 'csueldoVARPCT'))
                    $("#csueldoVAR").html(milmillones(VAR));
                    $("#csueldoPPTO").html(milmillones(PPTO));
                    $("#csueldoCUMPL").html(funre2(CUMP) + ' %');
                    $("#csueldoULT").html(milmillones(ULT));
                    if (CUMP <= 0.8) {
                        $("#csueldoCUMPLBAR").css({
                            'background-color': '#d13913',
                            'width': funre2(CUMP) + '%'
                        });
                    } else if (CUMP >= 1) {
                        $("#csueldoCUMPLBAR").css({
                            'background-color': '#4dc487',
                            'width': funre2(CUMP) + '%'
                        });
                    } else {
                        $("#csueldoCUMPLBAR").css({
                            'width': funre2(CUMP) + '%'
                        });
                    }
                } else {
                    if (window.chartCS) window.chartCS.destroy();
                    $("#csueldoVAR").html('');
                    $("#csueldoPPTO").html('');
                    $("#csueldoCUMPL").html('');
                    $("#csueldoULT").html('');
                    $("#csueldoCUMPLBAR").css({
                        'background-color': '#d13913',
                        'width': '0%'
                    });
                    $('#csueldoVARPCT').html('');
                }
            }
        });
    }


    function PAP() {
        var id = $("#id_usuario").val();
        if (id != "null") {
            $.ajax({
                type: "GET",
                data: {
                    "id": id
                },
                async: true,
                url: "{{route('PAP')}}",
                success: function(result) {
                    var arr = JSON.parse(result);
                    if (arr !== undefined) {
                        if (arr['FOTO'] != '') {
                            var FOTO = JSON.parse(arr['FOTO'])[0];
                            var str = '';
                            $('#ORD').html(FOTO['CTD_ORD_ACT']);
                            $('#BEN').html(numberWithCommas(FOTO['CTD_BEN_ACT']));
                            $('#CP12').html(FOTO['NUEVO_BEN_ACT']);
                            $('#POT').html(FOTO['CTD_PROX_ACT']);
                            str = PositivoNegativo(FOTO['VAR_CTD_ORD'], 'VAR_CTD_ORD');
                            $('#varORD').html(FOTO['VAR_CTD_ORD'] == 0 ? str : FOTO['VAR_CTD_ORD'] > 0 ? '+' + FOTO['VAR_CTD_ORD'] : '-' + Math.abs(FOTO['VAR_CTD_ORD']));
                            str = PositivoNegativo(FOTO['VAR_NUEVOS_BEN'], 'varBEN');
                            $('#varBEN').html(numberWithCommas(FOTO['VAR_CTD_BEN'] == 0 ? str : FOTO['VAR_CTD_BEN'] > 0 ? '+' + FOTO['VAR_CTD_BEN'] : '-' + Math.abs(FOTO['VAR_CTD_BEN'])));
                            str = PositivoNegativo(FOTO['VAR_NUEVOS_BEN'], 'varCP12');
                            $('#varCP12').html(FOTO['VAR_NUEVOS_BEN'] == 0 ? str : FOTO['VAR_NUEVOS_BEN'] > 0 ? '+' + FOTO['VAR_NUEVOS_BEN'] : '-' + Math.abs(FOTO['VAR_NUEVOS_BEN']));

                        } else {
                            $('#ORD').html('');
                            $('#BEN').html('');
                            $('#CP12').html('');
                            $('#POT').html('');
                            $('#varORD').html('');
                            $('#varBEN').html('');
                            $('#varCP12').html('');
                            $('#varPOT').html('');
                        }
                        if (arr['EVOLUTIVO'] != '') {
                            var EVOLUTIVO = JSON.parse(arr['EVOLUTIVO'])[0];
                            $('#IBC').html(milmillones(EVOLUTIVO['VOL_PAP_CTA_ACT']));
                            str = PositivoNegativo(EVOLUTIVO['VAR_VOL_PAP_CTA'], 'varIBC');
                            str = PositivoNegativo(EVOLUTIVO['VOL_PAP_CTA_ACUM_ACT'], 'IBCACUM');
                            $('#varIBC').html(milmillones(EVOLUTIVO['VAR_VOL_PAP_CTA']));
                            $('#PCTvarIBC').html(PositivoNegativo(EVOLUTIVO['PCT_VOL_PAP_CTA'], 'PCTvarIBC'));
                            $('#IBCACUM').html(milmillones(EVOLUTIVO['VOL_PAP_CTA_ACUM_ACT']));

                            $('#IBCN').html(milmillones(EVOLUTIVO['VOL_PAP_CON_CTA_ACT']));
                            str = PositivoNegativo(EVOLUTIVO['VAR_VOL_PAP_CON_CTA'], 'varIBCN');
                            str = PositivoNegativo(EVOLUTIVO['VOL_PAP_CON_CTA_ACUM_ACT'], 'IBCNACUM');
                            $('#varIBCN').html(milmillones(EVOLUTIVO['VAR_VOL_PAP_CON_CTA']));
                            $('#PCTvarIBCN').html(PositivoNegativo(EVOLUTIVO['PCT_VOL_PAP_CON_CTA'], 'PCTvarIBCN'));
                            $('#IBCNACUM').html(milmillones(EVOLUTIVO['VOL_PAP_CON_CTA_ACUM_ACT']));

                            $('#CIBK').html(milmillones(EVOLUTIVO['VOL_PAP_SIN_CTA_ACT']));
                            str = PositivoNegativo(EVOLUTIVO['VAR_VOL_PAP_SIN_CTA'], 'varCIBK');
                            str = PositivoNegativo(EVOLUTIVO['VOL_PAP_SIN_CTA_ACUM_ACT'], 'CIBKACUM');
                            $('#varCIBK').html(milmillones(EVOLUTIVO['VAR_VOL_PAP_SIN_CTA']));
                            $('#PCTvarCIBK').html(PositivoNegativo(EVOLUTIVO['PCT_VOL_PAP_SIN_CTA'], 'PCTvarCIBK'));
                            $('#CIBKACUM').html(milmillones(EVOLUTIVO['VOL_PAP_SIN_CTA_ACUM_ACT']));

                            PAPACT(EVOLUTIVO);
                        }
                        if (arr['DCTO'] != '') {
                            var EVOLUTIVO = JSON.parse(arr['DCTO'])[0];
                            DCTFCT(EVOLUTIVO);
                        }
                        if (arr['EVOLUTIVO2'] != '') {
                            var EVOLUTIVO2 = JSON.parse(arr['EVOLUTIVO2']);
                            var IBC_PAP = [];
                            var IBCN_PAP = [];
                            var CIBK_PAP = [];
                            var fecha = [];
                            var dif = 0;
                            var str = '';
                            var fecha2 = ["Dic", "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Set", "Oct", "Nov", "Dic"];
                            for (let index = 0; index < EVOLUTIVO2.length; index++) {
                                const element = EVOLUTIVO2[index];
                                fecha.push(fecha2[index] + "-" + element['PERIODO'].substring(2, 4));
                                IBC_PAP.push(Math.round(element['VOL_PAP_CTA']));
                                IBCN_PAP.push(Math.round(element['VOL_PAP_CON_CTA']));
                                CIBK_PAP.push(Math.round(element['VOL_PAP_SIN_CTA']));
                            }

                            if (window.LineChartPP) window.LineChartPP.destroy();
                            window.LineChartPP = new Chart(ctxPP, {
                                type: 'line',
                                data: {
                                    labels: fecha,
                                    datasets: [{
                                            label: 'Abono en cuentas',
                                            borderColor: '#5b9bd5',
                                            fill: false,
                                            data: IBC_PAP,
                                        },
                                        {
                                            label: 'Cheque + interbancario (Con cuenta)',
                                            borderColor: '#ed7d31',
                                            fill: false,
                                            data: IBCN_PAP,
                                        },
                                        {
                                            label: 'Cheque + interbancario (Sin cuenta)',
                                            borderColor: '#a5a5a5',
                                            fill: false,
                                            data: CIBK_PAP,
                                        }
                                    ]
                                },
                                options: {
                                    responsive: true,
                                    legend: {
                                        display: false
                                    },
                                    tooltips: {
                                        callbacks: {
                                            label: function(tooltipItem, chart) {
                                                var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label;
                                                return datasetLabel + ': S/ ' + milmillones(tooltipItem.yLabel);
                                            }
                                        }
                                    },
                                    scales: {
                                        yAxes: [{
                                            ticks: {
                                                callback: function(value) {
                                                    return milmillones(value);
                                                }
                                            }
                                        }]
                                    }
                                }
                            });
                        } else {
                            if (window.LineChartPP) window.LineChartPP.destroy();
                        }
                    } else {
                        if (window.LineChartPP) window.LineChartPP.destroy();
                        $('#ORD').html('');
                        $('#C1P').html('');
                        $('#C2P').html('');
                        $('#POT').html('');
                        $('#varORD').html('');
                        $('#varC1P').html('');
                        $('#varC2P').html('');
                        $('#varPOT').html('');
                    }
                }
            });
        } else {
            if (window.LineChartPP) window.LineChartPP.destroy();
            $('#ORD').html('');
            $('#C1P').html('');
            $('#C2P').html('');
            $('#POT').html('');
            $('#varORD').html('');
            $('#varC1P').html('');
            $('#varC2P').html('');
            $('#varPOT').html('');
        }

    }

    function CSUELDOG() {
        var id = $("#id_usuario").val();
        if (id != "null") {
            $.ajax({
                type: "GET",
                data: {
                    "id": id
                },
                async: true,
                url: "{{route('CSUELDO')}}",
                success: function(result) {
                    var arr = JSON.parse(result);
                    if (arr !== undefined) {
                        var CSUELDO = [];
                        var fecha = [];
                        var dif = 0;
                        var ppt = 0;
                        var str = '';
                        for (let index = 0; index < arr.length; index++) {
                            const element = arr[index];
                            fecha.push(Math.round(element['MES']));
                            CSUELDO.push(element['MONTO_FOCO']);
                        }
                        dif = (CSUELDO[arr.length - 1] - CSUELDO[arr.length - 2]);
                        str = PositivoNegativo(dif, 'ultVarEco')
                        $('#ultVarEco').html(dif > 0 ? '+' + milmillones(Math.abs(dif)) : '-' + milmillones(Math.abs(dif)));

                        ppt = $("#csueldoPPTO").text().indexOf('MM') ? parseFloat($("#csueldoPPTO").text().replace(/,/g, '')) * 1000 * 1000 : parseFloat($("#csueldoPPTO").text().replace(/,/g, '')) * 1000;
                        dif = (CSUELDO[arr.length - 1] - ppt);
                        str = PositivoNegativo(dif, 'ultVarPptoEco')
                        $('#ultVarPptoEco').html(dif > 0 ? '+' + milmillones(Math.abs(dif)) : '-' + milmillones(Math.abs(dif)));

                        if (window.LineChartCSG) window.LineChartCSG.destroy();
                        window.LineChartCSG = new Chart(ctxCSG, {
                            type: 'line',
                            data: {
                                labels: fecha,
                                datasets: [{
                                    label: 'Abono Foco',
                                    borderColor: '#5b9bd5',
                                    fill: false,
                                    data: CSUELDO,
                                }]
                            },
                            options: {
                                responsive: true,
                                legend: {
                                    display: false
                                },
                                scales: {
                                    yAxes: [{
                                        ticks: {
                                            display: false
                                        }
                                    }]
                                },
                                tooltips: {
                                    callbacks: {
                                        label: function(tooltipItem, chart) {
                                            var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label;
                                            return datasetLabel + ': S/ ' + milmillones(tooltipItem.yLabel);
                                        }
                                    }
                                },
                            }
                        });
                    } else {
                        if (window.LineChartCSG) window.LineChartCSG.destroy();
                    }
                }
            });
        } else {
            if (window.LineChartCSG) window.LineChartCSG.destroy();
        }
    }

    function Descuentos() {
        var id = $("#id_usuario").val();
        if (id != "null") {
            $.ajax({
                type: "GET",
                data: {
                    "id": id
                },
                async: true,
                url: "{{route('Descuentos')}}",
                success: function(result) {
                    var arr = JSON.parse(result);
                    var fecha = [];
                    var sFN = [];
                    var sDEF = [];
                    var sDFF = [];
                    var sDL = [];
                    var sDP = [];
                    var evolutivo = JSON.parse(arr["EVOLUTIVO"]);
                    var foto = JSON.parse(arr["FOTO"])[0];
                    $('#VV').html(milmillones(foto['SALDO_VEN_P0']));
                    $('#Inc').html(milmillones(foto['AUMENTO']));
                    $('#Cai').html(milmillones(foto['CAIDA']));
                    $('#DsctSF').html(milmillones(foto['DSCTO_SF_M0']));
                    $('#VarInc').html(PositivoNegativo(foto['PCT_AUMENTO'], 'VarInc'));
                    $('#varCai').html(PositivoNegativo(foto['PCT_CAIDA'], 'varCai'));
                    $('#varDsctSF').html(PositivoNegativo(foto['PCT_SF'], 'varDsctSF'));
                    fecha2 = ["Dic", "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Set", "Oct", "Nov", "Dic"];
                    for (let index = 0; index < evolutivo.length; index++) {
                        fecha[index] = fecha2[index] + "-" + evolutivo[index]['CODMES'].substring(2, 4);
                        sFN.push(evolutivo[index]['SALDO_FN']);
                        sDEF.push(evolutivo[index]['SALDO_DEF']);
                        sDFF.push(evolutivo[index]['SALDO_DFF']);
                        sDL.push(evolutivo[index]['SALDO_DL']);
                        sDP.push(evolutivo[index]['SALDO_DP']);
                    }
                    var dif = 0;
                    var str = '';
                    const i = evolutivo.length - 1;
                    dif = sFN[i] - sFN[i - 1];
                    str = PositivoNegativo(dif, "SaldoFN");
                    str = PositivoNegativo(dif / sFN[i - 1], "VarSaldoFN");
                    $('#VarSaldoFN').html(str);
                    $('#SaldoFN').html(milmillones(dif));
                    $("#SaldoFNULT").html(milmillones(sFN[i]));
                    dif = sDEF[i] - sDEF[i - 1];
                    str = PositivoNegativo(dif, "SaldoDE");
                    str = PositivoNegativo(dif / sDEF[i - 1], "VarSaldoDE");
                    $('#VarSaldoDE').html(str);
                    $('#SaldoDE').html(milmillones(dif));
                    $("#SaldoDEULT").html(milmillones(sDEF[i]));
                    dif = sDFF[i] - sDFF[i - 1];
                    str = PositivoNegativo(dif, "SaldoDFF");
                    str = PositivoNegativo(dif / sDFF[i - 1], "VarSaldoDFF");
                    $('#VarSaldoDFF').html(str);
                    $('#SaldoDFF').html(milmillones(dif));
                    $("#SaldoDFFULT").html(milmillones(sDFF[i]));
                    dif = sDL[i] - sDL[i - 1];
                    str = PositivoNegativo(dif, "SaldoDL");
                    str = PositivoNegativo(dif / sDL[i - 1], "VarSaldoDL");
                    $('#VarSaldoDL').html(str);
                    $('#SaldoDL').html(milmillones(dif));
                    $("#SaldoDLULT").html(milmillones(sDL[i]));
                    dif = sDP[i] - sDP[i - 1];
                    str = PositivoNegativo(dif, "SaldoDP");
                    str = PositivoNegativo(dif / sDP[i - 1], "VarSaldoDP");
                    $('#VarSaldoDP').html(str);
                    $('#SaldoDP').html(milmillones(dif));
                    $("#SaldoDPULT").html(milmillones(sDP[i]));
                    var sFN = [];
                    var sDEF = [];
                    var sDFF = [];
                    var sDL = [];
                    var sDP = [];
                    for (let index = 0; index < evolutivo.length; index++) {
                        sFN.push(evolutivo[index]['SALDO_FN']);
                        sDEF.push(evolutivo[index]['SALDO_DEF']);
                        sDFF.push(evolutivo[index]['SALDO_DFF']);
                        sDL.push(evolutivo[index]['SALDO_DL']);
                        sDP.push(evolutivo[index]['SALDO_DP']);
                    }
                    if (window.LineChartDG) window.LineChartDG.destroy();
                    window.LineChartDG = new Chart(ctxDG, {
                        type: 'line',
                        data: {
                            labels: fecha,
                            datasets: [{
                                    label: 'Factura Negociable ',
                                    borderColor: '#5b9bd5',
                                    fill: false,
                                    data: sFN,
                                },
                                {
                                    label: 'Dcto. Electrónico',
                                    borderColor: '#ed7d31',
                                    fill: false,
                                    data: sDEF,
                                },
                                {
                                    label: 'Dcto. Físico de Facturas',
                                    borderColor: '#a5a5a5',
                                    fill: false,
                                    data: sDFF,
                                },
                                {
                                    label: 'Dcto. Letras',
                                    borderColor: '#548235',
                                    fill: false,
                                    data: sDL,
                                },
                                {
                                    label: 'Dcto. Pagarés',
                                    borderColor: '#4472c4',
                                    fill: false,
                                    data: sDP,
                                }
                            ]
                        },
                        options: {
                            responsive: true,
                            legend: {
                                display: false
                            },
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        callback: function(value) {
                                            return milmillones(value);
                                        }
                                    }
                                }]
                            },
                            tooltips: {
                                callbacks: {
                                    label: function(tooltipItem, chart) {
                                        var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label;
                                        return datasetLabel + ': S/ ' + milmillones(tooltipItem.yLabel);
                                    }
                                }
                            },
                        }
                    });

                }
            });

        } else {
            if (window.LineChartDG) window.LineChartDG.destroy();
            $('#VV').html('');
            $('#Inc').html('');
            $('#Cai').html('');
            $('#DsctSF').html('');
            $('#VarInc').html('');
            $('#varCai').html('');
            $('#varDsctSF').html('');
        }
    }

    function milmillones(number) {
        str = '';
        number = funre(number);
        if (Math.abs(number / 1000) >= 1) {
            str = numberWithCommas(Math.round(number / 1000)) + " MM";
        } else {
            str = numberWithCommas(Math.round(number)) + " M";
        }
        return str;
    }

    function PositivoNegativo(number, id) {
        number = funre2(number);
        if (isFinite(number)) {
            str = number + " %";
            if (number > 0) {
                $("#" + id).css("color", "#4dc487");
            } else if (number < 0) {
                $("#" + id).css("color", "#d13913");
            } else {
                str = '-';
            }
        } else {
            str = '-';
        }
        return str;
    }

    function Cumplimiento() {
        var id = $("#id_usuario").val();
        if (id != "null") {
            $.ajax({
                type: "GET",
                data: {
                    "id": id
                },
                async: true,
                url: "{{route('cumplimiento')}}",
                success: function(result) {

                    var arr = JSON.parse(result);
                    var nota = 0;
                    var width_nota = $('#nota').width();
                    var width_financiera = $('#nota_financiera').width();
                    var width_ecosistema = $('#nota_ecosistema').width();
                    if (window.ChartNota) window.ChartNota.destroy();
                    $("#notaScore").html(funre4(arr['NOTA']) + "%");
                    nota = funre4(arr['NOTA']) > 100.00 ? 100.00 : funre4(arr['NOTA']);
                    window.ChartNota = new Chart(ctxNota, {
                        type: 'doughnut',
                        data: {
                            datasets: [{

                                data: [nota, funre4(100.00 - nota)],
                                backgroundColor: [
                                    '#4DD094',
                                    'whitesmoke',
                                ],
                                borderWidth: 0.6
                            }],
                            labels: [
                                'Nota',
                                '',
                            ]
                        },
                        options: {
                            legend: {
                                display: false
                            },
                            cutoutPercentage: 70,
                            responsive: true,
                            tooltips: {
                                enabled: false
                            },
                        }
                    });
                    if (window.ChartFinan) window.ChartFinan.destroy();
                    $("#notaFinan").html(funre4(arr['NOTA_FINANCIERA']) + "%");
                    nota = funre4(arr['NOTA_FINANCIERA']) > 100.00 ? 100.00 : funre4(arr['NOTA_FINANCIERA']);
                    window.ChartFinan = new Chart(ctxFinan, {
                        type: 'doughnut',
                        data: {
                            datasets: [{
                                label: arr['NOTA_FINANCIERA'],
                                data: [nota, funre4(100.00 - nota)],
                                backgroundColor: [
                                    '#4DD094',
                                    'whitesmoke',

                                ],
                                borderWidth: 0.6
                            }],
                            labels: [
                                'Nota Financiera',
                                '',
                            ]
                        },
                        options: {
                            legend: {
                                display: false
                            },
                            cutoutPercentage: 70,
                            responsive: true,
                            tooltips: {
                                enabled: false
                            },
                        }
                    });
                    if (window.ChartEco) window.ChartEco.destroy();
                    $("#notaEco").html(funre4(arr['NOTA_ECOSISTEMA']) + "%");
                    nota = funre4(arr['NOTA_ECOSISTEMA']) > 100.00 ? 100.00 : funre4(arr['NOTA_ECOSISTEMA']);
                    window.ChartEco = new Chart(ctxEco, {
                        type: 'doughnut',
                        data: {
                            datasets: [{
                                label: arr['NOTA_ECOSISTEMA'],
                                data: [nota, funre4(100.00 - nota)],
                                backgroundColor: [
                                    '#4DD094',
                                    'whitesmoke',

                                ],
                                borderWidth: 0.6
                            }],
                            labels: [
                                'Nota Ecosistema',
                                '',
                            ]
                        },
                        options: {
                            legend: {
                                display: false
                            },
                            cutoutPercentage: 70,
                            responsive: true,
                            tooltips: {
                                enabled: false
                            },
                        }
                    });
                    $('#cantCli').html(numberWithCommas(arr['CANT_CLIE']));
                }
            });
        } else {
            $('#cantCli').html('');
            if (window.ChartNota) window.ChartNota.destroy();
            $("#notaScore").html('');
            if (window.ChartFinan) window.ChartFinan.destroy();
            $("#notaFinan").html('');
            if (window.ChartEco) window.ChartEco.destroy();
            $("#notaEco").html('');
        }
    }

    function VarXProducto() {
        const id = $("#id_usuario").val();
        const tipoproducto = $("#filtroProducto").val()?$("#filtroProducto").val():false;
        if(tipoproducto)
            $.ajax({
                type: "GET",
                data: {
                    "id": id,
                    "tipoproducto": tipoproducto
                },
                async: false,
                url: "{{route('VarXProducto')}}",
                success: function(result) {
                    var arr = JSON.parse(result);
                    var fecha = [];
                    var variaciones = [];
                    var pptArray = [];
                    switch (tipoproducto) {
                        case "Col. Directas":
                            ppt = $("#pptocoldirectas").text().indexOf('MM') ? parseFloat($("#pptocoldirectas").text().replace(/,/g, '')) * 1000 * 1000 : parseFloat($("#pptocoldirectas").text().replace(/,/g, '')) * 1000;
                            break;
                        case "Col. Indirectas":
                            ppt = $("#pptocolindirectas").text().indexOf('MM') ? parseFloat($("#pptocolindirectas").text().replace(/,/g, '')) * 1000 * 1000 : parseFloat($("#pptocolindirectas").text().replace(/,/g, '')) * 1000;

                            break;
                        case "Depositos":
                            ppt = $("#pptodeposito").text().indexOf('MM') ? parseFloat($("#pptodeposito").text().replace(/,/g, '')) * 1000 * 1000 : parseFloat($("#pptodeposito").text().replace(/,/g, '')) * 1000;
                            break;
                    }
                    for (let index = 0; index < arr.length; index++) {
                        fecha.push(arr[index]['FECHA2'].substring(0, 6));
                        variaciones.push(arr[index]['SALDO_MILES']);
                        pptArray.push(ppt);
                    }
                    var ultvar = (variaciones[arr.length - 1] - variaciones[arr.length - 2]);
                    var ultvarMes = variaciones[arr.length - 1] - ppt;
                    var str = '';

                    str = PositivoNegativo(ultvar, "ultvar");
                    $("#ultvar").html(ultvar > 0 ? '+' + milmillones(ultvar) : '-' + milmillones(Math.abs(ultvar)));

                    str = PositivoNegativo(ultvarMes, "ultvarPpt");
                    $("#ultvarPpt").html(ultvarMes > 0 ? '+' + milmillones(ultvarMes) : '-' + milmillones(Math.abs(ultvarMes)));

                    if (window.LineChart) window.LineChart.destroy();
                    window.LineChart = new Chart(ctxProd, {
                        type: 'line',
                        data: {
                            labels: fecha,
                            datasets: [{
                                    label: 'Saldo FDP',
                                    data: variaciones,
                                    borderColor: '#4dc487',
                                    fill: false
                                }

                            ]
                        },
                        options: {
                            responsive: false,
                            legend: {
                                display: false
                            },
                            tooltips: {
                                callbacks: {
                                    label: function(tooltipItem, chart) {
                                        var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label;
                                        return datasetLabel + ': S/ ' + milmillones(tooltipItem.yLabel);
                                    }
                                }
                            },
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        callback: function(value) {
                                            return milmillones(value);
                                        }
                                    }
                                }]
                            }
                        }
                    });

                }
            });
    }



    function GraficosBottom() {
        var id = $("#id_usuario").val();
        var tipoProducto = $("#filtroProductoBtn").val();
        $.ajax({
            type: "GET",
            data: {
                "id": id,
                "tipoProducto": tipoProducto
            },
            async: true,
            url: "{{route('GraficosBottom')}}",
            success: function(result) {
                var arr = JSON.parse(result);
                //console.log(arr);
                if (window.PieChartBtm) window.PieChartBtm.destroy();
                const PIE = arr['PIE'][0];
                if (PIE !== undefined) {
                    window.PieChartBtm = new Chart(ctxPieBtm, {
                        data: {
                            labels: ["IBK", "BCP", "BBVA", "SCOTIA", "BIF"],
                            datasets: [{
                                data: [funre2(PIE['SOW_IBK']), funre2(PIE['SOW_BCP']), funre2(PIE['SOW_BBV']), funre2(PIE['SOW_SCT']), funre2(PIE['SOW_BIF'])],
                                backgroundColor: [
                                    "rgba(0, 176, 80, 0.85)",
                                    "rgba(228, 108, 10, 0.85)",
                                    "rgba(0, 112, 192, 0.85)",
                                    "rgba(255, 0, 0, 0.85)",
                                    "rgba(0, 176, 240, 0.85)"
                                ],
                                borderColor: "transparent",
                            }]
                        },
                        type: 'pie',
                        options: {
                            startAngle: -Math.PI / 4,
                            legend: {
                                display: false
                            },

                        }
                    });
                }

                if (window.LineChartBtm) window.LineChartBtm.destroy();
                const LINE = arr['LINE'];
                var fecha = [];
                var saldoIBK = [];
                var saldoBCP = [];
                var saldoBBVA = [];
                var ultvar = 0;
                var saldoSCT = [];
                var saldoBIF = [];
            
                var fecha2 = ["Dic", "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Set", "Oct", "Nov", "Dic"];
                // console.log(LINE.length);
                for (let index = 0; index < LINE.length; index++) {
                    fecha.push(fecha2[ parseInt(LINE[index]['CODMES'].substring(4, 6))] + "-" + LINE[index]['CODMES'].substring(2, 4));
                    // alert(LINE[index]['CODMES'].substring(4, 6) + "-" + LINE[index]['CODMES'].substring(2, 4));
                    saldoIBK.push(LINE[index]['BANCO_IBK']);
                    saldoBCP.push(LINE[index]['BANCO_BCP']);
                    saldoBBVA.push(LINE[index]['BANCO_BBVA']);
                    saldoSCT.push(LINE[index]['BANCO_SCOTIA']);
                    saldoBIF.push(LINE[index]['BANCO_BIF']);
                }
                var str = '';
                ultvar = (saldoIBK[saldoIBK.length - 1] - saldoIBK[saldoIBK.length - 2]);
                str = PositivoNegativo(ultvar, 'IBK');
                $('#IBKULT').html(milmillones(saldoIBK[saldoIBK.length - 1]));
                $("#IBK").html(ultvar > 0 ? '+' + milmillones(ultvar) : '-' + milmillones(Math.abs(ultvar)));
                $("#IBKPor").html("(" + PositivoNegativo(ultvar / (saldoIBK[saldoIBK.length - 2]), "IBKPor") + ")");

                ultvar = (saldoBCP[saldoBCP.length - 1] - saldoBCP[saldoBCP.length - 2]);
                str = PositivoNegativo(ultvar, 'BCP');
                $('#BCPULT').html(milmillones(saldoBCP[saldoBCP.length - 1]));
                $("#BCP").html(ultvar > 0 ? '+' + milmillones(ultvar) : '-' + milmillones(Math.abs(ultvar)));
                $("#BCPPor").html("(" + PositivoNegativo(ultvar / (saldoBCP[saldoBCP.length - 2]), "BCPPor") + ")");

                ultvar = (saldoBBVA[saldoBBVA.length - 1] - saldoBBVA[saldoBBVA.length - 2]);
                str = PositivoNegativo(ultvar, 'BBVA');
                $('#BBVAULT').html(milmillones(saldoBBVA[saldoBBVA.length - 1]));
                $("#BBVA").html(ultvar > 0 ? '+' + milmillones(ultvar) : '-' + milmillones(Math.abs(ultvar)));
                $("#BBVAPor").html("(" + PositivoNegativo(ultvar / (saldoBBVA[saldoBBVA.length - 2]), "BBVAPor") + ")");

                ultvar = (saldoSCT[saldoSCT.length - 1] - saldoSCT[saldoSCT.length - 2]);
                str = PositivoNegativo(ultvar, 'SCOTIA');
                $('#SCOTIAULT').html(milmillones(saldoSCT[saldoSCT.length - 1]));
                $("#SCOTIA").html(ultvar > 0 ? '+' + milmillones(ultvar) : '-' + milmillones(Math.abs(ultvar)));
                $("#SCOTIAPor").html("(" + PositivoNegativo(ultvar / (saldoSCT[saldoSCT.length - 2]), "SCOTIAPor") + ")");

                ultvar = saldoBIF[saldoBIF.length - 1] - saldoBIF[saldoBIF.length - 2];
                str = PositivoNegativo(ultvar, 'BIF');
                $('#BIFULT').html(milmillones(saldoBIF[saldoBIF.length - 1]));
                $("#BIF").html(ultvar > 0 ? '+' + milmillones(ultvar) : '-' + milmillones(Math.abs(ultvar)));
                $("#BIFPor").html("(" + PositivoNegativo(ultvar / saldoBIF[saldoBIF.length - 2], "BIFPor") + ")");

                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                ultvar = (saldoIBK[saldoBCP.length - 1] - saldoIBK[0]);
                str = PositivoNegativo(ultvar, 'IBKY2D');
                $("#IBKY2D").html(ultvar > 0 ? '+' + milmillones(ultvar) : '-' + milmillones(Math.abs(ultvar)));
                $("#IBKPorY2D").html("(" + PositivoNegativo(ultvar / (saldoIBK[0]), "IBKPorY2D") + ")");

                ultvar = (saldoBCP[saldoBCP.length - 1] - saldoBCP[0]);
                str = PositivoNegativo(ultvar, 'BCPY2D');
                $("#BCPY2D").html(ultvar > 0 ? '+' + milmillones(ultvar) : '-' + milmillones(Math.abs(ultvar)));
                $("#BCPPorY2D").html("(" + PositivoNegativo(ultvar / (saldoBCP[0]), "BCPPorY2D") + ")");

                ultvar = (saldoBBVA[saldoBBVA.length - 1] - saldoBBVA[0]);
                str = PositivoNegativo(ultvar, 'BBVAY2D');
                $("#BBVAY2D").html(ultvar > 0 ? '+' + milmillones(ultvar) : '-' + milmillones(Math.abs(ultvar)));
                $("#BBVAPorY2D").html("(" + PositivoNegativo(ultvar / (saldoBBVA[0]), "BBVAPorY2D") + ")");

                ultvar = (saldoSCT[saldoSCT.length - 1] - saldoSCT[0]);
                str = PositivoNegativo(ultvar, 'SCOTIAY2D');
                $("#SCOTIAY2D").html(ultvar > 0 ? '+' + milmillones(ultvar) : '-' + milmillones(Math.abs(ultvar)));
                $("#SCOTIAPorY2D").html("(" + PositivoNegativo(ultvar / (saldoSCT[0]), "SCOTIAPorY2D") + ")");

                ultvar = saldoBIF[saldoBIF.length - 1] - saldoBIF[0];
                str = PositivoNegativo(ultvar, 'BIFY2D');
                $("#BIFY2D").html(ultvar > 0 ? '+' + milmillones(ultvar) : '-' + milmillones(Math.abs(ultvar)));
                $("#BIFPorY2D").html("(" + PositivoNegativo(ultvar / saldoBIF[0], "BIFPorY2D") + ")");
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                if (LINE !== undefined) {
                    window.LineChartBtm = new Chart(ctxLineBtm, {
                        type: 'line',
                        data: {
                            labels: fecha,
                            datasets: [{
                                    data: saldoIBK,
                                    label: 'IBK',
                                    fill: false,
                                    borderColor: "#4dc487",
                                    backgroundColor: "#4dc487",
                                    borderWidth: 1
                                },
                                {
                                    data: saldoBCP,
                                    label: 'BCP ',
                                    fill: false,
                                    borderColor: "rgb(228, 108, 10)",
                                    backgroundColor: "rgb(228, 108, 10)",
                                    borderWidth: 1
                                },
                                {
                                    data: saldoBBVA,
                                    label: 'BBVA',
                                    fill: false,
                                    borderColor: "rgb(0, 112, 192)",
                                    backgroundColor: "rgb(0, 112, 192)",
                                    borderWidth: 1
                                },
                                {
                                    data: saldoSCT,
                                    label: 'SCOTIA',
                                    fill: false,
                                    borderColor: "rgb(255, 0, 0)",
                                    backgroundColor: "rgb(255, 0, 0)",
                                    borderWidth: 1
                                },
                                {
                                    data: saldoBIF,
                                    label: 'BIF',
                                    fill: false,
                                    borderColor: "rgb(0, 176, 240)",
                                    backgroundColor: "rgb(0, 176, 240)",
                                    borderWidth: 1
                                }
                            ],

                        },
                        options: {
                            responsive: false,
                            legend: {
                                display: false,
                                labels: {
                                    display: false
                                }
                            },
                            tooltips: {
                                callbacks: {
                                    label: function(tooltipItem, chart) {
                                        var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label;
                                        return datasetLabel + ': S/ ' + milmillones(tooltipItem.yLabel);
                                    }
                                }
                            },
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero: true,
                                        display: false
                                    }
                                }]
                            }
                        }
                    });
                }

            }
        });
    }

    function LineaMercadoTabla() {
        var id = $("#id_usuario").val();
        $.ajax({
            type: "GET",
            data: {
                "id": id,
            },
            async: true,
            url: "{{route('LineaMercadoTabla')}}",
            success: function(result) {
                // console.log(result);
                const arr = JSON.parse(result);
                const Lineas = arr["LINEA"];
                const Mercado = arr["MERCADO"];
                const CL = Lineas["CTD_LINEAS"];
                const U = Lineas["UTILIZACION"];
                const LV = Lineas["CTD_VIGENTES"];
                const LVE = Lineas["CTD_A_VENCER_MES"];
                const CLI = Mercado["CTD_CLIENTES_DIR_SF"];
                const CD = Mercado["COL_DIRECTAS_VIG"];
                const RM = Lineas["RIESGO_MAXIMO"];
                const CI = Mercado["COL_INDIRECTAS_TOT"];
                const GAR = Mercado["GAR_SF"];
                const varCL = funre2(Lineas["VAR_CANT_LINEAS"]);
                const varU = funre2(Lineas["VAR_UTILIZACION"]);
                const varRM = funre2(Lineas['VAR_RIESGO_MAXIMO']);
                const varCLI = Mercado["VAR_CTD_CLIENTES_DIR_SF"];
                const varCD = funre(Mercado["VAR_COL_DIRECTAS_VIG"]);
                const varCI = funre(Mercado["VAR_COL_INDIRECTAS_TOT"]);
                const varGAR = funre(Mercado["VAR_GAR_SF"]);
                const PORV = funre2(CL > 0 ? LV / CL : 0) + '%';
                const arrVar = {
                    "varCL": varCL,
                    "varRM": varRM,
                    "varU": varU
                };
                const arrVar2 = {
                    "varCD": varCD,
                    "varCI": varCI,
                    "varGAR": varGAR
                };
                if (funre2(LV / CL) >= 80) {
                    $('#PORV').css('color', '#4dc487');
                } else if (funre2(LV / CL) >= 60 & funre2(LV / CL) < 80) {
                    $('#PORV').css('color', '#ffc000');
                } else {
                    $('#PORV').css('color', '#d13913');
                }
                $('#PORV').html(PORV);
                $('#CL').html(numberWithCommas(CL));
                $('#RM').html(milmillones(RM));
                $('#LVE').html(numberWithCommas(LVE));
                $('#CLI').html(numberWithCommas(CLI));
                const arrMontos = {
                    "U": U,
                    "CD": CD,
                    "CI": CI,
                    "GAR": GAR
                }
                for (key in arrMontos) {
                    $('#' + key).html(milmillones(arrMontos[key]));
                }
                for (key in arrVar2) {
                    if (Math.abs(arrVar2[key]) > 1000) {
                        var number = Math.round(arrVar2[key] / 1000);
                        if (arrVar2[key] > 0) {
                            $('#' + key).css('color', '#4dc487');
                            $('#' + key).html(' + ' + numberWithCommas(number) + ' MM');
                        } else if (arrVar2[key] < 0) {
                            $('#' + key).css('color', '#d13913');
                            $('#' + key).html(' - ' + numberWithCommas(Math.abs(number)) + ' MM');
                        } else {
                            $('#' + key).css('color', 'lightgray');
                            $('#' + key).html('-');
                        }
                    } else {
                        var number = Math.round(arrVar2[key]);
                        if (arrVar2[key] > 0) {
                            $('#' + key).css('color', '#4dc487');
                            $('#' + key).html(' + ' + numberWithCommas(number) + ' M');
                        } else if (arrVar2[key] < 0) {
                            $('#' + key).css('color', '#d13913');
                            $('#' + key).html(' - ' + numberWithCommas(Math.abs(number)) + ' M');
                        } else {
                            $('#' + key).css('color', 'lightgray');
                            $('#' + key).html('-');
                        }
                    }
                }

                for (key in arrVar) {
                    if (arrVar[key] > 0) {
                        $('#' + key).css('color', '#4dc487');
                        $('#' + key).html(' + ' + numberWithCommas(arrVar[key]) + ' %');
                    } else if (arrVar[key] < 0) {
                        $('#' + key).css('color', '#d13913');
                        $('#' + key).html(' - ' + numberWithCommas(Math.abs(arrVar[key])) + '%');
                    } else {
                        $('#' + key).css('color', 'lightgray');
                        $('#' + key).html('-');
                    }
                }
                if (varCLI > 0) {
                    $('#varCLI').css('color', '#4dc487');
                    $('#varCLI').html(' + ' + numberWithCommas(varCLI));
                } else if (varCLI < 0) {
                    $('#varCLI').css('color', '#d13913');
                    $('#varCLI').html(' - ' + numberWithCommas(Math.abs(varCLI)));
                } else {
                    $('#varCLI').css('color', 'lightgray');
                    $('#varCLI').html('-');
                }
            }
        });
    }

    function isOdd(num) {
        return num % 2;
    }

    function getTopBtmMERCADO() {
        var id = $("#id_usuario").val();
        var tipoFrecuencia = $("#filtroFrecuenciaMERCADO").val();
        var tipoProducto = $("#filtroProductoBtn").val();
        var tipoBanco = $("#filtroBANCOMERCADO").val();
        var topBtm = $("#topBtmMERCADO").hasClass("fa-arrow-down");
        $.ajax({
            type: "GET",
            data: {
                "id": id,
                "tipoFrecuencia": tipoFrecuencia,
                "tipoProducto": tipoProducto,
                "tipoBanco": tipoBanco,
                "topBtm": topBtm
            },
            async: true,
            url: "{{route('getTopBottomMERCADO')}}",
            success: function(result) {
                var arr = JSON.parse(result);
                var index = 0;
                $("#AvatarListMERCADO").empty()
                const x = document.getElementById("AvatarListMERCADO");
                arr.slice(0, 14).forEach(element => {
                    if (isOdd(index)) {
                        var str = `<div id="` + element['CODUNICOCLI'] + `Wrap" class="row avatarWrapper avatar-container" >
                                <div class="col-md-4 col-sm-4 col-xs-4 avatar">
                                    <i class="fas fa-user avatar-icon"></i>
                                </div>
                                <div class="col-md-7 col-sm-7 col-xs-7" style="padding-left: 20px;">
                                    <div style="font-weight: bold;">` + element['NOMBRE_COMPLETO'] + `</div>
                                    <div style="margin-left: 0px;" class="row">
                                        <label style="margin-right:10px;">CU:</label>
                                        <span>` + element['CODUNICOCLI'] + `</span>
                                    </div>
                                    <div style="margin-left: 0px;" class="row">
                                        <label >Var:</label><span id="` + element['CODUNICOCLI'] + `"> ` + (element['VAR'] > 0 ? '+' + milmillones(Math.abs(element['VAR'])) : '-' + milmillones(Math.abs(element['VAR']))) + `</span>
                                        <label>&nbsp Saldo Actual:</label><span> ` + milmillones(element['MTOSALDO']) + `</span>
                                    </div>
                                </div>
                                <div style="font-size: 1.6em;width: 31%;display: flex;justify-content: flex-end;padding: 30px;">
                                
                                </div>
                            </div>`;
                    } else {
                        var str = `<div id="` + element['CODUNICOCLI'] + `Wrap" class="row avatarWrapper avatar-container2" >
                                <div class="col-md-4 col-sm-4 col-xs-4 avatar">
                                    <i class="fas fa-user avatar-icon"></i>
                                </div>
                                <div class="col-md-7 col-sm-7 col-xs-7" style="padding-left: 20px;">
                                    <div style="font-weight: bold;">` + element['NOMBRE_COMPLETO'] + `</div>
                                    <div style="margin-left: 0px;" class="row">
                                        <label style="margin-right:10px;">CU:</label>
                                        <span>` + element['CODUNICOCLI'] + `</span>
                                    </div>
                                    <div style="margin-left: 0px;" class="row">
                                        <label >Var:</label><span id="` + element['CODUNICOCLI'] + `"> ` + (element['VAR'] > 0 ? '+' + milmillones(Math.abs(element['VAR'])) : '-' + milmillones(Math.abs(element['VAR']))) + `</span>
                                        <label>&nbsp Saldo Actual:</label><span> ` + milmillones(element['MTOSALDO']) + `</span>
                                    </div>
                                </div>
                                <div style="font-size: 1.6em;width: 31%;display: flex;justify-content: flex-end;padding: 30px;">
                                </div>
                            </div>`;
                    }
                    x.insertAdjacentHTML('beforeend', str);
                    funre(element['VAR']) < 0 ? $('#' + element['CODUNICOCLI']).css('color', '#d13913') : $('#' + element['CODUNICOCLI']).css('color', '#4dc487');
                    index = index + 1;
                });
                $('#maskWaiting').removeClass('maskAppear');
            }
        });

    }

    function getTopBtm() {
        var id = $("#id_usuario").val();
        var tipoFrecuencia = $("#filtroFrecuencia").val();
        var tipoProducto = $("#filtroProducto").val();
        var topBtm = $("#topBtm").hasClass("fa-arrow-down");
        $.ajax({
            type: "GET",
            data: {
                "id": id,
                "tipoFrecuencia": tipoFrecuencia,
                "tipoProducto": tipoProducto,
                "topBtm": topBtm
            },
            async: true,
            url: "{{route('getTopBtm')}}",
            success: function(result) {
                var arr = JSON.parse(result);
                var index = 0;
                $("#AvatarList").empty()
                const x = document.getElementById("AvatarList");
                arr.slice(0, 14).forEach(element => {
                    if (isOdd(index)) {
                        var str = `<div class="row avatar-container" >
                                <div class="col-md-4 col-sm-4 col-xs-4 avatar">
                                    <i class="fas fa-user avatar-icon"></i>
                                </div>
                                <div class="col-md-7 col-sm-7 col-xs-7" style="padding-left: 20px;">
                                    <div style="font-weight: bold;">` + element['NOMBRE_CLIENTE'] + `</div>
                                    <div style="margin-left: 0px;" class="row">
                                        <label style="margin-right:10px;">CU:</label>
                                        <span>` + element['COD_UNICO'] + `</span>
                                    </div>
                                    <div style="margin-left: 0px;" class="row">
                                        <label >Var:</label><span id="` + element['COD_UNICO'] + `"> ` + (element['VAR'] > 0 ? '+' + milmillones(Math.abs(element['VAR'])) : '-' + milmillones(Math.abs(element['VAR']))) + `</span>
                                        <label>&nbsp Saldo Actual:</label><span> ` + milmillones(element['SALDO_PUNTUAL']) + `</span>
                                    </div>
                                </div>
                                <div style="font-size: 1.6em;width: 31%;display: flex;justify-content: flex-end;padding: 30px;">
                                
                                </div>
                            </div>`;
                    } else {
                        var str = `<div class="row avatar-container2" >
                                <div class="col-md-4 col-sm-4 col-xs-4 avatar">
                                    <i class="fas fa-user avatar-icon"></i>
                                </div>
                                <div class="col-md-7 col-sm-7 col-xs-7" style="padding-left: 20px;">
                                    <div style="font-weight: bold;">` + element['NOMBRE_CLIENTE'] + `</div>
                                    <div style="margin-left: 0px;" class="row">
                                        <label style="margin-right:10px;">CU:</label>
                                        <span>` + element['COD_UNICO'] + `</span>
                                    </div>
                                    <div style="margin-left: 0px;" class="row">
                                        <label >Var:</label><span id="` + element['COD_UNICO'] + `"> ` + (element['VAR'] > 0 ? '+' + milmillones(Math.abs(element['VAR'])) : '-' + milmillones(Math.abs(element['VAR']))) + `</span>
                                        <label>&nbsp Saldo Actual:</label><span> ` + milmillones(element['SALDO_PUNTUAL']) + `</span>
                                    </div>
                                </div>
                                <div style="font-size: 1.6em;width: 31%;display: flex;justify-content: flex-end;padding: 30px;">
                                </div>
                            </div>`;
                    }
                    x.insertAdjacentHTML('beforeend', str);
                    funre(element['VAR']) < 0 ? $('#' + element['COD_UNICO']).css('color', '#d13913') : $('#' + element['COD_UNICO']).css('color', '#4dc487');
                    index = index + 1;
                });
            }
        });

    }

    function openWindow(url) {

        window.open(url, '_parent ', "", "modal=no");
        window.focus();

    }

    function coldirectas() {
        var id = $("#id_usuario").val();
        $.ajax({
            type: "GET",
            data: {
                "id": id
            },
            async: false,
            url: "{{route('coldirectas')}}",
            success: function(result) {
                var arrTot = JSON.parse(result);
                var fechasAct = arrTot["FECHAS"][0];
                var fechasActL = arrTot["FECHASLINEAS"][0];
                var fechasActM = arrTot["FECHASMERCADO"][0];
                $("#actualizacionActividades").html(fechasAct["FECHA_ACTUALIZACION"]);
                $("#actualizacionLineas").html(fechasActL["FECHA_ACTUALIZACION"]);
                $("#actualizacionMercado").html(fechasActM["FECHA_ACTUALIZACION"]);
                var arr = arrTot["INDIRECTAS"][0];
                colIndirectas(arr);
                var arr = arrTot["DEPOSITOS"][0];
                depositos(arr);
                var arr = arrTot["DIRECTAS"][0];
                if (arr !== undefined) {
                    var varmCD = arr['VAR_MILES'];
                    var varCD = arr['VAR'];
                    var varmCD_P = arr['VAR_P'];
                    var cumplCD = arr['CUMPLIMIENTO'];
                    var avanceCD = arr['AVANCE'];
                    var pptoCD = arr['META_MILES'];
                    var pptoCDANUAL = arr['META_ANUAL'];
                    var coldfinalCD = arr['SALPUNT_SOL'];
                    var coldfinalCDPROM = arr['SALPROM_SOL'];
                    var vector = [arr['M1'], arr['M2'], arr['M3'], arr['M4'],
                        arr['M5'], arr['M6'], arr['M7'], arr['M8'],
                        arr['M9'], arr['M10'], arr['M11'], arr['M12']
                    ];
                    var mesesCol = [];
                    var mesesColF = [];
                    var mesesColF2 = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Setiembre", "Octubre", "Noviembre", "Diciembre"];
                    for (let index = 0; index < vector.length; index++) {
                        if (vector[index] != null) {
                            mesesCol.push(vector[index]);
                            mesesColF.push(mesesColF2[index]);
                        }
                    }
                    if (window.chartCD) window.chartCD.destroy();
                    window.chartCD = new Chart(ctxCD, {
                        type: 'line',
                        data: {
                            labels: mesesColF,
                            datasets: [{
                                data: mesesCol,
                                label: 'Col. Directas'
                            }]
                        },
                        options: {
                            responsive: true,
                            legend: {
                                display: false
                            },
                            elements: {
                                line: {
                                    borderColor: '#4dc487',
                                    borderWidth: 2
                                },
                                point: {
                                    radius: 4
                                }
                            },
                            tooltips: {
                                callbacks: {
                                    label: function(tooltipItem, chart) {
                                        var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label;
                                        return datasetLabel + ': S/ ' + milmillones(tooltipItem.yLabel);
                                    }
                                }
                            },
                            scales: {
                                yAxes: [{
                                    display: false
                                }],
                                xAxes: [{
                                    display: false
                                }]
                            }
                        }
                    });
                    $('#porcVarDir').html(PositivoNegativo(varmCD_P, 'porcVarDir'))
                    $("#varcoldirectas").html(milmillones(varCD));
                    $("#pptocoldirectas").html(milmillones(pptoCD));
                    $("#pptocoldirectasANUAL").html(milmillones(pptoCDANUAL));
                    $("#cumplimientocoldirectas").html(funre2(cumplCD) + ' % ');
                    $("#avancecoldirectas").html(funre2(avanceCD) + ' % ');
                    $("#coldfinal").html(milmillones(coldfinalCD));
                    $("#coldfinalPROM").html(milmillones(coldfinalCDPROM));
                    if (cumplCD <= 0.8) {
                        $("#cumplimientocoldirectasSEM").css({
                            'background-color': '#d13913'
                        });
                    } else if (cumplCD >= 1) {
                        $("#cumplimientocoldirectasSEM").css({
                            'background-color': '#4dc487'
                        });
                    } else {
                        $("#cumplimientocoldirectasSEM").css({
                            'background-color': 'rgb(255,192,0)'
                        });
                    }
                    if (avanceCD <= 0.8) {
                        $("#avancecoldirectasSEM").css({
                            'background-color': '#d13913'
                        });
                    } else if (avanceCD >= 1) {
                        $("#avancecoldirectasSEM").css({
                            'background-color': '#4dc487'
                        });
                    } else {
                        $("#avancecoldirectasSEM").css({
                            'background-color': 'rgb(255,192,0)'
                        });
                    }
                } else {
                    if (window.chartCD) window.chartCD.destroy();
                    $("#varcoldirectas").html('');
                    $("#pptocoldirectas").html('');
                    $("#cumplimientocoldirectas").html('');
                    $("#coldfinal").html('');
                    $("#DirectasCumpl").css({
                        'background-color': '#d13913',
                        'width': '0%'
                    });
                    $('#porcVarDir').html('');
                }
            }
        });
    }

    async function colIndirectas(arr) {
        if (arr !== undefined) {
            var varmCID = arr['VAR_MILES'];
            var varCID = arr['VAR'];
            var varmCID_P = arr['VAR_P'];
            var cumplCID = arr['CUMPLIMIENTO'];
            var avanceCID = arr['AVANCE'];
            var pptoCID = arr['META_MILES'];
            var pptoCIDANUAL = arr['META_ANUAL'];
            var coldfinalCID = arr['SALPUNT_SOL'];
            var coldfinalCIDPROM = arr['SALPROM_SOL'];
            var mesesColF = [];
            var vector = [arr['M1'], arr['M2'], arr['M3'], arr['M4'],
                arr['M5'], arr['M6'], arr['M7'], arr['M8'],
                arr['M9'], arr['M10'], arr['M11'], arr['M12']
            ];
            var mesesCol = [];
            var mesesColF2 = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Setiembre", "Octubre", "Noviembre", "Diciembre"];
            for (let index = 0; index < vector.length; index++) {
                if (vector[index] != null) {
                    mesesCol.push(vector[index]);
                    mesesColF.push(mesesColF2[index]);
                }
            }
            if (window.chartCI) chartCI.destroy();
            window.chartCI = new Chart(ctxCI, {
                type: 'line',
                data: {
                    labels: mesesColF,
                    datasets: [{
                        data: mesesCol,
                        label: 'Col. Indirectas'
                    }]
                },
                options: {
                    spanGaps: true,
                    responsive: false,
                    legend: {
                        display: false
                    },
                    elements: {
                        line: {
                            borderColor: '#4dc487',
                            borderWidth: 2
                        },
                        point: {
                            radius: 4
                        }
                    },
                    tooltips: {
                        callbacks: {
                            label: function(tooltipItem, chart) {
                                var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label;
                                return datasetLabel + ': S/ ' + milmillones(tooltipItem.yLabel);
                            }
                        }
                    },
                    scales: {
                        yAxes: [{
                            display: false
                        }],
                        xAxes: [{
                            display: false
                        }]
                    }
                }
            });
            $('#porcVarInd').html(PositivoNegativo(varmCID_P, 'porcVarInd'))
            $("#varcolindirectas").html(milmillones(varCID));
            $("#pptocolindirectas").html(milmillones(pptoCID));
            $("#cumplimientocolindirectas").html(funre2(cumplCID) + ' %');
            $("#pptocolindirectasANUAL").html(milmillones(pptoCIDANUAL));
            $("#avancecolindirectas").html(funre2(avanceCID) + ' % ');
            $("#colindfinal").html(milmillones(coldfinalCID));
            $("#colindfinalPROM").html(milmillones(coldfinalCIDPROM));
            if (cumplCID <= 0.8) {
                $("#cumplimientocolindirectasSEM").css({
                    'background-color': '#d13913'
                });
            } else if (cumplCID >= 1) {
                $("#cumplimientocolindirectasSEM").css({
                    'background-color': '#4dc487'
                });
            } else {
                $("#cumplimientocolindirectasSEM").css({
                    'background-color': 'rgb(255,192,0)'
                });
            }
            if (avanceCID <= 0.8) {
                $("#avancecolindirectasSEM").css({
                    'background-color': '#d13913'
                });
            } else if (avanceCID >= 1) {
                $("#avancecolindirectasSEM").css({
                    'background-color': '#4dc487'
                });
            } else {
                $("#avancecolindirectasSEM").css({
                    'background-color': 'rgb(255,192,0)'
                });
            }
        } else {
            if (window.chartCI) window.chartCI.destroy();
            $("#varcolindirectas").html('');
            $("#pptocolindirectas").html('');
            $("#cumplimientocolindirectas").html('');
            $("#colindfinal").html('');
            $('#porcVarInd').html('');
            $("#IndirectasCumpl").css({
                'background-color': '#d13913',
                'width': '0%'
            });
        }
    }

    async function depositos(arr) {
        if (arr !== undefined) {
            var varmD = arr['VAR_MILES'];
            var varD = arr['VAR'];
            var varmD_P = arr['VAR_P'];
            var cumplD = arr['CUMPLIMIENTO'];
            var pptoD = arr['META_MILES'];
            var dfinalCID = arr['SALPUNT_SOL'];
            var dfinalCIDPROM = arr['SALPROM_SOL'];
            var avanceD = arr['AVANCE'];
            var pptoDANUAL = arr['META_ANUAL'];

            var vector = [arr['M1'], arr['M2'], arr['M3'], arr['M4'],
                arr['M5'], arr['M6'], arr['M7'], arr['M8'],
                arr['M9'], arr['M10'], arr['M11'], arr['M12']
            ];
            var mesesCol = [];
            var mesesColF = [];
            var mesesColF2 = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Setiembre", "Octubre", "Noviembre", "Diciembre"];
            for (let index = 0; index < vector.length; index++) {
                if (vector[index] != null) {
                    mesesCol.push(vector[index]);
                    mesesColF.push(mesesColF2[index]);
                }
            }
            if (window.chartD) chartD.destroy();
            window.chartD = new Chart(ctxD, {
                type: 'line',
                data: {
                    labels: mesesColF,
                    datasets: [{
                        data: mesesCol,
                        label: 'Depósito'
                    }]
                },
                options: {
                    responsive: false,
                    legend: {
                        display: false
                    },
                    elements: {
                        line: {
                            borderColor: '#4dc487',
                            borderWidth: 2
                        },
                        point: {
                            radius: 4
                        }
                    },
                    tooltips: {
                        callbacks: {
                            label: function(tooltipItem, chart) {
                                var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label;
                                return datasetLabel + ': S/ ' + milmillones(tooltipItem.yLabel);
                            }
                        }
                    },
                    scales: {
                        yAxes: [{
                            display: false
                        }],
                        xAxes: [{
                            display: false
                        }]
                    }
                }
            });

            $('#porcVarDep').html(PositivoNegativo(varmD_P, 'porcVarDir'))
            $("#vardeposito").html(milmillones(varD));
            $("#pptodeposito").html(milmillones(pptoD));
            $("#cumplimientodep").html(funre2(cumplD) + ' % ');
            $("#depfinal").html(milmillones(dfinalCID));
            $("#depfinalPROM").html(milmillones(dfinalCIDPROM));
            $("#pptodepositoANUAL").html(milmillones(pptoDANUAL));
            $("#avancedep").html(funre2(avanceD) + ' % ');
            if (cumplD <= 0.8) {
                $("#cumplimientodepSEM").css({
                    'background-color': '#d13913'
                });
            } else if (cumplD >= 1) {
                $("#cumplimientodepSEM").css({
                    'background-color': '#4dc487'
                });
            } else {
                $("#cumplimientodepSEM").css({
                    'background-color': 'rgb(255,192,0)'
                });
            }
            if (avanceD <= 0.8) {
                $("#avancedepSEM").css({
                    'background-color': '#d13913'
                });
            } else if (avanceD >= 1) {
                $("#avancedepSEM").css({
                    'background-color': '#4dc487'
                });
            } else {
                $("#avancedepSEM").css({
                    'background-color': 'rgb(255,192,0)'
                });
            }
        } else {
            if (window.chartD) window.chartD.destroy();
            $("#vardeposito").html('');
            $("#pptodeposito").html('');
            $("#cumplimientodeposito").html('');
            $("#depfinal").html('');
            $("#DepositosCumpl").css({
                'background-color': '#d13913',
                'width': '0%'
            });
            $('#porcVarDep').html('');
        }
    }

    function funre(numero) { //Miles redondeado entero
        var flotante = parseFloat(numero);
        var resultado = Math.round(flotante / 1000);
        return resultado;
    }

    function funre2(numero) { //Porcentajes con un decimal
        var flotante = parseFloat(numero);
        var resultado = Math.round(flotante * 1000, 0) / 10;
        return resultado;
    }

    function funre3(numero) { //
        var flotante = parseFloat(numero);
        var resultado = (Math.round(flotante / 100, 0) / 10);
        return resultado;
    }

    function funre4(numero) {
        var flotante = parseFloat(numero);
        var resultado = (Math.round(flotante * 10, 0) / 10);
        return resultado;
    }

    function numberWithCommas(x) {
        if (x) {
            if (x != 0) {
                var parts = x.toString().split(".");
                parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                return parts.join(".");
            } else {
                return '0';
            }
        } else {
            return '0';
        }
    }
</script>
@stop