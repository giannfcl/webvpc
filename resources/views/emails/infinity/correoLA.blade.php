@extends('emails.infinity.mailLayout')

@section('logo')
	<!-- <center><img src = "{{ URL::asset('img/vpconnect.png') }}" alt="Logo" title="Logo" style="width:50%" data-auto-embed="attachment"></center> -->
@stop
@section('detalles')
<div class="row" style="text-align: left;">
	<label>Estimados,</label><br>
	<label>Su apoyo con la siguiente gestión de <b>{{isset($datos['MENSAJE']) ? $datos['MENSAJE'] : null}}</b> del Cliente :</label>
</div><br>
<div style="text-align: left;">
	<div class="row">
		<label>CLIENTE: </label>
		<label>{{isset($datos['CLIENTE']) ? $datos['CLIENTE'] : null}}</label>
	</div>
	<div class="row">
		<label>CU: </label>
		<label>{{isset($datos['CU']) ? $datos['CU'] : null}}</label>
	</div>
	<div class="row">
		<label>EJECUTIVO: </label>
		<label>{{isset($datos['EJECUTIVO']) ? $datos['EJECUTIVO'] : null}}</label>
	</div>
	<div class="row">
		<label>JEFE: </label>
		<label>{{isset($datos['JEFE']) ? $datos['JEFE'] : null}}</label>
	</div>
</div><br><br>
<div style="text-align: left;">
	<u>DETALLE</u>
	<div class="row">
		<label>MOTIVO: </label>
		<label>{{isset($datos['MOTIVO']) ? $datos['MOTIVO'] : null}}</label>
	</div>	
	<div class="row">
		<label>LINEAS VIGENTE: </label>
		<label>{{isset($datos['LINEAS']) ? $datos['LINEAS'] : null}}</label>
	</div>
</div><br>
@if(isset($datos['GESTION']))
<div style="text-align: left;">
	<u>GESTION</u>
	<div class="row">
		<label>NOMBRE AUTONOMIA: </label>
		<label>{{isset($datos['GESTION']['REGISTRO_EN']) ? $datos['GESTION']['REGISTRO_EN'] : null}}</label>
	</div>
	<div class="row">
		<label>FECHA GESTION: </label>
		<label>{{isset($datos['GESTION']['FECHA_GESTION']) ? $datos['GESTION']['FECHA_GESTION'] : null}}</label>
	</div>
	<div class="row">
		<label>POLITICA: </label>
		<label>{{isset($datos['GESTION']['POLITICA']) ? $datos['GESTION']['POLITICA'] : null}}</label>
	</div>
	<div class="row">
		<label>DECISION: </label>
		<label>{{isset($datos['GESTION']['DECISION']) ? $datos['GESTION']['DECISION'] : null}}</label>
	</div>
	<div class="row">
		<label>COMENTARIO: </label>
		<label>{{isset($datos['GESTION']['COMENTARIO']) ? $datos['GESTION']['COMENTARIO'] : null}}</label>
	</div>
</div>
@endif
@stop

@section('contacto')
	Enviado por VPConnect <br/>
	Cualquier duda contactar al siguiente correo <br/>
	mciezan@intercorp.com.pe<br/>
	jchau@intercorp.com.pe<br/>
@stop