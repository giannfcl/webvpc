@extends('Layouts.layout')

@section('js-libs')
<link href="{{ URL::asset('css/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ URL::asset('css/datatables.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ URL::asset('css/formValidation.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ URL::asset('css/multiselect.min.css') }}" rel="stylesheet" type="text/css">

<script type="text/javascript" src="{{ URL::asset('js/jszip.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/pdfmake.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/vfs_fonts.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/buttons.flash.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/buttons.print.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/buttons.html5.min.js') }}"></script>

<script type="text/javascript" src="{{ URL::asset('js/numeral.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/chart.bundle.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/utils.js') }}"></script>

<script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.es.min.js') }}"></script>

<script type="text/javascript" src="{{ URL::asset('js/formvalidation/formValidation.popular.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/formvalidation/language/es_CL.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/formvalidation/framework/bootstrap.min.js') }}"></script>

<script type="text/javascript" src="{{ URL::asset('js/multiselect.min.js') }}"></script>
@stop

@section('content')
@section('pageTitle', 'Landing')
<style type="text/css">
	/*span{
		font-size: 12px
	}*/
	tr{
		font-size: 12px
	}
</style>
<div class="x_panel">
	
	<h2>Lista</h2>
    <div class="clearfix"></div>
	<div class="row">
		<div class="x_panel">
			<div class="x_content">
				<table class="table table-striped table-bordered jambo_table" id="datatable">
					<thead>
						<tr>
							<th>FECHA</th>
							<th>DNI/RUC</th>
							<th>Cliente</th>
							<th>Celular/Email</th>
							<th>Asunto/Consulta</th>
							<th>Otros Comentarios</th>
							<th>Acuerdos</th>
							<th>Acciones</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>



<div class="modal fade" tabindex="-1" role="dialog" id="Modalgestion">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Gestión</h4>
			</div>
			<div class="modal-body">
				<form id="frmGestionLanding" class="form-horizontal" enctype="multipart/form-data" action="{{route('covid19.gestioncliente')}}">
					<input hidden="hidden" name="formid" id="formid">
					<div class="row">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">RUC:</label>
						<div class="input-group col-md-7 col-sm-9 col-xs-12">
							<input class="form-control" name="formdoc" id="formdoc" type="text" value="" readonly="readonly">
						</div>
					</div>
					<div class="row">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">NOMBRE COMPLETO:</label>
						<div class="input-group col-md-7 col-sm-9 col-xs-12">
							<input class="form-control" id="formnombre" type="text" value="" readonly="readonly">
						</div>
					</div>
					<div class="row">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">COMENTARIOS:</label>
						<div class="input-group col-md-7 col-sm-9 col-xs-12">
							<textarea class="form-control" required name="formotroscomentarios"></textarea>
						</div>
					</div>
					<div class="row">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">ACUERDOS:</label>
						<div class="input-group col-md-7 col-sm-9 col-xs-12">
							<textarea class="form-control" required name="formacuerdos"></textarea>
						</div>
					</div>
					<center><button class="btn btn-success" type="submit">Guardar</button></center>
				</form>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
</div>
@stop

@section('js-scripts')
<script type="text/javascript">
	$(document).ready(function() {
		tablaCLientesLanding();
	});
	function tablaCLientesLanding() {
		if ($.fn.dataTable.isDataTable('#datatable')) {
			$('#datatable').DataTable().destroy();
		}
		//console.log(bancas, zonales, jefaturas, ejecutivos);
		$('#datatable').DataTable({
			processing: true,
			"bAutoWidth": false,
			rowId: 'staffId',
			dom: 'Blfrtip',
			buttons: [
				{
					"extend": 'excel',
					"text": 'EXCEL',
					"className": 'btn btn-success btn-xs'
				}
			],
			serverSide: true,
			language: {
				"url": "{{ URL::asset('js/Json/Spanish.json') }}"
			},
			ajax: {
				"type": "GET",
				"url": "{{ route('covid19.getlista') }}",
				data: function(data) {
					data.banca = 1;
				}
			},
			"aLengthMenu": [
				[30, 60, -1],
				[30, 60, "Todo"]
			],
			"iDisplayLength": 30,
			"order": [
				[1, "desc"]
			],
			columnDefs: [
				{
					targets: 0,
					data: null,
					render: function(data, type, row) {
						return row.FECHA;
					}
				},
				{
					targets: 1,
					data: null,
					render: function(data, type, row) {
						return row.NUM_DOC;
					}
				},
				{
					targets: 2,
					data: null,
					render: function(data, type, row) {
						return row.NOMBRE_COMPLETO;
					}
				},
				{
					targets: 3,
					data: null,
					render: function(data, type, row) {
						$linea1="<span>CELULAR: "+row.CELULAR+"</span>";
						$linea2="<br><span>EMAIL: "+row.EMAIL+"</span>";
						return $linea1+$linea2;
					}
				},
				{
					targets: 4,
					data: null,
					render: function(data, type, row) {
						$linea1="<span>ASUNTO: "+row.ASUNTO+"</span>";
						$linea2="<br><span>CONSULTA: "+row.CONSULTA+"</span>";
						return $linea1+$linea2;
					}
				},
				{
					targets: 5,
					data: null,
					render: function(data, type, row) {
						if (row.OTROS_COMENTARIOS) {
							return row.OTROS_COMENTARIOS;
						}else{
							return "";
						}
					}
				},
				{
					targets: 6,
					data: null,
					render: function(data, type, row) {
						if (row.ACUERDOS) {
							return row.ACUERDOS;
						}else{
							return "";
						}
					}
				},
				{
					targets: 7,
					data: null,
					render: function(data, type, row) {
						if (row.FECHA_GESTION==null) {
							return "<button style='font-size:10px !important' identificador='"+row.ID+"' doc='"+row.NUM_DOC+"' nombre='"+row.NOMBRE_COMPLETO+"' class='btn btn-primary gestionar'>GESTIONAR</button>";
						}else{
							return "";
						}
					}
				}
			],
			columns: [
				{
					data: 'FECHA',
					name: 'FECHA',
					searchable: false
				},
				{
					data: 'NUM_DOC',
					name: 'NUM_DOC'
				},
				{
					data: 'NOMBRE_COMPLETO',
					name: 'NOMBRE_COMPLETO'
				}
			]
		});
	}

	$(document).on("click",".gestionar",function () {
		$("#Modalgestion").modal();
		$("#formdoc").val($(this).attr('doc'));
		$("#formnombre").val($(this).attr('nombre'));
		$("#formid").val($(this).attr('identificador'));
	});	
</script>
@stop