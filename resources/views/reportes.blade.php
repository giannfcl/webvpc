@extends('Layouts.layout')

@section('js-libs')
<script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.es.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/chart.bundle.js') }}"></script>

@stop

@section('content')
<div class="row">
    <div class="col-md-1 col-sm-1 col-xs-1"></div>
    <div class="col-md-3 col-sm-3 col-xs-3" style="position:relative;display: flex;align-items: center;justify-content: center;">
        <div class="menu row">
            @foreach($categorias as $c)
            <p id="{{substr(str_replace(' ', '', $c['CATEGORIA']),2,strlen(str_replace(' ', '', $c['CATEGORIA'])))}}" value="{{str_replace(' ', '', $c['CATEGORIA'])}}" class="menuItem OmnesBlack <?php if (substr(str_replace(' ', '', $c['CATEGORIA']), 2, strlen(str_replace(' ', '', $c['CATEGORIA']))) == 'GestiónCartera') { echo 'selected'; } ?>"><?php echo $c['CATEGORIA'] ?></p>
            @endforeach
        </div>

    </div>
    <div class="col-md-1 col-sm-1 col-xs-1"></div>
    <div class="col-md-6 col-sm-6 col-xs-6">
        @foreach($niveles as $i => $c)
        <div class="row reporteWrapper <?php echo substr(str_replace(' ', '', $c['CATEGORIA']), 2, strlen(str_replace(' ', '', $c['CATEGORIA']))) ?> 
        <?php if (substr(str_replace(' ', '', $c['CATEGORIA']), 2, strlen(str_replace(' ', '', $c['CATEGORIA']))) != 'GestiónCartera') {
            echo 'disappear';
        } ?>" categoria="{{$c['CATEGORIA']}}">
            <div class="row btnReport">
                <div class=" col-md-9 col-sm-9 col-xs-9">
                    <div class="nivel" style="text-align: start;"><?php echo $c['NIVEL']=="APlanificador BE"?"Planificador BE":$c['NIVEL']; ?></div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-3">
                    <div class="row hidebar"><i id="TophideBtnReporte{{$i}}" class="hidebtn fas fa-chevron-down"></i></div>
                </div>
            </div>
            <div class="parentWrapper">
                <div id="TophideBtnReporte{{$i}}Section" class="wrapper" style="margin-top: -{{$factores[$i]['FACTOR']*350}}px">
                    @foreach($reportes as $r)
                    @if($r["NIVEL"]==$c["NIVEL"] & $r["CATEGORIA"]==$c["CATEGORIA"] & $r["REPORTE"]!=null)
                    <div class="row">
                        <div class="row text-justify texto ">
                            <div class="col-md-9 col-sm-9 col-xs-9">
                                <h3 class="OmnesBlack"><?php echo $r['REPORTE'] ?></h3>
                                <p><?php echo $r['DESCRIPCION'] ?> </p>
                                <div class="row" style="padding-top: 20px;">
                                    <span style="font-size:1.3em;">Última actualizacíon:</span>
                                    <span style="font-size:1.3em; font-weight:bold;"><?php echo $r['FECHA ACTUALIZACION'] ?></span>
                                </div>
                                <div class="row">
                                    <span style="font-size:1.3em;">Responsable:</span>
                                    <span style="font-size:1.3em; font-weight:bold;"> <?php echo $r['RESPONSABLE'] ?></span>
                                </div>

                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-3">
                                @if($r['REPORTE']=='Cash - Dashboard')
                                    <a target="_blank" href="https://app.powerbi.com/view?r=eyJrIjoiNjBhNmU4OGEtODM1Ny00MDcwLWJhNGEtZGNkNDNmN2NlMDIzIiwidCI6ImU1YTZlNDRlLWE1NzctNDM0OC1hOGUxLTdhMGYwNDMxNzU4NiIsImMiOjR9"> <label class="butonD">Enlace <i class="fas fa-link icono"></i> </label></a>
                                @elseif($r['REPORTE']=='Reporte CAR')
                                    <a target="_blank" href="http://s275va2:8080/BOE/OpenDocument/opendoc/openDocument.jsp?sIDType=CUID&iDocID=AUSGBR8Lyb5FoHgRc7d5w0c"> <label class="butonD">Enlace <i class="fas fa-link icono"></i> </label></a>
                                @else
                                    @if($EDITAR)
                                    <a onclick="prueba('{{$r['REPORTE'].'_'.$c['NIVEL'].'_'.$c['CATEGORIA'].'_'.$r['RESPONSABLE'].'_'.$r['DESCRIPCION']}}')" id="Editar{{str_replace(' ', '', $r['REPORTE'])}}" class="Editar"> <label style="color:#73879C;background:white;" class="butonD">Actualizar <i class="fas fa-upload icono"></i> </label></a>
                                    @endif
                                    <a id="Descargas{{str_replace(' ', '', $r['REPORTE'])}}" class="btndescargar" href="{{route('download', ['file' => str_replace('/','|','RGC/'.$r['NOMREPOR'])])}}" download="{{$r['REPORTE']}}"> <label class="butonD">Descargar <i class="fas fa-download icono"></i> </label></a>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div style="font-size:1.5em; font-weight:bold;padding:30px; text-align: center; width: 100%; border-bottom-style:dashed; border-bottom-width:2px;"></div>
                    @endif
                    @endforeach
                </div>
            </div>
        </div>
        @endforeach
    </div>
    <div class="col-md-1 col-sm-1 col-xs-1"></div>
</div>
<div id="ModalShadow" class="disappear">
    <div style="background-color:white; width:90%; max-width: 700px; min-width: 500px; padding:40px" class="row" id="ModalShowContainer">
        <div class="upload-btn-wrapper">
            <button style="margin: 0 !important" class="btn">Subir archivo</button>
            <div id="fileName" style="float:right;padding-left:15px"></div>
            <input id="InformeExcel" type="file" name="myfile" accept=".xlsx, .xlsm, .xlsb, .pdf" />
        </div>
        <form>
            <div class="title" id="filtro1"></div>
            <div class="title" id="filtro2"></div>
            <div class="form-group">
                <label for="exampleInputNOMINF">Nombre del Informe</label>
                <input type="NOMINF" class="form-control" id="exampleInputNOMINF" placeholder="Nombre del informe">
            </div>
            <div class="form-group">
                <label for="exampleInputDESCRIPCION">Descripción</label>
                <input type="DESCRIPCION" class="form-control" id="exampleInputDESCRIPCION" placeholder="Descripcion">
            </div>
            <label for="exampleInputNOMINF">Fecha de actualizacion</label>
            <div class="container">
                <div class="row">
                    <div class='col-sm-6'>
                        <div class="form-group">
                            <div class='input-group date' id='datetimepicker1'>
                                <input id="datetimepicker" type='text' class="form-control" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <script type="text/javascript">

                    </script>
                </div>
            </div>
            <div class="form-group form-check">
                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                <label class="form-check-label" for="exampleCheck1">Usar la fecha de hoy</label>
            </div>
            <div id="tablaReportes" class="btn btn-primary">Confirmar</div>
        </form>
    </div>
</div>

<div class="modal fade" id="cargando" tabindex="-1" role="dialog">
    <div class="modal-dialog loader" role="document">
        <button type="button" class="close cerrarcargando" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" hidden>&times;</span></button>
    </div>
</div>
@stop

<style>
    .loader {
        margin-top:10%;
        border: 16px solid #f3f3f3;
        border-radius: 50%;
        border-top: 16px solid #3498db;
        width: 120px !important;
        height: 120px !important;
        -webkit-animation: spin 2s linear infinite; /* Safari */
        animation: spin 2s linear infinite;
    }

    @-webkit-keyframes spin {
        0% { -webkit-transform: rotate(0deg); }
        100% { -webkit-transform: rotate(360deg); }
    }

    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
    #ModalShadow {
        height: 100%;
        display: flex;
        width: 100%;
        align-items: center;
        justify-content: center;
        position: fixed;
        top: 0;
        left: 0;
        background: rgba(0, 0, 0, 0.8);
        z-index: 9;
    }

    .space {
        height: 50px;
    }

    .menuItem {
        cursor: pointer;
        transition: 0.25s all;
        margin: 0;
        color: gray;
        padding: 30px;
        background: #f2f5f0;

    }

    .menuItem:hover {
        opacity: 0.75;
    }

    .selected {
        background: #47a651;
        cursor: auto;
        color: white;
        opacity: 1 !important;
    }

    .menu {
        position: fixed;
        min-height: 70px;
        width: 400px;
        border-color: #73879C;
        top: 150px;
        border-width: 1px;
        border-radius: 10px;
        font-size: 1.5em;
    }

    .butonD {
        border-style: solid;
        border-width: 1px;
        border-color: #73879C;
        border-radius: 9px;
        margin-top: 20px;
        font-size: 1.15em;
        width: 100%;
        display: flex;
        align-items: center;
        justify-content: center;
        padding: 10px;
        cursor: pointer;
        transition: 0.5s all;
        background-color: #73879C;
        color: white;
    }

    .upload-btn-wrapper {
        position: relative;
        border-style: solid;
        overflow: hidden;
        display: flex;
        justify-content: start;
        align-items: center;
        border-width: 1px;
        border-color: lightgrey;
        margin-bottom: 20px;
    }
    }

    .btn {
        border: 2px solid gray;
        background: whitesmoke;
        color: gray;
        padding: 8px 20px;
        border-radius: 8px;
        font-size: 20px;
        font-weight: bold;
    }

    .upload-btn-wrapper input[type=file] {
        position: absolute;
        left: 0;
        top: 0;
        opacity: 0;
    }

    .butonD:hover {
        background-color: white;
        color: #73879C;

    }

    .title {
        padding-bottom: 20px;
        font-size: 20px;
    }

    .icono {
        padding-left: 20px;
    }

    .texto {
        display: flex;
        justify-content: center;
        padding-top: 30px;
        align-items: center;

    }

    .OmnesBlack {
        font-family: OmnesBold !important;
        font-size: 1.5m;
        width: 100%;
    }

    .imgSamp {
        height: 200px;
        background-size: contain;
        background-repeat: no-repeat;
        background-position: center center;
    }

    .titleReport {
        display: flex;
        font-size: 1.5em;
        text-align: center;
        padding: 20px;
        margin: 20px;
        justify-content: center;
        border-bottom: solid 1px;
    }

    .btnReport {
        padding: 20px;
        font-size: 1.3em;
        text-align: center;
        border-radius: 10px;
        border-style: solid;
        background:white;
        border-width: 1px;
        color: rgb(0, 176, 80);
        border-color: rgb(0, 176, 80);
        margin-top: 20px;
    }

    #content {
        background: white !important;
    }

    body {
        min-width: 1200px !important;
        background: white !important;
    }

    @font-face {
        font-family: 'Omnes';
        src: url('../public/fonts/omnes-light-webfont.woff2') format('woff2'),
            url('../public/fonts/omnes-light-webfont.woff') format('woff');
        font-weight: normal;
        font-style: normal;

    }

    @font-face {
        font-family: 'OmnesRegular';
        src: url('../public/fonts/omnes-regular-webfont.woff2') format('woff2'),
            url('../public/fonts/omnes-regular-webfont.woff') format('woff');
        font-weight: normal;
        font-style: normal;

    }

    @font-face {
        font-family: 'OmnesBold';
        src: url('../public/fonts/omnes-medium-webfont.woff2') format('woff2'),
            url('../public/fonts/omnes-medium-webfont.woff') format('woff');
        font-weight: normal;
        font-style: normal;

    }

    body {
        font-family: Omnes !important;
    }

    .hidebar {
        display: flex;
        justify-content: flex-end;
    }

    .hidebtn {
        font-size: 1.3em;
        cursor: pointer;
    }

    .wrapper {
        transition: 1s all;
        padding: 20px;
    }

    .parentWrapper {
        overflow: hidden;

    }

    .disappear {
        display: none !important;
    }
</style>
@section('js-scripts')
<script>
    $(document).on('click','.btndescargar',function() {
        $.ajax({
            type: 'POST',
            data: {
                "CATEGORIA": $(this).closest('div.reporteWrapper').attr('categoria'),
                "NIVEL": $(this).closest('div.reporteWrapper').find('div.nivel').html(),
                "NOMBRE_REPORTE" : $(this).closest('div.texto').find('div h3').html(),
            },
            async: true,
            url: "{{route('datosdescarga')}}",
            beforeSend: function() {
                $("#cargando").modal();
            },
            success: function(result) {
            },
            complete: function() {
                $(".cerrarcargando").click();
            }
        });
    });

    $(document).on('click', '#tablaReportes', function(e) {
        // console.log(window);
        if (window.file != null) {
            i = window.file.name.indexOf('.', 0);
            tipo = window.file.name.substring(i, window.file.name.length);
            window.fecha = window.condition ? timeConverter(Date.now()) : timeConverter(Date.parse($('#datetimepicker').val()).valueOf());
            window.DESCRIPCION = $('#exampleInputDESCRIPCION').val();

            if (window.INFORME != null &
                window.NIVEL != null &
                window.CATEGORIA != null &
                window.fecha != null
            ) {
                data = new FormData();
                data.append('FILE', window.file);
                data.append('FILENAME', window.file.name);
                data.append('CATEGORIA', window.CATEGORIA);
                data.append('REPORTE',$('#exampleInputNOMINF').val());
                data.append('REPORTEANT', window.INFORME);
                data.append('DESCRIPCION', window.DESCRIPCION);
                data.append('NIVEL', window.NIVEL);
                data.append('TIPO', tipo);
                data.append('FECHA', window.fecha);
                // console.log(data);
                $.ajax({
                    type: 'POST',
                    data: data,
                    contentType: false,
                    cache: false,
                    processData: false,
                    async: false,
                    url: "{{route('actualizar.reportes')}}",
                    beforeSend: function() {
                        $("#cargando").modal();
                    },
                    success: function(result) {
                        // console.log(result);
                        if (result) {
                            window.location.reload();
                        }
                    },
                    timeout: 18000,
                    error: function(request, status, err) {
                        // console.log(request, status, err);
                        if (status == "timeout") {
                            Swal.fire("Su petición demoro mas de lo permitido");
                        } else {
                            Swal.fire("error inesperado comunicate con soporte de vpconnect");
                        }
                    },
                    complete: function() {
                        $(".cerrarcargando").click();
                    }
                });
                window.INFORME = $('#exampleInputNOMINF').val();
            }
        }
    })

    function timeConverter(UNIX_timestamp) {
        var a = new Date(UNIX_timestamp);
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        var year = a.getFullYear();
        var month = months[a.getMonth()];
        var date = a.getDate();
        var hour = a.getHours();
        var min = a.getMinutes();
        var sec = a.getSeconds();
        var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec;
        return time;
    }

    function prueba(cadena) {
        i = cadena.indexOf('_', 0);
        i2 = cadena.indexOf('_', i + 1);
        i3 = cadena.indexOf('_', i2 + 1);
        i4 = cadena.indexOf('_', i3 + 1);
        // console.log('asdad');
        window.INFORME = cadena.substring(0, i);
        window.NIVEL = cadena.substring(i + 1, i2);
        window.CATEGORIA = cadena.substring(i2 + 1, i3);
        window.RESPONSABLE = cadena.substring(i3 + 1, i4);
        window.DESCRIPCION = cadena.substring(i4 + 1, cadena.length);
        $('#exampleInputNOMINF').val(INFORME);
        $('#exampleInputDESCRIPCION').val(DESCRIPCION);
        $("#ModalShadow").removeClass("disappear");
        $("#filtro1").text(CATEGORIA);
        $("#filtro2").text(NIVEL);
    }
    $(function() {
        $('#datetimepicker1').datetimepicker({

        });
    });
    $(document).on('change', '#InformeExcel', function() {
        $('#fileName').html($('input#InformeExcel')[0].files[0].name);
        window.file = $('input#InformeExcel')[0].files[0];
    });
    $('input[type=checkbox]').change(
        function() {
            window.condition = this.checked;
            if (this.checked) {
                $("#datetimepicker").prop('disabled', true);
            } else {
                $("#datetimepicker").prop('disabled', false);
            }
        });
    $(".menuItem").click(function() {
        var idi = this.getAttribute("value").substring(2, this.getAttribute("value").length);
        $(".menuItem").removeClass("selected");
        $("#" + idi).addClass("selected");
        $(".reporteWrapper").addClass("disappear");
        $("." + idi).removeClass("disappear");
    });
    $(".hidebtn").click(function() {
        var idi = this.id;
        show(idi);
    });
    $('#ModalShadow').click(function() {
        $("#ModalShadow").addClass("disappear");
    });

    function show(idi) {
        var height = $("#" + idi + "Section").height() * 1.1;
        if ($("#" + idi).hasClass("fa-chevron-up")) {
            $("#" + idi).removeClass("fa-chevron-up");
            $("#" + idi).addClass("fa-chevron-down");
            $("#" + idi + "Section").css("margin-top", "-" + height + "px");
        } else {
            $("#" + idi).addClass("fa-chevron-up");
            $("#" + idi).removeClass("fa-chevron-down");
            $("#" + idi + "Section").css("margin-top", "0px");
        }
    }
    $(document).on('click', '#ModalShowContainer', function(event) {
        $("#ModalShadow").removeClass("disappear");

    });
    $(document).on('click', '.heatSensorDescargas', function(event) {
        event.stopPropagation();
        event.stopImmediatePropagation();
        $.ajax({
            type: 'POST',
            data: {
                "IDHTML": this.id,
                "TIPO": 'Descargas'
            },
            async: true,
            url: "{{route('heatAnalytic')}}",
            success: function(result) {

            }
        });
    });
    $(document).on('click', '.heatSensorSection', function(event) {
        event.stopPropagation();
        event.stopImmediatePropagation();
        var tipo = $("#" + this.id).hasClass('fa-chevron-up') ? 'Secciones Mostrar Reporte' : 'Secciones Ocultar Reporte';
        $.ajax({
            type: 'POST',
            data: {
                "IDHTML": this.id,
                "TIPO": tipo
            },
            async: true,
            url: "{{route('heatAnalytic')}}",
            success: function(result) {
                
            }
        });
    });
</script>
@stop