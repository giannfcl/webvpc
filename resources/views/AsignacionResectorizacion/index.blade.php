@extends('Layouts.layout')

@section('js-libs')
<link href="{{ URL::asset('css/formValidation.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ URL::asset('css/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css">

<script type="text/javascript" src="{{ URL::asset('js/formvalidation/formValidation.popular.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/formvalidation/language/es_CL.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.es.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/formvalidation/framework/bootstrap.min.js') }}"></script>



@stop
<?php
$nuevo = isset($nuevo) ? $nuevo : '0';
$cant = isset($cant) ? $cant : '0';
$buscando = isset($buscando) ? $buscando : '0';
// dd($nuevo,$buscando);
?>
@section('content')

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_content">
                <form action="{{route('rr.sector')}}" class="form-horizontal">
                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="" class="control-label col-md-4">DNI/RUC:</label>
                            <div class="col-md-8">
                                <input id="txtNumDoc" class="form-control formatInputNumber" type="text" onkeypress="return inputLimiter(event,'custom')" value="{{$data ? $data->NUM_DOC : ''}}" name="documento" minlength="8" maxlength="15">
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="" class="control-label col-md-4">CODIGO UNICO:</label>
                            <div class="col-md-8">
                                <input id="txtCu" class="form-control formatInputNumber" type="text" onkeypress="return inputLimiter(event,'custom')" value="" name="codunico" maxlength="10">
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <button id="buttonBuscar" type="Submit" class="btn btn-primary"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Buscar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @if($buscando)
        @if($nuevo=='1')
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    <form class="form-horizontal" action="{{ route('be.miprospecto.nuevo') }}" enctype="multipart/form-data" method="POST">
                        <div class="row">
                            <div class="form-group col-md-8">
                                <label for="" class="control-label col-md-4">Razon Social:</label>
                                <div class="col-md-4">
                                    <input class="form-control formatInputNumber" name="nombre" type="text" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-8">
                                <label for="" class="control-label col-md-4">Documento:</label>
                                <div class="col-md-4">
                                    <input class="form-control formatInputNumber" name="documento" type="text" minlength="8" maxlength="15" value="{{isset($documento) ? $documento : ''}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-8">
                                <label for="" class="control-label col-md-4">Facturación (soles):</label>
                                <div class="col-md-4">
                                    <input id="txtFact" class="form-control formatInputNumber" name="facturacion" type="text">
                                </div>
                            </div>
                        </div><br>

                        @if($cant>=5)
                        <div class="row">
                            <div class="form-group col-md-2">
                            </div>
                            <div class="form-group col-md-8 colored">
                                <label style="font-size: 1.3em;">Se Requiere V°B:</label>
                                <p>
                                    <p>Excediste la cantidad de referidos permitidos en el mes. Se requiere que adjuntes la conformidad de tu jefatura</p>
                                    <p>
                                        <div class="col-md-4">
                                            <input type="file" name="adjuntoReferido" class="form-control" style="border-style: none; width: 100%; border-radius:30px" required>
                                        </div>
                            </div>
                        </div>
                        @endif
                        <div class="row">
                            <div class="form-group col-md-5">
                            </div>
                            <div class="form-group col-md-6">
                                <button type="submit" class="btn btn-success">Guardar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        @else
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">
                            <form id="frmDatosReasignacion" class="form-horizontal form-label-left" enctype="multipart/form-data" action="{{ route('rr.guardar') }}" method="POST">

                                <div id="divReferidoExistente">
                                    <input class="form-control hidden" readonly="readonly" value="{{$data!=null ? $data->NUM_DOC : ($lead!=null ? $lead->NUM_DOC : null)}}" name="numDocumento">
                                    <input class="form-control hidden" readonly="readonly" value="{{$data!=null ? $data->BANCA_ACTUAL : ''}}" name="banca">
                                    <input class="form-control hidden" readonly="readonly" value="{{$data!=null ? $data->CODSECTORISTA : ''}}" name="codSectorista">
                                    <input class="form-control hidden" readonly="readonly" value=" " id="codcorto" name="codcorto">
                                    <input class="form-control hidden" readonly="readonly" value="{{$data!=null ? $data->COD_CORTO_ACTUAL : ''}}" name="codcortoOrigen">
                                    <input class="form-control hidden" readonly="readonly" value="{{$lead!=null ? $lead->REGISTRO_EN : ''}}" name="registroAsignado">
                                    <input class="form-control hidden" readonly="readonly" value="{{$data!=null ? $data->REGISTRO_SECTORISTA_ACTUAL : ''}}" name="registroSectorista">
                                    <input class="form-control hidden" readonly="readonly" value="{{$data!=null ? $data->COD_UNICO : ''}}" name="codigoUnico">
                                    <input class="form-control hidden" readonly="readonly" value="{{ Auth::user()->REGISTRO}}" name="destino">
                                    <div class="from-group col-md-12">
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputEmail1">Razón Social</label>
                                            <input class="form-control" readonly="readonly" value="{{$data!=null ? $data->NOMBRE_CLIENTE : ''}}" name="nombre">
                                        </div>
                                    </div>
                                    <div class="from-group col-md-12">
                                        <div class="form-group col-md-2">
                                            <label for="exampleInputEmail1">Calificación SBS</label>
                                            <input class="form-control" readonly="readonly" value="{{$data!=null ? $data->CLASIF_SBS_SF : ''}}" name="calificacion">
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="exampleInputEmail1">FEVE</label>
                                            <input class="form-control" readonly="readonly" value="{{$data!=null ? $data->FEVE : ''}}" name="feve">
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="exampleInputEmail1">Deuda Directa</label>
                                            <input class="form-control" readonly="readonly" value=" S/. {{$data!=null ? number_format($data->DEUDA_DIRECTA,0,'.',',')  : ''}}" name="deuda">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <div class="form-group col-md-4">
                                            <label for="exampleInputEmail1">Asignado Actual</label><br>
                                            <label class="control-label bordelabel txtalingleft" style="text-align: left;background:#eee;border-width: 0.5px;border-color: #CCC;border-style: solid;width: 100%;height:33px;cursor:text;font-family: inherit;font-weight: inherit;font-size: 12px;color: #555"><span style="margin-left: 10px">{{$lead!=null && $lead->COD_SECTORISTA!=NULL && $lead->COD_SECTORISTA!='' ? $lead->REGISTRO_EN.' -- '  : ''}}{{$lead!=null ? $lead->EJECUTIVO_NOMBRE : ''}}</span>
                                                @if ($lead && $lead->EJECUTIVO_NOMBRE==null)
                                                <span class="pull-right" style="margin-right: 10px;padding-bottom: 5px;">
                                                    <i class="fas fa-check fa-lg" style="color:#26B99A;"></i>
                                                </span>
                                                @else
                                                <span class="pull-right" style="margin-right: 10px;padding-bottom: 5px;">
                                                    <i class="fas fa"></i>
                                                </span>
                                                @endif
                                            </label>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="exampleInputEmail1">Sectorista Actual</label><br>
                                            <div class="form-group row">
                                                <label class="control-label bordelabel txtalingleft" style="text-align: left;background:#eee;border-width: 0.5px;border-color: #CCC;border-style: solid;width: 100%;height:33px;cursor:text;font-family: inherit;font-weight: inherit;font-size: 12px;color: #555;"><span style="margin-left: 10px;">{{$data!=null && $data->BANCA_ACTUAL!=NULL && $data->BANCA_ACTUAL!='' ? $data->BANCA_ACTUAL.' -- ' : ''}}{{$data!=null && $data->CODSECTORISTA!=NULL && $data->CODSECTORISTA!='' ? $data->CODSECTORISTA.' -- ' : ''}}{{$data!=null ? $data->NOMBRE_SECTORISTA_ACTUAL : ''}}</span>
                                                    @if ($data!=null && $data->NOMBRE_SECTORISTA_ACTUAL==null)
                                                    <span class="pull-right" style="margin-right: 10px;padding-bottom: 5px;">
                                                        <i class="fas fa-check fa-lg" style="color:#26B99A;"></i>
                                                    </span>
                                                    @else
                                                    <span class="pull-right" style="margin-right: 10px;padding-bottom: 5px;">
                                                        <i class="fas fa"></i>
                                                    </span>
                                                    @endif
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <div class="row" style="display: flex;padding-left: 20px;">
                                            <label for="exampleInputEmail1" style="">Asignar y Resectorizar</label>
                                            <div class="form-group col-md-4">
                                                @if($datosejecutivo!=null)
                                                <select class="form-control {{count($datosejecutivo)>1 ? '' : 'hidden'}}" id="_codsectoristas" style="width: 50%;">
                                                    @foreach($datosejecutivo as $sectorista)
                                                    <option value="{{$sectorista->COD_CORTO}}--{{$sectorista->ENCARGADO}}">{{$sectorista->COD_CORTO}}</option>
                                                    @endforeach
                                                </select>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-4 col-sm-4 col-xs-4">
                                            <input id="ejecutivo" class="form-control" readonly="readonly" value="{{ Auth::user()->NOMBRE }}">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <div class="form-group col-md-2">
                                            <label for="exampleInputEmail1">Última Etapa</label>
                                            <input class="form-control" readonly="readonly" value="{{$lead!=null ? $lead->ETAPA_NOMBRE : ''}}" name="etapa">
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="exampleInputEmail1">Motivo de Eliminación</label>
                                            <input class="form-control" readonly="readonly" value="" name="motivoDetalle">
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="exampleInputEmail1">Fecha de Última Etapa</label>
                                            <div class="input-group">
                                                <div class="input-group-addon styleAddOn"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></div>
                                                <input class="form-control" readonly="readonly" value="{{$lead!=null ? $lead->FECHA_ETAPA : ''}}" name="fechaUltimaE">
                                            </div>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="exampleInputEmail1">Fecha de Última Visita</label>
                                            <div class="input-group">
                                                <div class="input-group-addon styleAddOn"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></div>
                                                <input class="form-control" readonly="readonly" value="{{$lead!=null ? $lead->FECHA_ULTIMA_VISITA : ''}}" name="fechaUltimaV">
                                            </div>
                                        </div>
                                    </div>
                                    @if($lead!=null)
                                            @if ($data==null)
                                                @if(Auth::user()->REGISTRO!=$lead->REGISTRO_EN)
                                                    @if ($cant>=5)
                                                        <div class="form-group col-md-5 colored">
                                                            <label for="exampleInputEmail1" style="font-size:1.3em">Se Requiere V°B :</label>
                                                            <p>
                                                                <p for="exampleInputEmail1" style="font-size: 0.9em;">(Excediste la cantidad de referidos permitidos en el periodo. Se requiere que adjuntes la conformidad de tu jefe)</p>
                                                                <div class="input-group col-md-6 col-sm-6 col-xs-6">
                                                                    <input type="file" name="adjuntoDocumento1" class="form-control image" style="border-style: none; width: 100%; border-radius:30px" required>
                                                                </div>
                                                        </div>
                                                        <div class="form-group col-md-5"></div>
                                                    @endif
                                                    <div class="form-group col-md-6 colored">
                                                        <label for="exampleInputEmail8" style="font-size:1.3em">Se Requiere V°B del Asignado actual:</label>
                                                        <p>
                                                            <p for="exampleInputEmail8" style="font-size: 0.9em;">(Importante: Debes adjuntar la conformidad con el V°B del Sectorista correspondiente, en caso se adjunte otro documento se considerará una falta grave)</p>
                                                            <div class="input-group col-md-6 col-sm-6 col-xs-6">
                                                                <input type="file" name="adjuntoDocumento2" class="form-control image" style="border-style: none; width: 100%; border-radius:30px" required>
                                                            </div>
                                                    </div>
                                                @endif
                                            @else
                                                @if(Auth::user()->REGISTRO!=$lead->REGISTRO_EN)
                                                    @if ($cant>=5)
                                                        <div class="form-group col-md-5 colored">
                                                            <label for="exampleInputEmail1" style="font-size:1.3em">Se Requiere V°B :</label>
                                                            <p>
                                                                <p for="exampleInputEmail1" style="font-size: 0.9em;">(Excediste la cantidad de referidos permitidos en el periodo. Se requiere que adjuntes la conformidad de tu jefe)</p>
                                                                <div class="input-group col-md-6 col-sm-6 col-xs-6">
                                                                    <input type="file" name="adjuntoReferido" class="form-control image" style="border-style: none; width: 100%; border-radius:30px" required>
                                                                </div>
                                                        </div>
                                                        <div class="form-group col-md-5"></div>
                                                    @endif
                                                    @if($data->REGISTRO_SECTORISTA_ACTUAL==$lead->REGISTRO_EN && in_array($data->BANCA_ACTUAL,array('BC','BE','BEL','BEP','G0')))
                                                        <div class="form-group col-md-6 colored">
                                                            <label for="exampleInputEmail8" style="font-size:1.3em">Se Requiere V°B :</label>
                                                            <p>
                                                                <p for="exampleInputEmail8" style="font-size: 0.9em;">(Importante: Debes adjuntar la conformidad con el V°B del Asignado/Sectorista correspondiente, en caso se adjunte otro documento se considerará una falta grave)</p>
                                                                <div class="input-group col-md-6 col-sm-6 col-xs-6">
                                                                    <input type="file" name="adjuntoDocumento2" class="form-control image" style="border-style: none; width: 100%; border-radius:30px" required>
                                                                </div>
                                                        </div>
                                                    @else
                                                        <div class="form-group col-md-6 colored">
                                                            <label for="exampleInputEmail8" style="font-size:1.3em">Se Requiere V°B del Asignado actual:</label>
                                                            <p>
                                                                <p for="exampleInputEmail8" style="font-size: 0.9em;">(Importante: Debes adjuntar la conformidad con el V°B del Sectorista correspondiente, en caso se adjunte otro documento se considerará una falta grave)</p>
                                                                <div class="input-group col-md-6 col-sm-6 col-xs-6">
                                                                    <input type="file" name="adjuntoDocumento1" class="form-control image" style="border-style: none; width: 100%; border-radius:30px" required>
                                                                </div>
                                                        </div>
                                                        @if(in_array($data->BANCA_ACTUAL,array('BC','BE','BEL','BEP')))
                                                            <div class="form-group col-md-6 colored">
                                                                <label for="exampleInputEmail8" style="font-size:1.3em">Se Requiere V°B del Sectorista actual:</label>
                                                                <p>
                                                                    <p for="exampleInputEmail8" style="font-size: 0.9em;">(Importante: Debes adjuntar la conformidad con el V°B del Sectorista correspondiente, en caso se adjunte otro documento se considerará una falta grave)</p>
                                                                    <div class="input-group col-md-6 col-sm-6 col-xs-6">
                                                                        <input type="file" name="adjuntoDocumento2" class="form-control image" style="border-style: none; width: 100%; border-radius:30px" required>
                                                                    </div>
                                                            </div>
                                                        @endif
                                                    @endif
                                                @endif
                                            @endif
                                            <center><button class="btn btn-success" type="submit">Enviar</button></center>
                                    @else
                                        @if($data!=null)
                                            @if(Auth::user()->REGISTRO!=$data->REGISTRO_SECTORISTA_ACTUAL)
                                                @if ($cant>=5)
                                                    <div class="form-group col-md-5 colored">
                                                        <label for="exampleInputEmail1" style="font-size:1.3em">Se Requiere V°B :</label>
                                                        <p>
                                                            <p for="exampleInputEmail1" style="font-size: 0.9em;">(Excediste la cantidad de referidos permitidos en el periodo. Se requiere que adjuntes la conformidad de tu jefe)</p>
                                                            <div class="input-group col-md-6 col-sm-6 col-xs-6">
                                                                <input type="file" name="adjuntoDocumento1" class="form-control image" style="border-style: none; width: 100%; border-radius:30px" required>
                                                            </div>
                                                    </div>
                                                    <div class="form-group col-md-5"></div>
                                                @endif
                                                @if(in_array($data->BANCA_ACTUAL,array('BC','BE','BEL','BEP')))
                                                    <div class="form-group col-md-6 colored">
                                                        <label for="exampleInputEmail8" style="font-size:1.3em">Se Requiere V°B del Sectorista actual:</label>
                                                        <p>
                                                            <p for="exampleInputEmail8" style="font-size: 0.9em;">(Importante: Debes adjuntar la conformidad con el V°B del Sectorista correspondiente, en caso se adjunte otro documento se considerará una falta grave)</p>
                                                            <div class="input-group col-md-6 col-sm-6 col-xs-6">
                                                                <input type="file" name="adjuntoDocumento2" class="form-control image" style="border-style: none; width: 100%; border-radius:30px" required>
                                                            </div>
                                                    </div>
                                                @endif
                                            @endif
                                            <center><button class="btn btn-success" type="submit">Enviar</button></center>
                                        @endif
                                    @endif
                            </form>
                        </div>
                    </div>
                </div>
        @endif
    @endif

</div>
<style>
    .colored {
        background: #2A3F54;
        color: white !important;
        font-weight: 300 !important;
        padding: 30px;
        border-radius: 20px;

    }

    .x_panel {
        width: 100%;
        box-shadow: 0px 3px 8px 0px;
        padding: 10px 17px;
        display: inline-block;
        background: #fff;
        border: 1px solid #E6E9ED;
        margin-bottom: 30px;
        -webkit-column-break-inside: avoid;
        -moz-column-break-inside: avoid;
        column-break-inside: avoid;
        opacity: 1;
        transition: all .2s ease;
    }
</style>
<script>
    $(document).ready(function() {
        $("#ejecutivo").val($("#_codsectoristas").val());
        var codcorto = $("#_codsectoristas").val().split("--");
        $("#codcorto").val(codcorto[0]);
    });
    $(document).on('change', '#_codsectoristas', function() {
        $("#ejecutivo").val($(this).val());
        var sectorista_escogido = $("#_codsectoristas").val().split("--");
        $("#codcorto").val(sectorista_escogido[0]);
    });

    function inputLimiter(e, allow) {
        var AllowableCharacters = '';

        if (allow == 'custom') {
            AllowableCharacters = ' 1234567890.';
        }


        var k = document.all ? parseInt(e.keyCode) : parseInt(e.which);
        if (k != 13 && k != 8 && k != 0) {
            if ((e.ctrlKey == false) && (e.altKey == false)) {
                return (AllowableCharacters.indexOf(String.fromCharCode(k)) != -1);
            } else {
                return true;
            }
        } else {
            return true;
        }

    }
</script>

@stop