@extends('Layouts.layout')

@section('js-libs')
<link href="{{ URL::asset('css/formValidation.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ URL::asset('css/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css">

<script type="text/javascript" src="{{ URL::asset('js/formvalidation/formValidation.popular.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/formvalidation/language/es_CL.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.es.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/formvalidation/framework/bootstrap.min.js') }}"></script>



@stop
<?php
$modoLinkedin = in_array(Auth::user()->REGISTRO, App\Entity\Usuario::getUsuariosLinkedin());
$modoComunicacion = in_array(Auth::user()->REGISTRO, App\Entity\Usuario::getUsuariosComunicacion());
$nuevo = isset($nuevo) ? $nuevo : '0';
$cant = isset($cant) ? $cant : '0';
?>
@section('content')

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_content">
                <form action="" class="form-horizontal">
                    <div class="row">
                        <input type="text" hidden="hidden" name="areferido" value="2">
                        <div class="form-group col-md-4">
                            <label for="" class="control-label col-md-4">DNI/RUC:</label>
                            <div class="col-md-8">
                                <input id="txtNumDoc" class="form-control formatInputNumber" type="text" onkeypress="return inputLimiter(event,'custom')" value="<?php echo ($data ? $data->NUM_DOC : '') ?>" name="documento" minlength="8" maxlength="15">
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="" class="control-label col-md-4">CODIGO UNICO:</label>
                            <div class="col-md-8">
                                <input id="txtCu" class="form-control formatInputNumber" type="text" onkeypress="return inputLimiter(event,'custom')" value="<?php echo '' ?>" name="codunico" maxlength="10">
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <button id="buttonBuscar" type="Submit" class="btn btn-primary"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Buscar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @if($nuevo=='1')
    <form class="form-horizontal" action="{{ route('be.miprospecto.nuevo') }}" enctype="multipart/form-data" method="POST">
        <div class="row">
            <div class="form-group col-md-8">
                <label for="" class="control-label col-md-4">Razón Social:</label>
                <div class="col-md-4">
                    <input class="form-control formatInputNumber" name="nombre" type="text" required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-8">
                <label for="" class="control-label col-md-4">Documento:</label>
                <div class="col-md-4">
                    <input class="form-control formatInputNumber" name="documento" type="text" minlength="8" maxlength="15" value="{{isset($documento) ? $documento : ''}}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-8">
                <label for="" class="control-label col-md-4">Facturación (soles):</label>
                <div class="col-md-4">
                    <input id="txtFact" class="form-control formatInputNumber" name="facturacion" type="text">
                </div>
            </div>
        </div><br>
        @if($cant>=5)
        <div class="row">
            <div class="form-group col-md-2">
            </div>
            <div class="form-group col-md-8">
                <label style=" color: red;">Se Requiere V°B:</label>
                <p>
                    <label>Excediste la cantidad de referidos permitidos en el mes. Se requiere que adjuntes la conformidad de tu jefatura</label>
                    <p>
                        <div class="col-md-4">
                            <input type="file" name="adjuntoReferido" class="form-control" style="border-style: none" required>
                        </div>
            </div>
        </div>
        @endif
        <div class="row">
            <div class="form-group col-md-5">
            </div>
            <div class="form-group col-md-6">
                <button type="submit" class="btn btn-success">Guardar</button>
            </div>
        </div>
    </form>
    @else
    @if($data != null && $lead != null)
    <form id="frmDatosReasignacion" class="form-horizontal form-label-left" enctype="multipart/form-data" action="{{ route('rr.guardar-asignacion') }}" method="POST">

        <div id="divReferidoExistente">
            <input class="form-control hidden" readonly="readonly" value="<?php  ?>" name="caso">
            <input class="form-control hidden" readonly="readonly" value="<?php echo ($data ? $data->NUM_DOC : '') ?>" name="numDocumento" id="numDocumento">
            <input class="form-control hidden" readonly="readonly" value="<?php echo ($data ? $data->BANCA_ACTUAL : '') ?>" name="banca">
            <input class="form-control hidden" readonly="readonly" value="<?php echo ($data ? $data->CODSECTORISTA : '') ?>" name="codSectorista">
            <input class="form-control hidden" readonly="readonly" value=" " name="codcorto" id="codcorto">
            <input class="form-control hidden" readonly="readonly" value="<?php echo ($lead ? $lead->REGISTRO_EN : '') ?>" name="registroAsignado">
            <input class="form-control hidden" readonly="readonly" value="<?php echo ($data ? $data->REGISTRO_SECTORISTA_ACTUAL : '') ?>" name="registroSectorista">
            <input class="form-control hidden" readonly="readonly" value="<?php echo ($data ? $data->COD_UNICO : '') ?>" name="codigoUnico">
            <input class="form-control hidden" readonly="readonly" value="{{ Auth::user()->REGISTRO }}" name="destino">
            <input class="form-control hidden" value="<?php echo (isset($caso) ? $caso : '') ?>">
            <div class="row" style="display: flex;flex-direction: column;">
                <div style="
    margin: 23px;
    background: white;
    box-shadow: 0px 10px 8px 0px lightgrey;
    padding-top: 15px;
">
                    <div class="from-group col-md-12">
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Razón Social:</label>
                            <input class="form-control" readonly="readonly" value="<?php echo ($data ? $data->NOMBRE_CLIENTE : '') ?>" name="nombre">
                        </div>
                    </div>
                    <div class="from-group col-md-12">
                        <div class="form-group col-md-2">
                            <label for="exampleInputEmail1">Calificación SBS</label>
                            <input class="form-control" readonly="readonly" value="<?php echo ($data ? $data->CLASIF_SBS_SF : '') ?>" name="calificacion">
                        </div>
                        <div class="form-group col-md-2">
                            <label for="exampleInputEmail1">FEVE</label>
                            <input class="form-control" readonly="readonly" value="<?php echo ($data ? $data->FEVE : '') ?>" name="feve">
                        </div>
                        <div class="form-group col-md-2">
                            <label for="exampleInputEmail1">Deuda Directa</label>
                            <input class="form-control" readonly="readonly" value="<?php echo "S/." . ($nuevo ? number_format($nuevo->DEUDA_DIRECTA, 0, '.', ',')  : '') ?>" name="deuda">
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <div class="form-group col-md-4">
                            <label for="exampleInputEmail1">Asignado Actual</label>
                            <input class="form-control" readonly="readonly" value="<?php echo ($lead ? $lead->EJECUTIVO_NOMBRE : '') ?>" name="asignacionActual">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="exampleInputEmail1">Sectorista Actual</label>
                            <input class="form-control" readonly="readonly" value="<?php echo ($data ? $data->NOMBRE_SECTORISTA_ACTUAL : '') ?>" name="SectoristaActual">
                        </div>
                    </div>
                    <div class="form-group col-md-12" style="margin-left: 11px;">
                        <div class="row">
                            <div class="form-group col-md-2" style="margin-bottom: 0;">
                                <label for="exampleInputEmail1" style="">Asignar y Resectorizar</label>
                            </div>
                            <div class="form-group col-md-4">
                                @if($datosejecutivo!=null)
                                <select class="form-control {{count($datosejecutivo)>1 ? '' : 'hidden'}}" id="_codsectoristas" style="width: 50%;">
                                    @foreach($datosejecutivo as $codsectoristas)
                                    <option value="{{$codsectoristas->COD_CORTO}}--{{$codsectoristas->EN}}">{{$codsectoristas->COD_CORTO}}</option>
                                    @endforeach
                                </select>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4" style="padding-right: 17px;">
                                <input id="ejecutivo" class="form-control" readonly="readonly" value="{{ Auth::user()->NOMBRE }}" style="">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <div class="form-group col-md-2">
                            <label for="exampleInputEmail1">Última Etapa</label>
                            <input class="form-control" readonly="readonly" value="<?php echo ($data ? $data->ETAPA_NOMBRE : '') ?>" name="etapa">
                        </div>
                        <div class="form-group col-md-2">
                            <label for="exampleInputEmail1">Motivo de Eliminación</label>
                            <input class="form-control" readonly="readonly" value="<?php   ?>" name="motivoDetalle">
                        </div>
                        <div class="form-group col-md-2">
                            <label for="exampleInputEmail1">Fecha de Última Etapa</label>
                            <div class="input-group">
                                <div class="input-group-addon styleAddOn"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></div>
                                <input class="form-control" readonly="readonly" value="<?php echo ($data ? $data->FECHA_ULTIMO_ETAPA : '') ?>" name="fechaUltimaE">
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="exampleInputEmail1">Fecha de Última Visita</label>
                            <div class="input-group">
                                <div class="input-group-addon styleAddOn"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></div>
                                <input class="form-control" readonly="readonly" value="<?php  ?>" name="fechaUltimaV">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-12" hidden>
                        <label for="exampleInputEmail1">Sectorista</label>
                        <input class="form-control" readonly="readonly" name="sectorista">
                    </div>
                    @if (Auth::user()->REGISTRO!=$lead->REGISTRO_EN && $data->BANCA_ACTUAL!='G0')
                    <div class="form-group col-md-12" style=" padding: 20px;">
                        <div class="form-group col-md-6" style="color: white; background: #73879C;padding: 20px;">
                            <label for="exampleInputEmail1" style="font-size: 1.35em;margin-bottom: 20px;font-weight: 300;">Se Requiere V°B :</label>
                            <p>
                                <label for="exampleInputEmail1" style="font-size: 0.9em; font-weight: 300;">Debes adjuntar la conformidad, en caso se adjunte otro documento se considerará una falta grave</label>
                                <div class="input-group col-md-12 col-sm-12 col-xs-12">
                                    <input style="border-style: none;border-radius: 7px;padding: 10px;height: 40px;" type="file" name="adjuntoDocumento1" id="adjuntoDocumento1" class="form-control image" style="border-style: none">
                                </div>
                        </div>
                    </div>
                    <center>
                        <div style="padding:20px">
                            <button class="btn btn-primary btn-lg btn-block btn-success" type="submit" style="font-size: 1.35em;
    padding-top: 10px;
    padding-bottom: 10px;
    padding-right: 30px;
    padding-left: 30px;
    border-style: none;
    display: flex;
    float: right;
    margin-bottom: 30px;">Guardar</button>
                        </div>
                    </center>
                    @elseif (Auth::user()->REGISTRO!=$lead->REGISTRO_EN && $data->BANCA_ACTUAL=='G0')
                    <div class="form-group col-md-12" style=" padding: 20px;">
                        <div class="form-group col-md-6" style="color: white;background: #73879C;padding: 20px;">
                            <label for="exampleInputEmail1" style="font-size: 1.35em;margin-bottom: 20px;font-weight: 300;">Se Requiere V°B del Asignado actual:</label>
                            <p>
                                <label for="exampleInputEmail1" style="font-size: 0.9em; font-weight: 300;">Debes adjuntar la conformidad con el V°B del Asignado correspondiente, en caso se adjunte otro documento se considerará una falta grave</label>
                                <div class="input-group col-md-12 col-sm-12 col-xs-12">
                                    <input style="border-style: none;border-radius: 7px;padding: 10px;height: 40px;" type="file" name="adjuntoDocumento1" required id="adjuntoDocumento1" class="form-control image" style="border-style: none" {{ $cant>=5 ? required : '' }}>
                                </div>
                        </div>
                        <div class="form-group col-md-6" style="color: white;background: #73879C;padding: 20px;">
                            <label for="exampleInputEmail1" style="font-size: 1.35em;margin-bottom: 20px;font-weight: 300;">Se Requiere V°B del Sectorista actual:</label>
                            <p>
                                <label for="exampleInputEmail1" style="font-size: 0.9em; font-weight: 300;">Debes adjuntar la conformidad con el V°B del Sectorista correspondiente, en caso se adjunte otro documento se considerará una falta grave</label>
                                <div class="input-group col-md-12 col-sm-12 col-xs-12">
                                    <input style="border-style: none;border-radius: 7px;padding: 10px;height: 40px;" type="file" name="adjuntoDocumento2" id="adjuntoDocumento2" class="form-control image" style="border-style: none">
                                </div>
                        </div>
                    </div>
                    <center>
                        <div style="padding:20px"><button class="btn btn-primary btn-lg btn-success" type="submit" style="font-size: 1.35em;
    padding-top: 10px;
    padding-bottom: 10px;
    padding-right: 30px;
    padding-left: 30px;
    border-style: none;
    display: flex;
    float: right;
    margin-bottom: 30px;">Guardar</button></div>
                    </center>
                    @endif
                </div>
            </div>
        </div>

    </form>
    @endif
    @endif


</div>

<script>
    $(document).ready(function() {
        $("#ejecutivo").val($("#_codsectoristas").val());
    });
    $(document).on('change', '#_codsectoristas', function() {
        $("#ejecutivo").val($(this).val());
        $("#codcorto").val($(this).val());
    });

    function inputLimiter(e, allow) {
        var AllowableCharacters = '';

        if (allow == 'custom') {
            AllowableCharacters = ' 1234567890.';
        }


        var k = document.all ? parseInt(e.keyCode) : parseInt(e.which);
        if (k != 13 && k != 8 && k != 0) {
            if ((e.ctrlKey == false) && (e.altKey == false)) {
                return (AllowableCharacters.indexOf(String.fromCharCode(k)) != -1);
            } else {
                return true;
            }
        } else {
            return true;
        }

    }
</script>

@stop