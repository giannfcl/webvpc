@extends('reprograc.utilitario')
@section('vista-reprogramacioncreditos')
    <div class="modal fade" id="cargando" tabindex="-1" role="dialog">
        <div class="modal-dialog loader" role="document">
            <button type="button" class="close cerrarcargando" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" hidden>&times;</span></button>
        </div>
    </div>
    
    @if ($rol == 3)
        <div class="x_panel">
            <div class="row">
                <div class="form-group col-md-3">
                    <label class="control-label col-md-4" style="margin-bottom: 0">Estado</label>
                    <div class="col-md-8" style="margin-bottom: 0">
                        <select id="filter_estado" class="form-control">
                            <option value="" selected>Todos...</option>
                            <option value="1">Pendientes</option>
                            <option value="2">Aprobados</option>
                            <option value="3">Rechazados</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div>
        <div class="row">
            <div class="x_panel">
                <div class="x_content">
                    <table class="table table-striped table-bordered jambo_table" id="datatb">
                        <thead>
                            <tr>
                                <th>Crédito</th>
                                <th>Cliente</th>
                                <th>Fecha Próximo Pago</th>
                                <th>Producto</th>
                                <th>Saldo</th>
                                <th>Reprogramación Realizada</th>
                                <th>Credito Hijo T0</th>
                                <th>Estado</th>
                                <th>Gestión</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="Modalgestionrpc">
        <div class="modal-dialog" role="document" style="width: 1200px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">(<span id="mcodunico"></span>) <span id="mcliente"></span></h4>
                    <label id="labelsolicitud"></label>
                </div>
                <div class="modal-body">
                    <form id="formRenegociacion" enctype="multipart/form-data" action="{{route('rp.gestionar')}}" style="padding-left: 40px">
                        <input type="hidden" name="mcodcredito" id="mcodcredito">
                        <div class="row" style="margin-top: -15px;">
                            <div class="col-sm-6">
                                <div class="row">
                                    <h4>INFORMACIÓN CLIENTE:</h4>
                                </div>
                                <div class="row">
                                    <label class="col-md-5">Código de Crédito:</label>
                                    <div class="col-md-6">
                                        <label id="codigocredito"></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-md-5">Producto:</label>
                                    <div class="col-md-6">
                                        <label id="mproducto"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="row">
                                    <h4>INFORMACIÓN ACTUAL:</h4>
                                </div>
                                <div class="row">
                                    <label class="col-md-5">Fecha de próximo <br>pago cuota actual:</label>
                                    <div class="col-md-6">
                                        <label id="fechaproxpago"></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-md-5">Número de cuotas <br>originales de crédito:</label>
                                    <div class="col-md-6">
                                        <label id="cuotasoriginales"></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-md-5">Número de <br>cuotas pendientes:</label>
                                    <div class="col-md-6">
                                        <label id="cuotaspendientes"></label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="divOpcionesPago">
                            <div class="row">
                                <h4>FACILIDADES DE PAGO:</h4>
                            </div>
                            <div class="row" id="ejecutivoasignado" hidden="hidden">
                                <div class="col-sm-10">
                                    <ul>
                                        <li>Ejecutivo Asignado : <span id="lblejecutivoasignado"></span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="row" id="riesgosasignado" hidden="hidden">
                                <div class="col-sm-10">
                                    <ul>
                                        <li>Riesgos Asignado : <b id="lblriesgosasignado"></b></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="row">
                                <!-- ejecutivo propuesta -->
                                <div id="EjecutivoPropuesta" hidden="hidden" class="col-sm-4">
                                    <b id="enpropuesta">Propuesta Ejecutivo</b>
                                    <ul>
                                        <li>Meses de Gracia: <b id="enpropuesta1"></b></li>
                                        <li>Primeras Cuotas "50% Cuota": <b id="enpropuesta2"></b></li>
                                        <li>Resto de Cuotas "Flat": <b id="enpropuesta3"></b></li>
                                        <li>1 Cuota adicional final (Ballon): <b id="enpropuesta4"></span></li>
                                        <li>Cuotas adicionales: <b id="enpropuesta5"></b></li>
                                        <li>Total Cuotas: <b id="enpropuesta6"></b></li>
                                        <li>Comentario: <b id="enpropuestaComentario"></b></li>
                                    </ul>
                                </div>
                                <!-- riesgos propuesta -->
                                <div id='divOpcionesPagoRiesgos' class="col-sm-4">
                                    <b>Opcion Riesgos</b>
                                    <ul>
                                        <li>Solución de Pago: <b id="lblPropuestaRiesgoTipo" style="font-size: 13px"></b></li>
                                        <li>Meses de Gracia: <b id="lblPropuestaRiesgoMgracia" style="font-size: 13px"></b></li>
                                        <li>Primeras Cuotas "50% Cuota": <b id="lblPropuestaRiesgoPrimCuotas" style="font-size: 13px"></b></li>
                                        <li>Resto de Cuotas "Flat": <b id="lblPropuestaRiesgoResCuotas" style="font-size: 13px"></b></li>
                                        <li>1 Cuota Adicional Final (Ballon): <b id="lblPropuestaRiesgoUCuotaAdicBall" style="font-size: 13px"></b></li>
                                        <li>Cuotas Adicionales: <b id="lblPropuestaRiesgoCAdicic" style="font-size: 13px"></b></li>
                                        <li>Total Cuotas: <b id="lblPropuestaRiesgoTCuotas" style="font-size: 13px"></b></li>
                                        <li>Comentarios: <b id="lblPropuestaRiesgoComentario" style="font-size: 13px"></b></li>
                                    </ul>
                                </div>
                                <div id='divOpcionesPago1' class="col-sm-4">
                                    <!-- Aquí se pega por javascript dependiendo de un flg-->
                                </div>
                                <div id='divOpcionesPago2' class="col-sm-4">
                                    <!-- Aquí se pega por javascript dependiendo de un flg-->
                                </div>
                            </div>
                        </div>

                        <div class="row DerivacionRiesgos" hidden="hidden">
                            <ul>
                                <li>Derivacion con Riegos: <span id="lblRechazadoRiesgos"></span></li>
                            </ul>
                        </div>

                        <div class="row">
                            <h4>Información solicitada por el cliente:</h4>
                        </div>
                        <div class="row form-group">
                            <label class="col-md-5 control-label">Respuesta:</label>
                            <label class="col-md-1">:</label>
                            <div class="col-md-4">
                                <select class="form-control" id="mrespuesta" name="mrespuesta" required>  
                                </select>
                            </div>
                        </div>

                        <div class="row form-group divSubGestion">
                            <label class="col-md-5 control-label">Sub Gestión:</label>
                            <label class="col-md-1">:</label>
                            <div class="col-md-4">
                                <select class="form-control" id="msubgestion" name="msubgestion">
                                    <option value="">Seleccionar Opción</option>
                                    <option value="1">Solicitud Pendiente</option>
                                    <option value="2">Solicitud Cargada</option>
                                    <option value="3">No Acepta</option>
                                </select>
                            </div>
                        </div>

                        <!-- ejecutivo -->
                        <div class="row ocultos" disabled hidden="hidden">
                            <div class="col-md-4 form-group row">
                                <label class="col-md-8 control-label">Meses de Gracia:</label>
                                <div class="col-md-4">
                                    <input class="form-control ocultos input-number" disabled name="driesgosgracia" id="driesgosgracia" type="text" maxlength="2">
                                </div>
                            </div>
                            <div class="col-md-4 form-group row">
                                <label class="col-md-8 control-label">Primeras Cuotas "50% Cuota":</label>
                                <div class="col-md-4">
                                    <input class="form-control ocultos input-number" disabled name="driesgosprimcuotas" id="driesgosprimcuotas" type="text" maxlength="2">
                                </div>
                            </div>
                            <div class="col-md-4 form-group row">
                                <label class="col-md-8 control-label">Resto de Cuotas "Flat":</label>
                                <div class="col-md-4">
                                    <select class="form-control ocultos" disabled hidden="hidden" name="driesgosrestocuotas" id="driesgosrestocuotas">
                                        <option value="">Seleccionar Opcion</option>
                                        <option value="1">SI</option>
                                        <option value="2">NO</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row ocultos" disabled hidden="hidden">
                            
                            <div class="col-md-4 form-group row">
                                <label class="col-md-8 control-label">1 Cuota adicional final (Balloon):</label>
                                <div class="col-md-4">
                                    <select class="form-control ocultos" disabled hidden="hidden" name="driesgoscuotaadicball" id="driesgoscuotaadicball">
                                        <option value="">Seleccionar Opcion</option>
                                        <option value="1">SI</option>
                                        <option value="2">NO</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4 form-group row">
                                <label class="col-md-8 control-label">Cuotas adicionales:</label>
                                <div class="col-md-4">
                                    <input class="form-control ocultos input-number" disabled hidden="hidden" name="driesgoscuotaadic" id="driesgoscuotaadic" type="text" maxlength="2">
                                </div>
                            </div>
                            <div class="col-md-4 form-group row">
                                <label class="col-md-8 control-label">Total Cuotas <br><span style="font-size:11px">(Restructuración/Consolidación)</span>:</label>
                                <div class="col-md-4">
                                    <input class="form-control ocultos input-number" disabled hidden="hidden" name="driesgostotalcuotas" id="driesgostotalcuotas" type="text" maxlength="2">
                                </div>
                            </div>
                        </div>

                        <div class="row ocultos" disabled hidden="hidden">
                            
                        </div><br>


                        <!-- riesgos inputs -->
                        <div class="row ocultosriesgos" disabled hidden="hidden">
                            <div class="col-md-4  ocultosriesgos" disabled hidden="hidden">
                                <div class="form-group row">
                                    <label class="col-md-6 control-label">Solución de Pago:</label>
                                    <div class="col-md-6">
                                        <select class="form-control" id="dtipocuota" name="dtipocuota">
                                            <option value="">Seleccionar Opción</option>
                                            <option value="Cambio de Fecha">Cambio de Fecha</option>
                                            <option value="Cronograma Especial">Cronograma Especial</option>
                                            <option value="Cuota Ballon">Cuota Ballon</option>
                                            <option value="Reprogramación">Reprogramación</option>
                                            <option value="Consolidación">Consolidación</option>
                                            <option value="Refinanciamiento">Refinanciamiento</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4  ocultosriesgos" disabled hidden="hidden">
                                <div class="form-group row">
                                    <label class="col-md-8 control-label">Meses de Gracia:</label>
                                    <div class="col-md-4">
                                        <input class="form-control ocultosriesgos input-number" disabled name="propuestagracia" id="propuestagracia" type="text">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4  ocultosriesgos" disabled hidden="hidden">
                                <div class="form-group row">
                                    <label class="col-md-8 control-label">Primeras Cuotas "50% Cuota":</label>
                                    <div class="col-md-4">
                                        <input class="form-control ocultosriesgos input-number" disabled name="propuestaprimcuotas" id="propuestaprimcuotas" type="text">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div     class="row ocultosriesgos" disabled hidden="hidden">
                            <div class="col-md-4  ocultosriesgos" disabled hidden="hidden">
                                <div class="form-group row">
                                    <label class="col-md-8 control-label">Resto de Cuotas "Flat":</label>
                                    <div class="col-md-4">
                                        <select class="form-control ocultosriesgos" disabled hidden="hidden" name="propuestarestocuotas" id="propuestarestocuotas">
                                            <option value="">Elegir</option>
                                            <option value="1">SI</option>
                                            <option value="2">NO</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4  ocultosriesgos" disabled hidden="hidden">
                                <div class="form-group row">
                                    <label class="col-md-8 control-label">1 Cuota adicional final (Balloon):</label>
                                    <div class="col-md-4">
                                        <select class="form-control ocultosriesgos" disabled hidden="hidden" name="propuestacuotaadicball" id="propuestacuotaadicball">
                                            <option value="">Elegir</option>
                                            <option value="1">SI</option>
                                            <option value="2">NO</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 ocultosriesgos" disabled hidden="hidden">
                                <div class="form-group row">
                                    <label class="col-md-8 control-label">Cuotas adicionales:</label>
                                    <div class="col-md-4">
                                        <input class="form-control ocultosriesgos input-number" disabled hidden="hidden" name="propuestacuotaadic" id="propuestacuotaadic" type="text" maxlength="2">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row ocultosriesgos" disabled hidden="hidden">
                            <div class="col-md-4 ocultosriesgos" disabled hidden="hidden">
                                <div class="form-group row">
                                    <label class="col-md-8 control-label">Total Cuotas <br><span style="font-size:11px">(Restructuración/Consolidación)</span>:</label>
                                    <div class="col-md-4">
                                        <input class="form-control ocultosriesgos input-number" disabled hidden="hidden" name="propuestatotalcuotas" id="propuestatotalcuotas" type="text" maxlength="2">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>

                       @if(in_array($rol,array(\App\Entity\Usuario::ROL_RIESGO_BPE,\App\Entity\Usuario::ROL_ANALISTA_RIESGOS_COBRANZA)))
                            <div class="row form-group">
                                <label class="col-md-2 control-label">Comentarios:</label>
                                <div class="col-md-8">
                                    <textarea class="form-control" id="rcomentario" name="rcomentario"></textarea>
                                </div>
                            </div>
                        @else
                            <div class="row form-group">
                                <label class="col-md-2 control-label">Comentarios:</label>
                                <div class="col-md-8">
                                    <textarea class="form-control" id="gescomentario" name="gescomentario"></textarea>
                                </div>
                            </div>
                        @endif
                        
                        <br>
                        <center class="guardarGestion"><button class="btn btn-success" type="submit">Guardar</button></center>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div>

    <style>
        .modal-body {
		max-height: calc(95vh - 75px);
		overflow-y: auto;
	    }
    </style>
    <label id="rol" class="hidden">{{$rol}}</label>

@stop