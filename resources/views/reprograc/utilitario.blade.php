@extends('Layouts.layout')

@section('js-libs')
    <link href="{{ URL::asset('css/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('css/datatables.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('css/formValidation.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('css/multiselect.min.css') }}" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="{{ URL::asset('js/jszip.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/dataTables.buttons.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/pdfmake.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/vfs_fonts.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/buttons.flash.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/buttons.print.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/buttons.html5.min.js') }}"></script>

    <script type="text/javascript" src="{{ URL::asset('js/numeral.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/chart.bundle.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/utils.js') }}"></script>

    <script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.es.min.js') }}"></script>

    <script type="text/javascript" src="{{ URL::asset('js/formvalidation/formValidation.popular.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/formvalidation/language/es_CL.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/formvalidation/framework/bootstrap.min.js') }}"></script>

    <script type="text/javascript" src="{{ URL::asset('js/multiselect.min.js') }}"></script>
@stop

@section('content')
    <style type="text/css">
        tr{
            font-size: 10px
        }
        .loader {
            margin-top:10%;
            border: 16px solid #f3f3f3;
            border-radius: 50%;
            border-top: 16px solid #3498db;
            width: 120px !important;
            height: 120px !important;
            -webkit-animation: spin 2s linear infinite; /* Safari */
            animation: spin 2s linear infinite;
        }

        @-webkit-keyframes spin {
            0% { -webkit-transform: rotate(0deg); }
            100% { -webkit-transform: rotate(360deg); }
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }
    </style>
        @yield('vista-reprogramacioncreditos')
@stop

@section('js-scripts')
<script type="text/javascript">
    var rol = "{{$rol}}";
    var en =[
                "{{\App\Entity\Usuario::ROL_EJECUTIVO_NEGOCIO}}"
                , "{{\App\Entity\Usuario::ROL_CALL}}"
            ];
    var tv_se = [
                "{{\App\Entity\Usuario::ROL_TELEVENTAS}}"
                ,"{{\App\Entity\Usuario::ROL_SUPERVISOR_TELEVENTAS}}"
                ,"{{\App\Entity\Usuario::ROL_SOLUCION_EMPRESA}}"
                ,"{{\App\Entity\Usuario::ROL_CANALES_DISTRIBUCION}}"
            ];
    var riesgosbpe = [
                "{{\App\Entity\Usuario::ROL_RIESGO_BPE}}"
                ,"{{\App\Entity\Usuario::ROL_ANALISTA_RIESGOS_COBRANZA}}"
            ];
    tablaRP("{{$rol}}");

    function loadForm(revertirLimite){
            $('#formRenegociacion').formValidation('destroy', true);

            $('#formRenegociacion').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                'fechapago': {
                    validators: {
                        notEmpty: {
                            message: 'Debes seleccionar una fecha'
                        }
                    }
                },
                'rdiascuotas': {
                    validators: {
                        notEmpty: {
                            message: 'Debes ingresar un valor'
                        },
                        numeric: {
                            message: 'El dato ingresado no es numérico'
                        }
                    }
                },
                'gestiontelefono': {
                    validators: {
                        notEmpty: {
                            message: 'Ingresa un teléfono'
                        },
                        digits: {
                            message: 'Teléfono no válido'
                        }
                    }
                },
                'rcuotasadic': {
                    validators: {
                        notEmpty: {
                            message: 'ingresar un valor'
                        },
                        numeric: {
                            message: 'El dato ingresado no es numérico'
                        },
                        callback: {
                            message: 'Se ha excedido el límite de dias gracia + cuotas',
                            callback: function(value, validator, $field) {
                                final = diasgraciacalculado($("#fechaultpago").html(),$("#fechapago").val(),$('#rcuotasadic').val())
                                return (final <= revertirLimite);
                            }
                        }
                    }
                }
            }
        }).off('success.form.fv');
    }

    function diasgraciacalculado(fecha1,fecha2,cuotasAdic) {
        array_fechavenccuotapagada = $.trim(fecha1).length>0?fecha1.split("-"):false;
        array_fechaproxpagocliente = $.trim(fecha2).length>0?fecha2.split("-"):false;

        if (array_fechavenccuotapagada && array_fechaproxpagocliente) {
            var f1 = new Date(parseInt(array_fechavenccuotapagada[0]),parseInt(array_fechavenccuotapagada[1]-1),parseInt(array_fechavenccuotapagada[2])).getTime();
            var f2 = new Date(parseInt(array_fechaproxpagocliente[0]),parseInt(array_fechaproxpagocliente[1]-1),parseInt(array_fechaproxpagocliente[2])).getTime();
        }else{
            var f1 = 0;
            var f2 = 0;
        }

        final = 0;
        if (cuotasAdic){
            final = (f2-f1)/(1000*60*60*24)  + cuotasAdic * 30
        }else{
            final = (f2-f1)/(1000*60*60*24)
        }

        $('#formRenegociacion').find('input[name="rdiascuotas"]').val(final)
        return final;
    }

    function limpiarformulario() {
        $("select").val("");
        $("textarea").val("");
        $("#rdiascuotas").val("");
        $("#rcuotasadic").val("");
        $("#gestiontelefono").val("");
        $("#gestionemail").val("");
        
        $("#driesgosgracia").val("");
        $("#driesgosprimcuotas").val("");
        $("#driesgosrestocuotas").val("");
        $("#driesgoscuotaadicball").val("");
        $("#driesgoscuotaadic").val("");
        $("#driesgostotalcuotas").val("");

        $("#propuestagracia").val("");
        $("#propuestagracia").val("");
        $("#propuestagracia").val("");
        $("#propuestagracia").val("");
        $("#propuestagracia").val("");
        $("#propuestagracia").val("");
        
        $(".ocultos").attr("disabled", "disabled");
        $(".ocultos").attr("hidden", "hidden");

        $(".ocultosriesgos").attr("disabled", "disabled");
        $(".ocultosriesgos").attr("hidden", "hidden");

        $("#EjecutivoPropuesta").attr("hidden","hidden");
        $(".DerivacionRiesgos").attr("hidden","hidden");
    }

    $(document).on("change", "#mrespuesta", function () {
        $(".ocultos").attr("disabled", "disabled");
        $(".ocultos").attr("hidden", "hidden");
        $(".ocultosriesgos").attr("disabled", "disabled");
        $(".ocultosriesgos").attr("hidden", "hidden");
        $(".divSubGestion").addClass("hidden");

        if ($(this).val() == 14) {
            $(".ocultos").removeAttr("disabled");
            $(".ocultos").removeAttr("hidden");
        }

        if(['1','2'].includes($(this).val())){
            $(".ocultosriesgos").removeAttr("disabled");
            $(".ocultosriesgos").removeAttr("hidden");
        }
        if(['11','12','13'].includes($(this).val())){
            $(".divSubGestion").removeClass("hidden");
        }
    });

    $(document).on("click", ".btnmodal", function () {
        $("#Modalgestionrpc").modal();
        //console.log($(this).val());
        limpiarformulario();

        if(jQuery.inArray(rol,riesgosbpe)>=0){
            $("#EjecutivoPropuesta").removeAttr("hidden");
            $(".ocultosriesgos").removeAttr("disabled", "disabled");
            $(".ocultosriesgos").removeAttr("hidden", "hidden");
        }
        $("#labelsolicitud").attr("hidden","hidden");
        $("#ejecutivoasignado").attr("hidden","hidden");
        $("#riesgosasignado").attr("hidden","hidden");
        
        $(".guardarGestion").html("<button class='btn btn-success' type='submit'>Guardar</button>");
        $(".guardarGestion button").removeAttr("disabled");
        if ( $.trim($(this).attr('codcredito')).length>0 ) {
            $.ajax({
                url: '{{route("rp.getrpcbycodcredito")}}',
                type: 'POST',
                async: false,
                data: {
                    codcredito: $(this).attr('codcredito')
                },
                beforeSend: function() {
                    $("#cargando").modal();
                },
                success:function(result) {
                    $("#mcodcredito").val(result.COD_CREDITO);

                    $("#mcodunico").html(result.COD_UNICO);
                    $("#mcliente").html(result.CLIENTE);
                    $("#mproducto").html(result.PRODUCTO_REAL);
                    $("#codigocredito").html(result.COD_CREDITO);

                    $("#fechaultpago").html(result.FECHA_ULTIMO_PAGO);
                    $("#fechaproxpago").html(result.FECHA_PROXIMO_PAGO);
                    $("#cuotasoriginales").html(result.CUOTAS_ORIGINALES);
                    $("#cuotaspendientes").html(result.CUOTAS_PENDIENTES);

                    $("#gescomentario").val(result.GESTION_COMENTARIO);
                    $("#rcomentario").val(result.PROPUESTA_RIESGOS_COMENTARIOS);

                    // Si no hay ninguna opcion de pago (1,2 o riesgos)
                    $('#divOpcionesPago1').addClass('hidden');
                    $('#divOpcionesPago2').addClass('hidden');
                    $('#divOpcionesPagoRiesgos').addClass('hidden');
                    if (result.opcionesPago <= 0){
                        $('#divOpcionesPago').addClass('hidden');
                    }else{
                        //Segun existan opciones de pago
                        $('#divOpcionesPago').removeClass('hidden');
                        if (result.OPCION_PAGO_1_FLAG == '1'){
                            $('#divOpcionesPago1').removeClass('hidden');
                            $('#divOpcionesPago1').html(result.OP1_TXT);
                        }
                        if (result.OPCION_PAGO_2_FLAG == '1'){
                            $('#divOpcionesPago2').removeClass('hidden');
                            $('#divOpcionesPago2').html(result.OP2_TXT);
                        }
                        if (result.OPCION_PAGO_RIESGOS_FLAG == '1' && jQuery.inArray(rol,riesgosbpe)<0){
                            $('#divOpcionesPagoRiesgos').removeClass('hidden');
                            $('#lblPropuestaRiesgoTipo').html(result.PROPUESTA_RIESGOS_TIPO)
                            $('#lblPropuestaRiesgoMgracia').html(result.PROPUESTA_RIESGOS_MGRACIA)
                            $('#lblPropuestaRiesgoPrimCuotas').html(result.PROPUESTA_RIESGOS_PRIMERAS_CUOTAS)
                            $('#lblPropuestaRiesgoResCuotas').html(result.PROPUESTA_RIESGOS_RESTO_CUOTAS==1?'SI':(result.PROPUESTA_RIESGOS_RESTO_CUOTAS==2?'NO':''))
                            $('#lblPropuestaRiesgoUCuotaAdicBall').html(result.PROPUESTA_RIESGOS_UNA_CUOTA_ADICIONAL_BALL==1?'SI':(result.PROPUESTA_RIESGOS_UNA_CUOTA_ADICIONAL_BALL==2?'NO':''))
                            $('#lblPropuestaRiesgoCAdicic').html(result.PROPUESTA_RIESGOS_CUOTAS_ADICIONALES)
                            $('#lblPropuestaRiesgoTCuotas').html(result.PROPUESTA_RIESGOS_TOTAL_CUOTAS)
                            $('#lblPropuestaRiesgoComentario').html(result.PROPUESTA_RIESGOS_COMENTARIOS)
                        }
                    }

                    //Cargando opciones de respuesta
                    $("#mrespuesta").find('option').remove().end().append($("<option />").val('').text('--Selecciona una respuesta--'))
                    $.each(result.opcionesResultado, function (i, item) {
                        $("#mrespuesta").append($('<option>', {
                            value: i,
                            text : item
                        }));
                    });
                    
                    if(jQuery.inArray(rol,riesgosbpe)>=0){
                        $("#mrespuesta").val(result.RIESGOS_GESTION);
                    }else{
                        $("#mrespuesta").val((result.GESTION_RESPUESTA==14 && result.RIESGOS_GESTION==2)?'':result.GESTION_RESPUESTA);
                    }
                    if(jQuery.inArray(rol,riesgosbpe)<0 && result.RIESGOS_GESTION==3){
                        $(".DerivacionRiesgos").removeAttr("hidden");
                        $("#lblRechazadoRiesgos").html("Rechazado");
                    }

                    $("#mrespuesta").change();
                    $("#msubgestion").val(result.SUBGESTION_RESPUESTA);

                    
                    if (jQuery.inArray(rol,riesgosbpe)>=0){
                        if (result.FLAG_SOLICITUD_WEB == 1){
                            $("#enpropuesta").html("Propuesta Web ("+result.DERIVADO_RIESGOS_TIPO+")");
                        }else{
                            $("#enpropuesta").html("Propuesta Ejecutivo");
                        }
                        
                        $("#enpropuesta1").html(result.DERIVADO_RIESGOS_MGRACIA);
                        $("#enpropuesta2").html(result.DERIVADO_RIESGOS_PRIMERAS_CUOTAS);
                        $("#enpropuesta3").html(result.DERIVADO_RIESGOS_RESTO_CUOTAS==1?'SI':(result.DERIVADO_RIESGOS_RESTO_CUOTAS==2?'NO':''));
                        $("#enpropuesta4").html(result.DERIVADO_RIESGOS_UNA_CUOTA_ADICIONAL_BALL==1?'SI':(result.DERIVADO_RIESGOS_UNA_CUOTA_ADICIONAL_BALL==2?'NO':''));
                        $("#enpropuesta5").html(result.DERIVADO_RIESGOS_CUOTAS_ADICIONALES);
                        $("#enpropuesta6").html(result.DERIVADO_RIESGOS_TOTAL_CUOTAS);
                        $("#enpropuestaComentario").html(result.GESTION_COMENTARIO);
                        
                        $("#dtipocuota").val(result.PROPUESTA_RIESGOS_TIPO);
                        $("#propuestagracia").val(result.PROPUESTA_RIESGOS_MGRACIA);
                        $("#propuestaprimcuotas").val(result.PROPUESTA_RIESGOS_PRIMERAS_CUOTAS);
                        $("#propuestarestocuotas").val(result.PROPUESTA_RIESGOS_RESTO_CUOTAS);
                        $("#propuestacuotaadicball").val(result.PROPUESTA_RIESGOS_UNA_CUOTA_ADICIONAL_BALL);
                        $("#propuestacuotaadic").val(result.PROPUESTA_RIESGOS_CUOTAS_ADICIONALES);
                        $("#propuestatotalcuotas").val(result.PROPUESTA_RIESGOS_TOTAL_CUOTAS);

                        //Si hay dato de ejecutivo de negocio muestro el valor del EN
                        if(result.NOMBREEJECUTIVO){
                            $("#ejecutivoasignado").removeAttr("hidden");
                            $("#lblejecutivoasignado").html(result.NOMBREEJECUTIVO||' ');
                        }
                        //Mensaje de Soliciud Cargada
                        if (["2"].includes(result.SUBGESTION_RESPUESTA)) {
                            $("#labelsolicitud").removeAttr("hidden");
                            $("#labelsolicitud").html("Solicitud Cargada");
                        }
                        //Casos donde deshabilito el boton de AR
                        if (["3"].includes(result.RIESGOS_GESTION) //Rechazado
                        || (["2"].includes(result.SUBGESTION_RESPUESTA)) //Aprobado y Solicitud Cargada
                        ) {
                            $(".guardarGestion button").attr("disabled","disabled");
                        }
                    }else if(jQuery.inArray(rol,riesgosbpe)<0){
                        //Si la gestion es derivar a riesgo
                        if (["14"].includes(result.GESTION_RESPUESTA)) {
                            $("#driesgosgracia").val(result.DERIVADO_RIESGOS_MGRACIA);
                            $("#driesgosprimcuotas").val(result.DERIVADO_RIESGOS_PRIMERAS_CUOTAS);
                            $("#driesgosrestocuotas").val(result.DERIVADO_RIESGOS_RESTO_CUOTAS);
                            $("#driesgoscuotaadicball").val(result.DERIVADO_RIESGOS_UNA_CUOTA_ADICIONAL_BALL);
                            $("#driesgoscuotaadic").val(result.DERIVADO_RIESGOS_CUOTAS_ADICIONALES);
                            $("#driesgostotalcuotas").val(result.DERIVADO_RIESGOS_TOTAL_CUOTAS);
                        }
                        //Si hay dato de analista de riesgos muestro el valor del AR
                        if(result.NOMBRERIESGOS){
                            $("#riesgosasignado").removeAttr("hidden");
                            $("#lblriesgosasignado").html(result.NOMBRERIESGOS||' ');
                        }
                        //Casos donde deshabilito el boton de EN
                        if (
                            (["1","10"].includes(result.GESTION_RESPUESTA)) //Revertir y Mantener Cronograma
                        || (["11","12","13"].includes(result.GESTION_RESPUESTA) && ["2"].includes(result.SUBGESTION_RESPUESTA)) //Solicitudes Aprobadas
                        || ["14"].includes(result.GESTION_RESPUESTA) && ["1"].includes(result.RIESGOS_GESTION) //Derivado a Riesgos y Pendiente de Riesgos
                        ) {
                            $(".guardarGestion button").attr("disabled","disabled");
                        }
                    }
                },
                complete: function(resultado) {
                    console.log(resultado);
                    $(".cerrarcargando").click();
                }
            });
        }
    });
    $('#formRenegociacion').submit(function(e){
        //DERIVO A RIESGOS
        if ($('#mrespuesta').val()==14) {
            meses_gracia = $("#driesgosgracia").val()!=''?$("#driesgosgracia").val():false;
            driesgosprimcuotas = $("#driesgosprimcuotas").val()!=''?$("#driesgosprimcuotas").val():false;
            resto_cuotas_flat = $("#driesgosrestocuotas").val()!=''?$("#driesgosrestocuotas").val():false;
            una_cuota_adicional_ballon = $("#driesgoscuotaadicball").val()!=''?$("#driesgoscuotaadicball").val():false;
            cuotas_adicionales = $("#driesgoscuotaadic").val()!=''?$("#driesgoscuotaadic").val():false;
            total_cuotas = $("#driesgostotalcuotas").val()!=''?$("#driesgostotalcuotas").val():false;
            if(!(meses_gracia && driesgosprimcuotas && resto_cuotas_flat && una_cuota_adicional_ballon && cuotas_adicionales && total_cuotas)){
                Swal.fire(
                    'Se necesitan todos los datos de la solución de pago para poder derivar a riesgos',
                    'Click en el botón!',
                    'warning'
                )
                return false;
            }
		}
        //ACEPTO OPCION PAGO 1 y 2
        if (jQuery.inArray($('#mrespuesta').val(),[11,12])>=0) {
            if(!($("#msubgestion").val()>0)){
                Swal.fire(
                    'Se necesita seleccionar una subgestión',
                    'Click en el botón!',
                    'warning'
                )
                return false;
            }
		}
        this.submit();
    });

    $(document).on("click", ".btnmodalver", function () {
        $("#ModalVergestionrpc").modal();
    });

    $(document).on("click", ".btneditar", function () {
        $("#ModalEditargestionrpc").modal();
    });

    $(document).on("change","#filter_estado",function(){
		tablaRP("{{$rol}}");
	});

    function tablaRP(rol = null) {
        // Swal.fire(rol);
        if ($.fn.dataTable.isDataTable('#datatb')) {
            $('#datatb').DataTable().destroy();
        }
        $('#datatb').DataTable({
            processing: true,
            "bAutoWidth": false,
            rowId: 'staffId',
            dom: 'Blfrtip',
            buttons: [
                {
                    "extend": 'excel',
                    "text": 'EXCEL',
                    "className": 'btn btn-success btn-xs'
                }
            ],
            serverSide: true,
            language: {
                "url": "{{ URL::asset('js/Json/Spanish.json') }}"
            },
            ajax: {
                "type": "GET",
                "url": "{{ route('rp.getrpc') }}",
                data: function (data) {
                    data.rol = rol;
                    data.riesgos_gestion = $("#filter_estado").val();
                }
            },
            "aLengthMenu": [
                [30, 60, -1],
                [30, 60, "Todo"]
            ],
            "iDisplayLength": 30,
            "order": [
                [7, "desc"]
            ],
            columnDefs: [
                {
                    targets: 0,
                    data: 'WCC.COD_CREDITO',
                    name: 'WCC.COD_CREDITO',
                    render: function (data, type, row) {
                        return row.COD_CREDITO;
                    }
                },
                {
                    targets: 1,
                    data: 'WCC.COD_UNICO',
                    name: 'WCC.COD_UNICO',
                    render: function (data, type, row) {
                        linea1 = "<span>CU: " + row.COD_UNICO + "<span>";
                        linea2 = "<br><span>CLIENTE: " + row.CLIENTE + "<span>";
                        if (row.CLIENTE_DIRECCION){
                            linea3 = "<br><span>DIRECCION RRLL: " + (row.CLIENTE_DIRECCION || '-') + "<span>";
                        }else{
                            linea3=""
                        }
                        
                        linea4 = '';
                        if ($.inArray(rol, tv_se) >= 0) {
                            linea4 = "<br><span>EN: " + row.NOMBRE_EN||'-' + "<span>"
                        }
                        return linea1 + linea2 + linea3 + linea4;
                    }
                },
                {
                    targets: 2,
                    data: 'WCC.NUM_DOC',
                    name: 'WCC.NUM_DOC',
                    sortable: false,
                    render: function (data, type, row) {
                        linea1 = "<span>" + (row.FECHA_PROXIMO_PAGO || '') + "<span>";
                        return linea1;
                    }
                },
                {
                    targets: 3,
                    data: null,
                    searchable: false,
                    sortable: false,
                    render: function (data, type, row) {
                        linea1 = "<span>" + row.PRODUCTO_REAL + "<span>";
                        if (row.TIPO_CREDITO){
                            linea2 = "<span><br>(" + (row.TIPO_CREDITO || 'Desconocido') + ")";
                        }else{
                            linea2=""
                        }
                        
                        return linea1 + linea2;
                    }
                },
                {
                    targets: 4,
                    data: null,
                    searchable: false,
                    sortable: false,
                    render: function (data, type, row) {
                        if(row.SALDO_MONEDA_ORIGINAL){
                            linea1 = "<span>" + formatNumber(row.SALDO_MONEDA_ORIGINAL) + "<span>";
                            linea2 = "<br><span>TEA: " + (row.V_TEA<0||'0') + "%<span>";
                        }else{
                            linea1 = ""
                            linea2 = "-"
                        }
                        return linea1 + linea2;
                    }
                },
                {
                    targets: 5,
                    data: null,
                    searchable: false,
                    sortable: false,
                    render: function (data, type, row) {
                        
                        linea = "<span>Días de Gracia: " + row.TOTAL_AMPLIACION_DIAS + "<span>" 
                            + "<br><span>Cuotas Adic: " + row.TOTAL_AMPLIACION_CUOTAS + "<span>";
                        
                        
                        return linea;
                    }
                },
                {
                    targets: 6,
                    data: null,
                    searchable: false,
                    sortable: false,
                    render: function (data, type, row) {
                        return row.CREDITO_HIJO
                    }
                },
                {
                    targets: 7,
                    data: 'WCC.FLAG_SOLICITUD_WEB',
                    name: 'WCC.FLAG_SOLICITUD_WEB',
                    searchable: false,
                    sortable: true,
                    render: function (data, type, row) {
                        if (row.FLAG_SOLICITUD_WEB == '1')
                            return '<h5><span class="label label-success">WEB</span></h5>';
                        else
                            return '';
                    }
                },
                {
                    targets: 8,
                    data: null,
                    searchable: false,
                    sortable: false,
                    render: function (data, type, row) {
                        if (["{{\App\Entity\Usuario::ROL_RIESGO_BPE}}","{{\App\Entity\Usuario::ROL_ANALISTA_RIESGOS_COBRANZA}}"].includes($('#rol').html())){
                            estados = []
                            estados['1'] = 'Pendiente';
                            estados['2'] = 'Aprobados';
                            estados['3'] = 'Rechazado';
                            html = "<u><a codcredito='" + row.COD_CREDITO + "' class='btn btn-default btnmodal'>" + estados[row.RIESGOS_GESTION]+ "</a></u>";
                        }else{
                            if (row.GESTION_RESPUESTA) {
                                estados = []
                                estados['1'] = 'Acepta';
                                estados['2'] = 'Acepta con Condiciones';
                                estados['3'] = 'No Acepta';
                                estados['4'] = 'Lo Pensará';
                                estados['6'] = 'Revertir';
                                estados['5'] = 'Derivar Ejecutivo';
                                estados['10'] = 'Revertir Cronograma Original';
                                estados['11'] = 'Aceptó Opción1';
                                estados['12'] = 'Aceptó Opción2';
                                estados['13'] = 'Aceptó Opción Riesgos';
                                estados['14'] = 'Enviado a Riesgos';

                                html = "<u><a codcredito='" + row.COD_CREDITO + "' class='btn btn-default btnmodal'>" + estados[row.GESTION_RESPUESTA]+ "</a></u>";
                            }else{
                                html = "<u><a codcredito='" + row.COD_CREDITO + "' class='btn btn-warning btnmodal'>Pendiente</a></u>";
                            }
                        }
                        
                        return html;
                    }
                },
                {
                    targets: 9,
                    data: 'WCC.CLIENTE',
                    name: 'WCC.CLIENTE',
                    visible: false,
                    sortable: false,
                    render: function (data, type, row) {
                        return "-";
                    }
                }
            ]
        });
    };

    function formatNumber(number) {
        return parseFloat(number).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')
    };

    $('.input-number').on('input', function () { 
		this.value = this.value.replace(/[^0-9]/g,'');
	});

</script>
@stop