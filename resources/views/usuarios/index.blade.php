@extends('Layouts.layout')
@section('pageTitle','Usuarios')
@section('js-libs')
    <link href="{{ URL::asset('css/datatables.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('css/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="{{ URL::asset('js/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/dataTables.buttons.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/numeral/numeral.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/multiselect.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.es.min.js') }}"></script>
@stop

@section('content')
    <a class='btn btn-success' onclick="Musuario(1);"> Agregar Usuario </a>
    <table class='table table-striped table-bordered jambo_table' id="datatableusu">
        <thead>
            <tr>
                <th scope="col" style="font-size: 12.5px;">REGISTRO</th>
                <th scope="col" style="font-size: 12.5px;">NOMBRE</th>
                <th scope="col" style="font-size: 12.5px;">ROL</th>
                <th scope="col" style="font-size: 12.5px;">ESTADO</th>
                <th scope="col" style="font-size: 12.5px;">BANCA</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>

<div class="modal fade" tabindex="-1" role="dialog" id="modalUsuario">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close cerrarformusuario" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="col-sm-4">
                    <h4 class="modal-title Formtitulo">Nuevo Usuario</h4>
                </div>
            </div>
            <div class="modal-body">
                <form id="formUsuario" class="form-horizontal form-label-left" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="control-label col-sm-5 col-xs-12" style="text-align: left;">REGISTRO:</label>
                        <div class="col-sm-6 col-xs-12" style="padding-left:0px">
                            <input autocomplete="off" class="form-control"  type="text" name="registro">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5 col-xs-12" style="text-align: left;">NOMBRE:</label>
                        <div class="col-sm-6 col-xs-12" style="padding-left:0px">
                            <input autocomplete="off" class="form-control"  type="text" name="nombre">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5 col-xs-12" style="text-align: left;">DNI (opcional):</label>
                        <div class="col-sm-6 col-xs-12" style="padding-left:0px">
                            <input autocomplete="off" class="form-control"  type="text" name="dni">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5 col-xs-12" style="text-align: left;">ROL:</label>
                        <div class="col-sm-6 col-xs-12" style="padding-left:0px">
                            <select name="rol" class="form-control">
                                <option value="">Seleccionar Rol</option>
                                @foreach($roles as $rol)
                                    <option value="{{$rol->ID_ROL}}">{{$rol->NOMBRE}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5 col-xs-12" style="text-align: left;">BANCA:</label>
                        <div class="col-sm-6 col-xs-12" style="padding-left:0px">
                            <select name="banca" class="form-control">
                                <option value="">Seleccionar Banca</option>
                                @foreach($tiposbancas as $tipobanca)
                                    <option value="{{$tipobanca->CODIGO}}">{{$tipobanca->DESCRIPCION}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <center class="boton">
                        
                    </center>

                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="cargando" tabindex="-1" role="dialog">
    <div class="modal-dialog loader" role="document">
        <button type="button" class="close cerrarcargando" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" hidden>&times;</span></button>
    </div>
</div>
<style type="text/css">
    .loader {
        margin-top:10%;
        border: 16px solid #f3f3f3;
        border-radius: 50%;
        border-top: 16px solid #3498db;
        width: 120px !important;
        height: 120px !important;
        -webkit-animation: spin 2s linear infinite; /* Safari */
        animation: spin 2s linear infinite;
    }

    @-webkit-keyframes spin {
        0% { -webkit-transform: rotate(0deg); }
        100% { -webkit-transform: rotate(360deg); }
    }

    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
</style>
@stop

@section('js-scripts')
<script type="text/javascript">
    tablausuarios();

    function Musuario(tipo,vregistro=null){
        $("#modalUsuario").modal();
        $("#modalUsuario input[name='registro']").val("");
        $("#modalUsuario input[name='nombre']").val("");
        $("#modalUsuario input[name='dni']").val("");
        $("#modalUsuario select[name='rol']").val("");
        $("#modalUsuario select[name='banca']").val("");
        if (tipo==1) {
            $(".Formtitulo").html("Nuevo Usuario");
            $("#modalUsuario center.boton").html('<button type="button" class="btn btn-success" onclick="Addusuario();">Agregar</button>');
        }else{
            $(".Formtitulo").html("Actualizar Usuario");
            if (vregistro) {

                fetch('{{route("usuarios.getinfousuario")}}',{
                    headers: {
                      'Accept': 'application/json',
                      'Content-Type': 'application/json'
                    },
                    method: 'POST',
                    body: JSON.stringify({
                        registro: vregistro
                    })
                }).then(function(firstresponse) {
                    $("#cargando").modal();
                    return firstresponse.json();
                }).then(function(result) {
                    $("#modalUsuario input[name='registro']").val(result.REGISTRO);
                    $("#modalUsuario input[name='nombre']").val(result.NOMBRE);
                    $("#modalUsuario input[name='dni']").val(result.DNI);
                    $("#modalUsuario select[name='rol']").val(result.ROL);
                    $("#modalUsuario select[name='banca']").val(result.BANCA);
                    $("#modalUsuario center.boton").html('<button type="button" class="btn btn-success" onclick="Updateusuario();">Actualizar</button>');
                }).then(function(lastresponse) {
                    $(".cerrarcargando").click();
                });

            }
        }
    }

    function Addusuario() {
        registro = $.trim($("#modalUsuario input[name='registro']").val()).length>0?$("#modalUsuario input[name='registro']").val():null;
        nombre = $.trim($("#modalUsuario input[name='nombre']").val()).length>0?$("#modalUsuario input[name='nombre']").val():null;
        rol = $.trim($("#modalUsuario select[name='rol']").val()).length>0?$("#modalUsuario select[name='rol']").val():null;
        if (registro && nombre && rol) {
            $.ajax({
                type: "GET",
                data: $("#formUsuario").serialize(),
                url: '{{route("usuarios.nuevo")}}',
                beforeSend: function() {
                    $("#cargando").modal();
                },
                success: function(result) {
                    //console.log(result);
                    if(result[0]){
                        $(".cerrarformusuario").click();
                    }
                    tablausuarios();
                    Swal.fire(
                      result[1],
                      'Click en el botón!',
                      'success'
                    );
                },
                complete: function() {
                    $(".cerrarcargando").click();
                }
            });
        }else{
            Swal.fire(
              'Se necesita el registro',
              'Click en el botón!',
              'error'
            );
        }
    }

    function Updateusuario(registro) {
        registro = $.trim($("#modalUsuario input[name='registro']").val()).length>0?$("#modalUsuario input[name='registro']").val():null;
        nombre = $.trim($("#modalUsuario input[name='nombre']").val()).length>0?$("#modalUsuario input[name='nombre']").val():null;
        rol = $.trim($("#modalUsuario select[name='rol']").val()).length>0?$("#modalUsuario select[name='rol']").val():null;
        if (registro && nombre && rol) {
            $.ajax({
                type: "GET",
                data: $("#formUsuario").serialize(),
                url: '{{route("usuarios.updateusuario")}}',
                beforeSend: function() {
                    $("#cargando").modal();
                },
                success: function(result) {
                    //console.log(result);
                    if(result[0]){
                        $(".cerrarformusuario").click();
                    }
                    tablausuarios();
                    Swal.fire(
                      result[1],
                      'Click en el botón!',
                      'success'
                    );
                },
                complete: function() {
                    $(".cerrarcargando").click();
                }
            });
        }else{
            Swal.fire(
              'Se necesita el registro',
              'Click en el botón!',
              'error'
            );
        }
    }

    function tablausuarios(datos=null){  
        if ($.fn.dataTable.isDataTable('#datatableusu')) {
            $('#datatableusu').DataTable().destroy();
        }
        
        $('#datatableusu').DataTable({
            destroy:true,
            processing: true,
            "bAutoWidth": false, //ajustar tamaño de vista
            rowId: 'staffId',
            // dom: 'Blfrtip',
            serverSide: true,
            language: {"url": "{{ URL::asset('js/Json/Spanish.json') }}"},
            ajax: {
                "type": "GET",
                "url": "{{ route('usuarios.lista') }}", //ACA SE ENVÍA LOS DATOS
                data: function(data) {
                    data.registro = datos;
                }
            },
            "aLengthMenu": [
                [25, 50, -1],
                [25, 50, "Todo"] 
            ],
            "iDisplayLength": 25, //muestra los 25 primeros
            "order": [
                [0, "asc"] //orden de formar desc
            ],
            columnDefs: [
                {
                    targets: 0,
                    data: 'REGISTRO',
                    name: 'REGISTRO',
                    sortable: true,
                    render: function(data, type, row) {
                        string = '"'+row.REGISTRO+'"';
                        $linea1="<a onclick='Musuario(2,"+string+");'>"+(row.REGISTRO||'-')+"</a>";
                        return $linea1;
                    }
                },
                {
                    targets: 1,
                    data: 'NOMBRE',
                    name: 'NOMBRE',
                    sortable: true,
                    render: function(data, type, row) {
                        $linea1="<div>"+(row.NOMBRE||'-')+"</div>";
                        return $linea1;
                    }
                },
                {
                    targets: 2,
                    data: 'NOMBREROL',
                    name: 'NOMBREROL',
                    sortable: false,
                    render: function(data, type, row) {
                        $linea1="<div>"+(row.NOMBREROL||'-')+"</div>";
                        return $linea1;
                    }
                },
                {
                    targets: 3,
                    searchable: false,
                    sortable: false,
                    render: function(data, type, row) {
                        if (row.FLAG_ACTIVO==1) {
                            return "<div>ACTIVO</div>";
                        }
                        return "<div>INACTIVO</div>";
                    }
                },
                {
                    targets: 4,
                    data: 'BANCA',
                    name: 'BANCA',
                    render: function(data, type, row) {
                            return row.BANCA||('-');
                    }
                }
            ]
        });
    }
</script>
@stop