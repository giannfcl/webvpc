<div style="width:{{$width}}; float:left;">
        <button class="NxtBtn" onclick="{{$function}}">{{$texto}}</button>
</div>
<style>
.NxtBtn {
        margin-bottom: 9px;
        font-size: 1.2em;
        padding-right: 20px;
        padding-left: 20px;
        padding-top: 10px;
        border-style: solid;
        border-radius: 9px;
        background: #4DD094;
        margin-left: 25px;
        font-weight: bold;
        padding-bottom: 10px;
        color: white;
        transition: 0.25s all;
        border-color: transparent;
}
.NxtBtn:hover {
        border-color: #4DD094 !important;
        background: transparent !important;
        color: #4DD094 !important;
    }
</style>
