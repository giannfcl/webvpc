@if(!$hide)
    <div style="width:{{ $width }}; float:left;padding-right:20px;padding-left:20px;" class="justify-content-center">
        <p>{{ $tituloFiltro }}</p>
        <div class="form-group filtro">
            <select class="form-control" id="{{$id}}">
            @if($array!=null)
                @if(count($array)>=1)
                @foreach($array as $b)
                @if($b[$valor] == null)
                <option value="{{$b[$valor]}}" selected>Todos</option>
                @else
                <option value="{{$b[$valor]}}">{{$b[$etiqueta]}}</option>
                @endif
                @endforeach
                @endif
            @else
            <option selected>Todos</option>
            @endif
            </select>
        </div>
    </div>
    <style>
        .filtro {
            display: flex;

            justify-content: center;
        }
    </style>
@endif