<div style="display:{{($hide)?'none':'block'}}">
    <{{$tagTitle}}>
        {{$title}}
    </{{$tagTitle}}>
</div>
<div id="{{ $id }}" class="row ">
    {{$sections}}
</div>