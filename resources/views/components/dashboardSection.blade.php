<div style="float:left; padding:10px;width:{{ $width }}">
    <div id=" {{ $id }}" class="section-dashboard justify-content-md-center" style="width:100%">
        <div style="display:{{($rigid)?'none':'flex'}}" class="row hidebar"><i id="{{$idHideBtn}}" onclick="toggle('{{$idbodyWrapper}}',this.id)" class="  {{($hide) ? 'fa-chevron-up' :'fa-chevron-down'}}  hidebtn fas"></i></div>
        <div class="row graphic-top parentWrapper">
            <div class="wrapper" id="{{$idbodyWrapper}}">
                {{$body}}
            </div>
        </div>
    </div>
</div>

<script>
    document.addEventListener("DOMContentLoaded", function() {
        toggle('{{$idbodyWrapper}}', '{{$idHideBtn}}');
    });

    function hasClass(element, className) {
        return (' ' + element.className + ' ').indexOf(' ' + className + ' ') > -1;
    }

    function toggle(id, id2) {
        const element = document.getElementById(id);
        const element2 = document.getElementById(id2);
        if (hasClass(element2, "fa-chevron-up")) {
            element2.classList.remove("fa-chevron-up");
            element2.classList.add("fa-chevron-down");
            var height = element.offsetHeight;
            element.style.marginTop = '-' + (height * 1.5).toString() + 'px';
        } else {
            element.style.marginTop = '0px';
            element2.classList.remove("fa-chevron-down");
            element2.classList.add("fa-chevron-up");
        }
    }
</script>
<style>
    .wrapper {
        transition: 1s all;
        display: flex;
    align-items: flex-end;
    }

    .section-dashboard {
        background: white;
        float: left;
        padding: 20px;
        border-radius: 5px;
        box-shadow: 0 3px 11px 0px rgb(220, 220, 220);
    }

    .hidebtn {
        font-size: 15px !important;
        width: 40px !important;
        text-align: center;
        cursor: pointer;
    }

    .parentWrapper {
        overflow: hidden;

    }

    .hidebar {
        display: flex;
        justify-content: flex-end;
    }
</style>