<div id="maskWaiting" class="maskAppear">
    <div class="waitingwraper">
        <img style="height:100px;" src="../public/img/waiting.gif" alt="">
        <p style="margin-top: 15px;font-size: 1.3em;">Cargando...</p>
    </div>
</div>
<style>
    .waitingwraper {
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        background: white;
        padding: 40px;
        border-radius: 27px;
    }

    #maskWaiting {
        align-items: center;
        justify-content: center;
        position: fixed;
        background: rgba(0, 0, 0, 0.3);
        z-index: 999999999999999;
        top: 0;
        right: 0;
        width: 100vw;
        height: 100vh;
        display: none;
    }

    .maskAppear {
        display: flex !important;

    }
</style>