<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="shortcut icon" href="{{ URL::asset('img/ibk.png') }}">
    <title>@yield('pageTitle')</title>

    <link href="{{ URL::asset('css/layout.css') }}" rel="stylesheet" type="text/css">  <!-- ESTILOS QUE USO JONATHAN SALVADOR DESDE JUNIO 2019 -->


    <link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('css/custom/custom.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('css/custom/webvpc.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('css/font-awesome-5.7.1.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('css/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css">

    <!--JS-->
    <script type="text/javascript" src="{{ URL::asset('js/jquery-1.12.4.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('bower_components/moment/min/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/bootstrap-datetimepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/impl.js?v001') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/typeahead.bundle.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('sweetalert/dist/sweetalert2.all.js') }}"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    @yield('js-libs')
</head>

<body class="nav-md" style="color: rgb(128,128,128) !important">
    <div id="mainBody" style="max-width: 100% !important; padding:0 !important" class="container body">
        <div class="main_container" style="background: white !important;width: inherit !important;">

            <div id="panelBar" class="col-md-3 left_col" style="
                box-shadow: 0px 0 20px 0px rgba(0,0,0,0.17);
                position: fixed;">
                <div style="background: white;" class="left_col scroll-view" style="width: 100%">
                    <div class="clearfix"></div>

                    <!-- menu profile quick info -->
                    <div class="profile clearfix" style="display:flex; flex-direction:column;justify-content:center;align-items:center">
                        <div class="" style="padding-top:15px;padding-right: 15px;width: 100% !important;padding-left: 15px;">
                            <p>Bienvenido,</p>
                            @if (Auth::user())
                                <h4 style="font-size:1.2em">{{ Auth::user()->NOMBRE }}</h4>
                                <h6>{{ Auth::user()->REGISTRO }}</h6>
                            @endif
                        </div>
                    </div>
                    <!-- /menu profile quick info -->
                    <!-- sidebar menu -->
                    <div id="sidebar_menu" class="main_menu_side hidden-print main_menu" style="font-size:0.95em">
                        <div class="menu_section">
                            <ul id="ulNav" class="nav side-menu">
                                <li class="{{ (Route::currentRouteName() == 'encuesta.interna.principal')? 'active':'' }}" style="background:rgb(255,153,148)">
                                    <a style="color:white !important;    display: flex;justify-content: center;align-items: center;" href="{{ route('encuesta.interna.principal') }}"><i style="margin-right: 5px;" class="icony fas fa-user"></i>
                                        <div>Encuesta de Satisfacción</div>
                                    </a>
                                </li>
                            </ul>
                        </div>

                    </div>
                    <!-- /menu footer buttons -->
                </div>
            </div>
            <!-- top navigation -->
            <div id="topNav" class="top_nav">
                <div class="nav_menu" style="background-color: #4DD094 !important;">
                    <nav>
                        <div id="showhide" style="cursor:pointer;float: left;height: 60px;display: flex;justify-content: center;align-items: center; text-align: center; font-size: 1.5em; padding: 20px;">
                            <a style="color: white !important"><i class="icony fas fa-bars"></i></a>
                        </div>
                        <script>

                        </script>
                        <div class="dropdown">
                            <div class="btn btn-secondary dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i style="font-size: 1.6em;" class="far fa-user"></i>
                            </div>
                            <div style="padding: 20px;margin-top: 15px; left: -327px;width:400px" class="dropdown-menu" aria-labelledby="dropdownMenuButton">

                                <a class="li-link2 clickvista" href="{{ route('pass.change') }}"><i class="icony fa fa-key"></i>
                                    <div>Cambiar contraseña</div>
                                </a>
                                <a class="li-link2" href="{{ route('logout') }}">
                                    <span class="icony glyphicon glyphicon-off" aria-hidden="true"></span>
                                    <div>Cerrar Sesión</div>
                                </a>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
            <!-- /top navigation -->

            <!-- page content -->
            <div id="content" class="right_col" role="main" style="min-height: 1550px;background:rgb(241, 241, 241) !important;">
                <div style="">
                    <div class="page-title" style="display:none">
                        <div class="row">
                            <div class="col-sm-8">
                                <h3>@yield('pageTitle')</h3>
                            </div>
                            <div class="text-right">
                                @yield('tituloDerecha')
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    @include('flash::message')

                    @yield('content')
                </div>
            </div>
        </div>
    </div>
</body>


<div class="modal fade" id="cargando" tabindex="-1" role="dialog">
	<div class="modal-dialog loader" role="document">
		<button type="button" class="close cerrarcargando" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" hidden>&times;</span></button>
	</div>
</div>

@yield('js-scripts')

</html>
