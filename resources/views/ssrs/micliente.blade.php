<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <link rel="shortcut icon" href="{{ URL::asset('img/ibk.png') }}">

        <title>Mi Cliente - VPConnect</title>

        <!-- Fonts 
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        -->

        <!-- CSS-->
        <link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ URL::asset('css/custom/custom.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ URL::asset('css/custom/webvpc.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ URL::asset('css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ URL::asset('css/font-awesome-5.7.1.min.css') }}" rel="stylesheet" type="text/css">

        <!--JS-->
        <script type="text/javascript" src="{{ URL::asset('js/jquery-1.12.4.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/bootstrap.min.js') }}"></script>       
        <script type="text/javascript" src="{{ URL::asset('js/impl.js?v001') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/typeahead.bundle.js') }}"></script>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
            <script src="{{ URL::asset('js/html5shiv.js') }}"></script>
            <script src="{{ URL::asset('js/respond.min.js') }}"></script>
          <![endif]-->
        
    </head>

    <body style="margin:0px;padding:0px;overflow:hidden">
        <iframe src="{{$url}}" 
                frameborder="0" allowFullScreen="true"

                style="overflow:hidden; overflow-x:hidden; overflow-y:hidden; position:absolute; top:0px;left:0px;right:0px;bottom:0px; " 
                height="100%" width="100%"></iframe>
    </body>
</html>

