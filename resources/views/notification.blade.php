<?php
use \App\Http\Controllers\NotificationController as Notification; ?>
@extends('Layouts.layout')

@section('js-libs')
<script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.es.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/chart.bundle.js') }}"></script>
@stop

@section('content')

<div class="row">
    <div class="col-md-4">
        <div class="x_panel">
            <div class="x_title">
                <h2>Categorias</h2>
                <ul class="nav navbar-right panel_toolbox"></ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content form-horizontal">
                <ul class="nav nav-pills nav-stacked">
                @foreach($catalogo as $categoria)
                   <li {{$categoria->ORDEN=='1'?'class="active"':''}}> 
                       <h2 class="categoriaNotificacion">
                           {{($categoria->CATEGORIA).' ('.($categoria->NOTIFICACIONES_PENDIENTES).')'}}
                        </h2>
                    </li>
                @endforeach
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="x_panel">
            <div class="x_content form-horizontal">
                <div class="row"></div>
                <div class="row">
                    <table class="table table-striped table-bordered jambo_table" id="datatables">
                        <thead>
							<tr>
								<th style="text-align: center;color: white;">Mensaje</th>
                                <th style="text-align: center;color: white;">Fecha</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
               </div>
            </div>
        </div>
    </div>
</div>
<style>
    #datatables_wrapper > .row{
        width: 100%;
        padding: 13px;
    }
    .categoriaNotificacion{
        padding: 20px;
        cursor: pointer;
        font-family: Omnes Medium;
        background-color:white;
        margin:0 !important;
        font-size: 20px;
    }
    .categoriaNotificacion:hover{
        -webkit-filter: brightness(95%) !important;
        -webkit-transition: all 0.7s ease !important;
        -moz-transition: all 0.7s ease !important;
        -o-transition: all 0.7s ease !important;
        -ms-transition: all 0.7s ease !important;
        transition: all 0.7s ease !important;
    }
</style>
@stop

@section('js-scripts')
<script>
    $(document).ready(function() {
        var categoriaIni = 'Atrasos';
        // '{{$catalogo->count()>0?$catalogo[0]->CATEGORIA:null}}';
        call_notificaciones(null,categoriaIni,null,null,null,null);
    });
    function call_notificaciones(modulo,categoria,contenido,leido,fechaIni,fechaFin){
        if ($.fn.dataTable.isDataTable('#datatables')) {
			$('#datatables').DataTable().destroy();
		}
		$('#datatables').DataTable({
			destroy:true,
            processing: true,
            "bAutoWidth": false, 
            rowId: 'staffId',
            serverSide: true,
			language: {
				"url": "{{ URL::asset('js/Json/Spanish.Json') }}"
			},
			ajax: {
				type: "POST",
				url: "{{ route('getNotificationsTOT') }}",
				data: function(data) {
					data.modulo = modulo ? modulo : null;
					data.categoria = categoria ? categoria : null;
					data.contenido = contenido ? contenido : null;
					data.leido = leido ? leido : null;
					data.fechaIni = fechaIni ? fechaIni : null;
					data.fechaFin = fechaFin ? fechaFin : null;
                },
            },
			"aLengthMenu": [
				[10, 20, -1],
				[10, 20, "Todo"]
            ],
            "order": [
                [1, "desc"] //orden de formar desc
            ],
			"iDisplayLength": 10,
			columnDefs: [
				{
					targets: 0,
                    name: 'CONTENIDO',
                    data: 'CONTENIDO',
                    searchable: true,
					render: function(data, type, row) {
                        var result = '';
                        if(row.URL==null){
                            result = row.CONTENIDO
                        }else{
                            result = '<a href="'+ APP_URL+row.URL+'">'+ row.CONTENIDO+'</a>'
                        }
						return result
					}
				},
				{
					targets: 1,
                    name: 'FECHA_NOTIFICACION',
                    data: 'FECHA_NOTIFICACION',
                    searchable: false,
					sortable: false,
					render: function(data, type, row) {
						return row.FECHA_NOTIFICACION;
					}
				}
			]
		});
    }
</script>
@stop