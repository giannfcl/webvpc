         @extends('layouts.layout')
 
<style> 
td.details-control {
    background: fa-plus no-repeat center center;
    cursor: pointer;
}
tr.shown td.details-control {
    background: fa-plus no-repeat center center;
}

table.dataTable thead .sorting_asc {
/* background-image: url("../images/sort_asc.png"); */
background-color: white !important;
}
table.dataTable thead .sorting {
/* background-image: url("../images/sort_asc.png"); */
background-color: white !important;
}
</style>             
    @section('content')
        <div class="" role="main" >
  
            <div class="page-title">
              <div class="title_left">
                <h1>Mi Prospecto</h1>
              </div>
            </div>


            <div class="row">
              <div class="clearfix"></div>
                <div class="col-md-8 col-sm-12 col-xs-12">
                    <form class="form-verrtrical"><!-- action="{{ route('bpe.campanha.asistente.leads.listar') }}"-->
                          <div class="form-group">
                              <label for="" class="control-label col-md-1">Documento:</label>
                              <div class="col-md-3">
                                <input class="form-control" type="text" name="lead" id="txtLead"> <!--<input class="form-control" type="text" name="lead" id="txtLead"> <!---->
                              </div>
                          </div>
                                  <div class="form-group">
                                      <label for="" class="control-label col-md-1">Razon social: </label>
                                      <div class="col-md-3">
                                      <select class="form-control" name="ejecutivo" id="cboEjecutivo">
                                          <option value="">---Todos----</option>
                                      </select>
                                      </div>
                                  </div>
                          <div class="form-group">
                          <button type="button" class="btn" id="btnLimpiar">Limpiar</button>
                          <button type="submit" class="btn btn-primary" style="border-color:#00a94f;; background-color:#00a94f;">Buscar</button>
                          <button type="submit" class="btn btn-primary" style="border-color:#00a94f;; background-color:#00a94f;" id="btnfiltros">Mas Filtros</button>               
                      </div>
                  </form>
                </div>
                   <div class="clearfix"></div>
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">

                  <div class="x_content">
                    
                    <div class="table-responsive">
                      <table id="prospectos" class="display" width="100%" cellspacing="0">
                          <thead>
                              <tr>
                                  <th>Tipo</th>
                                  <th>Asignacion</th>
                                  <th>Estrategia</th>
                                  <th>Cliente</th>
                                  <th>Deuda Directa</th>
                                  <th>Deuda Indirecta</th>
                                  <th>Gestion</th>
                                  <th>Comentarios</th>
                                  <th>Semaforo</th>
                                  <th>Detalles</th>
                              </tr>
                          </thead>
                          <tfoot>
                              <tr>
                                  <th>Tipo</th>
                                  <th>Asignacion</th>
                                  <th>Estrategia</th>
                                  <th>Cliente</th>
                                  <th>Deuda Directa</th>
                                  <th>Deuda Indirecta</th>
                                  <th>Gestion</th>
                                  <th>Comentarios</th>
                                  <th>Semaforo</th>
                                  <th>Detalles</th>
                              </tr>
                          </tfoot>
                          <tbody>
                          </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
<script> 
$(document).ready(function() {

    $('#btnfiltros').click(function(){
        $('#prospectos thead th').each( function () {
        var title = $(this).text();
        if(title != 'Semaforo' && title != 'Detalles'&& title != 'Comentarios')
        $(this).html( '<input class="col-xs" type="text" placeholder="Buscar '+title+'" />' );
    } );

    });

    $('#prospectos').DataTable( {
        "search":     false,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        }
    } );
        table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );
} );
</script>
<!-- /page content -->
		 @stop
