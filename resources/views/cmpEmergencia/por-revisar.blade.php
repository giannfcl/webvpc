@extends('cmpEmergencia.menu')

@section('datable')

<div>
    <table class='table table-striped table-bordered jambo_table' id="datatable">
        <thead>
            <tr>
                <th scope="col" style="font-size: 12.5px;">Canal</th>
                <th scope="col" style="font-size: 12.5px;">Empresa</th>
                <th scope="col" style="font-size: 12.5px;">Desembolso RP1</th>
                <th scope="col" style="font-size: 12.5px;">EN Sectorizado / Zonal</th>
                <th scope="col" style="font-size: 12.5px;">Ventas<br>Info referencial</th>
                <th scope="col" style="font-size: 12.5px;">Monto Solicitado</th>
                <th scope="col" style="font-size: 12.5px;">Motivo<br>Oferta BPE</th>
                <th scope="col" style="font-size: 12.5px;">Gestionado</th>
                <th scope="col" style="font-size: 12.5px;">Detalle Gestión</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>


<div class="modal fade" tabindex="-1" role="dialog" id="modalGestion">
    <div class="modal-dialog" role="document" style="width: 1200px;">
        <div class="modal-content">
            <div class="modal-body">
                <form id="formGestionar" method="POST" class="form-horizontal form-label-left" enctype="multipart/form-data" action="{{route('cmpEmergencia.cmpgestionar')}}">
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		                <h4 class="modal-title">Gestionar</h4>
		            </div>
		            <br>
                    <input hidden="hidden" name="etapa_actual" value="{{$request['etapa']}}">
                    <div class="row">
						<div class="col-sm-6">
							<div class="row">
								<div class="form-group">
									<label class="control-label col-sm-4 col-xs-12" style="text-align: left;">RUC:</label>
									<div class="col-sm-8 col-xs-12" style="padding-left:0px">
										<input autocomplete="off" class="form-control"  type="text" id="gnumruc" name="gnumruc" readonly>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Nombre:</label>
									<div class="col-sm-8 col-xs-12" style="padding-left:0px">
										<input autocomplete="off" class="form-control"  type="text" id="gcliente" readonly>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Teléfono:</label>
									<div class="col-sm-8 col-xs-12" style="padding-left:0px">
										<input autocomplete="off" class="form-control input-number"  type="text" id="gtelefono" name="gtelefono" maxlength="9">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Correo:</label>
									<div class="col-sm-8 col-xs-12" style="padding-left:0px">
										<input autocomplete="off" class="form-control"  type="text" id="gcorreo" name="gcorreo">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Ventas:</label>
									<div class="col-sm-8 col-xs-12" style="padding-left:0px">
										<input autocomplete="off" class="form-control"  type="text" name="gventasa" id="gventasa" onkeypress="return filterFloat(event,this);" onpaste="return filterFloat(event,this);">
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="row">
								<div class="form-group">
									<label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Deuda MicroEmpresa:</label>
									<div class="col-sm-8 col-xs-12" style="padding-left:0px">
										<input autocomplete="off" class="form-control"  type="text" name="gDeudaMicroempresa" id="gdeudaMicroempresa" readonly>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-4 col-xs-12" style="text-align: left;">¿TIENE DESEMBOLSO RP1?:</label>
		                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
		                                <select class="form-control" name="gflgReactiva1" id="gflgReactiva1">
		                                    <option value="">--Opciones--</option>
		                                    <option value="0">No</option>
		                                    <option value="1" disabled>Sí(Interbank)</option>
		                                    <option value="2">Sí(Otro banco)</option>
		                                </select>
		                            </div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Banco dondé tomo RP1:</label>
									<div class="col-sm-8 col-xs-12" style="padding-left:0px">
										<select class="form-control" name="bancor1" id="bancor1">
											<option value="">--Opciones--</option>
											@foreach($bancos as $banco)
												<option value="{{$banco->CODIGO}}">{{$banco->DESCRIPCION}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Desembolso RP1 S/.:</label>
									<div class="col-sm-8 col-xs-12" style="padding-left:0px">
										<input autocomplete="off" class="form-control"  type="text" name="gmontoReactiva1" id="gmontoReactiva1" onkeypress="return filterFloat(event,this);" onpaste="return filterFloat(event,this);" maxlength="18">
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Monto Disponible RP2 S/.:</label>
									<div class="col-sm-5 col-xs-8" style="padding-left:0px">
										<input autocomplete="off" class="form-control"  type="text" name="gmontocalculado" id="gmontocalculado" readonly>
									</div>
									<div class="col-sm-3 col-xs-4" style="padding-left:0px">
										<input autocomplete="off" class="form-control"  type="text" name="gcoberturacalculado" id="gcoberturacalculado" readonly>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-header">
		                <h4 class="modal-title">Reactiva 2</h4>
		            </div>
		            <br>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label class="control-label col-sm-5 col-xs-12"  style="text-align: left;">Detalle de Gestión :</label>
								<div class="col-sm-6 col-xs-12" style="padding-left:0px">
									<select class="form-control" id="ggestion" name="ggestion" required>
										<option selected value="">Elige una opción</option>
										@foreach ($gestiones as $gestion)
												<option value="{{ $gestion->GESTION_ID }}">{{ $gestion->GESTION_NOMBRE }}</option>
										@endforeach
									</select>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-5 col-xs-12"  style="text-align: left;">Monto Solicitado S/.:</label>
								<div class="col-sm-4 col-xs-8" style="padding-left:0px">
									<input autocomplete="off" class="form-control"  type="text" name="gmontosolicitado" id="gmontosolicitado" onkeypress="return filterFloat(event,this);" onpaste="return filterFloat(event,this);">
								</div>
								<div class="col-sm-2 col-xs-4" style="padding-left:0px">
									<input autocomplete="off" class="form-control"  type="text" name="gcoberturasolicitada" id="gcoberturasolicitada" readonly>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-5 col-xs-12"  style="text-align: left;">Plazo / Gracia:</label>
								<div class="col-sm-6 col-xs-12" style="padding-left:0px">
									<select autocomplete="off" class="form-control" id="gplazogracia" name="gplazogracia">
										<option value="">Seleccionar una Opción</option>
										@foreach($plazosgracias as $plazogracia)
											<option value="{{$plazogracia[0]}}">{{$plazogracia[1]}}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Oferta Riesgos S/.:</label>
								<div class="col-sm-6 col-xs-12" style="padding-left:0px">
									<input autocomplete="off" class="form-control"  type="text" name="gmontosriesgo" id="gmontosriesgo" readonly>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Riesgo Descubierto:</label>
								<div class="col-sm-6 col-xs-12" style="padding-left:0px">
									<input autocomplete="off" class="form-control"  type="text" name="griesgoDescubierto" id="griesgoDescubierto" readonly>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Comentario</label>
								<div class="col-sm-6 col-xs-12" style="padding-left:0px">
									<textarea class="form-control"  type="text" name="gcomentario" id="gcomentario"></textarea>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-header">
		                <h4 class="modal-title">
		                	Reactiva 1 <span class="r1-icon glyphicon glyphicon-chevron-down" style="cursor: pointer" aria-hidden="true"></span>
		                </h4>
		            </div>
		            <br>
					<div class="row r1-area hidden">
						<div class="col-sm-6">
							<div class="form-group">
								<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Etapa:</label>
								<div class="col-sm-6 col-xs-12" style="padding-left:0px">
									<input autocomplete="off" class="form-control"  type="text" name="gr1etapa" readonly>
								</div>
							</div>
							<div class="form-group fg-r1-desembolsado">
								<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Tasa:</label>
								<div class="col-sm-6 col-xs-12" style="padding-left:0px">
									<input autocomplete="off" class="form-control"  type="text" name="gr1tasa" readonly>
								</div>
							</div>
							<div class="form-group fg-r1-desembolsado">
								<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Monto:</label>
								<div class="col-sm-6 col-xs-12" style="padding-left:0px">
									<input autocomplete="off" class="form-control"  type="text" name="gr1monto" readonly>
								</div>
							</div>
							<div class="form-group fg-r1-desembolsado">
								<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Garantía:</label>
								<div class="col-sm-6 col-xs-12" style="padding-left:0px">
									<input autocomplete="off" class="form-control"  type="text" name="gr1garantia" readonly>
								</div>
							</div>
							<div class="form-group fg-r1-no-desembolsado">
								<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Fecha Contacto:</label>
								<div class="col-sm-6 col-xs-12" style="padding-left:0px">
									<input autocomplete="off" class="form-control"  type="text" name="gr1fechaContacto" readonly>
								</div>
							</div>
							<div class="form-group fg-r1-no-desembolsado">
								<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Plazo:</label>
								<div class="col-sm-6 col-xs-12" style="padding-left:0px">
									<input autocomplete="off" class="form-control"  type="text" name="gr1plazo" readonly>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group fg-r1-desembolsado">
								<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Plazo:</label>
								<div class="col-sm-6 col-xs-12" style="padding-left:0px">
									<input autocomplete="off" class="form-control"  type="text" name="gr1plazo" readonly>
								</div>
							</div>
							<div class="form-group fg-r1-desembolsado">
								<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Gracia:</label>
								<div class="col-sm-6 col-xs-12" style="padding-left:0px">
									<input autocomplete="off" class="form-control"  type="text" name="gr1gracia" readonly>
								</div>
							</div>
							<div class="form-group fg-r1-desembolsado">
								<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Riesgo Descubierto:</label>
								<div class="col-sm-6 col-xs-12" style="padding-left:0px">
									<input autocomplete="off" class="form-control"  type="text" name="gr1riesgoDescubierto" readonly>
								</div>
							</div>
							<div class="form-group fg-r1-no-desembolsado">
								<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Monto Solicitado:</label>
								<div class="col-sm-6 col-xs-12" style="padding-left:0px">
									<input autocomplete="off" class="form-control"  type="text" name="gr1monto" readonly>
								</div>
							</div>
						</div>
					</div>
                    <center>
                        <button class="btn btn-success guardargestion" type="submit">Guardar</button>
                    </center>
                </form>
            </div>
        </div>
    </div>
</div>

@stop

<style>
	div.modal-header{
		padding: 3px !important;
	}
	.modal-body {
		max-height: calc(95vh - 75px);
		overflow-y: auto;
	}
</style>

@section('js-vista')
<script>

	$('.r1-icon').click(function(){
		$(this).toggleClass('glyphicon-chevron-down').toggleClass('glyphicon-chevron-up');
		$('.r1-area').toggleClass('hidden');
	});

    configurarTabla("{{$request['etapa']}}");

    var gestiones = <?php echo json_encode($gestiones); ?>;
    // console.log(gestiones);
    // ids que inahbilitaran plazo
    var ids = ['5'];

    function ValidarMontos(monsol,moncal) {

        var vmonsol = numeral(monsol).value();
        var vmoncal = numeral(moncal).value();
        // console.log(vmonsol);
        // console.log(vmoncal);
        if(vmonsol>vmoncal){
            return true;
        }else{
            return false;
        }
    }

    $(document).on("click",".gestionar",function(){
        $("#modalGestion").modal();
        ruc = $(this).attr('numruc');
        if(ruc){
            $.ajax({
                type: "GET",
                data: {
                    numruc: ruc
                },
                url: '{{route("cmpEmergencia.buscarinfo")}}',
                beforeSend: function() {
                    $("#cargando").modal();
                },
                success: function(result) {
                    // console.log(result);
                    if(result){
						$("#gnumruc").val(result['NUM_RUC']);
						$("#gcliente").val(result['NOMBRE']);
						$("#gtelefono").val(result['TELEFONO1']);
						$("#gcorreo").val(result['CORREO']);
						$("#gventasa").val(numeral(result['SOLICITUD_VOLUMEN_VENTA']).format('0'));
						$("#gtasaTentativa").val(result['SOLICITUD_TASA'])
						$("#gflgReactiva1").val(result['FLG_TOMO_REACTIVA_1']);
						$("#bancor1").val(result['BANCO_REACTIVA_1']);
						$("#gdeudaMicroempresa").val(numeral(result['SOLICITUD_DEUDA_MICRO_PROM']).format('0,0'))
						$("#ggestion").val(result['CONTACTO_RESULTADO']);
						$("#gcomentario").val(result['CONTACTO_COMENTARIO']);
						$("#gmontosriesgo").val(numeral(result['RIESGOS_MONTO']).format('0,0'));


						if(result['SOLICITUD_GRACIA']==null || result['SOLICITUD_PLAZO']==null){
							$("#gplazogracia").val("");
						}else{
							$("#gplazogracia").val(result['SOLICITUD_PLAZO']+"-"+result['SOLICITUD_GRACIA']);
						}

						//CARGA DATOS REACTIVA 1
						if (result['R1_ETAPA_ACTUAL'] == '7'){
							$('.fg-r1-desembolsado').removeClass('hidden');
							$('.fg-r1-no-desembolsado').addClass('hidden');
						}else{
							$('.fg-r1-desembolsado').addClass('hidden');
							$('.fg-r1-no-desembolsado').removeClass('hidden');
						}
						$('#formGestionar input[name="gr1etapa"]').val(result['R1_ETAPA_NOMBRE']||'-')
						$('#formGestionar input[name="gr1fechaContacto"]').val(result['R1_FECHA_CONTACTO']||'-')
						$('#formGestionar input[name="gr1gracia"]').val(result['R1_GRACIA']||'-')
						$('#formGestionar input[name="gr1monto"]').val(numeral(result['R1_MONTO']).format('0,0'))
						$('#formGestionar input[name="gr1plazo"]').val((result['R1_PLAZO']||'-') + ' meses')
						$('#formGestionar input[name="gr1tasa"]').val(result['R1_TASA']||'-')

						garantia = getCoberturaMonto(numeral(result['R1_MONTO']).value())
						$('#formGestionar input[name="gr1garantia"]').val(garantia*100 + '%')
						$('#formGestionar input[name="gr1riesgoDescubierto"]').val('S/. ' + numeral((1-garantia)*result['R1_MONTO']).format('0,0'))

						//Flag de Si tomo Reactiva 1
						$("#gmontoReactiva1").val(result['MONTO_PRESTAMO_REACTIVA_1']);
						ComboFlgreactiva1(result['FLG_TOMO_REACTIVA_1']);

						//Logica Monto disponible
						montoReactiva1 = numeral($('#gmontoReactiva1').val());
						resultado = calculoReactiva2(result['SOLICITUD_VOLUMEN_VENTA'],result['SOLICITUD_DEUDA_MICRO_PROM'],montoReactiva1.value());
						montoDisponible = resultado[0];
						coberturaCal = resultado[1];
						$('#gmontocalculado').val(numeral(montoDisponible).format('0,0'));
						$('#gcoberturacalculado').val(coberturaCal*100 + '%');
						montoSolicitado = numeral(result['SOLICITUD_OFERTA']);
						// Solicitud: Monto, cobertura y riesgo descubierto
						if (montoSolicitado.value() <= 0 ){
							montoSolicitado = numeral(montoDisponible);
						}
						$("#gmontosolicitado").val(montoSolicitado.format('0,0'));
						coberturaSol = getCoberturaMonto(montoSolicitado.value() + montoReactiva1.value());
						$('#griesgoDescubierto').val('S/. ' + numeral((1-coberturaSol)*montoSolicitado.value()).format('0,0'));
						$('#gcoberturasolicitada').val(coberturaSol*100 + '%');
					}
                },
                complete: function() {
                    $(".cerrarcargando").click();
                }
            });
        }
    });

    function actualizarSolicitud(){
		montoSolicitado = numeral($("#gmontosolicitado").val());
		montoReactiva = numeral($("#gmontoReactiva1").val());
		coberturaSol = getCoberturaMonto(montoSolicitado.value() + montoReactiva.value())
		$('#gcoberturasolicitada').val(coberturaSol*100 + '%')
		$('#formGestionar input[name="griesgoDescubierto"]').val('S/. ' + numeral((1-coberturaSol)*montoSolicitado.value()).format('0,0'))
	}

	function actualizarCalculado(){
		result = calculoReactiva2(
			numeral($("#gventasa").val()).value(),
			numeral($("#gdeudaMicroempresa").val()).value(),
			numeral($('#gmontoReactiva1').val()).value()
		)
		montoDisponible = result[0]
		cobertura = result[1]
		$('#gmontocalculado').val(numeral(montoDisponible).format('0,0'));
		$('#gcoberturacalculado').val(cobertura*100 + '%')
	}

	$("#gventasa").keyup(function() {
		actualizarCalculado()
	});

	$("#gmontosolicitado").keyup(function(event) {
		actualizarCalculado()
	});

	$("#gmontoReactiva1").keyup(function(event) {
		actualizarCalculado()
		actualizarSolicitud()
	});

	$("#gflgReactiva1").change(function(event) {
		ComboFlgreactiva1($(this).val());
		actualizarCalculado()
		actualizarSolicitud()
	});

    function configurarTabla(datos=null){
        if ($.fn.dataTable.isDataTable('#datatable')) {
            $('#datatable').DataTable().destroy();
        }

        $('#datatable').DataTable({
            processing: true,
            "bAutoWidth": false,
            rowId: 'staffId',
            // dom: 'Blfrtip',
            serverSide: true,
            language: {"url": "{{ URL::asset('js/Json/Spanish.json') }}"},
            ajax: {
                    "type": "GET",
                    "url": "{{ route('cmpEmergencia.lista') }}",
                    data: function(data) {
                        data.etapa = datos;
                    }
            },
            "aLengthMenu": [
                    [25, 50, -1],
                    [25, 50, "Todo"]
            ],
            "iDisplayLength": 25,
            "order": [
                    [0, "desc"]
            ],
            columnDefs: [
                {
                    targets: 0,
                    data: 'WCL.CANAL_ACTUAL',
                    name: 'WCL.CANAL_ACTUAL',
                    searchable: false,
                    render: function(data, type, row) {
                        return row.CANAL_ACTUAL;
                    }
                },
                {
                    targets: 1,
                    data: 'WCL.NUM_RUC',
                    name: 'WCL.NUM_RUC',
                    render: function(data, type, row) {
                        $linea1="<span>"+row.NOMBRE+"</span><br>";
                        $linea2="<span>"+row.NUM_RUC+"</span>";
                        return $linea1+$linea2;
                    }
                },
                {
                    targets: 2,
                    data: null,
                    searchable: false,
                    sortable: false,
                    render: function(data, type, row) {
                        return "<span>S/. "+numeral(row.MONTO_PRESTAMO_REACTIVA_1).format('0,0')+"</span><br>";
                    }
                },
                {
                    targets: 3,
                    data: null,
                    searchable: false,
                    sortable: false,
                    render: function(data, type, row) {
                        $linea1="<span>Sectorista: "+(row.SECTORISTA_NOMBRE|| '-')+"</span><br>";
                        $linea2="<span>Zonal: "+(row.NOMBRE_ZONAL || '-')+"</span>";
                        return $linea1+$linea2;
                    }
                },
                {
                    targets: 4,
                    data: null,
                    searchable: false,
                    sortable: false,
                    render: function(data, type, row) {
                        $linea1="<span>S/. "+numeral(row.SOLICITUD_VOLUMEN_VENTA).format('0,0')+"</span><br>";
						//$linea2="<span>Aporte: S/. "+numeral(row.SOLICITUD_ESSALUD).format('0,0')+"</span>";
						return $linea1;
                    }
                },
                {
                    targets: 5,
                    data: null,
                    searchable: false,
                    sortable: false,
                    render: function(data, type, row) {
                        if (row.SOLICITUD_OFERTA==null) {
                            return "<span>Volumen: S/. "+numeral(row.PRODUCTO_MONTO).format('0,0')+"</span><br>";
                        }else{
                            return "<span>Volumen: S/. "+numeral(row.SOLICITUD_OFERTA).format('0,0')+"</span><br>";
                        }
                    }
                },
                {
                    targets: 6,
                    data: null,
                    searchable: false,
                    sortable: false,
                    render: function(data, type, row) {
                        if (row.MOTIVO_OFERTA_BPE) {
                            return row.MOTIVO_OFERTA_BPE;
                        }
                        return "-";
                    }
                },
                {
                    targets: 7,
                    data: null,
                    searchable: false,
                    sortable: false,
                    render: function(data, type, row) {
                        if(row.CONTACTO_RESULTADO==null)
                            return "No";
                        else
                            return "Si";
                    }
                },
                {
                    targets: 8,
                    data: null,
                    searchable: false,
                    sortable: false,
                    render: function(data, type, row) {
                        if(row.GESTION_NOMBRE==null){
                            $html="<u><a class='gestionar' style='cursor: pointer' numruc='"+row.NUM_RUC+"'>Pendiente</a></u>";
                        }else{
                            $html="<u><a class='gestionar' style='cursor: pointer' numruc='"+row.NUM_RUC+"'>"+row.GESTION_NOMBRE+"</a></u>";
                        }
                        return $html;
                    }
                },
                {
                    targets: 9,
                    data: 'WCL.NOMBRE',
                    name: 'WCL.NOMBRE',
                    visible: false,
                    searchable: true,
                    render: function(data, type, row) {
                            return row.NOMBRE;
                    }
                }
            ]
        });
    }
</script>
@stop
