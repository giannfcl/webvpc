@extends('cmpEmergencia.menu')

@section('datable')

<div>
  <table class='table table-striped table-bordered jambo_table' id = 'datatable'>
    <thead>
      <tr>
        <th>Empresa</th>
        <th>Flujo</th>
        <th>Ventas / ESSALUD</th>
        <th>Oferta Solicitada</th>
        <th>Documentación Completa</th>
        <th>Acciones</th>
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modalEvaluacion">
        <div class="modal-dialog" role="document" style="width: 900px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Validacion</h4>
                </div>
                <div class="modal-body">
                <form id="formEvaluacion"  method = 'POST' class="form-horizontal form-label-left" enctype="multipart/form-data" action="{{route('cmpEmergencia.enviar-gtp')}}">
    
                    <div class="form-group add">
                        <div class="col-sm-6" >
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">RUC: </label>
                            <div class="input-group col-md-9 col-sm-9 col-xs-12">
                                <input autocomplete="off" class="form-control"  type="text" name="numruc" readonly>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Teléfono:</label>
                                <div class="input-group col-md-9 col-sm-9 col-xs-12">
                                    <input autocomplete="off" class="form-control"  type="text" name="telefono" readonly>
                                </div>
                        </div>
                    </div>
    
                    <div class="form-group add">
                            <div class="col-sm-6">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Empresa:</label>
                                <div class="input-group col-md-9 col-sm-9 col-xs-12">
                                    <input autocomplete="off" class="form-control"  type="text" name="cliente" readonly>
                                </div>
                            </div>
            
                            <div class="col-sm-6">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Correo:</label>
                                    <div class="input-group col-md-9 col-sm-9 col-xs-12">
                                        <input autocomplete="off" class="form-control"  type="text" name="telefono" readonly>
                                    </div>
                            </div>
                    </div>
    
                    <hr style="width:100%;">

                    <div class="form-group add">
                        <div class="col-sm-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Monto Solicitado Cliente S/: </label>
                            <div class="input-group col-md-9 col-sm-9 col-xs-12">
                                <input autocomplete="off" class="form-control"  type="text" name="soloferta" readonly onkeypress="return filterFloat(event,this);" maxlength="11">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Plazo (meses): </label>
                            <div class="input-group col-md-9 col-sm-9 col-xs-12">
                                <input autocomplete="off" class="form-control"  type="text" name="solplazo" readonly>
                            </div>
                        </div>
                    </div>

                    <div class="form-group add">
                        <div class="col-sm-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Monto Calculado S/: </label>
                            <div class="input-group col-md-9 col-sm-9 col-xs-12">
                                <input autocomplete="off" class="form-control"  type="text" name="montoMax" readonly>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Oferta Riesgos S/: </label>
                            <div class="input-group col-md-9 col-sm-9 col-xs-12">
                                <input autocomplete="off" class="form-control"  type="text" name="riesgosMonto" readonly>
                            </div>
                        </div>
                    </div>

                    <hr style="width:100%;"> 

                    <div class="form-group add">
                        <div class="col-sm-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Documentos Completos: </label>
                            <div class="input-group col-md-9 col-sm-9 col-xs-12">
                                <select class="form-control" name="flgcompleto" required>
                                    <option disabled selected value="">Seleccionar Opción</option>
                                    <option value="1">SI</option>
                                    <option value="0">NO</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Vigencia Poderes</label>
                            <div class="input-group col-md-9 col-sm-9 col-xs-12">
                                <input autocomplete="off" class="form-control docDetalle"  type="text" value="-" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-group add">
                        <div class="col-sm-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Pagaré</label>
                            <div class="input-group col-md-9 col-sm-9 col-xs-12">
                                <input autocomplete="off" class="form-control docDetalle"  type="text" value="-" readonly>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Ficha RUC y DNI de RL</label>
                            <div class="input-group col-md-9 col-sm-9 col-xs-12">
                                <input autocomplete="off" class="form-control docDetalle"  type="text" value="-" readonly>
                            </div>
                        </div>
                    </div>

                    <div class="form-group add">
                        <div class="col-sm-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Acuerdo</label>
                            <div class="input-group col-md-9 col-sm-9 col-xs-12">
                                <input autocomplete="off" class="form-control docDetalle"  type="text" value="-" readonly>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">IBR y EEFF</label>
                            <div class="input-group col-md-9 col-sm-9 col-xs-12">
                                <input autocomplete="off" class="form-control docDetalle"  type="text" value="-" readonly>
                            </div>
                        </div>
                    </div>

                    <div class="form-group add">
                        <div class="col-sm-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Carta sol. crédito</label>
                            <div class="input-group col-md-9 col-sm-9 col-xs-12">
                                <input autocomplete="off" class="form-control docDetalle"  type="text" value="-" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-group add">
                        <div class="col-sm-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">ESSALUD/ PDT o DJ</label>
                            <div class="input-group col-md-9 col-sm-9 col-xs-12">
                                <input autocomplete="off" class="form-control docDetalle"  type="text" value="-" readonly>
                            </div>
                        </div>
                    </div>

                    <div class="form-group add">
                        <div class="col-sm-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">File</label>
                            <div class="input-group col-md-9 col-sm-9 col-xs-12">
                                <input autocomplete="off" class="form-control docDetalle"  type="text" value="-" readonly>
                            </div>
                        </div>
                    </div>

                    <input type="hidden" name="flgGtp" value="0" />
                    
                    <center>
                        <button class="btn btn-success" type="submit">Guardar</button>

                        <button class="btngtp btn btn-success" type="submit" disabled>Enviar a GTP</button>
                    </center>

                    </form>
                </div>
            </div>
        </div>
    </div>

@stop

@section('js-vista')
<script>
  $(document).ready(function(){
		$(".nombre_"+"{{$request['etapa']}}").removeClass("btn-success");
		$(".nombre_"+"{{$request['etapa']}}").addClass("btn-danger");
		configurarTabla("{{$request['etapa']}}");
    });

$(document).on("click",".btngtp",function(){
    $('#formEvaluacion input[name="flgGtp"]').val(1);
});

$(document).on("change","#formEvaluacion select[name='flgcompleto']",function(){
    checkGtp();
});

function checkGtp(){
    if ($('#formEvaluacion select[name="flgcompleto"]').val() == 1){
        $('#formEvaluacion .docDetalle').val('SI');
        $( ".btngtp" ).prop( "disabled", false);
    }else{
        $('#formEvaluacion .docDetalle').val('NO');
        $( ".btngtp" ).prop( "disabled", true);
    }
}
        

$(document).on("click",".enviarGTP",function(){
        $("#modalEvaluacion").modal();
        //limpiar form/loading animation
        var ruc = $(this).attr('ruc')
        $.ajax({
                url: "{{ route('cmpEmergencia.buscarinfo') }}",
                type: 'GET',
                data: {
                    numruc: ruc
                },
                success: function (result) {
                    $('#formEvaluacion input[name="numruc"]').val(result['NUM_RUC']);
                    $('#formEvaluacion input[name="cliente"]').val(result['NOMBRE']);
                    $('#formEvaluacion input[name="telefono"]').val(result['TELEFONO1']);
                    //$('#formEvaluacion input[name="email"]').val(result['CORREO']);
                    //$('#formEvaluacion input[name="docessalud"]').val(result['DOCUMENTACION_ESSALUD']);
                    //$('#formEvaluacion input[name="djpdt"]').val(result['DOCUMENTACION_PDTDJ']);
                    //$('#formEvaluacion input[name="volventa"]').val('S/. ' + (result['SOLICITUD_VOLUMEN_VENTA']||'-'));
                    //$('#formEvaluacion input[name="essalud"]').val('S/. ' + (result['SOLICITUD_ESSALUD']||'-'));
                    $('#formEvaluacion input[name="soloferta"]').val('S/. ' + (result['SOLICITUD_OFERTA']||'-'));
                    $('#formEvaluacion input[name="solplazo"]').val((result['SOLICITUD_PLAZO']||'-'));
                    $('#formEvaluacion input[name="riesgosMonto"]').val('S/. ' + (result['RIESGOS_MONTO']||'-'));
                    $monto = recalcularMonto(result['SOLICITUD_VOLUMEN_VENTA'],result['SOLICITUD_ESSALUD']);
                    $('#formEvaluacion input[name="montoMax"]').val('S/. ' + $monto);
                    $('#formEvaluacion select[name="flgcompleto"]').val(result['DOCUMENTACION_COMPLETA']||'0');
                    checkGtp();
                },
                error: function (xhr, status, text) {
                    
                    Swal.fire('Hubo un error al registrar el dato de contacto, inténtelo mas tarde');
                }
            });
    });

  function configurarTabla(datos=null){
        if ($.fn.dataTable.isDataTable('#datatable')) {
                $('#datatable').DataTable().destroy();
        }

        $('#datatable').DataTable({
            processing: true,
            "bAutoWidth": false,
            rowId: 'staffId',
            // dom: 'Blfrtip',
            serverSide: true,
            language: {"url": "{{ URL::asset('js/Json/Spanish.json') }}"},
            ajax: {
                    "type": "GET",
                    "url": "{{ route('cmpEmergencia.lista') }}",
                    data: function(data) {
                            data.etapa = datos;
                    }
            },
            "aLengthMenu": [
                    [25, 50, -1],
                    [25, 50, "Todo"]
            ],
            "iDisplayLength": 25,
            "order": [
                    [0, "desc"]
            ],
            columnDefs: [
                {
                        targets: 0,
                        data: 'WCL.NUM_RUC',
                        name: 'WCL.NUM_RUC',
                        searchable: true,
                        render: function(data, type, row) {
                                return row.NUM_RUC + '<br/>' + row.NOMBRE;
                        }
                    },
                    {
                        targets: 1,
                        data: null,
                        searchable: false,
                        render: function(data, type, row) {
                                return row.FLUJO;
                        }
                    },
                    {
                        targets: 2,
                        data: null,
                        searchable: false,
                        render: function(data, type, row) {
                                return 'Ventas Anuales: S/ ' + (row.SOLICITUD_VOLUMEN_VENTA||'-')
                                +'<br> ESSALUD: S/ ' + (row.SOLICITUD_ESSALUD||'-');
                        }
                    },
                    {
                        targets: 3,
                        data: null,
                        searchable: false,
                        render: function(data, type, row) {
                                return 'S/ ' + (row.SOLICITUD_OFERTA||'-')
                        }
                    },
                    {
                        targets: 4,
                        data: null,
                        searchable: false,
                        render: function(data, type, row) {
                                if (row.DOCUMENTACION_COMPLETA == '1'){
                                        return 'SI';
                                }else{
                                        return 'NO';
                                }
                        }
                    },
                    {
                        targets: 5,
                        data: null,
                        searchable: false,
                        render: function(data, type, row) {
                                return '<button class="btn btn-sm btn-primary enviarGTP" ruc="' + row.NUM_RUC + '">Enviar GTP</button>';
                        }
                    },       
            ]
        });
    }
</script>
@stop
