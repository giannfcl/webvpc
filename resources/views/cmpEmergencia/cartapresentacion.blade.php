<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--link href="{{ URL::asset('css/bt4/bootstrap.min.css') }}" rel="stylesheet" type="text/css"-->
        <title>Banca Pequeña Empresa</title>

    </head>
    <style type="text/css">
        body{
              margin:0;
              font-family:-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";
              font-size:1rem;
              font-weight:400;line-height:1.5;color:#212529;text-align:left;
              background-color:#fff;
              margin-left: 90px;
              margin-right: 90px;
              margin-top: 8px;
              margin-bottom: 8px;
        }
        .tablaTexto table {
            table-layout: auto !important;
            width: 100%;
        }

        .tablaTexto th, .myclass td, .myclass thead th, .myclass tbody td, .myclass tfoot td, .myclass tfoot th {
            width: auto !important;
            font-size: 7pt !important;
        }
    </style>

    <body style="text-align: justify;">
        <div style="text-align: left; font-size: 11px">{{$fecha}}</div>
        <section style="font-size: 10px">
            <br>
           <p style="text-align: left;">Señores</p>
           <p style="text-align: left;"><strong>Banco Internacional del Perú S.A.A – INTERBANK</strong></p>
           <p style="text-align: left;">Av. Carlos Villaran N° 140 – La Victoria.<br>
            Lima, Perú. - </p>
           <p style="text-align: left;"><strong>Presente.-</strong></p>
           <p style="text-align: justify;">
               A través de esta comunicación, desde la dirección electrónica registrada en la Solicitud de Crédito de Desembolso, y a través de los representantes debidamente autorizados, les envío en forma escaneada los siguientes documentos por el Crédito solicitado a través del Programa REACTIVA PERU (en adelante el “Programa”). <br>
            <ol style="list-style-type: lower-roman;" >
                <li style="text-align: justify;">   Acuerdo de Crédito Solicitud De Desembolso y Declaración Jurada debidamente firmado. </li>
                <li style="text-align: justify;">   Pagare incompleto debidamente firmado.</li>
                <li style="text-align: justify;">   Acuerdo de llenado debidamente firmado.</li>
            </ol>
           </p>
           <p style="text-align: justify; ">
            Asimismo, el Cliente autoriza a Interbank a modificar la tasa y monto del Crédito de acuerdo a la disponibilidad de los fondos del Programa y sus políticas internas de calificación, antes de solicitar la garantía a Corporación Financiera de Desarrollo S.A – COFIDE (en adelante “COFIDE”), siempre que las modificaciones beneficien al mismo Cliente.
           </p>
           <p style="text-align: justify;">
            El Cliente declara expresamente que se obliga a regularizar y presentar en original y debidamente firmados (i) el Acuerdo Crédito Solicitud De Desembolso y Declaración Jurada (ii) el Pagare y (iii) el Acuerdo de llenado, incluyendo cualquier modificación sobre la tasa y el monto del Crédito solicitado, máximo a los siete (7) días hábiles desde la recepción de la presente comunicación, caso contrario Interbank estará facultado a dejar sin efecto la operación y solicitar la devolución de garantía a COFIDE.  En este caso, el cliente quedará obligado a presentar la Carta de Desistimiento de Crédito firmada por sus representantes autorizados, según el procedimiento establecido por COFIDE y que el cliente declara conocer.
           </p>
            <p style="text-align: justify;">
            Si el incumplimiento del Cliente genera a Interbank, cualquier tipo de penalidad y/o sanción por COFIDE y/o el Banco Central de Reserva del Perú – BCRP, el Banco quedará facultado automáticamente a cargar y/o sobregirar en cualquiera de las cuentas que registre el cliente, el mismo importe por el que se haya sancionado al Banco, quedando inclusive facultado para abrir una nueva cuenta a nombre del cliente, para el mismo fin.
            </p>
            <p style="text-align: justify;">
            En consecuencia, el Cliente asume las responsabilidades que sean aplicables por el cumplimiento, la veracidad, exactitud de la información proporcionada y de las firmas que acompañan los documentos, eximiendo de cualquier responsabilidad Interbank.
            </p>
            <p style="text-align: justify;">
            Se deja expresa constancia que la presente declaración, se efectúa en tenor a lo dispuesto por el artículo 179° de la Ley General del Sistema Financiero y del Sistema de Seguros y Orgánica de la Superintendencia de Banca y Seguros N° 26702.
            </p>
            <br>
            <div style="text-align: left;font-size: 9px;">Firma: _______________________________________</div>
            <br>
            <table class="tablaTexto">
            <tr>
                <td style="font-size: 9px;">Razón Social y/o Persona Natural con Negocio (el “Cliente”): </td>
                <td style="width: 320px;border-bottom-style:solid;border-bottom-width:0.5px;font-size: 9px; padding-left: 3px;">
                    {{$acuerdos->NOMBRE}}
                </td>
            </tr>
            </table>
            <br>
            <table class="tablaTexto">
                <tr>
                    <td style="font-size: 9px;">Representa legal: </td>
                    <td style="width: 250px;border-bottom-style:solid;border-bottom-width:0.5px;font-size: 9px;">
                        @if(substr($acuerdos->NUM_RUC,0,2)==20)
                            {{$acuerdos->RRLL_APELLIDOS}} {{$acuerdos->RRLL_NOMBRES}}
                        @endif
                    </td>
                </tr>
            </table>
            <br>
            <table class="tablaTexto">
                <tr>
                    <td style="font-size: 9px;">RUC/ DNI: </td>
                    <td style="width: 250px;border-bottom-style:solid;border-bottom-width:0.5px;font-size: 9px;">
                        @if(substr($acuerdos->NUM_RUC,0,2)==20)
                            {{$acuerdos->NUM_RUC}}
                        @endif
                        @if(substr($acuerdos->NUM_RUC,0,2)!=20)
                              {{substr($acuerdos->NUM_RUC,2,8)}}
                        @endif
                    </td>
                </tr>
            </table>
        </section>
    </body>
    </html>
