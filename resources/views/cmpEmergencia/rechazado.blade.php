@extends('cmpEmergencia.menu')

@section('datable')

<div>
    <table class='table table-striped table-bordered jambo_table' id="datatable">
        <thead>
            <tr>
                <th>Empresa</th>
                <th>Flujo</th>
                <th>Oferta Rechazada</th>
                <th>Responsable de Rechazo</th>
                <th>Fecha de Rechazo</th>
                <th>Motivo</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>


@stop

@section('js-vista')
<script>
    configurarTabla("{{$request['etapa']}}");

    function configurarTabla(datos=null){
        if ($.fn.dataTable.isDataTable('#datatable')) {
            $('#datatable').DataTable().destroy();
        }

        $('#datatable').DataTable({
            processing: true,
            "bAutoWidth": false,
            rowId: 'staffId',
            // dom: 'Blfrtip',
            serverSide: true,
            language: {"url": "{{ URL::asset('js/Json/Spanish.json') }}"},
            ajax: {
                    "type": "GET",
                    "url": "{{ route('cmpEmergencia.lista') }}",
                    data: function(data) {
                            data.etapa = datos;
                    }
            },
            "aLengthMenu": [
                    [25, 50, -1],
                    [25, 50, "Todo"]
            ],
            "iDisplayLength": 25,
            "order": [
                    [0, "desc"]
            ],
            columnDefs: [
                {
                        targets: 0,
                        data: 'WCL.NUM_RUC',
					    name: 'WCL.NUM_RUC',
                        searchable: true,
                        render: function(data, type, row) {
                                return row.NUM_RUC + '<br/>' + row.NOMBRE;
                        }
                    },
                    {
                        targets: 1,
                        data: null,
                        searchable: false,
                        sortable:false,
                        render: function(data, type, row) {
                                return row.FLUJO;
                        }
                    },
                    {
                        targets: 2,
                        data: null,
                        searchable: false,
                        sortable:false,
                        render: function(data, type, row) {
                                return row.SOLICITUD_OFERTA;
                        }
                    },
                    {
                        targets: 3,
                        data: null,
                        searchable: false,
                        sortable:false,
                        render: function(data, type, row) {
                                return row.RECHAZO_RESPONSABLE;
                        }
                    },
                    {
                        targets: 4,
                        data: null,
                        searchable: false,
                        sortable:false,
                        render: function(data, type, row) {
                                return row.RECHAZO_FECHA;
                        }
                    },
                    {
                        targets: 5,
                        data: null,
                        searchable: false,
                        sortable:false,
                        render: function(data, type, row) {
                                return row.RECHAZO_MOTIVO;
                        }
                    },
                    {
                        targets: 6,
                        data: 'WCL.NOMBRE',
					    name: 'WCL.NOMBRE',
                        visible: false,
                        searchable: true,
                        render: function(data, type, row) {
                                return row.NOMBRE;
                        }
                    }       
            ]
        });
    }
</script>
@stop