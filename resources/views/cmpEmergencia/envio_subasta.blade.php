@extends('cmpEmergencia.menu')

@section('datable')

<table class='table table-striped table-bordered jambo_table' id="datatablesubastados">
  <thead>
    <tr>
      <th scope="col" style="font-size: 12.5px;">Empresa</th>
      <th scope="col" style="font-size: 12.5px;">Monto Solicitado / % Cobertura</th>
      <th scope="col" style="font-size: 12.5px;">Condiciones</th>
      <th scope="col" style="font-size: 12.5px;">Tasa</th>
      <th scope="col" style="font-size: 12.5px;">Estado Subasta</th>
      <th scope="col" style="font-size: 12.5px;">Detalle Gestión</th>
    </tr>
  </thead>
</table>
@stop

<!-- {{route('cmpEmergencia.bpe.gestionar')}} -->
<div class="modal fade" tabindex="-1" role="dialog" id="modalSubasta">
    <div class="modal-dialog" role="document" style="width: 1200px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Validación</h4>
            </div>
            <div class="modal-body">
                <form id="formSubasta" method="POST" class="form-horizontal form-label-left" enctype="multipart/form-data" action="{{route('cmpEmergencia.cmpgestionsubasta')}}">
                    <div class="form-group">
                        <div class="col-sm-6" >
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;" >RUC: </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px" >
                                <input autocomplete="off" class="form-control"  type="text" name="numruc" readonly>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Ventas Anuales S/.: </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                              <input autocomplete="off" class="form-control editar"  name="volventa" disabled onkeypress="return filterFloat(event,this);">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Empresa:</label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input autocomplete="off" class="form-control"  type="text" name="cliente" disabled>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Oferta Riesgos S/.: </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input autocomplete="off" class="form-control"  type="text" name="riesgosMonto" id="riesgosMonto" readonly>
                            </div>
                        </div>                        
                    </div>

                    <div class="form-group">
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Campaña: </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input autocomplete="off" class="form-control"  type="text" name="campanha" disabled>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">¿TIENE DESEMBOLSO RP1?:</label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                            <select class="form-control editar" name="gflgReactiva1" id="gflgReactiva1" disabled>
                                <option value="">--Opciones--</option>
                                <option value="0">No</option>
                                <option value="1" disabled>Sí(Interbank)</option>
                                <option value="2">Sí(Otro banco)</option>
                            </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Teléfono:</label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input autocomplete="off" class="form-control input-number"  type="text" name="telefono" disabled>
                            </div>
                        </div>
                        <div class="col-sm-6">
                          <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Banco dondé tomo RP1:</label>
                          <div class="col-sm-8   col-xs-12" style="padding-left:0px">
                            <select class="form-control editar" name="bancor1" id="bancor1" disabled>
                                <option value="">--Opciones--</option>
                                @foreach($bancos as $banco)
                                    <option value="{{$banco->CODIGO}}">{{$banco->DESCRIPCION}}</option>
                                @endforeach
                            </select>
                          </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Correo: </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input autocomplete="off" class="form-control"  type="text" name="email" disabled>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Desembolso RP1 :</label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input autocomplete="off" class="form-control editar"  name="gmontoReactiva1" id="gmontoReactiva1" readonly onkeypress="return filterFloat(event,this);" >
                            </div>
                        </div>
                    </div>

                    <div class="modal-header"  style="padding-left:0px;">
                        <h4 class="modal-title">Propuesta al Cliente</h4>
                    </div>
                    <br>

                    <div class="form-group">
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Monto Solicitado S/.: </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input autocomplete="off" class="form-control editar"  type="text" name="soloferta" id="soloferta" disabled onkeypress="return filterFloat(event,this);" maxlength="11">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Garantía: </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input autocomplete="off" class="form-control"  type="text" readonly id="garantia" name="garantia" >
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-6"></div>
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Plazo / Gracia:</label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <select autocomplete="off" class="form-control editar" id="plazogracia" name="plazogracia" disabled>
                                    <option value="">Seleccionar una Opción</option>
                                    @foreach($plazosgracias as $plazogracia)
                                        <option value="{{$plazogracia[0]}}">{{$plazogracia[1]}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Monto Garantizado : </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input autocomplete="off" class="form-control"  type="text" name="montogarantizado" id="montogarantizado" disabled>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Riesgo Descubierto : </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input autocomplete="off" class="form-control"  type="text" name="montoriesgoibk" id="montoriesgoibk" disabled>
                            </div>
                        </div>
                    </div>

                    <div class="modal-header"  style="padding-left:0px;">
                      <h4 class="modal-title">Confirmación de Subasta</h4>
                    </div>
                    <br>
                    <div class="form-group">
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Tasa max:</label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input autocomplete="off" class="form-control"  type="text" name="tasamax" readonly>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Tasa Final: </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input autocomplete="off" class="form-control"  type="text" name="tasafinal" required>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-6">
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Detalle Gestión: </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <select class="form-control" name="gestionsubasta" id="gestionsubasta" required>
                                    <option value="">Elige una Opción</option>
                                    @foreach($gestionessubasta as $gestionsubasta)
                                        <option value="{{$gestionsubasta['ID']}}">{{$gestionsubasta['NOMBRE']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-6">
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Comentario: </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <textarea class="form-control" name="comentariosubasta"></textarea>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-primary botoneditar" type="button" style="margin-left: 5%;" valor="1">Editar</button>
                    <button class="btn btn-success sologuardar" style="margin-left: 5%;">Guardar</button>
                    <button class="btn btn-success guardarsubasta" style="margin-left: 20%;">Confirmar Subasta</button>
                </form>
            </div>
        </div>
    </div>
</div>



@section('js-vista')
<style>
    #modalSubasta input,textarea,select{
        margin-bottom: -16px;
    }
    #modalSubasta div.modal-header{
        padding: 4px !important;
    }
    .tooltip-inner {
        max-width: 350px;
        /* If max-width does not work, try using width instead */
        width: 350px; 
    }
</style>
<script>
    tablasubastados("{{$request['etapa']}}"); //MANDEMOS ESA ETAPA EN QUE SE ENCUENTRE
    $('[data-toggle="tooltip"]').tooltip();
    var gestionessubasta = <?php echo json_encode($gestionessubasta); ?>;
	// console.log(gestionessubasta);
	var gsubasta={};
    $.each(gestionessubasta, function( i,v,a) {
        gsubasta[v["ID"]]=v["NOMBRE"];
    });
    // console.log(gsubasta);

    $(document).on("click",".botoneditar",function() {
        if ($(this).attr("valor")=="1") {
            $(".editar").prop("disabled",false);
            $("#gestionsubasta").removeAttr("required");
            $(".sologuardar").prop("disabled",false);
            $(this).attr("valor","0");
            $(this).html("Cancelar");
        }else{
            $(".editar").prop("disabled",true);
            $("#gestionsubasta").attr("required");
            $(".sologuardar").prop("disabled",true);
            $(this).attr("valor","1");
            $(this).html("Editar");
        }
    });

    $(document).on("click",".popupsubasta",function(){
        $("#modalSubasta").modal();
        $(".editar").prop("disabled",true);
        $(".botoneditar").attr("valor","1");
        $(".botoneditar").html("Editar");
        $('#formSubasta input[name="gmontoReactiva1"]').removeAttr("readonly");
        $('#formSubasta input[name="gmontoReactiva1"]').prop("disabled",true);
        var ruc = $(this).attr('ruc');
        $.ajax({
            url: "{{ route('cmpEmergencia.buscarinfo') }}",
            type: 'GET',
            data: {
                numruc: ruc
            },
            beforeSend: function() {
                $("#cargando").modal();
            },
            success: function (result) {
                $('#formSubasta input[name="numruc"]').val(result['NUM_RUC']);
                $('#formSubasta input[name="cliente"]').val(result['NOMBRE']);
                $('#formSubasta input[name="campanha"]').val(result['CAMPANIA']);
                $('#formSubasta input[name="telefono"]').val(result['TELEFONO1']);
                $('#formSubasta input[name="email"]').val(result['CORREO']);
                
                $('#formSubasta input[name="riesgosMonto"]').val(numeral(result['RIESGOS_MONTO']).format('0,0'));
                $('#formSubasta input[name="volventa"]').val((result['SOLICITUD_VOLUMEN_VENTA']||'0'));
                $('#formSubasta select[name="gflgReactiva1"]').val(result['FLG_TOMO_REACTIVA_1']);
                $('#formSubasta input[name="gmontoReactiva1"]').val((result['MONTO_PRESTAMO_REACTIVA_1']||'0'));
                $('#formSubasta input[name="soloferta"]').val((result['SOLICITUD_OFERTA']||'0'));
                $('#formSubasta select[name="plazogracia"]').val(result['SOLICITUD_PLAZO']+'-'+result['SOLICITUD_GRACIA']);
                $('#formSubasta select[name="bancor1"]').val(result['BANCO_REACTIVA_1']||'');
                if ($(".botoneditar").attr("valor")!="1") {
                    ComboFlgreactiva1(result['FLG_TOMO_REACTIVA_1']);
                }

                $('#formSubasta input[name="tasatentativa"]').val((result['SOLICITUD_TASA']));
                
                soloferta = isNaN(parseFloat($('#formSubasta input[name="soloferta"]').val()))?0:parseFloat($('#formSubasta input[name="soloferta"]').val());
                montorp1 = isNaN(parseFloat($('#formSubasta input[name="gmontoReactiva1"]').val()))?0:parseFloat($('#formSubasta input[name="gmontoReactiva1"]').val());
                monto_para_garantia = soloferta+montorp1;
                garantia = getCoberturaMonto(monto_para_garantia);
                $('#formSubasta input[name="garantia"]').val(garantia*100 + '%');
                $('#montogarantizado').val('S/. ' + numeral(garantia*soloferta).format('0,0'));
                $('#formSubasta input[name="montoriesgoibk"]').val('S/. ' + numeral((1-garantia)*soloferta).format('0,0'));

                $('#formSubasta input[name="tasamax"]').val(result['TASA_MAXIMA_BCR']);
                $('#formSubasta input[name="tasafinal"]').val(result['TASACLIENTE']);

                $('#formSubasta select[name="gestionsubasta"]').val(result['GESTION_SUBASTA']);
                $('#formSubasta textarea[name="comentariosubasta"]').html((result['GESTION_SUBASTA_COMENTARIO']));

                $('#formSubasta input[name="tasafinal"]').keyup();
            },
            complete: function() {
                $(".cerrarcargando").click();
            }
        });
    });

    $("#gflgReactiva1").change(function(event) {
        ComboFlgreactiva1($("#gflgReactiva1").val());
    });

    $('#formSubasta input[name="soloferta"]').keyup(function() {
        montorp1 = isNaN(parseFloat($('#formSubasta input[name="gmontoReactiva1"]').val()))?0:parseFloat($('#formSubasta input[name="gmontoReactiva1"]').val());
        soloferta = isNaN(parseFloat($(this).val()))?0:parseFloat($(this).val());
		garantia = getCoberturaMonto(parseFloat(soloferta+montorp1));
        $('#formSubasta input[name="garantia"]').val(numeral(garantia*100).format('0,00') + '%');
        $('#montogarantizado').val('S/. ' + numeral(garantia*soloferta).format('0,00'));
        $('#formSubasta input[name="montoriesgoibk"]').val('S/. ' + numeral((1-garantia)*soloferta).format('0,00'));
	});

    $('#formSubasta input[name="gmontoReactiva1"]').keyup(function() {
        montorp1 = isNaN(parseFloat($(this).val()))?0:parseFloat($(this).val());
        soloferta = isNaN(parseFloat($('#formSubasta input[name="soloferta"]').val()))?0:parseFloat($('#formSubasta input[name="soloferta"]').val());
		garantia = getCoberturaMonto(parseFloat(soloferta+montorp1));
        $('#formSubasta input[name="garantia"]').val(numeral(garantia*100).format('0,00') + '%');
        $('#montogarantizado').val('S/. ' + numeral(garantia*soloferta).format('0,00'));
        $('#formSubasta input[name="montoriesgoibk"]').val('S/. ' + numeral((1-garantia)*soloferta).format('0,00'));
	});

    $('#formSubasta select[name="gestionsubasta"]').change(function() {
        $('#formSubasta input[name="tasafinal"]').keyup();
    });
    $('#formSubasta input[name="tasafinal"]').keyup(function() {
        if($('#formSubasta select[name="gestionsubasta"]').val()=="0"){
            $(".sologuardar").attr("disabled","disabled");
            permitido = CalcularTasa($('#formSubasta input[name="tasamax"]').val(),$('#formSubasta input[name="tasafinal"]').val());
            if(permitido){
                $(".guardarsubasta").removeAttr("disabled");
            }else{
                $(".guardarsubasta").attr("disabled","disabled");
            }            
        }else{
            $(".sologuardar").removeAttr("disabled");
            $(".guardarsubasta").attr("disabled","disabled");
        }
	});

    function CalcularTasa(tasamax=0,tasafinal=0) {
        vtasamax = parseFloat(tasamax?tasamax:0);
        vtasafinal = parseFloat(tasafinal?tasafinal:0);
        if(vtasamax >= vtasafinal){
            return true;
        }else{
            return false;
        }
    }

    function tablasubastados(datos=null){  
        if ($.fn.dataTable.isDataTable('#datatablesubastados')) {
            $('#datatablesubastados').DataTable().destroy();
        }
        
        $('#datatablesubastados').DataTable({
            processing: true,
            "bAutoWidth": false, //ajustar tamaño de vista
            rowId: 'staffId',
            // dom: 'Blfrtip',
            serverSide: true,
            language: {"url": "{{ URL::asset('js/Json/Spanish.json') }}"},
            ajax: {
                "type": "GET",
                "url": "{{ route('cmpEmergencia.lista') }}", //ACA SE ENVÍA LOS DATOS
                data: function(data) {
                    data.etapa = datos;
                }
            },
            "aLengthMenu": [
                [25, 50, -1],
                [25, 50, "Todo"] 
            ],
            "iDisplayLength": 25, //muestra los 25 primeros
            // "order": [
            //     [0, "desc"] //orden de formar desc
            // ],
            columnDefs: [
                {
                    targets: 0,
                    data: 'WCL.NUM_RUC',
                    name: 'WCL.NUM_RUC',
                    render: function(data, type, row) {
                        return row.NUM_RUC + '<br>' + row.NOMBRE;
                    }
                },
                {
                    targets: 1,
                    data: null,
                    searchable: false,
                    sortable: false,
                    render: function(data, type, row) {
                        if (isNaN(parseFloat(row.SOLICITUD_OFERTA))) {mon = 0} else {mon=parseFloat(row.SOLICITUD_OFERTA)};;
                        if (isNaN(parseFloat(row.MONTO_PRESTAMO_REACTIVA_1))) {solof = 0} else {solof=parseFloat(row.MONTO_PRESTAMO_REACTIVA_1)};
                        return 'S/. ' + numeral(row.SOLICITUD_OFERTA).format('0,0')
                        + '( ' + getCoberturaMonto(parseFloat(mon)+parseFloat(solof))*100 + '%)'
                    }
                },
                {
                    targets: 2,
                    data: null,
                    searchable: false,
                    sortable: false,
                    render: function(data, type, row) 
                    {
                        $tasaten="";
                        if (row.TASA_FINAL_CLIENTE==null) {
                            $tasaten="<span align='left' >Tasa Ten. : "+(row.SOLICITUD_TASA||'-')+" %</span><br>";
                        }
                        return $tasaten+"<span align='left' >"+(row.SOLICITUD_PLAZO||'-')+" meses -"+(row.SOLICITUD_GRACIA||'-')+" gracia</span>";
                    }
                },
                {
                    targets: 3,
                    data: 'WCL.TASA_MAXIMA_BCR',
                    name: 'WCL.TASA_MAXIMA_BCR',
                    searchable: false,
                    render: function(data, type, row) {
                        return "<span align='left' >Máx : "+(row.TASA_MAXIMA_BCR||'-')+" %</span><br><span align='left' >Final : "+(row.TASA_FINAL_CLIENTE||'-')+" %</span>";
                    }
                },
                {
                    targets: 4,
                    data: 'WCL.ESTADO_SUBASTA',
                    name: 'WCL.ESTADO_SUBASTA',
                    searchable: false,
                    render: function(data, type, row) {
                        var texto = "PENDIENTE";
                        if(row.ESTADO_SUBASTA){
                            var texto = "SUBASTADO";
                        }
                        return "<u><a class='popupsubasta' ruc='"+row.NUM_RUC+"' >"+texto+"</a></u>";
                    }
                },
                {
                    targets: 5,
                    data: null,
                    data: 'WCL.GESTION_SUBASTA',
                    name: 'WCL.GESTION_SUBASTA',
                    searchable: false,
                    render: function(data, type, row) {
                        if(row.GESTION_SUBASTA==null){
                            return "PENDIENTE";
                        }
                        return gsubasta[row.GESTION_SUBASTA];
                    }
                },
                {
                    targets: 6,
                    data: 'WCL.NOMBRE',
                    name: 'WCL.NOMBRE',
                    sortable: false,
                    visible: false,
                    render: function(data, type, row) {
                        return row.NOMBRE;
                    }
                }
            ]
        });
    }

</script>
@stop