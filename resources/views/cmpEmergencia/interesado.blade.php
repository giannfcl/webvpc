@extends('cmpEmergencia.menu')

@section('datable')

<table class='table table-striped table-bordered jambo_table' id="datatableinteresado">
    <thead>
        <tr>
            <th scope="col" style="font-size: 12.5px;">Canal</th>
            <th scope="col" style="font-size: 12.5px;">Empresa</th>
            <th scope="col" style="font-size: 12.5px;">Desembolso RP1</th>
            <th scope="col" style="font-size: 12.5px;">Ventas</th>
            <th scope="col" style="font-size: 12.5px;">Oferta Riesgos</th>
            <th scope="col" style="font-size: 12.5px;">Monto Solicitado por Cliente</th>
            <th scope="col" style="font-size: 12.5px;">Condiciones</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
@stop
<!-- {{route('cmpEmergencia.bpe.gestionar')}} -->
<div class="modal fade" tabindex="-1" role="dialog" id="modalInteresado">
    <div class="modal-dialog" role="document" style="width: 1200px;">
        <div class="modal-content">
            <div class="modal-body">
                <form id="formInteresado" method="POST"  class="form-horizontal form-label-left" enctype="multipart/form-data" action="{{route('cmpEmergencia.validacionInteresado')}}">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Validación</h4>
                    </div>
                    <br>

                    <div class="form-group">
                        <div class="col-sm-6" >
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;" >RUC: </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input autocomplete="off" class="form-control"  type="text" name="numruc" readonly>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Teléfono:</label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input autocomplete="off" class="form-control input-number"  type="text" name="telefono">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Empresa:</label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input autocomplete="off" class="form-control"  type="text" name="cliente" readonly>
                            </div>
                        </div>

                         <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Correo: </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input autocomplete="off" class="form-control"  type="text" name="email">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Campaña: </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input autocomplete="off" class="form-control"  type="text" name="campanha" readonly>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Oferta Riesgos S/.: </label>
                            <div class="col-sm-4 col-xs-12" style="padding-left:0px">
                                <input autocomplete="off" class="form-control"  type="text" name="riesgosMonto" id="riesgosMonto" readonly>
                            </div>
                            <div class="col-sm-4 col-xs-12" style="padding-left:0px">
                                <input autocomplete="off" class="form-control"  type="text" name="riesgosCobertura" id="riesgosCobertura" readonly>
                            </div>
                        </div>
                    </div>

                    <div class="modal-header"  style="padding-left:0px;">
                        <h4 class="modal-title">Documentación</h4>
                    </div>
                    <br>

                    <div class="form-group">
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">REPORTE TRIBUTARIO A TERCEROS: </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <select class="form-control" name="flgrtterceros" required>
                                    <option value="0">NO</option>
                                    <option value="1">SI</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Ventas Anuales S/.: </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input autocomplete="off" class="form-control"  name="volventa" placeholder="" required onkeypress="return filterFloat(event,this);" onpaste="return filterFloat(event,this);">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">¿TIENE DESEMBOLSO RP1?:</label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <select class="form-control editable" name="gflgReactiva1" id="gflgReactiva1">
                                    <option value="">--Opciones--</option>
                                    <option value="0">No</option>
                                    <option value="1" disabled>Sí(Interbank)</option>
                                    <option value="2">Sí(Otro banco)</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Banco dondé tomo RP1:</label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <select class="form-control" name="bancor1" id="bancor1">
                                    <option value="">--Opciones--</option>
                                    @foreach($bancos as $banco)
                                        <option value="{{$banco->CODIGO}}">{{$banco->DESCRIPCION}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Monto Disponible :</label>
                            <div class="col-sm-4 col-xs-12" style="padding-left:0px">
                                <input autocomplete="off" class="form-control" name="montoCalculado" id="montoCalculado" readonly onkeypress="return filterFloat(event,this);" onpaste="return filterFloat(event,this);">
                            </div>
                            <div class="col-sm-4 col-xs-12" style="padding-left:0px">
                                <input autocomplete="off" class="form-control"  type="text" name="calculadoCobertura" id="calculadoCobertura" readonly>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Desembolso RP1 :</label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input autocomplete="off" class="form-control"  name="gmontoReactiva1" id="gmontoReactiva1" required onkeypress="return filterFloat(event,this);" onpaste="return filterFloat(event,this);">
                            </div>
                        </div>
                    </div>

                    <div class="modal-header" style="padding-left:0px;">
                        <h4 class="modal-title">Propuesta al Cliente</h4>
                    </div>
                    <br>

                    <div class="form-group">
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Monto Solicitado S/.: </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input autocomplete="off" class="form-control" type="text" name="soloferta" onkeypress="return filterFloat(event,this);" onpaste="return filterFloat(event,this);" maxlength="11">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Garantía: </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input autocomplete="off" class="form-control" type="text" readonly id="garantia" name="garantia">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Monto Garantizado : </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input autocomplete="off" class="form-control" type="text" name="montogarantizado" id="montogarantizado" readonly>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Riesgo Descubierto : </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input autocomplete="off" class="form-control" type="text" name="montoriesgoibk" id="montoriesgoibk" readonly>
                            </div>
                        </div>
                    </div>

                    <br>
                    <input type="hidden" name="condicion" value="1" />
                    <center>
                        <button class="btn btn-success guardarinteresado" type="submit">Guardar</button>
                    </center>
                </form>
            </div>
        </div>
    </div>
</div>


@section('js-vista')

<style>
    #modalInteresado input,textarea,select{
        margin-bottom: -15px;
    }
    .guardarinteresado{
        margin-left: 10%;
        margin-top: -10px;
    }
    #modalInteresado div.modal-header{
        padding: 4px !important;
    }
    .tooltip-inner {
        max-width: 350px;
        /* If max-width does not work, try using width instead */
        width: 350px;
    }

</style>

<script>
    tablainteresados("{{$request['etapa']}}");
    $('[data-toggle="tooltip"]').tooltip();

    $(document).on("click",".gestionInteresado",function(){
        $("#modalInteresado").modal();
        var ruc = $(this).attr('ruc')
        $.ajax({
            url: "{{ route('cmpEmergencia.buscarinfo') }}",
            type: 'GET',
            data: {
                numruc: ruc
            },
            beforeSend: function() {
                $("#cargando").modal();
            },
            success: function (result) {
                // console.log(result);
                $('#formInteresado input[name="numruc"]').val(result['NUM_RUC']);
                $('#formInteresado input[name="cliente"]').val(result['NOMBRE']);
                $('#formInteresado input[name="campanha"]').val(result['CAMPANIA']);
                $('#formInteresado input[name="telefono"]').val(result['TELEFONO1']);
                $('#formInteresado input[name="email"]').val(result['CORREO']);
                $('#formInteresado select[name="flgReactiva1"]').val(result['FLG_TOMO_REACTIVA_1']||'0');
                $('#formInteresado select[name="flgrtterceros"]').val(result['FLG_REPORTE_TRIBUTARIO_TERCEROS']||'0');
                $('#formInteresado input[name="productoMonto"]').val('S/. ' + (result['PRODUCTO_MONTO']||'-'));
                $('#formInteresado input[name="volventa"]').val((result['SOLICITUD_VOLUMEN_VENTA']));
                $('#formInteresado input[name="gmontoReactiva1"]').val(result['MONTO_PRESTAMO_REACTIVA_1']);

                $('#formInteresado input[name="riesgosMonto"]').val(numeral(result['RIESGOS_MONTO']).format('0,0'));
                $('#formInteresado input[name="riesgosCobertura"]').val(getCoberturaMonto(result['RIESGOS_MONTO'])*100 + '%')

                $('#formInteresado input[name="tasatentativa"]').val((result['SOLICITUD_TASA']));
                $('#formInteresado select[name="gflgReactiva1"]').val(result['FLG_TOMO_REACTIVA_1']);
                $('#formInteresado select[name="bancor1"]').val(result['BANCO_REACTIVA_1']||'');
                montoSolicitado = numeral(result['SOLICITUD_OFERTA']);

                ComboFlgreactiva1(result['FLG_TOMO_REACTIVA_1']||'0');

                //Logica Monto disponible
                montoReactiva1 = numeral($('#formInteresado input[name="gmontoReactiva1"]').val());
                resultado = calculoReactiva2(result['SOLICITUD_VOLUMEN_VENTA'],0,montoReactiva1.value());
                montoDisponible = resultado[0];
                coberturaCal = resultado[1];
                $('#montoCalculado').val(numeral(montoDisponible).format('0,0'));
                $('#calculadoCobertura').val(coberturaCal*100 + '%');

                // Solicitud: Monto, cobertura y riesgo descubierto

                $('#formInteresado input[name="soloferta"]').val(montoSolicitado.format('0'));
                coberturaSol = getCoberturaMonto(montoSolicitado.value() + montoReactiva1.value());
                $('#garantia').val(coberturaSol*100 + '%');
                $('#montogarantizado').val('S/. ' + numeral(coberturaSol*montoSolicitado.value()).format('0,0'));
                $('#montoriesgoibk').val('S/. ' + numeral((1-coberturaSol)*montoSolicitado.value()).format('0,0'));


                checkRiesgoVsCalculado()
            },
            complete: function() {
                // $("#cargando").hide();
                $(".cerrarcargando").click();
            }
        });
    });

    function checkRiesgoVsCalculado(){
        var A=numeral($("#montoCalculado").val()).value();
        var B=numeral($("#riesgosMonto").val()).value();
        if (A <= B) {
            //inputC.attr("readonly",false)
            $('#formInteresado input[name="dato"]').val(1)
        }else{
            //inputC.attr("readonly",true)
            $('#formInteresado input[name="dato"]').val(0)
        }
    }

    $('#formInteresado').submit(function(){
        $('#formInteresado input[name="condicion"]').val(1)
    })


	function actualizarCalculado(){
		result = calculoReactiva2(
			numeral($('#formInteresado input[name="volventa"]').val()).value(),
			0,
			numeral($('#formInteresado input[name="gmontoReactiva1"]').val()).value()
		)
		montoDisponible = result[0]
		cobertura = result[1]

		$('#montoCalculado').val(numeral(montoDisponible).format('0,0'));
		$('#calculadoCobertura').val(cobertura*100 + '%')
	}

    function actualizarSolicitud(){
        montoReactiva1 = numeral($('#formInteresado input[name="gmontoReactiva1"]').val())
		montoSolicitado = numeral($('#formInteresado input[name="soloferta"]').val())
        coberturaSol = getCoberturaMonto(montoSolicitado.value() + montoReactiva1.value())
        $('#garantia').val(coberturaSol*100 + '%')
        $('#montoriesgoibk').val('S/. ' + numeral((1-coberturaSol)*montoSolicitado.value()).format('0,0'))
        $('#montogarantizado').val('S/. ' + numeral(coberturaSol*montoSolicitado.value()).format('0,0'))
	}

    $('#formInteresado input[name="soloferta"]').keyup(function() {
        actualizarSolicitud();
	});

    $("#formInteresado input[name='soloferta']").bind('paste', function(e) {
        actualizarSolicitud();
    });

    $('#formInteresado input[name="gmontoReactiva1"]').keyup(function() {
        actualizarCalculado();
        actualizarSolicitud();
	});

    $("#formInteresado input[name='gmontoReactiva1']").bind('paste', function(e) {
        actualizarCalculado();
        actualizarSolicitud();
    });

	$('#formInteresado input[name="volventa"]').keyup(function() {
        actualizarCalculado();
    });

    $("#formInteresado input[name='volventa']").bind('paste', function(e) {
        actualizarCalculado();
	});

    $('#formInteresado select[name="gflgReactiva1"]').change(function(event) {
        ComboFlgreactiva1($(this).val());
        actualizarCalculado();
        actualizarSolicitud();
    });

    function tablainteresados(datos=null){
        if ($.fn.dataTable.isDataTable('#datatableinteresado')) {
            $('#datatableinteresado').DataTable().destroy();
        }

        $('#datatableinteresado').DataTable({
            destroy:true,
            processing: true,
            "bAutoWidth": false, //ajustar tamaño de vista
            rowId: 'staffId',
            // dom: 'Blfrtip',
            serverSide: true,
            language: {"url": "{{ URL::asset('js/Json/Spanish.json') }}"},
            ajax: {
                "type": "GET",
                "url": "{{ route('cmpEmergencia.lista') }}", //ACA SE ENVÍA LOS DATOS
                data: function(data) {
                    data.etapa = datos;
                }
            },
            "aLengthMenu": [
                [25, 50, -1],
                [25, 50, "Todo"]
            ],
            "iDisplayLength": 25, //muestra los 25 primeros
            "order": [
                [0, "desc"] //orden de formar desc
            ],
            columnDefs: [
                {
                    targets: 0,
                    data: 'ORDEN_CANAL',
                    data: 'ORDEN_CANAL',
                    sortable: true,
                    searchable: false,
                    render: function(data, type, row) {
                        $linea1="<div>"+(row.CANAL_ACTUAL||'-')+"</div>";
                        return $linea1;
                    }
                },
                {
                    targets: 1,
                    data: 'WCL.NUM_RUC',
                    name: 'WCL.NUM_RUC',
                    render: function(data, type, row) {
                        $linea1="<u><a class='gestionInteresado' ruc='"+row.NUM_RUC+"'><span align='center' >"+row.NUM_RUC+"<br><span>"+ row.NOMBRE+"</span></span></a></u>";
                        return $linea1;
                    }
                },
                {
                    targets: 2,
                    data: 'WCL.NOMBRE',
                    name: 'WCL.NOMBRE',
                    searchable: false,
                    sortable: false,
                    render: function(data, type, row) {
                        return "<span>S/. "+numeral(row.MONTO_PRESTAMO_REACTIVA_1).format('0,0')+"</span><br>";
                    }
                },
                {
                    targets: 3,
                    data: null,
                    searchable: false,
                    sortable: false,
                    render: function(data, type, row) {
                        $linea1="<span>S/. "+numeral(row.SOLICITUD_VOLUMEN_VENTA).format('0,0')+"</span><br>";
						//$linea2="<span>Aporte: S/. "+numeral(row.SOLICITUD_ESSALUD).format('0,0')+"</span>";
						return $linea1;
                    }
                },
                {
                    targets: 4,
                    data: null,
                    searchable: false,
                    sortable: false,
                    render: function(data, type, row) {
                        if(row.MONTO_RIESGO==null)
                            return "S/. -";

                        else
                            return "S/. " + numeral(row.MONTO_RIESGO).format('0,0')
                            + ' (' + getCoberturaMonto(row.MONTO_RIESGO)*100 + '%)';

                    }

                },
                {
                    targets: 5,
                    data: null,
                    searchable: false,
                    sortable: false,
                    render: function(data, type, row) {
                        if(row.SOLICITUD_OFERTA==null){
							return "S/. -";
                        }
						else{
                            return "S/. " + numeral(row.SOLICITUD_OFERTA).format('0,0') + ' (' + getCoberturaMonto(numeral(row.SOLICITUD_OFERTA).value() + numeral(row.MONTO_PRESTAMO_REACTIVA_1).value())*100 + '%)';
                        }
                    }
                },
                {
                    targets: 6,
                    data: null,
                    searchable: false,
                    sortable: false,
                    render: function(data, type, row) {
                        return "<span align='left' >Plazo : "+(row.SOLICITUD_PLAZO||'0')+" meses<br><span>Gracia : "+(row.SOLICITUD_GRACIA||'0')+" meses</span></span>";
                    }
                },
                {
					targets: 7,
					data: 'WCL.NOMBRE',
					name: 'WCL.NOMBRE',
					visible: false,
					searchable: true,
					render: function(data, type, row) {
							return row.NOMBRE;
					}
                }
            ]
        });
    }
</script>
@stop
