@extends('Layouts.layout')

@section('js-libs')
<link href="{{ URL::asset('css/datatables.min.css') }}" rel="stylesheet" type="text/css">

<script type="text/javascript" src="{{ URL::asset('js/datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/numeral/numeral.min.js') }}"></script>

@stop

@section('content')
<?php $zonales=[]; ?>
<!-- <div class="x_panel">
	<div class="form-group col-md-3">
		<label class="control-label col-md-4" style="margin-bottom: 0">Zonales</label>
		<div class="col-md-8" style="margin-bottom: 0">
			<select id="id_zona" class="form-control" name="id_zona">
				<option value="" selected>Todos...</option>
				@if(count($zonales)>0)
					@foreach($zonales as $zonal)
						<option value="{{$zonal->ID_ZONA}}">{{$zonal->ZONA}}</option>
					@endforeach
				@endif
			</select>
		</div>
	</div>
	<div class="form-group col-md-3">
		<label class="control-label col-md-4" style="margin-bottom: 0">Centros</label>
		<div class="col-md-8" style="margin-bottom: 0">
			<select id="id_centro" class="form-control" name="id_centro">
				<option value="" selected>Todos...</option>
			</select>
		</div>
	</div>
	<div class="form-group col-md-3">
		<label class="control-label col-md-4" style="margin-bottom: 0">Tiendas</label>
		<div class="col-md-8" style="margin-bottom: 0">
			<select id="id_tienda" class="form-control" name="id_tienda">
				<option value="" selected>Todos...</option>
			</select>
		</div>
	</div>
</div> -->


<div class="x_panel">
	<table class='table table-striped table-bordered jambo_table' id="dataTableLeads">
			<thead>
				<tr>
					<th>Empresa</th>
					<th>Resultado Web</th>
					<th>Banca / Ejecutivo</th>
					<th>Flujo Interno</th>
					<th>Comunicación Rechazo</th>
					<th>Último Contacto <br> Cliente</th>
					<th>Ventas<br>ESSALUD<br>(Anuales)</th>
					<th>Monto Oferta</th>
					<th>Priorización<br>Comunicación</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
	</table>
</div>

@stop

<div class="modal fade" tabindex="-1" role="dialog" id="modalGestion">
    <div class="modal-dialog" role="document" style="width: 720px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Nuevo</h4>
            </div>
            <div class="modal-body">
				<form id="formGestionar" method="POST" class="form-horizontal form-label-left" enctype="multipart/form-data" action="#">
					<div class="row">
						<div class="col-sm-6">
							<div class="row">
								<div class="form-group">
									<label class="control-label col-sm-3 col-xs-12">RUC:</label>
									<div class="col-sm-9 col-xs-12" style="padding-left:0px">
										<input autocomplete="off" class="form-control"  type="text" name="numruc" readonly>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-3 col-xs-12">Cliente:</label>
									<div class="col-sm-9 col-xs-12" style="padding-left:0px">
										<input autocomplete="off" class="form-control"  type="text" name="cliente" readonly>
									</div>
								</div>
							</div>
						</div>
                    	<button class="btn btn-success btnGtp" type="submit">Guardar</button>
            		</div>
               </form>
			</div>
		</div>
    </div>
</div>

<div class="modal fade" id="cargando" tabindex="-1" role="dialog">
	<div class="modal-dialog loader" role="document">
		<button type="button" class="close cerrarcargando" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" hidden>&times;</span></button>
	</div>
</div>

<ul id="tooltipTexts" class="hidden">
	{{---
	<li class="etapa-bpe-1">Base pre-aprobada por riesgos, puede encontrarse asignado a un ejecutivo o a Televentas. El EN aun no ha logrado contactarse con el cliente</li>
<<<<<<< HEAD
	--}}
	<li class="etapa-bpe-3">El EN/Televentas se ha contactado con el cliente y muestra interes en el crédito. Ya se tienen los de essalud, ventas y el monto deseado por el cliente	</li>
=======
	<li class="etapa-bpe-3">El EN/Televentas se ha contactado con el cliente y muestra interes en el crédito. Ya se tienen los de essalud, ventas y el monto deseado por el cliente</li>
>>>>>>> 93ad8785df155c4c77072fa67fd7f338272df0d0
	<li class="etapa-bpe-4">Los datos del cliente(essalud/ventas/monto solicitado)+ han sido enviados a riesgos para su revisión</li>
	<li class="etapa-bpe-6">Los datos del cliente son correctos y estan pasando el proceso de desembolso</li>
	<li class="etapa-bpe-8">Se contacto al cliente y no esta interesado o no califica. Tambien puede haber sido rechazado por riesgos</li>

	<li class="etapa-inmb-11">Formato listo para envío a COFIDE</li>
	<li class="etapa-inmb-12">Formato listo para envío a COFIDE</li>
	
	<li class="etapa-retail-1">Base pre-aprobada por riesgos, puede encontrarse asignado a un ejecutivo o a Televentas. El EN aun no ha logrado contactarse con el cliente</li>
	<li class="etapa-retail-3">El EN/Televentas se ha contactado con el cliente y muestra interes en el crédito. Ya se tienen los de essalud, ventas y el monto deseado por el cliente</li>
	<li class="etapa-retail-4">Los datos del cliente(essalud/ventas/monto solicitado)+ han sido enviados a riesgos para su revisión</li>
	<li class="etapa-retail-8">Se contacto al cliente y no esta interesado o no califica. Tambien puede haber sido rechazado por riesgos</li>

	<li class="etapa-1">Base pre-aprobada por riesgos. EN cuenta con montos essalud y ventas, oferta riesgos, plazo/gracia, monto solicita cliente</li>
	<li class="etapa-3">Muestra interés por el crédito. EN cuenta con al menos un documento (essalud o ventas) y tasa tentativa</li>
	<li class="etapa-4">EN cuenta con reporte tributario terceros (essalud y ventas), confirma correo de cobranza, aceptación del cliente sobre monto</li>
	<li class="etapa-10">Relación de clientes a la espera que el GD asigne una subasta ganada</li>
	<li class="etapa-5">EN cuenta con doc completa para armar file y listo para ingresar el WIO</li>
	<li class="etapa-6">EN cuenta con doc completa para armar file y listo para ingresar el WIO</li>
	<li class="etapa-12">Se cuenta con línea (información proporcionada por TI) por proceso automático</li>
	<li class="etapa-13">Cuenta con alguna observación: 1) “Monto prestamos supera ventas o Essalud 2) “Sin subasta preasignada” 3) “Tasa ingresada mayor a tasa repo”</li>
	<li class="etapa-14">No cuenta con observaciones, se genera el envio a Cofide</li>
	<li class="etapa-7">Se encuentra en la base de desembolso por proceso automático</li>
	<li class="etapa-9"></li>


	<li class="rechazoweb-A">No cumplen los filtros dados por el Estado o no se encuentran en nuestro Universo accionable​</li>
	<li class="rechazoweb-B">Clientes que no pertenecen ni al Universo BPE, VPC (No BPE) o Retail que previamente fue evaluado por riesgos</li>
	<li class="rechazoweb-C">Clientes filtrados por no tener una Calificación Normal 100% en el ultimo RCC antes del lanzamiento de reactiva, o por no tener un RUC con una antigüedad de mas de tres años</li>
	<li class="rechazoweb-D">Clientes filtrados porque su oferta calculada no cumple el criterio mínimo que se le impone según su Banca (BPE, VPC o Retail)</li>

	<li class="webaprobado-A">Solicitud aprobada por riesgos</li>
	<li class="webaprobado-B">Solicitud rechazada por riesgos (BPE)</li>
	<li class="webaprobado-C">No evaluado por riesgos</li>

	<li class="formularioincompleto-E">Clientes que pasaron todos los filtros pero no terminaron de enviar su solicitud</li>
</ul>

<style>
	table,tr,td,th{
		font-size: 10.5px;
	}
	.loader {
		margin-top:10%;
		border: 16px solid #f3f3f3;
		border-radius: 50%;
		border-top: 16px solid #3498db;
		width: 120px !important;
		height: 120px !important;
		-webkit-animation: spin 2s linear infinite; /* Safari */
		animation: spin 2s linear infinite;
	}

	@-webkit-keyframes spin {
		0% { -webkit-transform: rotate(0deg); }
		100% { -webkit-transform: rotate(360deg); }
	}

	@keyframes spin {
		0% { transform: rotate(0deg); }
		100% { transform: rotate(360deg); }
	}

	.modal-body {
		max-height: calc(95vh - 75px);
		overflow-y: auto;
	}
</style>
@section('js-scripts')
<script>
    
	dtLeads();
	$('[data-toggle="tooltip"]').tooltip();
	
	function dtLeads(id_zona=null,id_centro=null,id_tienda=null){
		if ($.fn.dataTable.isDataTable('#dataTableLeads')) {
			$('#dataTableLeads').DataTable().destroy();
		}
		
		$('#dataTableLeads').DataTable({
			processing: true,
			"bAutoWidth": false,
			rowId: 'staffId',
			// dom: 'Blfrtip',
			serverSide: true,
			language: {"url": "{{ URL::asset('js/Json/Spanish.json') }}"},
			ajax: {
				"type": "GET",
				"url": "{{ route('cmpEmergencia.solemp.lista') }}",
				data: function(data) {
					data.etapa = null;
					data.id_zona = null;
					data.id_centro = null;
					data.id_tienda = null;
				}
			},
			"aLengthMenu": [
				[25, 50, -1],
				[25, 50, "Todo"]
			],
			"iDisplayLength": 25,
			columnDefs: [
				{
					targets: 0,
					data: 'WCVTR.NUM_RUC',
                    name: 'WCVTR.NUM_RUC',
					sortable: false,
					render: function(data, type, row) {
						return '<span>('+(row.NUM_RUC || '-') + ') ' + (row.NOMBRE || '-')+'</span>';
					}
				},
				{
					targets: 1,
					data: null,
					searchable: false,
					sortable: false,
					render: function(data, type, row) {
						if (row.RESULTADO_WEB=="FORMULARIO INCOMPLETO") {
							return '<u aria-hidden="true"  data-toggle="tooltip" data-placement="right" title="'+datatooltip(2,{RESULTADO_WEB:row.RESULTADO_WEB,DETALLE_WEB_NUM:row.DETALLE_WEB.substring(0, 1)})+'">'+row.RESULTADO_WEB+'</u>';
						}
						else if (row.RESULTADO_WEB=="APROBADO WEB") {
							if(row.ANALISIS_RIESGOS){
								return '<u aria-hidden="true"  data-toggle="tooltip" data-placement="right" title="'+datatooltip(2,{RESULTADO_WEB:row.RESULTADO_WEB,ANALISIS_RIESGOS_LETRA:row.ANALISIS_RIESGOS.substring(0,1)})+'">'+row.RESULTADO_WEB+'-<br>'+ row.ANALISIS_RIESGOS+'</u>'
							}else{
								return (row.RESULTADO_WEB || "-");
							}
						}
						else{
							if (row.DETALLE_WEB) {
								return '<u aria-hidden="true"  data-toggle="tooltip" data-placement="right" title="'+datatooltip(2,{RESULTADO_WEB:row.RESULTADO_WEB,DETALLE_WEB_NUM:row.DETALLE_WEB.substring(0, 1)})+'">'+row.RESULTADO_WEB+'-<br>('+row.DETALLE_WEB+')</u>';
							}else{
								return (row.RESULTADO_WEB || "-");
							}
						}
					}
				},
				{
					targets: 2,
					data: null,
                    sortable: false,
					searchable: false,
					render: function(data, type, row) {
						return '(' + (row.BANCA || "-") + ') ' + (row.EJECUTIVO || "-");
					}
				},
				{
					targets: 3,
					data: null,
					searchable: false,
					sortable: false,
					render: function(data, type, row) {
						if (row.ETAPA_ID && row.FLUJO_INTERNO!='Pendiente registro en landing'){
							return "<u aria-hidden='true'  data-toggle='tooltip' data-placement='right' title='"+datatooltip(1,{BANCA:row.BANCA,ETAPA_ID:row.ETAPA_ID})+"'>"+(row.FLUJO_INTERNO || "-")+"</u>";
						}else if(row.FLUJO_INTERNO=='Pendiente registro en landing'){
							return "<u aria-hidden='true'  data-toggle='tooltip' data-placement='right' title='Cliente aprobado pero aun no se registra en la landing. Una vez que se registre en la landing recién se le asignara un ejecutivo'>"+row.FLUJO_INTERNO+"</u>";
						}
					}
				},
				{
					targets: 4,
					data: null,
                    searchable: false,
					sortable: false,
					render: function (data,type,row) {
						if (row.RESULTADO_WEB == "APROBADO WEB" && row.ANALISIS_RIESGOS == "B. RECHAZADO POR RIESGOS") {
							if (row.RECHAZO_FECHA){
								return row.RECHAZO_FECHA+'/<br>'+row.RECHAZO_CONTACTO;
							}else{
								return 'Pendiente'
							}
							
						}else{
							return "-";
						}
					}
				},
				{
					targets: 5,
					data: null,
                    searchable: false,
					sortable: false,
					render: function (data,type,row) {
						return (row.FECHA_CONTACTO || "-");
					}
				},
				{
					targets: 6,
					data: null,
					searchable: false,
					sortable: false,
					render: function(data, type, row) {
						if (row.RESULTADO_WEB=="RECHAZADO WEB") {
							return "-";
	                    }else{
	                    	return 'Ventas: S/ ' + (numeral(row.VENTAS_ANUALES).format('0,0')||'-')
	                        	+'<br> ESSALUD: S/ ' + (numeral(row.VENTAS_ESSALUD).format('0,0')||'-');
	                    }
					}
				},
				{
					targets: 7,
					data: 'WCVTR.MONTO_CALCULADO',
					name: 'WCVTR.MONTO_CALCULADO',
                    searchable: false,
					sortable: false,
					render: function(data, type, row) {
						if (row.RESULTADO_WEB=="RECHAZADO WEB") {
							return "-";
	                    }else{
							return 'Solicitado: S/.' + numeral(row.MONTO_SOLICITADO).format("0,00")
							+'<br> Calculado: S/.' + (numeral(row.MONTO_CALCULADO).format('0,0')||'-');
						}
					}
				},
				{
					targets: 8,
					data: null,
					name: null,
                    searchable: false,
					sortable: false,
					render: function(data, type, row) {
						// if(row.RESULTADO_WEB == 'APROBADO WEB'){
							if (row.PRIORIZACION_FLAG == 1) {checked = 'checked'} else {checked = ''}
							html = '';

							if(row.PRIORIZACION_ESTADO == 1){
								html = '<input class="chkPriorizar" flujointerno="' + row.FLUJO_INTERNO + '" resultadoweb="' + row.RESULTADO_WEB + '" ruc="' + row.NUM_RUC + '" type="checkbox" value="" '+ checked + ' disabled>';
							}else{
								html = '<input class="chkPriorizar" flujointerno="' + row.FLUJO_INTERNO + '" resultadoweb="' + row.RESULTADO_WEB + '" ruc="' + row.NUM_RUC + '" type="checkbox" value="" '+ checked + '>';
							}
							return html;
						// }else{
						// 	return '';
						// }
						
					}
				},
				{
					targets: 9,
					data: 'WCVTR.NOMBRE',
                    name: 'WCVTR.NOMBRE',
					sortable: false,
					visible: false,
					render: function(data, type, row) {
						return '';
					}
				}
			]
		});
	}

	$(document).on('change','.chkPriorizar',function(){
		elem = $(this);
		ruc = elem.attr('ruc');
		flujointerno = elem.attr('flujointerno');
		resultadoweb = elem.attr('resultadoweb');
		value = elem.is(":checked");
		if (ruc) {
			$.ajax({
	            url: "{{ route('cmpEmergencia.solemp.priorizar') }}",
	            type: 'POST',
	            data: {
					ruc: ruc,
					condicion: value,
					flujointerno: flujointerno,
					resultadoweb: resultadoweb
				},
				beforeSend: function() {
					elem.prop('disabled',true)
				},
	            success: function (result) {
					
	            },
	            error: function (xhr, status, text) {
	                e.preventDefault();
	                Swal.fire('Hubo un error al registrar al actualizar los datos, contáctese con administración');
					elem.prop('checked',!value)
					elem.prop('disabled',false)
	            },
				complete: function() {
					elem.prop('disabled',false)
				}
	        });
		}
	})

	function detallesweb(num) {
		data = {
			1:'1. No se encuentra en base Reactiva',
			2:'2. No cumplen RP',
			3:'3. Segmento Retail',
			4:'4. No cumplen filtros IBK',
			5:'5. No tienen info de ESSALUD o SUNAT',
			6:'6. No cumplen ticket minimo',
			7:'7. No ha llenado formulario',
			8:'8. Leads completan formulario',
		}
		return data[num];
	}

	function datatooltip(tipo,data=[]) {
		// console.log(data);

		if (tipo == 1) {
			if (data['ETAPA_ID']) {
				if (data['BANCA']=='BPE') {
					return $("#tooltipTexts li.etapa-bpe-"+data['ETAPA_ID']).html();
				}
				else if (data['BANCA']=='INMB') {
					return $("#tooltipTexts li.etapa-inmb-"+data['ETAPA_ID']).html();
				}
				else if (data['BANCA']=='RETAIL') {
					return $("#tooltipTexts li.etapa-retail-"+data['ETAPA_ID']).html();
				}
				else if (data['BANCA']=='BC' || data['BANCA']=='BE') {
					return $("#tooltipTexts li.etapa-"+data['ETAPA_ID']).html();
				}
				else{
					return "";
				}
			}else{
				return "";
			}
		}
		else if (tipo == 2) {
			if (data['RESULTADO_WEB']=='RECHAZADO WEB') {
				return $("#tooltipTexts li.rechazoweb-"+data['DETALLE_WEB_NUM']).html();
			}
			else if (data['RESULTADO_WEB']=='APROBADO WEB') {
				return $("#tooltipTexts li.webaprobado-"+data['ANALISIS_RIESGOS_LETRA']).html();
			}
			else if (data['RESULTADO_WEB']=='FORMULARIO INCOMPLETO'){
				return $("#tooltipTexts li.formularioincompleto-"+data['DETALLE_WEB_NUM']).html();
			}else{
				return "";
			}
		}else{
			return "";
		}
	}
</script>
@stop