@extends('cmpEmergencia.menu')

@section('datable')

<table class='table table-striped table-bordered jambo_table' id="datatableEvaluacion">
  <thead>
    <tr>

      <th scope="col" style="font-size: 12.5px;">Empresa</th>
      <th scope="col" style="font-size: 12.5px;">Desembolso RP1</th>
      <th scope="col" style="font-size: 12.5px;">Ventas</th>
      <th scope="col" style="font-size: 12.5px;">Oferta Riesgos / % Garantía</th>
      <th scope="col" style="font-size: 12.5px;">Monto Solicitado por Cliente / % Garantía</th>
      <th scope="col" style="font-size: 12.5px;">Condiciones</th>
      <th scope="col" style="font-size: 12.5px;">Tiene Documentos</th>
    </tr>
  </thead>
</table>

@stop


<div class="modal fade" tabindex="-1" role="dialog" id="modalEvaluacion">
    <div class="modal-dialog" role="document" style="width: 1200px;">
        <div class="modal-content">
            <div class="modal-body">
                <form id="formEvaluacion" method="POST"  class="form-horizontal form-label-left" enctype="multipart/form-data" action="{{route('cmpEmergencia.docs-completos')}}">
                  <input autocomplete="off" class="form-control input-number"  type="hidden" name="ratioRiesgos" value="{{ $ratioRiesgos }}">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Validación</h4>
                  </div>
                  <br>

                  <div class="form-group">
                    <div class="col-sm-6" >
                      <label class="control-label col-sm-4 col-xs-12" style="text-align: left;" >RUC: </label>
                      <div class="col-sm-8 col-xs-12" style="padding-left:0px" >
                        <input autocomplete="off" class="form-control" type="text" name="numruc" readonly>
                      </div>
                    </div>

                    <div class="col-sm-6">
                      <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Teléfono:</label>
                      <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                        <input autocomplete="off" class="form-control input-number" type="text" name="telefono">
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-sm-6">
                      <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Empresa:</label>
                      <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                          <input autocomplete="off" class="form-control" type="text" name="cliente" readonly>
                      </div>
                    </div>

                    <div class="col-sm-6">
                      <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Correo: </label>
                      <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                        <input autocomplete="off" class="form-control" type="text" name="email">
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-sm-6">
                      <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Campaña: </label>
                      <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                          <input autocomplete="off" class="form-control" type="text" name="campanha" readonly>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Oferta Riesgos S/.: </label>
                      <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                          <input autocomplete="off" class="form-control" type="text" name="riesgosMonto" id="riesgosMonto" readonly>
                      </div>
                    </div>
                  </div>

                  <div class="modal-header" style="padding-left:0px;">
                    <h4 class="modal-title">Documentación</h4>
                  </div>
                  <br>

                  <div class="form-group">
                    <div class="col-sm-6">
                        <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">REPORTE TRIBUTARIO A TERCEROS: </label>
                        <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                          <select class="form-control" name="flgrtterceros" required>
                            <option value="0">NO</option>
                            <option value="1">SI</option>
                          </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Ventas Anuales S/.: </label>
                        <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                          <input autocomplete="off" class="form-control"  name="volventa" required onkeypress="return filterFloat(event,this);" onpaste="return filterFloat(event,this);">
                        </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-sm-6">
                      <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">¿TIENE DESEMBOLSO RP1?:</label>
                      <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                        <select class="form-control editable" name="gflgReactiva1" id="gflgReactiva1">
                          <option value="">--Opciones--</option>
                          <option value="0">No</option>
                          <option value="1" disabled>Sí(Interbank)</option>
                          <option value="2">Sí(Otro banco)</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Banco dondé tomo RP1:</label>
                      <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                        <select class="form-control" name="bancor1" id="bancor1">
                          <option value="">--Opciones--</option>
                          @foreach($bancos as $banco)
                            <option value="{{$banco->CODIGO}}">{{$banco->DESCRIPCION}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                      <div class="col-sm-6">
                        <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Monto Disponible :</label>
                        <div class="col-sm-4 col-xs-12" style="padding-left:0px">
                          <input autocomplete="off" class="form-control" name="montoCalculado" id="montoCalculado" readonly onkeypress="return filterFloat(event,this);" onpaste="return filterFloat(event,this);" >
                        </div>
                        <div class="col-sm-4 col-xs-12" style="padding-left:0px">
                          <input autocomplete="off" class="form-control" type="text" name="calculadoCobertura" id="calculadoCobertura" readonly>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Desembolso RP1 :</label>
                        <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                          <input autocomplete="off" class="form-control" name="gmontoReactiva1" id="gmontoReactiva1" required onkeypress="return filterFloat(event,this);"onpaste="return filterFloat(event,this);" >
                        </div>
                      </div>
                  </div>

                  <div class="modal-header"  style="padding-left:0px;">
                    <h4 class="modal-title">Propuesta al Cliente</h4>
                  </div>
                  <br>

                  <div class="form-group">
                    <div class="col-sm-6">
                      <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Monto Solicitado S/.: </label>
                      <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                        <input autocomplete="off" class="form-control" type="text" name="soloferta" id="soloferta" onkeypress="return filterFloat(event,this);" onpaste="return filterFloat(event,this);" maxlength="11">
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Garantía: </label>
                      <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                        <input autocomplete="off" class="form-control" type="text" readonly id="garantia" name="garantia">
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-sm-6">
                      <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Monto Garantizado : </label>
                      <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                        <input autocomplete="off" class="form-control" type="text" name="montogarantizado" id="montogarantizado" readonly>
                      </div>
                    </div>

                    <div class="col-sm-6">
                      <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Riesgo Descubierto : </label>
                      <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                        <input autocomplete="off" class="form-control" type="text" name="montoriesgoibk" id="montoriesgoibk" readonly>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-sm-6">
                      <label class="control-label col-sm-7 col-xs-12" style="text-align: left;">Correo de Contacto que recibirá las notificaciones de crédito: </label>
                      <div class="col-sm-5 col-xs-12" style="padding-left:0px">
                        <input autocomplete="off" class="form-control" name="cemail">
                      </div>
                    </div>

                    <div class="col-sm-6">
                      <label class="control-label col-sm-9 col-xs-12"style="text-align: left;">Confirmar si el monto final ha sido aceptado por el cliente: </label>
                      <div class="col-sm-3 col-xs-12" style="padding-left:0px">
                        <input type="checkbox" class ="check" name="flgConfirmar" value="1">
                      </div>
                    </div>
                  </div>
                  <br>

                  <input type="hidden" name="accion" value="0" />
                  <center>
                    <button class="btn btn-success" type="submit" id="btnGuardar">Guardar</button>
                    <button class="btn btn-success" type="submit" id="btnEnviarSubasta">Enviar a Subasta</button>
                  </center>
                </form>
            </div>
        </div>
    </div>
</div>


@section('js-vista')
<style>
    #modalEvaluacion input,textarea,select{
        margin-bottom: -15px;
    }
    #modalEvaluacion div.modal-header{
        padding: 8px !important;
    }
    .tooltip-inner {
        max-width: 350px;
        /* If max-width does not work, try using width instead */
        width: 350px;
    }
    .check{
        width: 22px;
        height: 22px;
    }
    .swal2-html-container{
      text-align: left;
    }
</style>

<script>
  tablariesgos("{{$request['etapa']}}"); //MANDEMOS ESA ETAPA EN QUE SE ENCUENTRE
  $('[data-toggle="tooltip"]').tooltip();

  $('#btnGuardar').click(function(e){
    $('#formEvaluacion input[name="accion"]').val(0);
  });

  $('#btnEnviarSubasta').click(function(e){
    $('#formEvaluacion input[name="accion"]').val(1);
  });

  $('#formEvaluacion').submit(function(e){
    solicitud = numeral($('#formEvaluacion input[name="soloferta"]').val()).value();
    calculado = numeral($('#formEvaluacion input[name="montoCalculado"]').val()).value();
    riesgos = numeral($('#formEvaluacion input[name="riesgosMonto"]').val()).value();
    ratio = numeral($('#formEvaluacion input[name="ratioRiesgos"]').val()).value();

    if ( $('#formEvaluacion input[name="accion"]').val() == 1 ){
        if(
            $('#formEvaluacion input[name="cemail"]').val() == ''
            || $('#formEvaluacion input[name="flgConfirmar"]').is(':checked') == false
            || $('#formEvaluacion select[name="flgrtterceros"]').val() != 1
            || solicitud <= 0
            || solicitud > calculado * ratio
            || solicitud > riesgos * ratio
          ){
            Swal.fire({
              icon :"warning",
              title:"No has cumplido todas las condiciones para enviar a subasta:",
              html:
                "<ul><li>Documentacion completa</li>"
                + "<li>Correo electrónico y confirmación del cliente</li>"
                + "<li>Monto solicitado menor al de riegos y el calculado</li></ul>",
              width: 350,
            })
            return false;
        }
    }
  });

  $(document).on("click",".gestionEvaluacion",function(){
      $("#modalEvaluacion").modal();
      var ruc = $(this).attr('ruc');
      $.ajax({
          url: "{{ route('cmpEmergencia.buscarinfo') }}",
          type: 'GET',
          data: {
              numruc: ruc
          },
          beforeSend: function() {
              $("#cargando").modal();
          },
          success: function (result) {
              $('#formEvaluacion input[name="numruc"]').val(result['NUM_RUC']);
              $('#formEvaluacion input[name="cliente"]').val(result['NOMBRE']);
              $('#formEvaluacion input[name="campanha"]').val(result['CAMPANIA']);
              $('#formEvaluacion input[name="telefono"]').val(result['TELEFONO1']);
              $('#formEvaluacion input[name="email"]').val(result['CORREO']);
              $('#formEvaluacion input[name="cemail"]').val(result['CORREO_CONFIRMACION']);
              $('#formEvaluacion select[name="flgrtterceros"]').val(result['FLG_REPORTE_TRIBUTARIO_TERCEROS']||'0');
              $('#formEvaluacion input[name="volventa"]').val((result['SOLICITUD_VOLUMEN_VENTA']||'0'));
              $('#formEvaluacion input[name="gmontoReactiva1"]').val((result['MONTO_PRESTAMO_REACTIVA_1']||'0'));
              $('#formEvaluacion input[name="soloferta"]').val(numeral(result['SOLICITUD_OFERTA']).value());
              $('#formEvaluacion input[name="riesgosMonto"]').val(numeral(result['RIESGOS_MONTO']).format('0,0'));
              $('#formEvaluacion select[name="gflgReactiva1"]').val(result['FLG_TOMO_REACTIVA_1']);
              $('#formEvaluacion select[name="bancor1"]').val(result['BANCO_REACTIVA_1']||'');
              ComboFlgreactiva1(result['FLG_TOMO_REACTIVA_1']);

              //Logica Monto disponible
              montoReactiva1 = numeral($('#formEvaluacion input[name="gmontoReactiva1"]').val());
              resultado = calculoReactiva2(result['SOLICITUD_VOLUMEN_VENTA'],0,montoReactiva1.value());
              montoDisponible = resultado[0];
              coberturaCal = resultado[1];
              $('#montoCalculado').val(numeral(montoDisponible).format('0,0'));
              $('#calculadoCobertura').val(coberturaCal*100 + '%');

              // Solicitud: Monto, cobertura y riesgo descubierto
              montoSolicitado = numeral(result['SOLICITUD_OFERTA']);
              $('#formEvaluacion input[name="soloferta"]').val(montoSolicitado.format('0'));
              coberturaSol = getCoberturaMonto(montoSolicitado.value() + montoReactiva1.value());
              $('#garantia').val(coberturaSol*100 + '%');
              $('#montoriesgoibk').val('S/. ' + numeral((1-coberturaSol)*montoSolicitado.value()).format('0,0'));
              $('#montogarantizado').val('S/. ' + numeral(coberturaSol*montoSolicitado.value()).format('0,0'));

          },
          complete: function() {
              // $("#cargando").hide();
              $(".cerrarcargando").click();
          }
      });
  });

  function actualizarCalculado(){
		result = calculoReactiva2(
			numeral($('#formEvaluacion input[name="volventa"]').val()).value(),
			0,
			numeral($('#formEvaluacion input[name="gmontoReactiva1"]').val()).value()
		)
		montoDisponible = result[0]
		cobertura = result[1]
		$('#montoCalculado').val(numeral(montoDisponible).format('0,0'));
		$('#calculadoCobertura').val(cobertura*100 + '%')
	}

  function actualizarSolicitud(){
    montoReactiva1 = numeral($('#formEvaluacion input[name="gmontoReactiva1"]').val())
		montoSolicitado = numeral($('#formEvaluacion input[name="soloferta"]').val())
    coberturaSol = getCoberturaMonto(montoSolicitado.value() + montoReactiva1.value())
    $('#garantia').val(coberturaSol*100 + '%')
    $('#montoriesgoibk').val('S/. ' + numeral((1-coberturaSol)*montoSolicitado.value()).format('0,0'))
    $('#montogarantizado').val('S/. ' + numeral(coberturaSol*montoSolicitado.value()).format('0,0'))
	}

  $('#formEvaluacion input[name="soloferta"]').keyup(function() {
    actualizarSolicitud()
	});

  $("#formEvaluacion input[name='soloferta']").bind('paste', function(e) {
    actualizarSolicitud();
  });

  $('#formEvaluacion input[name="gmontoReactiva1"]').keyup(function() {
		actualizarCalculado()
    actualizarSolicitud()
	});

  $("#formEvaluacion input[name='gmontoReactiva1']").bind('paste', function(e) {
    actualizarCalculado();
    actualizarSolicitud();
  });

	$('#formEvaluacion input[name="volventa"]').keyup(function() {
		actualizarCalculado()
	});

  $("#formEvaluacion input[name='volventa']").bind('paste', function(e) {
    actualizarCalculado();
  });

  $('#formEvaluacion select[name="gflgReactiva1"]').change(function() {
    ComboFlgreactiva1($(this).val())
    actualizarCalculado()
    actualizarSolicitud()
  });

  function tablariesgos(datos=null){
      if ($.fn.dataTable.isDataTable('#datatableEvaluacion')) {
        $('#datatableEvaluacion').DataTable().destroy();
      }

      $('#datatableEvaluacion').DataTable({
          processing: true,
          "bAutoWidth": false, //ajustar tamaño de vista
          rowId: 'staffId',
          // dom: 'Blfrtip',
          serverSide: true,
          language: {"url": "{{ URL::asset('js/Json/Spanish.json') }}"},
          ajax: {
              "type": "GET",
              "url": "{{ route('cmpEmergencia.lista') }}", //ACA SE ENVÍA LOS DATOS
              data: function(data) {
                  data.etapa = datos;
              }
          },
          "aLengthMenu": [
              [25, 50, -1],
              [25, 50, "Todo"]
          ],
          "iDisplayLength": 25, //muestra los 25 primeros
          // "order": [
          //     [0, "desc"] //orden de formar desc
          // ],
          columnDefs: [
              {
                  targets: 0,
                  data: 'WCL.NUM_RUC',
                  name: 'WCL.NUM_RUC',
                  render: function(data, type, row) {
                    return row.NUM_RUC + '<br>' + row.NOMBRE;
                  }
              },
              {
                  targets: 1,
                  data: null,
                  searchable: false,
                  sortable: false,
                  render: function(data, type, row) {
                    return "<span>S/. "+numeral(row.MONTO_PRESTAMO_REACTIVA_1).format('0,0')+"</span><br>";
                  }
              },
              {
                  targets: 2,
                  data: null,
                  searchable: false,
                  sortable: false,
                  width: '10%',
                  render: function(data, type, row) {
                    $linea1="<span>S/. "+numeral(row.SOLICITUD_VOLUMEN_VENTA).format('0,0')+"</span><br>";
                    //$linea2="<span>Aporte: S/. "+numeral(row.SOLICITUD_ESSALUD).format('0,0')+"</span>";
                    return $linea1;
                  }
              },
              {
                  targets: 3,
                  data: null,
                  searchable: false,
                  sortable: false,
                  render: function(data, type, row) {
                    riesgo_monto = row.RIESGOS_MONTO?parseFloat(row.RIESGOS_MONTO):0;
                    montorp1 = row.MONTO_PRESTAMO_REACTIVA_1?parseFloat(row.MONTO_PRESTAMO_REACTIVA_1):0;
                    monto_paracobertura = riesgo_monto+montorp1;

                    return 'S/. ' + numeral(row.RIESGOS_MONTO).format('0,0')
                      + ' ( ' + getCoberturaMonto(monto_paracobertura)*100 + '%)'
                  }
              },
              {
                  targets: 4,
                  data: null,
                  searchable: false,
                  sortable: false,
                  render: function(data, type, row) {
                    soloferta = row.SOLICITUD_OFERTA?parseFloat(row.SOLICITUD_OFERTA):0;
                    montorp1 = row.MONTO_PRESTAMO_REACTIVA_1?parseFloat(row.MONTO_PRESTAMO_REACTIVA_1):0;
                    return 'S/. ' + numeral(row.SOLICITUD_OFERTA).format('0,0')
                      + '( ' + getCoberturaMonto(soloferta+montorp1)*100 + '%)';
                  }
              },
              {
                  targets: 5,
                  data: null,
                  searchable: false,
                  sortable: false,
                  render: function(data, type, row) {
                    return "<span align='left' >Plazo : "+(row.SOLICITUD_PLAZO||'0')+" meses<br><span>Gracia : "+(row.SOLICITUD_GRACIA||'0')+" meses</span></span>";
                  }
              },
              {
                  targets: 6,
                  data: null,
                  searchable: false,
                  sortable: false,
                  render: function(data, type, row) {
                      $linea1="<span>Reporte Tributario Tercero: "+(row.FLG_REPORTE_TRIBUTARIO_TERCEROS|| '-')+"</span><br>";
                    return "<u><a class='gestionEvaluacion' ruc='"+  row.NUM_RUC +"'>"+$linea1+"</a></u>";
                  }
              },
              {
          				targets: 7,
          				data: 'WCL.NOMBRE',
          				name: 'WCL.NOMBRE',
          				visible: false,
          				searchable: true,
          				render: function(data, type, row) {
          					return row.NOMBRE;
          				}
              }
          ]
      });
  }

</script>
@stop
