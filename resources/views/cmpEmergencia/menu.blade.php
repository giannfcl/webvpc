@extends('Layouts.layout')
@section('pageTitle','CMP Emergencia')
@section('js-libs')
<link href="{{ URL::asset('css/datatables.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ URL::asset('css/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css">

<script type="text/javascript" src="{{ URL::asset('js/datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/numeral/numeral.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/multiselect.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.es.min.js') }}"></script>
@stop

@section('content')
<div class='content'>
	<div class='x_panel'>
		<div class="col-sm-3" style="padding-left: 0px;">
			{!! $menu !!}
		</div>

		<div class="col-sm-9" style="padding-left: 0px;padding-top:45px">
			@yield('datable')
			<!-- DATATABLE -->
		</div>
	</div>
</div>
<style>
	.dataTables_filter{
		margin-right: 50px
	}
	table,tr{
        font-size: 10px;
    }
	.loader {
		margin-top:10%;
		border: 16px solid #f3f3f3;
		border-radius: 50%;
		border-top: 16px solid #3498db;
		width: 120px !important;
		height: 120px !important;
		-webkit-animation: spin 2s linear infinite; /* Safari */
		animation: spin 2s linear infinite;
	}

	@-webkit-keyframes spin {
		0% { -webkit-transform: rotate(0deg); }
		100% { -webkit-transform: rotate(360deg); }
	}

	@keyframes spin {
		0% { transform: rotate(0deg); }
		100% { transform: rotate(360deg); }
	}
</style>
@stop

@section('js-scripts')

<script>
	function filterFloat(evt,input){
		// Backspace = 8, Enter = 13, ‘0′ = 48, ‘9′ = 57, ‘.’ = 46, ‘-’ = 43
		var key = window.Event ? evt.which : evt.keyCode;
		var chark = String.fromCharCode(key);
		var tempValue = input.value+chark;

		if ((key >= 48 && key <= 57) || key == 13 || key == 8 || key == 13 || key == 0 || key == 46)
			return true
		else
			return false

		
		if(key >= 48 && key <= 57){
			if(filter(tempValue)=== false){
				return false;
			}else{       
				return true;
			}
		}else{
			if(key == 8 || key == 13 || key == 0) {     
				return true;              
			}else if(key == 46){
					if(filter(tempValue)=== false){
						return false;
					}else{       
						return true;
					}
			}else{
				return false;
			}
		}
	}
	function filter(__val__){
		var preg = /^([0-9]+\.?[0-9]{0,2})$/; 
		if(preg.test(__val__) === true){
			return true;
		}else{
		return false;
		}
		
	}

	$('.input-number').on('input', function () { 
		this.value = this.value.replace(/[^0-9]/g,'');
	});

	function recalcularMonto(venta,deudaMicroPromedio){

        if (isNaN(parseFloat(venta))) {venta = 0} else {venta = parseFloat(venta)/4};
        if (isNaN(parseFloat(deudaMicroPromedio))) {deudaMicroPromedio = 0} else {deudaMicroPromedio = parseFloat(deudaMicroPromedio)*2};

        max = 0;
        if(venta >= deudaMicroPromedio){
            max = venta;
        }else{
            max = deudaMicroPromedio;
        }

        return Math.floor(max);
    }

	function getCobertura(monto){
		var cobertura = 0.0;
		if(monto <= 88200){
			cobertura = 0.98;
		}else if(monto <= 712500){
			cobertura = 0.95;
		}else if(monto <= 6750000){
			cobertura = 0.9;
		}else{
			cobertura = 0.8;
		}

		return cobertura;
	}

	function getCoberturaMonto(monto){
		cobertura = 0.0;
		if(monto<=90000){
			cobertura = 0.98;
		}else if(monto<=750000){
			cobertura = 0.95;
		}else if(monto<=7500000){
			cobertura = 0.9;
		}else if(monto<=10000000){
			cobertura = 0.8;
		}
		return cobertura;
	}

	function calculoReactiva2(venta,deudaMicro,reactiva1){
		var montoCalculado1 = recalcularMonto(venta,deudaMicro);
		var cobertura = getCobertura(montoCalculado1)
		var montoCalculado2 = (montoCalculado1 - getCoberturaMontoReactiva1(reactiva1))/cobertura

		if(montoCalculado2 >= 10000000) montoCalculado2 = 10000000
		if(montoCalculado2 <= 0) montoCalculado2 = 0
		return [montoCalculado2,cobertura]
	}

	function getCoberturaMontoReactiva1(monto){
		var c = 0;

		if (monto <= 0)
			return 0;

		if (monto <= 30000){
			c = 0.98
		}else if (monto <= 300000){
			c = 0.95
		}else if (monto <= 5000000){
			c = 0.90
		}else if (monto <= 10000000){
			c = 0.80
		}else{
			return 0
		}

		return monto*c

	}
	

	function ComboFlgreactiva1(flgtomoreactiva1){
		if ( flgtomoreactiva1 == null){
			$("#gflgReactiva1").prop('disabled',false)
			$('#gmontoReactiva1').prop('readonly',false)
			$("#bancor1").prop('disabled',false)
		}else{
			if (flgtomoreactiva1 == '1'){
				$("#gflgReactiva1").prop('disabled',false)
				$('#gmontoReactiva1').prop('readonly',false)
				$("#bancor1").prop('disabled',false)
			}
			if (flgtomoreactiva1 == '0'){
				$("#gflgReactiva1").prop('disabled',false)
				$("#gmontoReactiva1").val(0);
				$('#gmontoReactiva1').prop('readonly',true)
				$("#bancor1").val("");
				$("#bancor1").prop('disabled',true)
			}
			if (flgtomoreactiva1 == '2'){
				$("#gflgReactiva1").prop('disabled',false)
				$('#gmontoReactiva1').prop('readonly',false)
				$("#bancor1").prop('disabled',false)
			}
		}
	}

@if(isset($request['etapa']))
	$(".nombre_"+"{{$request['etapa']}}").removeClass("btn-success");
	$(".nombre_"+"{{$request['etapa']}}").addClass("btn-danger");
	$(".triangulo_"+"{{$request['etapa']}}").css("color","#d43f3a");
@endif
</script>
	@yield('js-vista')
@stop