@extends('cmpEmergencia.menu')

@section('datable')

<div>
  <table class='table table-striped table-bordered jambo_table' id = "datatable">
    <thead>
      <tr>
        <th>Empresa</th>
        <th>Oferta Solicitada</th>
        <th>Tasa</th>
        <th>Plazo / Gracia</th>
        <th>Fecha Aprobacion de Subasta</th>
        <th>Estado</th>
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modalEstadoDOC">
    <div class="modal-dialog" role="document" style="width: 1200px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Validacion</h4>
            </div>
            <div class="modal-body">
                <form id="formvaldoc" method="POST" class="form-horizontal form-label-left" enctype="multipart/form-data" action ="{{route('cmpEmergencia.enviar-valdoc')}}">
                    <input autocomplete="off" class="form-control input-number"  type="hidden" name="ratioRiesgos" value="{{ $ratioRiesgos }}">
                    <div class="form-group">
                        <div class="col-sm-6" >
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">RUC: </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input class="form-control"  type="text" name="numruc" readonly>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Correo:</label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input class="form-control editar" type="text" name="email" disabled>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Empresa:</label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input class="form-control"  type="text" name="cliente" disabled>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Garantía: </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input class="form-control" type="text" disabled id="garantia" name="garantia" >
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Monto Solicitado S/.: </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input class="form-control editar" type="text" name="soloferta" id="soloferta" disabled onkeypress="return filterFloat(event,this);" onpaste="return filterFloat(event,this);" maxlength="11">
                            </div>
                            <input hidden="hidden" name="riesgomonto" disabled>
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Riesgo Descubierto : </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input class="form-control" type="text" name="montoRiesgoDescubierto" id="montoRiesgoDescubierto" disabled>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Monto Garantizado : </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input class="form-control" type="text" name="montogarantizado" id="montogarantizado" disabled>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Plazo / Gracia : </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <select autocomplete="off" class="form-control editar" id="plazogracia" name="plazogracia" disabled>
                                    <option value="">Seleccionar una Opción</option>
                                    @foreach($plazosgracias as $plazogracia)
                                        <option value="{{$plazogracia[0]}}">{{$plazogracia[1]}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Tasa Final : </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input class="form-control" type="text" name="tasaclientefinal" disabled>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">¿TIENE DESEMBOLSO RP1?:</label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                            <select class="form-control editar" name="gflgReactiva1" id="gflgReactiva1" disabled>
                                <option value="">--Opciones--</option>
                                <option value="0">No</option>
                                <option value="1" disabled>Sí(Interbank)</option>
                                <option value="2">Sí(Otro banco)</option>
                            </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Ventas Anuales S/.: </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                              <input autocomplete="off" class="form-control editar"  name="volventa" disabled onkeypress="return filterFloat(event,this);" onpaste="return filterFloat(event,this);">
                            </div>
                        </div>
                        <div class="col-sm-6">
                          <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Banco dondé tomo RP1:</label>
                          <div class="col-sm-8   col-xs-12" style="padding-left:0px">
                            <select class="form-control editar" name="bancor1" id="bancor1" disabled>
                                <option value="">--Opciones--</option>
                                @foreach($bancos as $banco)
                                    <option value="{{$banco->CODIGO}}">{{$banco->DESCRIPCION}}</option>
                                @endforeach
                            </select>
                          </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Teléfono:</label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input class="form-control editar"  type="text" name="telefono" disabled onkeypress="return filterFloat(event,this);" onpaste="return filterFloat(event,this);">
                            </div>
                        </div>
                        <div class="col-sm-6">
                          <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Desembolso RP1 S/.: </label>
                          <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input autocomplete="off" class="form-control editar"  name="gmontoReactiva1" id="gmontoReactiva1" readonly onkeypress="return filterFloat(event,this);" onpaste="return filterFloat(event,this);">
                          </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-5 col-xs-12">Documentación Completa: </label>
                        <div class="col-sm-3 col-xs-12">
                            <select class="form-control" name="flgcompleto" required>
                                <option disabled selected value="">Seleccionar Opción</option>
                                <option value="1">SI</option>
                                <option value="0">NO</option>
                            </select>
                        </div>
                    </div>
                    <hr style="width:100%;">
                    <div class="form-group">
                        <div class="col-sm-2">
                            <ul class="nav nav-pills" id="navs">
                                @if(count($vistasdocs)>0)
                                    @foreach($vistasdocs as $vistadoc)
                                        <li class="{{$vistadoc['ID']=='1'?'active':''}}" style="width: 160px;margin-top: 10px">
                                            <a class="col-sm-10" id="v-{{$vistadoc['ID']}}-tab" data-toggle="tab" href="#v-{{$vistadoc['ID']}}">{{$vistadoc['NOMBRE']}}</a>
                                            <div class="col-sm-2 {{$vistadoc['ID']=='2'?'active':''}}" style="padding-top:6px;">
                                                &#x25B6
                                            </div>
                                        </li><br>
                                    @endforeach
                                @else
                                    <h5>No se encontro vistas</h5>
                                @endif
                            </ul>
                        </div>
                        <div class="col-sm-10 tab-content" style="padding-left: 0px">
                            @if(count($vistasdocs)>0)
                                @foreach($vistasdocs as $vistadoc)
                                    <div class="tab-pane fade{{$vistadoc['ID']=='1'?' in active':''}}" id="v-{{$vistadoc['ID']}}">
                                        @if($vistadoc['ID']=='1')
                                            <div class="col-sm-12">
                                                <label>1)El cliente está comprendido en Ley 30737</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i style="padding-right: 30%;" class="fa fa-question-circle" aria-hidden="true"  data-toggle="tooltip" data-placement="right" title="(Lava Jato o Club de la Construcción). Consultar pantalla de MINJUS para asegurarte que no se encuentre en estas 3 listas"></i><br>
                                                <label>2)Acuerdo de llenado de Pagaré R2</label><br>
                                                <label>3)Pagaré R2</label><br>
                                                <label>4)Acuerdo de Crédito - Solicitud de Desembolso y DJ</label><br>
                                            </div>
                                        @elseif($vistadoc['ID']=='2')
                                            <div class="col-sm-6">
                                                <label>1)No debe tener deudas tributarias <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;administradas por la SUNAT</label><i style="padding-right: 30%;" class="fa fa-question-circle" aria-hidden="true"  data-toggle="tooltip" data-placement="right" title="Por períodos anteriores al año 2020, exigibles en cobranza coactiva que totalicen un importe mayor a 1 UIT al momento de solicitar el crédito"></i><br>
                                                <label>2)Al 29 Feb 2020 en el RCC el 90% de la deuda <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;debe estar con calificación Normal o CPP</label><i style="padding-right: 30%;" class="fa fa-question-circle" aria-hidden="true"  data-toggle="tooltip" data-placement="right" title="(Pueden acceder empresas sin calificación a la fecha, siempre y cuando durante los últimos 12 meses hayan estado en Normal en el RCC o no hayan tenido clasificación en los últimos 12 meses). Adjunta el Sentinel o Infocorp"></i><br>
                                                <label>3)El cliente está comprendido en Ley 30737</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i style="padding-right: 30%;" class="fa fa-question-circle" aria-hidden="true"  data-toggle="tooltip" data-placement="right" title="(Lava Jato o Club de la Construcción). Consultar pantalla de MINJUS para asegurarte que no se encuentre en estas 3 listas"></i><br>
                                                <label>4)El cliente está vinculado a Intercorp</label><br>
                                                <label>5)Verificar que no pertenezca a la lista <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;de exclusión</label><i style="padding-right: 30%;" class="fa fa-question-circle" aria-hidden="true"  data-toggle="tooltip" data-placement="right" title="(lista estará en la Web - Anexo 1  del Reglamento)"></i><br>
                                            </div>
                                            <div class="col-sm-6">
                                                <label>6)Adjuntar reporte tributario a terceros</label><br>
                                                <label>7)Ficha SPLAFT</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i style="padding-right: 30%;" class="fa fa-question-circle" aria-hidden="true"  data-toggle="tooltip" data-placement="right" title="de acuerdo al formato establecido por IBK validado por COFIDE"></i><br>
                                                <label>8)Acuerdo de llenado de Pagaré R2</label><br>
                                                <label>9)Pagaré R2</label><br>
                                                <label>10)Acuerdo de Crédito - Solicitud de Desembolso y DJ​</label><br>
                                            </div>
                                            <div class="col-sm-4">
                                            </div>
                                        @endif
                                    </div>
                                @endforeach
                            @else
                                No se encontraron vistas de validación de documentación
                            @endif
                        </div>
                    </div>
                    <input type="hidden" name="flgenvrevision" value="0" />
                    <center>
                        <button class="btn btn-primary botoneditar" type="button" valor="1">Editar</button>
                        <button class="guardar btn btn-success" type="submit">Guardar</button>
                        <button class="btnenval btn btn-success" type="submit" disabled>Enviar</button>
                    </center>
                </form>
            </div>
        </div>
    </div>
</div>
@stop

@section('js-vista')
<style>
    #modalEstadoDOC input,select{
        margin-bottom: -15px;
    }
    #navs li.active>a{
        background-color: #f0ad4e !important;
        color: #73879C !important;
    }
    #navs li>a{
        background-color: #f0ad4e !important;
    }
    #navs li.active>div{
        color: #164BC5 !important;
        visibility: visible;
    }
    #navs li>div{
        visibility: hidden;
    }
    .tooltip-inner {
        max-width: 350px;
        width: 350px;
    }
</style>
<script>
    configurarTabla("{{$request['etapa']}}");
    $('[data-toggle="tooltip"]').tooltip();
    var plazosgracias = <?php echo json_encode($plazosgracias); ?>;
    var pla_gra={};
    $.each(plazosgracias, function( i,v,a) {
        pla_gra[v[0]]=v[1];
    });

    var gestionesfiles = <?php echo json_encode($gestionesfiles); ?>;
    var gfiles={};
    $.each(gestionesfiles, function( i,v,a) {
        gfiles[v["ID"]]=v["NOMBRE"];
    });

    $(document).on("click",".botoneditar",function() {
        if ($(this).attr("valor")=="1") {
            $(".editar").prop("disabled",false);
            $(this).attr("valor","0");
            $(this).html("Cancelar");
            ComboFlgreactiva1($("#gflgReactiva1").val());
        }else{
            $(".editar").prop("disabled",true);
            $(this).attr("valor","1");
            $(this).html("Editar");
        }
    });
    $("#gflgReactiva1").change(function(event) {
        ComboFlgreactiva1($("#gflgReactiva1").val());
    });


    function habilitarboton(){
        solicitud = numeral($("#formvaldoc input[name='soloferta']").val()).value();
        calculado = recalcularMonto($("#formvaldoc input[name='volventa']").val(),$("#formvaldoc input[name='gmontoReactiva1']").val());
        riesgos = numeral($('#formvaldoc input[name="riesgomonto"]').val()).value();
        ratio = numeral($('#formvaldoc input[name="ratioRiesgos"]').val()).value();
        if(solicitud > calculado || solicitud > riesgos * ratio){
            $(".btnenval").attr("disabled","disabled");
            $(".guardar").attr("disabled","disabled");
        }else{
            console.log('aprobado!');
            $(".btnenval").prop("disabled",false);
            $(".guardar").removeAttr("disabled",false);
        }
    }

    $("#formvaldoc input[name='volventa']").keyup(function() {
        habilitarboton();
    });

    $("#formvaldoc input[name='soloferta']").bind('paste', function(e) {
        solicitud = numeral($("#formvaldoc input[name='soloferta']").val()).value();
        calculado = recalcularMonto($("#formvaldoc input[name='volventa']").val(),$("#formvaldoc input[name='gmontoReactiva1']").val());
        riesgos = numeral($('#formvaldoc input[name="riesgomonto"]').val()).value();
        ratio = numeral($('#formvaldoc input[name="ratioRiesgos"]').val()).value();
        var ctl = $(this);
        setTimeout(function() {
            if(solicitud > calculado || solicitud > riesgos * ratio){
                $(".btnenval").attr("disabled","disabled");
                $(".guardar").attr("disabled","disabled");
            }else{
                $(".btnenval").removeAttr("disabled");
                $(".guardar").removeAttr("disabled");
            }
        }, 100);
    })

    $(document).on("click",".btnenval",function(){
        $('#formvaldoc input[name="flgenvrevision"]').val(1);
    });

    $(document).on("change","#formvaldoc select[name='flgcompleto']",function(){
        checkGtp();
    });

    function checkGtp(){
        if ($('#formvaldoc select[name="flgcompleto"]').val() == 1){
            $( ".btnenval" ).prop( "disabled", false);
        }else{
            $( ".btnenval" ).prop( "disabled", true);
        }
    }
    $(document).on("click",".verdocumentacion",function(){
        $("#modalEstadoDOC").modal();
        $(".editar").prop("disabled",true);
        $(".botoneditar").attr("valor","1");
        $(".botoneditar").html("Editar");
        $('#formvaldoc input[name="gmontoReactiva1"]').removeAttr("readonly");
        $('#formvaldoc input[name="gmontoReactiva1"]').prop("disabled",true);
        var ruc = $(this).attr('ruc')
        $.ajax({
            url: "{{ route('cmpEmergencia.buscarinfo') }}",
            type: 'GET',
            data: {
                numruc: ruc
            },
            beforeSend: function() {
                $("#cargando").modal();
            },
            success: function (result) {
                console.log(result);
                $('#formvaldoc input[name="numruc"]').val(result['NUM_RUC']);
                $('#formvaldoc input[name="cliente"]').val(result['NOMBRE']);
                $('#formvaldoc input[name="telefono"]').val(result['TELEFONO1']);
                $('#formvaldoc input[name="email"]').val(result['CORREO']);
                $('#formvaldoc input[name="soloferta"]').val(result['SOLICITUD_OFERTA']||'0');
                $('#formvaldoc input[name="volventa"]').val(result['SOLICITUD_VOLUMEN_VENTA']||'0');
                $('#formvaldoc select[name="gflgReactiva1"]').val(result['FLG_TOMO_REACTIVA_1']);
                $('#formvaldoc select[name="bancor1"]').val(result['BANCO_REACTIVA_1']);
                $('#formvaldoc input[name="gmontoReactiva1"]').val(result['MONTO_PRESTAMO_REACTIVA_1']);

                $monto = recalcularMonto(result['SOLICITUD_VOLUMEN_VENTA'],result['MONTO_PRESTAMO_REACTIVA_1']);
                $('#formvaldoc input[name="riesgomonto"]').val(result['RIESGOS_MONTO']||0);

                soloferta = isNaN(parseFloat($('#formvaldoc input[name="soloferta"]').val()))?0:parseFloat($('#formvaldoc input[name="soloferta"]').val());
                montorp1 = isNaN(parseFloat($('#formvaldoc input[name="gmontoReactiva1"]').val()))?0:parseFloat($('#formvaldoc input[name="gmontoReactiva1"]').val());

                monto_para_garantia = soloferta+montorp1;
                garantia = getCoberturaMonto(monto_para_garantia);
                $('#formvaldoc input[name="garantia"]').val(garantia*100 + '%');
                $('#montogarantizado').val('S/. ' + numeral(garantia*soloferta).format('0,0'));
                $('#formvaldoc input[name="montoRiesgoDescubierto"]').val('S/. ' + numeral((1-garantia)*soloferta).format('0,0'));

                $('#formvaldoc select[name="plazogracia"]').val(result['SOLICITUD_PLAZO']+'-'+result['SOLICITUD_GRACIA']);

                $('#formvaldoc select[name="flgcompleto"]').val(result['DOCUMENTACION_COMPLETA']||'0');
                $('#formvaldoc input[name="tasaclientefinal"]').val(result['TASA_FINAL_CLIENTE']||'0');

                console.log(result['ESTADO_FILE'])
                if($.inArray(result['ESTADO_FILE'],["1","3"])>=0){
                    $("#formvaldoc select[name='flgcompleto']").prop("disabled",true);
                    $( ".guardar" ).prop( "disabled", true);
                    $( ".btnenval" ).prop( "disabled", true);
                }else{
                    $("#formvaldoc select[name='flgcompleto']").prop("disabled",false);
                    $( ".guardar" ).prop( "disabled", false);
                    $( ".btnenval" ).prop( "disabled", false);
                    checkGtp();
                }
                habilitarboton();
            },
            complete: function() {
                $(".cerrarcargando").click();
            }
        });
    });

    $('#formvaldoc input[name="soloferta"]').keyup(function() {
        montorp1 = isNaN(parseFloat($('#formvaldoc input[name="gmontoReactiva1"]').val()))?0:parseFloat($('#formvaldoc input[name="gmontoReactiva1"]').val());
        soloferta = isNaN(parseFloat($(this).val()))?0:parseFloat($(this).val());
        garantia = getCoberturaMonto(parseFloat(soloferta+montorp1));
        $('#formvaldoc input[name="garantia"]').val(numeral(garantia*100).format('0,00') + '%');
        $('#montogarantizado').val('S/. ' + numeral(garantia*soloferta).format('0,00'));
        $('#formvaldoc input[name="montoRiesgoDescubierto"]').val('S/. ' + numeral((1-garantia)*soloferta).format('0,00'));

        habilitarboton();
    });

    $('#formvaldoc input[name="gmontoReactiva1"]').keyup(function() {
        montorp1 = isNaN(parseFloat($(this).val()))?0:parseFloat($(this).val());
        soloferta = isNaN(parseFloat($('#formvaldoc input[name="soloferta"]').val()))?0:parseFloat($('#formvaldoc input[name="soloferta"]').val());
        garantia = getCoberturaMonto(parseFloat(soloferta+montorp1));
        $('#formvaldoc input[name="garantia"]').val(numeral(garantia*100).format('0,00') + '%');
        $('#montogarantizado').val('S/. ' + numeral(garantia*soloferta).format('0,00'));
        $('#formvaldoc input[name="montoRiesgoDescubierto"]').val('S/. ' + numeral((1-garantia)*soloferta).format('0,00'));
    });

    function configurarTabla(datos=null){
        if ($.fn.dataTable.isDataTable('#datatable')) {
            $('#datatable').DataTable().destroy();
        }

        $('#datatable').DataTable({
            processing: true,
            "bAutoWidth": false,
            rowId: 'staffId',
            // dom: 'Blfrtip',
            serverSide: true,
            language: {"url": "{{ URL::asset('js/Json/Spanish.json') }}"},
            ajax: {
                    "type": "GET",
                    "url": "{{ route('cmpEmergencia.lista') }}",
                    data: function(data) {
                            data.etapa = datos;
                    }
            },
            "aLengthMenu": [
                    [25, 50, -1],
                    [25, 50, "Todo"]
            ],
            "iDisplayLength": 25,
            "order": [
                    [0, "desc"]
            ],
            columnDefs: [
                {
                    targets: 0,
                    data: 'WCL.NUM_RUC',
                    name: 'WCL.NUM_RUC',
                    render: function(data, type, row) {
                        return row.NUM_RUC + '<br/>' + row.NOMBRE;
                    }
                },
                {
                    targets: 1,
                    data: null,
                    searchable: false,
                    sortable:false,
                    render: function(data, type, row) {
                        return 'S/ ' + (row.SOLICITUD_OFERTA||'-')
                    }
                },
                {
                    targets: 2,
                    data: null,
                    searchable: false,
                    sortable:false,
                    render: function(data, type, row) {
                        if(row.TASA_FINAL_CLIENTE==null){
                            return "-";
                        }
                        return row.TASA_FINAL_CLIENTE+" %";
                    }
                },
                {
                    targets: 3,
                    data: null,
                    searchable: false,
                    sortable:false,
                    render: function(data, type, row) {
                        return "<span align='left' >Plazo : "+(row.SOLICITUD_PLAZO||'0')+" meses<br><span>Gracia : "+(row.SOLICITUD_GRACIA||'0')+" meses</span></span>";
                    }
                },
                {
                    targets: 4,
                    data: null,
                    searchable: false,
                    sortable:false,
                    render: function(data, type, row) {
                        if (row.FECHA_APROBACIO_SUBASTA == null){
                            return "-";
                        }
                        return row.FECHA_APROBACIO_SUBASTA;
                    }
                },
                {
                    targets: 5,
                    data: null,
                    searchable: false,
                    sortable:false,
                    render: function(data, type, row) {
                        if(row.ESTADO_FILE==null){
                            return "<u><a class='verdocumentacion' ruc='"+row.NUM_RUC+"'>PENDIENTE</a></u>";
                        }
                        return "<u><a class='verdocumentacion' ruc='"+row.NUM_RUC+"'>"+gfiles[row.ESTADO_FILE]+"</a></u>";
                    }
                },
                {
					targets: 6,
					data: 'WCL.NOMBRE',
					name: 'WCL.NOMBRE',
					visible: false,
					searchable: true,
					render: function(data, type, row) {
						return row.NOMBRE;
					}
                }
            ]
        });
    }
</script>
@stop
