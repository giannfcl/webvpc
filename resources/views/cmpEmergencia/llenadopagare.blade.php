<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Acuerdo de Llenado</title>

    </head>
    <style>
        .page_break{
            page-break-before: always;
        }
        .underline{
            border-bottom-style: solid !important;
            border-bottom-width: 0.05px !important;
            margin-bottom: 5px;
            padding-left:5px;
            padding-right: 5px;
        }
        .variable{
            border-bottom-style: solid;
        border-left-style: solid;
        border-right-style: solid;
        border-width: 0.5px;
        padding-left: 10px;
        margin-bottom:0;
        padding-right: 10px;
        }
        .space{
            width:10px;
        }
                 body
  {

        margin:0;
    font-family:-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";
        font-size:1rem;
        font-weight:400;line-height:1.5;color:#212529;text-align:left;
        background-color:#fff;
        margin-left: 90px;
        margin-right: 90px;
        margin-top: 8px;
        margin-bottom: 8px;
        margin-left: 0px;
        margin-right: 0px;
        margin-top: -30px;
        margin-bottom: -30px;
        }
  p {
        margin-top:0.25;
        margin-bottom:0.25em;
        margin-bottom: 0.25em;

        /*word-spacing: 0.3em;
        line-height: 9px;*/
    }
       table{
    border-collapse:collapse;

    }
    </style>
    <body class="nav-md" style="font-size: 6.55pt; margin: 0 !important;">
        <div style="
            max-width: 100vw;
            padding: 0px 0px 0px 0px;">
            <div style="padding-left:20px" class="row">
                <img style="width: 15%" src="{{ URL::asset('img/logoIBK.png') }}" alt="">
            </div>
            <div style=" text-align: center; font-size: 8pt;margin-bottom:5px;">
                <strong>Banca Pequeña Empresa</strong> <br>
                <strong>Acuerdo de Llenado de Pagaré</strong>
            </div>
            <div>
            <p style="margin-bottom:0;text-align: justify;">Conste por el presente documento, el ACUERDO DE LLENADO DE PAGARE que suscriben: <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></p>
            <div class="underline"> {{$datos->NOMBRE}}</div>
            <p style="text-align: justify;">
            (en adelante denominado EL CLIENTE) y BANCO INTERNACIONAL DEL PERU S.A.A. (en adelante denominado INTERBANK). <br>
            EL CLIENTE ha solicitado a INTERBANK uno ó más créditos, y en representación o como instrumento de cobro de dichas obligaciones, ha suscrito un Pagaré incompleto a favor de INTERBANK; por lo que mediante la firma del presente documento, EL CLIENTE e INTERBANK acuerdan de manera expresa e irrevocable, que INTERBANK queda autorizado para proceder con el llenado del mencionado Pagaré incompleto en los siguientes términos y condiciones: <br>
            <span class="underline"><strong> PRIMERO: </strong></span>AUTORIZACION DE LLENADO DE PAGARE <br>
            INTERBANK podrá proceder al llenado del Pagaré y en consecuencia a la ejecución del mismo,y de ser el caso automáticamente vencerán todos los plazos,en cualquiera de los siguientes casos: <br>
            a) Si EL CLIENTE incumpliese con el pago debido y oportuno de las obligaciones contraídas con INTERBANK y representadas en el Pagaré. <br>
            b) Si INTERBANK honrara el crédito indirecto representado en el Pagaré. <br>
            c) Si EL CLIENTE es declarado en concurso, suspende sus pagos o acuerda disolverse o liquidarse. <br>
            d) Si INTERBANK detectase falsedad en la información suministrada por EL CLIENTE.<br>
            e) Si EL CLIENTE de un modo general dejara de cumplir cualquiera de las obligaciones contraídas con INTERBANK.<br>
            <span class="underline"><strong> SEGUNDO: </strong></span>: INSTRUCCIONES DEL LLENADO DEL PAGARE INCOMPLETO <br>
            INTERBANK queda autorizado a llenar el Pagaré de acuerdo a lo siguiente: <br>
            a)  <span class="underline"><strong> Fecha de emisión: </strong></span>  Será la fecha de efectuado el desembolso o en la que se hubiera efectuado el último pago de intereses del crédito representado en el Pagaré, o la de honramiento del crédito indirecto representado en el Pagaré, o aquella en la cual INTERBANK proceda a hacer uso de las facultades otorgadas por el presente documento, a criterio de INTERBANK. <br>
            b)  <span class="underline"><strong> Fecha de vencimiento: </strong></span>Será la fecha de vencimiento del crédito representado en el Pagaré, o a partir del día siguiente de efectuado el honramiento del crédito indirecto representado en el Pagaré, o la fecha en que INTERBANK dé por vencidos todos los plazos de acuerdo a lo señalado en la Cláusula Primera, o podrá ser a la vista, a criterio de INTERBANK. <br>
            c)  <span class="underline"><strong> Importe: </strong></span>La cantidad que INTERBANK deberá consignar en el documento será equivalente al saldo de capital en la fecha de emisión del Pagaré. INTERBANK queda autorizado, de ser necesario, para colocar un sello en el reverso del Pagaré indicando el importe total adeudado en la fecha de llenado del Pagaré. <br>
            d) <span class="underline"><strong> Tasas de interés: </strong></span> Las tasas de interés compensatorio y moratorio fijadas por INTERBANK.<br>
            EL CLIENTE acepta y da por válidas todas las renovaciones y prórrogas totales o parciales que se anoten en el Pagaré, aún cuando no ésten suscritas por EL CLIENTE. <br>
            El presente documento constituye el acuerdo entre EL CLIENTE e INTERBANK, al amparo del artículo 10º de la Ley de Títulos Valores (Ley 27287). Si EL CLIENTE hubiera suscrito un contrato con Interbank, acordando condiciones distintas a las fijadas en este Acuerdo de Llenado de Pagaré, prevalecerán las condiciones del contrato. <br>
            <span class="underline"><strong> TERCERO: </strong></span>COPIA DEL PAGARE<br>
            EL CLIENTE deja expresa constancia que ha recibido de INTERBANK copia del Pagaré incompleto, en el momento en que INTERBANK recibió el mencionado Pagaré incompleto. <br>
            <span class="underline"><strong> CUARTO: </strong></span>RENUNCIA A TRANSFERENCIA DEL PAGARE<br>
            EL CLIENTE renuncia expresamente a la inclusión de una cláusula que impida o limite la libre negociación del Pagaré.<br>
            <span class="underline"><strong> QUINTO: </strong></span>AVALES<br>
            Los Avales de EL CLIENTE aceptan todas y cada una de las condiciones y términos señalados en el presente documento. <br>
            Se deja constancia que la información proporcionada por EL CLIENTE y LOS AVALES en el presente documento, tiene el carácter de declaración jurada, de acuerdo con el artículo 179 de la Ley Nº 26702.<br>
             <br>
             <span class="underline"><strong> EL CLIENTE </strong></span><br>
             <br>
            </p>
            <table style="width:100%; margin-bottom:5px;">
                <tr>
                    <td class="variable"><span style="padding-right:1px">firma(s) </span><strong></strong></td>
                </tr>
            </table>
            <table style="width:100%; margin-bottom:5px;">
                <tr>
                    <td>
                        <p style="margin-bottom: 0px;">(para persona jurídica: representante(s) legal(es) del cliente / para persona natural: cliente y cónyuge)</p>
                    </td>
                </tr>
            </table>
            <table  style="width:100%; margin-bottom:5px;">
                <tr>
                    <td class="variable"><span style="padding-right:1px">razón social / nombre </span><strong> {{$datos->NOMBRE}}</strong></td>
                </tr>
            </table>
            <table  style="width:100%; margin-bottom:5px;">
                <tr>
                    <td class="variable"><span style="padding-right:1px">d.o.i. </span>
                        <strong>
                            @if(substr($datos->NUM_RUC,0,2)!=20)
                                {{$datos->RRLL_DOCUMENTO}}
                            @endif
                             @if(substr($datos->NUM_RUC,0,2)==20)
                                {{$datos->NUM_RUC}}
                            @endif
                        </strong>
                    </td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">estado civil</span>
                        <strong>
                             @if(substr($datos->NUM_RUC,0,2)!=20)
                                @if($datos->RRLL_ESTADO_CIVIL == 1)
                                    Casado(a)
                                @endif

                                @if($datos->RRLL_ESTADO_CIVIL == 2)
                                    Conviviente(a)
                                @endif

                                @if($datos->RRLL_ESTADO_CIVIL == 3)
                                    Soltero
                                @endif

                                @if($datos->RRLL_ESTADO_CIVIL == 4)
                                    Viudo(a)
                                @endif

                                 @if($datos->RRLL_ESTADO_CIVIL == 5)
                                    Divorciado(a)
                                @endif

                                 @if($datos->RRLL_ESTADO_CIVIL == 6)
                                    Separado
                                @endif
                            @endif
                        </strong>
                    </td>
                </tr>
            </table>
            <table  style="width:100%; margin-bottom:5px;">
                <tr>
                    <td class="variable"><span style="padding-right:1px">domicilio </span>
                        <strong>  {{$direccion ? $direccion[0]->DIRECCION : ''}}</strong>
                    </td>
                </tr>
            </table>
            <table  style="width:100%; margin-bottom:5px;">
                <tr>
                    <td class="variable"><span style="padding-right:1px; width:400px">nombre representante legal / cónyuge </span>
                        <strong>
                            @if(in_array(substr($datos->NUM_RUC,0,2),['10','15']) && in_array($datos->RRLL_ESTADO_CIVIL,['1']))
                              {{$datos->RRLL_APELLIDOS_2}}  {{$datos->RRLL_NOMBRES_2}}
                            @endif
                            @if(substr($datos->NUM_RUC,0,2)==20)
                              {{$datos->RRLL_APELLIDOS}}  {{$datos->RRLL_NOMBRES}}
                            @endif
                        </strong>
                    </td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">d.o.i.</span>
                        <strong>
                            @if(in_array(substr($datos->NUM_RUC,0,2),['10','15']) && in_array($datos->RRLL_ESTADO_CIVIL,['1']))
                              {{$datos->RRLL_DOCUMENTO_2}}
                            @endif
                            @if(substr($datos->NUM_RUC,0,2)==20)
                                {{$datos->RRLL_DOCUMENTO}}
                            @endif
                        </strong>
                    </td>
                </tr>
            </table>
            <table  style="width:100%; margin-bottom:5px;">
                <tr>
                    <td class="variable"><span style="padding-right:1px; width:400px">nombre representante legal</span>
                        <strong>
                            @if(substr($datos->NUM_RUC,0,2)==20)
                                {{$datos->RRLL_APELLIDOS_2}} {{$datos->RRLL_NOMBRES_2}}
                            @endif
                        </strong>
                    </td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">d.o.i.</span>
                        <strong>
                         @if(substr($datos->NUM_RUC,0,2)==20)
                            {{$datos->RRLL_DOCUMENTO_2}}
                         @endif
                        </strong>
                    </td>
                </tr>
            </table>
            <span class="underline"><strong> AVALES </strong></span><br>
            <br>
            <table style="width:100%; margin-bottom:5px;">
                <tr>
                    <td class="variable"><span style="padding-right:1px">firma(s) </span><strong>  </strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">firma(s) </span><strong>  </strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">firma(s) </span><strong>  </strong></td>
                </tr>
                <tr>
                    <td>
                        <p style="margin-bottom: 0px;">(aval: persona natural / persona jurídica)</p>
                    </td>
                    <td class="space"></td>
                    <td>
                        <p style="margin-bottom: 0px;">(aval: persona natural / persona jurídica)</p>
                    </td>
                    <td class="space"></td>
                    <td>
                        <p style="margin-bottom: 0px;">(aval: persona natural / persona jurídica)</p>
                    </td>
                </tr>
            </table>
            <table style="width:100%; margin-bottom:5px;">
                <tr>
                    <td class="variable"><span style="padding-right:1px">razón soc. / nombre </span><strong> </strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">razón soc. / nombre </span><strong> </strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">razón soc. / nombre </span><strong> </strong></td>
                </tr>
            </table>
            <table style="width:100%; margin-bottom:5px;">
                <tr>
                    <td class="variable"><span style="padding-right:1px">d.o.i.</span><strong>  </strong></td>
                    <td class="space"></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">estado civil </span><strong></strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">d.o.i.</span><strong> </strong></td>
                    <td class="space"></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">estado civil</span><strong>  </strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">d.o.i.</span><strong>  </strong></td>
                    <td class="space"></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">estado civil</span><strong> </strong></td>
                </tr>
            </table>
            <table style="width:100%; margin-bottom:5px;">
                <tr>
                    <td class="variable"><span style="padding-right:1px">domicilio </span><strong> </strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">domicilio </span><strong>  </strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">domicilio </span><strong>  </strong></td>
                </tr>
            </table>
            <table style="width:100%; margin-bottom:5px;">
                <tr>
                    <td class="variable"><span style="padding-right:1px">rep. legal / cónyuge </span><strong> </strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">rep. legal / cónyuge </span><strong></strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">rep. legal / cónyuge </span><strong>  </strong></td>
                </tr>
            </table>
            <table style="width:100%; margin-bottom:5px;">
                <tr>
                    <td class="variable"><span style="padding-right:1px">d.o.i. </span><strong>  </strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">d.o.i. </span><strong> </strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">d.o.i. </span><strong>  </strong></td>
                </tr>
            </table>
            <table style="width:100%; margin-bottom:5px;">
                <tr>
                    <td class="variable"><span style="padding-right:1px">rep. Legal </span><strong>  </strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">rep. Legal </span><strong>  </strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">rep. Legal </span><strong>  </strong></td>
                </tr>
            </table>
            <table style="width:100%; margin-bottom:0x;">
                <tr>
                    <td class="variable"><span style="padding-right:1px">d.o.i. </span><strong> </strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">d.o.i. </span><strong></strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">d.o.i. </span><strong>  </strong></td>
                </tr>
            </table>
            </div>
        </div>
        <div style="position: absolute; bottom: -30px; width:700px; padding-right:100px; text-align:right;font-size:6.5pt;">
        Copia Banco
        </div>
        <div class="page_break"></div>
        <div style="
            max-width: 100vw;
            padding: 0px 0px 0px 0px;">
            <div style="padding-left:20px" class="row">
                <img style="width: 15%" src="{{ URL::asset('img/logoIBK.png') }}" alt="">
            </div>
            <div style=" text-align: center; font-size: 8pt;margin-bottom:5px;">
                <strong>Banca Pequeña Empresa</strong> <br>
                <strong>Acuerdo de Llenado de Pagaré</strong>
            </div>
            <div>
            <p style="margin-bottom:0;text-align: justify;">Conste por el presente documento, el ACUERDO DE LLENADO DE PAGARE que suscriben: <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></p>
            <div class="underline"> {{$datos->NOMBRE}}</div>
            <p style="text-align: justify;">
            (en adelante denominado EL CLIENTE) y BANCO INTERNACIONAL DEL PERU S.A.A. (en adelante denominado INTERBANK). <br>
            EL CLIENTE ha solicitado a INTERBANK uno ó más créditos, y en representación o como instrumento de cobro de dichas obligaciones, ha suscrito un Pagaré incompleto a favor de INTERBANK; por lo que mediante la firma del presente documento, EL CLIENTE e INTERBANK acuerdan de manera expresa e irrevocable, que INTERBANK queda autorizado para proceder con el llenado del mencionado Pagaré incompleto en los siguientes términos y condiciones: <br>
            <span class="underline"><strong> PRIMERO: </strong></span>AUTORIZACION DE LLENADO DE PAGARE <br>
            INTERBANK podrá proceder al llenado del Pagaré y en consecuencia a la ejecución del mismo,y de ser el caso automáticamente vencerán todos los plazos,en cualquiera de los siguientes casos: <br>
            a) Si EL CLIENTE incumpliese con el pago debido y oportuno de las obligaciones contraídas con INTERBANK y representadas en el Pagaré. <br>
            b) Si INTERBANK honrara el crédito indirecto representado en el Pagaré. <br>
            c) Si EL CLIENTE es declarado en concurso, suspende sus pagos o acuerda disolverse o liquidarse. <br>
            d) Si INTERBANK detectase falsedad en la información suministrada por EL CLIENTE.<br>
            e) Si EL CLIENTE de un modo general dejara de cumplir cualquiera de las obligaciones contraídas con INTERBANK.<br>
            <span class="underline"><strong> SEGUNDO: </strong></span>: INSTRUCCIONES DEL LLENADO DEL PAGARE INCOMPLETO <br>
            INTERBANK queda autorizado a llenar el Pagaré de acuerdo a lo siguiente: <br>
            a)  <span class="underline"><strong> Fecha de emisión: </strong></span>  Será la fecha de efectuado el desembolso o en la que se hubiera efectuado el último pago de intereses del crédito representado en el Pagaré, o la de honramiento del crédito indirecto representado en el Pagaré, o aquella en la cual INTERBANK proceda a hacer uso de las facultades otorgadas por el presente documento, a criterio de INTERBANK. <br>
            b)  <span class="underline"><strong> Fecha de vencimiento: </strong></span>Será la fecha de vencimiento del crédito representado en el Pagaré, o a partir del día siguiente de efectuado el honramiento del crédito indirecto representado en el Pagaré, o la fecha en que INTERBANK dé por vencidos todos los plazos de acuerdo a lo señalado en la Cláusula Primera, o podrá ser a la vista, a criterio de INTERBANK. <br>
            c)  <span class="underline"><strong> Importe: </strong></span>La cantidad que INTERBANK deberá consignar en el documento será equivalente al saldo de capital en la fecha de emisión del Pagaré. INTERBANK queda autorizado, de ser necesario, para colocar un sello en el reverso del Pagaré indicando el importe total adeudado en la fecha de llenado del Pagaré. <br>
            d) <span class="underline"><strong> Tasas de interés: </strong></span> Las tasas de interés compensatorio y moratorio fijadas por INTERBANK.<br>
            EL CLIENTE acepta y da por válidas todas las renovaciones y prórrogas totales o parciales que se anoten en el Pagaré, aún cuando no ésten suscritas por EL CLIENTE. <br>
            El presente documento constituye el acuerdo entre EL CLIENTE e INTERBANK, al amparo del artículo 10º de la Ley de Títulos Valores (Ley 27287). Si EL CLIENTE hubiera suscrito un contrato con Interbank, acordando condiciones distintas a las fijadas en este Acuerdo de Llenado de Pagaré, prevalecerán las condiciones del contrato. <br>
            <span class="underline"><strong> TERCERO: </strong></span>COPIA DEL PAGARE<br>
            EL CLIENTE deja expresa constancia que ha recibido de INTERBANK copia del Pagaré incompleto, en el momento en que INTERBANK recibió el mencionado Pagaré incompleto. <br>
            <span class="underline"><strong> CUARTO: </strong></span>RENUNCIA A TRANSFERENCIA DEL PAGARE<br>
            EL CLIENTE renuncia expresamente a la inclusión de una cláusula que impida o limite la libre negociación del Pagaré.<br>
            <span class="underline"><strong> QUINTO: </strong></span>AVALES<br>
            Los Avales de EL CLIENTE aceptan todas y cada una de las condiciones y términos señalados en el presente documento. <br>
            Se deja constancia que la información proporcionada por EL CLIENTE y LOS AVALES en el presente documento, tiene el carácter de declaración jurada, de acuerdo con el artículo 179 de la Ley Nº 26702.<br>
             <br>
             <span class="underline"><strong> EL CLIENTE </strong></span><br>
             <br>
            </p>
            <table style="width:100%; margin-bottom:5px;">
                <tr>
                    <td class="variable"><span style="padding-right:1px">firma(s) </span><strong></strong></td>
                </tr>
            </table>
            <table style="width:100%; margin-bottom:5px;">
                <tr>
                    <td>
                        <p style="margin-bottom: 0px;">(para persona jurídica: representante(s) legal(es) del cliente / para persona natural: cliente y cónyuge)</p>
                    </td>
                </tr>
            </table>
            <table  style="width:100%; margin-bottom:5px;">
                <tr>
                    <td class="variable"><span style="padding-right:1px">razón social / nombre </span><strong> {{$datos->NOMBRE}}</strong></td>
                </tr>
            </table>
            <table  style="width:100%; margin-bottom:5px;">
                <tr>
                    <td class="variable"><span style="padding-right:1px">d.o.i. </span>
                        <strong>
                            @if(substr($datos->NUM_RUC,0,2)!=20)
                                {{$datos->RRLL_DOCUMENTO}}
                            @endif

                            @if(substr($datos->NUM_RUC,0,2)==20)
                                {{$datos->NUM_RUC}}
                            @endif
                        </strong>
                    </td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">estado civil</span>
                        <strong>
                             @if(substr($datos->NUM_RUC,0,2)!=20)
                                @if($datos->RRLL_ESTADO_CIVIL == 1)
                                    Casado(a)
                                @endif

                                @if($datos->RRLL_ESTADO_CIVIL == 2)
                                    Conviviente(a)
                                @endif

                                @if($datos->RRLL_ESTADO_CIVIL == 3)
                                    Soltero
                                @endif

                                @if($datos->RRLL_ESTADO_CIVIL == 4)
                                    Viudo(a)
                                @endif

                                 @if($datos->RRLL_ESTADO_CIVIL == 5)
                                    Divorciado(a)
                                @endif

                                 @if($datos->RRLL_ESTADO_CIVIL == 6)
                                    Separado
                                @endif
                            @endif
                        </strong>
                    </td>
                </tr>
            </table>
            <table  style="width:100%; margin-bottom:5px;">
                <tr>
                    <td class="variable"><span style="padding-right:1px">domicilio </span>
                        <strong>{{$direccion ? $direccion[0]->DIRECCION : ''}}</strong>
                    </td>
                </tr>
            </table>
            <table  style="width:100%; margin-bottom:5px;">
                <tr>
                    <td class="variable"><span style="padding-right:1px; width:400px">nombre representante legal / cónyuge </span>
                        <strong>
                            @if(in_array(substr($datos->NUM_RUC,0,2),['10','15']) && in_array($datos->RRLL_ESTADO_CIVIL,['1']))
                              {{$datos->RRLL_APELLIDOS_2}}  {{$datos->RRLL_NOMBRES_2}}
                            @endif
                            @if(substr($datos->NUM_RUC,0,2)==20)
                              {{$datos->RRLL_APELLIDOS}}  {{$datos->RRLL_NOMBRES}}
                            @endif
                        </strong>
                    </td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">d.o.i.</span>
                        <strong>
                            @if(in_array(substr($datos->NUM_RUC,0,2),['10','15']) && in_array($datos->RRLL_ESTADO_CIVIL,['1']))
                              {{$datos->RRLL_DOCUMENTO_2}}
                            @endif
                            @if(substr($datos->NUM_RUC,0,2)==20)
                                {{$datos->RRLL_DOCUMENTO}}
                            @endif
                        </strong>
                    </td>
                </tr>
            </table>
            <table  style="width:100%; margin-bottom:5px;">
                <tr>
                    <td class="variable"><span style="padding-right:1px; width:400px">nombre representante legal</span>
                        <strong>
                            @if(substr($datos->NUM_RUC,0,2)==20)
                                {{$datos->RRLL_APELLIDOS_2}} {{$datos->RRLL_NOMBRES_2}}
                            @endif
                        </strong>
                    </td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">d.o.i.</span>
                        <strong>
                           @if(substr($datos->NUM_RUC,0,2)==20)
                              {{$datos->RRLL_DOCUMENTO_2}}
                           @endif
                        </strong>
                    </td>
                </tr>
            </table>
            <span class="underline"><strong> AVALES </strong></span><br>
            <br>
            <table style="width:100%; margin-bottom:5px;">
                <tr>
                    <td class="variable"><span style="padding-right:1px">firma(s) </span><strong>  </strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">firma(s) </span><strong>  </strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">firma(s) </span><strong>  </strong></td>
                </tr>
                <tr>
                    <td>
                        <p style="margin-bottom: 0px;">(aval: persona natural / persona jurídica)</p>
                    </td>
                    <td class="space"></td>
                    <td>
                        <p style="margin-bottom: 0px;">(aval: persona natural / persona jurídica)</p>
                    </td>
                    <td class="space"></td>
                    <td>
                        <p style="margin-bottom: 0px;">(aval: persona natural / persona jurídica)</p>
                    </td>
                </tr>
            </table>
            <table style="width:100%; margin-bottom:5px;">
                <tr>
                    <td class="variable"><span style="padding-right:1px">razón soc. / nombre </span><strong> </strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">razón soc. / nombre </span><strong> </strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">razón soc. / nombre </span><strong> </strong></td>
                </tr>
            </table>
            <table style="width:100%; margin-bottom:5px;">
                <tr>
                    <td class="variable"><span style="padding-right:1px">d.o.i.</span><strong>  </strong></td>
                    <td class="space"></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">estado civil </span><strong></strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">d.o.i.</span><strong> </strong></td>
                    <td class="space"></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">estado civil</span><strong>  </strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">d.o.i.</span><strong>  </strong></td>
                    <td class="space"></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">estado civil</span><strong> </strong></td>
                </tr>
            </table>
            <table style="width:100%; margin-bottom:5px;">
                <tr>
                    <td class="variable"><span style="padding-right:1px">domicilio </span><strong> </strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">domicilio </span><strong>  </strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">domicilio </span><strong>  </strong></td>
                </tr>
            </table>
            <table style="width:100%; margin-bottom:5px;">
                <tr>
                    <td class="variable"><span style="padding-right:1px">rep. legal / cónyuge </span><strong> </strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">rep. legal / cónyuge </span><strong></strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">rep. legal / cónyuge </span><strong>  </strong></td>
                </tr>
            </table>
            <table style="width:100%; margin-bottom:5px;">
                <tr>
                    <td class="variable"><span style="padding-right:1px">d.o.i. </span><strong>  </strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">d.o.i. </span><strong> </strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">d.o.i. </span><strong>  </strong></td>
                </tr>
            </table>
            <table style="width:100%; margin-bottom:5px;">
                <tr>
                    <td class="variable"><span style="padding-right:1px">rep. Legal </span><strong>  </strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">rep. Legal </span><strong>  </strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">rep. Legal </span><strong>  </strong></td>
                </tr>
            </table>
            <table style="width:100%; margin-bottom:0x;">
                <tr>
                    <td class="variable"><span style="padding-right:1px">d.o.i. </span><strong> </strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">d.o.i. </span><strong></strong></td>
                    <td class="space"></td>
                    <td class="variable"><span style="padding-right:1px">d.o.i. </span><strong>  </strong></td>
                </tr>
            </table>
            </div>
        </div>
        <div style="position: absolute; bottom: -30px; width:700px; padding-right:100px; text-align:right;font-size:6.5pt;">
        COPIA-BANCO
        </div>
    </body>
</html>
