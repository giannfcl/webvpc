@extends('cmpEmergencia.indexjs-bpe')

@section('vista-reactiva-bpe')

<div class="x_panel">
	@if ($esejecutivo == 0)
		<div class="row">
			<div class="form-group col-md-3">
				<label class="control-label col-md-4" style="margin-bottom: 0">Zonales</label>
				<div class="col-md-8" style="margin-bottom: 0">
					<select id="id_zona" class="form-control" name="id_zona">
						<option value="" selected>Todos...</option>
						@if(count($zonales)>0)
							@foreach($zonales as $zonal)
								<option value="{{$zonal->ID_ZONA}}">{{$zonal->ZONA}}</option>
							@endforeach
						@endif
					</select>
				</div>
			</div>
			<div class="form-group col-md-3">
				<label class="control-label col-md-4" style="margin-bottom: 0">Centros</label>
				<div class="col-md-8" style="margin-bottom: 0">
					<select id="id_centro" class="form-control" name="id_centro">
						<option value="" selected>Todos...</option>
					</select>
				</div>
			</div>
			<div class="form-group col-md-3">
				<label class="control-label col-md-4" style="margin-bottom: 0">Tiendas</label>
				<div class="col-md-8" style="margin-bottom: 0">
					<select id="id_tienda" class="form-control" name="id_tienda">
						<option value="" selected>Todos...</option>
					</select>
				</div>
			</div>
		</div>
	@endif
	<div class="row">
		<div class="form-group col-md-3">
			<label class="control-label col-md-4" style="margin-bottom: 0">Gestión</label>
			<div class="col-md-8" style="margin-bottom: 0">
				<select id="gestion_en" class="form-control">
					<option value="" selected>Todos...</option>
						@foreach($gestiones as $gestion)
							<option value="{{$gestion->GESTION_ID}}">{{$gestion->GESTION_NOMBRE}}</option>
						@endforeach
				</select>
			</div>
		</div>
		<div class="form-group col-md-3">
			<label class="control-label col-md-4" style="margin-bottom: 0">Riesgos</label>
			<div class="col-md-8" style="margin-bottom: 0">
				<select id="gestion_r" class="form-control">
					<option value="" selected>Todos...</option>
					@foreach($griesgos as $griesgo)
						<option value="{{$griesgo->GESTION_ID}}">{{$griesgo->GESTION_NOMBRE}}</option>
					@endforeach
				</select>
			</div>
		</div>
		<div class="form-group col-md-3">
			<label class="control-label col-md-4" style="margin-bottom: 0">Prioridad: </label>
			<div class="col-md-8" style="margin-bottom: 0">
				<select id="prioridad" class="form-control">
					<option value="" selected>Todos...</option>
					@foreach($prioridades as $prioridad)
						<option value="{{$prioridad}}">{{$prioridad}}</option>
					@endforeach
				</select>
			</div>
		</div>
		<div class="form-group col-md-3">
			<label class="control-label col-md-4" style="margin-bottom: 0">Tipo Cliente</label>
			<div class="col-md-8" style="margin-bottom: 0">
				<select id="clientescanal" class="form-control">
					<option value="" selected>Todos...</option>
					@foreach($clientecanales as $clientecanal)
						<option value="{{$clientecanal}}">{{$clientecanal}}</option>
					@endforeach
				</select>
			</div>
		</div>
		<div class="form-group col-md-3">
			<label class="control-label col-md-6" style="margin-bottom: 0">Tipo de Cambio</label>
			<div class="col-md-6" style="margin-bottom: 0">
				<select id="traderdesembolso" class="form-control">
					<option value="" selected>Todos...</option>
					<option value="1">SI</option>
					<option value="0">NO</option>
				</select>
			</div>
		</div>
	</div>
</div>


<div class="x_panel">
	<table class='table table-striped table-bordered jambo_table' id="dataTableLeads">
			<thead>
				<tr>
					<th scope="col" style="font-size: 12.5px;">Prior.</th>
					<th scope="col" style="font-size: 12.5px;">Empresa</th>
					<th scope="col" style="font-size: 12.5px;">Flujo</th>
					<th scope="col" style="font-size: 12.5px;">Ventas Anuales S/. / Fuente / Salud</th>
					<th scope="col" style="font-size: 12.5px;">Canal</th>
					<th scope="col" style="font-size: 12.5px;">Monto / Plazo Adicional</th>
					<th scope="col" style="font-size: 12.5px;">Campaña</th>
					<th scope="col" style="font-size: 12.5px;">Riesgos</th>
					<th scope="col" style="font-size: 12.5px;">Gestionar</th>
					<th scope="col" style="font-size: 12.5px;">RRLL</th>
					<th scope="col" style="font-size: 12.5px;">Telefono /<br> Email</th>
					<th scope="col" style="font-size: 12.5px;">Monto EsSalud</th>
					<th scope="col" style="font-size: 12.5px;">N° Trabajadores Independientes</th>
					<th scope="col" style="font-size: 12.5px;">Sueldo Promedio</th>
					<th scope="col" style="font-size: 12.5px;">Ejecutivo</th>
					<th scope="col" style="font-size: 12.5px;">Centro</th>
					<th scope="col" style="font-size: 12.5px;">Fecha Gestión</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
	</table>
</div>


<div class="modal fade" tabindex="-1" role="dialog" id="modalGestion">
    <div class="modal-dialog" role="document" style="width: 1020px;">
        <div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="padding:10px">
				<span aria-hidden="true">&times;</span>
			</button>
			<form id="formGestionar" method="POST" class="form-horizontal form-label-left" enctype="multipart/form-data" action="{{route('cmpEmergencia.bpe.gestionar')}}">
				<div class="modal-body">
						<ul class="nav nav-tabs" id="tabContent">
							<li id="liEmpresa"><a href="#empresa" data-toggle="tab">Empresa</a></li>
							{{-- <li id="liCredito"><a href="#credito" data-toggle="tab">Crédito</a></li> --}}
							<li id="liRRLL"><a id="rrllTab" href="#rrll" data-toggle="tab">RRLL</a></li>
							<li id="licrossell"><a href="#crossell" data-toggle="tab">Cross Sell</a></li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane" id="empresa">
								<div class="row" style="padding-top:15px">
									<div class="col-sm-6">
										<div class="form-group row">
											<label class="col-sm-2 col-form-label col-form-label-sm">Ruc:</label>
											<div class="col-sm-3">
												<input autocomplete="off" class="form-control"  type="text" name="numruc" readonly>
											</div>
											<label class="col-sm-3 col-form-label col-form-label-sm">Tipo Cliente:</label>
											<div class="col-sm-4">
												<input autocomplete="off" class="form-control"  type="text" name="tipoCliente" readonly>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-sm-2 col-form-label col-form-label-sm">Cliente:</label>
											<div class="col-sm-10">
												<input autocomplete="off" class="form-control"  type="text" name="cliente" readonly>
											</div>
										</div>
										<div class="form-group row" style="display:none">
											<label class="col-sm-2 col-form-label col-form-label-sm">Dirección:</label>
											<div class="col-sm-10">
												<input autocomplete="off" class="form-control" type="text" name="direccion" readonly>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-sm-2 col-form-label col-form-label-sm">Tipo Vía:</label>
											<div class="col-sm-5">
												<select class="form-control" name="tipoVia" disabled>
													<option value="">Selecciona una opción</option>
													@foreach ($tipoVia as $tipo)
														<option value="{{ $tipo->CODIGO }}">{{ $tipo->DESCRIPCION_COMPLETA }}</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-sm-2 col-form-label col-form-label-sm">Descp Vía:</label>
											<div class="col-sm-10">
												<input autocomplete="off" class="form-control"  type="text" name="direcVia" readonly>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-sm-2 col-form-label col-form-label-sm">Nro:</label>
											<div class="col-sm-3">
												<input autocomplete="off" class="form-control"  type="text" name="direcNro" readonly>
											</div>
											<label class="col-sm-2 col-form-label col-form-label-sm">Lote:</label>
											<div class="col-sm-3">
												<input autocomplete="off" class="form-control"  type="text" name="direcLote" readonly>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-sm-2 col-form-label col-form-label-sm">Urb. Zona:</label>
											<div class="col-sm-10">
												<input autocomplete="off" class="form-control"  type="text" name="direcUrb" readonly>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-sm-2 col-form-label col-form-label-sm">Manzana:</label>
											<div class="col-sm-3">
												<input autocomplete="off" class="form-control"  type="text" name="direcManz" readonly>
											</div>
											<label class="col-sm-2 col-form-label col-form-label-sm">Interior:</label>
											<div class="col-sm-3">
												<input autocomplete="off" class="form-control"  type="text" name="direcInt" readonly>
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<input id="ubigeo" name="ubigeo" type="hidden">
										<div class="form-group row">
											<label class="col-sm-3 col-form-label col-form-label-sm">Departamento:</label>
											<div class="col-sm-5">
												<select class="form-control" name="departamento" disabled>
													<option value="">Selecciona una opción</option>
														@foreach ($departamentos as $departamento)
															<option value="{{ $departamento->CodigoDepartamento }}">{{ $departamento->NombreDepartamento }}</option>
														@endforeach
												</select>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-sm-3 col-form-label col-form-label-sm">Provincia</label>
											<div class="col-sm-5">
												<select class="form-control" name="provincia" disabled>
													<option value="">Selecciona una opción</option>
												</select>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-sm-3 col-form-label col-form-label-sm">Distrito:</label>
											<div class="col-sm-5">
												<select class="form-control" name="distrito" disabled>
													<option value="">Selecciona una opción</option>
												</select>
											</div>
										</div>
										{{--
										<div class="form-group row">
											<label class="col-sm-3 col-form-label col-form-label-sm">Tienda:</label>
											<div class="col-sm-5">
												<select class="form-control editable" name="tienda" disabled>
													<option value="">Seleccionar</option>
												</select>
											</div>
										</div>
										--}}
										<div class="form-group row">
											<label class="col-sm-3 col-form-label col-form-label-sm">Teléfono:</label>
											<div class="col-sm-3">
												<input autocomplete="off" class="form-control input-number editable"  type="text" name="telefono" maxlength="9">
											</div>
											<div class="col-sm-3">
												<input autocomplete="off" class="form-control input-number editable"  type="text" name="telefono2" maxlength="9">
											</div>
											<div class="col-sm-3">
												<input autocomplete="off" class="form-control input-number editable"  type="text" name="telefono3" maxlength="9">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-sm-3 col-form-label col-form-label-sm">Email:</label>
											<div class="col-sm-8">
												<input autocomplete="off" class="form-control editable"  type="text" name="email" maxlength="75">
											</div>
										</div>
										{{--
										<div class="form-group row">
											<label class="col-sm-3 col-form-label col-form-label-sm">N° Empleados:</label>
											<div class="col-sm-3">
												<input autocomplete="off" class="form-control editable"  type="text" name="empleados">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-sm-3 col-form-label col-form-label-sm">Tipo Sociedad:</label>
											<div class="col-sm-5">
												<select class="form-control editable" name="tipoSociedad" >
													<option value="">Selecciona una opción</option>
														@foreach ($tipoSociedad as $tipo)
															<option value="{{ $tipo->CODIGO }}">{{ $tipo->DESCRIPCION }}</option>
														@endforeach
												</select>
											</div>
										</div>
										--}}
										<div class="form-group row">
											<label class="col-sm-3 col-form-label col-form-label-sm">Giro:</label>
											<div class="col-sm-5">
												<input class="form-control" type="text" name="girodesc" disabled>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="credito">
								<div class="row" style="padding-top:15px">
									<div class="col-sm-6">
										<div class="form-group row">
											<label class="col-sm-4 col-form-label col-form-label-sm">Tipo Cuenta:</label>
											<div class="col-sm-5">
												<select class="form-control editable" name="cuentaTipo">
													<option value="">Ingresa una opción</option>
													<option value="ahorro">AHORRO</option>
													<option value="corriente">CORRIENTE</option>
												</select>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-sm-4 col-form-label col-form-label-sm">N° Cuenta:</label>
											<div class="col-sm-5">
												<input autocomplete="off" class="form-control editable"  type="text" name="cuentaNumero" maxlength="25">
											</div>
										</div>
										{{--
										<div class="form-group row">
											<label class="col-sm-4 col-form-label col-form-label-sm">Gestión:</label>
											<div class="col-sm-5">
												<select class="form-control editable" name="gestion">
												</select>
											</div>
										</div>
										--}}
										<div class="form-group row hidden areaTipoEnvioGTP">
											<label class="col-sm-4 col-form-label col-form-label-sm">Tipo Envío:</label>
											<div class="col-sm-5">
												<select class="form-control" name="tipoEnvioGTP">
													<option value="">Selecciona una opción</option>
													@foreach ($tipoEnvioGtp as $k => $t)
														<option value="{{ $k }}">{{ $t }}</option>
													@endforeach

												</select>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-sm-4 col-form-label col-form-label-sm">Campaña:</label>
											<div class="col-sm-5">
												<select class="form-control editable" name="campanha" >
													@foreach ($campanhas as $key => $campanha)
														<option value="{{ $key }}">{{ $campanha}}</option>
													@endforeach
												</select>
											</div>
										</div>
										{{-- 
										<div class="form-group row">
											<label class="col-sm-4 col-form-label col-form-label-sm">Venta Anual:</label>
											<div class="col-sm-4">
												<input autocomplete="off" class="form-control editable"  type="text" name="ventas" onpaste="return false;" onkeypress="return filterFloat(event,this);" maxlength="15">
											</div>
										</div>
										--}}
										<div class="form-group row">
											<label class="col-sm-4 col-form-label col-form-label-sm">Analista Riesgos:</label>
											<div class="col-sm-6">
												<input autocomplete="off" class="form-control"  type="text" name="analista" disabled>
											</div>
										</div>
										<div class="form-group">
										<label class="col-sm-4 col-form-label col-form-label-sm">Riesgos Comentario:</label>
										<div class="col-sm-8">
											<textarea class="form-control" rows="3" name="riesgosComentario" disabled></textarea>
										</div>
										</div>

									</div>
									<div class="col-sm-6">
										<div class="form-group row">
											<label class="col-sm-4 col-form-label col-form-label-sm">Crédito Reactiva 1:</label>
											<div class="col-sm-6">
												<select class="form-control" name="flgReactiva1" disabled>
													<option value="">Selecciona una opción</option>
													<option value="0">No</option>
													<option value="1" disabled>Sí(Interbank)</option>
													<option value="2">Sí(Otro banco)</option>

												</select>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-sm-4 col-form-label col-form-label-sm">Banco Reactiva 1:</label>
											<div class="col-sm-6">
												<select class="form-control" name="bancoReactiva1" disabled>
													<option value="">Selecciona un Banco</option>
													@foreach ($bancos as $key => $banco)
														<option value="{{ $banco->CODIGO }}">{{ $banco->DESCRIPCION }}</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-sm-4 col-form-label col-form-label-sm">Monto Reactiva 1:</label>
											<div class="col-sm-4">
												<input autocomplete="off" class="form-control"  type="text" name="montoReactiva1" onpaste="return false;" onkeypress="return filterFloat(event,this);" maxlength="18" disabled>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-sm-4 col-form-label col-form-label-sm">Monto Riesgos:</label>
											<div class="col-sm-4">
												<input autocomplete="off" class="form-control"  type="text" name="riesgosMonto" readonly>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-sm-4 col-form-label col-form-label-sm">Monto Final:</label>
											<div class="col-sm-4">
												<input autocomplete="off" class="form-control"  type="text" name="montoFinal" readonly>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-sm-4 col-form-label col-form-label-sm">Monto Solicitado:</label>
											<div class="col-sm-4">
												<input autocomplete="off" class="form-control editable"  type="text" name="solicitudMonto" onpaste="return false;" onkeypress="return filterFloat(event,this);" maxlength="15">
											</div>
											<div class="col-sm-2">
												<input autocomplete="off" class="form-control"  type="text" name="solicitudCobertura" readonly>
											</div>
										</div>
										<div class="form-group row areaTasa hidden">
											<label class="col-sm-4 col-form-label col-form-label-sm">Tasa (%)</label>
											<div class="col-sm-4">
												<select autocomplete="off" class="form-control"  type="text" name="solicitudTasa">
													<option value="">--Tasa--</option>
												</select>
											</div>
											<div class="col-sm-3">
												<button id="btnGetTasas" class="btn btn-sm btn-primary">Buscar</button>
											</div>
										</div>
										<div class="form-group row areaTasaLectura">
											<label class="col-sm-4 col-form-label col-form-label-sm">Tasa (%)</label>
											<div class="col-sm-4">
												<input autocomplete="off" class="form-control"  type="text" name="tasa" readonly>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-sm-4 col-form-label col-form-label-sm">Gracia + Cuotas:</label>
											<div class="col-sm-6">
												<select class="form-control editable" name="solicitudGracia">
													<option value="12">Gracia 12 meses + 24 cuotas</option>
												</select>
											</div>
										</div>
									</div>
									<div class="form-group row">
											<label class="col-sm-4 col-form-label col-form-label-sm">Comentario:</label>
											<div class="col-sm-12">
												<textarea class="form-control editable" rows="2" name="comentario"></textarea>
											</div>
										</div>
								</div>
							</div>
							<div class="tab-pane" id="rrll">
								<div class="row" style="padding-top:15px">
									<div class="col-sm-6">
										<fieldset class="scheduler-border">
											<legend id="leg1" class="scheduler-border">Representante Legal 1</legend>
											<div class="form-group row">
												<label class="col-sm-4 col-form-label col-form-label-sm">DNI:</label>
												<div class="col-sm-4">
													<input autocomplete="off" class="form-control input-number"  type="text" name="rucrrll" {{$editarrll?'':'disabled'}}>
												</div>
											</div>
											<div class="form-group row">
												<label class="col-sm-4 col-form-label col-form-label-sm">Nombre:</label>
												<div class="col-sm-6">
													<input autocomplete="off" class="form-control"  type="text" name="rrll" {{$editarrll?'':'disabled'}}>
												</div>
											</div>
											<div class="form-group row">
												<label class="col-sm-4 col-form-label col-form-label-sm">Apellidos:</label>
												<div class="col-sm-6">
													<input autocomplete="off" class="form-control"  type="text" name="rrllApellidos" {{$editarrll?'':'disabled'}}>
												</div>
											</div>
											<div class="form-group row">
												<label class="col-sm-4 col-form-label col-form-label-sm">Estado Civil:</label>
												<div class="col-sm-6">
													<select class="form-control" name="rrllEstadoCivil" {{$editarrll?'':'disabled'}}>
														<option value="">Selecciona una opción</option>
														@foreach ($estadoCivil as $estado)
															<option value="{{ $estado->CODIGO }}">{{ $estado->DESCRIPCION }}</option>
														@endforeach
													</select>
												</div>
											</div>
										</fieldset>
									</div>
									<div class="col-sm-6">
										<fieldset class="scheduler-border">
											<legend id="leg2" class="scheduler-border">Representante Legal 2</legend>
											<div class="form-group row">
												<label class="col-sm-4 col-form-label col-form-label-sm">DNI:</label>
												<div class="col-sm-4">
													<input autocomplete="off" class="form-control input-number"  type="text" name="rucrrll2">
												</div>
											</div>
											<div class="form-group row">
												<label class="col-sm-4 col-form-label col-form-label-sm">Nombres:</label>
												<div class="col-sm-6">
													<input autocomplete="off" class="form-control"  type="text" name="rrll2">
												</div>
											</div>
											<div class="form-group row">
												<label class="col-sm-4 col-form-label col-form-label-sm">Apellidos:</label>
												<div class="col-sm-6">
													<input autocomplete="off" class="form-control"  type="text" name="rrllApellidos2">
												</div>
											</div>
											<div class="form-group row">
												<label class="col-sm-4 col-form-label col-form-label-sm">Estado Civil:</label>
												<div class="col-sm-6">
												<select class="form-control" name="rrllEstadoCivil2">
													<option value="">Selecciona una opción</option>
													@foreach ($estadoCivil as $estado)
														<option value="{{ $estado->CODIGO }}">{{ $estado->DESCRIPCION }}</option>
													@endforeach
												</select>
												</div>
											</div>
										</fieldset>
									</div>
								</div>

							</div>
							<div class="tab-pane" id="crossell">
								<div class="row" style="padding-top: 10px; padding-left: 80px;">
									<div class="form-group col-md-3">
										<label class="control-label col-md-4" style="margin-bottom: 0">Gestión</label>
										<div class="col-md-8" style="margin-bottom: 0">
											<select id="gestion_en" class="form-control" name="gestion">
												<option value="" selected>Todos...</option>
													@foreach($gestiones as $gestion)
														<option value="{{$gestion->GESTION_ID}}">{{$gestion->GESTION_NOMBRE}}</option>
													@endforeach
											</select>
										</div>
									</div>
									<div class="form-group col-md-4">
										<label class="col-sm-6 col-form-label col-form-label-sm">Venta Anual:</label>
										<div class="col-sm-6">
											<input autocomplete="off" class="form-control"  type="text" name="ventas" readonly="readonly">
										</div>
									</div>
								</div>
								<div class="row">
									<h2 style="padding-left:80px">Trading</h2>
									<div class="form-group row">
										<label class="col-sm-8" style="padding-left:90px">Tiene Cuenta Dólares:</label>
										<div class="col-sm-4">
											<input autocomplete="off" class="form-control" type="text" name="tienecuentadolares" readonly>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-8" style="padding-left:90px">Destino del crédito para compras de dólares:</label>
										<div class="col-sm-4">
											<select class="form-control" name="flg_creditocomprasdolares">
												<option value="" selected disabled>Seleccionar Opción</option>
												<option value="1">SI</option>
												<option value="0">NO</option>
											</select>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-8" style="padding-left:90px">¿Cuántos dólares comprará el cliente?:</label>
										<div class="col-sm-4">
											<input autocomplete="off" class="form-control" type="text" name="montodolarescompra" onpaste="return false;" readonly onkeypress="return filterFloat(event,this);">
										</div>
									</div>
								</div>

								<div class="row">
									<h2 style="padding-left:80px">Cuenta Sueldo</h2>
									<div class="form-group row">
										<div class="col-sm-6">
											<label class="col-sm-7" style="padding-left:90px">Acepta Cuenta Sueldo:</label>
											<div class="col-sm-5">
												<select class="form-control" name="flgcuensueldo">
													<option value="" disabled>Seleccionar Opción</option>
													<option value="1">SI</option>
													<option value="0">NO</option>
												</select>
											</div>
										</div>
									</div>
									<div class="form-group row">
										<div class="col-sm-6">
											<label class="col-sm-7" style="padding-left:90px">Monto Pago Es Salud:</label>
											<div class="col-sm-5">
												<input autocomplete="off" class="form-control" type="text" name="montopagoessalud" onpaste="return false;" onkeypress="return filterFloat(event,this);">
											</div>
										</div>
									</div>
									<div class="form-group row">
										<div class="col-sm-6">
											<label class="col-sm-7" style="padding-left:90px">Número Trabajadores Dependientes:</label>
											<div class="col-sm-5">
												<input autocomplete="off" class="form-control" type="text" name="numtrabajadores" onpaste="return false;" onkeypress="return filterFloat(event,this);">
											</div>
										</div>
										<div class="col-sm-6">
											<label class="col-sm-8" style="padding-left:90px">Sueldo Promedio:</label>
											<div class="col-sm-4">
												<input autocomplete="off" class="form-control" type="text" onpaste="return false;" readonly name="sueldopromedio">
											</div>
										</div>
									</div>
								</div>

								<div class="row">
									<h2 style="padding-left:80px">Cobro Simple</h2>
									<div class="form-group row">
										<div class="col-sm-6">
											<label class="col-sm-7" style="padding-left:90px">Acepta Cobro Simple:</label>
											<div class="col-sm-5">
												<select class="form-control" name="cs_flgcobrosimple">
													<option value="" disabled selected>Seleccionar Opción</option>
													<option value="1">SI</option>
													<option value="0">NO</option>
												</select>
											</div>
										</div>
										<div class="col-sm-6">
											<label class="col-sm-7" style="padding-left:90px">Cant. Clientes que le pagan:</label>
											<div class="col-sm-5">
												<select class="form-control" name="cs_cantclientespagan">
													<option value="0" disabled selected>Seleccionar Opción</option>
													<option value="1">Más de 8,000</option>
													<option value="2">Menos de 8,000</option>
												</select>
											</div>
										</div>
									</div>
									<div class="form-group row">
										<div class="col-sm-6">
											<label class="col-sm-7" style="padding-left:90px">Control de Cobranzas:</label>
											<div class="col-sm-5">
												<select class="form-control" name="cs_controlcobranza">
													<option value="0" disabled selected>Seleccionar Opción</option>
													<option value="1">Manual o Excel</option>
													<option value="2">Sistema o ERP</option>
												</select>
											</div>
										</div>
										<div class="col-sm-6">
											<label class="col-sm-7" style="padding-left:90px">Ticket promedio de Cobranza:</label>
											<div class="col-sm-5">
												<select class="form-control" name="cs_ticketpromediocobranza">
													<option value="0" disabled selected>Seleccionar Opción</option>
													<option value="1">Menos de S/ 100,000</option>
													<option value="2">Más de S/ 100,000</option>
												</select>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="canalActual">

					<center class="btnArea">
						<a id="btnValorado" class="btn btn-success btnGestion" role="button" onclick="generarValorado()">Valorado</a>
						<button id="btnGestion" class="btn btn-success btnGestion editable" type="submit">Gestionar</button>
					</center>

				</div>
				<input type="hidden" name="flgMontoLimite" value=""/>
				<input type="hidden" name="montoLimite" value=""/>
			</form>
			<div class="botonesdescargas" hidden="hidden">

			</div>
		</div>
    </div>
</div>

<div id="gestionOpciones" class="hidden">
	<option value="">Selecciona una opción</option>
	@foreach ($gestiones as $gestion)
		@if(!in_array($gestion->GESTION_ID,[20,21]))
			<option value="{{ $gestion->GESTION_ID }}"  {{ $gestion->GESTION_ID=='30'?'disabled':'' }} >{{ $gestion->GESTION_NOMBRE }}</option>
		@endif
	@endforeach
</div>

<div id="gestionOpcionesValorados" class="hidden">
	<option value="">Selecciona una opción</option>
	<option value="{{\App\Entity\cmpemergencia\Gestion::ENVIO_VALORADOS}}">Envío Valorados Cliente</option>
</div>

<div id="gestionOpcionesGTP" class="hidden">
	<option value="">Selecciona una opción</option>
	<option value="{{\App\Entity\cmpemergencia\Gestion::ENVIO_GTP}}">Enviar a GTP</option>
	<option value="{{\App\Entity\cmpemergencia\Gestion::NO_ACEPTA}}">No Acepta</option>
</div>

<div class="modal fade" id="cargando" tabindex="-1" role="dialog">
	<div class="modal-dialog loader" role="document">
		<button type="button" class="close cerrarcargando" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" hidden>&times;</span></button>
	</div>
</div>


@stop
