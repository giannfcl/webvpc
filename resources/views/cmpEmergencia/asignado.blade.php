@extends('cmpEmergencia.menu')

@section('datable')
<a class='btn btn-primary addreferido'>	Agregar Referido </a>

<table class='table table-striped table-bordered jambo_table' id="datatableasignados">
		<thead>
			<tr>
				<th scope="col" style="font-size: 12.5px;">Canal</th>
				<th scope="col" style="font-size: 12.5px;">Empresa</th>
				<th scope="col" style="font-size: 12.5px;">Desembolso RP1</th>
				<th scope="col" style="font-size: 12.5px;">Fecha Ultimo Metodizado</th>
				<th scope="col" style="font-size: 12.5px;">Ventas</th>
				<th scope="col" style="font-size: 12.5px;">Oferta Riesgos</th>
				<th scope="col" style="font-size: 12.5px;">Gestionado</th>
				<th scope="col" style="font-size: 12.5px;">Detalle Gestión</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
</table>

@stop

<div class="modal fade" tabindex="-1" role="dialog" id="modalNuevoReferido">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <form id="formNuevoReferido" method="POST" class="form-horizontal form-label-left" enctype="multipart/form-data" action="{{route('cmpEmergencia.nuevoasignado')}}">
		            <div class="modal-header">
		            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		            	<div class="col-sm-4">
		            		<h4 class="modal-title tnuevoreferido">Nuevo Referido</h4>
		            	</div>
		            	<div class="col-sm-8">
			            	<label class="control-label col-sm-4 col-xs-12" style="text-align: left;">RUC:</label>
	                        <div class="input-group col-sm-8 col-xs-12">
								<div class="col-sm-8 col-xs-12" style="padding-left:0px">
									<input autocomplete="off" class="form-control" type="text" id="addnumruc" maxlength="11">
								</div>
								<div class="col-sm-4 col-xs-12">
									<button type='button' class='btn btn-primary buscarinfo'>Buscar</button>
								</div>
							</div>
		            	</div>
                    </div>
					<!-- EN LA TABLA -->
					<div class="form-group noadd" disabled hidden='hidden'>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Empresa:</label>
                    	<label class="control-label col-md-3 col-sm-3 col-xs-12" id="nempresa"></label>
                    </div>
					<div class="form-group noadd" disabled hidden='hidden'>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Banca:</label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" id="banca"></label>
                    </div>
                    <div class="form-group noadd" disabled hidden='hidden'>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Asignado a:</label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" id="asignadoa"></label>
                    </div>


					<!-- NO ESTA EN LA TABLA -->
					<div class="modal-header add" disabled hidden='hidden'>
		            	<div class="col-sm-6">
		            		<h4 class="modal-title">Información Cliente</h4>
		            	</div>
		            	<div class="col-sm-6">
		            		<h4 class="modal-title">Información Reactiva</h4>
		            	</div>
		            </div>
		            <br class="add" disabled hidden='hidden'>
		            <div class="row add" disabled hidden='hidden'>
		            	<div class="col-sm-6">
							<div class="row">
								<div class="form-group">
									<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">RUC:</label>
									<div class="col-sm-7 col-xs-12" style="padding-left:0px">
										<input class="form-control"  type="text" id="numruc" name="numruc" readonly>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Empresa:</label>
									<div class="col-sm-7 col-xs-12" style="padding-left:0px">
										<input class="form-control"  type="text" name="nempresa" placeholder="Pepito S.A." required>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Teléfono:</label>
									<div class="col-sm-7 col-xs-12" style="padding-left:0px">
										<input autocomplete="off" class="form-control input-number" type="text" name="telefono" maxlength="9">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Correo:</label>
									<div class="col-sm-7 col-xs-12" style="padding-left:0px">
										<input autocomplete="off" class="form-control"  type="text" name="correo">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Ventas Anuales S/.<br>Declarado por cliente:</label>
									<div class="col-sm-7 col-xs-12" style="padding-left:0px">
										<input autocomplete="off" class="form-control"  type="text" name="ventasa" id="ventasa" onkeypress="return filterFloat(event,this);" onpaste="return filterFloat(event,this);" required>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="row">
								<div class="form-group">
									<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Deuda MicroEmpresa S/.:</label>
									<div class="col-sm-6 col-xs-12" style="padding-left:0px">
										<input autocomplete="off" class="form-control"  type="text" name="DeudaMicroempresa" id="deudaMicroempresa" readonly>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">¿TIENE DESEMBOLSO RP1?:</label>
		                            <div class="col-sm-6 col-xs-12" style="padding-left:0px">
		                                <select class="form-control" name="flgReactiva1" id="flgReactiva1">
		                                    <option value="">--Opciones--</option>
		                                    <option value="0">No</option>
		                                    <option value="1" disabled>Sí(Interbank)</option>
		                                    <option value="2">Sí(Otro banco)</option>
		                                </select>
		                            </div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Banco dondé tomo RP1:</label>
									<div class="col-sm-6 col-xs-12" style="padding-left:0px">
										<select class="form-control" name="addbancor1" id="addbancor1">
											<option value="">--Opciones--</option>
											@foreach($bancos as $banco)
												<option value="{{$banco->CODIGO}}">{{$banco->DESCRIPCION}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Desembolso RP1 S/.:</label>
									<div class="col-sm-6 col-xs-12" style="padding-left:0px">
										<input autocomplete="off" class="form-control"  type="text" name="addmontoReactiva1" id="addmontoReactiva1" onkeypress="return filterFloat(event,this);" onpaste="return filterFloat(event,this);" maxlength="18">
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Monto Disponible RP2 S/.:</label>
									<div class="col-sm-4 col-xs-8" style="padding-left:0px">
										<input autocomplete="off" class="form-control"  type="text" name="montocalculado" id="montocalculado" readonly>
									</div>
									<div class="col-sm-2 col-xs-4" style="padding-left:0px">
										<input autocomplete="off" class="form-control"  type="text" name="coberturacalculado" id="coberturacalculado" readonly>
									</div>
								</div>
							</div>
						</div>
		            </div>

					<div class="modal-header add" disabled hidden='hidden'>
		                <h4 class="modal-title">Reactiva 2</h4>
		            </div>
		            <br>
					<div class="row add" disabled hidden='hidden'>
						<div class="col-sm-6">
							<div class="form-group">
								<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Detalle de Gestión :</label>
								<div class="col-sm-6 col-xs-12" style="padding-left:0px">
									<select class="form-control" name="gestion" id="gestion" required>
										<option selected value="">Elige una opción</option>
										@foreach ($gestiones as $gestion)
											@if($gestion->GESTION_ID != 17)
												<option value="{{ $gestion->GESTION_ID }}">{{ $gestion->GESTION_NOMBRE }}</option>
											@endif
										@endforeach
									</select>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Monto Solicitado por Cliente S/.:</label>
								<div class="col-sm-4 col-xs-8" style="padding-left:0px">
									<input autocomplete="off" class="form-control"  type="text" name="montosolicitado" id="montosolicitado" onkeypress="return filterFloat(event,this);" onpaste="return filterFloat(event,this);">
								</div>
								<div class="col-sm-2 col-xs-4" style="padding-left:0px">
									<input autocomplete="off" class="form-control"  type="text" name="coberturasolicitada" id="coberturasolicitada" readonly>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Plazo / Gracia:</label>
								<div class="col-sm-6 col-xs-12" style="padding-left:0px">
									<select autocomplete="off" class="form-control" id="plazogracia" name="plazogracia" required>
										<option value="">Seleccionar una Opción</option>
										@foreach($plazosgracias as $plazogracia)
											<option value="{{$plazogracia[0]}}">{{$plazogracia[1]}}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Oferta Riesgos S/.:</label>
								<div class="col-sm-6 col-xs-12" style="padding-left:0px">
									<input autocomplete="off" class="form-control"  type="text" name="montosriesgo" id="montosriesgo" readonly>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Riesgo Descubierto:</label>
								<div class="col-sm-6 col-xs-12" style="padding-left:0px">
									<input autocomplete="off" class="form-control"  type="text" name="riesgoDescubierto" id="riesgoDescubierto" readonly>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Comentario</label>
								<div class="col-sm-6 col-xs-12" style="padding-left:0px">
									<textarea class="form-control"  type="text" name="comentario" id="comentario"></textarea>
								</div>
							</div>
						</div>
					</div>

                    <center class='add' disabled hidden='hidden'>
                        <button class="btn btn-success add guardarreferido" disabled hidden='hidden' type="submit">Agregar</button>
                    </center>
                </form>
            </div>
		</div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modalGestion">
    <div class="modal-dialog" role="document" style="width: 1200px;">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="modal-body">
				<form id="formGestionar" method="POST" class="form-horizontal form-label-left" enctype="multipart/form-data" action="{{route('cmpEmergencia.cmpgestionar')}}">
		            <div class="modal-header">
		            	<div class="col-sm-6">
		            		<h4 class="modal-title">Información Cliente</h4>
		            	</div>
		            	<div class="col-sm-6">
		            		<h4 class="modal-title">Información Reactiva</h4>
		            	</div>
		            </div>
		            <br>
					<input hidden="hidden" name="etapa_actual" value="{{$request['etapa']}}">
					<div class="row">
						<div class="col-sm-6">
							<div class="row">
								<div class="form-group">
									<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">RUC:</label>
									<div class="col-sm-7 col-xs-12" style="padding-left:0px">
										<input class="form-control"  type="text" id="gnumruc" name="gnumruc" readonly>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Empresa:</label>
									<div class="col-sm-7 col-xs-12" style="padding-left:0px">
										<input class="form-control"  type="text" id="gcliente" disabled>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Teléfono:</label>
									<div class="col-sm-7 col-xs-12" style="padding-left:0px">
										<input autocomplete="off" class="form-control input-number" type="text" id="gtelefono" name="gtelefono" maxlength="9">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Correo:</label>
									<div class="col-sm-7 col-xs-12" style="padding-left:0px">
										<input autocomplete="off" class="form-control"  type="text" id="gcorreo" name="gcorreo">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Ventas Anuales S/.<br>Declarado por cliente:</label>
									<div class="col-sm-7 col-xs-12" style="padding-left:0px">
										<input autocomplete="off" class="form-control"  type="text" name="gventasa" id="gventasa" onkeypress="return filterFloat(event,this);" onpaste="return filterFloat(event,this);">
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="row">
								<div class="form-group">
									<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Deuda MicroEmpresa S/.:</label>
									<div class="col-sm-6 col-xs-12" style="padding-left:0px">
										<input autocomplete="off" class="form-control"  type="text" name="gDeudaMicroempresa" id="gdeudaMicroempresa" readonly>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">¿TIENE DESEMBOLSO RP1?:</label>
									<div class="col-sm-6 col-xs-12" style="padding-left:0px">
										<select class="form-control" name="gflgReactiva1" id="gflgReactiva1">
											<option value="">--Opciones--</option>
											<option value="0">No</option>
											<option value="1" disabled>Sí(Interbank)</option>
											<option value="2">Sí(Otro banco)</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Banco dondé tomo RP1:</label>
									<div class="col-sm-6 col-xs-12" style="padding-left:0px">
										<select class="form-control" name="bancor1" id="bancor1">
											<option value="">--Opciones--</option>
											@foreach($bancos as $banco)
												<option value="{{$banco->CODIGO}}">{{$banco->DESCRIPCION}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Desembolso RP1 S/.:</label>
									<div class="col-sm-6 col-xs-12" style="padding-left:0px">
										<input autocomplete="off" class="form-control"  type="text" name="gmontoReactiva1" id="gmontoReactiva1" onkeypress="return filterFloat(event,this);" onpaste="return filterFloat(event,this);" maxlength="18">
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Monto Disponible RP2 S/.:</label>
									<div class="col-sm-4 col-xs-8" style="padding-left:0px">
										<input autocomplete="off" class="form-control"  type="text" name="gmontocalculado" id="gmontocalculado" readonly>
									</div>
									<div class="col-sm-2 col-xs-4" style="padding-left:0px">
										<input autocomplete="off" class="form-control"  type="text" name="gcoberturacalculado" id="gcoberturacalculado" readonly>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-header">
		                <h4 class="modal-title">Reactiva 2</h4>
		            </div>
		            <br>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Detalle de Gestión :</label>
								<div class="col-sm-6 col-xs-12" style="padding-left:0px">
									<select class="form-control" id="ggestion" name="ggestion" required>
										<option selected value="">Elige una opción</option>
										@foreach ($gestiones as $gestion)
											@if($gestion->GESTION_ID != 17)
												<option value="{{ $gestion->GESTION_ID }}">{{ $gestion->GESTION_NOMBRE }}</option>
											@endif
										@endforeach
									</select>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Monto Solicitado por Cliente S/.:</label>
								<div class="col-sm-4 col-xs-8" style="padding-left:0px">
									<input autocomplete="off" class="form-control"  type="text" name="gmontosolicitado" id="gmontosolicitado" onkeypress="return filterFloat(event,this);" onpaste="return filterFloat(event,this);">
								</div>
								<div class="col-sm-2 col-xs-4" style="padding-left:0px">
									<input autocomplete="off" class="form-control"  type="text" name="gcoberturasolicitada" id="gcoberturasolicitada" readonly>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Plazo / Gracia:</label>
								<div class="col-sm-6 col-xs-12" style="padding-left:0px">
									<select autocomplete="off" class="form-control" id="gplazogracia" name="gplazogracia">
										<option value="">Seleccionar una Opción</option>
										@foreach($plazosgracias as $plazogracia)
											<option value="{{$plazogracia[0]}}">{{$plazogracia[1]}}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Oferta Riesgos S/.:</label>
								<div class="col-sm-6 col-xs-12" style="padding-left:0px">
									<input autocomplete="off" class="form-control"  type="text" name="gmontosriesgo" id="gmontosriesgo" readonly>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Riesgo Descubierto:</label>
								<div class="col-sm-6 col-xs-12" style="padding-left:0px">
									<input autocomplete="off" class="form-control"  type="text" name="griesgoDescubierto" id="griesgoDescubierto" readonly>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Comentario</label>
								<div class="col-sm-6 col-xs-12" style="padding-left:0px">
									<textarea class="form-control"  type="text" name="gcomentario" id="gcomentario"></textarea>
								</div>
							</div>
						</div>
					</div>

					<div class="modal-header">
		                <h4 class="modal-title">Reactiva 1  <span class="r1-icon glyphicon glyphicon-chevron-down" style="cursor: pointer" aria-hidden="true"></span> </h4>
		            </div>
		            <br>
					<div class="row r1-area hidden">
						<div class="col-sm-6">
							<div class="form-group">
								<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Etapa:</label>
								<div class="col-sm-6 col-xs-12" style="padding-left:0px">
									<input autocomplete="off" class="form-control"  type="text" name="gr1etapa" readonly>
								</div>
							</div>
							<div class="form-group fg-r1-desembolsado">
								<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Tasa:</label>
								<div class="col-sm-6 col-xs-12" style="padding-left:0px">
									<input autocomplete="off" class="form-control"  type="text" name="gr1tasa" readonly>
								</div>
							</div>
							<div class="form-group fg-r1-desembolsado">
								<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Monto:</label>
								<div class="col-sm-6 col-xs-12" style="padding-left:0px">
									<input autocomplete="off" class="form-control"  type="text" name="gr1monto" readonly>
								</div>
							</div>
							<div class="form-group fg-r1-desembolsado">
								<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Garantía:</label>
								<div class="col-sm-6 col-xs-12" style="padding-left:0px">
									<input autocomplete="off" class="form-control"  type="text" name="gr1garantia" readonly>
								</div>
							</div>
							<div class="form-group fg-r1-no-desembolsado">
								<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Fecha Contacto:</label>
								<div class="col-sm-6 col-xs-12" style="padding-left:0px">
									<input autocomplete="off" class="form-control"  type="text" name="gr1fechaContacto" readonly>
								</div>
							</div>
							<div class="form-group fg-r1-no-desembolsado">
								<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Plazo:</label>
								<div class="col-sm-6 col-xs-12" style="padding-left:0px">
									<input autocomplete="off" class="form-control"  type="text" name="gr1plazo" readonly>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group fg-r1-desembolsado">
								<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Plazo:</label>
								<div class="col-sm-6 col-xs-12" style="padding-left:0px">
									<input autocomplete="off" class="form-control"  type="text" name="gr1plazo" readonly>
								</div>
							</div>
							<div class="form-group fg-r1-desembolsado">
								<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Gracia:</label>
								<div class="col-sm-6 col-xs-12" style="padding-left:0px">
									<input autocomplete="off" class="form-control"  type="text" name="gr1gracia" readonly>
								</div>
							</div>
							<div class="form-group fg-r1-desembolsado">
								<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Riesgo Descubierto:</label>
								<div class="col-sm-6 col-xs-12" style="padding-left:0px">
									<input autocomplete="off" class="form-control"  type="text" name="gr1riesgoDescubierto" readonly>
								</div>
							</div>
							<div class="form-group fg-r1-no-desembolsado">
								<label class="control-label col-sm-5 col-xs-12" style="text-align: left;">Monto Solicitado:</label>
								<div class="col-sm-6 col-xs-12" style="padding-left:0px">
									<input autocomplete="off" class="form-control"  type="text" name="gr1monto" readonly>
								</div>
							</div>
						</div>
					</div>

                    <center>
                        <button class="btn btn-success guardargestion" type="submit">Guardar</button>
                    </center>
                </form>
            </div>
		</div>
    </div>
</div>

<style>
	#modalGestion div.modal-body {
		max-height: calc(100vh - 75px);
		overflow-y: auto;
	}
	#modalNuevoReferido div.modal-header{
		padding: 3px !important;
	}
	#modalGestion div.modal-header{
        padding: 3px !important;
    }
    #modalGestion input,textarea,select{
        margin-bottom: -15px;
    }
</style>

@section('js-vista')
<script>

	$('.r1-icon').click(function(){
		$(this).toggleClass('glyphicon-chevron-down').toggleClass('glyphicon-chevron-up');
		$('.r1-area').toggleClass('hidden');
	});

	tablaasignados("{{$request['etapa']}}");
	var gestiones = <?php echo json_encode($gestiones); ?>;
	// console.log(gestiones);
	// ids que inhabilitaran plazo
	var ids = ['5']; // Interesado ( como gestión )

	function ValidarMontos(monsol,moncal) {

		var vmonsol = numeral(monsol).value();
		var vmoncal = numeral(moncal).value();
		// console.log(vmonsol);
		// console.log(vmoncal);
		if(vmonsol>vmoncal){
			return true;
		}else{
			return false;
		}
	}

	$(document).on("click",".addreferido",function(){
		$("#modalNuevoReferido").modal();
		$("#addnumruc").val('');
		$(".tnuevoreferido").html("Nuevo Referido");
		$(".noadd").attr('disabled','disabled');
		$(".noadd").attr('hidden','hidden');
		$(".add").attr('disabled','disabled');
		$(".add").attr('hidden','hidden');
		$("#modalNuevoReferido div.modal-dialog").css("width","600px");
	});

	$(document).on("click",".buscarinfo",function(){
		$("#modalNuevoReferido input[name='numruc']").val("");
		$("#modalNuevoReferido input[name='nempresa']").val("");
		$("#modalNuevoReferido input[name='telefono']").val("");
		$("#modalNuevoReferido input[name='correo']").val("");
		$("#modalNuevoReferido input[name='ventasa']").val("");
		$("#modalNuevoReferido input[name='DeudaMicroempresa']").val("");
		$("#modalNuevoReferido select[name='flgReactiva1']").val("");
		$("#modalNuevoReferido select[name='addbancor1']").val("");
		$("#modalNuevoReferido input[name='addmontoReactiva1']").val("");
		$("#modalNuevoReferido input[name='montocalculado']").val("");
		$("#modalNuevoReferido input[name='coberturacalculado']").val("");
		$("#modalNuevoReferido select[name='gestion']").val("");
		$("#modalNuevoReferido input[name='montosolicitado']").val("");
		$("#modalNuevoReferido input[name='coberturasolicitada']").val("");
		$("#modalNuevoReferido select[name='plazogracia']").val("");
		$("#modalNuevoReferido input[name='montosriesgo']").val("");
		$("#modalNuevoReferido input[name='riesgoDescubierto']").val("");
		$("#modalNuevoReferido textarea[name='comentario']").val("");

		$(".noadd").attr('disabled','disabled');
		$(".noadd").attr('hidden','hidden');
		$(".add").attr('disabled','disabled');
		$(".add").attr('hidden','hidden');
		ruc = $("#addnumruc").val()?$("#addnumruc").val():null;
		if(ruc){
			$.ajax({
				type: "GET",
				data: {
					numruc: ruc
				},
				url: '{{route("cmpEmergencia.buscarinfo")}}',
				beforeSend: function() {
					$("#cargando").modal();
				},
				success: function(result) {
					if(result){
						$("#modalNuevoReferido div.modal-dialog").css("width","600px");
						$(".tnuevoreferido").html("Referido");
						$(".noadd").removeAttr('disabled');
						$(".noadd").removeAttr('hidden');
						$("#nempresa").html(result['NOMBRE']);
						$("#banca").html(result['BANCA']);
						$("#asignadoa").html(result['ASIGNADO_EJECUTIVO']);
					}else{
						$("#modalNuevoReferido div.modal-dialog").css("width","1200px");
						$(".tnuevoreferido").html("Agregar Referido");
						$(".add").removeAttr('disabled');
						$(".add").removeAttr('hidden');
						$("#numruc").val(ruc);
					}
				},
				complete: function() {
					$(".cerrarcargando").click();
				}
			});
		}
	});

	$(document).on("click",".gestionar",function(){
		$("#modalGestion").modal();
		ruc = $(this).attr('numruc');
		if(ruc){
			$.ajax({
				type: "GET",
				data: {
					numruc: ruc
				},
				url: '{{route("cmpEmergencia.buscarinfo")}}',
				beforeSend: function() {
					$("#cargando").modal();
				},
				success: function(result) {
					// console.log(result);
					if(result){
						$('#formGestionar input[name="gnumruc"]').val(result['NUM_RUC']);
						$('#formGestionar input[id="gcliente"]').val(result['NOMBRE']);
						$('#formGestionar input[name="gtelefono"]').val(result['TELEFONO1']);
						$('#formGestionar input[name="gcorreo"]').val(result['CORREO']);
						$('#formGestionar input[name="gventasa"]').val(numeral(result['SOLICITUD_VOLUMEN_VENTA']).format('0'));
						$('#formGestionar select[name="gflgReactiva1"]').val(result['FLG_TOMO_REACTIVA_1']);
						$('#formGestionar input[name="gdeudaMicroempresa"]').val(numeral(result['SOLICITUD_DEUDA_MICRO_PROM']).format('0,0'));
						$('#formGestionar select[name="ggestion"]').val(result['CONTACTO_RESULTADO']);
						$('#formGestionar textarea[name="gcomentario"]').val(result['CONTACTO_COMENTARIO']);
						$('#formGestionar input[name="gmontosriesgo"]').val(numeral(result['RIESGOS_MONTO']).format('0,0'));
						$("#bancor1").val(result['BANCO_REACTIVA_1']||'');

						montoSolicitado = numeral(result['SOLICITUD_OFERTA']);


						if(result['SOLICITUD_GRACIA']==null || result['SOLICITUD_PLAZO']==null){
							$("#gplazogracia").val("");
						}else{
							$("#gplazogracia").val(result['SOLICITUD_PLAZO']+"-"+result['SOLICITUD_GRACIA']);
						}

						//CARGA DATOS REACTIVA 1
						if (result['R1_ETAPA_ACTUAL'] == '7'){
							$('.fg-r1-desembolsado').removeClass('hidden');
							$('.fg-r1-no-desembolsado').addClass('hidden');
						}else{
							$('.fg-r1-desembolsado').addClass('hidden');
							$('.fg-r1-no-desembolsado').removeClass('hidden');
						}
						$('#formGestionar input[name="gr1etapa"]').val(result['R1_ETAPA_NOMBRE']||'-');
						$('#formGestionar input[name="gr1fechaContacto"]').val(result['R1_FECHA_CONTACTO']||'-');
						$('#formGestionar input[name="gr1gracia"]').val(result['R1_GRACIA']||'-');
						$('#formGestionar input[name="gr1monto"]').val(numeral(result['R1_MONTO']).format('0.0'));
						$('#formGestionar input[name="gr1plazo"]').val((result['R1_PLAZO']||'-') + ' meses');
						$('#formGestionar input[name="gr1tasa"]').val(result['R1_TASA']||'-');

						garantia = getCoberturaMonto(numeral(result['R1_MONTO']).value());
						$('#formGestionar input[name="gr1garantia"]').val(garantia*100 + '%');
						$('#formGestionar input[name="gr1riesgoDescubierto"]').val('S/. ' + numeral((1-garantia)*result['R1_MONTO']).format('0,0'));

						//Flag de Si tomo Reactiva 1
						$("#gmontoReactiva1").val(result['MONTO_PRESTAMO_REACTIVA_1']);
						ComboFlgreactiva1(result['FLG_TOMO_REACTIVA_1']);
						if (result['FLG_TOMO_REACTIVA_1']=="2" && result['BANCO_REACTIVA_1']=="2") {
							$("#gflgReactiva1").prop('disabled',true);
							$('#gmontoReactiva1').prop('readonly',true);
							$("#bancor1").prop('disabled',true);
						}

						//Logica Monto disponible
						montoReactiva1 = numeral($('#gmontoReactiva1').val());
						resultado = calculoReactiva2(result['SOLICITUD_VOLUMEN_VENTA'],result['SOLICITUD_DEUDA_MICRO_PROM'],montoReactiva1.value());
						montoDisponible = resultado[0];
						coberturaCal = resultado[1];
						$('#gmontocalculado').val(numeral(montoDisponible).format('0,0'));
						$('#gcoberturacalculado').val(coberturaCal*100 + '%');

						// Solicitud: Monto, cobertura y riesgo descubierto
						if (montoSolicitado.value() <= 0 ){
							montoSolicitado = numeral(montoDisponible);
						}
						$("#gmontosolicitado").val(montoSolicitado.format('0,0'));
						coberturaSol = getCoberturaMonto(montoSolicitado.value() + montoReactiva1.value());
						$('#griesgoDescubierto').val('S/. ' + numeral((1-coberturaSol)*montoSolicitado.value()).format('0,0'));
						$('#gcoberturasolicitada').val(coberturaSol*100 + '%');
					}
				},
				complete: function() {
					$(".cerrarcargando").click();
				}
			});
		}
	});

	function actualizarCalculadoReferido(){
		result = calculoReactiva2(
			numeral($("#ventasa").val()).value(),
			numeral($("#deudaMicroempresa").val()).value(),
			numeral($('#montoReactiva1').val()).value()
		)
		montoDisponible = result[0]
		cobertura = result[1]
		$('#montocalculado').val(numeral(montoDisponible).format('0,0'));
		$('#coberturacalculado').val(cobertura*100 + '%')
	}

	function actualizarSolicitudReferido(){
		montoSolicitado = numeral($("#montosolicitado").val());
		montoReactiva = numeral($("#addmontoReactiva1").val());
		coberturaSol = getCoberturaMonto(montoSolicitado.value() + montoReactiva.value())
		$("#coberturasolicitada").val(coberturaSol*100 + '%')
		$("#riesgoDescubierto").val('S/. ' + numeral((1-coberturaSol)*montoSolicitado.value()).format('0,0'))
	}

	function ComboFlgreactiva1Referido(flgtomoreactiva1){
		if ( flgtomoreactiva1 == null){
			$("#flgReactiva1").prop('disabled',false)
			$('#addmontoReactiva1').prop('readonly',false)
			$("#addbancor1").prop('disabled',false)
		}else{
			if (flgtomoreactiva1 == '1'){
				$("#flgReactiva1").prop('disabled',false)
				$('#addmontoReactiva1').prop('readonly',false)
				$("#addbancor1").prop('disabled',false)
			}
			if (flgtomoreactiva1 == '0'){
				$("#flgReactiva1").prop('disabled',false)
				$("#addmontoReactiva1").val(0);
				$('#addmontoReactiva1').prop('readonly',true)
				$("#addbancor1").val("");
				$("#addbancor1").prop('disabled',true)
			}
			if (flgtomoreactiva1 == '2'){
				$("#flgReactiva1").prop('disabled',false)
				$('#addmontoReactiva1').prop('readonly',false)
				$("#addbancor1").prop('disabled',false)
			}
		}
	}

	$("#ventasa").keyup(function() {
		actualizarCalculadoReferido()
	});

	$("#montosolicitado").keyup(function(event) {
		actualizarSolicitudReferido()
	});

	$("#montosolicitado").bind('paste', function(e) {
		var ctl = $(this);
		//setTimeout para ejecutar la funcion despues de 100 segundos
		setTimeout(function() {
			var validado = ValidarMontos($("#montosolicitado").val(),$("#montocalculado").val());
			if(validado==true){
				$(".guardarreferido").attr("disabled","disabled");
			}else{
				$(".guardarreferido").removeAttr("disabled");
			}
		}, 100);
	});

	$("#addmontoReactiva1").keyup(function(event) {
		actualizarCalculadoReferido()
		actualizarSolicitudReferido()
	});

	$("#flgReactiva1").change(function(event) {
		ComboFlgreactiva1Referido($("#flgReactiva1").val());
		actualizarCalculadoReferido()
		actualizarSolicitudReferido()
	});

	function actualizarCalculado(){
		result = calculoReactiva2(
			numeral($("#gventasa").val()).value(),
			numeral($("#gdeudaMicroempresa").val()).value(),
			numeral($('#gmontoReactiva1').val()).value()
		)
		montoDisponible = result[0]
		cobertura = result[1]
		$('#gmontocalculado').val(numeral(montoDisponible).format('0,0'));
		$('#gcoberturacalculado').val(cobertura*100 + '%')
	}

	function actualizarSolicitud(){
		montoSolicitado = numeral($("#gmontosolicitado").val());
		montoReactiva = numeral($("#gmontoReactiva1").val());
		coberturaSol = getCoberturaMonto(montoSolicitado.value() + montoReactiva.value())
		$('#gcoberturasolicitada').val(coberturaSol*100 + '%')
		$('#formGestionar input[name="griesgoDescubierto"]').val('S/. ' + numeral((1-coberturaSol)*montoSolicitado.value()).format('0,0'))
	}

	$("#gventasa").keyup(function() {
		actualizarCalculado()
	});

	$("#gmontosolicitado").keyup(function(event) {
		actualizarSolicitud()
	});

	$("#gmontosolicitado").bind('paste', function(e) {
		var ctl = $(this);
		//setTimeout para ejecutar la funcion despues de 100 segundos
		setTimeout(function() {
			var validado = ValidarMontos($("#gmontosolicitado").val(),$("#gmontocalculado").val());
			if(validado==true){
				$(".guardargestion").attr("disabled","disabled");
			}else{
				$(".guardargestion").removeAttr("disabled");
			}
		}, 100);
	});

	$("#gmontoReactiva1").keyup(function(event) {
		actualizarCalculado()
		actualizarSolicitud()
	});

	$("#gflgReactiva1").change(function(event) {
		ComboFlgreactiva1($("#gflgReactiva1").val());
		actualizarCalculado()
		actualizarSolicitud()
	});

	$('#formGestionar').submit(function(){

		if ($('#formGestionar select[name="ggestion"]').val() == ''){
			Swal.fire('No has seleccionado una gestión');
			return false
		}

		if ($('#formGestionar select[name="ggestion"]').val() == 5){
			if (numeral($('#formGestionar input[name="gmontosolicitado"]').val()).value()<=0){
				Swal.fire('El monto solicitado debe ser mayor a 0');
				return false
			}
			if ($('#formGestionar select[name="gplazogracia"]').val() == ''){
				Swal.fire('Debes seleccionar un periodo de plazo/gracia');
				return false
			}
		}
	});

	function tablaasignados(datos=null){
		if ($.fn.dataTable.isDataTable('#datatableasignados')) {
			$('#datatableasignados').DataTable().destroy();
		}

		$('#datatableasignados').DataTable({
			processing: true,
			"bAutoWidth": false,
			rowId: 'staffId',
			// dom: 'Blfrtip',
			serverSide: true,
			language: {"url": "{{ URL::asset('js/Json/Spanish.json') }}"},
			ajax: {
				"type": "GET",
				"url": "{{ route('cmpEmergencia.lista') }}",
				data: function(data) {
					data.etapa = datos;
				}
			},
			"aLengthMenu": [
				[25, 50, -1],
				[25, 50, "Todo"]
			],
			"iDisplayLength": 25,
			"order": [
				[0, "desc"],[1, "desc"]
			],
			columnDefs: [
				{
					targets: 0,
					data: 'WCL.CANAL_ACTUAL',
					name: 'WCL.CANAL_ACTUAL',
					searchable: false,
					render: function(data, type, row) {
						return row.CANAL_ACTUAL;
					}
				},
				{
					targets: 1,
					data: 'WCL.NUM_RUC',
					name: 'WCL.NUM_RUC',
					render: function(data, type, row) {
						$linea1="<span>"+row.NOMBRE+"</span><br>";
						$linea2="<span>"+row.NUM_RUC+"</span>";
						return $linea1+$linea2;
					}
				},
				{
					targets: 2,
					data: null,
					searchable: false,
					sortable: false,
					render: function(data, type, row) {
						return "<span>S/. "+numeral(row.MONTO_PRESTAMO_REACTIVA_1).format('0,0')+"</span><br>";
					}
				},
				{
					targets: 3,
					data: null,
					searchable: false,
					sortable: false,
					render: function(data, type, row) {
						$linea1="<span>Fecha: "+(row.CEF_FECHA|| '-')+"</span><br>";
						$linea2="<span>Monto: S/. "+(row.CEF_VENTAS || '-')+"</span>";
						return $linea1+$linea2;
					}
				},
				{
					targets: 4,
					data: null,
					searchable: false,
					sortable: false,
					render: function(data, type, row) {
						$linea1="<span>S/. "+numeral(row.SOLICITUD_VOLUMEN_VENTA).format('0,0')+"</span><br>";
						return $linea1;
					}
				},
				{
					targets: 5,
					data: null,
					searchable: false,
					sortable: false,
					render: function(data, type, row) {
						return  "S/. " + (numeral(row.RIESGOS_MONTO).format('0,0'));
					}
				},
				{
					targets: 6,
					data: null,
					searchable: false,
                    sortable: false,
					render: function(data, type, row) {
						if(row.CONTACTO_RESULTADO==null)
							return "No";
						else
							return "Si";
					}
				},
				{
					targets: 7,
					data: null,
					searchable: false,
                    sortable: false,
					render: function(data, type, row) {
						if(row.GESTION_NOMBRE==null){
							$html="<u><a class='gestionar' style='cursor: pointer' numruc='"+row.NUM_RUC+"'>Pendiente</a></u>";
						}else{
							$html="<u><a class='gestionar' style='cursor: pointer' numruc='"+row.NUM_RUC+"'>"+row.GESTION_NOMBRE+"</a></u>";
						}
						return $html;
					}
				},
                {
					targets: 8,
					data: 'WCL.NOMBRE',
					name: 'WCL.NOMBRE',
					visible: false,
					searchable: true,
					render: function(data, type, row) {
							return row.NOMBRE;
					}
                }
			]
		});
	}
</script>
@stop
