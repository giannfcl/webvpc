@extends('cmpEmergencia.menu')

@section('datable')

<div class="row" style="padding-bottom: 15px">
    <div class="form-group add">
        <div class="col-sm-2" >
            <select class="form-control" id="cboBanca" >
                <option value="">-- BANCA --</option>
                @foreach ($bancas as $key => $value)
                    <option value="{{ $key }}">{{ $value }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-sm-3">
            <select class="form-control" id="cboZonal" >
                <option value="">-- ZONAL --</option>
                @foreach ($zonales as $zonal)
                    <option value="{{ $zonal->ID_ZONAL }}">{{ $zonal->ZONAL }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-sm-4">
            <select class="form-control" id="cboEjecutivo" >
                <option value="">-- EJECUTIVO --</option>
                @foreach ($ejecutivos as $ejecutivo)
                    <option value="{{ $ejecutivo->REGISTRO }}">{{ $ejecutivo->NOMBRE }}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>

<div>
  <table class='table table-striped table-bordered jambo_table' id = "datatable">
    <thead>
      <tr>
        <th>Empresa</th>
        <th>Banca/Zonal</th>
        <th>Encargado</th>
        <th>Fecha de Envío</th>
        <th>Monto Aprobado</th>
        <th>Tasa</th>
        <th>Riesgo Descubierto</th>
        <th>Estado Revision</th>
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modalValidacion">
    <div class="modal-dialog" role="document" style="width: 1000px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Validacion</h4>
            </div>
            <div class="modal-body">
                <form id="formValidacion"  method = 'POST' class="form-horizontal form-label-left" enctype="multipart/form-data" action ="{{route('cmpEmergencia.validacion.gestionar')}}">
                    <div class="form-group add">
                        <div class="col-sm-6" >
                            <label class="control-label col-sm-4 col-xs-12">RUC: </label>
                            <div class="input-group col-sm-8 col-xs-12">
                                <input autocomplete="off" class="form-control"  type="text" name="numruc" readonly>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12">Teléfono:</label>
                            <div class="input-group col-sm-8 col-xs-12">
                                <input autocomplete="off" class="form-control"  type="text" name="telefono" readonly>
                            </div>
                        </div>
                    </div>
    
                    <div class="form-group add">
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12">Empresa:</label>
                            <div class="input-group col-sm-8 col-xs-12">
                                <input autocomplete="off" class="form-control"  type="text" name="cliente" readonly>
                            </div>
                        </div>
        
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12">Correo:</label>
                            <div class="input-group col-sm-8 col-xs-12">
                                <input autocomplete="off" class="form-control"  type="text" name="email" readonly>
                            </div>
                        </div>
                    </div>

                    <div class="form-group add">
                        <div class="col-sm-6">
                            <label class="control-label col-md-4 col-sm-4 col-xs-12" style="text-align: left;">Monto Solicitado S/.: </label>
                            <div class="input-group col-md-8 col-sm-8 col-xs-12" style="padding-left:0px">
                                <input autocomplete="off" class="form-control"  type="text" name="soloferta" id="soloferta" readonly onkeypress="return filterFloat(event,this);" maxlength="11">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label col-md-4 col-sm-4 col-xs-12" style="text-align: left;">Garantía: </label>
                            <div class="input-group col-md-8 col-sm-8 col-xs-12" style="padding-left:0px">
                                <input autocomplete="off" class="form-control"  type="text" readonly id="garantia" name="garantia" >
                            </div>
                        </div>
                    </div>

                    <div class="form-group add">
                        <div class="col-sm-6">
                            <label class="control-label col-md-4 col-sm-4 col-xs-12" style="text-align: left;">Monto Garantizado : </label>
                            <div class="input-group col-md-8 col-sm-8 col-xs-12" style="padding-left:0px">
                                <input autocomplete="off" class="form-control"  type="text" name="montogarantizado" id="montogarantizado" readonly>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <label class="control-label col-md-4 col-sm-4 col-xs-12" style="text-align: left;">Riesgo Descubierto : </label>
                            <div class="input-group col-md-8 col-sm-8 col-xs-12" style="padding-left:0px">
                                <input autocomplete="off" class="form-control"  type="text" name="montoriesgoibk" id="montoriesgoibk" readonly>
                            </div>
                        </div>
                    </div>

                    <div class="form-group add">
                        <div class="col-sm-6">
                            <label class="control-label col-md-4 col-sm-4 col-xs-12" style="text-align: left;">Tasa Máxima: </label>
                            <div class="input-group col-md-8 col-sm-8 col-xs-12" style="padding-left:0px">
                                <input autocomplete="off" class="form-control"  type="text" name="tasaMaxima" id="tasaMaxima" readonly>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <label class="control-label col-md-4 col-sm-4 col-xs-12" style="text-align: left;">Tasa Final: </label>
                            <div class="input-group col-md-8 col-sm-8 col-xs-12" style="padding-left:0px">
                                <input autocomplete="off" class="form-control"  type="text" name="tasaAcordada" id="tasaAcordada" readonly>
                            </div>
                        </div>
                    </div>

                    <div class="form-group add">
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12">Plazo / Gracia: </label>
                            <div class="input-group col-sm-8 col-xs-12">
                                <input autocomplete="off" class="form-control"  type="text" name="solplazo" readonly>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <label class="control-label col-sm-4 col-xs-12">Estado Revisión: </label>
                        <div class="form-group">
                            <div class="col-sm-5 col-xs-12" style="padding-left:0px">
                                <select class="form-control" name="gestion" required>
                                    <option value="">Selecciona una opción</option>
                                    @foreach ($gestiones as $key => $gestion)
                                        <option value="{{ $gestion->GESTION_ID }}">{{ $gestion->GESTION_NOMBRE }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <hr style="width:100%;">
                    <div class="form-group">
                        <div class="col-sm-2">
                            <ul class="nav nav-pills" id="navs">
                                @if(count($vistasdocs)>0)
                                    @foreach($vistasdocs as $vistadoc)
                                        <li class="{{$vistadoc['ID']=='COFIDE'?'active':''}}" style="width: 160px;margin-top: 10px">
                                            <a class="col-sm-10" id="v-{{$vistadoc['ID']}}-tab" data-toggle="tab" href="#v-{{$vistadoc['ID']}}">{{$vistadoc['NOMBRE']}}</a>
                                            <div class="col-sm-2 {{$vistadoc['ID']=='COFIDE'?'active':''}}" style="padding-top:6px;">
                                                &#x25B6
                                            </div>
                                        </li><br>
                                    @endforeach
                                @else
                                    <h5>No se encontro vistas</h5>
                                @endif
                            </ul>
                        </div>
                        <div class="col-sm-10 tab-content" style="padding-left: 0px">
                            @if(count($vistasdocs)>0)
                                @foreach($vistasdocs as $vistadoc)
                                    <div class="tab-pane fade{{$vistadoc['ID']=='COFIDE'?' in active':''}}" id="v-{{$vistadoc['ID']}}">
                                        @if($vistadoc['ID']=='COFIDE')
                                            <div class="col-sm-7">
                                                <label>1.Reporte tributario</label><i style="padding-right: 30%;" class="fa fa-question-circle" aria-hidden="true"  data-toggle="tooltip" data-placement="right" title="unat en Línea (clave sol)​ Incluye Pagos ESSALUD 2019 y Ventas mensuales 2019"></i><br>
                                                <label>2.Ficha SPLAFT (Formato IBK validado por COFIDE) </label><a target="_blank" href="https://interbankpe-my.sharepoint.com/:f:/g/personal/kberrios_intercorp_com_pe/EqJQkzVbWPlOt8YAy_vh1_sBgcXjNMkk9YuatvsblEhsCg?e=28WGmc"><i class="fa fa-link"></i></a><i class="fa fa-question-circle" aria-hidden="true"  data-toggle="tooltip" data-placement="right" title="Carpeta compartida de cumplimiento:  https://interbankpe-my.sharepoint.com/:f:/g/personal/kberrios_intercorp_com_pe/ EqJQkzVbWPlOt8YAy_vh1_sBgcXjNMkk9YuatvsblEhsCg?e=28WGmc En estos casos GTP solo valida la existencia del documento"></i><br>
                                                <label>3.Clasificación RCC (Sentinel/SBS) a Febrero 2020</label><i class="fa fa-question-circle" aria-hidden="true"  data-toggle="tooltip" data-placement="right" title="Infocorp o Sentinel"></i><br>
                                                <label>4.Verificar que no está en la lista de exclusión</label><i class="fa fa-question-circle" aria-hidden="true"  data-toggle="tooltip" data-placement="right" title="Anexo 1 del Reglamento Operativo de Reactiva. En estos casos GTP solo valida la existencia del documento"></i><br>
                                                <label>5.No esté dentro de la Ley 30737 </label><a target="_blank" href="https://www.minjus.gob.pe/ley-n-30737-y-su-reglamento/"><i class="fa fa-link"></i></a><i class="fa fa-question-circle" aria-hidden="true"  data-toggle="tooltip" data-placement="right" title="3 Listas del Ministerio de Justicia https://www.minjus.gob.pe/ley-n-30737-y-su-reglamento/ En estos casos GTP solo valida la existencia del documento"></i><br>
                                                <label>6.verificar que no pertenezca al Grupo Intercorp</label><i class="fa fa-question-circle" aria-hidden="true"  data-toggle="tooltip" data-placement="right" title="GE Intercorp (CICS)"></i><br>
                                            </div>
                                            <div class="col-sm-5">
                                                <label>7.Reporte de deuda coactiva SUNAT a feb 2020</label><i class="fa fa-question-circle" aria-hidden="true"  data-toggle="tooltip" data-placement="right" title="Consulta RUC Reactiva En estos casos GTP solo valida la existencia del documento"></i><br>
                                                <label>8.Pagaré y Acuerdo de llenado de Pagaré</label><i class="fa fa-question-circle" aria-hidden="true"  data-toggle="tooltip" data-placement="right" title="Clientes lo deja en original en el banco y soporte adjunta PDF"></i><br>
                                                <label>9.Solicitud de desembolso y DJ del beneficiario</label><i class="fa fa-question-circle" aria-hidden="true"  data-toggle="tooltip" data-placement="right" title="Clientes lo deja en original en el banco y soporte adjunta PDF"></i><br>
                                                <label>10.Ficha RUC</label><i class="fa fa-question-circle" aria-hidden="true"  data-toggle="tooltip" data-placement="right" title="Consulta RUC"></i><br>
                                                <label>11.Validación del monto máx del crédito</label><i class="fa fa-question-circle" aria-hidden="true"  data-toggle="tooltip" data-placement="right" title="Conforme de ESCOM / FINCORP"></i><br>
                                            </div>
                                        @elseif($vistadoc['ID']=='ADICIONAL')
                                            <div class="col-sm-2">
                                            </div>
                                            <div class="col-sm-6">
                                                <label>1.DNI Represente Legal</label><br>
                                                <label>2.IBR</label><br>
                                                <label>3.EEFF 3 últimos años y corte</label><br>
                                                <label>4.Vigencia de poderes testimonio</label><br>
                                                <label>5.Docs de apertura</label><br>
                                                <label>6.VB de cumplimiento</label><i class="fa fa-question-circle" aria-hidden="true"  data-toggle="tooltip" data-placement="right" title="Recuerda que debes ingresar tu WF de cumplimiento"></i><br>
                                            </div>
                                            <div class="col-sm-4">
                                            </div>
                                        @endif
                                    </div>
                                @endforeach
                            @else
                                No se encontraron vistas de validación de documentación
                            @endif
                        </div>
                    </div>
                    <input type="hidden" name="flgenvrevision" value="0" />
                    <center>
                        <button class="guardar btn btn-success" type="submit">Guardar</button>
                    </center>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal" hidden><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      <div class="modal-body">
        <img src="" id="imagepreview">
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>

@stop

@section('js-vista')
<style>
    #modalValidacion input{
        margin-bottom: -11px;
    }
    #navs li.active>a{
        background-color: #f0ad4e !important;
        color: #73879C !important;
    }
    #navs li>a{
        background-color: #f0ad4e !important;
    }
    #navs li.active>div{
        color: #164BC5 !important;
        visibility: visible;
    }
    #navs li>div{
        visibility: hidden;
    }
    .tooltip-inner {
        max-width: 350px;
        /* If max-width does not work, try using width instead */
        width: 350px; 
    }
    #icon1{
        color : blue;
        font-size: : 150px;
    }
</style>
<script>

    $(document).on("change","#cboBanca",function(){
		if ($.fn.dataTable.isDataTable('#dataTableLeads')) {
			$('#dataTableLeads').DataTable().destroy();
		}
		configurarTabla();
	});

    $(document).on("change","#cboZonal",function(){
		if ($.fn.dataTable.isDataTable('#dataTableLeads')) {
			$('#dataTableLeads').DataTable().destroy();
		}
		configurarTabla();
	});

    $(document).on("change","#cboEjecutivo",function(){
		if ($.fn.dataTable.isDataTable('#dataTableLeads')) {
			$('#dataTableLeads').DataTable().destroy();
		}
		configurarTabla();
	});


    configurarTabla();
    $('[data-toggle="tooltip"]').tooltip();
    

    var gestionesfiles = <?php echo json_encode($gestionesfiles); ?>;
    var gfiles={};
    $.each(gestionesfiles, function( i,v,a) {
        gfiles[v["ID"]]=v["NOMBRE"];
    });
    // console.log(pla_gra);

    $('.formatfecha').each(function () {
        $(this).on('keydown', function () {
            return false;
        });

        $(this).datepicker({
            maxViewMode: 1,
            language: "es",
            autoclose: true,
            startDate: "0d",
            endDate: "+180d",
            format: "yyyy-mm-dd",
        });
    });

    $(document).on("click",".btnPopupModal",function(){
        $("#modalValidacion").modal();
        var ruc = $(this).attr('ruc');
        $.ajax({
            url: "{{ route('cmpEmergencia.validacion.buscar') }}",
            type: 'GET',
            data: {
                numruc: ruc,
            },
            beforeSend: function() {
                $("#cargando").modal();
            },
            success: function (result) {
                $('#formValidacion input[name="numruc"]').val(result['NUM_RUC']);
                $('#formValidacion input[name="cliente"]').val(result['NOMBRE']);
                $('#formValidacion input[name="campanha"]').val(result['CAMPANIA']);
                $('#formValidacion input[name="telefono"]').val(result['TELEFONO1']);
                $('#formValidacion input[name="email"]').val(result['CORREO'])
                $('#formValidacion input[name="riesgosMonto"]').val(numeral(result['RIESGOS_MONTO']).format('0,0'));
                $('#formValidacion input[name="soloferta"]').val((result['SOLICITUD_OFERTA']||'0'));
                
                $('#formValidacion input[name="tasatentativa"]').val((result['SOLICITUD_TASA']));
                
                $monto = recalcularMonto(result['SOLICITUD_VOLUMEN_VENTA'],result['SOLICITUD_ESSALUD']);
                $('#formValidacion input[name="montoCalculado"]').val($monto);

                garantia = getCoberturaMonto(result['SOLICITUD_OFERTA']);
                $('#formValidacion input[name="garantia"]').val(garantia*100 + '%');
                $('#montogarantizado').val('S/. ' + numeral(garantia*result['SOLICITUD_OFERTA']).format('0,0'));
                $('#formValidacion input[name="montoriesgoibk"]').val(numeral((1-garantia)*result['SOLICITUD_OFERTA']).format('0,0'));

                $('#formValidacion input[name="tasaMaxima"]').val(result['TASA_MAXIMA_BCR']);
                $('#formValidacion input[name="tasaAcordada"]').val(result['TASA_FINAL_CLIENTE']);
                $('#formValidacion input[name="solplazo"]').val(result['SOLICITUD_PLAZO'] + ' meses / ' + result['SOLICITUD_GRACIA'] + ' meses');

                $('#formValidacion select[name="gestionsubasta"]').val(result['GESTION_SUBASTA']);
                $('#formValidacion textarea[name="comentariosubasta"]').html((result['GESTION_SUBASTA_COMENTARIO']));

                $('#formValidacion input[name="tasafinal"]').keyup();
            },
            complete: function() {
                // $("#cargando").hide();
                $(".cerrarcargando").click();
            }
        });
    });

    $('#formValidacion input[name="soloferta"]').keyup(function() {
		garantia = getCoberturaMonto($(this).val())
        $('#formValidacion input[name="garantia"]').val(garantia*100 + '%');
        $('#montogarantizado').val('S/. ' + (garantia*$(this).val()).toFixed(0))
	});

    $('#formValidacion input[name="essalud"]').keyup(function() {
		calc = recalcularMonto($('#formValidacion input[name="volventa"]').val(),$('#formValidacion input[name="essalud"]').val());
        $('#formValidacion input[name="montoCalculado"]').val(calc);
	});

	$('#formValidacion input[name="volventa"]').keyup(function() {
		calc = recalcularMonto($('#formValidacion input[name="volventa"]').val(),$('#formValidacion input[name="essalud"]').val());
        $('#formValidacion input[name="montoCalculado"]').val(calc);
	});

    $('#formValidacion select[name="gestionsubasta"]').change(function() {
        $('#formValidacion input[name="tasafinal"]').keyup();
    });
    $('#formValidacion input[name="tasafinal"]').keyup(function() {
        if($('#formValidacion select[name="gestionsubasta"]').val()==2){
            $(".guardarsubasta").removeAttr("disabled");
        }else{
            permitido = CalcularTasa($('#formValidacion input[name="tasamax"]').val(),$('#formValidacion input[name="tasafinal"]').val());
            if(permitido){
                $(".guardarsubasta").removeAttr("disabled");
            }else{
                $(".guardarsubasta").attr("disabled","disabled");
            }            
        }
	});

    function CalcularTasa(tasamax=0,tasafinal=0) {
        vtasamax = parseFloat(tasamax?tasamax:0);
        vtasafinal = parseFloat(tasafinal?tasafinal:0);
        if(vtasamax >= vtasafinal){
            return true;
        }else{
            return false;
        }
    }

    function configurarTabla(datos=null){
        if ($.fn.dataTable.isDataTable('#datatable')) {
            $('#datatable').DataTable().destroy();
        }

        $('#datatable').DataTable({
            processing: true,
            "bAutoWidth": false,
            rowId: 'staffId',
            // dom: 'Blfrtip',
            serverSide: true,
            language: {"url": "{{ URL::asset('js/Json/Spanish.json') }}"},
            ajax: {
                "type": "GET",
                "url": "{{ route('cmpEmergencia.validacion.lista') }}",
                data: function(data) {
                    data.etapa = datos;
                    data.banca = $('#cboBanca').val();
                    data.zonal = $('#cboZonal').val();
                    data.ejecutivo = $('#cboEjecutivo').val();
                }
            },
            "aLengthMenu": [
                    [25, 50, -1],
                    [25, 50, "Todo"]
            ],
            "iDisplayLength": 25,
            "order": [
                    [0, "desc"]
            ],
            columnDefs: [
                {
                    targets: 0,
                    data: 'WCL.NUM_RUC',
                    name: 'WCL.NUM_RUC',
                    render: function(data, type, row) {
                        return row.NUM_RUC + '<br/>' + row.NOMBRE;
                    }
                },
                {
                    targets: 1,
                    data: null,
                    searchable: false,
                    sortable:false,
                    render: function(data, type, row) {
                        return (row.BANCA||'-')
                    }
                },
                {
                    targets: 2,
                    data: null,
                    searchable: false,
                    sortable:false,
                    render: function(data, type, row) {
                        return (row.NOMBRE_EJECUTIVO||'-')
                    }
                },
                {
                    targets: 3,
                    data: null,
                    searchable: false,
                    sortable:false,
                    render: function(data, type, row) {
                        return row.FECHA_APROBACIO_SUBASTA||'-'
                    }
                },
                {
                    targets: 4,
                    data: null,
                    searchable: false,
                    sortable:false,
                    render: function(data, type, row) {
                        return 'S/. ' + numeral(row.SOLICITUD_OFERTA).format('0,0')
                    }
                },
                {
                    targets: 5,
                    data: null,
                    searchable: false,
                    sortable:false,
                    render: function(data, type, row) {
                        return numeral(row.TASA_FINAL_CLIENTE).value() + '%'
                    }
                },
                {
                    targets: 6,
                    data: null,
                    searchable: false,
                    sortable:false,
                    render: function(data, type, row) {
                        return 'S/. ' + numeral(row.SOLICITUD_OFERTA * (1 - getCoberturaMonto(row.SOLICITUD_OFERTA))).format(0,0)
                    }
                },
                {
					targets: 7,
					data: 'WCL.VALIDACION_DOCUMENTACION_GESTION',
                    name: 'WCL.VALIDACION_DOCUMENTACION_GESTION',
					searchable: false,
                    sortable: true,
					render: function(data, type, row) {
                        if(row.VALIDACION_DOCUMENTACION_GESTION == null){
                            return '<button class="btnPopupModal btn btn-sm btn-warning" ruc="' +  row.NUM_RUC + '">Pendiente</button>';
                        }else{
                            return html = '<button class="btnPopupModal btn btn-sm btn-primary" ruc="' +  row.NUM_RUC + '">' + row.GESTION_NOMBRE +'</button>';
                        }
						
					}
                }
            ]
        });
    }
</script>
@stop
