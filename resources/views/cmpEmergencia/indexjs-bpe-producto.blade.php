@extends('Layouts.layout')

@section('js-libs')
<link href="{{ URL::asset('css/datatables.min.css') }}" rel="stylesheet" type="text/css">

<script type="text/javascript" src="{{ URL::asset('js/datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/numeral/numeral.min.js') }}"></script>
<!--Falta instalar la librería-->
<style type="text/css">
	table,tr{
			font-size: 11px;
	}
	fieldset.scheduler-border {
		border: 1px groove #ddd !important;
		padding: 0 1.4em 1.4em 1.4em !important;
		margin: 0 0 1.5em 0 !important;
		-webkit-box-shadow:  0px 0px 0px 0px #000;
				box-shadow:  0px 0px 0px 0px #000;
	}

	legend.scheduler-border {
		font-size: 1.2em !important;
		font-weight: bold !important;
		text-align: left !important;
		width:auto;
		padding:0 10px;
		border-bottom:none;
	}
	.loader {
		margin-top:10%;
		border: 16px solid #f3f3f3;
		border-radius: 50%;
		border-top: 16px solid #3498db;
		width: 120px !important;
		height: 120px !important;
		-webkit-animation: spin 2s linear infinite; /* Safari */
		animation: spin 2s linear infinite;
	}

	/* Safari */
	@-webkit-keyframes spin {
		0% { -webkit-transform: rotate(0deg); }
		100% { -webkit-transform: rotate(360deg); }
	}

	@keyframes spin {
		0% { transform: rotate(0deg); }
		100% { transform: rotate(360deg); }
	}

	.modal-body {
		max-height: calc(95vh - 75px);
		overflow-y: auto;
	}
</style>
@stop

@section('content')
  @yield('vista-reactiva-bpe')
@stop
@section('js-scripts')
<script>

	dtLeads();
	var combocantidadclientes = <?php echo json_encode($combocantidadclientes); ?>;
	var combocontrolcobranza = <?php echo json_encode($combocontrolcobranza); ?>;
	var comboticketpromedio = <?php echo json_encode($comboticketpromedio); ?>;

	function dtLeads(id_zona=null,id_centro=null,id_tienda=null,gestion_en=null,gestion_r=null,prioridad = null,traderdesembolso = null){

		if ($.fn.dataTable.isDataTable('#dataTableLeads')) {
			$('#dataTableLeads').DataTable().destroy();
		}
		$('#dataTableLeads').DataTable({
			processing: true,
			"bAutoWidth": false,
			rowId: 'staffId',
			// dom: 'Blfrtip',
			serverSide: true,
			language: {"url": "{{ URL::asset('js/Json/Spanish.json') }}"},
			ajax: {
				"type": "GET",
				"url": "{{ route('cmpEmergencia.bpe.lista-producto') }}",
				data: function(data) {
					data.etapa = null;
					data.id_zona = null;
					data.id_centro = null;
					data.id_tienda = null;
					data.gestion_en = null;
					data.gestion_r = null;
					data.prioridad = null;
					data.traderdesembolso = null;
				}
			},
			"aLengthMenu": [
				[25, 50, -1],
				[25, 50, "Todo"]
			],
			"iDisplayLength": 25,
			"order": [
				[4, "desc"]
            ],
			columnDefs: [
				{
					targets: 0,
					data: 'WCL.NUM_RUC',
		            name: 'WCL.NUM_RUC',
					render: function(data, type, row) {
						return row.NUM_RUC + '<br>' + row.NOMBRE;
					}
				},
				{
					targets: 1,
					data: 'WCL.FLG_COBRO_SIMPLE',
		            name: 'WCL.FLG_COBRO_SIMPLE',
					searchable: false,
					sortable: false,
					render: function(data, type, row) {
						if (row.FLG_COBRO_SIMPLE>0) {
							return "SI";
						}
						return "NO";
					}
				},
				{
					targets: 2,
					data: 'WCL.CS_CANT_CLIENTES_PAGAN',
					name: 'WCL.CS_CANT_CLIENTES_PAGAN',
          			searchable: false,
          			sortable: false,
					render: function(data, type, row) {
						if (row.CS_CANT_CLIENTES_PAGAN>0) {
			            	return combocantidadclientes[row.CS_CANT_CLIENTES_PAGAN];
			            }
			            return "-";
					}
				},
				{
					targets: 3,
					data: 'WCL.CS_CONTROL_COBRANZA',
					name: 'WCL.CS_CONTROL_COBRANZA',
					searchable: false,
					sortable: false,
					render: function(data, type, row) {
						if (row.CS_CONTROL_COBRANZA>0) {
							return combocontrolcobranza[row.CS_CONTROL_COBRANZA];
						}
						return "-";
					}
				},
				{
					targets: 4,
					data: 'WCL.CS_TICKET_PROM_COBRANZA',
					name: 'WCL.CS_TICKET_PROM_COBRANZA',
					searchable: false,
					sortable: false,
					render: function(data, type, row) {
						if (row.CS_TICKET_PROM_COBRANZA>0) {
							return comboticketpromedio[row.CS_TICKET_PROM_COBRANZA];
						}
						return "-";
					}
				},
				{
					targets: 5,
					data: null,
		            searchable: false,
		            sortable: false,
					render: function(data, type, row) {
                        return row.FECHA_GESTION;
					}
				}
			]
		});
	}

</script>
@stop
