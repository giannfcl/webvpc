@extends('cmpEmergencia.indexjs-bpe-producto')

@section('vista-reactiva-bpe')


<div class="x_panel">
	<table class='table table-striped table-bordered jambo_table' id="dataTableLeads">
			<thead>
				<tr>
					<th scope="col" style="font-size: 12.5px;">Empresa</th>
					<th scope="col" style="font-size: 12.5px;">Acepto Cobro<br>Simple</th>
					<th scope="col" style="font-size: 12.5px;">Cant. Clientes<br>le Pagan</th>
					<th scope="col" style="font-size: 12.5px;">Control de<br> Cobranzas</th>
					<th scope="col" style="font-size: 12.5px;">Ticket Prom.<br> Cobranzas</th>
					<th scope="col" style="font-size: 12.5px;">Fecha Gestión</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
	</table>
</div>

@stop
