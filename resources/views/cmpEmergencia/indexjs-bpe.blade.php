@extends('Layouts.layout')

@section('js-libs')
<link href="{{ URL::asset('css/datatables.min.css') }}" rel="stylesheet" type="text/css">

<script type="text/javascript" src="{{ URL::asset('js/datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/numeral/numeral.min.js') }}"></script>
<!--Falta instalar la librería-->
<style type="text/css">
	table,tr{
			font-size: 11px;
	}
	fieldset.scheduler-border {
		border: 1px groove #ddd !important;
		padding: 0 1.4em 1.4em 1.4em !important;
		margin: 0 0 1.5em 0 !important;
		-webkit-box-shadow:  0px 0px 0px 0px #000;
				box-shadow:  0px 0px 0px 0px #000;
	}

	legend.scheduler-border {
		font-size: 1.2em !important;
		font-weight: bold !important;
		text-align: left !important;
		width:auto;
		padding:0 10px;
		border-bottom:none;
	}
	.loader {
		margin-top:10%;
		border: 16px solid #f3f3f3;
		border-radius: 50%;
		border-top: 16px solid #3498db;
		width: 120px !important;
		height: 120px !important;
		-webkit-animation: spin 2s linear infinite; /* Safari */
		animation: spin 2s linear infinite;
	}

	/* Safari */
	@-webkit-keyframes spin {
		0% { -webkit-transform: rotate(0deg); }
		100% { -webkit-transform: rotate(360deg); }
	}

	@keyframes spin {
		0% { transform: rotate(0deg); }
		100% { transform: rotate(360deg); }
	}

	.modal-body {
		max-height: calc(95vh - 75px);
		overflow-y: auto;
	}
</style>
@stop

@section('content')
  @yield('vista-reactiva-bpe')
@stop
@section('js-scripts')
<script>

	dtLeads();
	function CalculoSueldoPromedio(monotoessalud=null,nrotrabajadoresdependientes=null) {
		porcentaje = 0.09;
		if (isNaN(parseFloat(monotoessalud))) {monotoessalud = 0};
		if (isNaN(parseFloat(nrotrabajadoresdependientes))) {nrotrabajadoresdependientes = 0};
		if (nrotrabajadoresdependientes>0) {
			//Swal.fire(numeral((monotoessalud/nrotrabajadoresdependientes)/porcentaje).format('0.[00]'));
			return numeral((monotoessalud/nrotrabajadoresdependientes)/porcentaje).format('0.[00]');
		}else{
			return '';
		}
	}
	$(document).on("change","#id_zona",function(){
		if ($.fn.dataTable.isDataTable('#dataTableLeads')) {
			$('#dataTableLeads').DataTable().destroy();
		}
		$("#id_centro").html("<option value='' selected>Todos...</option>");
		$("#id_tienda").html("<option value='' selected>Todos...</option>");
		if($(this).val()==""){
		}else{
			$.ajax({
				url: "{{ route('bpe.campanha.utils.getCentrosByZonal') }}",
				type: 'GET',
				data: {
					zonal: $(this).val()
				},
				success: function (result) {
					$centros="<option value='' selected>Todos...</option>";
					$.each( result, function( key, value ) {
						$centros+="<option value='"+value['ID_CENTRO']+"'>"+value['CENTRO']+"</option>";
					});
					$("#id_centro").html($centros);
				},
			});
		}
		dtLeads()
	});

	$(document).on("change","#id_centro",function(){
		if ($.fn.dataTable.isDataTable('#dataTableLeads')) {
			$('#dataTableLeads').DataTable().destroy();
		}
		$("#id_tienda").html("<option value='' selected>Todos...</option>");
		if($(this).val()==""){
		}else{
			$.ajax({
				url: "{{ route('bpe.campanha.utils.getTiendasByCentro') }}",
				type: 'GET',
				data: {
					centro: $(this).val()
				},
				success: function (result) {
					$tiendas="<option value='' selected>Todos...</option>";
					$.each( result, function( key, value ) {
						$tiendas+="<option value='"+value['ID_TIENDA']+"'>"+value['TIENDA']+"</option>";
					});
					$("#id_tienda").html($tiendas);
				},
			});
		}
		dtLeads();
	});

	$(document).on("change","#id_tienda",function(){
		dtLeads();
	});

	$(document).on("change","#gestion_en",function(){
		dtLeads();
	});

	$(document).on("change","#gestion_r",function(){
		dtLeads();
	});

	$(document).on("change","#prioridad",function(){
		dtLeads();
	});

	$(document).on("change","#clientescanal",function(){
		dtLeads();
	});

	$(document).on("change","#traderdesembolso",function(){
		dtLeads();
	});

	// FUNCION PARA OBTENER LAS TASAS
	$('#btnGetTasas').click(function(){
		$.ajax({
          url: "{{ route('cmpEmergencia.bpe.getTasas') }}",
          type: 'GET',
          data: {
  					ruc:  $('#formGestionar input[name="numruc"]').val(),
  					monto: $('#formGestionar input[name="solicitudMonto"]').val(),
  					cobertura: $('#formGestionar input[name="solicitudCobertura"]').val()
  				},
  				beforeSend: function() {
  					$('#btnGetTasas').prop('disabled',true).text('Buscando...');
  				},
          success: function (result) {
					var $cbo= $("#formGestionar select[name='solicitudTasa']");
					$cbo.find('option').remove().end()
					if (result['status'] == 'ok'){
						$.each(result['tasas'], function() {
							$cbo.append($("<option />").val(this.tasa).text(this.tasa + '%'));
						});
					}else{
						$cbo.append('<option value="0" selected>--Sin Subasta Disponible--</option>').val('0')
					}
				},
				complete: function() {
					$('#btnGetTasas').prop('disabled',false).text('Buscar');
				}
		});

	});

	///Carga de Datos
	$(document).on("click",".btnGestionar",function(){
		$("#modalGestion").modal();
		//limpiar form/loading animation
		var ruc = $(this).attr('ruc')
		$.ajax({
			url: "{{ route('cmpEmergencia.bpe.getByRuc') }}",
			type: 'GET',
			data: {
						ruc: ruc
					},
					beforeSend: function() {
						$("#cargando").modal();
					},
			success: function (result) {

			/************************/
			/*PASO 1: CARGA DE DATOS*/
			/************************/

			$('#formGestionar input[name="numruc"]').val(result['NUM_RUC']);
			$('#formGestionar input[name="cliente"]').val(result['NOMBRE']);

			$('#formGestionar input[name="rucrrll"]').val(result['RRLL_DOCUMENTO']);
			$('#formGestionar input[name="rrll"]').val(result['RRLL_NOMBRES']);
			$('#formGestionar input[name="rrllApellidos"]').val(result['RRLL_APELLIDOS']);
			$('#formGestionar select[name="rrllEstadoCivil"]').val(result['RRLL_ESTADO_CIVIL']);

			$('#formGestionar input[name="rucrrll2"]').val(result['RRLL_DOCUMENTO_2']);
			$('#formGestionar input[name="rrll2"]').val(result['RRLL_NOMBRES_2']);
			$('#formGestionar input[name="rrllApellidos2"]').val(result['RRLL_APELLIDOS_2']);
			$('#formGestionar select[name="rrllEstadoCivil2"]').val(result['RRLL_ESTADO_CIVIL_2']);

			$('#formGestionar select[name="tipoVia"]').val(result['DIR_TIPO_VIA']);
			$('#formGestionar input[name="direcNro"]').val(result['DIR_NRO']);
			$('#formGestionar input[name="direcInt"]').val(result['DIR_INTERIOR']);
			$('#formGestionar input[name="direcVia"]').val(result['DIR_DESCRIPCION_VIA']);
			$('#formGestionar input[name="direcLote"]').val(result['DIR_LOTE']);
			$('#formGestionar input[name="direcManz"]').val(result['DIR_MANZANA']);
			$('#formGestionar input[name="direcUrb"]').val(result['DIR_URB_ZONA']);

			$('#formGestionar select[name="tipoSociedad"]').val(result['TIPO_SOCIEDAD']);
			$('#formGestionar input[name="tipoCliente"]').val(result['TIPO_CLIENTE']);
			$('#formGestionar input[name="telefono"]').val(result['TELEFONO1']);
			$('#formGestionar input[name="telefono2"]').val(result['TELEFONO2']);
			$('#formGestionar input[name="telefono3"]').val(result['TELEFONO3']);
			$('#formGestionar input[name="email"]').val(result['CORREO']);
			$('#formGestionar input[name="direccion"]').val(result['DIRECCION']);
			$('#formGestionar input[name="analista"]').val(result['ANALISTA_RIESGOS']);
			$('#formGestionar textarea[name="riesgosComentario"]').val(result['RIESGOS_COMENTARIO']);

			if(result['DIR_UBIGEO'] != null){
				var departamento = result['DIR_UBIGEO'].substr(0, 2);
				var provincia    = result['DIR_UBIGEO'].substr(2, 2);
				var distrito     = result['DIR_UBIGEO'].substr(4, 2);

				$('#formGestionar select[name="departamento"]').val(departamento);
				$('#formGestionar select[name="provincia"]').val(provincia);
				$('#formGestionar select[name="distrito"]').val(distrito);

				changeDepartamento(departamento,provincia);
				changeProvincia(departamento,provincia,distrito);
				getTiendas(distrito,provincia,departamento,result['TIENDA'])
			}
			else{
				$('#formGestionar select[name="departamento"]').val("");
				$('#formGestionar select[name="provincia"]').val("");
				$('#formGestionar select[name="distrito"]').val("");
			}

				$('#formGestionar input[name="ventas"]').val(result['SOLICITUD_VOLUMEN_VENTA']||'0');
				$('#formGestionar input[name="essalud"]').val(result['SOLICITUD_ESSALUD']||'0');
				$('#formGestionar input[name="solicitudMonto"]').val(result['SOLICITUD_OFERTA']||'0');
				$('#formGestionar input[name="solicitudPlazo"]').val(result['SOLICITUD_PLAZO']);
				$('#formGestionar input[name="riesgosMonto"]').val(numeral(result['RIESGOS_MONTO']||'0').format('0,0'));
				$('#formGestionar textarea[name="comentario"]').val(result['CONTACTO_COMENTARIO']);
				$('#formGestionar input[name="empleados"]').val(result['NRO_EMPLEADOS']);
				$('#formGestionar select[name="bancoReactiva1"]').val(result['BANCO_REACTIVA_1']);
				$('#formGestionar input[name="cuentaNumero"]').val(result['CUENTA_NUMERO']);
				$('#formGestionar select[name="cuentaTipo"]').val($.trim(result['CUENTA_TIPO']));
				$('#formGestionar select[name="flgReactiva1"]').val(result['FLG_TOMO_REACTIVA_1']);
				$('#formGestionar input[name="tasa"]').val((result['TASA_FINAL_CLIENTE']||result['SUBASTA_TASA'])||'');

				$('#formGestionar input[name="flgMontoLimite"]').val(result['FLG_TOPE_MAXIMO'])
				$('#formGestionar input[name="montoLimite"]').val(result['TOPE_MAXIMO'])
				$('#formGestionar select[name="tipoEnvioGTP"]').val(result['TIPO_ENVIO_GTP']);
				$('#formGestionar input[name="canalActual"]').val(result['CANAL_ACTUAL']);
				$('#formGestionar input[name="girodesc"]').val(result['GIRO_DESCRIPCION']);

				//Información de riesgos
				var riesgosComentario = '';
				if(result['GESTION_SUB_NOMBRE']){
					riesgosComentario = result['GESTION_SUB_NOMBRE'] + ': ' + result['RIESGOS_COMENTARIO']
				}else{
					riesgosComentario = result['RIESGOS_COMENTARIO']
				}
				$('#formGestionar textarea[name="riesgosComentario"]').val(riesgosComentario);
				if (result['RIESGOS_GESTION']){
					$('.riesgosArea').removeClass('hidden');
				}else{
					$('.riesgosArea').addClass('hidden');
				}

				/********************************************/
				/* PASO 2: REVISAR SI TIENE MONTO REACTIVA 1 */
				/********************************************/

				//Status formulario segun flg reactiva
				if (result['FLG_TOMO_REACTIVA_1'] == null){
					//reactiva1 = recalcularMonto(result['SOLICITUD_VOLUMEN_VENTA'],result['SOLICITUD_ESSALUD'])
					reactiva1 = 0
				}else{
					if (result['FLG_TOMO_REACTIVA_1'] == '0'){
						reactiva1 = 0
					}else if (result['FLG_TOMO_REACTIVA_1'] == '1'){
						reactiva1 = result['MONTO_PRESTAMO_REACTIVA_1']
					}else if(result['FLG_TOMO_REACTIVA_1'] == '2'){
						reactiva1 = result['MONTO_PRESTAMO_REACTIVA_1']
					}
				}

				$('#formGestionar input[name="montoReactiva1"]').val(reactiva1)
				recalcularMontoReactiva2()
				actualizarCobertura()

				
				/********************************************/
				/* PASO 3: ESTADO DEL FORMULARIO SEGUN GESTION */
				/********************************************/
				
				//Informacion de televentas
				if(result['CANAL_ACTUAL'] == 'TELEVENTAS')	{
					$('.televentasArea').removeClass('hidden');
				}else{
					$('.televentasArea').addClass('hidden');
				}

				//Estado del formulario
				$('.areaTasa').addClass('hidden');
				$('.areaTasaLectura').removeClass('hidden');
				$('.areaTipoEnvioGTP').addClass('hidden');

				ubigeoSoloLectura(true)

				$("#formGestionar select[name='solicitudTasa']").find('option').remove().end().append($("<option />").val('').text('--Tasa--'))
				if (
					[1,3,8].includes(parseInt(result['ETAPA_ID']))||
					[3].includes(parseInt(result['CONTACTO_RESULTADO']))
				){
					editable()
					ubigeoSoloLectura(result['NUM_RUC'].substr(0,2) == '20')
					setGestiones();
					$('#formGestionar select[name="gestion"]').val(result['CONTACTO_RESULTADO'])
				}else if(
					[7].includes(parseInt(result['CONTACTO_RESULTADO'])) &&
					[10,14].includes(parseInt(result['RIESGOS_GESTION']))
				){
					reenvioRiesgos()
					setGestiones()
					$('#formGestionar select[name="gestion"]').val(result['CONTACTO_RESULTADO'])
				}else if(
					[9].includes(parseInt(result['RIESGOS_GESTION'])) &&
					[7].includes(parseInt(result['CONTACTO_RESULTADO']))
				){
					sololectura(false,false,false,false,false,false)
					if (result['TASA_FINAL_CLIENTE'] === null && result['SUBASTA_TASA'] === null){
						$('.areaTasa').removeClass('hidden');
						$('.areaTasaLectura').addClass('hidden');
					}
					setGestionesEnvioValorados();
					$('#formGestionar select[name="gestion"]').val('')
				}else if (
					[20].includes(parseInt(result['CONTACTO_RESULTADO']))
				){
					if (result['TASA_FINAL_CLIENTE'] === null && result['SUBASTA_TASA'] === null){
						$('.areaTasa').removeClass('hidden');
						$('.areaTasaLectura').addClass('hidden');
					}
					$('.areaTipoEnvioGTP').removeClass('hidden')
					$('#formGestionar select[name="tipoEnvioGTP"]').prop('disabled',false)
					sololectura(true,false,false,false,false,false)
					setGestionesEnvioGTP()
					$('#formGestionar select[name="gestion"]').val('')
				}else if (
					[21].includes(parseInt(result['CONTACTO_RESULTADO']))
				){
					if (result['TIPO_ENVIO_GTP'] == '2'){
						$('#formGestionar select[name="tipoEnvioGTP"]').prop('disabled',true)
						sololectura(true,true,false,false,true,false)
					}else{
						$('#formGestionar select[name="tipoEnvioGTP"]').prop('disabled',false)
						sololectura(true,false,false,false,true,false)
					}
					$('.areaTipoEnvioGTP').removeClass('hidden');
					setGestionesEnvioGTP();
					$('#formGestionar select[name="gestion"]').val(result['CONTACTO_RESULTADO'])
				}else{
					sololectura(true,true,false)
					setGestiones();
					$('#formGestionar select[name="gestion"]').val(result['CONTACTO_RESULTADO'])
				}

				/********************************************/
				/* PASO 4: PRODUCTOS CROSSELLING */
				/********************************************/

				$('#formGestionar input[name="tienecuentadolares"]').val(result['FLG_TIENE_CUENTA_DOL'] && result['FLG_TIENE_CUENTA_DOL']==1?'SI':'NO');
				$('#formGestionar select[name="flg_creditocomprasdolares"]').val(result['FLG_DESTINO_COMPRA_DOLARES']);
				if (result['FLG_DESTINO_COMPRA_DOLARES']==1) {
					$('#formGestionar input[name="montodolarescompra"]').val(result['MONTO_COMPRA_DOLARES']);
					$('#formGestionar input[name="montodolarescompra"]').removeAttr("readonly");
				}else{
					$('#formGestionar input[name="montodolarescompra"]').val("");
					$('#formGestionar input[name="montodolarescompra"]').attr("readonly","readonly");
				}

				botonValorado(result['CONTACTO_RESULTADO']);
				changeClass();
				$('#formGestionar select[name="flgcuensueldo"]').val(result['FLG_ACEPTO_CUENTA_SUELDO']||'');
				$('#formGestionar input[name="montopagoessalud"]').val(result['MONTO_PAGO_ESSALUD']);
				$('#formGestionar input[name="numtrabajadores"]').val(result['NRO_TRABAJADORES_QUINTA_CATEGORIA']);
				$('#formGestionar input[name="sueldopromedio"]').val(result['SUELDO_PROMEDIO']);
				DatosCuentaSueldo(result['FLG_ACEPTO_CUENTA_SUELDO']);
				$('#formGestionar select[name="cs_flgcobrosimple"]').val(result['FLG_COBRO_SIMPLE']).change();
				$('#formGestionar select[name="cs_cantclientespagan"]').val(result['CS_CANT_CLIENTES_PAGAN']||0);
				$('#formGestionar select[name="cs_controlcobranza"]').val(result['CS_CONTROL_COBRANZA']||0);
				$('#formGestionar select[name="cs_ticketpromediocobranza"]').val(result['CS_TICKET_PROM_COBRANZA']||0);

				if ("{{$rol}}"==90) {
					sololectura();
				}
				if ("{{$editarrll}}"==1) {
					$('#formGestionar input[name="telefono"]').attr("disabled",false);
					if (([1,2,3,4,5].includes(parseInt(result['CONTACTO_RESULTADO'])) || result['CONTACTO_RESULTADO']==null) && result['TIPO_CLIENTE']=='RETAIL') {
						ubigeoSoloLectura(false)
						$('#formGestionar select[name="tienda"]').attr("disabled",false);
						$('#formGestionar input[name="email"]').attr("disabled",false);
						$('#formGestionar input[name="empleados"]').attr("disabled",false);
					}
				}

		    },
			error: function (xhr, status, text) {
				e.preventDefault();
				Swal.fire(
				  'Hubo un error al registrar el dato de contacto, inténtelo mas tarde',
				  'CLick en el botón!',
				  'error'
				)
			},
			complete: function() {
				$(".cerrarcargando").click();
			}
    	});
	});

	function ubigeoSoloLectura(cond){
		$('#formGestionar select[name="tipoVia"]').attr("disabled",cond);
		$('#formGestionar input[name="direcVia"]').attr("readonly",cond);
		$('#formGestionar input[name="direcNro"]').attr("readonly",cond);
		$('#formGestionar input[name="direcLote"]').attr("readonly",cond);
		$('#formGestionar input[name="direcUrb"]').attr("readonly",cond);
		$('#formGestionar input[name="direcManz"]').attr("readonly",cond);
		$('#formGestionar input[name="direcInt"]').attr("readonly",cond);
		$('#formGestionar select[name="departamento"]').attr("disabled",cond);
		$('#formGestionar select[name="provincia"]').attr("disabled",cond);
		$('#formGestionar select[name="distrito"]').attr("disabled",cond);
	}
	$('#formGestionar select[name="cs_flgcobrosimple"]').change(function() {
		if ($(this).val()==1) {
			$('#formGestionar select[name="cs_controlcobranza"]').removeAttr("readonly");
			$('#formGestionar select[name="cs_cantclientespagan"]').removeAttr("readonly");
			$('#formGestionar select[name="cs_ticketpromediocobranza"]').removeAttr("readonly");
		}else{
			$('#formGestionar select[name="cs_controlcobranza"]').val(0);
			$('#formGestionar select[name="cs_cantclientespagan"]').val(0);
			$('#formGestionar select[name="cs_ticketpromediocobranza"]').val(0);
			$('#formGestionar select[name="cs_controlcobranza"]').attr("readonly","readonly");
			$('#formGestionar select[name="cs_cantclientespagan"]').attr("readonly","readonly");
			$('#formGestionar select[name="cs_ticketpromediocobranza"]').attr("readonly","readonly");
		}
	});
	function DatosCuentaSueldo(valor) {
		if (valor=='1')
		{
			$('#formGestionar input[name="montopagoessalud"]').removeAttr('readonly');
			$('#formGestionar input[name="numtrabajadores"]').removeAttr('readonly');
		}else{
			$('#formGestionar input[name="montopagoessalud"]').val(0);
			$('#formGestionar input[name="numtrabajadores"]').val(0);
			$('#formGestionar input[name="sueldopromedio"]').val(0);
			$('#formGestionar input[name="montopagoessalud"]').attr('readonly','readonly');
			$('#formGestionar input[name="numtrabajadores"]').attr('readonly','readonly');
		}
	}

	$('#formGestionar select[name="flgcuensueldo"]').change(function() {
		DatosCuentaSueldo($(this).val());
	});

	$('#formGestionar input[name="montopagoessalud"]').keyup(function() {
		$('#formGestionar input[name="sueldopromedio"]').val(CalculoSueldoPromedio($(this).val(),$('#formGestionar input[name="numtrabajadores"]').val()));
	});
	$('#formGestionar input[name="numtrabajadores"]').keyup(function() {
		$('#formGestionar input[name="sueldopromedio"]').val(CalculoSueldoPromedio($('#formGestionar input[name="montopagoessalud"]').val(),$(this).val()));
	});

	$(document).on("change",'#formGestionar select[name="flg_creditocomprasdolares"]',function() {
		if ($(this).val()=="1") {
			$('#formGestionar input[name="montodolarescompra"]').removeAttr("readonly");
		}else{
			$('#formGestionar input[name="montodolarescompra"]').val("");
			$('#formGestionar input[name="montodolarescompra"]').attr("readonly","readonly");
		}
	})

	function setGestiones(){
		setGestionesTotal('#gestionOpciones','Gestionar');
	}

	function setGestionesEnvioValorados(){
		setGestionesTotal('#gestionOpcionesValorados','Gestionar');
	}

	function setGestionesEnvioGTP(){
		setGestionesTotal('#gestionOpcionesGTP','Gestionar');
	}

	function setGestionesTotal(opciones,btnTexto){
		$('#formGestionar select[name="gestion"]').html($(opciones).html());
		$('#btnGestion').text(btnTexto)
	}

	$(document).on('change','#formGestionar select[name="sector"]', function(){
		changeGiro($(this).val(),null);
	});

	function changeGiro(sector,giro){
		var sector = sector;
		$.ajax({
        url: "{{ route('getGirosBySector') }}",
        type: 'GET',
        data: {
					sector: sector
				},
				beforeSend: function() {
					$('#formGestionar select[name="giro"]').prop('disabled',true);
				},
        success: function (result) {
						var $cboGiro = $("#formGestionar select[name='giro']");
						$cboGiro.find('option').remove().end().append('<option value="">Selecciona una opción</option>').val('')
						if (sector != ''){
							$.each(result, function() {
								if (this.CODIGO == giro){
									$cboGiro.append($("<option selected/>").val(this.CODIGO).text(this.DESCRIPCION));
								}else{
									$cboGiro.append($("<option />").val(this.CODIGO).text(this.DESCRIPCION));
								}
							});
						}
				},
				complete: function() {
					$('#formGestionar select[name="giro"]').prop('disabled',false);
				}
		});
	}

	function sololectura(montoSolicitado = true,gestion = true,boton = true,rrll = true,email=true,ubigeo = true){
		$('input.editable,select.editable,textarea.editable').prop("disabled",true)

		$('#formGestionar input[name="rucrrll"]').attr("disabled",rrll);
		$('#formGestionar input[name="rrll"]').attr("disabled",rrll);
		$('#formGestionar input[name="rrllApellidos"]').attr("disabled",rrll);
		$('#formGestionar select[name="rrllEstadoCivil"]').attr("disabled",rrll);

		$('#formGestionar input[name="rucrrll2"]').attr("disabled",rrll);
		$('#formGestionar input[name="rrll2"]').attr("disabled",rrll);
		$('#formGestionar input[name="rrllApellidos2"]').attr("disabled",rrll);
		$('#formGestionar select[name="rrllEstadoCivil2"]').attr("disabled",rrll);

		//excepciones
		$('#formGestionar input[name="email"]').attr("disabled",email);
		$('button.editable').prop('disabled',boton)
		$('#formGestionar input[name="solicitudMonto"]').prop('disabled',montoSolicitado)
		$('#formGestionar select[name="gestion"]').prop('disabled',gestion)
		$('#formGestionar select[name="tienda"]').prop('disabled',false)
	}

	function editable(){
		$('input.editable,select.editable,textarea.editable').prop("disabled",false)

		$('#formGestionar input[name="rucrrll"]').attr("disabled",false);
		$('#formGestionar input[name="rrll"]').attr("disabled",false);
		$('#formGestionar input[name="rrllApellidos"]').attr("disabled",false);
		$('#formGestionar select[name="rrllEstadoCivil"]').attr("disabled",false);

		$('#formGestionar input[name="rucrrll2"]').attr("disabled",false);
		$('#formGestionar input[name="rrll2"]').attr("disabled",false);
		$('#formGestionar input[name="rrllApellidos2"]').attr("disabled",false);
		$('#formGestionar select[name="rrllEstadoCivil2"]').attr("disabled",false);

		esPj = true;

		$('#formGestionar select[name="tipoVia"]').attr("disabled",esPj);
		$('#formGestionar input[name="direcVia"]').attr("readonly",esPj);
		$('#formGestionar input[name="direcNro"]').attr("readonly",esPj);
		$('#formGestionar input[name="direcLote"]').attr("readonly",esPj);
		$('#formGestionar input[name="direcUrb"]').attr("readonly",esPj);
		$('#formGestionar input[name="direcManz"]').attr("readonly",esPj);
		$('#formGestionar input[name="direcInt"]').attr("readonly",esPj);
		$('#formGestionar select[name="departamento"]').attr("disabled",esPj);
		$('#formGestionar select[name="provincia"]').attr("disabled",esPj);
		$('#formGestionar select[name="distrito"]').attr("disabled",esPj);


		$('button.editable').prop('disabled',false)
	}

	function reenvioRiesgos(){
		editable()
		$('#formGestionar select[name="gestion"]').prop("readonly",true);
	}

	function modoGestion(){
		$(".btnArea .btn").hide();
		$(".btnGestion").show();
		$('#gestionGtpArea').addClass("hidden");
		$('#formGestionar').attr('action',"{{ route('cmpEmergencia.bpe.gestionar') }}");
	}

	function botonValorado(gestion){
		if(gestion == 6 || gestion == 7 || gestion == 20 || gestion == 21)
		{
			$("#btnValorado").show();
		}
		else{
			$("#btnValorado").hide();
		}
	}

	function modoGtp(){
		$(".btnArea .btn").hide();
		$('#gestionGtpArea').removeClass("hidden");
		$('#formGestionar').attr('action',"{{ route('cmpEmergencia.bpe.enviarGtp') }}");
	}

	//Validaciones antes del guardado
	$('#formGestionar').submit(function(e){

		console.log($('#formGestionar select[name="gestion"]').prop('disabled'));

		if (!$('#formGestionar select[name="gestion"]').prop('disabled')){
			if (!parseInt($('#formGestionar select[name="gestion"]').val())) {
				Swal.fire(
					'Tienes que llenar la gestión en la pestaña Crédito',
					'Click en el botón!',
					'warning'
				)
				return false;
			}
		}
		

		var valiGen  = validacionGeneral();
		var valiPJ =  validacionesPJ();

		if(valiGen == "" && valiPJ == ""){
			solicitud 		= numeral($('#formGestionar input[name="solicitudMonto"]').val())
			final     		= numeral($('#formGestionar input[name="montoFinal"]').val())
			gestion   		= parseInt($('#formGestionar select[name="gestion"]').val())
			solicitudGracia = $.trim($('#formGestionar select[name="solicitudGracia"]').val())
			solicitudVentas = numeral($('#formGestionar input[name="ventas"]').val())
			montoReactiva1  = numeral($('#formGestionar input[name="montoReactiva1"]').val())
			flgReactiva1    = $.trim($('#formGestionar select[name="flgReactiva1"]').val())
			cuentaTipo      = $.trim($('#formGestionar select[name="cuentaTipo"]').val())
			cuentaNumero    = $.trim($('#formGestionar input[name="cuentaNumero"]').val())
			telefono 		= $.trim($('#formGestionar input[name="telefono"]').val())
			correo 			= $.trim($('#formGestionar input[name="email"]').val())
			giro 			= $.trim($('#formGestionar select[name="giro"]').val())
			sector 			= $.trim($('#formGestionar select[name="sector"]').val())
			canalActual 	= $.trim($('#formGestionar input[name="canalActual"]').val())
			riesgosMonto 	= numeral($('#formGestionar input[name="riesgosMonto"]').val())
			flgTopeMaximo	= numeral($('#formGestionar input[name="flgMontoLimite"]').val())
			topeMaximo		= numeral($('#formGestionar input[name="montoLimite"]').val())
			solicitudTasa 	= $.trim($('#formGestionar select[name="solicitudTasa"]').val())
			tasa 			= $.trim($('#formGestionar input[name="tasa"]').val())
			tipoEnvioGTP 	= $.trim($('#formGestionar select[name="tipoEnvioGTP"]').val())
			tienda		 	= $.trim($('#formGestionar select[name="tienda"]').val())

			if ([5,6,7,20,21].includes(gestion) &&
				(telefono.length  < 8 || correo.length < 5)){
				Swal.fire(
				  'No has ingresado correctamente los datos de contacto: teléfono o email',
				  'Click en el botón!',
				  'warning'
				)
				return false;
			}

			//if ([5,6,7,20,21].includes(gestion) && (solicitudGracia == '')){
			if ([6,7,20,21].includes(gestion) && (solicitudGracia == '')){
				Swal.fire(
				  'No has seleccionado un periodo de gracia + cuotas',
				  'Click en el botón!',
				  'warning'
				)
				return false;
			}

			if ([7,20,21].includes(gestion) && (cuentaTipo == '' || cuentaNumero == '')){
				Swal.fire(
				  'No has ingresado los datos de cuenta',
				  'Click en el botón!',
				  'warning'
				)
				return false;
			}
			if ([20,21].includes(gestion)) {
				if (!parseInt($('#formGestionar select[name="gestion"]').val())) {
					Swal.fire(
						'Tienes que llenar datos de Trading en la pestaña Cross Sell',
						'Click en el botón!',
						'warning'
					)
					return false;
				}
			}

			if (flgTopeMaximo.value() == 1){
				final = numeral(Math.min(final.value(),topeMaximo.value()))
			}else{
				final = numeral(Math.min(final.value()))
			}

			if ([21].includes(gestion) &&
				(solicitudTasa == '' && tasa == '')){
				Swal.fire(
				  'No has seleccionado una tasa',
				  'Click en el botón!',
				  'error'
				)
				return false;
			}

			if ([20].includes(gestion) &&
				(tienda == '')){
				Swal.fire(
					'No has seleccionado una tienda',
					'Click en el botón!',
					'error'
				)
				return false;
			}

			if ([21].includes(gestion) &&
				(tipoEnvioGTP == '')){
				Swal.fire(
					'No has seleccionado tipo de envío',
					'Click en el botón!',
					'error'
				)
				return false;
			}
			//if ([5,6,7,20,21].includes(gestion) &&
			if ([6,7,20,21].includes(gestion) &&
				(solicitud.value() < 1000 || solicitud.value() > final.value()) ){
				Swal.fire(
					'El monto solicitado del cliente debe ser mayor S/. 1,000 y menor a S/. ' + final.format('0,0'),
					'Click en el botón!',
					'error'
				)
				return false;
			}


			if(riesgosMonto.value() > 0 && solicitud.value() > riesgosMonto.value()){
				Swal.fire(
					'El monto solicitado debe ser menor al monto límite establecido por Riesgos',
					'Click en el botón!',
					'error'
				)
				return false;
			}

			var depa = $('#formGestionar select[name="departamento"]').val();
			var prov = $('#formGestionar select[name="provincia"]').val();
			var dist = $('#formGestionar select[name="distrito"]').val();

			if( depa != null && prov != null  && dist != null && depa != "" && prov != ""  && dist != "")
			{
				var ubigeo = depa + prov + dist;
				$('#formGestionar input[name="ubigeo"]').val(ubigeo);
			}

			var direccion = concatenacionDirección();
			$('#formGestionar input[name="direccion"]').val(direccion);
		}
		else{
				Swal.fire(
					valiGen + "\n" + valiPJ,
					'CLick en el botón!',
					'error'
				)
		    return false
		}

			$('#btnGestion').prop('disabled',true);
			// todavia
			e.preventDefault();
			const swalerta = Swal.mixin({
			  customClass: {
			    confirmButton: 'btn btn-success',
			    cancelButton: 'btn btn-danger'
			  },
			  buttonsStyling: false
			});
			swalerta.fire({
			  title: 'Estas seguro de esta acción?',
			  text: "Aun puedes revertir esto!",
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonText: 'Si',
			  cancelButtonText: 'No',
			  reverseButtons: true
			}).then((result) => {
			  if (result.value) {
			    this.submit();
			  }else{
					$('#btnGestion').prop('disabled',false);
				}
			});


	})

	function concatenacionDirección(){
			var tipoVia  = $('#formGestionar select[name="tipoVia"] option:selected').text();
			var direNro  = $('#formGestionar input[name="direcNro"]').val();
			var direcInt = $('#formGestionar input[name="direcInt"]').val();
			var direcVia = $('#formGestionar input[name="direcVia"]').val();
			var direcLote= $('#formGestionar input[name="direcLote"]').val();
			var direcManz= $('#formGestionar input[name="direcManz"]').val();
			var direcUrb = $('#formGestionar input[name="direcUrb"]').val();

			var direccion = tipoVia + " " + direcVia + " " + direNro + " " + direcInt + " " + direcLote + " " + direcManz + " " + direcUrb

			return direccion;
	}

	$('#formGestionar input[name="ventas"]').keyup(function() {
		recalcularMontoReactiva2();
	});

	$('#formGestionar input[name="solicitudMonto"]').keyup(function() {
		actualizarCobertura();
	});

  function actualizarCobertura(){
    montoReactiva1 = numeral($('#formGestionar input[name="montoReactiva1"]').val());
		montoSolicitado = numeral($('#formGestionar input[name="solicitudMonto"]').val());
    coberturaSol = getCoberturaMonto(montoSolicitado.value(),montoReactiva1.value());
    $('#formGestionar input[name="solicitudCobertura"]').val(coberturaSol*100 + '%');
	}

	function recalcularMonto(venta,essalud){

		var ventaMensual = 0
		if (!isNaN(parseFloat(venta))) {ventaMensual = parseFloat(venta)/12};
		if (!isNaN(parseFloat(essalud))) {essalud = parseFloat(essalud)*3} else {essalud = 0};
		max = 0;
		if (venta<645000 && venta>0){
			max = ventaMensual
		}else{
			if(ventaMensual >= essalud){
				max = ventaMensual;
			}else{
				max = essalud;
			}
		}

		if (max <= 0)
			return 0

		cobertura = 0
		if(max <= 29400) {
			cobertura = 0.98
		}else if(max <= 285000){
			cobertura = 0.95
		}else if(max <= 4500000){
			cobertura = 0.90
		}else {
			cobertura = 0.80
		}

		calc = max/cobertura
		itf = Math.round((calc*0.00005)/0.05)*0.05
		calc = Math.floor(calc-itf)-1;

		if (calc>10000000)
			calc = 10000000

		return calc
	}

	function recalcularMontoReactiva2(montoReactiva1){

		var montoReactiva1 = numeral($('#formGestionar input[name="montoReactiva1"]').val()).value();
		var venta = numeral($('#formGestionar input[name="ventas"]').val()).value();

		if ($('#formGestionar select[name="flgReactiva1"]').val() == '0'){
			montoReactiva1 = 0;
		}

		if (venta == 0){
			return 0;
		}

		var ventaMensual = 0;
		if (!isNaN(parseFloat(venta))) {
			ventaMensual = parseFloat(venta)/12
		}

		nVenta = ventaMensual *3

		var cobertura = 0
		if(nVenta <= 88200) {
			cobertura = 0.98
		}else if(nVenta <= 712500){
			cobertura = 0.95
		}else if(nVenta <= 6750000){
			cobertura = 0.90
		}else{
			cobertura = 0.8
		}

		prestamo1 = (ventaMensual * 3 - getCoberturaMontoReactiva1(montoReactiva1))/cobertura;
		if (prestamo1 >= 10000000) {
			prestamo1 = 10000000;
		}

		//prestamoMaximo = prestamo1 - montoReactiva1
		itf = Math.round((prestamo1*0.00005)/0.05)*0.05
		prestamo1 = Math.floor(prestamo1-itf)-1

		if (prestamo1 <= 0){
			prestamo1 = 0
		}
		$('#formGestionar input[name="montoFinal"]').val(numeral(prestamo1).format('0,0'));

		return [prestamo1,cobertura]

	}

	function montoFinal(montoCalculado,montoRiesgos){
		if (isNaN(parseFloat(montoCalculado))) {montoCalculado = 0} else {venta = parseFloat(montoCalculado)};
		if (isNaN(parseFloat(montoRiesgos))) {montoRiesgos = 0} else {montoRiesgos = parseFloat(montoRiesgos)};

		min = 0;
		if (montoRiesgos == 0){
			min = montoCalculado;
		}else if(montoRiesgos >= montoCalculado){
			min = montoCalculado;
		}else{
			min = montoRiesgos;
		}

		$('#formGestionar input[name="montoFinal"]').val(numeral(min).format('0,0.00'))
		return min;
	}

	function getCoberturaMonto(montoSolicitud,montoReactiva1){

		cobertura = 0.0;

		monto = montoSolicitud + Math.round((montoSolicitud*0.00005)/0.05)*0.05 + montoReactiva1

		if(monto<=90000){
			cobertura = 0.98;
		}else if(monto<=750000){
			cobertura = 0.95;
		}else if(monto<=7500000){
			cobertura = 0.9;
		}else{
			cobertura = 0.8;
		}
		return cobertura;
	}

	function getCoberturaMontoReactiva1(monto){
		cobertura = 0;

		if (monto <= 0)
			return 0;

		if (monto <= 30000){
			cobertura = 0.98
		}else if (monto <= 300000){
			cobertura = 0.95
		}else if (monto <= 5000000){
			cobertura = 0.90
		}else if (monto <= 10000000){
			cobertura = 0.80
		}else{
			return 0
		}

		return monto*cobertura

	}
	function dtLeads(id_zona=null,id_centro=null,id_tienda=null,gestion_en=null,gestion_r=null,prioridad = null,traderdesembolso = null){

		if ($.fn.dataTable.isDataTable('#dataTableLeads')) {
			$('#dataTableLeads').DataTable().destroy();
		}
		var verdatacuensueld = "{{$rol}}"==90?true:false;
		$('#dataTableLeads').DataTable({
			processing: true,
			"bAutoWidth": false,
			rowId: 'staffId',
			// dom: 'Blfrtip',
			serverSide: true,
			language: {"url": "{{ URL::asset('js/Json/Spanish.json') }}"},
			ajax: {
				"type": "GET",
				"url": "{{ route('cmpEmergencia.bpe.lista') }}",
				data: function(data) {
					data.etapa = null;
					data.id_zona = $("#id_zona").val();
					data.id_centro = $("#id_centro").val();
					data.id_tienda = $("#id_tienda").val();
					data.gestion_en = $("#gestion_en").val();
					data.gestion_r = $("#gestion_r").val();
					data.prioridad = $("#prioridad").val();
					data.traderdesembolso = $("#traderdesembolso").val();
					data.clientescanal = $("#clientescanal").val();
				}
			},
			"aLengthMenu": [
				[25, 50, -1],
				[25, 50, "Todo"]
			],
			"iDisplayLength": 25,
			"order": [
				[4, "desc"]
            ],
			columnDefs: [
				{
					targets: 0,
					data: 'WCL.PRIORIDAD_BPE',
		            name: 'WCL.PRIORIDAD_BPE',
		            sortable: true,
					searchable: false,
					visible: !verdatacuensueld,
					render: function(data, type, row) {
						return row.PRIORIDAD_BPE
					}
				},

				{
					targets: 1,
					data: 'WCL.NUM_RUC',
		            name: 'WCL.NUM_RUC',
		            sortable: false,
					render: function(data, type, row) {
						return row.NUM_RUC + '<br>' + row.NOMBRE;
					}
				},
				{
					targets: 2,
					data: null,
					searchable: false,
					sortable: false,
					visible: !verdatacuensueld,
					render: function(data, type, row) {
						return row.FLUJO;
					}
				},
				{
					targets: 3,
					data: null,
          			searchable: false,
          			sortable: false,
					visible: !verdatacuensueld,
					render: function(data, type, row) {
			            return 'Ventas Anuales: S/ ' + (numeral(row.SOLICITUD_VOLUMEN_VENTA).format('0,0')||'-')
			              +'<br> ESSALUD: S/ ' + (numeral(row.SOLICITUD_ESSALUD).format('0,0')||'-');
					}
				},
				{
					targets: 4,
					data: 'WCL.FLAG_REPORTE_TRIBUTARIO',
					name: 'WCL.FLAG_REPORTE_TRIBUTARIO',
					searchable: false,
					visible: !verdatacuensueld,
					render: function(data, type, row) {
						if(row.FLAG == '1'){
							return 'Backoffice';
						}else{
							return '';
						}

					}
				},
				{
					targets: 5,
					data: null,
		            searchable: false,
		            sortable: false,
					visible: !verdatacuensueld,
					render: function(data, type, row) {
                        return 'Monto: S/ ' + (numeral(row.SOLICITUD_OFERTA).format('0,0')||'-')
                        +'<br> Gracia/Plazo: ' + (row.SOLICITUD_GRACIA||'-') + ' / ' + (row.SOLICITUD_PLAZO||'-');
					}
				},
				{
					targets: 6,
					data: null,
		            searchable: false,
		            sortable: false,
					visible: !verdatacuensueld,
					render: function(data, type, row) {
						return row.CAMPANIA;
					}
				},
				{
					targets: 7,
					data: 'WCL.ETAPA_ID',
        			searchable: false,
					sortable: true,
					visible: !verdatacuensueld,
					render: function(data, type, row) {
						//html = row.ETAPA_NOMBRE
						html = ''
						if (row.RIESGOS_GESTION == 9){
							html += ' <span class="label label-success">Aprobado</span>'
						}
						if (row.RIESGOS_GESTION == 10){
							html += ' <span class="label label-warning">Observado</span>'
						}
						if(row.RIESGOS_GESTION == 14){
							html += ' <span class="label label-danger">Rechazado</span>'
						}
						if(row.RIESGOS_GESTION == 19){
							html += ' <span class="label label-default">Pendiente</span>'
						}
						return html;

					}
				},
				{
					targets: 8,
					data: 'WCL.CONTACTO_RESULTADO',
			        searchable: false,
			        sortable: true,
					visible: !verdatacuensueld,
					render: function(data, type, row) {
						html = '';
						if (row.CONTACTO_RESULTADO == null){
							html = '<button  onclick="validaRuc(' +  row.NUM_RUC + ')" class="btnGestionar btn btn-sm btn-warning" ruc="' +  row.NUM_RUC + '">Pendiente</button>';
						}else{
							html = '<button onclick="validaRuc(' +  row.NUM_RUC + ')" class="btnGestionar btn btn-sm btn-primary" ruc="' +  row.NUM_RUC + '">' + row.GESTION_NOMBRE +'</button>';
						}
						return html;
					}
				},
				{
					targets: 9,
					data: null,
			        searchable: false,
			        sortable: false,
					visible: verdatacuensueld,
					render: function(data, type, row) {
						rrll1="";
						rrll2="";
						if (row.RRLL_DOCUMENTO || row.RRLL_NOMBRES) {
							rrll1 = row.RRLL_DOCUMENTO+"<br>"+row.RRLL_APELLIDOS+row.RRLL_NOMBRES+"<br><br>";
						}
						if (row.RRLL_DOCUMENTO_2 || row.RRLL_NOMBRES_2) {
							rrll2 = row.RRLL_DOCUMENTO_2+"<br>"+row.RRLL_APELLIDOS_2+row.RRLL_NOMBRES_2;
						}
						return rrll1+rrll2 ;
					}
				},
				{
					targets: 10,
					data: null,
		            searchable: false,
		            sortable: false,
					visible: verdatacuensueld,
					render: function(data, type, row) {
						return "<span>"+(row.TELEFONO1 || '')+"</span><br><span>"+(row.CORREO || '')+"</span>";
					}
				},
				{
					targets: 11,
					data: null,
		            searchable: false,
		            sortable: false,
					visible: verdatacuensueld,
					render: function(data, type, row) {
						if (row.MONTO_PAGO_ESSALUD) {
							return "<span>S/."+row.MONTO_PAGO_ESSALUD+"</span>";
						}
						return '';
					}
				},
				{
					targets: 12,
					data: null,
		            searchable: false,
		            sortable: false,
					visible: verdatacuensueld,
					render: function(data, type, row) {
						return (row.NRO_TRABAJADORES_QUINTA_CATEGORIA || '');
					}
				},
				{
					targets: 13,
					data: null,
		            searchable: false,
		            sortable: false,
					visible: verdatacuensueld,
					render: function(data, type, row) {
						return (row.SUELDO_PROMEDIO || '');						
					}
				},
				{
					targets: 14,
					data: null,
		            searchable: false,
		            sortable: false,
					visible: verdatacuensueld,
					render: function(data, type, row) {
						return (row.NOMBRE_EJECUTIVO || '');
					}
				},
				{
					targets: 15,
					data: null,
		            searchable: false,
		            sortable: false,
					visible: verdatacuensueld,
					render: function(data, type, row) {
						return (row.CENTRO || '');
					}
				},
				{
					targets: 16,
					data: null,
		            searchable: false,
		            sortable: false,
					visible: verdatacuensueld,
					render: function(data, type, row) {
						return (row.FECHA_GESTION || '');
					}
				},
				{
					targets: 17,
					data: 'WCL.NOMBRE',
					name: 'WCL.NOMBRE',
		            searchable: true,
		            sortable: false,
					visible: false,
					render: function(data, type, row) {
						return (row.FECHA_GESTION || '');
					}
				}
			]
		});
	}

	function filterFloat(evt,input){
		// Backspace = 8, Enter = 13, ‘0′ = 48, ‘9′ = 57, ‘.’ = 46, ‘-’ = 43
		var key = window.Event ? evt.which : evt.keyCode;
		var chark = String.fromCharCode(key);
		var tempValue = input.value+chark;

		if ((key >= 48 && key <= 57) || key == 13 || key == 8 || key == 13 || key == 0 || key == 46)
			return true;
		else
			return false;


		if(key >= 48 && key <= 57){
			if(filter(tempValue)=== false){
				return false;
			}else{
				return true;
			}
		}else{
			if(key == 8 || key == 13 || key == 0) {
				return true;
			}else if(key == 46){
					if(filter(tempValue)=== false){
						return false;
					}else{
						return true;
					}
			}else{
				return false;
			}
		}
	}

	function filter(__val__){
		var preg = /^([0-9]+\.?[0-9]{0,2})$/;
		if(preg.test(__val__) === true){
			return true;
		}else{
		return false;
		}
	}

	$('.input-number').on('input', function () {
		this.value = this.value.replace(/[^0-9]/g,'');
	});

	function validacionesPJ(){
		mensaje = "";
		gestion = parseInt($('#formGestionar select[name="gestion"]').val());
		if ([6,7].includes(gestion)){
			if ($('#formGestionar input[name="numruc"]').val().substr(0, 2) == '20'){
				if($('#formGestionar select[name="tipoSociedad"]').val() == ""){
					mensaje = mensaje + " " + 'Tipo Sociedad,';
				}
				if($('#formGestionar input[name="rucrrll"]').val() == ""){
					mensaje = mensaje + " " + 'DNI RRLL,';
				}
				if($('#formGestionar input[name="rrll"]').val() == ""){
					mensaje = mensaje + " " + 'Nombre RRLL,';
				}
				if($('#formGestionar input[name="rrllApellidos"]').val() == ""){
					mensaje = mensaje + " " + 'Apellido RRLL,';
				}
				if($('#formGestionar select[name="rrllEstadoCivil"]').val() == ""){
					mensaje = mensaje + " " + 'Estado Civil RRLL';
				}
			}
		}


		if( mensaje != ""){
			mensaje = "El campo" + " " + mensaje + " " + "es obligatorio.";
		}

		return mensaje;
	};

	function validacionGeneral(){
		mensaje = "";
		gestion = parseInt($('#formGestionar select[name="gestion"]').val());
		if ([6,7,20,21].includes(gestion))
		{
			if($('#formGestionar select[name="giro"]').val() == ""){
				mensaje = mensaje + " " + 'Giro,';
			}
			if($('#formGestionar select[name="sector"]').val() == ""){
				mensaje = mensaje + " " + 'Sector,';
			}
			if($('#formGestionar select[name="departamento"]').val() == ""){
				mensaje = mensaje + " " + 'Departamento';
			}
			if($('#formGestionar select[name="provincia"]').val() == ""){
				mensaje = mensaje + " " + 'Provincia';
			}
			if($('#formGestionar select[name="distrito"]').val() == ""){
				mensaje = mensaje + " " + 'Distrito';
			}
			if( mensaje != ""){
				mensaje = "El campo" + " " + mensaje + " " + "es obligatorio.";
			}
		}
		return mensaje;
	};

	function botonespdf(){
		var ruc = $('#formGestionar input[name="numruc"]').val();
		return $(".botonesdescargas").html(
			"<a id='botondescarga1' target='_blank' href='{{route('cmpEmergencia.bpe.acuerdocreditopdf')}}?ruc="+ruc+"'>boton1</a>"+
			"<a id='botondescarga2' target='_blank' href='{{route('cmpEmergencia.bpe.pagarepdf')}}?ruc="+ruc+"'>boton2</a>"+
			"<a id='botondescarga3' target='_blank' href='{{route('cmpEmergencia.bpe.llenadopagarepdf')}}?ruc="+ruc+"'>boton3</a>"+
			"<a id='botondescarga4' target='_blank' href='{{route('cmpEmergencia.bpe.cartapresentacionpdf')}}?ruc="+ruc+"'>boton4</a>");
	};

	function clickbotonespdf(){
		$("#botondescarga2").get(0).click();
		$("#botondescarga3").get(0).click();
		$("#botondescarga4").get(0).click();
		$("#botondescarga1").get(0).click();
	};

	function generarValorado(){
		botonespdf();
		setTimeout(function() {
			clickbotonespdf();
		}, 150);
	};

	function validaRuc(ruc){
		var cadena = String(ruc)
		if( cadena.substr(0, 2) == "10"){
			$('#rrllTab').html("Relacionados");
			$('#leg1').html("Titular");
			$('#leg2').html("Conyúge");
		}
		else{
			$('#rrllTab').html("RRLL");
			$('#leg1').html("Representante Legal 1");
			$('#leg2').html("Representante Legal 2");
		}
	};

	$(document).on('change','#formGestionar select[name="departamento"]', function(){
		changeDepartamento($(this).val(),null);
	});

	$(document).on('change','#formGestionar select[name="provincia"]', function(){
		var depa = $('#formGestionar select[name="departamento"]').val();
		changeProvincia(depa,$(this).val(),null);
	});
	$(document).on('change','#formGestionar select[name="distrito"]', function(){
		var prov = $('#formGestionar select[name="provincia"]').val();
		var depa = $('#formGestionar select[name="departamento"]').val();
		getTiendas($(this).val(),prov,depa,null);
	});

	function changeDepartamento(codigoDepartamento,codigoProvincia){
		var departamento = codigoDepartamento;
		var provincia = codigoProvincia;

		$.ajax({
        url: "{{ route('getProvinciaByDepartamento') }}",
        type: 'GET',
        data: {
					departamento: departamento
				},
				beforeSend: function() {
					//$('#formGestionar select[name="provincia"]').prop('disabled',true);
					//$('#formGestionar select[name="distrito"]').prop('disabled',true);
				},
        success: function (result) {
					var $cbo= $("#formGestionar select[name='provincia']");
					$cbo.find('option').remove().end().append('<option value="">Selecciona una opción</option>').val('')
					$.each(result, function() {
						if (this.CodigoProvincia == provincia){
							$cbo.append($("<option selected/>").val(this.CodigoProvincia).text(this.NombreProvincia));
						}else{
							$cbo.append($("<option />").val(this.CodigoProvincia).text(this.NombreProvincia));
						}
					});
				},
				complete: function() {
				}
		});
	};

	function changeProvincia(codigoDepartamento,codigoProvincia,codigoDistrito){
		var departamento = codigoDepartamento;
		var provincia    = codigoProvincia;
		var distrito     = codigoDistrito;
		$.ajax({
        url: "{{ route('getDistritoByProvincia') }}",
        type: 'GET',
        data: {
					provincia:  provincia,
					departamento: departamento
				},
				beforeSend: function() {
					//$('#formGestionar select[name="distrito"]').prop('disabled',true);
				},
        success: function (result) {
					var $cbo= $("#formGestionar select[name='distrito']");
					$cbo.find('option').remove().end().append('<option value="">Selecciona una opción</option>').val('')
					$.each(result, function() {
						if (this.CodigoDistrito == distrito){
							$cbo.append($("<option selected/>").val(this.CodigoDistrito).text(this.NombreDistrito));
						}else{
							$cbo.append($("<option />").val(this.CodigoDistrito).text(this.NombreDistrito));
						}
					});
				},
				complete: function() {
				}
		});
	};

	function getTiendas(distrito,provincia,departamento,tienda = null){

		var $cbo= $("#formGestionar select[name='tienda']");

		if (ubigeo == null){
			$cbo.find('option').remove().end().append('<option value="">Seleccionar</option>').val('')
		}else{
			$.ajax({
        url: "{{ route('getTiendasByUbigeo') }}",
        type: 'GET',
        data: {
					distrito:  distrito,
					provincia:  provincia,
					departamento: departamento
				},
				beforeSend: function() {
					//$cbo.prop('disabled',true);
				},
        success: function (result) {
					$cbo.find('option').remove().end().append('<option value="">Seleccionar</option>').val('')
					$.each(result, function() {
						if (this.CODIGO == tienda){
							$cbo.append($("<option selected/>").val(this.CODIGO).text(this.DESCRIPCION_COMPLETA));
						}else{
							$cbo.append($("<option />").val(this.CODIGO).text(this.DESCRIPCION_COMPLETA));
						}
					});
				},
				complete: function() {
				}
			});
		}
	}

	function changeClass(){
		$("#liEmpresa").removeClass("active")
		$("#empresa").removeClass("tab-pane")

		$("#liEmpresa").addClass("active")
		$("#empresa").addClass("tab-pane active")

		$("#liCredito").removeClass("active")
		$("#liRRLL").removeClass("active")
		$("#licrossell").removeClass("active")
		$("#credito").removeClass("active")
		$("#rrll").removeClass("active")
		$("#crossell").removeClass("active")

		$("#credito").addClass("tab-pane")
		$("#rrll").addClass("tab-pane")

	};

</script>
@stop
