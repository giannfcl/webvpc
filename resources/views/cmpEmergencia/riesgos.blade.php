@extends('Layouts.layout')

@section('js-libs')
<link href="{{ URL::asset('css/datatables.min.css') }}" rel="stylesheet" type="text/css">

<script type="text/javascript" src="{{ URL::asset('js/datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/numeral/numeral.min.js') }}"></script>

@stop

@section('content')
<div class="x_panel">
	
	<div id="filtros" style="padding-bottom: 15px" class="row">
		<form id="formGestionar" method="POST" class="form-horizontal form-label-left" enctype="multipart/form-data" action="{{route('cmpEmergencia.riesgos.bpe.gestionar')}}">
			<div class="col-sm-6">
				<div class="form-group">
					<label class="control-label col-sm-3 col-xs-12">Estado:</label>
					<div class="col-sm-9 col-xs-12" style="padding-left:0px">
						<select id="cboEstado" class="form-control col-sm-4">
							<option value="">Todos los estados</option>
							<option value="19">Pendiente</option>
							<option value="9">Conforme</option>
							<option value="10">Observado</option>
							<option value="14">Rechazado</option>
						</select>
					</div>
				</div>
			</div>
		</form>
	</div>

	<table class='table table-striped table-bordered jambo_table' id="dataTableLeads">
			<thead>
				<tr>
					<th scope="col" style="font-size: 12.5px;">Centro</th>
					<th scope="col" style="font-size: 12.5px;">CU</th>
					<th scope="col" style="font-size: 12.5px;">Cliente</th>
					<th scope="col" style="font-size: 12.5px;">Representante</th>
					<th scope="col" style="font-size: 12.5px;">Distrito</th>
					<th scope="col" style="font-size: 12.5px;">Ejecutivo</th>
					<th scope="col" style="font-size: 12.5px;">Campaña</th>
					<th scope="col" style="font-size: 12.5px;">Gestionar</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
	</table>
</div>

@stop

<div class="modal fade" tabindex="-1" role="dialog" id="modalGestion">
    <div class="modal-dialog" role="document" style="width: 720px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Nuevo</h4>
            </div>
            <div class="modal-body">
				<form id="formGestionar" method="POST" class="form-horizontal form-label-left" enctype="multipart/form-data" action="{{route('cmpEmergencia.riesgos.bpe.gestionar')}}">
					<div class="row">
						<div class="col-sm-6">
							<div class="row">
								<div class="form-group">
									<label class="control-label col-sm-3 col-xs-12">RUC:</label>
									<div class="col-sm-9 col-xs-12" style="padding-left:0px">
										<input autocomplete="off" class="form-control"  type="text" name="numruc" readonly>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-3 col-xs-12">Cliente:</label>
									<div class="col-sm-9 col-xs-12" style="padding-left:0px">
										<input autocomplete="off" class="form-control"  type="text" name="cliente" readonly>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-3 col-xs-12">Tipo Cliente:</label>
									<div class="col-sm-9 col-xs-12" style="padding-left:0px">
										<input autocomplete="off" class="form-control"  type="text" name="tipoCliente" readonly>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-3 col-xs-12">Giro:</label>
									<div class="col-sm-9 col-xs-12" style="padding-left:0px">
										<input autocomplete="off" class="form-control"  type="text" name="girodesc" readonly>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-3 col-xs-12">Venta Anual:</label>
									<div class="col-sm-9 col-xs-12" style="padding-left:0px">
										<input autocomplete="off" class="form-control editable"  type="text" name="ventas" readonly>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-3 col-xs-12">Monto Reactiva1:</label>
									<div class="col-sm-9 col-xs-12" style="padding-left:0px">
										<input autocomplete="off" class="form-control"  type="text" name="montoReactiva1" readonly>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-3 col-xs-12">Banco Reactiva1:</label>
									<div class="col-sm-9 col-xs-12" style="padding-left:0px">
										<select class="form-control editable" name="bancoReactiva1" disabled>
											<option value="">Ninguno</option>
											@foreach ($bancos as $key => $banco)
												<option value="{{ $banco->CODIGO }}">{{ $banco->DESCRIPCION }}</option>
											@endforeach
										</select>
									</div>
								</div>
								
							</div>
						</div>
						<div class="col-sm-6">
							<div class="row">
								<div class="form-group">
									<label class="control-label col-sm-3 col-xs-12">Tipo Proceso:</label>
									<div class="col-sm-9 col-xs-12" style="padding-left:0px">
										<input autocomplete="off" class="form-control"  type="text" name="tipoProceso" readonly>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-3 col-xs-12">Monto Solicitado:</label>
									<div class="col-sm-9 col-xs-12" style="padding-left:0px">
										<input autocomplete="off" class="form-control"  type="text" name="solicitudMonto" readonly onkeypress="return filterFloat(event,this);" maxlength="11">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-3 col-xs-12">Cobertura:</label>
									<div class="col-sm-9 col-xs-12" style="padding-left:0px">
										<input autocomplete="off" class="form-control"  type="text" name="solicitudCobertura" readonly>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-3 col-xs-12">Plazo:</label>
									<div class="col-sm-9 col-xs-12" style="padding-left:0px">
										<input autocomplete="off" class="form-control"  type="text" name="solicitudPlazo" readonly>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-3 col-xs-12">Gracia:</label>
									<div class="col-sm-9 col-xs-12" style="padding-left:0px">
										<input autocomplete="off" class="form-control"  type="text" name="solicitudGracia" readonly>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-3 col-xs-12">Monto Aprobado:</label>
									<div class="col-sm-9 col-xs-12" style="padding-left:0px">
										<input autocomplete="off" class="form-control"  type="text" name="mtolimite" >
									</div>
								</div>
								{{-- 
								<div class="form-group">
									<label class="control-label col-sm-3 col-xs-12">Comentario:</label>
									<div class="col-sm-9 col-xs-12" style="padding-left:0px">
										<textarea class="form-control" rows="2" name="comentario"></textarea>
									</div>
								</div>
								--}}
								<div class="form-group">
									<label class="control-label col-sm-3 col-xs-12">Gestion:</label>
									<div class="col-sm-9 col-xs-12" style="padding-left:0px">
										<select class="form-control" name="gestion" required>
											<option value="">Selecciona una opción</option>
											@foreach ($gestiones as $key => $gestion)
												@if($gestion->GESTION_ID != 19)
												<option value="{{ $gestion->GESTION_ID }}">{{ $gestion->GESTION_NOMBRE }}</option>
												@endif
												
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group motivoArea">
									<label class="control-label col-sm-3 col-xs-12">Motivo:</label>
									<div class="col-sm-9 col-xs-12" style="padding-left:0px">
										<select class="form-control" name="subgestion" >
											<option value="">Selecciona una opción</option>
											@foreach ($subgestiones as $key => $subgestion)
												<option value="{{ $subgestion->GESTION_SUB_ID }}">{{ $subgestion->GESTION_SUB_NOMBRE }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-3 col-xs-12">Comentario:</label>
									<div class="col-sm-9 col-xs-12" style="padding-left:0px">
										<textarea class="form-control editable" rows="2" name="comentario"></textarea>
									</div>
								</div>
							</div>
						</div>
					</div>
                    
					
                    <center>
                        <button class="btn btn-success btnEnviar" type="submit">Gestionar</button>
                    </center>
                </form>
            </div>
		</div>
    </div>
</div>

<div class="modal fade" id="cargando" tabindex="-1" role="dialog">
	<div class="modal-dialog loader" role="document">
		<button type="button" class="close cerrarcargando" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" hidden>&times;</span></button>
	</div>
</div>

<style>
	.loader {
		margin-top:10%;
		border: 16px solid #f3f3f3;
		border-radius: 50%;
		border-top: 16px solid #3498db;
		width: 120px !important;
		height: 120px !important;
		-webkit-animation: spin 2s linear infinite; /* Safari */
		animation: spin 2s linear infinite;
	}

	/* Safari */
	@-webkit-keyframes spin {
		0% { -webkit-transform: rotate(0deg); }
		100% { -webkit-transform: rotate(360deg); }
	}

	@keyframes spin {
		0% { transform: rotate(0deg); }
		100% { transform: rotate(360deg); }
	}
</style>

@section('js-scripts')
<script>

	$(document).ready(function(){
		dtLeads();
	});

	$('#cboEstado').on("change", function(){
		console.log($(this).val())
    	dtLeads($(this).val())
  	});

	$(document).on("click",".btnGestionar",function(){
		$("#modalGestion").modal();
		//limpiar form/loading animation
		var ruc = $(this).attr('ruc')
		$.ajax({
                url: "{{ route('cmpEmergencia.riesgos.bpe.buscar') }}",
                type: 'GET',
                data: {
					ruc: ruc
				},
				beforeSend: function() {
					$("#cargando").modal();
				},
                success: function (result) {
					$('#formGestionar input[name="numruc"]').val(result['NUM_RUC']);
					$('#formGestionar input[name="cliente"]').val(result['NOMBRE']);
					$('#formGestionar input[name="tipoCliente"]').val(result['TIPO_CLIENTE']);
					$('#formGestionar input[name="telefono"]').val(result['TELEFONO1']);
					$('#formGestionar input[name="email"]').val(result['CORREO']);
					//$('#formGestionar input[name="direccion"]').val(result['DIRECCION']);
					$('#formGestionar input[name="solicitudMonto"]').val(numeral(result['SOLICITUD_OFERTA']).format('0,0'));
					$('#formGestionar input[name="montoReactiva1"]').val(numeral(result['MONTO_PRESTAMO_REACTIVA_1']).format('0,0'));
					$('#formGestionar input[name="solicitudPlazo"]').val(result['SOLICITUD_PLAZO']);
					$('#formGestionar input[name="solicitudGracia"]').val(result['SOLICITUD_GRACIA']);
					$('#formGestionar input[name="mtolimite"]').val(numeral(result['RIESGOS_MONTO']).format('0,0'));
					$('#formGestionar input[name="segmento"]').val(result['BANCA']);
					$('#formGestionar select[name="gestion"]').val(result['RIESGOS_GESTION']);
					$('#formGestionar select[name="subgestion"]').val(result['RIESGOS_GESTION_SUB']);
					$('#formGestionar input[name="ventas"]').val(numeral(result['SOLICITUD_VOLUMEN_VENTA']).format('0,0'));
					$('#formGestionar input[name="essalud"]').val(numeral(result['SOLICITUD_ESSALUD']).format('0,0'));
					$('#formGestionar input[name="solicitudCobertura"]').val(getCoberturaMonto(
					numeral(result['SOLICITUD_OFERTA']).value() + numeral(result['MONTO_PRESTAMO_REACTIVA_1']).value() ));
					$('#formGestionar textarea[name="comentario"]').val(result['RIESGOS_COMENTARIO']);
					$('#formGestionar select[name="bancoReactiva1"]').val(result['BANCO_REACTIVA_1']);
					$('#formGestionar input[name="girodesc"]').val(result['GIRO_DESCRIPCION']);
					$('#formGestionar input[name="tipoProceso"]').val(result['TIPO_PROCESO']);

					cargarMotivos();

					// Si ya esta aprobado, ya no es editable
					if (result['RIESGOS_GESTION'] == 9){
						$('#formGestionar .btnEnviar').prop('disabled',true)
					}else{
						$('#formGestionar .btnEnviar').prop('disabled',false)
					}
					
                },
                error: function (xhr, status, text) {
                    e.preventDefault();
                    Swal.fire('Hubo un error al registrar el dato de contacto, inténtelo mas tarde');
                },
				complete: function() {
					// $("#cargando").hide();
					$(".cerrarcargando").click();
				}
            });
	});

	$('#formGestionar select[name="gestion"]').change(function(){
		cargarMotivos()
	});

	function getCoberturaMonto(monto){
		cobertura = 0.0;

		if(monto<=90000){
			cobertura = 0.98;
		}else if(monto<=750000){
			cobertura = 0.95;
		}else if(monto<=7500000){
			cobertura = 0.9;
		}else{
			cobertura = 0.8;
		}
		return cobertura;
	}

	function cargarMotivos(){
		if($('#formGestionar select[name="gestion"]').val() == 10){
			$('.motivoArea').show()
		}else{
			$('.motivoArea').hide()
		}
	}
	

	$('#formGestionar').submit(function(e){
		if ($('#formGestionar select[name="gestion"]').val() == 10 
			&& $('#formGestionar select[name="gestion"]').val() == ""){
				Swal.fire('No ha seleccionado un motivo');
				return false;
		}
	});

	function dtLeads(estado = null){
		if ($.fn.dataTable.isDataTable('#dataTableLeads')) {
			$('#dataTableLeads').DataTable().destroy();
		}
		console.log('dt: ' + estado)
		dtable = $('#dataTableLeads').DataTable({
			processing: true,
			"bAutoWidth": false,
			rowId: 'staffId',
			// dom: 'Blfrtip',
			serverSide: true,
			language: {"url": "{{ URL::asset('js/Json/Spanish.json') }}"},
			ajax: {
				"type": "GET",
				"url": "{{ route('cmpEmergencia.riesgos.bpe.lista') }}",
				"data": function ( d ) {
        			d.estado = estado;
    			}
			},
			"aLengthMenu": [[25, 50, -1],[25, 50, "Todo"]],
			"iDisplayLength": 25,
			"order": [[4, "desc"]],
			columnDefs: [
				{
					targets: 0,
					data: 'WU.ID_CENTRO',
                    name: 'WU.ID_CENTRO',
					sortable: false,
					searchable: false,
					render: function(data, type, row) {
						return (row.ID_CENTRO||'');
					}
				},

				{
					targets: 1,
                    sortable: false,
					searchable: false,
					render: function(data, type, row) {
						return '-';
					}
				},
				{
					targets: 2,
					data: 'WCL.NUM_RUC',
                    name: 'WCL.NUM_RUC',
					searchable: true,
					sortable: false,
					render: function(data, type, row) {
						return row.NUM_RUC + '<br>'
						+ row.NOMBRE;
					}
				},
				{
					targets: 3,
					data: null,
                    searchable: false,
                    sortable: false,
					render: function(data, type, row) {
                        return (row.RRLL_DOCUMENTO||'-')
                        +'<br>' + (row.RRLL_NOMBRE||'-');
					}
				},
				{
					targets: 4,
					data: null,
                    searchable: false,
                    sortable: false,
					render: function(data, type, row) {
						return '';
					}
				},
				{
					targets: 5,
					data: null,
                    searchable: false,
                    sortable: false,
					render: function(data, type, row) {
                        return row.NOMBRE_EJECUTIVO;
					}
				},
				{
					targets: 6,
					data: null,
                    searchable: false,
                    sortable: false,
					render: function(data, type, row) {
						return row.CAMPANIA;
					}
				},
				{
					targets: 7,
					data: 'WCL.RIESGOS_GESTION',
                    searchable: false,
                    sortable: true,
					render: function(data, type, row) {
						html = '';
						if (row.RIESGOS_GESTION == 19 || row.RIESGOS_GESTION == null){
							html = '<button class="btnGestionar btn btn-sm btn-warning" ruc="' +  row.NUM_RUC + '">Pendiente</button>';
						}else{
							html = '<button class="btnGestionar btn btn-sm btn-primary" ruc="' +  row.NUM_RUC + '">' + row.GESTION_NOMBRE +'</button>';
						}

						return html;
					}
				}
			],
		});

		return dtable
	}
</script>
@stop