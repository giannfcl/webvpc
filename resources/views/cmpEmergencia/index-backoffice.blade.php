@extends('Layouts.layout')

@section('js-libs')
<link href="{{ URL::asset('css/datatables.min.css') }}" rel="stylesheet" type="text/css">

<script type="text/javascript" src="{{ URL::asset('js/datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/numeral/numeral.min.js') }}"></script>

<style type="text/css">
	fieldset.scheduler-border {
		border: 1px groove #ddd !important;
		padding: 0 1.4em 1.4em 1.4em !important;
		margin: 0 0 1.5em 0 !important;
		-webkit-box-shadow:  0px 0px 0px 0px #000;
				box-shadow:  0px 0px 0px 0px #000;
	}

	legend.scheduler-border {
		font-size: 1.2em !important;
		font-weight: bold !important;
		text-align: left !important;
		width:auto;
		padding:0 10px;
		border-bottom:none;
	}

input[type=checkbox]
{
  /* Doble-tamaño Checkboxes */
  -ms-transform: scale(2); /* IE */
  -moz-transform: scale(2); /* FF */
  -webkit-transform: scale(2); /* Safari y Chrome */
  -o-transform: scale(2); /* Opera */
  padding: 10px;
}

/* Tal vez desee envolver un espacio alrededor de su texto de casilla de verificación */
.checkboxtexto
{
  /* Checkbox texto */
  font-size: 110%;
  display: inline;
}
</style>
@stop

@section('content')

<div class="x_panel">
	<table class='table table-striped table-bordered jambo_table' id="dataTableLeads">
			<thead>
				<tr>
					<th scope="col" style="font-size: 12.5px;">Tope</th>
					<th scope="col" style="font-size: 12.5px;">Empresa</th>
					<th scope="col" style="font-size: 12.5px;">Flujo</th>
					<th scope="col" style="font-size: 12.5px;">Ventas Anuales S/. / Fuente / Salud</th>
					<th scope="col" style="font-size: 12.5px;">Canal</th>
					<th scope="col" style="font-size: 12.5px;">Monto / Plazo Adicional</th>
					<th scope="col" style="font-size: 12.5px;">Campaña</th>
					<th scope="col" style="font-size: 12.5px;">Riesgos</th>
					<th scope="col" style="font-size: 12.5px;">Gestionar</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
	</table>
</div>

@stop

@section('js-scripts')
<script>

	dtLeads();

	function dtLeads(id_zona=null,id_centro=null,id_tienda=null){
		if ($.fn.dataTable.isDataTable('#dataTableLeads')) {
			$('#dataTableLeads').DataTable().destroy();
		}

		$('#dataTableLeads').DataTable({
			processing: true,
			"bAutoWidth": false,
			rowId: 'staffId',
			// dom: 'Blfrtip',
			serverSide: true,
			language: {"url": "{{ URL::asset('js/Json/Spanish.json') }}"},
			ajax: {
				"type": "GET",
				"url": "{{ route('cmpEmergencia.backoffice.lista') }}",
				data: function(data) {
					data.etapa = null;
					data.id_zona = id_zona;
					data.id_centro = id_centro;
					data.id_tienda = id_tienda;
				}
			},
			"aLengthMenu": [
				[25, 50, -1],
				[25, 50, "Todo"]
			],
			"iDisplayLength": 25,
			"order": [
				[4, "desc"]
            ],
			columnDefs: [
				{
					targets: 0,
					data: 'WCL.PRIORIDAD',
                    name: 'WCL.PRIORIDAD',
                    sortable: true,
					searchable: false,
					render: function(data, type, row) {
						if(row.FLG_TOPE_MAXIMO == '1'){
							return 'Max ' + numeral(row.TOPE_MAXIMO).value()/1000 + 'M';
						}else{
							return '-';
						}
					}
				},

				{
					targets: 1,
					data: 'WCL.NUM_RUC',
                    name: 'WCL.NUM_RUC',
                    sortable: false,
					render: function(data, type, row) {
						return row.NUM_RUC + '<br>' + row.NOMBRE;
					}
				},
				{
					targets: 2,
					data: null,
					searchable: false,
					render: function(data, type, row) {
						return row.FLUJO;
					}
				},
				{
					targets: 3,
					data: null,
                    searchable: false,
                    sortable: false,
					render: function(data, type, row) {
                        return 'Ventas Anuales: S/ ' + (numeral(row.SOLICITUD_VOLUMEN_VENTA).format('0,0')||'-')
                        +'<br> ESSALUD: S/ ' + (numeral(row.SOLICITUD_ESSALUD).format('0,0')||'-');
					}
				},
				{
					targets: 4,
					searchable: false,
                    sortable: false,
					data: 'WCL.CANAL_ACTUAL',
					name: 'WCL.CANAL_ACTUAL',
					render: function(data, type, row) {
						return row.CANAL_ACTUAL;
					}
				},
				{
					targets: 5,
					data: null,
                    searchable: false,
                    sortable: false,
					render: function(data, type, row) {
                        return 'Monto: S/ ' + (numeral(row.SOLICITUD_OFERTA).format('0,0')||'-')
                        +'<br> Gracia/Plazo: ' + (row.SOLICITUD_GRACIA||'-') + ' / ' + (row.SOLICITUD_PLAZO||'-');
					}
				},
				{
					targets: 6,
					data: null,
                    searchable: false,
                    sortable: false,
					render: function(data, type, row) {
						return row.CAMPANIA;
					}
				},
				{
					targets: 7,
					data: 'WCL.ETAPA_ID',
                    searchable: false,
					sortable: true,
					render: function(data, type, row) {
						//html = row.ETAPA_NOMBRE
						html = ''
						if (row.RIESGOS_GESTION == 9){
							html += ' <span class="label label-success">Aprobado</span>'
						}
						if (row.RIESGOS_GESTION == 10){
							html += ' <span class="label label-warning">Observado</span>'
						}
						if(row.RIESGOS_GESTION == 14){
							html += ' <span class="label label-danger">Rechazado</span>'
						}
						return html;

					}
				},
				{

					targets: 8,
					data: 'WCL.FLAG',
                    searchable: false,
                    sortable: true,
					render: function(data, type, row) {
						html = '';
						if (row.FLAG == 1) {

							checked = 'checked disabled'


							}
							else {checked = ''}
							return '<input class="chkPriorizar" " ruc="' + row.NUM_RUC + '" type="checkbox" value="" '+ checked + '>';

						return html;
					}
				}
			],
		});
	}

	$(document).on('change','.chkPriorizar',function(){
        elem = $(this);
        ruc = elem.attr('ruc');
        value = elem.is(":checked");
        if (ruc) {
            $.ajax({
                url: "{{ route('cmpEmergencia.backoffice.actualizar') }}",
                type: 'POST',
                data: {
                    ruc: ruc,
                    condicion: value,
                },
                beforeSend: function() {
										$("#cargando").modal();
                    elem.prop('disabled',true)
                },
                success: function (result) {

                },
                error: function (xhr, status, text) {
                    e.preventDefault();
                    Swal.fire('Hubo un error al registrar al actualizar los datos, contáctese con administración');
                    elem.prop('checked',!value)
                    elem.prop('disabled',false)
                },
                complete: function() {
                    elem.prop('disabled',true)
										$(".cerrarcargando").click();
                }
            });
        }
    		});


</script>
@stop
