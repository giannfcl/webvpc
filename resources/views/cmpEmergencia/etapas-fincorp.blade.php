<ul style="padding-left: 0px;padding-top:20px">
		<div class="form-row">
			<div class="form-group col-md-8">
				<label>Etapas</label>
			</div>
			<div class="form-group col-md-4">
				<label>N° Leads</label>
			</div>
		</div>
	@if(count($etapas)>0)
		@foreach ($etapas as $etapa)
		<div class="form-row">
			<div class="form-group col-md-8">
				<a href="{{route('cmpEmergencia.validacion.index')}}" class="btn btn-success btn-lg btn-block" style="margin-bottom: 20px;padding-bottom: 8px;padding-top: 8px">{{$etapa->ETAPA_NOMBRE}}</a>
			</div>
			<div class="form-group col-md-4">
				<div class="form-group col-md-2">
					<h5>{{$etapa->TOTAL}}</h5>
				</div>
				<div class="form-group col-md-2 triangulo_{{$etapa->ETAPA_ID}}" style="padding-top:6px;color:#169F85">
					&#x25B6
				</div>
			</div>
		</div>
		@endforeach
	@else
		<h5>No se encontro etapas</h5>
	@endif
</ul>