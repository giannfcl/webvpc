<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--link href="{{ URL::asset('css/bt4/bootstrap.min.css') }}" rel="stylesheet" type="text/css"-->
        <title>Acuerdo de Crédito</title>

        <style type="text/css">
            body{
              margin:0;
              font-family:-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";
              font-size:1rem;
              font-weight:400;line-height:1.5;color:#212529;text-align:left;
              background-color:#fff;
              margin-left: 50px;
              margin-right: 50px;
              margin-top: 8px;
              margin-bottom: 8px;
            }
            li{
                margin-bottom: 0.10em;
                word-spacing: 0.25em;
            }
            p{
                margin-bottom: 0.5em;
                line-height: 13px;
            }
            .underline{
                border-bottom-style: solid !important;
                border-bottom-width: 0.05px !important;
                padding-left:5px;
                padding-right: 5px;
            }
            .last {
                position: absolute; bottom: -65px !important;
                left: 0px; right: 0px; height: 100px;
            }
            .page_break { page-break-before: always; }
            .tablaTexto table {
                table-layout: auto !important;
                width: 100%;
            }
            .tablaTexto th, .myclass td, .myclass thead th, .myclass tbody td, .myclass tfoot td, .myclass tfoot th {
                width: auto !important;
                font-size: 6pt !important;
            }
            .variable{
                border-bottom-style: solid;
                border-left-style: solid;
                border-right-style: solid;
                border-width: 0.5px;
                padding-left: 10px;
                padding-right: 10px;
            }
            .space{
                width:10px;
            }
        </style>
    </head>
    <body class="nav-md" style="font-size: 8pt;">
        <div style="max-width: 100vw;padding: 0px 0px 0px 0px;">
            <div class="row">
                <div class="col-sm-3">
                    <img style="width: 20%" src="{{ URL::asset('img/logoIBK.png') }}">
                </div>
                <div style=" text-align: center;" class="col-sm-6">
                    <strong style="font-size:11pt;">Acuerdo de Crédito<br>Solicitud de Desembolso y Declaración Jurada</strong>
                </div>
            </div>
            <div>
            <p style="text-align: justify;">
                <table style="width: 100%">
                    <tr>
                        <td style="width:60px;">Lima, </td>
                        <td style="width:500px;border-bottom-style:solid;border-bottom-width:0.5px;">{{$fecha}}</td>
                    </tr>
                </table>
                <br>
                Señores<br>
                <strong>Banco Internacional del Perú S.A.A. – Interbank</strong><br>
                Presente.-
                <br><br>
                <table style="width: 100%">
                    <tr>
                        <td style="width:80px">
                        Atención:
                        </td>
                        <td style="width:80px">Señor(a):</td>
                        <td style="border-bottom-style:solid;border-bottom-width:0.5px;">{{ Auth::user()->NOMBRE }}</td>
                    </tr>
                    <tr>
                        <td style="width:80px">
                        </td>
                        <td style="width:80px">Cargo:</td>
                        <td style="border-bottom-style:solid;border-bottom-width:0.5px;">
                        Ejecutivo
                        </td>
                    </tr>
                </table>
                <br>
                <table>
                    <tr>
                        <td>Referencia: </td>
                        <td> Acuerdo de Crédito - Solicitud de Desembolso y Declaración Jurada</td>
                    </tr>
                </table>
                <br>
                De mi mayor consideración:
                <br>
                <p style="text-align:justify">Por medio de la presente solicito se sirvan efectuar el desembolso del crédito solicitado a Interbank, de acuerdo con lo establecido en el Decreto Legislativo No. 1455, sus normas modificatorias, ampliatorias y/o complementarias, que crea el Programa “Reactiva Perú” (en adelante el “Crédito”) cuyas condiciones son las siguientes: </p>
                <br>
                 <table style="border-collapse: collapse;border: 1px solid black;">
                    <tr style="border: 1px solid black;">
                        <td style="border: 1px solid black; padding-left:3px;padding-right:3px;"><strong>Monto<br>Aprobado¹:</strong></td>
                        <td style="border: 1px solid black;padding-left:3px;padding-right:3px; width:500px;">
                            @if($datos->SOLICITUD_OFERTA>0)
                               S/ &nbsp;{{number_format($datos->SOLICITUD_OFERTA,2,".",",")}}
                            @endif
                            </td>
                    </tr>
                    <tr style="border: 1px solid black;">
                        <td style="border: 1px solid black;padding-left:3px;padding-right:3px;"><strong>Plazo*:</strong></td>
                        <td style="border: 1px solid black;padding-left:3px;padding-right:3px; width:500px;">
                        36 meses
                         </td>
                    </tr>
                    <tr style="border: 1px solid black;">
                        <td style="border: 1px solid black;padding-left:3px;padding-right:3px;"><strong>Tasa**²:</strong></td>
                        <td style="border: 1px solid black;padding-left:3px;padding-right:3px; width:500px;">Hasta de 4 %</td>
                    </tr>
                    <tr style="border: 1px solid black;">
                        <td style="border: 1px solid black;padding-left:3px;padding-right:3px;"><strong>Abono en <br> Cuenta N*:</strong></td>
                        <td style="border: 1px solid black; padding-left:3px;padding-right:3px;width:500px;">{{$datos->CUENTA_NUMERO}}</td>
                    </tr>
                 </table>
                        (*) Incluye período de gracia de 12 meses <br>
                        (**) TEA/TCEA sobre la base de 360 días calendarios.<br>
                        En tal sentido, instruyo a Interbank a enviar al momento de realizar el desembolso, el cronograma, hoja resumen y demás documentación pertinente al Crédito solicitado al correo electrónico:
                        <br>
                        <table style="width:500px">
                            <tr><td style="border-bottom-style:solid;border-bottom-width:0.5px;">
                            {{$datos->CORREO}}
                            </td></tr>
                        </table>
                        <br>
                        <p style="text-align:justify">
                        Esta solicitud tiene el carácter de irrevocable. El desembolso del crédito estará sujeto a que Interbank haya recibido
                        la Garantía del Gobierno Nacional establecida para el Programa “Reactiva Perú” lo que le será comunicado a la
                        dirección de correo electrónico antes indicada o a la que el Cliente tenga debidamente registrada en Interbank. Del
                        mismo modo autorizamos a cargar, en la oportunidad que corresponda, en la Cuenta Corriente indicada
                        previamente el importe de la comisión y/o gastos administrativos de COFIDE aplicables por el otorgamiento de
                        la Garantía del Gobierno Nacional, desde la emisión de ésta y por el plazo de repago del Crédito establecido en el
                        cronograma.
                        </p>
                        <div class="last">
                            ______________________________
                            <p style="font-size:7.5pt;">¹ No incluye el ITF que es el 0.005 %, el cual aplica sobre el monto aprobado.<br>
                            ² La tasa definitiva del Crédito se establecerá de acuerdo a la disponibilidad de los fondos del Programa y las políticas internas del Banco, pero en ningún caso será mayor al 4%. La tasa definitiva del crédito figurará en la Hoja Resumen que se enviará al cliente al momento del desembolso. <br>
                            </p>
                            <br>
                            <p style="font-size:7pt;text-align:right; width:100%">
                            Acuerdo de Crédito - Solicitud de Desembolso y Declaración Jurada D.Leg. 1455 Pág, 1 de 4
                            </p>
                        </div>
                        <div class="page_break"></div>
                        <!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  -->

                    <table>
                        <tr>
                            <td>Asimismo,</td>
                            <td style="width:480px; border-bottom-style:solid;border-bottom-width:0.5px;font-size: 9px;">{{$datos->NOMBRE}}</td>
                        </tr>
                    </table>
                    (el “Cliente”), me comprometo y cumplo con declarar bajo juramento lo siguiente: <br>
                    <ol>
                    <li style="line-height: 100%;text-align:justify">
                        Que a la fecha de la presente Declaración Jurada no participo y me obligo a no participar durante la vigencia del Crédito en procesos de producción, comercio o servicios de cualquier producto o actividad que se considere ilegal bajo las leyes o la normativa del país o bajo convenios y acuerdos internacionales ratificados, incluyendo las convenciones/legislación relativa a la protección de los recursos de biodiversidad o patrimonio cultural señalados en el Anexo 1 del Reglamento Operativo del Programa Reactiva Perú, establecido por Resolución Ministerial Nº 165-2020-EF/15, que incluyen las modificaciones, ampliaciones u otros  (en adelante “RO”), el mismo que nos ha sido debidamente informado y declaro conocer en su integridad, así como respecto de cualquier modificación al referido Anexo 1 que pueda ser realizada durante la vigencia del Crédito.
                    </li>
                    <li style="line-height: 100%;text-align:justify">
                        Declaro y me obligo a no usar los fondos del Crédito para (i) pagar obligaciones financieras, con Interbank y/o cualquier otra entidad del Sistema Financiero, salvo por aquellas deudas no vencidas que tengan cuotas ordinarias, según periodicidad establecida en un cronograma de pagos (ii) adquisición de activos fijos, (iii) compra de acciones o participaciones en empresas o cualquier instrumento o derecho que tenga la misma finalidad, (iv) compra de bonos y otros activos monetarios, así como para realizar aportes de capital, o (v) el pago de obligaciones financieras vencidas del Cliente.
                    </li>
                    <li style="line-height: 100%;text-align:justify">
                        Durante la vigencia del Crédito y hasta que no se haya cancelado en su totalidad, no realizaré ningún prepago de obligaciones financieras presentes o futuras frente a Interbank y/u otras entidades del Sistema Financiero, asumiendo que el incumplimiento origina de forma inmediata la aceleración del Crédito.
                    </li>
                    <li style="line-height: 100%;text-align:justify">
                        No tengo deudas tributarias administradas por la SUNAT, exigibles en cobranza coactiva mayores a 1 UIT por periodos anteriores al año 2020
                    </li>
                    <li style="line-height: 100%;text-align:justify">
                        Que cumplo con los criterios de elegibilidad referidos a la clasificación en la Central de Riesgos de la SBS, de conformidad con lo dispuesto en el RO.
                    </li>
                    <li style="line-height: 100%;text-align:justify">
                        Que durante la vigencia del Crédito no distribuiré dividendos ni acordaré cualquier otra modalidad o mecanismo que tenga el mismo efecto que una distribución de dividendos, ni efectuaré reparto de utilidades, salvo el porcentaje correspondiente a nuestros trabajadores.
                    </li>
                    <li style="line-height: 100%;text-align:justify">
                        Que toda la documentación entregada a Interbank en el marco de aplicación del Decreto Legislativo No. 1455 y su RO, así como sus normas modificatorias, es correcta, veraz y exigible.
                    </li>
                    <li style="line-height: 100%;text-align:justify">
                        Que el Crédito cumple con todos los requisitos y obligaciones detalladas en el Decreto Legislativo No. 1455 y el RO, sus modificaciones y/o ampliaciones.
                    </li>
                    <li style="line-height: 100%;text-align:justify">
                        Con la sola suscripción del Anexo A, del presente documento, manifiesto cumplir con las declaraciones contenidas en la misma, y de forma adicional, me obligo a mantener vigentes las declaraciones y/u obligaciones allí contenidas durante toda la vigencia del Crédito, reconociendo expresamente que el presente Crédito, otorgado en el marco del Programa Reactiva Perú, no podrá ser materia de reestructuración y/o refinanciamiento mediante el Procedimiento Acelerado de Refinanciación Concursal (PARC) creado mediante el Decreto Legislativo No.1511, u otro esquema concursal. Asimismo no podré incurrir en ninguno de los supuestos de inhabilitación de acuerdo a la Ley  No. 30225 – Ley de Contrataciones del Estado, mientras se encuentre vigente el Crédito.
                    </li>
                     <li style="line-height: 100%;text-align:justify">
                        Que con una periodicidad no mayor de 360 días calendarios contados desde el desembolso del Crédito, remitiré la información financiera y/o cualquier otra información que sea requerida por Interbank, incluyendo una actualización de la presente Declaración Jurada, de ser el caso.
                    </li>
                     <!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  -->
                   </ol>
                    <div style="line-height: 100%;text-align:justify">
                    Las declaraciones y compromisos antes descritos se mantendrán vigentes desde la fecha de esta Declaración Jurada y hasta que se haya cancelado la totalidad del Crédito. El Cliente faculta a Interbank a verificar en cualquier momento durante la vigencia del Crédito la veracidad y/o exactitud de estas declaraciones y de verificarse la falsedad y/o inexactitud de alguna de ellas, así como el incumplimiento de alguno de los compromisos asumidos, se producirá una causal de incumplimiento, quedando Interbank facultado a dar por vencidos todos los plazos y a proceder a la aceleración del Crédito de forma automática, sin necesidad de requerimiento previo o intimación en mora, así como a la ejecución de los colaterales, títulos valores y/o garantías personales otorgadas para garantizar el cumplimiento de pago del Crédito. El Cliente declara conocer y aceptar que Interbank, en estricto cumplimiento de lo prescrito en el RO, estará facultado para tal aceleración y no podrá otorgar dispensas a ningún incumplimiento, procediendo a completar el Pagaré, por haberse configurado los supuestos d) y e) del Acuerdo de Llenado de Pagaré.
                    </div>
                    <br>

                    <p style="text-align:justify;line-height: 100%">
                        El Cliente adjunta a la presente solicitud, (i) Pagaré y (ii) Acuerdo de Llenado de Pagaré, debidamente suscritos por apoderados facultados que, en conjunto con la presente Solicitud de Desembolso y Declaración Jurada, así como los demás documentos requeridos por Interbank para la evaluación correspondiente, conforman el presente “Acuerdo de Crédito”.
                        <br>
                        Adicionalmente, declaro conocer y aceptar que mis datos personales brindados a Interbank podrán ser transferidos a Corporación Financiera De Desarrollo S.A solo para las finalidades pertinentes al Crédito solicitado.<sup style="vertical-align: super;">3</sup>
                        <br>
                        <br>
                        Atentamente,
                    </p>

                    <div class="last">
                        <p style="font-size:7pt;text-align:right; width:100%">Acuerdo de Crédito - Solicitud de Desembolso y Declaración Jurada D.Leg. 1455 Pág, 2 de 4</p>
                    </div>
                    <div class="page_break"></div>



                 <p style="padding-top:5px;padding-bottom:5px;">Si eres <strong>Persona Jurídica</strong>, firma aquí:</p>
                 <div style="font-size:7pt; width:100%; border-style: solid; border-width:0.5px;padding:5px;padding-top:5px; padding-bottom:10px;">
                    <table class="tablaTexto">
                        <tr>
                            <td>Los abajo firmantes, representantes legales de </td>
                            <td style="width: 380px;border-bottom-style:solid;border-bottom-width:0.5px;font-size: 9px;">
                                @if(substr($datos->NUM_RUC,0,2)==20)
                                    {{$datos->NOMBRE}}
                                @endif
                            </td>
                        </tr>
                    </table>

                    <table class="tablaTexto">
                        <tr>
                            <td style="text-align:right;">, con RUC No</td>
                            <td style="width: 450px;border-bottom-style:solid;border-bottom-width:0.5px; text-align:center">
                                @if(substr($datos->NUM_RUC,0,2)==20)
                                    {{ $datos->NUM_RUC}}
                                @endif
                            </td>
                            <td style="text-align:right;">con domicilio en</td>
                        </tr>
                    </table>

                    <table class="tablaTexto">
                        <tr>
                            <td style="height:14pt;width: 550px;border-bottom-style:solid;border-bottom-width:0.5px;font-size: 9px;">
                                    @if(substr($datos->NUM_RUC,0,2)==20)
                                          {{$direccion ? $direccion[0]->DIRECCION : ''}}
                                    @endif
                            </td>
                        </tr>
                    </table>
                    con facultades suficientes para suscribir el presente Acuerdo de Crédito - Solicitud de Desembolso  y Declaración Jurada, firmamos la misma en señal de conformidad.
                    <table class="tablaTexto">
                        <tr>
                            <td style="height: 20pt; vertical-align:bottom">Firma:&nbsp;</td>
                            <td style="width: 250px;border-bottom-style:solid;border-bottom-width:0.5px; height: 20pt"></td>
                            <td>&nbsp;&nbsp;</td>
                            <td style="height: 20pt; vertical-align:bottom">Firma:&nbsp;</td>
                            <td style="width: 250px;border-bottom-style:solid;border-bottom-width:0.5px; height: 20pt"></td>
                            </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Nombre:&nbsp;</td>
                            <td style="width: 250px;border-bottom-style:solid;border-bottom-width:0.5px;">
                                @if(substr($datos->NUM_RUC,0,2)==20)
                                   {{$datos->RRLL_APELLIDOS}} {{$datos->RRLL_NOMBRES}}
                                @endif
                            </td>
                            <td>&nbsp;&nbsp;</td>
                            <td>Nombre:&nbsp;</td>
                            <td style="width: 250px;border-bottom-style:solid;border-bottom-width:0.5px;">
                                @if(substr($datos->NUM_RUC,0,2)==20)
                                   {{$datos->RRLL_APELLIDOS_2}} {{$datos->RRLL_NOMBRES_2}}
                                @endif
                            </td>
                            </tr>
                                <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                        <tr>
                            <td>DNI No.:&nbsp;</td>
                            <td style="width: 250px;border-bottom-style:solid;border-bottom-width:0.5px;">
                                @if(substr($datos->NUM_RUC,0,2)==20)
                                    {{$datos->RRLL_DOCUMENTO}}
                                @endif
                            </td>
                            <td>&nbsp;&nbsp;</td>
                            <td>DNI No.:&nbsp;</td>
                            <td style="width: 250px;border-bottom-style:solid;border-bottom-width:0.5px;">
                                @if(substr($datos->NUM_RUC,0,2)==20)
                                    {{$datos->RRLL_DOCUMENTO_2}}
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Cargo:&nbsp;</td>
                            <td style="width: 250px;border-bottom-style:solid;border-bottom-width:0.5px;">
                                @if(substr($datos->NUM_RUC,0,2)==20)
                                Representante Legal
                                @endif
                            </td>
                            <td>&nbsp;&nbsp;</td>
                            <td>Cargo:&nbsp;</td>
                            <td style="width: 250px;border-bottom-style:solid;border-bottom-width:0.5px;">
                                @if(substr($datos->NUM_RUC,0,2)==20)
                                    @if(strlen($datos->RRLL_DOCUMENTO_2) > 0)
                                        Representante Legal
                                    @endif
                                @endif
                            </td>
                        </tr>
                    </table>
                 </div>

                 <!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  -->
                 </p>
                 <p style="padding-top:5px;padding-bottom:5px;">Si eres <strong>Persona Natural con Negocio</strong>, firma aquí:</p>
                 <div style="font-size:7pt; width:100%; border-style: solid; border-width:0.5px;padding:5px;padding-top:5px; padding-bottom:10px;">
                    <table class="tablaTexto">
                        <tr>
                            <td>Yo, </td>
                            <td style="width: 350px;border-bottom-style:solid;border-bottom-width:0.5px;font-size: 9px;">
                                @if(substr($datos->NUM_RUC,0,2)!=20)
                                    {{$datos->NOMBRE}}
                                @endif
                            </td>
                            <td>, identificado con Documento de Identidad N°</td>
                        </tr>
                    </table>
                    <table class="tablaTexto">
                        <tr>
                            <td style="width: 200px;border-bottom-style:solid;border-bottom-width:0.5px; text-align:center;">
                                @if(substr($datos->NUM_RUC,0,2)!=20)
                                    {{$datos->RRLL_DOCUMENTO}}
                                @endif
                            </td>
                            <td >;de estado civil</td>
                            <td style="width: 200px;border-bottom-style:solid;border-bottom-width:0.5px; text-align:center;">
                                 @if(substr($datos->NUM_RUC,0,2)!=20)
                                    @if($datos->RRLL_ESTADO_CIVIL == 1)
                                        Casado(a)
                                    @endif

                                    @if($datos->RRLL_ESTADO_CIVIL == 2)
                                        Conviviente(a)
                                    @endif

                                    @if($datos->RRLL_ESTADO_CIVIL == 3)
                                        Soltero
                                    @endif

                                    @if($datos->RRLL_ESTADO_CIVIL == 4)
                                        Viudo(a)
                                    @endif

                                    @if($datos->RRLL_ESTADO_CIVIL == 5)
                                        Divorciado(a)
                                    @endif

                                    @if($datos->RRLL_ESTADO_CIVIL == 6)
                                        Separado
                                    @endif
                                @endif
                            </td>
                            <td>con domicilio en</td>
                        </tr>
                    </table>
                    <table class="tablaTexto">
                        <tr>
                            <td style="height:14pt; width: 580px;border-bottom-style:solid;border-bottom-width:0.5px; font-size: 9px;">
                                    @if(substr($datos->NUM_RUC,0,2)!=20)
                                         {{$direccion ? $direccion[0]->DIRECCION : ''}}
                                    @endif
                            </td>
                        </tr>
                    </table>
                    firmo la misma en señal de conformidad.
                    <br>
                    <table class="tablaTexto">
                        <tr>
                            <td style="vertical-align:bottom">Firma Titular:</td>
                            <td style="width: 250px;border-bottom-style:solid;border-bottom-width:0.5px; height: 20pt">&nbsp;</td>
                            <td>&nbsp;&nbsp;</td>
                            <td style="vertical-align:bottom">Firma cónyuge:</td>
                            <td style="width: 250px;border-bottom-style:solid;border-bottom-width:0.5px; height: 20pt">&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                    <table class="tablaTexto">
                        <tr>
                            <td>Nombre:&nbsp;</td>
                            <td style="width: 250px;border-bottom-style:solid;border-bottom-width:0.5px;">
                                @if(substr($datos->NUM_RUC,0,2)!=20)
                                     {{$datos->RRLL_APELLIDOS}} {{$datos->RRLL_NOMBRES}}
                                @endif
                            </td>
                            <td>&nbsp;&nbsp;</td>
                            <td>Nombre:&nbsp;</td>
                            <td style="width: 250px;border-bottom-style:solid;border-bottom-width:0.5px;">
                                @if(substr($datos->NUM_RUC,0,2)!=20)
                                     {{$datos->RRLL_APELLIDOS_2}} {{$datos->RRLL_NOMBRES_2}}
                                @endif
                            </td>
                        </tr>
                        </tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                    <table class="tablaTexto">
                        <tr>
                            <td>DNI No.:&nbsp;</td>
                            <td style="width: 250px;border-bottom-style:solid;border-bottom-width:0.5px;">
                                @if(substr($datos->NUM_RUC,0,2)!=20)
                                    {{$datos->RRLL_DOCUMENTO}}
                                @endif
                            </td>
                            <td>&nbsp;&nbsp;</td>
                            <td>DNI No.:&nbsp;</td>
                            <td style="width: 250px;border-bottom-style:solid;border-bottom-width:0.5px;">
                                @if(substr($datos->NUM_RUC,0,2)!=20)
                                    {{$datos->RRLL_DOCUMENTO_2}}
                                @endif
                            </td>
                        </tr>
                    </table>
                 </div>
                 <p style="padding-top:5px;padding-bottom:5px;">Aceptado por <strong>INTERBANK</strong>:</p>
                 <div style="font-size:7pt; width:100%; border-style: solid; border-width:0.5px;padding:5px;padding-top:5px; padding-bottom:10px;">
                    <p style="text-align:justify;">
                    Los abajo firmantes, representantes legales de Banco Internacional del Perú S.A.A-Interbank, con RUC No. 20100053455, con facultades suficientes para suscribir el presente documento, según poderes debidamente inscritos en la Partida No.11009129, del Registro de Personas Jurídicas de Lima, firmamos el mismo en señal de aceptación, con lo cual queda debidamente formalizado el presente Acuerdo de Crédito – Solicitud de Desembolso y Declaración Jurada.
                    </p>
                    <table class="tablaTexto">
                        <tr>
                            <td style="vertical-align:bottom">Firma:</td>
                            <td style="width: 200px;border-bottom-style:solid;border-bottom-width:0.5px; height: 18pt">&nbsp;</td>
                            <td>&nbsp;&nbsp;</td>
                            <td style="vertical-align:bottom">Firma:</td>
                            <td style="width: 200px;border-bottom-style:solid;border-bottom-width:0.5px; height: 18pt">&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Nombre:&nbsp;</td>
                            <td style="width: 200px;border-bottom-style:solid;border-bottom-width:0.5px;">&nbsp;</td>
                            <td>&nbsp;&nbsp;</td>
                            <td>Nombre:&nbsp;</td>
                            <td style="width: 200px;border-bottom-style:solid;border-bottom-width:0.5px;">&nbsp;</td>
                        </tr>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>DNI No.:&nbsp;</td>
                            <td style="width: 200px;border-bottom-style:solid;border-bottom-width:0.5px;">&nbsp;</td>
                            <td>&nbsp;&nbsp;</td>
                            <td>DNI No.:&nbsp;</td>
                            <td style="width: 200px;border-bottom-style:solid;border-bottom-width:0.5px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Cargo:&nbsp;</td>
                            <td style="width: 100px;border-bottom-style:solid;border-bottom-width:0.5px;">&nbsp;</td>
                            <td>&nbsp;&nbsp;</td>
                            <td>Cargo:&nbsp;</td>
                            <td style="width: 100px;border-bottom-style:solid;border-bottom-width:0.5px;">&nbsp;</td>
                        </tr>
                    </table>
                 </div>
                 <br>
                 <div class="last">
                 ______________________________
                        <br>
                        <p style="font-size:6.5pt"><sup style="vertical-align: super;">3</sup> Solo aplica para clientes Persona Natural Con Negocio</p>
                        <p style="font-size:7pt;text-align:right; width:100%">
                            Acuerdo de Crédito - Solicitud de Desembolso y Declaración Jurada D.Leg. 1455 Pág, 3 de 4
                        </p>

                 </div>
                 <div class="page_break"></div>
                        <!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  -->
                <!-- HOJA 4 -->
                <div style="text-align: center;"><p><strong> Anexo A <strong></p></div>
                    <table>
                        <tr>
                            <td>Nosotros, </td>
                            <td style="width:490px; border-bottom-style:solid;border-bottom-width:0.5px;font-size: 9px;">{{$datos->NOMBRE}}</td>
                        </tr>

                    </table>
                    <table>
                        <tr>
                            <td>,con Registro Único de Contribuyente </td>
                            <td style="width:250px; border-bottom-style:solid;border-bottom-width:0.5px;font-size: 9px;">{{$datos->NUM_RUC}}</td>
                        </tr>
                        <tr>
                            <td colspan="2">,declaramos ante el Banco Internacional del Perú S.A.A. – Interbank, lo siguiente: </td>
                        </tr>
                    </table>
                   <ol style="padding-top: 0px; margin-top: 0px; padding-bottom: 1px; margin-bottom: 1px;">
                    <li>
                        Que, no nos encontramos comprendidos dentro de los alcances de la Ley Nº 30737 ni de la Décimo Tercera Disposición Complementaria Final de la citada Ley.
                    </li>
                    <li>
                        Que, ninguno de los representantes acreditados ante el Banco Internacional del Perú S.A.A. - Interbank se encuentran sometidos a procesos por delitos de corrupción y conexos</p>
                    </li>
                    <li>
                        Que, al momento de presentar la presente declaración jurada no nos encontramos inhabilitados por el Tribunal De Contrataciones del Estado del Organismo Supervisor de la Contrataciones del Estado.
                    </li>
                    <li>
                        No contamos con un préstamo en el marco de REACTIVA PERÚ[
                                @if($datos->FLG_TOMO_REACTIVA_1>0)
                                    &nbsp;&nbsp;&nbsp;
                                @else
                                    X
                                @endif
                        ]
                    <li>

                        Sí contamos con un préstamo en el marco de REACTIVA PERÚ [
                                @if($datos->FLG_TOMO_REACTIVA_1>0)
                                    X
                                @else
                                    &nbsp;&nbsp;&nbsp;
                                @endif
                        ]
                    <table class="tablaTexto">
                        <tr>
                            <td style="width: 110px">Monto: </td>
                            <td>&nbsp;</td>
                            <td style="width: 200px; border-bottom-style:solid; border-bottom-width:0.5px;">
                                S/
                                @if($datos->MONTO_PRESTAMO_REACTIVA_1>0)
                                {{number_format( $datos->MONTO_PRESTAMO_REACTIVA_1,2)}}
                                @else
                                    &nbsp;
                                @endif
                            </td>
                        </tr>
                    </table>
                    <table>
                        <tr>
                            <td style="width: 110px">Banco Otorgante: </td>
                            <td>&nbsp;</td>
                            <td style="width: 450px; border-bottom-style:solid; border-bottom-width:0.5px;">
                                @foreach ($bancos as $banco)
                                @if($datos->BANCO_REACTIVA_1 == $banco->CODIGO)
                                    {{ $banco->DESCRIPCION_COMPLETA }}
                                @else
                                @endif
                            @endforeach
                            </td>
                        </tr>
                     </table>
                     <!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  -->
                   </ol>

                   <table style="width: 100%">
                    <tr>
                    <td style="width:60px;">Lima, {{$fecha}}</td>
                    </tr>
                    </table>
                    <p style="padding-top:0px;padding-bottom:5px; margin-top: 1px;">Si eres <strong>Persona Jurídica</strong>, firma aquí:</p>
                    <div style="font-size:7pt; width:100%; border-style: solid; border-width:0.5px;padding:5px;padding-top:5px; padding-bottom:10px;">
                       <table class="tablaTexto">
                           <tr>
                               <td>Los abajo firmantes, representantes legales de </td>
                               <td style="width: 380px;border-bottom-style:solid;border-bottom-width:0.5px;font-size: 9px;">
                                   @if(substr($datos->NUM_RUC,0,2)==20)
                                       {{$datos->NOMBRE}}
                                   @endif
                               </td>
                           </tr>
                       </table>

                       <table class="tablaTexto">
                           <tr>
                               <td style="text-align:right;">, con RUC No</td>
                               <td style="width: 450px;border-bottom-style:solid;border-bottom-width:0.5px; text-align:center">
                                    @if(substr($datos->NUM_RUC,0,2)==20)
                                        {{ $datos->NUM_RUC}}
                                    @endif
                               </td>
                               <td style="text-align:right;">con domicilio en</td>
                           </tr>
                       </table>

                       <table class="tablaTexto">
                           <tr>
                               <td style="height:14pt;width: 550px;border-bottom-style:solid;border-bottom-width:0.5px;font-size: 9px;">
                                       @if(substr($datos->NUM_RUC,0,2)==20)
                                             {{$direccion ? $direccion[0]->DIRECCION : ''}}
                                       @endif
                               </td>
                           </tr>
                       </table>

                      <p style="text-align:justify">con facultades suficientes para suscribir el presente Acuerdo de Crédito - Solicitud de Desembolso  y Declaración Jurada, firmamos la misma en señal de conformidad.</p>
                       <table class="tablaTexto">
                           <tr>
                               <td style="height: 20pt; vertical-align:bottom">Firma:</td>
                               <td style="width: 250px;border-bottom-style:solid;border-bottom-width:0.5px; height: 20pt"></td>
                               <td>&nbsp;&nbsp;</td>
                               <td style="height: 20pt; vertical-align:bottom">Firma:</td>
                               <td style="width: 250px;border-bottom-style:solid;border-bottom-width:0.5px; height: 20pt"></td>
                               </tr>
                           <tr>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                           </tr>
                           <tr>
                               <td>Nombre:&nbsp;</td>
                               <td style="width: 250px;border-bottom-style:solid;border-bottom-width:0.5px;">
                                   @if(substr($datos->NUM_RUC,0,2)==20)
                                      {{$datos->RRLL_APELLIDOS}} {{$datos->RRLL_NOMBRES}}
                                   @endif
                               </td>
                               <td>&nbsp;&nbsp;</td>
                               <td>Nombre:&nbsp;</td>
                               <td style="width: 250px;border-bottom-style:solid;border-bottom-width:0.5px;">
                                   @if(substr($datos->NUM_RUC,0,2)==20)
                                      {{$datos->RRLL_APELLIDOS_2}} {{$datos->RRLL_NOMBRES_2}}
                                   @endif
                               </td>
                               </tr>
                                   <tr>
                                   <td>&nbsp;</td>
                                   <td>&nbsp;</td>
                                   <td>&nbsp;</td>
                                   <td>&nbsp;</td>
                                   <td>&nbsp;</td>
                               </tr>
                           <tr>
                               <td>DNI No.:&nbsp;</td>
                               <td style="width: 250px;border-bottom-style:solid;border-bottom-width:0.5px;">
                                   @if(substr($datos->NUM_RUC,0,2)==20)
                                       {{$datos->RRLL_DOCUMENTO}}
                                   @endif
                               </td>
                               <td>&nbsp;&nbsp;</td>
                               <td>DNI No.:&nbsp;</td>
                               <td style="width: 250px;border-bottom-style:solid;border-bottom-width:0.5px;">
                                   @if(substr($datos->NUM_RUC,0,2)==20)
                                       {{$datos->RRLL_DOCUMENTO_2}}
                                   @endif
                               </td>
                           </tr>
                           <tr>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                           </tr>
                           <tr>
                               <td>Cargo:&nbsp;</td>
                               <td style="width: 250px;border-bottom-style:solid;border-bottom-width:0.5px;">
                                   @if(substr($datos->NUM_RUC,0,2)==20)
                                   Representante Legal
                                   @endif
                               </td>
                               <td>&nbsp;&nbsp;</td>
                               <td>Cargo:&nbsp;</td>
                               <td style="width: 250px;border-bottom-style:solid;border-bottom-width:0.5px;">
                                   @if(substr($datos->NUM_RUC,0,2)==20)
                                       @if(strlen($datos->RRLL_DOCUMENTO_2) > 0)
                                           Representante Legal
                                       @endif
                                   @endif
                               </td>
                           </tr>
                       </table>
                    </div>

                    <!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  --><!--  -->
                    </p>
                    <p style="padding-top:5px;padding-bottom:5px;">Si eres <strong>Persona Natural con Negocio</strong>, firma aquí:</p>
                    <div style="font-size:7pt; width:100%; border-style: solid; border-width:0.5px;padding:5px;padding-top:5px; padding-bottom:10px;">
                       <table class="tablaTexto">
                           <tr>
                               <td>Yo, </td>
                               <td style="width: 350px;border-bottom-style:solid;border-bottom-width:0.5px;font-size: 9px;">
                                   @if(substr($datos->NUM_RUC,0,2)!=20)
                                       {{$datos->NOMBRE}}
                                   @endif
                               </td>
                               <td>, identificado con Documento de Identidad N°</td>
                           </tr>
                       </table>
                       <table class="tablaTexto">
                           <tr>
                               <td style="width: 200px;border-bottom-style:solid;border-bottom-width:0.5px; text-align:center;">
                                   @if(substr($datos->NUM_RUC,0,2)!=20)
                                       {{$datos->RRLL_DOCUMENTO}}
                                   @endif
                               </td>
                               <td >;de estado civil</td>
                               <td style="width: 200px;border-bottom-style:solid;border-bottom-width:0.5px; text-align:center;">
                                    @if(substr($datos->NUM_RUC,0,2)!=20)
                                       @if($datos->RRLL_ESTADO_CIVIL == 1)
                                           Casado(a)
                                       @endif

                                       @if($datos->RRLL_ESTADO_CIVIL == 2)
                                           Conviviente(a)
                                       @endif

                                       @if($datos->RRLL_ESTADO_CIVIL == 3)
                                           Soltero
                                       @endif

                                       @if($datos->RRLL_ESTADO_CIVIL == 4)
                                           Viudo(a)
                                       @endif

                                       @if($datos->RRLL_ESTADO_CIVIL == 5)
                                           Divorciado(a)
                                       @endif

                                       @if($datos->RRLL_ESTADO_CIVIL == 6)
                                           Separado
                                       @endif
                                   @endif
                               </td>
                               <td>con domicilio en</td>
                           </tr>
                       </table>
                       <table class="tablaTexto">
                           <tr>
                               <td style="height:14pt; width: 580px;border-bottom-style:solid;border-bottom-width:0.5px; font-size: 9px;">
                                       @if(substr($datos->NUM_RUC,0,2)!=20)
                                            {{$direccion ? $direccion[0]->DIRECCION: ''}}
                                       @endif
                               </td>
                           </tr>
                       </table>
                       firmo la misma en señal de conformidad.
                       <br>
                       <br>
                       <table class="tablaTexto">
                           <tr>
                                <td style="vertical-align:bottom">Firma Titular:</td>
                                <td style="width: 200px;border-bottom-style:solid;border-bottom-width:0.5px; height: 20pt">&nbsp;</td>
                                <td>&nbsp;&nbsp;</td>
                                <td style="vertical-align:bottom">Firma cónyuge:</td>
                                <td style="width: 200px;border-bottom-style:solid;border-bottom-width:0.5px; height: 20pt">&nbsp;</td>
                           </tr>
                           <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                           </tr>
                       </table>
                       <table class="tablaTexto">
                           <tr>
                               <td>Nombre:&nbsp;</td>
                               <td style="width: 250px;border-bottom-style:solid;border-bottom-width:0.5px;">
                                   @if(substr($datos->NUM_RUC,0,2)!=20)
                                        {{$datos->RRLL_APELLIDOS}} {{$datos->RRLL_NOMBRES}}
                                   @endif
                               </td>
                               <td>&nbsp;&nbsp;</td>
                               <td>Nombre:&nbsp;</td>
                               <td style="width: 250px;border-bottom-style:solid;border-bottom-width:0.5px;">
                                   @if(substr($datos->NUM_RUC,0,2)!=20)
                                        {{$datos->RRLL_APELLIDOS_2}} {{$datos->RRLL_NOMBRES_2}}
                                   @endif
                               </td>
                           </tr>
                           </tr>
                               <tr>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                           </tr>
                       </table>
                       <table class="tablaTexto">
                           <tr>
                               <td>DNI No.:&nbsp;</td>
                               <td style="width: 250px;border-bottom-style:solid;border-bottom-width:0.5px;">
                                   @if(substr($datos->NUM_RUC,0,2)!=20)
                                       {{$datos->RRLL_DOCUMENTO}}
                                   @endif
                               </td>
                               <td>&nbsp;&nbsp;</td>
                               <td>DNI No.:&nbsp;</td>
                               <td style="width: 240px;border-bottom-style:solid;border-bottom-width:0.5px;">
                                   @if(substr($datos->NUM_RUC,0,2)!=20)
                                       {{$datos->RRLL_DOCUMENTO_2}}
                                   @endif
                               </td>
                           </tr>
                       </table>
                    </div>


                    <div class="last">
                        <p style="font-size:7pt;text-align:right; width:100%">Acuerdo de Crédito - Solicitud de Desembolso y Declaración Jurada D.Leg. 1455 Pág, 4 de 4</p>
                    </div>


            </div>
        </div>

    </body>
</html>
