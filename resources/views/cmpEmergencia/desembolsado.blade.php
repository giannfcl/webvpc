@extends('cmpEmergencia.menu')

@section('datable')

<div>
  <table class='table table-striped table-bordered jambo_table' id="datatable">
    <thead>
      <tr>
        <th scope="col">Empresa</th>
        <th scope="col">Fecha Desembolso</th>
        <th scope="col">Monto Desembolsado</th>
        <th scope="col">Tasa WIO</th>
        <th scope="col">Condiciones</th>
        <th scope="col">N° Crédito</th>
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table>
</div>


<div class="modal fade" tabindex="-1" role="dialog" id="modalDesembolsado">
    <div class="modal-dialog" role="document" style="width: 1000px;">
        <div class="modal-content">
            <div class="modal-body">
                <form id="formDesem" method="POST"  class="form-horizontal form-label-left" enctype="multipart/form-data" action="{{route('cmpEmergencia.validacionInteresado')}}">                    
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Información del Cliente</h4>
                    </div>
                    <br>
                    
                    <div class="form-group add">
                          <div class="col-sm-6" >
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;" >RUC: </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px" >
                                <input  class="form-control"  type="text" name="numruc" readonly>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Teléfono:</label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input  class="form-control input-number"  type="text" name="telefono" disabled>
                            </div>
                        </div>
                    </div>

                    <div class="form-group add">
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Empresa:</label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input  class="form-control"  type="text" name="cliente" disabled>
                            </div>
                        </div>

                         <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Correo: </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input  class="form-control"  type="text" name="email" disabled>
                            </div>
                        </div>
                    </div>

                    <div class="modal-header">
                        <h4 class="modal-title" style="padding-left:0px;">Información Desembolso</h4>
                    </div>
                    <br>

                    <div class="form-group add">
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Monto Desembolsado S/.: </label>
                            <div class="col-sm-8 col-xs-12">
                                <input  class="form-control"  name="montodesem" disabled>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">N° Crédito: </label>
                            <div class="col-sm-8 col-xs-12">
                                <input  class="form-control"  name="numcredito" disabled>
                            </div>
                        </div>
                    </div>

                    <div class="form-group add">
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Tasa: </label>
                            <div class="col-sm-8 col-xs-12">
                                <input  class="form-control"  name="tasaWio" readonly>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Garantía: </label>
                            <div class="col-sm-8 col-xs-12">
                                <input  class="form-control"  name="garantia" readonly>
                            </div>
                        </div>
                    </div>

                    <div class="form-group add">
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Monto Garantizado: </label>
                            <div class="col-sm-8 col-xs-12">
                                <input  class="form-control"  name="montoGarantizado" readonly>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Riesgo Descubierto: </label>
                            <div class="col-sm-8 col-xs-12">
                                <input  class="form-control"  name="riesgoDescubierto" readonly>
                            </div>
                        </div>
                    </div>

                    <div class="modal-header">
                        <h4 class="modal-title" style="padding-left:0px;">Información de Subasta</h4>
                    </div>
                    <br>

                    <div class="form-group add">
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Fecha subasta: </label>
                            <div class="col-sm-8 col-xs-12">
                                <input  class="form-control"  name="subastaFecha" readonly>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">N° subasta: </label>
                            <div class="col-sm-8 col-xs-12">
                                <input  class="form-control"  name="subastaNumero" readonly>
                            </div>
                        </div>
                    </div>

                    <div class="form-group add">
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Tasa Final: </label>
                            <div class="col-sm-8 col-xs-12">
                                <input  class="form-control"  name="tasaFinal" readonly>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Código Repo: </label>
                            <div class="col-sm-8 col-xs-12">
                                <input  class="form-control"  name="codigoRepo" readonly>
                            </div>
                        </div>
                    </div>

                    <div class="form-group add">
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Cobertura %: </label>
                            <div class="col-sm-8 col-xs-12">
                                <input  class="form-control"  name="wioCobertura" readonly>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop

<style type="text/css">
    #modalDesembolsado input,textarea,select{
        margin-bottom: -15px;
    }
    #modalDesembolsado div.modal-header{
        padding: 5px !important;
    }
</style>

@section('js-vista')
<script>
    configurarTabla("{{$request['etapa']}}");

    $(document).on("click",".verDetalles",function(){
        $("#modalDesembolsado").modal();
        var ruc = $(this).attr('ruc')
        $.ajax({
            url: "{{ route('cmpEmergencia.buscarinfo') }}",
            type: 'GET',
            data: {
                numruc: ruc
            },
            beforeSend: function() {
                $("#cargando").modal();
            },
            success: function (result) {
                $('#formDesem input[name="numruc"]').val(result['NUM_RUC']);
                $('#formDesem input[name="cliente"]').val(result['NOMBRE']);
                $('#formDesem input[name="telefono"]').val(result['TELEFONO1']);
                $('#formDesem input[name="email"]').val(result['CORREO']);

                $('#formDesem input[name="montodesem"]').val("$/."+numeral(result['DESEMBOLSO_MONTO']).format('0,0'));
                $('#formDesem input[name="numcredito"]').val(result['NUM_CREDITO']);

                $('#formDesem input[name="tasaWio"]').val(result['WIO_TASA'] + '%');
                garantia = getCoberturaMonto(result['WIO_MONTO_SOLICITADO']);
                $('#formDesem input[name="garantia"]').val(garantia*100 + '%');

                $('#formDesem input[name="montoGarantizado"]').val('S/. ' + numeral(garantia*result['WIO_MONTO_SOLICITADO']).format('0,0'));
                $('#formDesem input[name="riesgoDescubierto"]').val(numeral((1-garantia)*result['WIO_MONTO_SOLICITADO']).format('0,0'));

                $('#formDesem input[name="subastaFecha"]').val(result['FECHA_APROBACIO_SUBASTA']);
                $('#formDesem input[name="subastaNumero"]').val(result['NROSUBASTA']);
                $('#formDesem input[name="tasaFinal"]').val((result['SUBASTA_TASA']*100) + '%');
                $('#formDesem input[name="codigoRepo"]').val(result['SUBASTA_CODIGO_REPO']);
                $('#formDesem input[name="wioCobertura"]').val((result['SUBASTA_COBERTURA'] * 100) + '%');
                
            },
            complete: function() {
                $(".cerrarcargando").click();
            }
        }); 
    });

    function configurarTabla(datos=null){
        if ($.fn.dataTable.isDataTable('#datatable')) {
                $('#datatable').DataTable().destroy();
        }

        $('#datatable').DataTable({
            processing: true,
            "bAutoWidth": false,
            rowId: 'staffId',
            // dom: 'Blfrtip',
            serverSide: true,
            language: {"url": "{{ URL::asset('js/Json/Spanish.json') }}"},
            ajax: {
                    "type": "GET",
                    "url": "{{ route('cmpEmergencia.lista') }}",
                    data: function(data) {
                            data.etapa = datos;
                    }
            },
            "iDisplayLength": 25,
            "order": [
                    [0, "desc"]
            ],
            columnDefs: [
                {
                    targets: 0,
                    data: 'WCL.NUM_RUC',
                    name: 'WCL.NUM_RUC',
                    searchable: true,
                    width: '35%',
                    render: function(data, type, row) {
                        return "<u><a class='verDetalles' style='cursor: pointer' ruc='"+row.NUM_RUC+"'><span align='center' >"+row.NUM_RUC+"</span><br><span>"+ row.NOMBRE+"</span></a></u>";
                    }
                },
                {
                    targets: 1,
                    data: 'WCL.DESEMBOLSO_FECHA',
                    name: 'WCL.DESEMBOLSO_FECHA',
                    searchable: false,
                    render: function(data, type, row) {
                        if (row.DESEMBOLSO_FECHA) {
                            return row.FORMAT_DESEMBOLSO_FECHA;
                        }
                        return '-';
                    }
                },
                {
                    targets: 2,
                    data: 'WCL.DESEMBOLSO_MONTO',
                    name: 'WCL.DESEMBOLSO_MONTO',
                    searchable: false,
                    render: function(data, type, row) {
                        if (row.DESEMBOLSO_MONTO) {
                            return "S/. " +numeral(row.DESEMBOLSO_MONTO).format('0.00');
                        }
                        return '-';
                    }
                },
                {
                    targets: 3,
                    data: 'WCL.WIO_TASA',
                    name: 'WCL.WIO_TASA',
                    searchable: false,
                    render: function(data, type, row) {
                        if (row.WIO_TASA) {
                            return numeral(row.WIO_TASA).format('0.00') + " %";
                        }
                        return '-';
                    }
                },
                {
                    targets: 4,
                    data: null,
                    searchable: false,
                    sortable:false,
                    render: function(data, type, row) {
                        return "<span align='left' >"+(row.WIO_PLAZO||'0')+" meses -"+(row.WIO_GRACIA||'0')+" gracia</span>";
                    }
                },
                {
                    targets: 5,
                    data: null,
                    searchable: false,
                    sortable:false,
                    render: function(data, type, row) {
                        if (row.NUM_CREDITO) {
                            return row.NUM_CREDITO
                        }
                        return "-";
                    }
                }
            ]
        });
    }
</script>
@stop