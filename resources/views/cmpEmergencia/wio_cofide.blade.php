@extends('cmpEmergencia.menu')

@section('datable')

<table class='table table-striped table-bordered jambo_table' id="datatable">
        <thead>
            <tr>
                <th scope="col" style="font-size: 12.5px;">Empresa</th>
                <th scope="col" style="font-size: 12.5px;">Monto Solicitado Cliente <br>/ % Cobertura</th>
                <th scope="col" style="font-size: 12.5px;">Condiciones</th>
                <th scope="col" style="font-size: 12.5px;">Tasa WIO</th>
                <th scope="col" style="font-size: 12.5px;">Estado</th>
                <th scope="col" style="font-size: 12.5px;">Fecha de Observacion</th>
                <th scope="col" style="font-size: 12.5px;">Deadline</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
</table>


<div class="modal fade" tabindex="-1" role="dialog" id="modalWioCofide">
    <div class="modal-dialog" role="document" style="width: 1000px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Información del Cliente</h4>
            </div>
            <div class="modal-body">
                <form id="formWio" method="POST"  class="form-horizontal form-label-left" enctype="multipart/form-data" action="{{route('cmpEmergencia.wioparacofide')}}">

                    <div class="form-group add">
                          <div class="col-sm-6" >
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;" >RUC: </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px" >
                                <input  class="form-control" type="text" name="numruc" readonly>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Teléfono:</label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input  class="form-control input-number" type="text" name="telefono" disabled>
                            </div>
                        </div>
                    </div>

                    <div class="form-group add">
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Empresa:</label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input  class="form-control" type="text" name="cliente" disabled>
                            </div>
                        </div>

                         <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Correo: </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input  class="form-control" type="text" name="email" disabled>
                            </div>
                        </div>
                    </div>

                    <div class="form-group add">
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Tiene Línea: </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input  class="form-control" type="text" name="tieneLinea" disabled>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Oferta Riesgos S/.: </label>
                            <div class="col-sm-4 col-xs-12" style="padding-left:0px">
                                <input  class="form-control" type="text" name="riesgosMonto" id="riesgosMonto" disabled>
                            </div>
                            <div class="col-sm-4 col-xs-12" style="padding-left:0px">
                                <input  class="form-control" type="text" name="riesgosCobertura" id="riesgosCobertura" disabled>
                            </div>
                        </div>
                    </div>

                    <div class="form-group add">
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Ventas Anuales S/.: </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input  class="form-control" name="volventa" disabled onkeypress="return filterFloat(event,this);">
                            </div>
                        </div>
                        <div class="col-sm-6">
                          <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Desembolso RP1 S/.: </label>
                          <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input autocomplete="off" class="form-control editar"  name="gmontoReactiva1" disabled id="gmontoReactiva1" onkeypress="return filterFloat(event,this);" >
                          </div>
                        </div>
                    </div>

                    <div class="modal-header"  style="padding-left:0px;">
                        <h4 class="modal-title">Información WIO</h4>
                    </div><br>

                    <div class="form-group add">
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Monto WIO S/.: </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input  class="form-control" name="montoWio" disabled>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Garantía: </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input  class="form-control" name="garantia" disabled>
                            </div>
                        </div>
                    </div>

                    <div class="form-group add">
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Tasa WIO: </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input  class="form-control" name="tasaWio" disabled>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Plazo/Gracia: </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input  class="form-control" name="plazoGracia" disabled>
                            </div>
                        </div>
                    </div>

                    <div class="form-group add">
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Monto Garantizado: </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input  class="form-control" name="montoGarantizado" disabled>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Riesgo Descubierto: </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input  class="form-control" name="riesgoDescubierto" disabled>
                            </div>
                        </div>
                    </div>

                    <div class="modal-header"  style="padding-left:0px;">
                        <h4 class="modal-title">Información de Subasta</h4>
                    </div><br>

                    <div class="form-group add">
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Fecha subasta: </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input  class="form-control" name="subastaFecha" disabled>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">N° subasta: </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input  class="form-control" name="subastaNumero" disabled>
                            </div>
                        </div>
                    </div>

                    <div class="form-group add">
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Tasa Final: </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input  class="form-control" name="tasaFinal" disabled>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Código Repo: </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input  class="form-control" name="codigoRepo" disabled>
                            </div>
                        </div>
                    </div>

                    <div class="form-group add">
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Cobertura %: </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input  class="form-control" name="wioCobertura" disabled>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label col-sm-4 col-xs-12" style="text-align: left;">Deadline: </label>
                            <div class="col-sm-8 col-xs-12" style="padding-left:0px">
                                <input  class="form-control" name="subastaDeadline" readonly style="color: #ff0000; font-weight: bold;">
                            </div>
                        </div>
                    </div>
                    <center>
                        <button class="btn btn-sm btn-success guardarinfo">Guardar</button>
                    </center>
                </form>
            </div>
        </div>
    </div>
</div>

@stop



@section('js-vista')

<style>
    #modalWioCofide input,textarea,select{
        margin-bottom: -16px;
    }
    #modalWioCofide div.modal-header{
        padding: 4px !important;
    }
</style>

<script>
    tablaWIOCOFIDE("{{$request['etapa']}}");
    $('[data-toggle="tooltip"]').tooltip();

    function ValVentasEsludWio(venta,montoreactiva1,montowio) {
        if (isNaN(parseFloat(venta))) {v_venta = 0} else {v_venta = parseFloat(venta)/12};
        if (isNaN(parseFloat(montoreactiva1))) {v_montoreactiva1 = 0} else {v_montoreactiva1 = parseFloat(montoreactiva1)*3};
    }

    $('#formWio').submit(function(){
        montorp1 = numeral($('#formWio input[name="gmontoReactiva1"]').val());
        ventas = numeral($('#formWio input[name="volventa"]').val());
        montoWio = numeral($('#formWio input[name="montoWio"]').val());
        // console.log(montorp1.value());
        // console.log(ventas.value());
        montomax = recalcularMonto(ventas.value(),montorp1.value());
        // console.log(montomax);
        if(montoWio.value() > montomax){
            Swal.fire('El monto WIO es mayor al límite máximo permitido: S/ ' + numeral(montomax).format('0,0'))
            return false;
        }
    });


    $('#formWio input[name="gmontoReactiva1"]').keyup(function(){
        monto_wio = isNaN(parseFloat($('#formWio input[name="montoWio"]').val()))?0:parseFloat($('#formWio input[name="montoWio"]').val());
        montorp1 = isNaN(parseFloat($('#formWio input[name="gmontoReactiva1"]').val()))?0:parseFloat($('#formWio input[name="gmontoReactiva1"]').val());
        monto_para_garantia = monto_wio+montorp1;
        // Swal.fire(monto_para_garantia);
        garantia = getCoberturaMonto(monto_para_garantia);
        $('#formWio input[name="garantia"]').val(garantia*100 + '%');
        $('#formWio input[name="montoGarantizado"]').val('S/. ' + numeral(garantia*monto_para_garantia).format('0,0'));
        $('#formWio input[name="riesgoDescubierto"]').val(numeral((1-garantia)*monto_para_garantia).format('0,0'));
    });

    $(document).on("click",".info",function(){
        $("#modalWioCofide").modal();
        $(".guardarinfo").css("visibility","hidden");
        var ruc = $(this).attr('ruc')
        $.ajax({
            url: "{{ route('cmpEmergencia.buscarinfo') }}",
            type: 'GET',
            data: {
                numruc: ruc
            },
            beforeSend: function() {
                $("#cargando").modal();
            },
            success: function (result) {
                // console.log(result);
                $('#formWio input[name="numruc"]').val(result['NUM_RUC']);
                $('#formWio input[name="cliente"]').val(result['NOMBRE']);
                $('#formWio input[name="telefono"]').val(result['TELEFONO1']);
                $('#formWio input[name="email"]').val(result['CORREO']);
                $('#formWio input[name="riesgosMonto"]').val(numeral(result['RIESGOS_MONTO']).format('0,0'));
                $('#formWio input[name="tieneLinea"]').val(result['WIO_FLAG_TIENE_LINEA']==1?'SI':'NO');
                $('#formWio input[name="subastaDeadline"]').val(result['SUBASTA_DEADLINE']);

                monto_para_riesgos = (result['RIESGOS_MONTO']?parseFloat(result['RIESGOS_MONTO']):0)+(result['MONTO_PRESTAMO_REACTIVA_1']?parseFloat(result['MONTO_PRESTAMO_REACTIVA_1']):0);
                $('#formWio input[name="riesgosCobertura"]').val(getCoberturaMonto(monto_para_riesgos)*100 + '%');
                // if (result['WIO_OBSERVACION'] == "1"){
                //     $('#formWio input[name="volventa"]')
                //         .prop('disabled',false)
                //         .val(result['SOLICITUD_VOLUMEN_VENTA']);
                //     $('#formWio input[name="gmontoReactiva1"]')
                //         .prop('disabled',false)
                //         .val(result['MONTO_PRESTAMO_REACTIVA_1']);
                //     $(".guardarinfo").css("visibility","visible");
                // }else{
                //     $('#formWio input[name="volventa"]')
                //         .prop('disabled',true)
                //         .val(numeral(result['SOLICITUD_VOLUMEN_VENTA']).format('0,0'));
                //     $('#formWio input[name="gmontoReactiva1"]')
                //         .prop('disabled',true)
                //         .val(numeral(result['MONTO_PRESTAMO_REACTIVA_1']).format('0,0'));
                //     $(".guardarinfo").css("visibility","hidden");
                // }

                $('#formWio input[name="montoWio"]').val(numeral(result['WIO_MONTO_SOLICITADO']).format('0,0'));
                if (result['WIO_TASA']) {
                    tasa_wio = result['WIO_TASA'] + '%';
                }else{
                    tasa_wio = "";
                }
                $('#formWio input[name="tasaWio"]').val(tasa_wio);
                if (result['WIO_PLAZO']==null || result['WIO_GRACIA']==null) {
                    $('#formWio input[name="plazoGracia"]').val("");
                }else{
                    $('#formWio input[name="plazoGracia"]').val(result['WIO_PLAZO'] + ' meses /' +result['WIO_GRACIA']+ ' meses');
                }
                monto_para_garantia = (result['WIO_MONTO_SOLICITADO']?parseFloat(result['WIO_MONTO_SOLICITADO']):0)+(result['MONTO_PRESTAMO_REACTIVA_1']?parseFloat(result['MONTO_PRESTAMO_REACTIVA_1']):0);
                garantia = getCoberturaMonto(monto_para_garantia);
                $('#formWio input[name="garantia"]').val(garantia*100 + '%');
                $('#formWio input[name="montoGarantizado"]').val('S/. ' + numeral(garantia*result['WIO_MONTO_SOLICITADO']).format('0,0'));
                $('#formWio input[name="riesgoDescubierto"]').val(numeral((1-garantia)*result['WIO_MONTO_SOLICITADO']).format('0,0'));

                $('#formWio input[name="subastaFecha"]').val(result['FECHA_APROBACIO_SUBASTA']);
                $('#formWio input[name="subastaNumero"]').val(result['NROSUBASTA']);
                if (result['SUBASTA_TASA']) {
                    tasa = (result['SUBASTA_TASA']*100) + '%'
                }else{
                    tasa = "";
                }
                $('#formWio input[name="tasaFinal"]').val(tasa);
                $('#formWio input[name="codigoRepo"]').val(result['SUBASTA_CODIGO_REPO']);
                $('#formWio input[name="wioCobertura"]').val((result['SUBASTA_COBERTURA'] * 100) + '%');

            },
            complete: function() {
                $(".cerrarcargando").click();
            }
        });
    });

    function tablaWIOCOFIDE(datos=null){
        if ($.fn.dataTable.isDataTable('#datatable')) {
            $('#datatable').DataTable().destroy();
        }

        $('#datatable').DataTable({
            destroy:true,
            processing: true,
            "bAutoWidth": false, //ajustar tamaño de vista
            rowId: 'staffId',
            // dom: 'Blfrtip',
            serverSide: true,
            language: {"url": "{{ URL::asset('js/Json/Spanish.json') }}"},
            ajax: {
                "type": "GET",
                "url": "{{ route('cmpEmergencia.lista') }}", //ACA SE ENVÍA LOS DATOS
                data: function(data) {
                    data.etapa = datos;
                }
            },
            "iDisplayLength": 25, //muestra los 25 primeros
            "order": [
                [0, "desc"] //orden de formar desc
            ],
            columnDefs: [
                {
                    targets: 0,
                    data: 'WCL.NUM_RUC',
                    data: 'WCL.NUM_RUC',
                    width: '25%',
                    sortable: true,
                    searchable: false,
                    render: function(data, type, row) {
                        return "<span align='center' >"+row.NUM_RUC+"</span><br><span>"+ row.NOMBRE+"</span>";
                    }
                },
                {
                    targets: 1,
                    data: null,
                    sortable: false,
                    searchable: false,
                    render: function(data, type, row) {
                        mon_sol_wio = row.WIO_MONTO_SOLICITADO?parseFloat(row.WIO_MONTO_SOLICITADO):0;
                        montorp1 = row.MONTO_PRESTAMO_REACTIVA_1?parseFloat(row.MONTO_PRESTAMO_REACTIVA_1):0;
                        mon_para_garantia = mon_sol_wio+montorp1;
                        return "S/. " + numeral(row.WIO_MONTO_SOLICITADO).format('0,0') + ' (' + getCoberturaMonto(mon_para_garantia)*100 + '%)';
                    }
                },
                {
                    targets: 2,
                    data: null,
                    searchable: false,
                    sortable: false,
                    render: function(data, type, row) {
                        return "<span align='left' >Plazo : "+(row.WIO_PLAZO||'0')+" meses<br><span>Gracia : "+(row.WIO_GRACIA||'0')+" meses</span></span>";
                    }
                },
                {
                    targets: 3,
                    data: 'WCL.WIO_TASA',
                    name: 'WCL.WIO_TASA',
                    searchable: false,
                    render: function(data, type, row) {
                        if (row.WIO_TASA) {
                            return row.WIO_TASA+' %';
                        }
                        return '-';
                    }
                },
                {
					targets: 4,
					data: null,
                    searchable: false,
                    sortable: false,
                    width: '20%',
					render: function(data, type, row) {
                        if (row.WIO_OBSERVACION && row.WIO_OBSERVACION!='*') {
                            return "<u><a class='info' style='cursor: pointer' ruc='"+row.NUM_RUC+"'>"+row.WIO_OBSERVACION_TEXTO+"</a></u>";
                        }
                        return "-";
					}
                },
                {
					targets: 5,
					data: 'WCL.WIO_OBSERVACION_FECHA',
                    name: 'WCL.WIO_OBSERVACION_FECHA',
                    searchable: false,
					render: function(data, type, row) {
						return (row.WIO_OBSERVACION_FECHA || '-');
					}
                },
                {
					targets: 6,
					data: null,
                    name: null,
                    searchable: false,
                    sortable: false,
					render: function(data, type, row) {
                        if (row.SUBASTA_DEADLINE) {
                            return '<span style="color: red; font-weight: bold;">' + row.SUBASTA_DEADLINE + '</span>';
                        }
                        return "-";
					}
                }
            ]
        });
    }
</script>
@stop
