@extends('cmpEmergencia.menu')

@section('datable')
<div>
	<table class='table table-striped table-bordered jambo_table' id="datatablecontactados">
		<thead>
			<tr>
				<th>Empresa</th>
				<th>Flujo</th>
				<th>Oferta Solicitada</th>
				<th>Ultima Actividad / Fecha Revisión</th>
				<th>Revisión</th>
				<th>Gestión</th>
				<th>Acciones</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>

@stop

@section('js-vista')
<script>

	$(document).ready(function(){
		$(".nombre_"+"{{$request['etapa']}}").removeClass("btn-success");
		$(".nombre_"+"{{$request['etapa']}}").addClass("btn-danger");
		tablacontactados("{{$request['etapa']}}");
	});

	function tablacontactados(datos=null){
		if ($.fn.dataTable.isDataTable('#datatablecontactados')) {
			$('#datatablecontactados').DataTable().destroy();
		}
	}
</script>
@stop