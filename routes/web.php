<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

/*
  Codigos de ROLES (Ver Entidad Usuario)
  ROL_EJECUTIVO_NEGOCIO = 1;
  ROL_ASISTENTE_COMERCIAL = 2;
  ROL_SOPORTE = 4;
  ROL_CALL = 5;
  ROL_GERENTE_TIENDA = 6;
  ROL_GERENTE_CENTRO = 7;
  ROL_GERENTE_ZONA = 8;
  ROL_JEFE_CALL = 9;

 */


Route::get('/testdb', function () {
  //    return view('telefonos');
  try {
    DB::connection()->getPdo();
    // dd(DB::connection()->getconfig());
    echo ("Conexión Exitosa!");
  } catch (PDOException $e) {
    echo $e->getMessage();
    die("Could not connect to the database.  Please check your configuration.");
  }
});

Route::get('/bienvenida', function () {
  return view('bienvenida');
})->name('bienvenida');

Route::get('/vistasusuario', ['as' => 'clickruta', 'uses' => 'UsuariosController@clickruta']);

Route::group(['prefix' => 'usuarios','middleware' => ['authBase', 'authRol:666']], function () {
  Route::get('/index', ['as' => 'usuarios.index', 'uses' => 'UsuariosController@index']);
  Route::get('/getusuarios', ['as' => 'usuarios.lista', 'uses' => 'UsuariosController@getAllUsuarios']);
  Route::post('/getinfousuario', ['as' => 'usuarios.getinfousuario', 'uses' => 'UsuariosController@getinfousuario']);
  Route::get('/addusuario', ['as' => 'usuarios.nuevo', 'uses' => 'UsuariosController@addUsuario']);
  Route::get('/updateusuario', ['as' => 'usuarios.updateusuario', 'uses' => 'UsuariosController@updateUsuario']);
  //Route::get('/excelusuarios', ['as' => 'usuarios.excelusuarios', 'uses' => 'UsuariosController@excelusuarios']);
});

Route::group(['middleware' => ['authBase', 'authRol:20|21|24|23|25|26|29|30|31|52|53|100']], function () {
  Route::get('/gestioncartera', ['as' => 'gestion.cartera', 'uses' => 'GestioncarteraController@index']);
  Route::get('/DetalleClienteEvolutivo', ['as' => 'DetalleClienteEvolutivo', 'uses' => 'GestioncarteraController@DetalleClienteEvolutivo']);
  Route::get('/DetalleClienteFOTO', ['as' => 'DetalleClienteFOTO', 'uses' => 'GestioncarteraController@DetalleClienteFOTO']);
  Route::get('/cumplimiento', ['as' => 'cumplimiento', 'uses' => 'GestioncarteraController@getCumplimiento']);
  // Route::post('/gestioncartera/jefatura', ['as' => 'gestioncartera.jefatura', 'uses' => 'GestioncarteraController@jefatura']);
  Route::get('/coldirectas', ['as' => 'coldirectas', 'uses' => 'GestioncarteraController@coldirectas']);
  // Route::post('/colindirectas', ['as' => 'colindirectas', 'uses' => 'GestioncarteraController@colindirectas']);
  // Route::post('/depositos', ['as' => 'depositos', 'uses' => 'GestioncarteraController@depositos']);
  Route::get('/VarXProducto', ['as' => 'VarXProducto', 'uses' => 'GestioncarteraController@VarXProducto']);
  Route::get('/getUsuario', ['as' => 'getUsuario', 'uses' => 'GestioncarteraController@getUsuario']);
  Route::get('/getZonales', ['as' => 'getZonales', 'uses' => 'GestioncarteraController@getZonales']);
  Route::get('/getJefes', ['as' => 'getJefes', 'uses' => 'GestioncarteraController@getJefes']);
  Route::get('/getEjecutivos', ['as' => 'getEjecutivos', 'uses' => 'GestioncarteraController@getEjecutivos']);
  Route::get('/getTopBottomMERCADO', ['as' => 'getTopBottomMERCADO', 'uses' => 'GestioncarteraController@getTopBottomMERCADO']);
  Route::get('/getTopBtm', ['as' => 'getTopBtm', 'uses' => 'GestioncarteraController@getTopBtm']);
  Route::get('/LineaMercadoTabla', ['as' => 'LineaMercadoTabla', 'uses' => 'GestioncarteraController@LineaMercadoTabla']);
  Route::get('/GraficosBottom', ['as' => 'GraficosBottom', 'uses' => 'GestioncarteraController@GraficosBottom']);
  Route::get('/Descuentos', ['as' => 'Descuentos', 'uses' => 'GestioncarteraController@Descuentos']);
  Route::get('/PAP', ['as' => 'PAP', 'uses' => 'GestioncarteraController@PAP']);
  Route::get('/CSUELDO', ['as' => 'CSUELDO', 'uses' => 'GestioncarteraController@CSUELDO']);
  Route::get('/CUENTASUELDO', ['as' => 'CUENTASUELDO', 'uses' => 'GestioncarteraController@CUENTASUELDO']);
  Route::get('/heatAnalytic', ['as' => 'heatAnalytic', 'uses' => 'GestioncarteraController@heatAnalytic']);
});

Route::group(['middleware' => ['authBase', 'authRol:24|25|30|42|53']], function () {
  Route::get('/resumenHunter', ['as' => 'resumen.hunter', 'uses' => 'ResumenHunterController@index']);
  Route::post('/resumenTopGraphics', ['as' => 'resumen.top.graphics', 'uses' => 'ResumenHunterController@resumenTopGraphics']);
  Route::get('/resumenLeads', ['as' => 'resumen.leads', 'uses' => 'ResumenHunterController@getCantLeads']);
  Route::post('/resumenVisitas', ['as' => 'resumen.ranking.visitas', 'uses' => 'ResumenHunterController@resumenVisitas']);
  Route::post('/resumenEmpresas', ['as' => 'resumen.ranking.empresas', 'uses' => 'ResumenHunterController@getRankingEmpresas']);
  Route::post('/resumenVisitasEliminado', ['as' => 'resumen.ranking.visitas.eliminados', 'uses' => 'ResumenHunterController@resumenVisitasEliminado']);
  Route::post('/resumenEmpresasEliminado', ['as' => 'resumen.ranking.empresas.eliminados', 'uses' => 'ResumenHunterController@getRankingEmpresasEliminado']);
  Route::post('/resumenVisitasFecha', ['as' => 'resumen.ranking.visitas.fecha', 'uses' => 'ResumenHunterController@resumenVisitasFecha']);
});


Route::group(['middleware' => ['authBase', 'authRol:10|11|12|20|21|24|23|25|26|29|30|31|32|33|34|35|43|36|37|52|53|42|49|100']], function () {
  Route::post('/DetalleClienteEvolutivo', ['as' => 'DetalleClienteEvolutivo', 'uses' => 'GestioncarteraController@DetalleClienteEvolutivo']);
  Route::post('/DetalleClienteFOTO', ['as' => 'DetalleClienteFOTO', 'uses' => 'GestioncarteraController@DetalleClienteFOTO']);
  Route::post('/cumplimiento', ['as' => 'cumplimiento', 'uses' => 'GestioncarteraController@getCumplimiento']);
  Route::post('/coldirectas', ['as' => 'coldirectas', 'uses' => 'GestioncarteraController@coldirectas']);
  Route::post('/colindirectas', ['as' => 'colindirectas', 'uses' => 'GestioncarteraController@colindirectas']);
  Route::post('/depositos', ['as' => 'depositos', 'uses' => 'GestioncarteraController@depositos']);
  Route::post('/VarXProducto', ['as' => 'VarXProducto', 'uses' => 'GestioncarteraController@VarXProducto']);
  Route::post('/getUsuario', ['as' => 'getUsuario', 'uses' => 'GestioncarteraController@getUsuario']);
  Route::post('/getZonales', ['as' => 'getZonales', 'uses' => 'GestioncarteraController@getZonales']);
  Route::post('/getJefes', ['as' => 'getJefes', 'uses' => 'GestioncarteraController@getJefes']);
  Route::post('/getEjecutivos', ['as' => 'getEjecutivos', 'uses' => 'GestioncarteraController@getEjecutivos']);
  Route::post('/getTopBottomMERCADO', ['as' => 'getTopBottomMERCADO', 'uses' => 'GestioncarteraController@getTopBottomMERCADO']);
  Route::post('/getTopBtm', ['as' => 'getTopBtm', 'uses' => 'GestioncarteraController@getTopBtm']);
  Route::post('/LineaMercadoTabla', ['as' => 'LineaMercadoTabla', 'uses' => 'GestioncarteraController@LineaMercadoTabla']);
  Route::post('/GraficosBottom', ['as' => 'GraficosBottom', 'uses' => 'GestioncarteraController@GraficosBottom']);
  Route::post('/Descuentos', ['as' => 'Descuentos', 'uses' => 'GestioncarteraController@Descuentos']);
  Route::post('/PAP', ['as' => 'PAP', 'uses' => 'GestioncarteraController@PAP']);
  Route::post('/CSUELDO', ['as' => 'CSUELDO', 'uses' => 'GestioncarteraController@CSUELDO']);
  Route::post('/CUENTASUELDO', ['as' => 'CUENTASUELDO', 'uses' => 'GestioncarteraController@CUENTASUELDO']);
});
Route::post('/heatAnalytic', ['as' => 'heatAnalytic', 'uses' => 'HeatAnalyticController@heatAnalytic']);


Route::group(['middleware' => ['authBase', 'authRol:10|20|52|21|24|23|25|26|29|30|31|32|37|39|40|36|100|70|12|91|49']], function () {
  Route::get('/Reportes', ['as' => 'reportes', 'uses' => 'ReportesController@index']);
  Route::post('/actualizar', ['as' => 'actualizar.reportes', 'uses' => 'ReportesController@actualizar']);
  Route::post('/datosdescarga', ['as' => 'datosdescarga', 'uses' => 'ReportesController@datosdescarga']);
  Route::get('/Chatbotvicky', ['as' => 'chatbotvicky', 'uses' => 'GestioncarteraController@indexchatbot']);
});

Route::get('GestionLeads', 'InicioController@lCargaTablaLeadsog');
Route::get('/cima', ['as' => 'cimachatbot', 'uses' => 'CimaController@index']);

/* Rutas públicas */
Route::get('/', ['as' => 'login.index', 'uses' => 'LoginController@index']);
Route::post('/updateVisto', ['as' => 'updateVisto', 'uses' => 'NotificationController@updateVisto']);
Route::get('/updateVistoURL', ['as' => 'updateVistoURL', 'uses' => 'NotificationController@updateVistoURL']);
Route::post('/getNotifications', ['as' => 'getNotifications', 'uses' => 'NotificationController@getNotifications']);
Route::post('/getNotificationsTOT', ['as' => 'getNotificationsTOT', 'uses' => 'NotificationController@getNotificationsTOT']);
Route::get('/logout', ['as' => 'logout', 'uses' => 'LoginController@logout']);
Route::get('/notifications', ['as' => 'notifications', 'uses' => 'NotificationController@index']);
Route::post('/login', ['as' => 'login.attempt', 'uses' => 'LoginController@attempt']);

/* Tipos de Login : COMPLETAR */
Route::get('/demo-login', ['as' => '', 'uses' => 'LoginController@demoLogin']);
Route::post('/demo-attempt', ['as' => '', 'uses' => 'LoginController@demoAttempt']);

/* RUTAS RECUPERACION */
Route::get('/pass-gen', ['as' => 'pass.gen', 'uses' => 'PassController@gen']);
Route::post('/pass-save', ['as' => 'pass.save', 'uses' => 'PassController@save']);
Route::get('/extra-login', ['as' => 'pass.login', 'uses' => 'PassController@login']);
Route::post('/extra-attempt', ['as' => 'pass.attempt', 'uses' => 'PassController@attempt']);


Route::get('/update-massive', ['as' => '', 'uses' => 'ToolsController@updateMasive']);


//RUTA GET PARA MOSTRAR FORMULARIO
Route::get('/pass-change', ['as' => 'pass.change', 'uses' => 'PassController@updatePassForm']);

//RUTA POST PARA ACTUALIZAR CONTRASEÑA
Route::post('/pass-update', ['as' => 'pass.update', 'uses' => 'PassController@updatePassInDatabase']);

/* Rutas usuarios logeados */
Route::group(['middleware' => ['authBase']], function () {
  Route::get('/home', ['as' => 'home.index', 'uses' => 'HomeController@index']);
  Route::get('/bpe/campanha/validator/horarioEjecutivo', ['as' => 'bpe.campanha.validator.horarioEjecutivo', 'uses' => 'BPE\Campanhas\ValidatorController@horarioEjecutivo']);
  Route::get('/bpe/campanha/utils/get-motivo-by-resultado', ['as' => 'bpe.campanha.utils.getMotivoByResultado', 'uses' => 'BPE\Campanhas\UtilsController@getMotivoByResultado']);
  Route::get('/bpe/campanha/utils/get-tiendas-by-centro', ['as' => 'bpe.campanha.utils.getTiendasByCentro', 'uses' => 'BPE\Campanhas\UtilsController@getTiendasByCentro']);
  Route::get('/bpe/campanha/utils/get-centros-by-zonal', ['as' => 'bpe.campanha.utils.getCentrosByZonal', 'uses' => 'BPE\Campanhas\UtilsController@getCentrosByZonal']);
  Route::get('/bpe/campanha/utils/get-ejecutivos-by-tienda', ['as' => 'bpe.campanha.utils.getEjecutivosByTienda', 'uses' => 'BPE\Campanhas\UtilsController@getEjecutivosByTienda']);
  Route::post('/bpe/registrar-feedback', ['as' => 'bpe.campanha.utils.registrarFeedbackTelefono', 'uses' => 'BPE\Campanhas\UtilsController@registrarFeedbackTelefono']);
  Route::post('/bpe/quitar-feedback', ['as' => 'bpe.campanha.utils.quitarFeedbackTelefono', 'uses' => 'BPE\Campanhas\UtilsController@quitarFeedbackTelefono']);

  Route::get('/utils/get-provincias-by-departamento', ['as' => 'bpe.campanha.utils.getProvinciasByDepartamento', 'uses' => 'BPE\Campanhas\UtilsController@getProvinciasByDepartamento']);
  Route::get('/utils/get-distritos-by-provincia', ['as' => 'bpe.campanha.utils.getDistritosByProvincia', 'uses' => 'BPE\Campanhas\UtilsController@getDistritosByProvincia']);
});

Route::group(['middleware' => ['authBase', 'authRol:1|2|3|4|6|7|8|10|54']], function () {
  Route::get('/bpe/campanha/consulta-nuevos', ['as' => 'bpe.campanha.consulta-nuevos', 'uses' => 'BPE\Campanhas\ConsultaNuevosController@index']);
});

/* Rutas para Asistente Comercial */
Route::group(['prefix' => 'bpe/ac', 'middleware' => ['authBase', 'authRol:2']], function () {
  Route::get('leads', ['as' => 'bpe.campanha.asistente.leads.listar', 'uses' => 'BPE\Campanhas\Asistente\LeadsController@listar']);
  Route::get('cita/nuevo', ['as' => 'bpe.campanha.asistente.cita.nuevo', 'uses' => 'BPE\Campanhas\Asistente\CitaController@nuevo']);
  Route::post('cita/registrar', ['as' => 'bpe.campanha.asistente.cita.registrar', 'uses' => 'BPE\Campanhas\Asistente\CitaController@registrar']);
  Route::post('cita/registrar-gestion', ['as' => 'bpe.campanha.asistente.cita.registrarGestion', 'uses' => 'BPE\Campanhas\Asistente\CitaController@registrarGestion']);
});

/* Rutas para Ejecutivo de Negocio */
Route::group(['prefix' => 'bpe/en', 'middleware' => ['authBase', 'authRol:1|80']], function () {
  Route::post('actualizarlead',['as' => 'bpe.campanha.ejecutivo.actualizarlead','uses' => 'BPE\Campanhas\Ejecutivo\LeadsController@actualizarlead']);
  Route::get('leads', ['as' => 'bpe.campanha.ejecutivo.leads.listar', 'uses' => 'BPE\Campanhas\Ejecutivo\LeadsController@listar']);
  Route::get('detalle', ['as' => 'bpe.campanha.ejecutivo.leads.detalle', 'uses' => 'BPE\Campanhas\Ejecutivo\LeadsController@detalle']);
  Route::post('nuevo-contacto', ['as' => 'bpe.campanha.ejecutivo.leads.nuevo-contacto', 'uses' => 'BPE\Campanhas\Ejecutivo\LeadsController@nuevoContacto']);
  Route::post('reprogramar-cita', ['as' => 'bpe.campanha.ejecutivo.leads.reprogramar-cita', 'uses' => 'BPE\Campanhas\Ejecutivo\LeadsController@reprogramarCita']);
  Route::post('nueva-gestion', ['as' => 'bpe.campanha.ejecutivo.leads.nueva-gestion', 'uses' => 'BPE\Campanhas\Ejecutivo\LeadsController@nuevaGestion']);
  Route::post('update-etiqueta', ['as' => 'bpe.campanha.ejecutivo.leads.update-etiqueta', 'uses' => 'BPE\Campanhas\Ejecutivo\LeadsController@updateEtiqueta']);
  Route::post('enviar-asistente', ['as' => 'bpe.campanha.ejecutivo.leads.enviar-asistente', 'uses' => 'BPE\Campanhas\Ejecutivo\LeadsController@enviarAsistente']);
  Route::get('getpropensiones', ['as' => 'bpe.campanha.getpropensiones', 'uses' => 'BPE\Campanhas\Ejecutivo\LeadsController@getPropensiones']);

  //Para el Plan Cartera
  /*
    Route::get('cartera', ['as' => 'bpe.campanha.ejecutivo.clientes.cartera', 'uses' => 'BPE\Campanhas\Ejecutivo\CarteraController@cartera']);
    Route::get('cartera-detalle', ['as' => 'bpe.campanha.ejecutivo.clientes.cartera-detalle', 'uses' => 'BPE\Campanhas\Ejecutivo\CarteraController@carteraDetalle']);*/
  Route::post('cambiar-campanha', ['as' => 'bpe.campanha.ejecutivo.clientes.cambiar-campanha', 'uses' => 'BPE\Campanhas\Ejecutivo\CarteraController@cambioCampanha']);
  Route::post('nueva-gestion-cartera', ['as' => 'bpe.campanha.ejecutivo.clientes.nueva-gestion', 'uses' => 'BPE\Campanhas\Ejecutivo\CarteraController@nuevaGestion']);
});

/*Valorades*/
Route::group(['prefix' => 'bpe/en', 'middleware' => ['authBase', 'authRol:1|6|7|8|10']], function () {
  Route::get('pagarepdf', ['as' => 'bpe.campanha.ejecutivo.pagarepdf', 'uses' => 'BPE\Campanhas\Ejecutivo\LeadsController@pagarepdf']);
  Route::get('llenadopdf', ['as' => 'bpe.campanha.ejecutivo.llenadopdf', 'uses' => 'BPE\Campanhas\Ejecutivo\LeadsController@llenadopdf']);
  Route::get('ddjjfondocrecerpdf', ['as' => 'bpe.campanha.ejecutivo.ddjjpdf', 'uses' => 'BPE\Campanhas\Ejecutivo\LeadsController@ddjjfondocrecerpdf']);
  Route::get('cartainstruccionpdf', ['as' => 'bpe.campanha.ejecutivo.cartainstruccionpdf', 'uses' => 'BPE\Campanhas\Ejecutivo\LeadsController@cartainstruccionpdf']);
});

/* Rutas para Gerentes */
Route::group(['prefix' => 'bpe/gt', 'middleware' => ['authBase', 'authRol:6|7|8|10']], function () {
  Route::get('resumen-ejecutivos', ['as' => 'bpe.campanha.gerente.ejecutivo.resumen', 'uses' => 'BPE\Campanhas\Gerente\EjecutivoController@resumen']);
  Route::get('detalle-ejecutivos', ['as' => 'bpe.campanha.gerente.ejecutivo.detalle', 'uses' => 'BPE\Campanhas\Gerente\EjecutivoController@detalle']);
  Route::get('detalle-lead', ['as' => 'bpe.campanha.gerente.ejecutivo.detallelead', 'uses' => 'BPE\Campanhas\Ejecutivo\LeadsController@detalle']);
});

/* Rutas para Call */
Route::group(['prefix' => 'bpe/call', 'middleware' => ['authBase', 'authRol:5']], function () {
  Route::get('nuevo', ['as' => 'bpe.campanha.call.cita.nuevo', 'uses' => 'BPE\Campanhas\Call\CitaController@nuevo']);
  Route::post('guardar', ['as' => 'bpe.campanha.call.cita.guardar', 'uses' => 'BPE\Campanhas\Call\CitaController@guardar']);
});


/* Rutas para Ejecutivo de Soporte */
Route::group(['prefix' => 'bpe/soporte/', 'middleware' => ['authBase', 'authRol:4|7|8|10']], function () {
  Route::get('asignacion', ['as' => 'bpe.campanha.soporte.asignar.index', 'uses' => 'BPE\Campanhas\Soporte\AsignarController@index']);
  Route::post('asignar', ['as' => 'bpe.campanha.soporte.asignar.asignar', 'uses' => 'BPE\Campanhas\Soporte\AsignarController@asignar']);
  Route::get('consultar-lead', ['as' => 'bpe.soporte.consultar', 'uses' => 'BPE\Campanhas\Soporte\AsignarController@consultar']);
  Route::get('consultar-ejecutivo', ['as' => 'busqueda', 'uses' => 'BPE\Campanhas\Soporte\AsignarController@consultarEN']);
});

/* Rutas para reaisgnacion de Leads */
Route::group(['prefix' => 'bpe/tools', 'middleware' => ['authBase', 'authRol:7|8|9|10']], function () {
  Route::get('reasignacion', ['as' => 'bpe.campanha.gerente.reasignacion.index', 'uses' => 'BPE\Campanhas\Gerente\ReasignarController@index']);
  Route::post('reasignar', ['as' => 'bpe.campanha.gerente.reasignar.reasignar', 'uses' => 'BPE\Campanhas\Gerente\ReasignarController@reasignar']);
  Route::get('consultar-lead', ['as' => 'bpe.campanha.gerente.reasignar.consultar-lead', 'uses' => 'BPE\Campanhas\Gerente\ReasignarController@consultarLead']);
  Route::get('consultar-horario-ejecutivo', ['as' => 'bpe.campanha.gerente.reasignar.consultar-horario-ejecutivo', 'uses' => 'BPE\Campanhas\Gerente\ReasignarController@getHorarioEN']);
  Route::get('consultar-ejecutivo', ['as' => 'bpe.campanha.gerente.reaisgnar.consultar-ejecutivo', 'uses' => 'BPE\Campanhas\Gerente\ReasignarController@consultarEN']);
});

Route::group(['prefix' => 'bpe/tools', 'middleware' => ['authBase', 'authRol:9|10']], function () {
  Route::get('resumen-call', ['as' => 'bpe.campanha.herramientas.resumen-call', 'uses' => 'BPE\Campanhas\Herramientas\CitasController@resumenCallIndex']);
});

Route::group(['prefix' => 'bpe/tools', 'middleware' => ['authBase', 'authRol:6|7|8|9|10']], function () {
  Route::get('resumen-citas', ['as' => 'bpe.campanha.herramientas.resumen-citas', 'uses' => 'BPE\Campanhas\Herramientas\CitasController@resumenCitasIndex']);
});

/*Listado de Citas para jefes zonales y de centro*/
Route::group(['prefix' => 'bpe/jefe', 'middleware' => ['authBase', 'authRol:7|8']], function () {
  Route::get('/citas-reporte', ['as' => 'bpe.jefe.citas-reporte', 'uses' => 'BPE\Campanhas\Gerente\CitasController@citasReporte']);
  Route::get('/citas-data', ['as' => 'bpe.jefe.citas-data', 'uses' => 'BPE\Campanhas\Gerente\CitasController@citasData']);
});


/* Otras rutas compartidas */
Route::group(['prefix' => 'bpe/en', 'middleware' => ['authBase', 'authRol:1|7|8|9|10']], function () {
  Route::get('imprimir', ['as' => 'bpe.campanha.ejecutivo.leads.imprimir', 'uses' => 'BPE\Campanhas\Ejecutivo\LeadsController@imprimir']);
});

//Para el plan Cartera (Ejecutivos y gerentes)
Route::group(['prefix' => 'bpe/cli', 'middleware' => ['authBase', 'authRol:1|2|6|7|8|9|10|80']], function () {
  Route::get('cartera', ['as' => 'bpe.campanha.ejecutivo.clientes.cartera', 'uses' => 'BPE\Campanhas\Ejecutivo\CarteraController@cartera']);
  Route::get('cartera-detalle', ['as' => 'bpe.campanha.ejecutivo.clientes.cartera-detalle', 'uses' => 'BPE\Campanhas\Ejecutivo\CarteraController@carteraDetalle']);
  Route::get('imprimir', ['as' => 'bpe.campanha..ejecutivo.cartera.imprimir', 'uses' => 'BPE\Campanhas\Ejecutivo\CarteraController@imprimir']);
});

//vista asignacion
//Route::get('/reasignacion',['as'=>'busqueda', 'uses'=>'BPE\Campanhas\Soporte\AsignarController@index'] );
Route::get('/getHorarioEN', ['as' => 'busqueda', 'uses' => 'BPE\Campanhas\Soporte\AsignarController@getHorarioEN']);

/* Route::get('/reassignacion', function () {
  return view('bpe\campanhas\Soporte\Asignacion-Leads');
  }); */

//FIES
Route::group(['prefix' => 'fies/ejecutivo', 'middleware' => ['authBase', 'authRol:11|12']], function () {
  //lista
  Route::get('operaciones', ['as' => 'fies.operaciones.ejecutivo.operaciones.listar', 'uses' => 'FIES\operaciones\ejecutivo\OperacionesController@listar']);
  Route::get('registrar', ['as' => 'fies.operaciones.ejecutivo.operaciones.registro', 'uses' => 'FIES\operaciones\ejecutivo\OperacionesController@registrarOperacion']);

  //detalle
  Route::get('mostrarDatos-operacion', ['as' => 'fies.operaciones.ejecutivo.operaciones.detalle', 'uses' => 'FIES\operaciones\ejecutivo\OperacionesController@mostrarDatosOperacion']);
  Route::post('informacion-operacion', ['as' => 'fies.operaciones.ejecutivo.operaciones.informacion', 'uses' => 'FIES\operaciones\ejecutivo\OperacionesController@guardarDetalleGeneral']);
  Route::post('actualiza-producto', ['as' => 'fies.operaciones.ejecutivo.operaciones.producto', 'uses' => 'FIES\operaciones\ejecutivo\OperacionesController@guardarProducto']);
  // estaciones
  Route::post('actualiza-estacion-pipeline', ['as' => 'fies.operaciones.ejecutivo.operaciones.pipeline', 'uses' => 'FIES\operaciones\ejecutivo\OperacionesController@guardarPipeline']);
  Route::post('actualiza-estacion-cotizacion', ['as' => 'fies.operaciones.ejecutivo.operaciones.cotizacion', 'uses' => 'FIES\operaciones\ejecutivo\OperacionesController@guardarCotizacion']);
  Route::post('actualiza-estacion-fies', ['as' => 'fies.operaciones.ejecutivo.operaciones.fies', 'uses' => 'FIES\operaciones\ejecutivo\OperacionesController@guardarFies']);
  Route::post('actualiza-estacion-riesgos', ['as' => 'fies.operaciones.ejecutivo.operaciones.riesgos', 'uses' => 'FIES\operaciones\ejecutivo\OperacionesController@guardarRiesgos']);
  Route::post('actualiza-estacion-aprobado', ['as' => 'fies.operaciones.ejecutivo.operaciones.aprobado', 'uses' => 'FIES\operaciones\ejecutivo\OperacionesController@guardarAprobado']);

  Route::get('cerrarEstacion', ['as' => 'fies.operaciones.ejecutivo.operaciones.cerrarEstacion', 'uses' => 'FIES\operaciones\ejecutivo\OperacionesController@cerrarEstacion']);

  //cronograma
  Route::get('consultar-cliente', ['as' => 'fies.operaciones.ejecutivo.operaciones.consultar-cliente', 'uses' => 'FIES\operaciones\ejecutivo\OperacionesController@ConsultaCliente']);
  Route::get('mostrar-cronograma', ['as' => 'fies.operaciones.ejecutivo.operaciones.mostrar-cronograma', 'uses' => 'FIES\operaciones\ejecutivo\OperacionesController@mostrarCronograma']);
  Route::post('cargar-cronograma', ['as' => 'fies.operaciones.ejecutivo.operaciones.cargar-cronograma', 'uses' => 'FIES\operaciones\ejecutivo\OperacionesController@agregarCuota']);
  Route::get('eliminar-cuota', ['as' => 'fies.operaciones.ejecutivo.operaciones.eliminar-cuota', 'uses' => 'FIES\operaciones\ejecutivo\OperacionesController@eliminarCuota']);
  Route::post('actualizar-cuota', ['as' => 'fies.operaciones.ejecutivo.operaciones.actualizar-cuota', 'uses' => 'FIES\operaciones\ejecutivo\OperacionesController@actualizarCuota']);
  Route::get('actualizar-Operacion', ['as' => 'fies.operaciones.ejecutivo.operaciones.actualizar-Operacion', 'uses' => 'FIES\operaciones\ejecutivo\OperacionesController@actualizarOperacionProducto']);
  Route::get('lista-meses', ['as' => 'fies.operaciones.ejecutivo.operaciones.lista-meses', 'uses' => 'FIES\operaciones\ejecutivo\OperacionesController@listaMeses']);
  Route::post('registrar-operacion', ['as' => 'fies.operaciones.ejecutivo.operaciones.registrar-operacion', 'uses' => 'FIES\operaciones\ejecutivo\OperacionesController@setOperacion']);
});



/* * ************************************************************************** */
/* * *********************** RUTAS BANCA EMPRESA ****************************** */
/* * ************************************************************************** */

Route::group(['prefix' => 'be'], function () {

  // MI PROSPECTO
  Route::get('/miprospecto/lista', ['as' => 'be.miprospecto.lista.index', 'uses' => 'BE\MiProspectoController@index']);
  Route::post('/miprospecto/update-etapa', ['as' => 'be.miprospecto.update-etapa', 'uses' => 'BE\MiProspectoController@updateEtapa']);
  Route::get('/miprospecto/consulta-referido', ['as' => 'be.miprospecto.consulta-referido', 'uses' => 'BE\MiProspectoController@consultaReferido']);
  Route::post('/miprospecto/consulta-referido-post', ['as' => 'be.miprospecto.consulta-referido-post', 'uses' => 'BE\MiProspectoController@consultaReferido']);
  Route::post('/miprospecto/registro-referido', ['as' => 'be.miprospecto.registro-referido', 'uses' => 'BE\MiProspectoController@registroReferido']);
  Route::post('/miprospecto/eliminar', ['as' => 'be.miprospecto.eliminar', 'uses' => 'BE\MiProspectoController@eliminarProspecto']);
  Route::post('/micontacto/mantener-lead', ['as' => 'be.miprospecto.mantener-lead', 'uses' => 'BE\MiProspectoController@mantenerLead']);

  Route::get('/reasignaciones', ['as' => 'be.reasignaciones', 'uses' => 'BE\MiProspectoController@reasignaciones', 'middleware' => ['authBase', 'authRol:23']]);
  Route::post('/reasignaciones/buscar', ['as' => 'be.reasignaciones.buscar', 'uses' => 'BE\MiProspectoController@getLeadsAsignadas', 'middleware' => ['authBase', 'authRol:23']]);

  //MIS ACCIONES COMERCIALES
  Route::get('/misacciones/lista', ['as' => 'be.misacciones.lista.index', 'uses' => 'BE\AccionesComercialesController@index']);
  Route::get('/misacciones/consulta-cliente', ['as' => 'be.misacciones.consulta-cliente', 'uses' => 'BE\AccionesComercialesController@consultaCliente']);
  Route::post('/misacciones/registro-accion', ['as' => 'be.misacciones.registro-accion', 'uses' => 'BE\AccionesComercialesController@registroAccion']);
  Route::post('/misacciones/update-etapa', ['as' => 'be.misacciones.update-etapa', 'uses' => 'BE\AccionesComercialesController@updateEtapa']);
  Route::post('/misacciones/update-mes', ['as' => 'be.misacciones.update-mes', 'uses' => 'BE\AccionesComercialesController@updateMesActiv']);
  Route::post('/misacciones/update-kpi', ['as' => 'be.misacciones.update-kpi', 'uses' => 'BE\AccionesComercialesController@updateKPI']);
  Route::post('/misacciones/update-fechafin', ['as' => 'be.misacciones.update-fechafin', 'uses' => 'BE\AccionesComercialesController@updateFechaFin']);
  Route::get('/misacciones/nota/listar', ['as' => 'be.misacciones.nota.listar', 'uses' => 'BE\AccionesComercialesController@listarNotas']);
  Route::post('/misacciones/nota/agregar', ['as' => 'be.misacciones.nota.agregar', 'uses' => 'BE\AccionesComercialesController@agregarNota']);
  Route::post('/misacciones/nota/eliminar', ['as' => 'be.misacciones.nota.eliminar', 'uses' => 'BE\AccionesComercialesController@eliminarNota']);
  Route::post('/misacciones/eliminar', ['as' => 'be.misacciones.eliminar', 'uses' => 'BE\AccionesComercialesController@eliminarAccion']);
  Route::post('/misacciones/update-estrella', ['as' => 'be.misacciones.update-estrella', 'uses' => 'BE\AccionesComercialesController@updateEstrella']);
  Route::get('/misacciones/acciones-cliente', ['as' => 'be.misacciones.acciones-cliente', 'uses' => 'BE\AccionesComercialesController@getAccionesCliente']);
  Route::get('/misacciones/acciones-ingresar', ['as' => 'be.misacciones.acciones-ingresar', 'uses' => 'BE\AccionesComercialesController@listaIngresar']);
  // MI CONTACTO
  Route::post('/micontacto/update-datos', ['as' => 'be.micontacto.update-datos', 'uses' => 'BE\MiContactoController@updateDatos']);
  Route::get('/micontacto/quitar-contacto', ['as' => 'be.micontacto.quitar-contacto', 'uses' => 'BE\MiContactoController@quitarContacto']);
  Route::post('/micontacto/agregar-contacto', ['as' => 'be.micontacto.agregar-contacto', 'uses' => 'BE\MiContactoController@agregarContacto']);
  Route::post('/micontacto/update-contacto', ['as' => 'be.micontacto.update-contacto', 'uses' => 'BE\MiContactoController@updateContacto']);
  Route::post('/micontacto/add-feedback', ['as' => 'be.micontacto.add-feedback', 'uses' => 'BE\MiContactoController@addFeedback']);
  Route::post('/micontacto/quitar-feedback', ['as' => 'be.micontacto.quitar-feedback', 'uses' => 'BE\MiContactoController@quitarFeedback']);
  Route::post('/micontacto/add-contacto-data', ['as' => 'be.micontacto.add-contacto-data', 'uses' => 'BE\MiContactoController@addContactoData']);
  Route::post('/micontacto/set-contacto-tipo', ['as' => 'be.micontacto.set-contacto-tipo', 'uses' => 'BE\MiContactoController@setContactoTipo']);
  Route::post('/micontacto/set-contacto-encuesta', ['as' => 'be.micontacto.set-contacto-encuesta', 'uses' => 'BE\MiContactoController@setContactoEncuesta']);


  //ACTIVIDADES

  Route::get('/actividades', ['as' => 'be.actividades.index', 'uses' => 'BE\ActividadesController@index']);
  Route::get('/actividades/autocomplete-cliente', ['as' => 'be.actividades.autocomplete-cliente', 'uses' => 'BE\ActividadesController@autocomplete']);
  Route::get('/actividades/autocomplete-participante', ['as' => 'be.actividades.autocomplete-participante', 'uses' => 'BE\ActividadesController@autocompleteParticipantes']);
  Route::post('/actividades/agregar', ['as' => 'be.actividades.agregar', 'uses' => 'BE\ActividadesController@agregarActividad']);
  Route::get('/actividades/get', ['as' => 'be.actividades.get', 'uses' => 'BE\ActividadesController@get']);
  Route::get('/actividades/historialActividad', ['as' => 'be.actividades.historialActividad', 'uses' => 'BE\ActividadesController@historialActividades']);
  Route::post('/actividades/confirmar', ['as' => 'be.actividades.confirmar', 'uses' => 'BE\ActividadesController@confirmarEliminar']);
  Route::post('/actividades/updateVerificado', ['as' => 'be.actividades.update.verificado', 'uses' => 'BE\ActividadesController@updateVerificado']);
  //DETALLE VOLUMEN PAP
  Route::post('/detalle/volumenpap', ['as' => 'be.detalle.volumenpap', 'uses' => 'BE\MiProspectoController@detalleVpap']);

  //MORA
  Route::get('/creditosvencidos', ['as' => 'be.creditos.index', 'uses' => 'BE\InfiniteController@index']);
  Route::get('/Zonales', ['as' => 'be.creditos.getZonal', 'uses' => 'BE\InfiniteController@getZonal']);
  Route::get('/Ejecutivos', ['as' => 'be.creditos.getEjecutivo', 'uses' => 'BE\InfiniteController@getEjecutivo']);
  Route::post('/guardarGestion', ['as' => 'be.creditos.guardarGestion', 'uses' => 'BE\InfiniteController@guardarGestion']);

  //NOTAS
  Route::get('/miprospecto/nota/listar', ['as' => 'be.miprospecto.nota.listar', 'uses' => 'BE\MiProspectoController@listarNotas']);


  //ENOTE
  Route::get('/enote', ['as' => 'be.enote', 'uses' => 'BE\EnoteController@index']);
  Route::get('/enote/resumen', ['as' => 'be.enote.resumen', 'uses' => 'BE\EnoteController@resumen']);
  Route::get('/enote/get-amortizaciones', ['as' => 'be.enote.get-amortizaciones', 'uses' => 'BE\EnoteController@getAmortizaciones']);
  Route::get('/enote/get-vencimientos', ['as' => 'be.enote.get-vencimientos', 'uses' => 'BE\EnoteController@getVencimientos']);
  Route::get('/enote/get-operaciones', ['as' => 'be.enote.get-operaciones', 'uses' => 'BE\EnoteController@getOperaciones']);
  Route::post('/enote/update-venc-amort', ['as' => 'be.enote.update-venc-amort', 'uses' => 'BE\EnoteController@updateVencAmort']);
  Route::post('/enote/update-operaciones', ['as' => 'be.enote.update-operaciones', 'uses' => 'BE\EnoteController@updateOperaciones']);

  Route::get('/enote/autocomplet', ['as' => 'be.enote.autocomplet', 'uses' => 'BE\EnoteController@autocomplete']);
  Route::get('/enote/buscar-cliente', ['as' => 'be.enote.buscar-cliente', 'uses' => 'BE\EnoteController@buscarCliente']);
  Route::post('/enote/guardar-operacion', ['as' => 'be.enote.guardar-operacion', 'uses' => 'BE\EnoteController@guardarOperacion']);
  Route::post('/enote/operacion-nueva/registro', ['as' => 'be.enote.operacion-nueva.registro', 'uses' => 'BE\EnoteController@insertOperacion']);
  Route::get('/enote/getColDirecta', ['as' => 'be.enote.getColDirecta', 'uses' => 'BE\EnoteController@getColDirectas']);
  Route::get('/enote/getColIndirecta', ['as' => 'be.enote.getColIndirecta', 'uses' => 'BE\EnoteController@getColIndirectas']);

  Route::get('/enote/listar-notas', ['as' => 'be.enote.listar-notas', 'uses' => 'BE\EnoteController@listarNotas']);
  Route::post('/enote/agregar-nota', ['as' => 'be.enote.agregar-nota', 'uses' => 'BE\EnoteController@agregarNota']);
  Route::post('/enote/eliminar-nota', ['as' => 'be.enote.eliminar-nota', 'uses' => 'BE\EnoteController@eliminarNota']);

  Route::get('/enote/graficar', ['as' => 'be.enote.graficar', 'uses' => 'BE\EnoteController@graficar']);

  //ENOTE JEFES
  Route::get('/enote-jefe-menu', ['as' => 'be.enote-jefe.menu', 'uses' => 'BE\EnoteJefeController@menu']);
  Route::get('/enote-jefe', ['as' => 'be.enote-jefe', 'uses' => 'BE\EnoteJefeController@index']);
  Route::get('/enote/resumen-jefe', ['as' => 'be.enote.resumen-jefe', 'uses' => 'BE\EnoteJefeController@resumen']);
  Route::get('/enoteJefe/graficar', ['as' => 'be.enoteJefe.graficar', 'uses' => 'BE\EnoteJefeController@graficar']);

  Route::get('/enote-jefe/get-desemb-certeros', ['as' => 'be.enote-jefe.get-desemb-certeros', 'uses' => 'BE\EnoteJefeController@getDesembolsosCerteros']);
  Route::get('/enote-jefe/get-desemb-cotizacion', ['as' => 'be.enote-jefe.get-desemb-cotizacion', 'uses' => 'BE\EnoteJefeController@getDesembolsosCotizacion']);
  Route::get('/enote-jefe/get-caidas-proyectadas', ['as' => 'be.enote-jefe.get-caidas-proyectadas', 'uses' => 'BE\EnoteJefeController@getCaidasProyectadas']);
  Route::get('/enote-jefe/get-operaciones-perdidas', ['as' => 'be.enote-jefe.get-operaciones-perdidas', 'uses' => 'BE\EnoteJefeController@getOperacionesPerdidas']);
  Route::get('/enote-jefe/get-desemb-cot-futuros', ['as' => 'be.enote-jefe.get-desemb-cot-futuros', 'uses' => 'BE\EnoteJefeController@getDesembolsosCotizacionFuturos']);
});


Route::group(['prefix' => 'be', 'middleware' => ['authBase', 'authRol:20|21|22|23|24|25|26|27|28|29|30|31|37|51|52|53|100']], function () {

  //RESUMEN

  Route::get('/resumenbe', ['as' => 'be.resumenbe.index', 'uses' => 'BE\ResumenController@index2']);
  Route::get('/resumen', ['as' => 'be.resumen.index', 'uses' => 'BE\ResumenController@index']);
  Route::get('/utils/get-ejecutivos-by-jefatura', ['as' => 'be.utils.get-ejecutivos-by-jefatura', 'uses' => 'BE\UtilsController@getEjecutivosByJefatura']);
  Route::get('/utils/get-jefaturas-by-zonal', ['as' => 'be.resumen.get-jefaturas-by-zonal', 'uses' => 'BE\UtilsController@getJefaturasByZonal']);
  Route::get('/utils/get-productos-by-zonal', ['as' => 'be.resumen.get-productos-by-zonal', 'uses' => 'BE\UtilsController@getProductosByZonal']);
  Route::get('/utils/get-zonales-by-banca', ['as' => 'be.resumen.get-zonales-by-banca', 'uses' => 'BE\UtilsController@getZonalesByBanca']);
  Route::get('/utils/get-acciones-by-estrategia', ['as' => 'be.utils.get-acciones-by-estrategia', 'uses' => 'BE\UtilsController@getAccionesByEstrategia']);
  Route::get('/utils/get-etapas-by-accion', ['as' => 'be.utils.get-etapas-by-accion', 'uses' => 'BE\UtilsController@getEtapasByAccion']);
  Route::get('/utils/get-arbol-zonales', ['as' => 'be.utils.get-zonales-by-banca', 'uses' => 'BE\UtilsController@getArbolZonales']);
  Route::get('/utils/get-arbol-jefaturas', ['as' => 'be.utils.get-jefaturas-by-banca-zonal', 'uses' => 'BE\UtilsController@getArbolJefaturas']);
  Route::get('/utils/get-arbol-ejecutivos', ['as' => 'be.utils.get-ejecutivos-by-banca-zonal-jefatura', 'uses' => 'BE\UtilsController@getArbolEjecutivos']);
});


//NOTAS
Route::group(['prefix' => 'be', 'middleware' => ['authBase', 'authRol:20|21|22|23|24|31|100']], function () {
  Route::post('/miprospecto/nota/agregar', ['as' => 'be.miprospecto.nota.agregar', 'uses' => 'BE\MiProspectoController@agregarNota']);
  Route::post('/miprospecto/nota/eliminar', ['as' => 'be.miprospecto.nota.eliminar', 'uses' => 'BE\MiProspectoController@eliminarNota']);
});


//MI CONTACTO
Route::group(['prefix' => 'be', 'middleware' => ['authBase', 'authRol:20|21|22|23|24|25|26|27|28|29|30|31|36|37|38|39|40|53|54|999|100']], function () {
  Route::get('/micontacto', ['as' => 'be.micontacto.index', 'uses' => 'BE\MiContactoController@index']);
  Route::get('/micontacto/autocomplete-cliente', ['as' => 'be.micontacto.autocomplete-cliente', 'uses' => 'BE\MiContactoController@autocomplete']);
});


//COPA AMERICA 2019
//Route::group(['prefix' => 'mundial', 'middleware' => ['authBase','authRol:1|2|6|7|8|10|20|21|22|23|24|25|26|30|666']], function() {

Route::group(['prefix' => 'mundial', 'middleware' => ['authBase', 'authRol:1|2|6|7|8|10|11|20|21|22|23|24|25|26|29|30|666|32|28']], function () {
  Route::get('/principal', ['as' => 'mundial.principal', 'uses' => 'MundialController@principal']);
  Route::post('/insertar-resultados', ['as' => 'mundial.insertar', 'uses' => 'MundialController@insertarResultados']);
});

// 10|12|20|21|24|25|26|29|30|32|36|53|100
// 10|12|20|21|23|24|25|26|29|30|32|36|41|53|100
// Route::group(['prefix' => 'encuesta-interna', 'middleware' => ['authBase', 'authRol:1|8|10|12|20|21|23|24|25|666|29|30|31|32|36|37|40|49|52|60|70|100']], function () {
Route::group(['prefix' => 'encuesta-interna', 'middleware' => ['authBase']], function () {
  Route::get('/principal', ['as' => 'encuesta.interna.principal', 'uses' => 'EncuestaInternaController@principal']);

  Route::post('/guardar-resultados', ['as' => 'encuesta.guardar', 'uses' => 'EncuestaInternaController@guardarResultados']);
});


Route::group(['prefix' => 'encuesta', 'middleware' => ['authBase']], function () {
  Route::get('/resumen', ['as' => 'encuesta.interna.resumen', 'uses' => 'EncuestaInternaController@resumen']);
});


Route::group(['prefix' => 'ecosistema', 'middleware' => ['authBase', 'authRol:20|21|22|23|24|25|26|27|28|29|30|31|999|100']], function () {
  Route::get('/principal', ['as' => 'ecosistema.principal', 'uses' => 'EcosistemaController@ecosistemaPrincipal']);
  Route::get('/seguimiento-ingresado', ['as' => 'ecosistema.seguimiento.ingresado', 'uses' => 'EcosistemaController@seguimientoIngresado']);
  Route::get('/seguimiento-ingresado/graficar', ['as' => 'ecosistema.seguimiento.ingresado.graficar', 'uses' => 'EcosistemaController@graficarIngresados']);
  Route::get('/seguimiento-ingresado-status', ['as' => 'ecosistema.seguimiento.ingresado.status', 'uses' => 'EcosistemaController@getStatusProveedores']);
  Route::get('/seguimiento-recibido', ['as' => 'ecosistema.seguimiento.recibido', 'uses' => 'EcosistemaController@seguimientoRecibido']);
  Route::get('/obtener', ['as' => 'obtener.ecosistema', 'uses' => 'EcosistemaController@obtenerEcosistema']);
  Route::get('/obtener-nodo', ['as' => 'obtener.ecosistema.nodo', 'uses' => 'EcosistemaController@obtenerEcosistemaNodo']);
  Route::get('/buscar-ruc', ['as' => 'buscar.ruc.ecosistema', 'uses' => 'EcosistemaController@buscarRucEcosistema']);
  Route::get('/obtener-empresas', ['as' => 'buscar.obtener.empresas', 'uses' => 'EcosistemaController@obtenerEmpresas']);
});


Route::post('/reciprocidad', ['as' => 'reciprocidad', 'uses' => 'EcosistemaController@buscarNumDoc']);
//Route::post('/enviar-email', ['as' => 'ecosistema.email', 'uses' => 'EcosistemaController@enviarMail']);
Route::get('/Reciprocidad2', ['as' => 'Reciprocidad2', 'uses' => 'EcosistemaController@ReciprocidadPrincipal']);
Route::get('/Reciprocidad2/guardarDatos', ['as' => 'Reciprocidad2.guardarDatos', 'uses' => 'EcosistemaController@guardarDatos']);
Route::get('/Reciprocidad2/guardarDatos2', ['as' => 'Reciprocidad2.guardarDatos2', 'uses' => 'EcosistemaController@guardarDatos2']);
Route::get('/Reciprocidad2/Resultados_simular', ['as' => 'Reciprocidad2.Resultados_simular', 'uses' => 'EcosistemaController@Resultados_simular']);
Route::post('/getUsuarioReciprocidad', ['as' => 'getUsuarioReciprocidad', 'uses' => 'EcosistemaController@getUsuarioReciprocidad']);



/*******************
  LINEAS INFINITY ME
 ********************/
Route::group(['prefix' => 'infinity/me', 'middleware' => ['authBase', 'authRol:12|13|20|21|22|23|24|25|26|27|28|29|30|31|33|34|35|43|36|37|38|39|40|40|42|49|50|52|53|100']], function () {
  //Route::get('/resumen', ['as' => 'infinity.me.resumen', 'uses' => 'BE\Infinity\ME\IndexController@resumen']);
  //Route::get('/clientes', ['as' => 'infinity.me.clientes', 'uses' => 'BE\Infinity\ME\IndexController@getListaClientes', 'middleware' => ['authBase', 'authRol:40']]);
  Route::get('/cliente/politicas_comite', ['as' => 'infinity.me.cliente.politicas_comite', 'uses' => 'BE\Infinity\ME\IndexController@PoliticasComiteCliente']);
  //Route::get('/get-icono-alerta', ['as' => 'infinity.me.icono.alerta', 'uses' => 'BE\Infinity\ME\IndexController@getIconoAlerta']);
  //Route::get('/clientes/get', ['as' => 'infinity.me.clientes.get', 'uses' => 'BE\Infinity\ME\IndexController@getListaClientesTable']);
  //Route::get('/cliente/detalle', ['as' => 'infinity.me.cliente.detalle', 'uses' => 'BE\Infinity\ME\IndexController@detalle']);
  Route::get('/cliente/detalle/grafico', ['as' => 'infinity.me.cliente.detalle.grafico', 'uses' => 'BE\Infinity\ME\IndexController@getHistoriaClientes']);
  Route::post('/cliente/detalle/guardar-gestion', ['as' => 'infinity.me.detalle.guardar.gestion', 'uses' => 'BE\Infinity\ME\IndexController@guardarGestion']);
  Route::post('/cliente/detalle/guardar-documentacion', ['as' => 'infinity.me.detalle.guardar.documentacion', 'uses' => 'BE\Infinity\ME\IndexController@guardarDocumentacion']);
  Route::post('/cliente/detalle/guardar-recalculo', ['as' => 'infinity.me.detalle.guardar.recalculo', 'uses' => 'BE\Infinity\ME\IndexController@guardarRecalculo']);

  Route::get('/cliente/covid2/historia',['as' => 'infinity.me.cliente.covid2.historia', 'uses' => 'BE\Infinity\ME\CovidController2@historia']);
  Route::get('/cliente/conoceme', ['as' => 'infinity.me.cliente.conoceme', 'uses' => 'BE\Infinity\ME\ConocemeController@index']);
  Route::post('/cliente/conoceme/guardar', ['as' => 'infinity.me.cliente.conoceme.guardar', 'uses' => 'BE\Infinity\ME\ConocemeController@guardar']);

  Route::get('/cliente/visita', ['as' => 'infinity.me.cliente.visita', 'uses' => 'BE\Infinity\ME\VisitaController@index']);
  Route::post('/cliente/visita/guardar', ['as' => 'infinity.me.cliente.visita.guardar', 'uses' => 'BE\Infinity\ME\VisitaController@guardar']);
  //Route::get('/cliente/visitas/get', ['as' => 'infinity.me.clientes.visitas.get', 'uses' => 'BE\Infinity\ME\VisitaController@getVisitas']);

  //Route::get('/resumen/graficos-gestion', ['as' => 'infinity.me.grafico.gestion', 'uses' => 'BE\Infinity\ME\IndexController@resumenGraficarGestion']);
  //Route::get('/resumen/grafico-mora', ['as' => 'infinity.me.grafico.mora', 'uses' => 'BE\Infinity\ME\IndexController@resumenGraficarMora']);
  //Route::get('/resumen/grafico-lineal', ['as' => 'infinity.me.grafico.lineal', 'uses' => 'BE\Infinity\ME\IndexController@resumenGraficarLineal']);

  Route::get('/cliente/historia', ['as' => 'infinity.me.cliente.historia', 'uses' => 'BE\Infinity\ME\IndexController@getClienteHistoricoTable']);
  Route::get('/cliente/visita/historia', ['as' => 'infinity.me.visita.historia', 'uses' => 'BE\Infinity\ME\IndexController@visitaHistoria']);
  Route::get('/cliente/conoceme/historia', ['as' => 'infinity.me.conoceme.historia', 'uses' => 'BE\Infinity\ME\IndexController@conocemeHistoria']);
  Route::get('/cliente/conoceme/historia-pdf', ['as' => 'infinity.me.conoceme.historia-pdf', 'uses' => 'BE\Infinity\ME\IndexController@PDF_conoceme']);
});

Route::group(['middleware' => ['ipcheck']], function () {
  Route::get('/politica/cualitativa-diaria', ['as' => 'politica.cualitativa-diaria', 'uses' => 'PoliticaProcesoController@cualitativaDiaria']);
  Route::get('/politica/vencida', ['as' => 'politica.vencida', 'uses' => 'PoliticaProcesoController@PoliticasVencidas']);
  Route::get('/politica/cualitativa', ['as' => 'politica.cualitativa', 'uses' => 'PoliticaProcesoController@cualitativa']);
  Route::get('/politica/documentaria', ['as' => 'politica.documentaria', 'uses' => 'PoliticaProcesoController@documentaria']);
  Route::get('/politica/performance', ['as' => 'politica.performance', 'uses' => 'PoliticaProcesoController@performance']);
  Route::get('/politica/semaforo', ['as' => 'politica.semaforo', 'uses' => 'PoliticaProcesoController@semaforo']);
  Route::get('/lineas', ['as' => 'politica.lineas', 'uses' => 'PoliticaProcesoController@getLineas']);
});

//DEVELOPERS UNITED
Route::group(['prefix' => 'developers', 'middleware' => ['authBase', 'authRol:100']], function () {
  Route::get('/inicio', ['as' => 'inicio', 'uses' => 'developers\DeveloperController@index']);
});


Route::get('/proceso_politicas', ['as' => 'proceso_politicas', 'uses' => 'BE\Infinity\ME\IndexController@proceso_politicas']);

Route::get('/download/{file}', function ($file) {
  return response()->download(storage_path('app/' . str_replace('|', '/', $file)));
})->name('download');


/*******************
  LINEAS INFINITY GE
 ********************/
// Route::group(['prefix' => 'infinity/ge', 'middleware' => ['authBase', 'authRol:20|21|22|23|24|25|26|27|28|29|30|31|33|34|35|43|36|37|38|39|40']], function () {
//   Route::get('/clientes', ['as' => 'infinity.ge.clientes', 'uses' => 'BE\Infinity\ME\IndexController@getListaClientesGE']);
//   Route::get('/cliente/detalle', ['as' => 'infinity.ge.cliente.detalle', 'uses' => 'BE\Infinity\ME\IndexController@detalleGE']);
//   Route::get('/clientes/get', ['as' => 'infinity.ge.clientes.get', 'uses' => 'BE\Infinity\ME\IndexController@getListaClientesTableGE']);
// });


/*test-email*/
Route::get('/test-email', function () {
  $prueba = ['0' => '00'];
  foreach ($prueba as $key) {
    $emisor =  [
      '0' => ['address' => 'gestioncomercial@intercorp.com.pe', 'name' => 'VPConnect']
    ];
    $destino = ['gcumpalum@intercorp.com.pe'];
    $copiados = [
      '0' => ['address' => 'gcumpalum@intercorp.com.pe', 'name' => 'GianF']
      // '0'=>['address'=>'jtresierra@intercorp.com.pe','name'=>'Jose']
      // ,'1'=>['address'=>'lhuachacab@intercorp.com.pe','name'=>'Leti']
      // ,'2'=>['address'=>'jchau@intercorp.com.pe','name'=>'Jordan']
    ];
    $datos =  [
      'CU' => 'CODIGO DE CLIENTE',
      'CLIENTE' => 'NOMBRE CLIENTE',
      'EJECUTIVO' => 'NOMBRE EJECUTIVO',
      'JEFE' => 'NOMBRE JEFE',
      'MENSAJE' => 'Suspension',
      'MOTIVO' => 'Suspension',
      'LINEAS' => 'L1,L2,L3,...',
      'GESTION' => [
        'FECHA_GESTION' => '20190904',
        'POLITICA' => '1 rojo',
        'DECISION' => 'Desaprobado',
        'COMENTARIO' => 'Los comentarios que escriban los ejecutivos al gestionar una politica de este tipo, en caso tengan; en caso no tengan no habra datos de gestión'
      ]
    ];
    $mail = new \App\Mail\Infinity\Correo(
      1,
      $datos,
      $emisor,
      $destino,
      $copiados
    );

    if (dispatch(new App\Jobs\MailQueue($mail))) {
      $correoenviado = new \App\Entity\CorreosEnviados();
      $correoenviado->guardar($mail);
    };

    dispatch(new App\Jobs\MailQueue($mail));

  }
  return 'OK';
});

Route::get('/mailsalidala', function () {
  return view('emails.infinity.correoLA');
});

Route::group(['prefix' => 'ssrs', 'middleware' => ['authBase', 'authRol:10|13|14|20|21|22|23|24|25|26|27|28|29|30|32|36|37|41|31|666|999|51|52|54|100|49|70|91']], function () {
  Route::get('/saldos', ['as' => 'saldos', 'uses' => 'SSRSController@saldos']);
  Route::get('/variaciones', ['as' => 'variaciones', 'uses' => 'SSRSController@variaciones']);
  Route::get('/micliente', ['as' => 'micliente', 'uses' => 'SSRSController@miCliente']);
});

//DESCARGAS
Route::get('/download/{file}', function ($file) {
  return response()->download(storage_path('app/' . str_replace('|', '/', $file)));
})->name('download');


//Limpieza de cache
Route::get('/clear-cache', function () {
  Artisan::call('cache:clear');
  return "Cache is cleared";
});



//MI CUMPLIMIENTO
Route::group(['prefix' => 'micumplimiento/be', 'middleware' => ['authBase', 'authRol:20|21|22|23|24|25|26|27|28|29|30|31|33|34|35|43|36|37|38|39|40|100']], function () {
  Route::get('/resumen', ['as' => 'mi.cumplimiento', 'uses' => 'MC\MiCumplimientoController@index']);
  Route::post('/zonales', ['as' => 'mi.zonales', 'uses' => 'MC\MiCumplimientoController@getZonales']);
  Route::post('/lista', ['as' => 'mi.lista', 'uses' => 'MC\MiCumplimientoController@lista']);
  Route::get('/detalleindicadores', ['as' => 'mi.detalleindicadores', 'uses' => 'MC\MiCumplimientoController@detalleindicadores']);
  Route::post('/getEjecutivo', ['as' => 'mi.getEjecutivo', 'uses' => 'MC\MiCumplimientoController@getEjecutivos']);
  Route::post('/getJefaturas', ['as' => 'mi.getJefaturas', 'uses' => 'MC\MiCumplimientoController@getJefaturas']);
  Route::post('/detalle', ['as' => 'mi.detalle', 'uses' => 'MC\MiCumplimientoController@detalle']);
});



//REASIGNACION Y RESECTORIZACION
// 20|21|22|23|24|25|26|27|28|29|30|31|33|34|35|36|37|38|39|40
Route::group(['prefix' => 'rrsectorizacion/vpc', 'middleware' => ['authBase', 'authRol:20|21|22|23|24|25|26|27|28|29|30|31|33|34|35|43|36|37|38|39|40']], function () {
  Route::get('/Index', ['as' => 'rr.sector', 'uses' => 'AsignacionResectorizacion\RRsectorizacion@index']);
  Route::post('/miprospecto/nuevo', ['as' => 'be.miprospecto.nuevo', 'uses' => 'AsignacionResectorizacion\RRsectorizacion@registroReferidoNuevo']);
  Route::post('/guardar', ['as' => 'rr.guardar', 'uses' => 'AsignacionResectorizacion\RRsectorizacion@guardar']);
});


Route::get('/vistoVideoGF', ['as' => 'vistoVideoGF', 'uses' => 'BE\Infinity\AlertascarteraController@vistoVideoGF']);
Route::get('/buscarusuarioGF', ['as' => 'buscarusuarioGF', 'uses' => 'BE\Infinity\AlertascarteraController@buscarusuarioGF']);

Route::group(['middleware' => ['authBase', 'authRol:12|13|20|21|22|23|24|25|26|27|28|29|30|31|33|34|35|43|36|37|38|39|40|42|49|50|52|53|100']], function () {
  Route::get('/alertascartera', ['as' => 'infinity.alertascartera', 'uses' => 'BE\Infinity\AlertascarteraController@index']);
  Route::get('/alertascartera/clientes/get', ['as' => 'infinity.alertascartera.getclientes', 'uses' => 'BE\Infinity\AlertascarteraController@getListaClientes']);
  Route::get('/alertascartera/clientes/indicadores', ['as' => 'infinity.alertascartera.indicadores', 'uses' => 'BE\Infinity\AlertascarteraController@getIndicadores']);
  Route::get('/getResumenAlertas', ['as' => 'infinity.alertascartera.getResumenAlertas', 'uses' => 'BE\Infinity\AlertascarteraController@getResumenAlertas']);
  Route::get('/alertascartera/combos', ['as' => 'infinity.alertascartera.combos', 'uses' => 'BE\Infinity\AlertascarteraController@combos']);
  Route::get('/alertascartera/detalle', ['as' => 'infinity.alertascartera.detalle', 'uses' => 'BE\Infinity\AlertascarteraController@detalle']);
  Route::post('/alertascartera/guardarEstrategia', ['as' => 'infinity.alertascartera.guardarEstrategia', 'uses' => 'BE\Infinity\AlertascarteraController@guardarEstrategia']);
  Route::post('/alertascartera/validadorEliminar', ['as' => 'infinity.alertascartera.validadorEliminar', 'uses' => 'BE\Infinity\AlertascarteraController@validadorEliminar']);
  Route::post('/alertascartera/getCambios', ['as' => 'infinity.alertascartera.getCambios', 'uses' => 'BE\Infinity\AlertascarteraController@getCambios']);
  Route::post('/alertascartera/guardarCumplimiento', ['as' => 'infinity.alertascartera.guardarCumplimiento', 'uses' => 'BE\Infinity\AlertascarteraController@guardarCumplimiento']);
  Route::post('/alertascartera/addCumplimiento', ['as' => 'infinity.alertascartera.addCumplimiento', 'uses' => 'BE\Infinity\AlertascarteraController@addCumplimiento']);
  Route::get('/alertascartera/seguimiento', ['as' => 'infinity.alertascartera.seguimiento', 'uses' => 'BE\Infinity\AlertascarteraController@Seguimientodiario']);
  Route::get('/alertascartera/seguimiento/compromisos', ['as' => 'infinity.alertascartera.seguimiento.compromisos', 'uses' => 'BE\Infinity\AlertascarteraController@tablaCompromisos']);
  Route::get('/alertascartera/actividad', ['as' => 'infinity.alertascartera.actividad', 'uses' => 'BE\Infinity\AlertascarteraController@Saveactividad']);
  Route::get('/alertascartera/getdetactividad', ['as' => 'infinity.alertascartera.getdetactividad', 'uses' => 'BE\Infinity\AlertascarteraController@getdetactividad']);
  Route::get('/alertascartera/actualizaractividadcompromiso', ['as' => 'infinity.alertascartera.actualizaractividadcompromiso', 'uses' => 'BE\Infinity\AlertascarteraController@actualizaractividadcompromiso']);
  Route::get('/alertascartera/getMensajesAntiguos', ['as' => 'infinity.alertascartera.getMensajesAntiguos', 'uses' => 'BE\Infinity\AlertascarteraController@getMensajesAntiguos']);
  Route::get('/alertascartera/pdfseguimiento', ['as' => 'infinity.alertascartera.pdfseguimiento', 'uses' => 'BE\Infinity\AlertascarteraController@pdfseguimiento']);
  Route::get('/alertascartera/nuevomensaje', ['as' => 'alertascartera.nuevomensaje', 'uses' => 'BE\Infinity\AlertascarteraController@nuevomensaje']);
  Route::get('/alertascartera/agregargestiongys', ['as' => 'alertascartera.agregargestiongys', 'uses' => 'BE\Infinity\AlertascarteraController@agregargestiongys']);
});

Route::group(['prefix' => 'bpe/blacklist', 'middleware' => ['authBase', 'authRol:10|25|666']], function () {
  Route::get('/lista', ['as' => 'bpe.blacklist.lista', 'uses' => 'BPE\Blacklist\BlacklistController@index']);
  Route::get('/doc/nuevotelefonos', ['as' => 'bpe.blacklist.nuevotelefonos', 'uses' => 'BPE\Blacklist\BlacklistController@nuevotelefonos']);
});
//1|5|92|60|61|62|64|80|3|63
Route::group([ 'middleware' => ['authBase', 'authRol:80|3|63']], function () {
  Route::get('/rplista', ['as' => 'rp.lista', 'uses' => 'ReprogramaciondecreditoController@index']);
  Route::get('/rp/getrpc', ['as' => 'rp.getrpc', 'uses' => 'ReprogramaciondecreditoController@getListaRP']);
  Route::post('/rp/getrpcbycodcredito', ['as' => 'rp.getrpcbycodcredito', 'uses' => 'ReprogramaciondecreditoController@getrpcbycodcredito']);
  Route::get('/rp/gestionar', ['as' => 'rp.gestionar', 'uses' => 'ReprogramaciondecreditoController@gestionar']);
  Route::get('/rp/editgestionar', ['as' => 'rp.editgestionar', 'uses' => 'ReprogramaciondecreditoController@editgestionar']);
});


Route::get('/getGirosBySector', [ 'as' => 'getGirosBySector','uses'=>'CatalogoController@getGirosBySector'] );
Route::get('/getProvinciaByDepartamento', [ 'as' => 'getProvinciaByDepartamento','uses'=>'CatalogoController@getProvinciaByDepartamento'] );
Route::get('/getDistritoByProvincia', [ 'as' => 'getDistritoByProvincia','uses'=>'CatalogoController@getDistritoByProvincia'] );
Route::get('/getTiendasByUbigeo', [ 'as' => 'getTiendasByUbigeo','uses'=>'CatalogoController@getTiendasByUbigeo'] );

Route::group(['prefix' => 'cmpemergencia', 'middleware' => ['authBase', 'authRol:1|7|8|20|21|60|64|10|15|6|80|62|90|31|91|63']], function(){
  /*BE*/
  Route::get('/index',['as' => 'cmpEmergencia.index', 'uses' => 'CmpEmergencia\IndexController@index']);
  Route::get('/listar',['as' => 'cmpEmergencia.lista', 'uses' => 'CmpEmergencia\IndexController@listar']);
  Route::post('/nuevoasignado',['as' => 'cmpEmergencia.nuevoasignado', 'uses' => 'CmpEmergencia\IndexController@nuevoasignado']);
  Route::post('/validacionInteresado',['as' => 'cmpEmergencia.validacionInteresado', 'uses' => 'CmpEmergencia\IndexController@validarInteresado']);
  Route::get('/buscarinfo',['as' => 'cmpEmergencia.buscarinfo', 'uses' => 'CmpEmergencia\IndexController@buscarinfo']);
  Route::post('/cmpgestionar',['as' => 'cmpEmergencia.cmpgestionar', 'uses' => 'CmpEmergencia\IndexController@gestionar']);

  Route::post('/cmpgestionsubasta',['as' => 'cmpEmergencia.cmpgestionsubasta', 'uses' => 'CmpEmergencia\IndexController@gestionsubasta']);

  Route::post('/docs-completos',['as' => 'cmpEmergencia.docs-completos', 'uses' => 'CmpEmergencia\IndexController@documentosCompletos']);
  Route::post('/enviar-valdoc',['as' => 'cmpEmergencia.enviar-valdoc', 'uses' => 'CmpEmergencia\IndexController@enviarValDocumentacion']);

  Route::post('/wioparacofide',['as' => 'cmpEmergencia.wioparacofide', 'uses' => 'CmpEmergencia\IndexController@wioparacofide']);

  /*BPE*/
  Route::get('bpe/index',['as' => 'cmpEmergencia.bpe.index', 'uses' => 'CmpEmergencia\IndexBpeController@index']);
  Route::get('bpe/listar',['as' => 'cmpEmergencia.bpe.lista', 'uses' => 'CmpEmergencia\IndexBpeController@listar']);
  Route::get('bpe/indexproducto',['as' => 'cmpEmergencia.bpe.indexproducto', 'uses' => 'CmpEmergencia\IndexBpeController@indexproducto','middleware' => ['authBase', 'authRol:31|91']]);
  Route::get('bpe/listar/producto',['as' => 'cmpEmergencia.bpe.lista-producto', 'uses' => 'CmpEmergencia\IndexBpeController@listarproducto']);
  Route::get('bpe/getByRuc',['as' => 'cmpEmergencia.bpe.getByRuc', 'uses' => 'CmpEmergencia\IndexBpeController@getByRuc']);
  Route::post('bpe/gestionar',['as' => 'cmpEmergencia.bpe.gestionar', 'uses' => 'CmpEmergencia\IndexBpeController@gestionar']);
  Route::get('bpe/getTasas',['as' => 'cmpEmergencia.bpe.getTasas', 'uses' => 'CmpEmergencia\IndexBpeController@getTasas']);
  Route::post('enviarGtp',['as' => 'cmpEmergencia.bpe.enviarGtp', 'uses' => 'CmpEmergencia\IndexBPEController@enviarGtp']);
  Route::get('acuerdocreditopdf', ['as' => 'cmpEmergencia.bpe.acuerdocreditopdf', 'uses' => 'CmpEmergencia\IndexBPEController@acuerdocreditopdf']);
  Route::get('pagarepdf', ['as' => 'cmpEmergencia.bpe.pagarepdf', 'uses' => 'CmpEmergencia\IndexBPEController@pagarepdf']);
  Route::get('llenadopagarepdf', ['as' => 'cmpEmergencia.bpe.llenadopagarepdf', 'uses' => 'CmpEmergencia\IndexBPEController@llenadopagarepdf']);
  Route::get('cartapresentacionpdf', ['as' => 'cmpEmergencia.bpe.cartapresentacionpdf', 'uses' => 'CmpEmergencia\IndexBPEController@cartapresentacionpdf']);
  Route::get('pdfTotal', ['as' => 'cmpEmergencia.bpe.pdfTotal', 'uses' => 'CmpEmergencia\IndexBPEController@pdfTotal']);
});

/*Para Backoffice*/
Route::group(['prefix' => 'cmpemergencia/backoffice', 'middleware' => ['authBase', 'authRol:80|10|62']], function(){
  Route::get('/index',['as' => 'cmpEmergencia.backoffice.index', 'uses' => 'CmpEmergencia\IndexBackofficeController@index']);
  Route::get('/listar',['as' => 'cmpEmergencia.backoffice.lista', 'uses' => 'CmpEmergencia\IndexBackofficeController@listar']);
  Route::post('flag', ['as' => 'cmpEmergencia.backoffice.actualizar', 'uses' => 'CmpEmergencia\IndexBackofficeController@actualizar']);
});

Route::group(['prefix' => 'cmpemergencia/riesgos/bpe', 'middleware' => ['authBase', 'authRol:3|35|33|43|10|63']], function(){
  Route::get('index',['as' => 'cmpEmergencia.riesgos.bpe.index', 'uses' => 'CmpEmergencia\IndexRiesgosBPEController@index']);
  Route::get('listar',['as' => 'cmpEmergencia.riesgos.bpe.lista', 'uses' => 'CmpEmergencia\IndexRiesgosBPEController@listar']);
  Route::post('gestionar',['as' => 'cmpEmergencia.riesgos.bpe.gestionar', 'uses' => 'CmpEmergencia\IndexRiesgosBPEController@gestionar']);
  Route::get('buscar',['as' => 'cmpEmergencia.riesgos.bpe.buscar', 'uses' => 'CmpEmergencia\IndexRiesgosBPEController@buscar']);
});

Route::group(['prefix' => 'cmpemergencia/validacion', 'middleware' => ['authBase', 'authRol:11|12|36|37']], function(){
  Route::get('index',['as' => 'cmpEmergencia.validacion.index', 'uses' => 'CmpEmergencia\ValidacionController@index']);
  Route::get('listar',['as' => 'cmpEmergencia.validacion.lista', 'uses' => 'CmpEmergencia\ValidacionController@listar']);
  Route::get('buscar',['as' => 'cmpEmergencia.validacion.buscar', 'uses' => 'CmpEmergencia\ValidacionController@buscar']);
  Route::post('gestionar',['as' => 'cmpEmergencia.validacion.gestionar', 'uses' => 'CmpEmergencia\ValidacionController@gestionar']);
});

/*Solucion Empresa*/
Route::group(['prefix' => 'cmpemergencia/solemp', 'middleware' => ['authBase', 'authRol:21|61|29|6|10']], function(){
  Route::get('index',['as' => 'cmpEmergencia.solemp.index', 'uses' => 'CmpEmergencia\IndexSolempController@index']);
  Route::get('listar',['as' => 'cmpEmergencia.solemp.lista', 'uses' => 'CmpEmergencia\IndexSolempController@listar']);
  Route::post('priorizar',['as' => 'cmpEmergencia.solemp.priorizar', 'uses' => 'CmpEmergencia\IndexSolempController@priorizar']);
});

Route::group(['prefix' => 'usuarios','middleware' => ['authBase']], function () {
  Route::get('/reportes/view360',['as' => 'reportes.view360', 'uses' => 'View360Controller@index']);
});