<?php

namespace App\Mail\Infinity;
use App\Entity\Infinity\Cliente as Cliente;
use App\Entity\Infinity\Politica as Politica;
Use Log;

class Correo extends \App\Mail\Mail {

    const BUZON_VPCONNECT = ['0'=>['address'=>'gestioncomercial@intercorp.com.pe','name'=>'VPConnect']];
    const CABECERA_LA = "ESTE ES UN CORREO DE PRUEBA: LÍNEAS AUTOMÁTICAS - Notificaciones";//LÍNEAS AUTOMÁTICAS - Notificaciones
    const PLANTILLA_LA='emails.infinity.correoLA';
    const TIPO_LA = 1;

    //En desarrollo
    const CABECERA_REASIGNACION = "";
    const PLANTILLA_REASIGNACION = '';
    const TIPO_REASIGNACION = 2;

    public $_tipo;
    
    public $_datos;
    public $_subject;
    public $_plantilla;

    public $_to;
    public $_destinatarios;
    public $_copiados;

    public $from = [];
    public $cc = [];
    
    public function __construct($tipo,$datos,$emisor,$destino,$copiados) {

        $this->from=!empty($emisor) ? $emisor : self::BUZON_VPCONNECT;
        $this->_to=$destino;
        $this->cc=$copiados;

        $this->_tipo=$tipo;
        if ($this->_tipo==self::TIPO_LA) {

            $this->_subject=self::CABECERA_LA;
            $this->_plantilla=self::PLANTILLA_LA;
            $this->_datos=!empty($datos) ? $datos : null;
        }
    }

    public function build() {
        return $this
            ->to($this->_to)
            ->subject($this->_subject)
            ->view($this->_plantilla)
            ->with('datos',$this->_datos);
    }

}