<?php

namespace App\Entity;
use App\Model\Ecosistema as mEcosistema;
use Jenssegers\Date\Date as Carbon;

class Ecosistema extends \App\Entity\Base\Entity {

    static function estaEnArreglo($arreglo,$ruc){
      $encontrado=false;
      foreach($arreglo as $elemento){
        if($elemento['RUC']==$ruc){
          $encontrado=true;
          break;
        }
      }
      return $encontrado;
    }

    static function getGrafoEcosistema(){
      $model= new mEcosistema();
      $sql=$model->getEcosistema()->get();

      $grafo=[];
      //Análisis bidireccional (grafo)
      foreach($sql as $registro){   
        //Empresa de la izquierda de la tabla
        $ruc1=$registro->RUC;
        $cu1=$registro->CU;
        $nombre1=$registro->NOMBRE;
        $flg1=$registro->FLG_CLIENTE_IBK_1;
        //Qué es de la empresa 2
        $tipo1=$registro->TIPO=='P'?'C':'P';

        //Empresa de la derecha de la empresa
        $ruc2=$registro->RUC_CP;
        $cu2=$registro->CU_CP;
        $nombre2=$registro->NOMBRE_CP;
        $flg2=$registro->FLG_CLIENTE_IBK_2;
        //Qué es de la empresa 1
        $tipo2=$registro->TIPO;

        //Info del adyacente a la empresa1
        $adyacente1=['RUC'=>$ruc2,
                    'NOMBRE'=>$nombre2,
                    'CU'=>$cu2,
                    'TIPO'=>$tipo2,
                    'FLAG_DIFERENCIADOR'=>$flg2];

        //Info del adyacente a la empresa2
        $adyacente2=['RUC'=>$ruc1,
                    'NOMBRE'=>$nombre1,
                    'CU'=>$cu1,
                    'TIPO'=>$tipo1,
                    'FLAG_DIFERENCIADOR'=>$flg1];


        //Analizamos la empresa de la izquierda
        if(!self::estaEnArreglo($grafo,$ruc1)){
          $adyacentes=[];
          $adyacentes[]=$adyacente1;
          $grafo[]=[
              'RUC'=>$ruc1,
              'NOMBRE'=>$nombre1,
              'CU'=>$cu1,
              'FLAG_DIFERENCIADOR'=>$flg1,
              'ADYACENTES'=>$adyacentes
          ];
        }
        else{
          //Si ya existe, unicamente se añade el adyacente (Se puede mejorar)
          for($i=0;$i<count($grafo);$i++){
            if($grafo[$i]['RUC']==$ruc1){
              if(!self::estaEnArreglo($grafo[$i]['ADYACENTES'],$ruc2))
                $grafo[$i]['ADYACENTES'][]=$adyacente1;
              break;
            }
          }
        }

        //Analizamos la empresa de la derecha
        if(!self::estaEnArreglo($grafo,$ruc2)){
          $adyacentes=[];
          $adyacentes[]=$adyacente2;
          $grafo[]=[
              'RUC'=>$ruc2,
              'NOMBRE'=>$nombre2,
              'CU'=>$cu2,
              'FLAG_DIFERENCIADOR'=>$flg2,
              'ADYACENTES'=>$adyacentes
          ];
        }
        else{
          //Si ya existe, unicamente se añade el adyacente (Se puede mejorar)
          for($i=0;$i<count($grafo);$i++){
            if($grafo[$i]['RUC']==$ruc2){
              if(!self::estaEnArreglo($grafo[$i]['ADYACENTES'],$ruc1))
                $grafo[$i]['ADYACENTES'][]=$adyacente2;
              break;
            }
          }
        }


      }

      return $grafo;
    }


    static function getNodoEcosistema($ruc){
        $model= new mEcosistema();
        return $model->getNodoEcosistema($ruc,self::sgetPeriodoBE())->get();
    }

    static function buscarRucEcosistema($cuBuscar,$razonBuscar){
        $model= new mEcosistema();        
        $rucBuscar=$model->buscarRucEcosistema($cuBuscar,$razonBuscar)->get();
        //dd($rucBuscar);
        if (count($rucBuscar)>0)
          $rucBuscar=$rucBuscar[0]->NUM_DOC;
        else 
          $rucBuscar='';

        return $rucBuscar;
    }

    static function obtenerEmpresas(){
        $model= new mEcosistema();        
        return $model->obtenerEmpresas();
    }


    static function seguimientoIngresado($usuario,$busqueda){
        $model= new mEcosistema();        
        //Aquí debe haber una lógica dependiendo del usuario
        return $model->seguimientoIngresado($busqueda)->orderBy('B.NUM_EMPRESAS','DESC')->orderBy('B.NOMBRE','ASC')->get();
    }

    static function seguimientoIngresadoAgrupado($usuario,$busqueda){
        $model= new mEcosistema();        
        //Aquí debe haber una lógica dependiendo del usuario
        $lista=$model->seguimientoIngresado($busqueda)
        ->orderBy('B.BANCA')
        ->orderBy('B.ZONAL')
        ->orderBy('B.JEFATURA')
        ->orderBy('B.NUM_EMPRESAS')
        ->orderBy('B.NOMBRE')
        ->get();
          //dd($lista);
        $seguimiento=array();
        $banca='';
        $zonal='';
        $jefatura='';
        $ejecutivo='';

        $ejecutivoNuevo=array();
        $jefaturaNueva=array();
        $zonalNueva=array();
        $bancaNueva=array();

        $contBanca=0;
        $contZonal=0;
        $contJefatura=0;
        $contEjecutivo=0;
        
        $arregloFiltro=[
          'ejecutivo' => $busqueda['ejecutivo'],
          'jefatura' => $busqueda['jefatura'],
          'zonal' => $busqueda['zonal'],
          'banca'=>$busqueda['banca'],
          'fechaAvance'=>$busqueda['fechaAvance'],
          'fechaCorte'=>$busqueda['fechaCorte']
        ];
        //dd($busqueda);

        foreach ($lista as $fila) {

            if($banca!=$fila->BANCA){
                if($contBanca>0){ 
                    $jefaturaNueva['EJECUTIVOS'][]=$ejecutivoNuevo;
                    $zonalNueva['JEFATURAS'][]=$jefaturaNueva;
                    $bancaNueva['ZONALES'][]=$zonalNueva;
                    $seguimiento[]=$bancaNueva;
                }
                $bancaNueva=array();
                //Aquí se debe de consultar
                $arregloFiltro['banca']=$fila->BANCA;
                $arregloFiltro['zonal']=null;
                $arregloFiltro['jefatura']=null;
                $infoTotal=$model->seguimientoIngresadoTotales($arregloFiltro)->first();

                $bancaNueva=['BANCA'=>$fila->BANCA,
                              'NUM_EMPRESAS_CORTE'=>$infoTotal->NUM_EMPRESAS_CORTE,
                                'NUM_EMPRESAS'=>$infoTotal->NUM_EMPRESAS,
                                'TOTAL'=>$infoTotal->TOTAL,
                                'CUENTA_MEL'=>$infoTotal->CUENTA_MEL,
                                'CUENTA_GEL'=>$infoTotal->CUENTA_GEL,
                                'CUENTA_BEP'=>$infoTotal->CUENTA_BEP,
                                'CUENTA_BC'=>$infoTotal->CUENTA_BC,
                                'CUENTA_BPE'=>$infoTotal->CUENTA_BPE,
                                'CUENTA_MESA'=>$infoTotal->CUENTA_MESA,
                                'VOLUMEN_ECOSISTEMA'=>$infoTotal->VOLUMEN_ECOSISTEMA,
                                'VOLUMEN_REAL'=>$infoTotal->VOLUMEN_REAL,
                                'VOLUMEN_META'=>$infoTotal->VOLUMEN_META,
                                'PROV_TOTAL'=>$infoTotal->PROV_TOTAL,
                                'PROV_REAL'=>$infoTotal->PROV_REAL,
                                'PROV_META'=>$infoTotal->PROV_META,
                                'ZONALES'=>[]];
                $banca=$fila->BANCA;
                $contBanca++;

            }
            if($zonal!=$fila->ZONAL){
                if($contZonal>0 and $fila->BANCA==$zonalNueva['BANCA']) {
                    $jefaturaNueva['EJECUTIVOS'][]=$ejecutivoNuevo;
                    $zonalNueva['JEFATURAS'][]=$jefaturaNueva;
                    $bancaNueva['ZONALES'][]=$zonalNueva;
                }
                $zonalNueva=array();

                //Aquí se debe de consultar
                $arregloFiltro['banca']=$fila->BANCA;
                $arregloFiltro['zonal']=$fila->ID_ZONA;
                $arregloFiltro['jefatura']=null;
                $infoTotal=$model->seguimientoIngresadoTotales($arregloFiltro)->first();

                $zonalNueva=['BANCA'=>$fila->BANCA,
                                'ID_ZONA'=>$fila->ID_ZONA,
                                'ZONAL'=>$fila->ZONAL,
                                'ID_JEFATURA'=>$fila->ID_JEFATURA,
                                'NUM_EMPRESAS_CORTE'=>$infoTotal->NUM_EMPRESAS_CORTE,
                                'NUM_EMPRESAS'=>$infoTotal->NUM_EMPRESAS,
                                'TOTAL'=>$infoTotal->TOTAL,
                                'CUENTA_MEL'=>$infoTotal->CUENTA_MEL,
                                'CUENTA_GEL'=>$infoTotal->CUENTA_GEL,
                                'CUENTA_BEP'=>$infoTotal->CUENTA_BEP,
                                'CUENTA_BC'=>$infoTotal->CUENTA_BC,
                                'CUENTA_BPE'=>$infoTotal->CUENTA_BPE,
                                'CUENTA_MESA'=>$infoTotal->CUENTA_MESA,
                                'VOLUMEN_ECOSISTEMA'=>$infoTotal->VOLUMEN_ECOSISTEMA,
                                'VOLUMEN_REAL'=>$infoTotal->VOLUMEN_REAL,
                                'VOLUMEN_META'=>$infoTotal->VOLUMEN_META,
                                'PROV_TOTAL'=>$infoTotal->PROV_TOTAL,
                                'PROV_REAL'=>$infoTotal->PROV_REAL,
                                'PROV_META'=>$infoTotal->PROV_META,
                                'JEFATURAS'=>[]];
                $zonal=$fila->ZONAL;
                $contZonal++;

            }
            if($jefatura!=$fila->JEFATURA){
                if($contJefatura>0 and $fila->ZONAL==$jefaturaNueva['ZONAL']){ 
                    $jefaturaNueva['EJECUTIVOS'][]=$ejecutivoNuevo;
                    $zonalNueva['JEFATURAS'][]=$jefaturaNueva;
                }
                $jefaturaNueva=array();

                //Aquí se debe de consultar
                $arregloFiltro['banca']=$fila->BANCA;
                $arregloFiltro['zonal']=$fila->ID_ZONA;
                $arregloFiltro['jefatura']=$fila->ID_JEFATURA;
                $infoTotal=$model->seguimientoIngresadoTotales($arregloFiltro)->first();

                $jefaturaNueva=[
                                //'BANCA'=>$fila->BANCA,
                                'ZONAL'=>$fila->ZONAL,
                                'ID_ZONA'=>$fila->ID_ZONA,
                                'ID_JEFATURA'=>$fila->ID_JEFATURA,
                                'JEFATURA'=>$fila->JEFATURA,
                                'NUM_EMPRESAS_CORTE'=>$infoTotal->NUM_EMPRESAS_CORTE,
                                'NUM_EMPRESAS'=>$infoTotal->NUM_EMPRESAS,
                                'TOTAL'=>$infoTotal->TOTAL,
                                'CUENTA_MEL'=>$infoTotal->CUENTA_MEL,
                                'CUENTA_GEL'=>$infoTotal->CUENTA_GEL,
                                'CUENTA_BEP'=>$infoTotal->CUENTA_BEP,
                                'CUENTA_BC'=>$infoTotal->CUENTA_BC,
                                'CUENTA_BPE'=>$infoTotal->CUENTA_BPE,
                                'CUENTA_MESA'=>$infoTotal->CUENTA_MESA,
                                'VOLUMEN_ECOSISTEMA'=>$infoTotal->VOLUMEN_ECOSISTEMA,
                                'VOLUMEN_REAL'=>$infoTotal->VOLUMEN_REAL,
                                'VOLUMEN_META'=>$infoTotal->VOLUMEN_META,
                                'PROV_TOTAL'=>$infoTotal->PROV_TOTAL,
                                'PROV_REAL'=>$infoTotal->PROV_REAL,
                                'PROV_META'=>$infoTotal->PROV_META,                                
                                'EJECUTIVOS'=>[]];
                $jefatura=$fila->JEFATURA;
                $contJefatura++;

            }
            //Aquí las EJECUTIVOS siempre existen
            if($contEjecutivo>0 and $fila->JEFATURA==$ejecutivoNuevo['JEFATURA'])
                $jefaturaNueva['EJECUTIVOS'][]=$ejecutivoNuevo;
            $ejecutivoNuevo=array();
            //dd(substr($fila->JEFATURA,0,1)=="B");
            $ejecutivoNuevo=[
                //'BANCA'=>$fila->BANCA,
                //'ZONAL'=>$fila->ZONAL,
                'JEFATURA'=>$fila->JEFATURA,
                'ID_JEFATURA'=>$fila->ID_JEFATURA,
                'EJECUTIVO'=>$fila->NOMBRE,
                'REGISTRO'=>$fila->REGISTRO,
                'NUM_EMPRESAS_CORTE'=>$fila->NUM_EMPRESAS_CORTE,
                'NUM_EMPRESAS'=>$fila->NUM_EMPRESAS,
                'TOTAL'=>$fila->TOTAL,
                'CUENTA_MEL'=>$fila->CUENTA_MEL,
                'CUENTA_GEL'=>$fila->CUENTA_GEL,
                'CUENTA_BEP'=>$fila->CUENTA_BEP,
                'CUENTA_BC'=>$fila->CUENTA_BC,
                'CUENTA_BPE'=>$fila->CUENTA_BPE,
                'CUENTA_MESA'=>$fila->CUENTA_MESA,
                'VOLUMEN_ECOSISTEMA'=>$fila->VOLUMEN_ECOSISTEMA,
                'VOLUMEN_REAL'=>$fila->VOLUMEN_REAL,
                'VOLUMEN_META'=>$fila->VOLUMEN_META,
                'PROV_TOTAL'=>$fila->PROV_TOTAL,
                'PROV_REAL'=>$fila->PROV_REAL,
                'PROV_META'=>$fila->PROV_META
              ];
            
            $contEjecutivo++;
        }

        if($ejecutivoNuevo!=NULL){
            $jefaturaNueva['EJECUTIVOS'][]=$ejecutivoNuevo;
            $zonalNueva['JEFATURAS'][]=$jefaturaNueva;
            $bancaNueva['ZONALES'][]=$zonalNueva;
            $seguimiento[]=$bancaNueva;
        }
      

       return $seguimiento;
        
    }

    static function seguimientoIngresadoTotales($usuario,$busqueda){
        $model= new mEcosistema();        
        //Aquí debe haber una lógica dependiendo del usuario
        return $model->seguimientoIngresadoTotales($busqueda)->first();
    }
     
    static function getStatusProveedoresIngresados($busqueda=[]){
        $model = new mEcosistema();
        return $model->getStatusProveedoresIngresados(self::sgetPeriodoBE(),$busqueda)->get();    
    }

    static function graficarIngresados($busqueda=[]){
        $model = new mEcosistema();
        return $model->graficarIngresados(self::sgetPeriodoBE(),$busqueda);    
    }

    static function seguimientoRecibido($usuario,$busqueda){
        $model= new mEcosistema();        
        //Aquí debe haber una lógica dependiendo del usuario
        return $model->seguimientoRecibido($busqueda)->get();
    }


    

}
