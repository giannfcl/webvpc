<?php

namespace App\Entity;
use Jenssegers\Date\Date as Carbon;
use App\Model\LeadCampanha as mLeadCampanha;
use App\Entity\Canal as Canal;

class LeadCampanha extends \App\Entity\Base\Entity {

	protected $_periodo;
	protected $_lead;
	protected $_fechaCarga;
	protected $_codUnico;
	protected $_campanha;
	protected $_canalDespliegue;
	protected $_canalActual;
	protected $_prioridad;
	protected $_fechaActualizacion;

    //Para acciones comerciales
    protected $_montoIngresos;
    protected $_fechaInicio;
    protected $_fechaFin;
    protected $_registroResponsable;
    protected $_tipoAsignacion;
    protected $_tooltip;
    protected $_kpi;
    protected $_codSectUniq;
    protected $_flgHabilitado;
    protected $_flgAccion;
    protected $_mesActivacion;
    protected $_idActividad;

    const TIPO_ASIGNACION_EJECUTIVO = 'EJECUTIVO';

    function setValueToTable() {
        return $this->cleanArray([
	        'PERIODO' => $this->_periodo,
	        'NUM_DOC' => $this->_lead,
	        'FECHA_CARGA' => $this->_fechaCarga,
	        'COD_UNICO' => $this->_codUnico,
	        'ID_CAMP_EST' => $this->_campanha,
	        'CANAL_ACTUAL' => $this->_canalActual,
	        'CANAL_DESPLIEGUE' => $this->_canalDespliegue,
	        'FECHA_ACTUALIZACION' => $this->_fechaActualizacion,
	        'PRIORIDAD' => $this->_prioridad,
            'MONTO_INGRESOS'=> $this->_montoIngresos,
            'FECHA_INICIO'=> $this->_fechaInicio,
            'FECHA_FIN'=> $this->_fechaFin,
            'REGISTRO_RESPONSABLE'=> $this->_registroResponsable,
            'TIPO_ASIGNACION'=> $this->_tipoAsignacion,
            'TOOLTIP'=> $this->_tooltip,
            'KPI'=> $this->_kpi,
            'COD_SECT_UNIQ_EN'=> $this->_codSectUniq,
            'FLG_HABILITADO'=> $this->_flgHabilitado,
            'FLG_ACCION'=> $this->_flgAccion,
            'MES_ACTIVACION'=> $this->_mesActivacion,
            'ID_ACTIVIDAD'=> $this->_idActividad,
        ]);
    }



    static function getCampanhasByLead($lead,$canales){
    	$model = new mLeadCampanha();
    	return $model->getCampanhasByLead(self::sgetPeriodo(),$lead,$canales)->get();
    }

    static function getCampanhasAtributoGestionByLead($lead,$canales = []){
    	$model = new mLeadCampanha();
        if (!$canales){
            $canales = Canal::getCanalesEjecutivo();
        }
        $campanhas = $model->getCampanhasByLeadFull(self::sgetPeriodo(),$lead,$canales)->get();
        foreach($campanhas as $key => $campanha){
            $atributos = explode('|', $campanha->ATRIBUTO);
            $valores = explode('|', $campanha->VALOR);
            foreach($atributos as $j => $a){
                //Reviso si hay algun atributo de tasa piso
                if (in_array($a,['Tasa Piso'])){
                    $tasa = preg_replace( '/[^0-9.]/', '', $valores[$j]);
                    if (is_numeric($tasa)){
                        $campanhas[$key]->TASA_MINIMA = $valores[$j];
                    }
                }

                //Reviso si hay algun atributo de Oferta Tope
                if (in_array($a,['Oferta'])){
                    $oferta = preg_replace( '/[^0-9]/', '', $valores[$j]);
                    if (is_numeric($oferta)){
                        $campanhas[$key]->MONTO_MAXIMO = $valores[$j];
                    }
                }
                //Reviso si hay algun atributo de Oferta Tope
                if (in_array($a,['Plazo'])){
                    $campanhas[$key]->PLAZOS = array_filter(explode(' ', preg_replace( '/[^0-9 ]/', '',$valores[$j] )));
                }
                //Reviso si hay algun atributo de Oferta Tope
                if (in_array($a,['Gracia'])){
                    $campanhas[$key]->GRACIAS = array_filter(explode(' ', preg_replace( '/[^0-9 ]/', '',$valores[$j] )));
                }
            }
        }
        return $campanhas;
    }


    static function getDistritosByEjecutivo($ejecutivo=null,$tipo = NULL){
    	// FALTA CACHE
        //Tipo= 1 para el caso de clientes (cartera)
    	$model = new mLeadCampanha();
    	return $model->getDistritosByEjecutivo(self::sgetPeriodo(),$ejecutivo,Canal::getCanalesEjecutivo(),$tipo)->get();
    }

    static function getDistritosByAsistente($ejecutivo){
		// FALTA CACHE        
    }

    static function getCampanhasByEjecutivo($ejecutivo){
    	// FALTA CACHE
    	$model = new mLeadCampanha();
    	return $model->getCampanhasByEjecutivo(self::sgetPeriodo(),$ejecutivo,Canal::getCanalesEjecutivo())->get();
    }

    static function getPropensiones(){
        // FALTA CACHE
        $model = new mLeadCampanha();
        return $model->getPropensiones()->get();
    }

    static function getCampanhasByEjecutivoResumido($ejecutivo){
        $model=new mLeadCampanha();
        return $model->getCampanhasByEjecutivoResumido(self::sgetPeriodo(),$ejecutivo,Canal::getCanalesEjecutivo())->get();
    }

    static function getCampanhasByAsistente($ejecutivo){
    	$model = new mLeadCampanha();
        return $model->getCampanhasByAsistente(self::sgetPeriodo(),$ejecutivo,Canal::getCanalesAsistente())->get();
    }

    static function getResumenByEjecutivo($ejecutivo,$campanhas){

        $model = new mLeadCampanha();
        if (!is_array($campanhas) && !is_null($campanhas)){ //Caso de una sola campaña
            $campanhas = [$campanhas];
        }
        return $model->getResumenByEjecutivo(self::sgetPeriodo(),$ejecutivo,Canal::getCanalesEjecutivo(),$campanhas)->first();
    }

    //Cartera

    static function getCampanhasCartera(){
        $model= new mLeadCampanha();
        $query=$model->getCampanhasCartera()->get();
               
        return $query;
    }

    static function getResumenCarteraByEjecutivo($ejecutivo,$filtros){

        $model= new mLeadCampanha();
        $query=$model->getResumenCarteraByEjecutivo(self::sgetPeriodo(),$ejecutivo,$filtros)->first();
        //dd($query);
        return $query;
    }


    static function getResumenCarteraByJefe($ejecutivo,$filtros){

        $model= new mLeadCampanha();
        $query=$model->getResumenCarteraByJefe(self::sgetPeriodo(),$ejecutivo,$filtros)->first();
        //dd($query);
        return $query;
    }
    static function getResumenCarteraByAsistente($asistente,$filtros){

        $model= new mLeadCampanha();
        $query=$model->getResumenCarteraByAsistente(self::sgetPeriodo(),$asistente,$filtros)->first();
        //dd($query);
        return $query;
    }


    function cambioCampanhaCliente($ejecutivo, $cliente, $campanhaNueva){
        $model= new mLeadCampanha();
        if ($cliente==NULL or $campanhaNueva==NULL){
            $this->setMessage('No se puede actualizar la campaña');
            return false;
        }
        else{
            $model->cambioCampanhaCliente(self::sgetPeriodo(),$ejecutivo,$cliente, $campanhaNueva); 
            $this->setMessage('La campaña fue actualizada exitosamente');
            return true;
        }

    }

    function cambiarFechaFin(\App\Entity\Usuario $ejecutivo){
        //dd("Salir");
        $model=new mLeadCampanha();

        if($this->_fechaInicio>$this->_fechaFin){
            
            //$this->setMessage('La fecha fin no puede ser menor a la fecha de inicio');
            return -1;
        }

        $this->_periodo=self::sgetPeriodoBE();
        $this->_registroResponsable=$ejecutivo->getValue('_registro');
        $this->_fechaCarga=Carbon::now();

        if ($model->updateFechaFin($this->setValueToTable())){            
            return true;
        }
        else{
            $this->setMessage('No se pudo cambiar la fecha fin');
            return false;
        }
    }
}


        