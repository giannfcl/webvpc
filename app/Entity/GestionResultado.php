<?php

namespace App\Entity;

use App\Model\GestionResultado as mGestionResultado;
use Illuminate\Support\Facades\Cache;

class GestionResultado extends \App\Entity\Base\Entity {

    const ACTIVO = '1';
    const INACTIVO = '0';

    const ACEPTA_CAMPANHA_TEXT = 'ACEPTA CAMPAÑA';

    /*FALTA: Como configurar globalmente?*/
    const RESULTADO_LO_PENSARA_ID = 3;
    const MOTIVO_LO_PENSARA_ID = 12;

    const RESULTADO_FUERA_DE_PERFIL_POR_VENTAS= 8;
    const RESULTADO_MALAS_REFERENCIAS_CREDITICIAS= 10;
    const RESULTADO_ACEPTA_GIRO= 12;
    const RESULTADO_ACEPTA_CAMPANHA = 1;
    const RESULTADO_ACEPTA_CREDITO= 7;
    const RESULTADO_ACEPTA_CREDITO_GIRO= 11;
    const RESULTADO_CONTACTO_AVAL=13;
    const RESULTADO_NO_CONTESTA=14;
    const RESULTADO_NRO_ERRADO=15;
    const RESULTADO_MALOGRADO_CORTADO=16;
    const RESULTADO_CONTACTO_TITULAR=17;
    const RESULTADO_CONTACTO_FAMILIAR=18;
    const RESULTADO_CONTACTO_VECINO=19;
    const RESULTADO_ENVIO_GTP=25;

    const MOTIVO_FUERA_DE_PERFIL_POR_VENTAS= 31;
    const MOTIVO_MALAS_REFERENCIAS_CREDITICIAS= 32;
    const MOTIVO_ACEPTA_GIRO= 33;
    const MOTIVO_CONTACTO_AVAL=34;
    const MOTIVO_NO_CONTESTA=35;
    const MOTIVO_NRO_ERRADO=36;
    const MOTIVO_MALOGRADO_CORTADO=37;
    const MOTIVO_CONTACTO_TITULAR=38;
    const MOTIVO_CONTACTO_FAMILIAR=39;
    const MOTIVO_CONTACTO_VECINO=40;  
    const MOTIVO_ENVIO_GTP = 100;


    const CACHE_GET_MOTIVOS_RESULTADO = 'get-motivo-by-resultado-';
    const CACHE_GET_MOTIVOS = 'get-motivos';
    const CACHE_GET_RESULTADOS = 'get-resultados-campanaha-';

    /*FALTA: AGREGAR CACHE*/

    static function getBpeResultados($campanhas){

        //$result = Cache::rememberForever(self::CACHE_GET_RESULTADOS, function() use($campanhas){
        $data = mGestionResultado::getResultados($campanhas)->get();
        $formatted = [];
        foreach ($data as $key => $row) {
            $formatted[$row->ID_CAMP_EST][] = $data[$key];
        }
        //dd($formatted);
        //\Debugbar::info($formatted);
        return $formatted;
        //});

       
        
        return $result;
    }

    static function getBpeResultadosAC(){

        // Default de cualquier campaña BPE
        $items = self::getBpeResultados(1);
        $items = array_first($items);
        foreach ($items as $key => $item) {
            if ($item->desc == self::ACEPTA_CAMPANHA_TEXT) {
                unset($items[$key]);
            }
        }
        return $items;
    }

    static function getBpeMotivosByResultado($resultado){
        //Arreglar el tema del caché
        //$result = Cache::rememberForever(self::CACHE_GET_MOTIVOS_RESULTADO . $resultado, function() use ($resultado){
            return mGestionResultado::get(Banca::BPE,self::ACTIVO,$resultado)->get();
        //});

        return $result;
    }

    static function getBpeMotivos(){
        //Arreglar el tema del caché
        //$result = Cache::rememberForever(self::CACHE_GET_MOTIVOS, function() {
            return mGestionResultado::get(Banca::BPE,self::ACTIVO,null)->get();
            
        //});
        //dd($result);
        return $result;
    }

    static function getResultadosAcepta(){
        return [self::RESULTADO_ACEPTA_GIRO,self::RESULTADO_ACEPTA_CAMPANHA,
        self::RESULTADO_ACEPTA_CREDITO,self::RESULTADO_ACEPTA_CREDITO_GIRO,self::RESULTADO_ENVIO_GTP];
    }

}