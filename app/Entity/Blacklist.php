<?php 

namespace App\Entity;
use \Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Jenssegers\Date\Date as Carbon;
use App\Model\Blacklist as mBlacklist;
use DB;

class Blacklist extends \App\Entity\Base\Entity {
	const CANT_BY_PAGE=15;
	function Buscar($busqueda)
	{
		$model = new mBlacklist();
        $consulta = $model->buscar($busqueda);

        $totalCount=$consulta->count();
        $query = $consulta->take(self::CANT_BY_PAGE)
                ->skip(self::CANT_BY_PAGE * ($busqueda['page'] - 1))
                ->get();
        $paginator = new Paginator($query, $totalCount, self::CANT_BY_PAGE, $busqueda['page']);
        return $paginator;
	}

	function nuevotelefonos($nuevo)
	{
		$model = new mBlacklist();
		return $model->NuevoTelefonoEmpresa($nuevo);
	}
	function gettelefonosexistentes($datos)
	{
		$model = new mBlacklist();
		return $model->gettelefonosexistentes($datos);
	}
}
?>