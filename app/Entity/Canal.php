<?php

namespace App\Entity;

class Canal extends \App\Entity\Base\Entity {


    const EJECUTIVO_NEGOCIO = 'EN';
    const CALL_CENTER = 'CALL';
    const ASISTENTE_COMERCIAL = 'AC';
    const NUBE = 'NUBE';
    const EJECUTIVO_CALL = 'CALL';

    static function getCanalesEjecutivo(){
    	return [self::EJECUTIVO_NEGOCIO/*,self::EJECUTIVO_CALL*/];
    }

    static function getCanalesAsistente(){
    	return [self::ASISTENTE_COMERCIAL,self::EJECUTIVO_NEGOCIO/*,self::EJECUTIVO_CALL*/];
    }

    static function getCanalesCall(){
        return [self::CALL_CENTER,/*self::EJECUTIVO_CALL,*/ self::EJECUTIVO_NEGOCIO];
    }

    static function getAll(){
        return [self::ASISTENTE_COMERCIAL,self::EJECUTIVO_NEGOCIO,self::CALL_CENTER,self::NUBE/*,self::EJECUTIVO_CALL*/];
    }

}