<?php

namespace App\Entity\BE;

use App\Model\BE\ActividadParticipante as mActividadParticipante;
use Jenssegers\Date\Date as Carbon;

class ActividadParticipante extends \App\Entity\Base\Entity {

    protected $_tipoParticipante;
    protected $_actividadId;
    protected $_idParticipante;
    protected $_registroEn;

    static function getFeedbacks() {
        return [self::FEEDBACK_POSITIVO, self::FEEDBACK_NEGATIVO];
    }

    static function getAreasValidas(){
        return [
            'ADMINISTRADOR',
            'AUDITORÍA',
            'BANCA EMPRESA',
            'BANCA PEQUEÑA EMPRESA',
            'BT COMERCIAL',
            'ESCOM',
            'FIES',
            'FINCORP',
            'GDH',
            'GTP',
            'MESA DE DINERO',
            'PLANILLAS',
            'PPYSS',
            'PPYSS - COMERCIAL',
            'RIESGOS',
            'VPC',
            'BANCA CORPORATIVA',
            'PRODUCTOS'
        ];
    }

    function setValueToTable() {
        return $this->cleanArray([
                    'TIPO_PARTICIPANTE' => $this->_tipoParticipante,
                    'ACTIVIDAD_ID' => $this->_actividadId,
                    'ID_CONTACTO' => $this->_idParticipante,
                    'REGISTRO_EN' => $this->_registroEn,
        ]);
    }

    function listarParticipantes($numDoc) {
        $model = new mActividadParticipante();
        //return $participantes -> $model::
    }

    static function autocompleteParticipantes($termino, $ejecutivo = null) {
        $model = new mActividadParticipante();
        return $model->getAutocompleteParticipante(
                        self::sgetPeriodoBE(), $termino, $ejecutivo,self::getAreasValidas())->get();
    }

}
