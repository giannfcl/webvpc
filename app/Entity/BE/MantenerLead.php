<?php

namespace App\Entity\BE;

use App\Model\BE\MantenerLead as mMantenerLead;
use Jenssegers\Date\Date as Carbon;

class MantenerLead extends \App\Entity\Base\Entity {


    protected $_periodo;
    protected $_numdoc;
    protected $_fechaRegistro;
    protected $_jefe;
    protected $_estrategia;
    protected $_etapa;
    protected $_diasPendiente;
    

    function setValueToTable() {
        return $this->cleanArray([
            'PERIODO' => $this->_periodo,
            'NUM_DOC' => $this->_numdoc,
            'FECHA_REGISTRO' => ($this->_fechaRegistro)? $this->_fechaRegistro->toDateTimeString() : null,
            'REGISTRO_EN' => $this->_jefe,
            'ID_CAMP_EST' => $this->_estrategia,
            'ETAPA_ID' => $this->_etapa,
            'DIAS_PENDIENTE' => $this->_diasPendiente,
        ]);
    }

    function insert(\App\Entity\Usuario $en){
        $lead = Lead::getLeadsEjecutivo($en, ['documento' => $this->_numdoc])->first();
        if ($lead){
            $this->_periodo = $this->getPeriodoBE();
            $this->_fechaRegistro = Carbon::now();
            $this->_jefe = $en->getValue('_registro');
            $this->_estrategia = $lead->ESTRATEGIA_ID;
            $this->_etapa = $lead->ETAPA_ID;
            $this->_diasPendiente = $lead->DIAS_PENDIENTE;

            $model = new mMantenerLead();
            if ($model->insert($this->setValueToTable())){
                return true;
            }else{
                $this->setMessage('Hubo un error al ingresar su información, inténtelo nuevamente');
                return false;
            }

        }else{
            $this->setMessage('El lead seleccionado no se encuentra activo');
            return false;
        }
    }

    function delete(\App\Entity\Usuario $en){
        $lead = Lead::getLeadsEjecutivo($en, ['documento' => $this->_numdoc])->first();
        if ($lead){
            $model = new mMantenerLead();
            if ($model->quitar($this->getPeriodoBE(),$this->_numdoc)){
                return true;
            }else{
                $this->setMessage('Hubo un error al ingresar su información, inténtelo nuevamente');
                return false;
            }

        }else{
            $this->setMessage('El lead seleccionado no se encuentra activo');
            return false;
        }
    }
}