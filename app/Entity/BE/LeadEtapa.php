<?php

namespace App\Entity\BE;

use App\Model\BE\LeadEtapa as mLeadEtapa;
use App\Entity\BE\AccionComercial as AccionComercial;
use App\Model\BE\Lead as mLead;
use Jenssegers\Date\Date as Carbon;

class LeadEtapa extends \App\Entity\Base\Entity {

    protected $_periodo;
    protected $_numdoc;
    protected $_etapa;
    protected $_estrategia;
    protected $_ejecutivo;
    protected $_ejecutivoAsignado;
    protected $_fechaRegistro;
    protected $_flgUltimo;
    protected $_tooltip;


    function setProperties($data) {
    }

    function setValueToTable() {
        return $this->cleanArray([
            'PERIODO' => $this->_periodo,
            'NUM_DOC' => $this->_numdoc,
            'ID_ETAPA' => $this->_etapa,
            'ID_CAMP_EST' => $this->_estrategia,
            'REGISTRO_EN' => $this->_ejecutivo,
            'REGISTRO_EN_ASIGNADO' => $this->_ejecutivoAsignado,
            'FECHA_REGISTRO' => ($this->_fechaRegistro)? $this->_fechaRegistro->toDateTimeString() : null,
            'FLG_ULTIMO' => $this->_flgUltimo,
            'TOOLTIP' => $this->_tooltip, 
        ]);


    }

    function cambiarEtapa(\App\Entity\Usuario $usuario,$tipo=null){
        $hoy=Carbon::now();
        $this->_periodo = $this->getPeriodoBE();

        if($tipo!='accion'){
            $this->_fechaRegistro = Carbon::now();
            $lead = Lead::getLeadsEjecutivo($usuario,['documento' => $this->_numdoc])->first();

            if (!$lead){
                $this->setMessage('Lead no disponible');
                return false;
            }

            $this->_estrategia = $lead->ESTRATEGIA_ID;
            $this->_ejecutivoAsignado = $lead->REGISTRO_EN;
            $this->_flgUltimo = 1;

            $actividad = new Actividad();
            $actividad->setValues([
                '_numdoc' => $this->_numdoc,
                '_ejNegocio' => $this->_ejecutivo,
                '_fechaActividad' => $this->_fechaRegistro,
                '_ejNegocio' => $lead->REGISTRO_EN,
                '_titulo' => 'ME INTERESA',
                '_tipo' => Actividad::TIPO_CAMBIO_ESTADO,
                '_periodo' => $this->_periodo,
                '_idCampEst' => $this->_estrategia

            ]);

            $model = new mLeadEtapa();
            return $model->actualizar($this->setValueToTable(),$actividad->setValueToTable());
        }
        else{

            $accion = AccionComercial::getClientesEjecutivo($usuario,
                ['documento' => $this->_numdoc,
                'accion'=>$this->_estrategia,
                'tooltip'=>$this->_tooltip])->first(); 

            $etapaAccion=AccionComercial::getEtapaAccion($this->_estrategia,$this->_etapa)->first();
               

            if (!$accion){
                $this->setMessage('Acción no disponible');
                return false;
            }

            //Conseguimos al Encargado (Cash o de Negocios)
            if($accion->ENCARGADO_EN==1)
                $registroEjecutivo=$accion->EN;      
            else if ($accion->ENCARGADO_EC==1)
                $registroEjecutivo=$accion->EC;      

            $this->_fechaRegistro = Carbon::now();
            $this->_fechaActualizacion = Carbon::now();
            $this->_estrategia = $accion->ID_ACCION;
            $this->_ejecutivo = $registroEjecutivo;
            $this->_ejecutivoAsignado = $registroEjecutivo;
            $this->_flgUltimo = 1;
            $this->_tooltip=$accion->TOOLTIP;


            $actividad = new Actividad();
            $actividad->setValues([
                '_numdoc' => $this->_numdoc,               
                '_fechaActividad' => $this->_fechaRegistro,
                '_ejNegocio' => $this->_ejecutivoAsignado,
                '_titulo' => $etapaAccion->NOMBRE_ETAPA,
                '_tipo' => Actividad::TIPO_CAMBIO_ESTADO,
                '_periodo' => $this->_periodo,
                '_idCampEst' => $this->_estrategia
            ]);


            $model = new mLeadEtapa();            
            $result=$model->actualizar($this->setValueToTable(),$actividad->setValueToTable(),$tipo);

            /*
                if ($result){
                    $model->actualizarInicio($this->setValueToTable(),$hoy);
                }
            */
            return $result;
        }
    }

}