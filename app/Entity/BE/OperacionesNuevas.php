<?php

namespace App\Entity\BE;

use Jenssegers\Date\Date as Carbon;
use App\Model\BE\OperacionesNuevas as mOperacionesNuevas;

class OperacionesNuevas extends \App\Entity\Base\Entity {

	protected $_id;
    protected $_fechaDesembolso;
    protected $_codUnico;
    protected $_documento;
    protected $_registro;
    protected $_producto;
    protected $_montoCertero;
    protected $_montoCotizacion;
	protected $_flagDesembolso;
	protected $_flagPerdida;
    protected $_accionComercial;
    protected $_tipoOperacion;
    protected $_usuario;
    protected $_fechaActualizacion;

    protected $_tipoMonto;
    protected $_monto;

    const TIPO_OPERACION_OPERACION_NUEVA = 1;
    const TIPO_OPERACION_PRECANCELACION = 2;

    const TIPO_MONTO_CERTERO = 'certero';
    const TIPO_MONTO_COTIZACION = 'cotizacion';


	function setValueToTable() {
        return $this->cleanArray([
            'ID_OPERACION' => $this->_id,
            'FECHA_DESEMBOLSO' => $this->_fechaDesembolso? $this->_fechaDesembolso:null ,
            'COD_UNICO' => $this->_codUnico,
            'NUM_DOC' => $this->_documento,
            'REGISTRO_EN' => $this->_registro,
            'ID_PRODUCTO' => $this->_producto,
            'MONTO_CERT' => $this->_montoCertero,
            'MONTO_ENCOT' => $this->_montoCotizacion,
            'FLG_DESEMBOLSADO' => $this->_flagDesembolso,
            'FLG_PERDIDO' => $this->_flagPerdida,
            'ID_CAMP_EST' => $this->_accionComercial,
            'ID_TIPO_OPERACION' => $this->_tipoOperacion,
            'REG_USUARIO' => $this->_usuario,
            'FECHA_ACT' => $this->_fechaActualizacion? $this->_fechaActualizacion->toDateTimeString() : Carbon::now()->toDateTimeString()
        ]);
    }

    function massiveUpdate($items){
        $model = new mOperacionesNuevas();

        $operaciones = array();
        foreach($items as $item){
            $operaciones[] = $item->setValueToTable();
        }

        if ($model->massiveUpdate($operaciones)){
            $this->setMessage('Los datos han sido actualizados correctamente');
            return true;
        }else{
            $this->setMessage('Error en petición a la base de datos');
            return false;
        }
    }

    static function getOperacionesPerdidas($periodo,$jefatura = null,$zonal = null,$banca = null){        
        $model = new mOperacionesNuevas();
        return $model->getOperacionesPerdidas($periodo,$jefatura,$zonal,$banca)->get();    
    }
    static function getDesembolsosCotizacionFuturos($periodo,$jefatura = null,$zonal = null,$banca = null){        
        $model = new mOperacionesNuevas();
        return $model->getDesembolsosCotizacionFuturos($periodo,$jefatura,$zonal,$banca)->get();    
    }
    static function getOperacionesNuevas(\App\Entity\Usuario $en, $periodo,$ejecutivo = null){
    	$model = new mOperacionesNuevas();
        return $model->getOperacionesNuevas($periodo? $periodo:self::sgetPeriodoBE(),$ejecutivo? $ejecutivo:$en->getValue('_registro'))->get();
    }

    static function getDesembolsosCerteros($periodo,$jefatura = null,$zonal = null,$banca = null){
        $model = new mOperacionesNuevas();
        return $model->getOperacionesNuevas($periodo? $periodo: self::sgetPeriodoBE(),null,$jefatura,$zonal,$banca,true)->get();    
    }

    static function getDesembolsosCotizacion($periodo,$jefatura = null,$zonal = null,$banca = null){
        $model = new mOperacionesNuevas();
        return $model->getOperacionesNuevas($periodo? $periodo: self::sgetPeriodoBE(),null,$jefatura,$zonal,$banca,null,true)->get();    
    }
    static function getCompromiso($periodo,$ejecutivo = null, $jefatura = null,$zonal = null,$banca = null){
        $model = new mOperacionesNuevas();        
        return $model->getCompromiso($periodo? $periodo: self::sgetPeriodoBE(),$ejecutivo,$jefatura,$zonal,$banca)->get()->keyBy('TIPO_PRODUCTO');       
    }
    static function getProyeccionOperacionesIniciales($periodo,$ejecutivo = null, $jefatura = null,$zonal = null,$banca = null){
        $model = new mOperacionesNuevas();
        $data = $model->getProyeccionOperacionesIniciales($periodo? $periodo: self::sgetPeriodoBE(),$ejecutivo,$jefatura,$zonal,$banca)->get()->keyBy('FECHA_DESEMBOLSO');
        return $data;
    }
    static function getProyeccionOperacionesNuevas($periodo,$ejecutivo = null, $jefatura = null,$zonal = null,$banca = null){
        $model = new mOperacionesNuevas();
        $data = $model->getProyeccionOperacionesNuevas($periodo? $periodo: self::sgetPeriodoBE(),$ejecutivo,$jefatura,$zonal,$banca)->get()->keyBy('FECHA_DESEMBOLSO');
        return $data;
    }

    static function test(){
    	$model = new mOperacionesNuevas();
        return $model->test()->get();
    }


    static function getMesesOperacion(){
        $lista = [];
        
        $model = new mOperacionesNuevas(); 
        $periodos = $model->getMesesOperacion()->get();

        foreach ($periodos as $periodo) {
            $actual = Carbon::createFromFormat('Ymd', $periodo->PERIODO.'01');    
            $lista[] =   ['key'=> $actual->format('Ym') ,'value'=> $actual->format('F Y')];
        }   
        return $lista;
    }

    function insert(){

        if ($this->_tipoMonto == self::TIPO_MONTO_CERTERO){
            $this->_montoCotizacion = 0;
            $this->_montoCertero = $this->_monto;
        }
        if ($this->_tipoMonto == self::TIPO_MONTO_COTIZACION){
            $this->_montoCertero = 0;
            $this->_montoCotizacion = $this->_monto;   
        }
        
        //los precancelacion son negativos
        if ($this->_tipoOperacion == self::TIPO_OPERACION_PRECANCELACION){
            $this->_montoCertero = $this->_montoCertero;
            $this->_montoCotizacion = $this->_montoCertero;
        }
        


        $this->_flagDesembolso = 0;
        $this->_flagPerdida = 0;

        $model = new mOperacionesNuevas();
        if ($model->registrar($this->setValueToTable())){
            return true;
        }else{
            return false;
        }

    }
}
