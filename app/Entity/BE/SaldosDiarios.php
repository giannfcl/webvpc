<?php

namespace App\Entity\BE;

use Jenssegers\Date\Date as Carbon;
use App\Model\BE\SaldosDiarios as mSaldosDiarios;

class SaldosDiarios extends \App\Entity\Base\Entity {

    const ESCALA = 1000;
    const  ESCALA_JEFE = 1000000;
    
										
    static function getGraficoDirectas(\App\Entity\Usuario $en, $periodo,$ejecutivo = null){									
    	$model = new \App\Model\BE\SaldosDiarios();
    	return $model->getGraficoDirectas($ejecutivo? $ejecutivo:$en->getValue('_registro'),$periodo);        
    }

    static function getGraficoIndirectas(\App\Entity\Usuario $en, $periodo,$ejecutivo = null){
    	$model = new \App\Model\BE\SaldosDiarios();
    	return $model->getGraficoIndirectas($ejecutivo? $ejecutivo:$en->getValue('_registro'),$periodo);
    }

    static function getLastSaldoPunta($periodo,$registro = null,$jefatura= null,$zonal= null,$banca= null){
        $model = new \App\Model\BE\SaldosDiarios();
        \Debugbar::info($periodo);
        return $model->getLastSaldoPunta($periodo,$registro,$jefatura,$zonal,$banca)->first();
    }

    static function process($periodo,$usuario,$ejecutivo,$jefatura=null,$banca=null,$zonal=null,$index='ejecutivo'){
        \Debugbar::info($periodo);
        if($index!='ejecutivo'){
            $escala = self::ESCALA_JEFE;
        }
        else{
            $escala = self::ESCALA;
        }
        //$usuario = $ejecutivo? $ejecutivo: $usuario;

        $model = new \App\Model\BE\SaldosDiarios();
        
        #saldo punta del cierre mes anterior
        #$saldoAnterior = 

        $sPuntas = SaldosDiarios::getLastSaldoPunta($periodo,$ejecutivo,$jefatura,$zonal,$banca);
        $desembolsos = OperacionesNuevas::getProyeccionOperacionesNuevas($periodo,$ejecutivo,$jefatura,$zonal,$banca);
        $caidas = VencimientoAmortizacion::getProyeccionVencAmort($periodo,$ejecutivo,$jefatura,$zonal,$banca);
        $saldos = $model->getSaldos($periodo,$ejecutivo,$jefatura,$zonal,$banca)->get()->keyBy('FECHA');        
        
        /*proyecciones*/
        $Desembcompromiso = OperacionesNuevas::getProyeccionOperacionesIniciales($periodo,$ejecutivo,$jefatura,$zonal,$banca);
        $caidasCompromiso = VencimientoAmortizacion::getProyeccionVencAmortInicial($periodo,$ejecutivo,$jefatura,$zonal,$banca); 
        $compromiso = OperacionesNuevas::getCompromiso($periodo,$ejecutivo,$jefatura,$zonal,$banca);
        /*certer*/          
        $cert = SaldosDiarios::getProyeccionCertera($periodo,$saldos,$desembolsos,$caidas);           
        \Debugbar::info($compromiso);
        
        //Si no hay nada
        if(!$sPuntas){
            return [];
        }

        //Fecha de Saldo Diario
        $fechaSD = Carbon::createFromFormat('Y-m-d', $sPuntas->FECHA);
        $fechaUltimo = Carbon::createFromFormat('Y-m-d',$sPuntas->FECHA);
        if(SaldosDiarios::getParametros('PERIODO',null)==$periodo){            
            $fechaInicio = Carbon::createFromFormat('Y-m-d', $sPuntas->FECHA)->addDay(-17); 
            $intervalo =35; 
            $fechaUltimo = Carbon::createFromFormat('Y-m-d', $sPuntas->FECHA)->lastOfMonth();            
        }else{
            //COmenzamos el gráfico 2 semanas antes (saldos)            
            $fechaInicio = Carbon::createFromFormat('Y-m-d',SaldosDiarios::getParametros('FECHA_INI_SD',$periodo));
            $intervalo = $fechaInicio->diffInDaysFiltered(function(Carbon $date) {return !$date->isWeekend();},$fechaSD,false) + 1;
        }
        
        //\Debugbar::info($desembolsos);
        //\Debugbar::info($fechaUltimo);
        $result = array();
        $i = 0;

        $acumDirecta = 0;
        $acumIndirecta = 0;
        #caso acomulacion de mes siguiente 
        $saldoPuntaDirectaProx = 0;
        $saldoPrimDirectaProx = 0;
        $saldoPuntaIndirectaProx = 0;
        $saldoPromIndirectaProx = 0;
        $ProyDirectasProx = 0;
        $ProyIndirectasProx = 0;
        $vencDirectaProx = 0;
        $amortDirectaProx = 0;
        $DesembDirectaProx = 0;
        $preCancelaDirectaProx = 0;
        $vencIndirectaProx = 0;
        $amortIndirectaProx = 0;
        $DesembIndirectaProx = 0;
        $preCancelaIndirectaProx = 0;   


                $acumDirectaProyC = 0; 
                $acumIndirectaProyC = 0;
                $acumDirectaPrevio =0;
                $acumIndirectaPrevio = 0;
                $varuno =0;
                $vardos = 0;
        //El gráfico tendra x días desde la $fechaInicio
        while ($i <$intervalo) {

            // Si el día es el despues de la fecha Saldos Diarios, seteamos por primera vez la proyeccion como el saldo punta
            $diferencia = $fechaInicio->diffInDaysFiltered(function(Carbon $date) {return !$date->isWeekend();},$fechaSD,false);            
            
            /*si excede el ultimo dia del mes*/            
            $difMeses = (integer)$fechaInicio->month - (integer)$fechaUltimo->month;                    
            $difAño = (integer)$fechaInicio->year - (integer)$fechaUltimo->year;                    
            
            $dif = 0;
            
            /*diferencia entre la fecha cierre y la fecha sd*/                                
            if($difMeses==1 || $difAño>0){
                $dif = 1;                
            }                        
            if(!$fechaInicio->isWeekend() || $diferencia < 0){
                
                // LOGICA PARA PROYECCIONES
                // *PENDIENTE SEPARAR VENCIMIENTOS DE AMORTIZACIONES

                //Busco vencimientos/amortz/desembolsos 
                
                $vencimientoDirecta = (isset($caidas[$fechaInicio->toDateString()])? $caidas[$fechaInicio->toDateString()]->MONTO_CAIDA_DIR_VENC : 0);
                $amortizacionDirecta = (isset($caidas[$fechaInicio->toDateString()])? $caidas[$fechaInicio->toDateString()]->MONTO_CAIDA_DIR_AMORT : 0);
                $desembolsoDirecta = (isset($desembolsos[$fechaInicio->toDateString()])? $desembolsos[$fechaInicio->toDateString()]->MONTO_DIR_NUEVO : 0);
                $precanDirecta = (isset($desembolsos[$fechaInicio->toDateString()])? $desembolsos[$fechaInicio->toDateString()]->MONTO_DIR_PRECAN : 0);
                
                $vencimientoIndirecta = (isset($caidas[$fechaInicio->toDateString()])? $caidas[$fechaInicio->toDateString()]->MONTO_CAIDA_IND_VENC : 0);
                $amortizacionIndirecta = (isset($caidas[$fechaInicio->toDateString()])? $caidas[$fechaInicio->toDateString()]->MONTO_CAIDA_IND_AMORT : 0);
                $desembolsoIndirecta = (isset($desembolsos[$fechaInicio->toDateString()])? $desembolsos[$fechaInicio->toDateString()]->MONTO_IND_NUEVO : 0);
                $precanIndirecta = (isset($desembolsos[$fechaInicio->toDateString()])? $desembolsos[$fechaInicio->toDateString()]->MONTO_IND_PRECAN : 0);

                /*proyecciones*/
                $compAmortizacionDirecta = (isset($caidasCompromiso[$fechaInicio->toDateString()])? $caidasCompromiso[$fechaInicio->toDateString()]->MONTO_CAIDA_DIR_AMORT : 0);
                $compVencimientoDirecta = (isset($caidasCompromiso[$fechaInicio->toDateString()])? $caidasCompromiso[$fechaInicio->toDateString()]->MONTO_CAIDA_DIR_VENC : 0);
                $comDesembolsoDirecta = (isset($Desembcompromiso[$fechaInicio->toDateString()])? $Desembcompromiso[$fechaInicio->toDateString()]->MONTO_DIR_NUEVO : 0);
                $comPrecanDirecta = (isset($Desembcompromiso[$fechaInicio->toDateString()])? $Desembcompromiso[$fechaInicio->toDateString()]->MONTO_DIR_PRECAN : 0);

                $compAmortizacionIndirecta = (isset($cacidasCompromiso[$fechaInicio->toDateString()])? $caidasCompromiso[$fechaInicio->toDateString()]->MONTO_CAIDA_DIR_AMORT : 0);
                $compVencimientoDIndirecta = (isset($caidasCompromiso[$fechaInicio->toDateString()])? $caidasCompromiso[$fechaInicio->toDateString()]->MONTO_CAIDA_IND_VENC : 0);
                $comDesembolsoIndirecta = (isset($Desembcompromiso[$fechaInicio->toDateString()])? $Desembcompromiso[$fechaInicio->toDateString()]->MONTO_IND_NUEVO : 0);
                $comPrecanIndirecta = (isset($Desembcompromiso[$fechaInicio->toDateString()])? $Desembcompromiso[$fechaInicio->toDateString()]->MONTO_IND_PRECAN : 0);
                
               

                if($acumDirectaPrevio==0){    
                     //\Debugbar::info($fechaInicio->toDateString());                       
                     $acumDirectaProyC = isset($saldos[$fechaInicio->toDateString()]) ? $saldos[$fechaInicio->toDateString()]->SALDO_PUNTA_DIRECTAS : 0;
                    $acumIndirectaProyC = isset($saldos[$fechaInicio->toDateString()]) ? $saldos[$fechaInicio->toDateString()]->SALDO_PUNTA_INDIRECTAS : 0;
                }else{
                    $acumDirectaProyC = $acumDirectaPrevio + $desembolsoDirecta - $amortizacionDirecta - $vencimientoDirecta - $precanDirecta;
                    $acumIndirectaProyC = $acumIndirectaPrevio + $desembolsoIndirecta - $amortizacionIndirecta - $vencimientoIndirecta - $precanIndirecta;  
                }

                $acumDirectaPrevio = isset($saldos[$fechaInicio->toDateString()]) ? $saldos[$fechaInicio->toDateString()]->SALDO_PUNTA_DIRECTAS : 0;
                $acumIndirectaPrevio = isset($saldos[$fechaInicio->toDateString()]) ? $saldos[$fechaInicio->toDateString()]->SALDO_PUNTA_INDIRECTAS : 0;                                
                //\Debugbar::info($precanDirecta);
                //Las proyecciones deben iniciarse despues de la fecha de saldo diarios, si es antes entonces tiene valor 0 para que no aparezca en la barra en el grafico                    
                if($fechaInicio->lte($fechaSD)){
                    $acumDirecta = 0;
                    $acumIndirecta = 0;                    
                    $acumPrevioDirecto =$sPuntas->SALDO_PUNTA_DIRECTAS;
                    $acumPrevioIndirecto =$sPuntas->SALDO_PUNTA_INDIRECTAS;                 
                }
                else{   
                        // Si el día es el despues de la fecha Saldos Diarios, seteamos por primera vez la proyeccion como el saldo punta                    
                        if ($diferencia == -1){
                            $acumDirecta = $acumPrevioDirecto + $desembolsoDirecta - $amortizacionDirecta - $vencimientoDirecta - $precanDirecta;
                            $acumIndirecta = $acumPrevioIndirecto+ $desembolsoIndirecta - $amortizacionIndirecta - $vencimientoIndirecta - $precanIndirecta;
                        }else{
                        
                             // Caso contrario vamos acumulando (+desembolso) (-amortizacion) (-vencimiento)
                            $acumDirecta = $acumDirecta + $desembolsoDirecta - $amortizacionDirecta - $vencimientoDirecta - $precanDirecta;
                            $acumIndirecta = $acumIndirecta + $desembolsoIndirecta - $amortizacionIndirecta - $vencimientoIndirecta - $precanIndirecta;                        
                        }

                        if($fechaInicio>=$fechaSD){
                                $acumDirectaProyC = $acumDirecta;                                
                                $acumIndirectaProyC = $acumIndirecta;                                
                        }
                    /*Acomular la info del siguiente mes*/                           
                        if($dif == 1){                             
                            //\Debugbar::info((string)$fechaInicio);
                            $mesProx = $fechaInicio->month;
                            $añoProx = $fechaInicio->year;

                            $saldoPuntaDirectaProx = $saldoPuntaDirectaProx + isset($saldos[$fechaInicio->toDateString()]) ? number_format($saldos[$fechaInicio->toDateString()]->SALDO_PUNTA_DIRECTAS/$escala,0,'','') : 0;
                            $saldoPrimDirectaProx = $saldoPrimDirectaProx + isset($saldos[$fechaInicio->toDateString()]) ? number_format($saldos[$fechaInicio->toDateString()]->SALDO_PUNTA_DIRECTAS/$escala,0,'','') : 0;
                            $saldoPuntaIndirectaProx =$saldoPuntaIndirectaProx + isset($saldos[$fechaInicio->toDateString()]) ? number_format($saldos[$fechaInicio->toDateString()]->SALDO_PUNTA_DIRECTAS/$escala,0,'','') : 0;
                            $saldoPromIndirectaProx =$saldoPromIndirectaProx + isset($saldos[$fechaInicio->toDateString()]) ? number_format($saldos[$fechaInicio->toDateString()]->SALDO_PROMEDIO_DIRECTAS/$escala,0,'','') : 0;

                            $ProyDirectasProx = number_format($acumDirectaProyC/$escala,0,'','');
                            $ProyIndirectasProx = number_format($acumIndirectaProyC/$escala,0,'','');


                            $vencDirectaProx =$vencDirectaProx +  number_format($vencimientoDirecta/$escala,0,'','');
                            $amortDirectaProx =$amortDirectaProx +  number_format($amortizacionDirecta/$escala,0,'','');
                            $DesembDirectaProx =$DesembDirectaProx +  number_format($desembolsoDirecta/$escala,0,'','');
                            $preCancelaDirectaProx =$preCancelaDirectaProx +  number_format($precanDirecta/$escala,0,'','');

                            
                            $vencIndirectaProx =$vencIndirectaProx +  number_format($vencimientoIndirecta/$escala,0,'','');
                            $amortIndirectaProx =$amortIndirectaProx +  number_format($amortizacionIndirecta/$escala,0,'','');
                            $DesembIndirectaProx =$DesembIndirectaProx +  number_format($desembolsoIndirecta/$escala,0,'','');
                            $preCancelaIndirectaProx =$preCancelaIndirectaProx +  number_format($precanIndirecta/$escala,0,'','');

                            /*
                            $saldoProyCertDir = isset($cert[$fechaInicio->toDateString()]) ? number_format($cert[$fechaInicio->toDateString()]['SALDO_PROYECTADO_DIR']/$escala,0,'','') : null;
                            $saldoProyCertInd = isset($cert[$fechaInicio->toDateString()]) ? number_format($cert[$fechaInicio->toDateString()]['SALDO_PROYECTADO_IND']/$escala,0,'','') : null;
                            */

                            $saldoProyCertDir = number_format($acumDirectaProyC/$escala,0,'','');;
                            $saldoProyCertInd = number_format($acumIndirectaProyC/$escala,0,'','');;
                            
                            $MontoCompromisoDir = isset($compromiso['DIRECTAS']) ? number_format($compromiso['DIRECTAS']->COMPROMISO/$escala,0,'','') : null;
                            $MontoCompromisoInd = isset($compromiso['INDIRECTAS']) ? number_format($compromiso['INDIRECTAS']->COMPROMISO/$escala,0,'','') : null;
                        }   

                }   
                
                $valSaldos = isset($saldos[$fechaInicio->toDateString()]) ? $saldos[$fechaInicio->toDateString()]->SALDO_PUNTA_DIRECTAS : null;
                $valProyecDir = $acumDirecta;
                $valProyecInd = $acumIndirecta;
                
                if($valProyecDir==0 && $valProyecInd==0 && !$valSaldos || $dif == 1){
                    //\Debugbar::info('Feriado');
                }else{                    
                    $result[] = [
                        'FECHA'=>$fechaInicio->toDateString(),
                        'DIA' => $fechaInicio->day,
                        'MES' => $fechaInicio->month,
                        'AÑO'=> $fechaInicio->year,
                        'SALDO_PUNTA_DIRECTAS' => isset($saldos[$fechaInicio->toDateString()]) ? number_format($saldos[$fechaInicio->toDateString()]->SALDO_PUNTA_DIRECTAS/$escala,0,'','') : null,
                        'SALDO_PROMEDIO_DIRECTAS' => isset($saldos[$fechaInicio->toDateString()]) ? number_format($saldos[$fechaInicio->toDateString()]->SALDO_PROMEDIO_DIRECTAS/$escala,0,'','') : null,
                        'SALDO_PUNTA_INDIRECTAS' => isset($saldos[$fechaInicio->toDateString()]) ? number_format($saldos[$fechaInicio->toDateString()]->SALDO_PUNTA_INDIRECTAS/$escala,0,'','') : null,
                        'SALDO_PROMEDIO_INDIRECTAS' => isset($saldos[$fechaInicio->toDateString()]) ? number_format($saldos[$fechaInicio->toDateString()]->SALDO_PROMEDIO_INDIRECTAS/$escala,0,'','') : null,

                        'PROYECCION_DIRECTAS' => number_format($acumDirectaProyC/$escala,0,'',''),
                        'PROYECCION_INDIRECTAS' => number_format($acumIndirectaProyC/$escala,0,'',''),

                        'VENCIMIENTO_DIRECTA' =>  number_format($vencimientoDirecta/$escala,0,'',''),
                        'AMORTIZACION_DIRECTA' =>  number_format($amortizacionDirecta/$escala,0,'',''),
                        'DESEMBOLSO_DIRECTA' =>  number_format($desembolsoDirecta/$escala,0,'',''),
                        'PRE_CANCELACIONES_DIRECTA' =>  number_format($precanDirecta/$escala,0,'',''),

                        'VENCIMIENTO_INDIRECTA' =>  number_format($vencimientoIndirecta/$escala,0,'',''),
                        'AMORTIZACION_INDIRECTA' =>  number_format($amortizacionIndirecta/$escala,0,'',''),
                        'DESEMBOLSO_INDIRECTA' =>  number_format($desembolsoIndirecta/$escala,0,'',''),
                        'PRE_CANCELACIONES_INDIRECTA' =>  number_format($precanIndirecta/$escala,0,'',''),
                        /*
                        'SALDO_PROYECTADO_DIR' => isset($cert[$fechaInicio->toDateString()]) ? number_format($cert[$fechaInicio->toDateString()]['SALDO_PROYECTADO_DIR']/$escala,0,'','') : null,
                        'SALDO_PROYECTADO_IND' => isset($cert[$fechaInicio->toDateString()]) ? number_format($cert[$fechaInicio->toDateString()]['SALDO_PROYECTADO_IND']/$escala,0,'','') : null,
                        */
                        'SALDO_PROYECTADO_DIR' => number_format($acumDirectaProyC/$escala,0,'',''),
                        'SALDO_PROYECTADO_IND' => number_format($acumIndirectaProyC/$escala,0,'',''),

                        'MONTO_COMPROMISO_DIR' => isset($compromiso['DIRECTAS']) ? number_format($compromiso['DIRECTAS']->COMPROMISO/$escala,0,'','') : null,
                        'MONTO_COMPROMISO_IND' => isset($compromiso['INDIRECTAS']) ? number_format($compromiso['INDIRECTAS']->COMPROMISO/$escala,0,'','') : null,
                    ];
                }
                $i++;
            }
            $fechaInicio->addDay();
        }  
        if(SaldosDiarios::getParametros('PERIODO',null)==$periodo){ 
                    $result[] = [
                        'FECHA'=>$fechaInicio->toDateString(),
                        'DIA' => 1,
                        'MES' => $mesProx,
                        'AÑO'=> $añoProx,
                        'SALDO_PUNTA_DIRECTAS' => $saldoPuntaDirectaProx,
                        'SALDO_PROMEDIO_DIRECTAS' => $saldoPrimDirectaProx,
                        'SALDO_PUNTA_INDIRECTAS' => $saldoPuntaIndirectaProx,
                        'SALDO_PROMEDIO_INDIRECTAS' => $saldoPromIndirectaProx,

                        'PROYECCION_DIRECTAS' => $ProyDirectasProx,
                        'PROYECCION_INDIRECTAS' => $ProyIndirectasProx,

                        'VENCIMIENTO_DIRECTA' =>  $vencDirectaProx,
                        'AMORTIZACION_DIRECTA' =>  $amortDirectaProx,
                        'DESEMBOLSO_DIRECTA' =>  $DesembDirectaProx,
                        'PRE_CANCELACIONES_DIRECTA' =>  $preCancelaDirectaProx,

                        'VENCIMIENTO_INDIRECTA' =>  $vencIndirectaProx,
                        'AMORTIZACION_INDIRECTA' =>  $amortIndirectaProx,
                        'DESEMBOLSO_INDIRECTA' =>  $DesembIndirectaProx,
                        'PRE_CANCELACIONES_INDIRECTA' =>  $preCancelaIndirectaProx,
                        
                        /*
                        'SALDO_PROYECTADO_DIR' => $saldoProyCertDir,
                        'SALDO_PROYECTADO_IND' => $saldoProyCertInd,
                        */

                        'SALDO_PROYECTADO_DIR' => $saldoProyCertDir,
                        'SALDO_PROYECTADO_IND' => $saldoProyCertInd,

                        'MONTO_COMPROMISO_DIR' => $MontoCompromisoDir,
                        'MONTO_COMPROMISO_IND' => $MontoCompromisoInd,
                    ];
        }
        
        \Debugbar::info($result);
        return $result;

    }
    static function getProyeccionCertera($periodo,$saldos,$desembolsos,$caidas){
        
        /*
        $model = new \App\Model\BE\SaldosDiarios();
        #generar la fecha de cierre Anterior
        $saldos = $model->getSaldos($periodo,$ejecutivo,$jefatura,$zonal,$banca)->get()->keyBy('FECHA');        
        $desembolsos = OperacionesNuevas::getProyeccionOperacionesNuevas($periodo,$ejecutivo,$jefatura,$zonal,$banca);
        $caidas = VencimientoAmortizacion::getProyeccionVencAmort($periodo,$ejecutivo,$jefatura,$zonal,$banca);        
        */
        $fechaAnt = SaldosDiarios::getParametros('FECHA_INI_SD',$periodo);
        $fechaEva = Carbon::createFromFormat('Y-m-d', $fechaAnt);

        $saldoPuntaMesAntDir = isset($saldos[$fechaEva->toDateString()]) ? $saldos[$fechaEva->toDateString()]->SALDO_PUNTA_DIRECTAS : 0;
        $saldoPuntaMesAntInd = isset($saldos[$fechaEva->toDateString()]) ? $saldos[$fechaEva->toDateString()]->SALDO_PUNTA_INDIRECTAS : 0;
        #traer los montos certeros
        $intervalo = 45;#setear mes un medio de intervalo
        $i=0;
        while ($i<$intervalo) {
            if(!$fechaEva->isWeekend()){

                $vencimientoDirecta = (isset($caidas[$fechaEva->toDateString()])? $caidas[$fechaEva->toDateString()]->MONTO_CAIDA_DIR_VENC : 0);
                $amortizacionDirecta = (isset($caidas[$fechaEva->toDateString()])? $caidas[$fechaEva->toDateString()]->MONTO_CAIDA_DIR_AMORT : 0);
                $desembolsoDirecta = (isset($desembolsos[$fechaEva->toDateString()])? $desembolsos[$fechaEva->toDateString()]->MONTO_DIR_NUEVO : 0);
                $precanDirecta = (isset($desembolsos[$fechaEva->toDateString()])? $desembolsos[$fechaEva->toDateString()]->MONTO_DIR_PRECAN : 0);
                
                $vencimientoIndirecta = (isset($caidas[$fechaEva->toDateString()])? $caidas[$fechaEva->toDateString()]->MONTO_CAIDA_IND_VENC : 0);
                $amortizacionIndirecta = (isset($caidas[$fechaEva->toDateString()])? $caidas[$fechaEva->toDateString()]->MONTO_CAIDA_IND_AMORT : 0);
                $desembolsoIndirecta = (isset($desembolsos[$fechaEva->toDateString()])? $desembolsos[$fechaEva->toDateString()]->MONTO_IND_NUEVO : 0);
                $precanIndirecta = (isset($desembolsos[$fechaEva->toDateString()])? $desembolsos[$fechaEva->toDateString()]->MONTO_IND_PRECAN : 0);


                $saldoPuntaMesAntDir = $saldoPuntaMesAntDir + $desembolsoDirecta - $amortizacionDirecta - $vencimientoDirecta + $precanDirecta;
                $saldoPuntaMesAntInd = $saldoPuntaMesAntInd + $desembolsoIndirecta - $amortizacionIndirecta - $vencimientoIndirecta + $precanIndirecta; 

                                
                    $result = [
                        'fecha'=>$fechaEva->toDateString(),
                        'SALDO_PROYECTADO_DIR' => $saldoPuntaMesAntDir,
                        'SALDO_PROYECTADO_IND' => $saldoPuntaMesAntInd,
                        'desembolsoDir' => $desembolsoDirecta,
                        'desembolsoInd' => $desembolsoIndirecta,

                        'amortDir' => $amortizacionDirecta,
                        'amortInd' => $amortizacionIndirecta,

                        'precDir' =>  $precanDirecta,
                        'precInd' =>  $precanIndirecta,
                        'vencDir' =>  $vencimientoDirecta,
                        'vencInd' =>  $vencimientoIndirecta,
                    ];
                

                    if($i==0){
                        $resultr = collect([$result]);
                    }else{
                        $resultr->push($result);    
                    }
                    //$resultr = collect([$result]);                
            }
            $i++;
            $fechaEva->addDay();
        }
        #crear la lista de proyecciones certeras
        return $resultr->keyBy('fecha');#->keyby();
    }
    static function getParametros($parametro,$periodo){        
        $model = new mSaldosDiarios();        
        
        $data = $model->getParametros($periodo)->get();

        switch ($parametro) {
            case 'FECHA_ACT_SD':
                return $data[0]->FECHA_ACT_SD;
                break;
            case 'FECHA_ACT_VENC':
                return $data[0]->FECHA_ACT_VENC;
                break;
            case 'FECHA_INI_SD':
                return $data[0]->FECHA_INI_SD;
                break;
            case 'PERIODO':
                return $data[0]->PERIODO;
                break;                
            default:
                return $data[0]->FECHA_ACT_SD;
                break;
        }
    }
}



	