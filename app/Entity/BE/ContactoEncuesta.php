<?php

namespace App\Entity\BE;

use App\Model\BE\ContactoEncuesta as mContactoEncuesta;

class ContactoEncuesta extends \App\Entity\Base\Entity {

    protected $_id;
    protected $_tipo;
    protected $_flag;


    function setValueToTable() {
        return $this->cleanArray([
            'ID_CONTACTO' => $this->_id,
            'TIPO_CONTACTO' => $this->_tipo,
            'FLG_CONTACTO' => $this->_flag,
        ]);
    }

    function agregar(){
        $model = new mContactoEncuesta();
        if ($model->agregar($this->setValueToTable())){
            return true;
        }else{
            $this->setMessage('Hubo un error al registrar tu información');
            return false;
        }

    }

    function actualizar(){
        $model = new mContactoEncuesta();
        if ($model->actualizar($this->setValueToTable())){
            return true;
        }else{
            $this->setMessage('Hubo un error al registrar tu información');
            return false;
        }
    }

    function quitar(){
        $model = new mContactoEncuesta();
        if ($model->quitar($this->setValueToTable())){
            return true;
        }else{
            $this->setMessage('Hubo un error al registrar tu información');
            return false;
        }
    }
}
