<?php

namespace App\Entity\BE;

use Jenssegers\Date\Date as Carbon;
use App\Model\Infinity\Alertacartera as mAlertacartera;
use App\Entity\Notification as Notification;
use App\Model\Infinity\CLiente as mCLiente;

class SegVigCasos extends \App\Entity\Base\Entity {

    protected $_id;
    protected $_periodo;
    protected $_codunico;
    protected $_motivo;
    protected $_fecharegistro;
    protected $_flggestionado;
    protected $_fechagestionado;
    protected $_mensaje;

    function setProperties($data) {
        $this->setValues([
            '_id' => $data->ID,
            '_periodo' => $data->PERIODO,
            '_codunico' => $data->COD_UNICO,
            '_motivo' => $data->MOTIVO,
            '_fecharegistro' => $data->FECHA_REGISTRO,
            '_flggestionado' => $data->FLG_GESTIONADO,
            '_fechagestionado' => $data->FECHA_GESTIONADO,
            '_mensaje' => $data->MENSAJE,
        ]);
    }

    function setValueToTable() {
        return $this->cleanArray([
            'PERIODO' => $this->_periodo,
            'COD_UNICO' => $this->_codunico,
            'MOTIVO' => $this->_motivo,
            'FECHA_REGISTRO' => $this->_fecharegistro,
            'FLG_GESTIONADO' => $this->_flggestionado,
            'FECHA_GESTIONADO' => $this->_fechagestionado,
            'MENSAJE' => $this->_mensaje,
        ]);
    }

    function agregargestiongys($data)
    {
        $this->_periodo = self::sgetPeriodoInfinity();
        $this->_codunico = $data['codunico_gesgys'];
        $this->_motivo = 'GYS';
        $this->_fecharegistro = Carbon::now();
        $this->_flggestionado = 0;
        $this->_fechagestionado = null;
        $this->_mensaje = $data['comentario_gesgys'];
        $model = new mAlertacartera();
        if ($model->agregargestiongys($this->setValueToTable())) {
            
            $datos = mCLiente::datosForNotificationGys($data['codunico_gesgys']);
            $modelnotification = new Notification();
            $modelnotification->setValues([
                '_idTipo'=>$datos->ID_TIPO_NOTIFICACION,
                '_registro'=>$datos->REGISTRO,
                '_contenido'=>$datos->CONTENIDO,
                '_fechaNotificacion'=>Carbon::now(),
                '_flgLeido'=>$datos->FLG_LEIDO,
                '_valor1'=>$datos->VALOR_1,
                '_valor2'=>$datos->VALOR_2,
                '_valor3'=>$datos->VALOR_3,
                '_url'=>$datos->URL
            ]);

            return $modelnotification->agregarnotificacion($modelnotification->setValueToTable());
        }else{
            return false;
        }
    }
}