<?php

namespace App\Entity\BE;

use App\Model\BE\ReporteEjecutivos as mReporteEjecutivos;
use \Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Jenssegers\Date\Date as Carbon;

class ReporteEjecutivos extends \App\Entity\Base\Entity {

    //protected $_registro;
    protected $_ejecutivo;
    protected $_zonal;
    protected $_jefatura;
    protected $_leads;
    protected $_avance;
    //protected $_pendiente;
    protected $_contactado;
    protected $_evaluacion;
    protected $_visitas;
    const ITEMS_PER_PAGE = 15;

    function setValueToTable() {
        return $this->cleanArray([
                'REGISTRO'=> $this->_registro,
                'EJECUTIVO'=> $this->_ejecutivo,
                'ZONAL'=> $this->_zonal,
                'JEFATURA'=> $this->_jefatura,
                'LEADS'=> $this->_leads,
                'AVANCE'=> $this->_avance,
                //'LEADS_ETAPA_PENDIENTE'=> $this->_pendiente,
                'LEADS_ETAPA_CONTACTADO'=> $this->_contactado,
                'LEADS_ETAPA_EVALUACION_IBK'=> $this->_evaluacion,
                'VISITAS'=> $this->_visitas,
        ]);
    }
    
    static function getReporteEjecutivo($busqueda = [],$orden=[]){
        $model =new mReporteEjecutivos();
        $query= $model->getReporteEjecutivo(self::sgetPeriodoBE(),$busqueda,$orden,self::periodoToDia(self::sgetPeriodoBE())->addMonths(2)->format('Y-m-d'));
        $totalCount=$model->countReporteEjecutivo(self::sgetPeriodoBE(),$busqueda,$orden,self::periodoToDia(self::sgetPeriodoBE())->addMonths(2)->format('Y-m-d'));
        
        $results = $query
                ->take(self::ITEMS_PER_PAGE)
                ->skip(self::ITEMS_PER_PAGE * ($busqueda['page'] - 1))
                ->get();
                
        $paginator = new Paginator($results, $totalCount->CANTIDAD, self::ITEMS_PER_PAGE, $busqueda['page']);

        return $paginator;
    }

    function getBEResumenBanca($idcentro=null)
    {
        $model=new mReporteEjecutivos();
        return $model->getBEResumenBanca(self::sgetPeriodoBE(),$idcentro);
    }

}


