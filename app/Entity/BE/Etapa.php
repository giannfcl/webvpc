<?php

namespace App\Entity\BE;

use App\Model\BE\Etapa as mEtapa;

class Etapa extends \App\Entity\Base\Entity {


	const PENDIENTE = 1;
	const ME_INTERESA = 2;
	const CONTACTADO = 3;
	const EVALUACION_IBK = 4;
	const APROBADO = 5;
	const DESEMBOLSADO = 6;
    const DENEGADO = 7;
    const PENDIENTE_ACCION = 8;

    const COLOR_SEMAFORO_VERDE = '#2dc937';
    const COLOR_SEMAFORO_AMBAR = '#e7b416';
    const COLOR_SEMAFORO_ROJO = '#cc3232';

    const DIAS_SEMAFORO_CONFIG1 = [
                                    ['min' => 0, 'max' => 14, 'color' => self::COLOR_SEMAFORO_VERDE],
                                    ['min' => 15, 'max' => 21, 'color' => self::COLOR_SEMAFORO_AMBAR],
                                    ['min' => 22, 'max' => PHP_INT_MAX, 'color' => self::COLOR_SEMAFORO_ROJO],
                                ];

    const DIAS_SEMAFORO_CONFIG2 = [
                                    ['min' => 0, 'max' => 30, 'color' => self::COLOR_SEMAFORO_VERDE],
                                    ['min' => 31,'max' => 60, 'color' => self::COLOR_SEMAFORO_AMBAR],
                                    ['min' => 61, 'max' => PHP_INT_MAX, 'color' => self::COLOR_SEMAFORO_ROJO],
                                ];

    static function getAll($tipo) {
        //Tipo es ACCIONES o PROSPECTOS
        $model = new mEtapa();
        return $model->listar(null,$tipo)->get();
    }

    static function getEditables() {
        $model = new mEtapa();
        return $model->listar(1)->get();
    }

    static function getHabilitadas() {
        $model = new mEtapa();
        return $model->listar(1)->get();
    }

    static function getConSemaforo(){
    	return [
    		self::CONTACTADO,
    		self::EVALUACION_IBK,
    		self::APROBADO
    	];
    }

    static function getColorSemaforo($etapa,$dias){
        $color = '';

        if ($etapa == self::PENDIENTE || $etapa == self::ME_INTERESA || $etapa == self::EVALUACION_IBK){
            foreach (self::DIAS_SEMAFORO_CONFIG1 as $rango) {
                if ($dias >= $rango['min'] && $dias <= $rango['max'])
                    return $rango['color'];
            }
        }

        if ($etapa == self::APROBADO || $etapa == self::CONTACTADO){
            foreach (self::DIAS_SEMAFORO_CONFIG2 as $rango) {
                if ($dias >= $rango['min'] && $dias <= $rango['max'])
                    return $rango['color'];
            }
        }

        return $color;
    }

    static function getSemaforos(){
        return [
            'rojo' => 'Rojo',
            'ambar' => 'Ámbar',
            'verde' => 'Verde',
        ];
    }

}