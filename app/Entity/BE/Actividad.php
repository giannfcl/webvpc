<?php

namespace App\Entity\BE;

use \Illuminate\Pagination\LengthAwarePaginator as Paginator;
use App\Model\BE\Actividad as mActividad;
use App\Entity\BE\ActividadParticipante as aParticipante;
use App\Entity\BE\accionComercial as accionComercial;
use Jenssegers\Date\Date as Carbon;

class Actividad extends \App\Entity\Base\Entity {

    protected $_idActividad;
    protected $_numdoc;
    protected $_ejNegocio;
    protected $_fechaActividad;
    protected $_titulo;
    protected $_tipo;
    protected $_flgRenoviacionLinea;
    protected $_ubicacion;
    protected $_temasComerciales;
    protected $_temasCrediticios;
    protected $_fechaRegistro;
    protected $_periodo;
    protected $_idCampEst;
    protected $_participantesIbk;
    protected $_participantesCliente;
    protected $_accionesComerciales;
    protected $_flgLinkedin;
    //protected $_flgLinkedin2;

    const ITEMS_PER_PAGE = 6;

    const TIPO_CAMBIO_ESTADO = 'CAMBIO DE ESTADO';

    function setValueToTable() {
        return $this->cleanArray([
            'ACTIVIDAD_ID' => $this->_idActividad,
            'NUM_DOC' => $this->_numdoc,
            'REGISTRO_EN' => $this->_ejNegocio,
            'FECHA_ACTIVIDAD' => $this->_fechaActividad,
            'TITULO' => $this->_titulo,
            'TIPO' => $this->_tipo,
            'FLG_RENOVACION_LINEA' => $this->_flgRenoviacionLinea,
            'UBICACION' => $this->_ubicacion,
            'TEMAS_COMERCIALES' => $this->_temasComerciales,
            'TEMAS_CREDITICIOS' => $this->_temasCrediticios,
            'FECHA_REGISTRO' => ($this->_fechaRegistro) ? $this->_fechaRegistro->toDateTimeString() : null,
            'PERIODO' => $this->_periodo,
            'ID_CAMP_EST' => $this->_idCampEst,
            'FLG_LINKEDIN' => $this->_flgLinkedin,
            //'FLG_LINKEDIN_2' => $this->_flgLinkedin2,
        ]);
    }

    static function listarActividades($busqueda) {
        $model = new mActividad();
        $query = $model::getActividades($busqueda['documento'], self::sgetPeriodoBE());
        //dd($query->get());
        //Paginacion
        $totalCount = $query->count();
        
        $results = $query
                ->take(self::ITEMS_PER_PAGE)
                ->skip((self::ITEMS_PER_PAGE) * ($busqueda['page'] - 1))
                ->get();
        
         foreach ($results as $result) {
            $partIbk = explode('|', $result->CONTACTO_IBK);
            $partCliente = explode('|', $result->CONTACTO_CLIENTES);
            $result->P_CLIENTES = [];
            $result->P_CLIENTES = $partCliente;
        }
        foreach ($results as $result) {
            $partIbk = explode('|', $result->CONTACTO_IBK);
            $partCliente = explode('|', $result->CONTACTO_CLIENTES);
            $result->P_IBK = [];
            $result->P_IBK = $partIbk;
        }
        \Debugbar::info($busqueda['page']);
        $paginator = new Paginator($results, $totalCount, self::ITEMS_PER_PAGE, $busqueda['page']);

        return $paginator;
    }



    static function buscarVisitasRepetidas($numDoc,$registro,$fecha) {
        $model = new mActividad();
        return $model::buscarVisitasRepetidas($numDoc,$registro,$fecha)->get()->count();
    }

    static function listarComunicaciones($numDoc){
        $model=new mActividad();
        return $model::getComunicaciones($numDoc)->get();
    }
    function agregarActividad(\App\Entity\Usuario $usuario) {
        $hoy = Carbon::now();
        //Buscamos al cliente: vemos su ejecutivo, si es cliente o prospecto / que Accion o Estrategia tiene
        $lead = Lead::getLeadsEjecutivo($usuario,['documento' => $this->_numdoc],false)->first();

        if (!$lead){
            dd('El cliente no se encuentra disponible para el registro de actividad');
            return false;
        }

        $etapas = [];
        $actividades = [];
        $accionesComerciales = [];
        $encargados=[];
        $visualizaciones=[];
        $notas=[];
        $updateEtapas = false;

        //Si es cliente (acciones comerciales)
        //if ($lead->FLAG_ES_CLIENTE == '1'){
            //dd($this->_accionesComerciales);
            //Por cada acción comercial agregada
            $cliente = AccionComercial::buscarCliente(AccionComercial::getCodUnico($this->_numdoc));         
            foreach ($this->_accionesComerciales as $key => $item) {

                //Si es que soy un ejecutivo de producto o analista
                //if(in_array($usuario->getValue('_rol'),array_merge(\App\Entity\Usuario::getEjecutivosProductoBE(),\App\Entity\Usuario::getAnalistasInternosBE()))){
                $ejecutivos=\App\Entity\Usuario::getFarmersHunters($usuario->getValue('_banca'),$usuario->getValue('_zona'),$usuario->getValue('_centro'));
                $regEjecutivos=[];

                for($i=0;$i<$ejecutivos->count();$i++){
                    $regEjecutivos[$i]=$ejecutivos[$i]->REGISTRO;
                }
                //}
                
                if(isset($item['fFin']) and $hoy>$item['fFin']) {                   
                    $this->setMessage('La fecha fin no puede ser menor a la fecha de inicio/registro');
                    return false;
                }

                //Tenemos que ver ahora si la acción es delegada o no
                //NO DELEGAR
                $flgDelegado=0;
                $flgAccion=0;
                if($item['cboDelegado']==NULL || $item['cboDelegado']=="NO DELEGAR"){
                    $ejecutivoResponsable=$usuario->getValue('_registro');
                }
                else{
                    $ejecutivoResponsable=$item['cboDelegado'];
                    $flgDelegado=1;
                    $flgAccion=1;
                }

                 if (in_array($usuario->getValue('_rol'), \App\Entity\Usuario::getEjecutivosProductoBE())){
                    $ejecutivoResponsable=$usuario->getValue('_registro');
                }

                $usuarioRegistro=$usuario->getValue('_registro');

                //Datos de Lead Etapa - Se registra la primera etapa
                $etapaInicial= AccionComercial::getEtapaAccion($item['idAccion'])->first();

                //Datos de Lead - Estrategia
                $leadCampanha = new \App\Entity\LeadCampanha();
                $leadCampanha->setValues([
                    '_periodo' => AccionComercial::sgetPeriodoBE(),
                    '_lead' => $cliente->NUM_DOC,
                    '_codUnico'=>$cliente->COD_UNICO,
                    '_fechaCarga' => $hoy,
                    '_fechaActualizacion' => $hoy,
                    '_campanha' => $item['idAccion'],
                    '_fechaInicio'=>$hoy,
                    '_fechaFin'=>$item['fFin'],            
                    '_kpi'=>$item['kpiAccion'],
                    '_tooltip'=>AccionComercial::TOOLTIP,
                    '_flgHabilitado'=>'1',
                    '_flgAccion'=>$flgAccion,
                    '_mesActivacion'=>$item['cboMesActiv'],
                    '_registroResponsable'=>$usuarioRegistro, //El que la registró
                    '_codSectUniq'=>$cliente->COD_SECT_UNIQ_EN,
                    '_prioridad'=>'0',         
                ]);
                $accionesComerciales[] = $leadCampanha->setValueToTable();

                
                $leadEtapa = new LeadEtapa();
                $leadEtapa->setValues([
                    '_periodo' => AccionComercial::sgetPeriodoBE(),
                    '_numdoc' => $cliente->NUM_DOC,
                    '_etapa' => $etapaInicial->ID_ETAPA,
                    '_estrategia' => $item['idAccion'],
                    '_ejecutivoAsignado' => $ejecutivoResponsable,
                    '_ejecutivo' => $ejecutivoResponsable,
                    '_flgUltimo' => '1',
                    '_fechaRegistro' => $hoy,
                    '_tooltip'=>AccionComercial::TOOLTIP,
                ]);
                $etapas[] = $leadEtapa->setValueToTable();


                $actividad = new Actividad();
                $actividad->setValues([
                    '_numdoc' => $cliente->NUM_DOC,
                    '_ejNegocio' => $ejecutivoResponsable,
                    '_fechaActividad' => $hoy,
                    '_titulo' => 'ASIGNADO',
                    '_tipo' => ACTIVIDAD::TIPO_CAMBIO_ESTADO,
                    '_fechaRegistro' => $hoy,
                    '_periodo' => AccionComercial::sgetPeriodoBE(),
                    '_idCampEst' => $item['idAccion'],
                ]);
                $actividades[] = $actividad->setValueToTable();
                
                /*$actividad2 = new Actividad();
                $actividad2->setValues([
                    '_numdoc' => $cliente->NUM_DOC,
                    '_ejNegocio' => $ejecutivoResponsable,
                    '_fechaActividad' => $hoy,
                    '_titulo' => $etapaInicial->NOMBRE_ETAPA,
                    '_tipo' => ACTIVIDAD::TIPO_CAMBIO_ESTADO,
                    '_fechaRegistro' => $hoy,
                    '_periodo' => AccionComercial::sgetPeriodoBE(),
                    '_idCampEst' => $item['idAccion'],
                ]);
                $actividades[] = $actividad2->setValueToTable();*/


                //Generamos los dos registros de responsables
                $encargado = new \App\Entity\BE\AccionResponsable();
                $visualizacion = new \App\Entity\BE\AccionResponsable();

                $encargado->setValues([
                            '_periodo'=>AccionComercial::sgetPeriodoBE(),
                            '_accion'=>$item['idAccion'],
                            '_numdoc'=>$cliente->NUM_DOC,       
                            '_regResponsable'=>$ejecutivoResponsable,            
                            '_tooltip'=>AccionComercial::TOOLTIP,
                            '_fechaRegistro'=>$hoy,                    
                            '_encargado'=>1,            
                            '_flgUltimo'=>1,            
                ]); 
                
                //Registramos la visualizacion
                if (in_array($usuario->getValue('_rol'), \App\Entity\Usuario::getEjecutivosBE()) and !$flgDelegado){
                        //Si la genera un  EN y no la delega
                        $encargado->setValues([                                   
                            '_tipoEj'=>'EN',            
                        ]); 
                }
                else{
                        //Si la genera un EC, la debe de ver el EN o
                        //Si la delega el EN al EC, la debe se ver el EN
                        $encargado->setValues([                       
                                '_tipoEj'=>'EC',         
                        ]);  

                        $visualizacion->setValues([
                            '_periodo'=>AccionComercial::sgetPeriodoBE(),
                            '_accion'=>$item['idAccion'],
                            '_numdoc'=>$cliente->NUM_DOC,
                            '_regResponsable'=>$cliente->REGISTRO_EN,
                            '_tooltip'=>AccionComercial::TOOLTIP,
                            '_fechaRegistro'=>$hoy,
                            '_tipoEj'=>'EN',
                            '_encargado'=>0,            
                            '_flgUltimo'=>1,            
                        ]);                           
                }
                $encargados[]=$encargado->setValueToTable();
                $visualizaciones[]=$visualizacion->setValueToTable();


                //Registramos las notas (si las hubiese)
                $nota = new  \App\Entity\BE\NotaEjecutivo();
                if(isset($item['nota']) and $item['nota']!=NULL){              
                    $nota->setValues([
                            '_periodo'=>AccionComercial::sgetPeriodoBE(),
                            '_ejecutivo' => $usuario->getValue('_registro'),
                            '_numdoc' => $cliente->NUM_DOC,
                            '_nota' => $item['nota'],
                            '_idAccion'=>$item['idAccion'],
                            '_tooltip'=>AccionComercial::TOOLTIP,
                            '_fechaRegistro'=>$hoy,
                    ]); 
                }
                $notas[]=$nota->setValueToTable(); 
      

            }
         

        //}else{
            //Si la estrategia del lead esta en pendiente/me interesa hay que cambiarla a CONTACTADO
            if(in_array($lead->ETAPA_ID, [Etapa::PENDIENTE,Etapa::ME_INTERESA])){

                //Etapa contactado 
                $leadEtapa = new LeadEtapa();
                $leadEtapa->setValues([
                    '_periodo' => self::sgetPeriodoBE(),
                    '_numdoc' => $this->_numdoc,
                    '_etapa' => Etapa::CONTACTADO,
                    '_estrategia' => $lead->ESTRATEGIA_ID,
                    '_ejecutivo' => $usuario->getValue('_registro'),
                    '_ejecutivoAsignado' => $lead->REGISTRO_EN,
                    '_fechaRegistro' => $hoy,
                    '_flgUltimo' => 1,
                ]);
                $etapas[] = $leadEtapa->setValueToTable();

                //tambien requiere que las otras etapas de este cliente pasen a flg_ultimo = 0
                $updateEtapas = true;


                //Hay que registrar una actividad adicional: CAMBIO DE ETAPA a contactado
                $actividadExtra = new Actividad();
                $actividadExtra->setValues([
                    '_periodo' => self::sgetPeriodoBE(),
                    '_numdoc' => $this->_numdoc,
                    '_ejNegocio' => $usuario->getValue('_registro'),
                    '_fechaActividad' => $hoy,
                    '_fechaRegistro' => $hoy,
                    '_titulo' => 'CONTACTADO',
                    '_idCampEst' => $lead->ESTRATEGIA_ID,
                    '_tipo' => self::TIPO_CAMBIO_ESTADO,

                ]);
                $actividades[] = $actividadExtra->setValueToTable();
            }
        //}


        $this->setValues([
            '_periodo' => parent::getPeriodoBE(),
            '_fechaRegistro' => $hoy,
        ]);

        //Limpiamos lista de participantes
        $this->_participantesIbk = array_diff(array_filter($this->_participantesIbk),['sinParticipante']);
        $this->_participantesCliente = array_diff(array_filter($this->_participantesCliente),['sinParticipante']);


        // Si no hay participantes ibk o participantes clientes
        if ($this->_tipo <> 'CORREO'){
            if(count($this->_participantesIbk) < 1 || count($this->_participantesCliente) < 1){
                $this->setMessage('No has ingresado participantes para la actividad');
                return false;
            }
        }

        // Si no ha ingresado Temas Comerciales o Temas Crediticios
        if(is_null($this->_temasCrediticios) && is_null($this->_temasComerciales)){
            $this->setMessage('Ingresa al menos uno de los temas comerciales o crediticios');
            return false;   
        }

        
        //Seteando los participantes
        $participantes = [];
        $participante = new aParticipante();

        foreach ($this->_participantesCliente as $pc) {
            $participante->setValues([
                '_tipoParticipante' => 'CLIENTE',
                '_idParticipante' => $pc,
            ]);
            $participantes[] = $participante->setValueToTable();
        }

        foreach ($this->_participantesIbk as $pibk) {
            $participante->setValues([
                '_tipoParticipante' => 'INTERBANK',
                '_registroEn' => $pibk,
                '_idParticipante' =>NULL
            ]);
            $participantes[] = $participante->setValueToTable();
        }

        $modelActividad = new mActividad();
        

        /*
        $fecha_asignacion=$modelActividad->getFechaAsignacion($this->_periodo,$this->_numdoc)->first();
        if( $fecha_asignacion->FECHA_REGISTRO >= $this->_fechaActividad && $fecha_asignacion->FLG_CLIENTE <> 1){
            $this->setMessage('La fecha de actividad no puede ser antes de la asignación del lead');
            return false;
        }
        */
        //dd($this->setValueToTable(), $participantes,$etapas,$actividades,$accionesComerciales,$updateEtapas,$encargados,$visualizaciones,$notas);
        
        if ($modelActividad->insertarActividad($this->setValueToTable(), $participantes,$etapas,$actividades,
            $accionesComerciales,$updateEtapas,$encargados,$visualizaciones,$notas)) {
            $this->setMessage('La actividad ha sido registrada correctamente');
            return true;
        } else {
            $this->setMessage('Hubo un error al ingresar sus datos, por favor intentar luego');
            return false;
        }
    }

}