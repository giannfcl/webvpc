<?php

namespace App\Entity\BE;

use Jenssegers\Date\Date as Carbon;

class SegActividad extends \App\Entity\Base\Entity {

    protected $_id;
    protected $_registroen;
    protected $_fecharegistro;
    protected $_codunico;
    protected $_fechaactualizaciondoc;
    protected $_fechaactividad;
    protected $_tipo;
    protected $_accion;
    protected $_fechainicioatraso;
    protected $_deudatotal;
    protected $_deudaatrasada;
    protected $_deudavencida;
    protected $_dias;
    protected $_pctdeudaproblema;
    protected $_motivoimpago;
    protected $_responsable;
    protected $_comentario;
    protected $_compromisopago;
    protected $_cumplecompromiso;

    function setProperties($data) {
        $this->setValues([
            '_id' => $data->ID,
            '_registroen' => $data->REGISTRO_EN,
            '_fecharegistro' => $data->FECHA_REGISTRO,
            '_codunico' => $data->COD_UNICO,
            '_fechaactualizaciondoc' => $data->FECHA_ACTUALIZACION_DOC,
            '_fechaactividad' => $data->FECHA_ACTIVIDAD,
            '_tipo' => $data->TIPO,
            '_accion' => $data->ACCION,
            '_fechainicioatraso' => $data->FECHA_INICIO_ATRASO,
            '_deudatotal' => $data->DEUDA_TOTAL,
            '_deudaatrasada' => $data->DEUDA_ATRASADA,
            '_deudavencida' => $data->DEUDA_VENCIDA,
            '_dias' => $data->DIAS,
            '_pctdeudaproblema' => $data->PCT_DEUDA_PROBLEMA,
            '_motivoimpago' => $data->MOTIVO_IMPAGO,
            '_responsable' => $data->RESPONSABLE,
            '_comentario' => $data->COMENTARIO,
            '_compromisopago' => $data->COMPROMISO_PAGO,
            '_cumplecompromiso' => $data->CUMPLE_COMPROMISO,
        ]);
    }

    function setValueToTable() {
        return $this->cleanArray([
            'REGISTRO_EN' => $this->_registroen,
            'FECHA_REGISTRO' => $this->_fecharegistro,
            'COD_UNICO' => $this->_codunico,
            'FECHA_ACTUALIZACION_DOC' => $this->_fechaactualizaciondoc,
            'FECHA_ACTIVIDAD' => $this->_fechaactividad,
            'TIPO' => $this->_tipo,
            'ACCION' => $this->_accion,
            'FECHA_INICIO_ATRASO' => $this->_fechainicioatraso,
            'DEUDA_TOTAL' => $this->_deudatotal,
            'DEUDA_ATRASADA' => $this->_deudaatrasada,
            'DEUDA_VENCIDA' => $this->_deudavencida,
            'DIAS' => $this->_dias,
            'PCT_DEUDA_PROBLEMA' => $this->_pctdeudaproblema,
            'MOTIVO_IMPAGO' => $this->_motivoimpago,
            'RESPONSABLE' => $this->_responsable,
            'COMENTARIO' => $this->_comentario,
            'COMPROMISO_PAGO' => $this->_compromisopago,
            'CUMPLE_COMPROMISO' => $this->_cumplecompromiso,
        ]);
    }
}