<?php

namespace App\Entity\BE;

use \Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Jenssegers\Date\Date as Carbon;
use App\Model\BE\InfiniteBase as mBase;

class InfiniteBase extends \App\Entity\Base\Entity {

    protected $_fecha;
    protected $_idClie;
    protected $_nomClie;
    protected $_banca;
    protected $_codUnico;
    protected $_regEjecutivo;
    protected $_zonal;
    protected $_ejecutivo;
    protected $numDocumento;
    protected $_codProducto;
    protected $_fechaVencimiento;
    protected $_diasVencimiento;
    protected $_deuda;
    protected $_flgGestionado;

    const ITEMS_PER_PAGE = 20;

    static function getByLead($page, $zonal, $jefatura, $regEjecutivo) {

        $today = Carbon::now()->format('Ymd');
        \Debugbar::info($today);

        $model = new mBase();
        $data = $model->getLista($today, $zonal, $jefatura, $regEjecutivo);


        $totalCount = $data->count();
        $results = $data
                ->take(self::ITEMS_PER_PAGE)
                ->skip(self::ITEMS_PER_PAGE * ($page - 1))
                ->get();

        $paginator = new Paginator($results, $totalCount, self::ITEMS_PER_PAGE, $page);

        return $paginator;
    }

    function getEjecutivos($banca, $zonal) {
        $model = new mBase();
        $data = $model->getEjecutivos($banca, $zonal)->get();
        return $data;
    }

    function getZonal($banca) {
        $model = new mBase();
        $data = $model->getZonal($banca)->get();
        return $data;
    }

    function getBanca() {
        $model = new mBase();
        $data = $model->getBanca()->get();
        return $data;
    }

    static function updateGestion($data) {
        $model = new mBase();
        if ($model->updateGestion($data)) {
            return true;
        } else {
            //$this->setMessage('Hubo un error al registrar su información. Inténtelo más tarde.');
            return false;
        }
    }

}
