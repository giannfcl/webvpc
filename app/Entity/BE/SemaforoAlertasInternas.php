<?php

namespace App\Entity\BE;

use App\Model\BE\Etapa as mEtapa;
use App\Entity\BE\VencimientoAmortizacion as VencimientoAmortizacion;

class SemaforoAlertasInternas extends \App\Entity\Base\Entity {


	const COLOR_SEMAFORO_NEGRO = '#000000';
    const COLOR_SEMAFORO_AMBAR = '#e7b416';
    const COLOR_SEMAFORO_ROJO = '#cc3232';

    const DIAS_SEMAFORO_CONFIG1 = [
                                    ['min' => 0, 'max' => 8, 'color' => self::COLOR_SEMAFORO_AMBAR],
                                    ['min' => 9, 'max' => 15, 'color' => self::COLOR_SEMAFORO_ROJO],
                                    ['min' => 16, 'max' => PHP_INT_MAX, 'color' => self::COLOR_SEMAFORO_NEGRO],
                                ];


    static function getColorSemaforo($dias){
        $color = '';

        foreach (self::DIAS_SEMAFORO_CONFIG1 as $rango) {
            if ($dias >= $rango['min'] && $dias <= $rango['max'])
                return $rango['color'];
        }

        return $color;
    }

    static function getSemaforos(){
        return [
            'rojo' => 'Rojo',
            'ambar' => 'Ámbar',
            'verde' => 'Verde',
        ];
    }
}