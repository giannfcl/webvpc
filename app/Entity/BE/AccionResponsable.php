<?php

namespace App\Entity\BE;

use App\Entity\BE\AccionComercial as AccionComercial;
use Jenssegers\Date\Date as Carbon;

class AccionResponsable extends \App\Entity\Base\Entity {

    protected $_periodo;
    protected $_accion;
    protected $_numdoc;
    protected $_regResponsable;
    protected $_tooltip;
    protected $_fechaRegistro;
    protected $_tipoEj;
    protected $_encargado;
    protected $_flgUltimo;

    function setProperties($data) {
    }


    function setValueToTable() {
        return $this->cleanArray([
            'PERIODO'=> $this->_periodo,
            'ID_CAMP_EST'=> $this->_accion,
            'NUM_DOC'=> $this->_numdoc,
            'REGISTRO'=> $this->_regResponsable,
            'TOOLTIP'=> $this->_tooltip,
            'FECHA_CARGA'=> $this->_fechaRegistro,
            'TIPO_EJ'=> $this->_tipoEj,
            'ENCARGADO'=> $this->_encargado,
            'FLG_ULTIMO'=> $this->_flgUltimo,
        ]);


    }



}