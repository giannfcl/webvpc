<?php

namespace App\Entity\BE;

use Jenssegers\Date\Date as Carbon;
use App\Model\BE\VencimientoAmortizacion as mVencimientoAmortizacion;

class VencimientoAmortizacion extends \App\Entity\Base\Entity {

    protected $_fecha;
    protected $_codUnico;
    protected $_idProducto;
    protected $_fechaVencimiento;
    protected $_flagRenovacion;
    protected $_porcRenovacion;
    protected $_usuarioActualizacion;

    const VENCIMIENTO = 'V';
    const AMORTIZACION = 'A';

    function setValueToTable() {
        return $this->cleanArray([
            'FECHA' => $this->_fecha,
            'COD_UNICO' => $this->_codUnico,
            'ID_PRODUCTO' => $this->_idProducto,
            'FECHA_VENCIMIENTO' => $this->_fechaVencimiento,
            'FLG_SE_RENUEVA' => $this->_flagRenovacion,
            'PORC_RENOVACION' => $this->_porcRenovacion,
            'REG_USUARIO' => $this->_usuarioActualizacion,
        ]);
    }

    function massiveUpdate($items){
        $model = new mVencimientoAmortizacion();

        $vencarms = array();
        foreach($items as $item){
            $vencarms[] = $item->setValueToTable();
        }

        if ($model->massiveUpdate($vencarms)){
            $this->setMessage('Los datos han sido actualizados correctamente');
            return true;
        }else{
            $this->setMessage('Error en petición a la base de datos');
            return false;
        }
    }


    static function getAmortizaciones(\App\Entity\Usuario $en, $periodo, $ejecutivo = null){
        $model = new mVencimientoAmortizacion();        
        return $model->getAmortVencEjecutivo($periodo? $periodo:self::sgetPeriodoBE(),$ejecutivo? $ejecutivo:$en->getValue('_registro') ,self::AMORTIZACION,null,null,null,1);
    }

    static function getVencimientos(\App\Entity\Usuario $en, $periodo, $ejecutivo = null){
        $model = new mVencimientoAmortizacion();
        return $model->getAmortVencEjecutivo($periodo? $periodo:self::sgetPeriodoBE(),$ejecutivo? $ejecutivo:$en->getValue('_registro'),self::VENCIMIENTO,null,null,null,1);
    }

    //static function getResumenSaldos(\App\Entity\Usuario $en, $periodo, $ejecutivo = null,$banca,$zona){
    static function getResumenSaldos(\App\Entity\Usuario $en, $periodo, $ejecutivo = null,$banca,$zona){
    	$model = new \App\Model\BE\VencimientoAmortizacion();
    	$result = $model->getResumenSaldos($ejecutivo,$periodo,$banca,$zona);

        $aux = array();
        foreach ($result as $item) {
            if ($item->TIPO_PRODUCTO == 'DIRECTAS'){
                $aux[0] = $item;
            }
            if ($item->TIPO_PRODUCTO == 'INDIRECTAS'){
                $aux[1] = $item;   
            }
        }
        if (!isset($aux[1])){
            $item = new \stdClass();
            $item->TIPO_PRODUCTO = 'INDIRECTAS';
            $item->SALDO_INICIAL = 0;
            $item->SALDO_ACTUAL = 0;
            $aux[1] = $item;
        }

        return $aux;
    }
    

    static function getResumenMeta(\App\Entity\Usuario $en, $periodo, $ejecutivo = null,$banca,$zonal){
    	$model = new \App\Model\BE\VencimientoAmortizacion();
    	$result =  $model->getResumenMeta($ejecutivo,$periodo,$banca,$zonal);        
        $aux = array();
        foreach ($result as $item) {
            if ($item->TIPO_PRODUCTO == 'DIRECTAS'){
                $aux[0] = $item;
            }
            if ($item->TIPO_PRODUCTO == 'INDIRECTAS'){
                $aux[1] = $item;   
            }
        }
        if (!isset($aux[1])){
            $item = new \stdClass();
            $item->TIPO_PRODUCTO = 'INDIRECTAS';
            $item->PPTOMES = 0;
            $item->SALDO_PROMEDIO = 0;
            $item->GAP = 0;
            $item->AVANCE = 0;
            $aux[1] = $item;
        }

        return $aux;
    }

    

    static function getResumenDesembolsos(\App\Entity\Usuario $en, $periodo, $ejecutivo = null,$banca=null,$zonal=null){

    	$model = new \App\Model\BE\VencimientoAmortizacion();
    	$result =  $model->getResumenDesembolsos($ejecutivo,$periodo,$banca,$zonal);

        $aux = array();
        foreach ($result as $item) {
            if ($item->TIPO_PRODUCTO == 'DIRECTAS'){
                $aux[0] = $item;
            }
            if ($item->TIPO_PRODUCTO == 'INDIRECTAS'){
                $aux[1] = $item;   
            }
        }
        if (!isset($aux[1])){
            $item = new \stdClass();
            $item->TIPO_PRODUCTO = 'INDIRECTAS';
            $item->COMPROMISO = 0;
            $item->MONTO_CERT = 0;
            $item->MONTO_ENCOT = 0;
            $aux[1] = $item;
        }
       // dd($aux);
        return $aux;
    }

    
	static function getResumenProyeccInicial(\App\Entity\Usuario $en, $periodo, $ejecutivo = null,$banca,$zonal){
    	$model = new \App\Model\BE\VencimientoAmortizacion();
    	$result = $model->getResumenProyeccInicial($ejecutivo,$periodo,$banca,$zonal);


        $aux = array();
        foreach ($result as $item) {
            if ($item->TIPO_PRODUCTO == 'DIRECTAS'){
                $aux[0] = $item;
            }
            if ($item->TIPO_PRODUCTO == 'INDIRECTAS'){
                $aux[1] = $item;   
            }
        }
        if (!isset($aux[1])){
            $item = new \stdClass();
            $item->TIPO_PRODUCTO = 'INDIRECTAS';
            $item->SALDO_DESEM_CERT = 0;
            $item->SALDO_PROYEC_CERT = 0;
            $aux[1] = $item;
        }

        return $aux;
    }

    

	static function getResumenCaidas(\App\Entity\Usuario $en, $periodo, $ejecutivo = null,$banca=null,$zonal=null){
    	$model = new \App\Model\BE\VencimientoAmortizacion();
    	$result = $model->getResumenCaidas($ejecutivo,$periodo,$banca,$zonal);

        $aux = array();
        foreach ($result as $item) {
            if ($item->TIPO_PRODUCTO == 'DIRECTAS'){
                $aux[0] = $item;
            }
            if ($item->TIPO_PRODUCTO == 'INDIRECTAS'){
                $aux[1] = $item;   
            }
        }
        if (!isset($aux[1])){
            $item = new \stdClass();
            $item->TIPO_PRODUCTO = 'INDIRECTAS';
            $item->MONTO_CAIDA = 0;
            $aux[1] = $item;
        }

        if (!isset($aux[0])){
            $item = new \stdClass();
            $item->TIPO_PRODUCTO = 'DIRECTAS';
            $item->MONTO_CAIDA = 0;
            $aux[0] = $item;
        }

        return $aux;
    }
    static function getResumenJefes(\App\Entity\Usuario $en, $periodo, $ejecutivo = null,$banca=null,$zonal=null,$jefatura,$rol){
        
        /*lista de personas a cargo*/
        $modelPersonas = new \App\Model\BE\VencimientoAmortizacion();
        $resultPersonales = $modelPersonas->getPersonal($rol,$banca,$zonal,$jefatura)->keyBy('REGISTRO');

        /*datos detalle de personas*/
        $modelSaldos = new \App\Model\BE\VencimientoAmortizacion();
        $resultSaldos = $modelSaldos->getResumenSaldosJefes($ejecutivo,$periodo,$banca,$zonal,$jefatura,$rol)->keyBy('IND');  

        $modelCaidas = new \App\Model\BE\VencimientoAmortizacion();
        $resultCaidas = $modelCaidas->getResumenCaidasJefes($ejecutivo,$periodo,$banca,$zonal,$jefatura,$rol)->keyBy('IND');
        
        $modelDesemb = new \App\Model\BE\VencimientoAmortizacion();
        $resultDesemb =  $modelDesemb->getResumenDesembolsosJefes($ejecutivo,$periodo,$banca,$zonal,$jefatura,$rol)->keyBy('IND');    
        
        $modelProy = new \App\Model\BE\VencimientoAmortizacion();
        $resultProy = $modelProy->getResumenProyeccInicialJefes($ejecutivo,$periodo,$banca,$zonal,$jefatura,$rol)->keyBy('IND');
        
                
        $modelMetas = new \App\Model\BE\VencimientoAmortizacion();
        $resultMetas =  $modelMetas->getResumenMetaJefes($ejecutivo,$periodo,$banca,$zonal,$jefatura,$rol)->keyBy('IND');

        $result = array();
        //\Debugbar::info($resultSaldos);
        //\Debugbar::info($resultCaidas);
        //\Debugbar::info($resultProy);
        //\Debugbar::info($resultDesemb);
        \Debugbar::info($resultMetas);

                /*poblar resultado*/
                foreach ($resultPersonales as $resultPersonal){                                
                    $directas = 'DIRECTAS';
                    $indirectas ='INDIRECTAS';
                    $result[] = [
                        'REGISTRO' => $resultPersonal->REGISTRO,
                        'NOMBRE' => $resultPersonal->NOMBRE,
                        'TIPO_PRODUCTO' => $directas,
                        'SALDO_ACTUAL' => isset($resultSaldos[$directas.$resultPersonal->REGISTRO]) ? $resultSaldos[$directas.$resultPersonal->REGISTRO]->SALDO_ACTUAL : 0,
                        'SALDO_INICIAL' => isset($resultSaldos[$directas.$resultPersonal->REGISTRO]) ? $resultSaldos[$directas.$resultPersonal->REGISTRO]->SALDO_INICIAL : 0,
                         
                        'MONTO_CAIDA' => isset($resultCaidas[$directas.$resultPersonal->REGISTRO]) ? $resultCaidas[$directas.$resultPersonal->REGISTRO]->MONTO_CAIDA : 0,                    

                        'SALDO_DESEM_CERT' => isset($resultProy[$directas.$resultPersonal->REGISTRO]) ? $resultProy[$directas.$resultPersonal->REGISTRO]->SALDO_DESEM_CERT : 0,
                        'SALDO_PROYEC_CERT' => isset($resultProy[$directas.$resultPersonal->REGISTRO]) ? $resultProy[$directas.$resultPersonal->REGISTRO]->SALDO_PROYEC_CERT : 0,

                        'COMPROMISO' =>  isset($resultDesemb[$directas.$resultPersonal->REGISTRO]) ? $resultDesemb[$directas.$resultPersonal->REGISTRO]->COMPROMISO : 0,
                        'MONTO_CERT' =>  isset($resultDesemb[$directas.$resultPersonal->REGISTRO]) ? $resultDesemb[$directas.$resultPersonal->REGISTRO]->MONTO_CERT : 0,
                        'MONTO_ENCOT' =>  isset($resultDesemb[$directas.$resultPersonal->REGISTRO]) ? $resultDesemb[$directas.$resultPersonal->REGISTRO]->MONTO_ENCOT : 0,

                        'META' =>  isset($resultMetas[$directas.$resultPersonal->REGISTRO]) ? $resultMetas[$directas.$resultPersonal->REGISTRO]->META : 0,
                        'SALDO_PROMEDIO' =>  isset($resultMetas[$directas.$resultPersonal->REGISTRO]) ? $resultMetas[$directas.$resultPersonal->REGISTRO]->SALDO_PROMEDIO : 0,
                        'GAP' =>  isset($resultMetas[$directas.$resultPersonal->REGISTRO]) ? $resultMetas[$directas.$resultPersonal->REGISTRO]->GAP : 0,
                        'AVANCE' =>  isset($resultMetas[$directas.$resultPersonal->REGISTRO]) ? $resultMetas[$directas.$resultPersonal->REGISTRO]->AVANCE : 0,
                    ];
                    $result[] = [
                        'REGISTRO' => $resultPersonal->REGISTRO,
                        'NOMBRE' => $resultPersonal->NOMBRE,
                        'TIPO_PRODUCTO' => $indirectas,
                        'SALDO_ACTUAL' => isset($resultSaldos[$indirectas.$resultPersonal->REGISTRO]) ? $resultSaldos[$indirectas.$resultPersonal->REGISTRO]->SALDO_ACTUAL : 0,
                        'SALDO_INICIAL' => isset($resultSaldos[$indirectas.$resultPersonal->REGISTRO]) ? $resultSaldos[$indirectas.$resultPersonal->REGISTRO]->SALDO_INICIAL : 0,
                         
                        'MONTO_CAIDA' => isset($resultCaidas[$indirectas.$resultPersonal->REGISTRO]) ? $resultCaidas[$indirectas.$resultPersonal->REGISTRO]->MONTO_CAIDA : 0,                    

                        'SALDO_DESEM_CERT' => isset($resultProy[$indirectas.$resultPersonal->REGISTRO]) ? $resultProy[$indirectas.$resultPersonal->REGISTRO]->SALDO_DESEM_CERT : 0,
                        'SALDO_PROYEC_CERT' => isset($resultProy[$indirectas.$resultPersonal->REGISTRO]) ? $resultProy[$indirectas.$resultPersonal->REGISTRO]->SALDO_PROYEC_CERT : 0,

                        'COMPROMISO' =>  isset($resultDesemb[$indirectas.$resultPersonal->REGISTRO]) ? $resultDesemb[$indirectas.$resultPersonal->REGISTRO]->COMPROMISO : 0,
                        'MONTO_CERT' =>  isset($resultDesemb[$indirectas.$resultPersonal->REGISTRO]) ? $resultDesemb[$indirectas.$resultPersonal->REGISTRO]->MONTO_CERT : 0,
                        'MONTO_ENCOT' =>  isset($resultDesemb[$indirectas.$resultPersonal->REGISTRO]) ? $resultDesemb[$indirectas.$resultPersonal->REGISTRO]->MONTO_ENCOT : 0,

                        'META' =>  isset($resultMetas[$indirectas.$resultPersonal->REGISTRO]) ? $resultMetas[$indirectas.$resultPersonal->REGISTRO]->META : 0,
                        'SALDO_PROMEDIO' =>  isset($resultMetas[$indirectas.$resultPersonal->REGISTRO]) ? $resultMetas[$indirectas.$resultPersonal->REGISTRO]->SALDO_PROMEDIO : 0,
                        'GAP' =>  isset($resultMetas[$indirectas.$resultPersonal->REGISTRO]) ? $resultMetas[$indirectas.$resultPersonal->REGISTRO]->GAP : 0,
                        'AVANCE' =>  isset($resultMetas[$indirectas.$resultPersonal->REGISTRO]) ? $resultMetas[$indirectas.$resultPersonal->REGISTRO]->AVANCE : 0,
                    ];

                    /*info totales por jecutivos*/
                    $totxEjeSaldoActual =(isset($resultSaldos[$indirectas.$resultPersonal->REGISTRO]) ? $resultSaldos[$indirectas.$resultPersonal->REGISTRO]->SALDO_ACTUAL : 0) +( isset($resultSaldos[$directas.$resultPersonal->REGISTRO]) ? $resultSaldos[$directas.$resultPersonal->REGISTRO]->SALDO_ACTUAL : 0);
                    $totxEjeSaldoInicial =(isset($resultSaldos[$indirectas.$resultPersonal->REGISTRO]) ? $resultSaldos[$indirectas.$resultPersonal->REGISTRO]->SALDO_INICIAL : 0) +(isset($resultSaldos[$directas.$resultPersonal->REGISTRO]) ? $resultSaldos[$directas.$resultPersonal->REGISTRO]->SALDO_INICIAL : 0);
                    $totxEjeMontoCaida =(isset($resultCaidas[$indirectas.$resultPersonal->REGISTRO]) ? $resultCaidas[$indirectas.$resultPersonal->REGISTRO]->MONTO_CAIDA : 0) +(isset($resultCaidas[$directas.$resultPersonal->REGISTRO]) ? $resultCaidas[$directas.$resultPersonal->REGISTRO]->MONTO_CAIDA : 0);                    
                    $totxEjeSaldoDesemCert =(isset($resultProy[$indirectas.$resultPersonal->REGISTRO]) ? $resultProy[$indirectas.$resultPersonal->REGISTRO]->SALDO_DESEM_CERT : 0) + (isset($resultProy[$directas.$resultPersonal->REGISTRO]) ? $resultProy[$directas.$resultPersonal->REGISTRO]->SALDO_DESEM_CERT : 0);
                    $totxEjeSaldoProyCert =(isset($resultProy[$indirectas.$resultPersonal->REGISTRO]) ? $resultProy[$indirectas.$resultPersonal->REGISTRO]->SALDO_PROYEC_CERT : 0) + (isset($resultProy[$directas.$resultPersonal->REGISTRO]) ? $resultProy[$directas.$resultPersonal->REGISTRO]->SALDO_PROYEC_CERT : 0);
                    $totxEjeCompromiso =(isset($resultDesemb[$indirectas.$resultPersonal->REGISTRO]) ? $resultDesemb[$indirectas.$resultPersonal->REGISTRO]->COMPROMISO : 0) + (isset($resultDesemb[$directas.$resultPersonal->REGISTRO]) ? $resultDesemb[$directas.$resultPersonal->REGISTRO]->COMPROMISO : 0);
                    $totxEjeMontoCert =(isset($resultDesemb[$indirectas.$resultPersonal->REGISTRO]) ? $resultDesemb[$indirectas.$resultPersonal->REGISTRO]->MONTO_CERT : 0) + (isset($resultDesemb[$directas.$resultPersonal->REGISTRO]) ? $resultDesemb[$directas.$resultPersonal->REGISTRO]->MONTO_CERT : 0);
                    $totxEjeMontoEncot =(isset($resultDesemb[$indirectas.$resultPersonal->REGISTRO]) ? $resultDesemb[$indirectas.$resultPersonal->REGISTRO]->MONTO_ENCOT : 0) + (isset($resultDesemb[$directas.$resultPersonal->REGISTRO]) ? $resultDesemb[$directas.$resultPersonal->REGISTRO]->MONTO_ENCOT : 0);
                    $totxEjeMeta =(isset($resultMetas[$indirectas.$resultPersonal->REGISTRO]) ? $resultMetas[$indirectas.$resultPersonal->REGISTRO]->META : 0) + (isset($resultMetas[$directas.$resultPersonal->REGISTRO]) ? $resultMetas[$directas.$resultPersonal->REGISTRO]->META : 0);
                    $totxEjeSaldoPromedio =(isset($resultMetas[$indirectas.$resultPersonal->REGISTRO]) ? $resultMetas[$indirectas.$resultPersonal->REGISTRO]->SALDO_PROMEDIO : 0) + (isset($resultMetas[$directas.$resultPersonal->REGISTRO]) ? $resultMetas[$directas.$resultPersonal->REGISTRO]->SALDO_PROMEDIO : 0);
                    $totxEjeGap =(isset($resultMetas[$indirectas.$resultPersonal->REGISTRO]) ? $resultMetas[$indirectas.$resultPersonal->REGISTRO]->GAP : 0) + (isset($resultMetas[$directas.$resultPersonal->REGISTRO]) ? $resultMetas[$directas.$resultPersonal->REGISTRO]->GAP : 0);
                    
                    if($totxEjeMeta==0){
                            $totxEjeMeta=1;
                    }

                      $result[]=[
                        'REGISTRO' => $resultPersonal->REGISTRO,
                        'NOMBRE' => $resultPersonal->NOMBRE,
                        'TIPO_PRODUCTO' => 'TOTAL',
                        'SALDO_ACTUAL' => $totxEjeSaldoActual,
                        'SALDO_INICIAL' => $totxEjeSaldoInicial,
                         
                        'MONTO_CAIDA' => $totxEjeMontoCaida,

                        'SALDO_DESEM_CERT' => $totxEjeSaldoDesemCert,
                        'SALDO_PROYEC_CERT' => $totxEjeSaldoProyCert,

                        'COMPROMISO' => $totxEjeCompromiso, 
                        'MONTO_CERT' => $totxEjeMontoCert, 
                        'MONTO_ENCOT' => $totxEjeMontoEncot, 

                        'META' =>  $totxEjeMeta,
                        'SALDO_PROMEDIO' => $totxEjeSaldoPromedio, 
                        'GAP' => $totxEjeGap, 
                        'AVANCE' => (100*$totxEjeSaldoPromedio)/$totxEjeMeta, 
                    ];                    
                }   
                     \Debugbar::info($result);  
            return $result;    
    }
    static function getResumenCaidasJefes(\App\Entity\Usuario $en, $periodo, $ejecutivo = null,$banca=null,$zonal=null,$jefatura,$rol){
        $model = new \App\Model\BE\VencimientoAmortizacion();
        $result = $model->getResumenCaidasJefes($ejecutivo,$periodo,$banca,$zonal,$jefatura,$rol);    
        return $result;
    }
    static function getResumenDesembolsosJefes(\App\Entity\Usuario $en, $periodo, $ejecutivo = null,$banca=null,$zonal=null,$jefatura=null,$rol){

        $model = new \App\Model\BE\VencimientoAmortizacion();
        $result =  $model->getResumenDesembolsosJefes($ejecutivo,$periodo,$banca,$zonal,$jefatura,$rol);    
        return $result;
    }
    static function getResumenProyeccInicialJefes(\App\Entity\Usuario $en, $periodo, $ejecutivo = null,$banca,$zonal,$jefatura,$rol){
        $model = new \App\Model\BE\VencimientoAmortizacion();
        $result = $model->getResumenProyeccInicialJefes($ejecutivo,$periodo,$banca,$zonal,$jefatura,$rol);
        return $result;
    }
    static function getResumenSaldosJefes(\App\Entity\Usuario $en, $periodo, $ejecutivo = null,$banca,$zona,$jefatura,$rol){
        
        $model = new \App\Model\BE\VencimientoAmortizacion();
        $result = $model->getResumenSaldosJefes($ejecutivo,$periodo,$banca,$zona,$jefatura,$rol);        
        return $result;
    }
    static function getResumenMetaJefes(\App\Entity\Usuario $en, $periodo, $ejecutivo = null,$banca,$zonal,$jefatura,$rol){
        $model = new \App\Model\BE\VencimientoAmortizacion();
        $result =  $model->getResumenMetaJefes($ejecutivo,$periodo,$banca,$zonal,$jefatura,$rol);
        return $result;
    }
    static function getProyeccionVencAmort($periodo,$ejecutivo = null,$jefatura= null,$zonal= null,$banca= null){
        $model = new mVencimientoAmortizacion();
        $data = $model->getProyeccionVencAmort($periodo,$ejecutivo,$jefatura,$zonal,$banca)->get()->keyBy('FECHA_VENCIMIENTO');
        //$data = $model->getProyeccionVencAmort($periodo,$ejecutivo,$jefatura,'BE','BELZONAL3')->get()
        //$data = $model->getProyeccionVencAmort($periodo,$ejecutivo,$jefatura,'BE','BELZONAL3')->get()->keyBy('FECHA_VENCIMIENTO');        
        return $data;
    }

    static function getProyeccionVencAmortInicial($periodo,$ejecutivo = null,$jefatura= null,$zonal= null,$banca= null){
        $model = new mVencimientoAmortizacion();
        $data = $model->getProyeccionVencAmortInicial($periodo,$ejecutivo,$jefatura,$zonal,$banca)->get()->keyBy('FECHA_VENCIMIENTO');
        //$data = $model->getProyeccionVencAmort($periodo,$ejecutivo,$jefatura,'BE','BELZONAL3')->get()
        //$data = $model->getProyeccionVencAmort($periodo,$ejecutivo,$jefatura,'BE','BELZONAL3')->get()->keyBy('FECHA_VENCIMIENTO');        
        return $data;
    }
  

}
	
  