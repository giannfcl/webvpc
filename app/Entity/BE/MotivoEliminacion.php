<?php

namespace App\Entity\BE;

use App\Model\BE\MotivoEliminacion as mMotivoEliminacion;
use Illuminate\Support\Facades\Cache;

class MotivoEliminacion extends \App\Entity\Base\Entity {

    const CACHE_GET_MOTIVO_DETALLE_ELIMINACION = 'get-motivo-detalle-eliminacion-';
    const MOTIVO_OTROS =1;

    static function get($motivo,$detalle){

        //FALTA CACHE
        $result = Cache::rememberForever(self::CACHE_GET_MOTIVO_DETALLE_ELIMINACION . $motivo . '-'. $detalle , function() use ($motivo,$detalle){
            $model = new mMotivoEliminacion();
            return $model->get($motivo,$detalle)->first();
        });

        return $result;
    }

    static function getAC($motivo,$detalle){

        //FALTA CACHE
        //$result = Cache::rememberForever(self::CACHE_GET_MOTIVO_DETALLE_ELIMINACION . $motivo . '-'. $detalle , function() use ($motivo,$detalle){
        $model = new mMotivoEliminacion();
        return $model->getAC($motivo,$detalle)->first();
        //});

        //return $result;
    }

}