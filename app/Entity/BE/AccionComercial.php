<?php

namespace App\Entity\BE;

use \Illuminate\Pagination\LengthAwarePaginator as Paginator;
use App\Model\BE\Lead as mLead;
use App\Model\BE\AccionComercial as mAccion;
use App\Model\BE\EjecutivoSectorista as mEjecutivoSectorista;

use App\Entity\Usuario as Usuario;
use App\Entity\LeadCampanha as LeadCampanha;
use App\Entity\BE\AccionResponsable as AccionResponsable;
use App\Entity\BE\LeadEliminado as LeadEliminado;
use App\Entity\Campanha as Estrategia;
use Jenssegers\Date\Date as Carbon;

class AccionComercial extends \App\Entity\Base\Entity {

    protected $_periodo;
    protected $_numdoc;
    protected $_codUnico;
    protected $_codSBS;
    protected $_departamento;
    protected $_provincia;
    protected $_distrito;
    protected $_direccion;
    protected $_telefono;
    protected $_tipoProspecto;
    protected $_flagCliente;
    protected $_deudaDirecta;
    protected $_nombre;
    protected $_segmento;
    protected $_estado;

    const ITEMS_PER_PAGE = 15;
    const TIPO_ASIGNACION= "EJECUTIVO";
    const TOOLTIP= "*";

    function setProperties($data) {
        $this->setValues([
            '_periodo' => $data->PERIODO,
            '_numdoc' => $data->NUM_DOC,
            '_departamento' => $data->DEPARTAMENTO,
            '_provincia' => $data->PROVINCIA,
            '_distrito' => $data->DISTRITO,
            '_direccion' => $data->DIRECCION,
            '_telefono' => $data->TELEFONO,
        ]);
    }

    function setValueToTable() {
        return $this->cleanArray([
            'PERIODO' => $this->_periodo,
            'NUM_DOC' => $this->_numdoc,
            'DEPARTAMENTO' => $this->_departamento,
            'PROVINCIA' => $this->_provincia,
            'DISTRITO' => $this->_distrito,
            'DIRECCION' => $this->_direccion,
            'TELEFONO' => $this->_telefono,
            'NOMBRE' => $this->_nombre,
            'TIPO_PROSPECTO' => $this->_tipoProspecto,
            'FLG_ES_CLIENTE' => $this->_flagCliente,
            'DEUDA_DIRECTA' => $this->_deudaDirecta,
            'COD_UNICO' => $this->_codUnico,
            'COD_SBS' => $this->_codSBS,
            'SEGMENTO_BANCA' => $this->_segmento,
            'ESTADO_LEAD' => $this->_estado,
        ]);
    }

    static function getTipos(){
        $model= new mAccion();
        return $model->getTipos()->get();
    } 

    static function getNumDoc($codUnico){
        $model= new mAccion();        
        return $model->getNumDoc($codUnico,self::sgetPeriodoBE())->first()->NUM_DOC;
    }

    static function getCodUnico($numDoc){
        $model= new mAccion();        
        return $model->getCodUnico($numDoc,self::sgetPeriodoBE())->first()->COD_UNICO;
    }

    static function getMesesAsignacion(\App\Entity\Usuario $en){
        $model= new mAccion();

        if (in_array($en->getValue('_rol'),Usuario::getEjecutivosBE()))
            return $model->getMesesAsignacion($en->getValue('_registro'))->get();
        else 
            return $model->getMesesAsignacion()->get();
    }
    static function getNombreAcciones($idAccion =NULL){
        $model= new mAccion();        
        return $model->getNombreAcciones($idAccion)->get();            
    }

    static function getNombreLista($idAcciones =[]){
        $model= new mAccion();        
        return $model->getNombreLista($idAcciones)->get();            
    }

    static function getAccionCliente($numDoc,$idAccion,$tooltip=null){
        $model=new mAccion();
        return $model->getAccionCliente(self::sgetPeriodoBE(),$numDoc,$idAccion,$tooltip)->first(); 
    }

    static function buscarCliente($codUnico){
        $model = new mAccion();
        return $model->getCliente(self::sgetPeriodoBE(),$codUnico)->first();
    }    

    static function getEtapaAccion($idAccion,$idEtapa=NULL){
        $model = new mAccion();
        return $model->getEtapaAccion($idAccion,$idEtapa)->get();
    }


    static function getCategorias(){
        $model = new mAccion();
        return $model->getCategorias()->get();
    }    

    static function getAccionesEstrategias(){
        $model = new mAccion();
        $acciones=$model->getAccionesEstrategias()->get();
        $estAcciones=[];
        foreach ($acciones as $accion){
                $estrategia = $accion->ESTRATEGIA;
                $estAcciones[$estrategia][] = $accion;
        }        
        return  $estAcciones;
    } 

    static function getAccionesExistentes($numDoc,$checks){
        $model = new mAccion();        
        $acciones=$model->getAccionesExistentes(self::sgetPeriodoBE(),$numDoc,$checks)->get();
        return $acciones;
    }


    //Utils
    static function getEstrategias($idAccion=null){
        $model = new mAccion();
        return $model->getEstrategias($idAccion)->get();
    }

    static function getEtapas($accion=null,$estrategia=null){
        $model = new mAccion();
        return $model->getEtapas($accion,$estrategia)->get();
    }

    static function getAcciones($estrategia=null){
        $model = new mAccion();
        return $model->getAcciones($estrategia)->get();
    }

    static function getMesesActivacion(){
        $model = new mAccion();
        return $model->getMesesActivacion()->get();
    }

    static function getAccionAvanzada($numDoc){
        $model = new mAccion();
        return $model->getAccionAvanzada($numDoc);
    }

    static function tieneCampanhaActiva($numDoc){
        $model = new mAccion();
        return $model->tieneCampanhaActiva(self::sgetPeriodoBE(),$numDoc);
    }
    function registrarAccion($codUnico,$ejecutivo,$datosAccion,$atributos,$insertar){

        //VALIDACIONES GENERALES PARA EL CLIENTE
        //Busco al cliente
        //Para obtener su documento y existe porque ya se llegó al registro
        $cliente = self::buscarCliente($codUnico); 

        //Si es que soy un analista de negocio
        //$ejecutivos=Usuario::getFarmersHunters($atributos['banca'],$atributos['zonal'],$atributos['jefatura']);
        //$regEjecutivos=[];

        //for($i=0;$i<$ejecutivos->count();$i++){
          //  $regEjecutivos[$i]=$ejecutivos[$i]->REGISTRO;
        //}

         //El cliente también puede estar sectorizado a un analista
          //  $regEjecutivos[$ejecutivos->count()]=$ejecutivo;

        //VALIDACIONES POR ACCIÓN
        //Se busca que el cliente no tenga la acción
        $model = new mAccion();
        $busqueda=self::getAccionCliente($cliente->NUM_DOC,$datosAccion['idAccion'],self::TOOLTIP);
        if($busqueda!=NULL){
            $this->setMessage('El cliente '.$cliente->NOMBRE.' ya tiene asignada la acción: '.self::getNombreAcciones($datosAccion['idAccion'])[0]->NOMBRE);
            return false;
        }
        
        //Debo de validar dependiendo del rol del ejecutivo => datosAccion['rol']
        if(in_array($atributos['rol'], array_merge(Usuario::getEjecutivosBE())) && ($datosAccion['delegado']==NULL || $datosAccion['delegado']=="NO DELEGAR")){
            //Si soy ejecutivo (o analista) y no delego
            if($datosAccion['kpi']==NULL or $datosAccion['fFin']==NULL){
                $this->setMessage('El KPI y la Fecha Fin son obligatorios para la acción '.self::getNombreAcciones($datosAccion['idAccion'])[0]->NOMBRE. ' registrada al cliente '.$cliente->NOMBRE);
                return false;
            }           

        }
        else if (in_array($atributos['rol'], Usuario::getEjecutivosProductoBE())){
            if($datosAccion['kpi']==NULL or $datosAccion['mesActiv']==NULL){
                $this->setMessage('El KPI y el mes de Activación son obligatorios para la acción '.self::getNombreAcciones($datosAccion['idAccion'])[0]->NOMBRE. ' registrada al cliente '.$cliente->NOMBRE);
                return false;
            }
        }

        $hoy = Carbon::now();
        if(isset($datosAccion['fFin']))
            if($hoy>$datosAccion['fFin']){
                //dd("HOLI");
                $this->setMessage('La fecha fin no puede ser menor a la fecha de inicio/registro');
                return false;
        }

        //Usuario irá a la tabla de WEBVPC_LEAD_CAMP_EST
        $usuarioRegistro=$ejecutivo;

        //Para los jefes debemos de conseguir el ejecutivo de negocio
        if(in_array($atributos['rol'], Usuario::getDivisionBE())){
            $ejecutivo=$cliente->REGISTRO_EN;
            if($datosAccion['delegado']==""){
                $this->setMessage('Debes seleccionar un ejecutivo cash a delegar');
                    return false;
            }
        }      

        //Tenemos que ver ahora si la acción es delegada o no
        //NO DELEGAR

        $flgDelegado=0;
        $flgAccion=0;
        if($datosAccion['delegado']==NULL || $datosAccion['delegado']=="NO DELEGAR"){
            $ejecutivoResponsable=$cliente->REGISTRO_EN;
        }
        else{
            $ejecutivoResponsable=$datosAccion['delegado'];
            $flgDelegado=1;
            $flgAccion=1;
        }

        //Para ejecutivos de producto
        if (in_array($atributos['rol'], Usuario::getEjecutivosProductoBE())){
            $ejecutivoResponsable=$ejecutivo;
        }

        //dd("VALIDEMOS",$cliente,$busqueda,$ejecutivo,$ejecutivoResponsable);
        //Datos de Lead Etapa - Se registra la primera etapa
        $etapaInicial= self::getEtapaAccion($datosAccion['idAccion'])->first();

        $leadEtapa = new LeadEtapa();
        $leadEtapa->setValues([
            '_periodo' => self::sgetPeriodoBE(),
            '_numdoc' => $cliente->NUM_DOC,
            '_etapa' => $etapaInicial->ID_ETAPA,
            '_estrategia' => $datosAccion['idAccion'],
            '_ejecutivoAsignado' => $ejecutivoResponsable,
            '_ejecutivo' => $ejecutivoResponsable,
            '_flgUltimo' => '1',
            '_fechaRegistro' => $hoy,
            '_tooltip'=>self::TOOLTIP,
        ]);

        //Datos de Lead - Estrategia
        $leadCampanha = new LeadCampanha();
        $leadCampanha->setValues([
            '_periodo' => self::sgetPeriodoBE(),
            '_lead' => $cliente->NUM_DOC,
            '_codUnico'=>$codUnico,
            '_fechaCarga' => $hoy,
            '_fechaActualizacion' => $hoy,
            '_campanha' => $datosAccion['idAccion'],
            '_fechaInicio'=>$hoy,
            '_fechaFin'=>$datosAccion['fFin'],            
            '_kpi'=>$datosAccion['kpi'],
            '_tooltip'=>self::TOOLTIP,
            '_flgHabilitado'=>'1',
            '_flgAccion'=>$flgAccion,
            '_mesActivacion'=>$datosAccion['mesActiv'],
            '_registroResponsable'=>$usuarioRegistro, //El que la registró
            '_codSectUniq'=>$cliente->COD_SECT_UNIQ_EN,
            '_prioridad'=>'1',         
        ]);

        $actividad = new Actividad();
        
        $actividad->setValues([
            '_numdoc' => $cliente->NUM_DOC,
            '_ejNegocio' => $ejecutivoResponsable,
            '_fechaActividad' => $hoy,
            '_titulo' => 'ASIGNADO',
            '_tipo' => ACTIVIDAD::TIPO_CAMBIO_ESTADO,
            '_fechaRegistro' => $hoy,
            '_periodo' => self::sgetPeriodoBE(),
            '_idCampEst' => $datosAccion['idAccion'],
        ]);

        //prueba commit
        //Agregar elemento en el timeline de actividades
        $actividad2 = new Actividad();
        $actividad2->setValues([
            '_numdoc' => $cliente->NUM_DOC,
            '_ejNegocio' => $ejecutivoResponsable,
            '_fechaActividad' => $hoy,
            '_titulo' => $etapaInicial->NOMBRE_ETAPA,
            '_tipo' => ACTIVIDAD::TIPO_CAMBIO_ESTADO,
            '_fechaRegistro' => $hoy,
            '_periodo' => self::sgetPeriodoBE(),
            '_idCampEst' => $datosAccion['idAccion'],
        ]);

        //Generamos los dos registros de responsables
        $encargado = new AccionResponsable();
        $visualizacion = new AccionResponsable();

        $encargado->setValues([
                    '_periodo'=>self::sgetPeriodoBE(),
                    '_accion'=>$datosAccion['idAccion'],
                    '_numdoc'=>$cliente->NUM_DOC,       
                    '_regResponsable'=>$ejecutivoResponsable,            
                    '_tooltip'=>self::TOOLTIP,
                    '_fechaRegistro'=>$hoy,                    
                    '_encargado'=>1,            
                    '_flgUltimo'=>1,            
                ]); 

        
        //Registramos la visualizacion

        if (in_array($atributos['rol'], array_merge(Usuario::getEjecutivosBE(),Usuario::getAnalistasInternosBE())) and !$flgDelegado){
                //Si la genera un  EN y no la delega
                $encargado->setValues([                                   
                    '_tipoEj'=>'EN',            
                ]); 
        }
        else{
                //Si la genera un EC, la debe de ver el EN o
                //Si la delega el EN al EC, la debe se ver el EN
                $encargado->setValues([                       
                        '_tipoEj'=>'EC',         
                ]);  

                $visualizacion->setValues([
                    '_periodo'=>self::sgetPeriodoBE(),
                    '_accion'=>$datosAccion['idAccion'],
                    '_numdoc'=>$cliente->NUM_DOC,
                    '_regResponsable'=>$cliente->REGISTRO_EN,
                    '_tooltip'=>self::TOOLTIP,
                    '_fechaRegistro'=>$hoy,
                    '_tipoEj'=>'EN',
                    '_encargado'=>0,            
                    '_flgUltimo'=>1,            
                ]);                           
        }

        if($insertar==1){
            //return back();
            
            //Debieron haber pasado todas las pruebas y se inserta correctamente
            if ($model->insertAccionComercial($leadEtapa->setValueToTable(),$leadCampanha->setValueToTable(),$actividad->setValueToTable(),$actividad2->setValueToTable(),$encargado->setValueToTable(),$visualizacion->setValueToTable())){
                return true;
            }else{
                $this->setMessage('Hubo un error en el servidor de base de datos');
                return false;
            }
        }
        else return true;
    }



    static function getClientesEjecutivo(\App\Entity\Usuario $en, $busqueda = [], $orden = [],$flgActividades=false) {
        $model = new mAccion();
        //Si es gerente de división
        if (in_array($en->getValue('_rol'),Usuario::getDivisionBE())){
            $query = $model->getClienteGerencia(self::sgetPeriodoBE(),$busqueda, $orden,$flgActividades);
        }
        //Si es Gerente de Banca
        elseif(in_array($en->getValue('_rol'),Usuario::getBanca())){            
            $query = $model->getClienteBanca(self::sgetPeriodoBE(), $en->getValue('_banca'),$busqueda,$orden,$flgActividades);
        } 
        //Si es Jefe Zonal
        elseif(in_array($en->getValue('_rol'),Usuario::getZonalesBE())){            
            $query = $model->getClienteZonal(self::sgetPeriodoBE(), $en->getValue('_zona'), $busqueda, $orden,$flgActividades);
        } 
        //Si es Jefatura
        elseif(in_array($en->getValue('_rol'),Usuario::getJefaturasBE())){
            $query = $model->getClienteJefatura(self::sgetPeriodoBE(), $en->getValue('_centro'), $busqueda, $orden,$flgActividades);
        }
        //Si es Ejecutivo de Negocios o de Producto
        else{
            $query = $model->getClienteEjecutivo(self::sgetPeriodoBE(), $en->getValue('_registro'),$en->getValue('_rol'),
                                                     $busqueda, $orden,$flgActividades);
        }       
        
        return $query;
    }


    static function getMisClientes(\App\Entity\Usuario $en, $busqueda, $orden = []){
        //filtrar los no clientes
        //$busqueda['cliente'] = 0;
        $query = self::getClientesEjecutivo($en,$busqueda,$orden);  

        $totalCount = $query->count();
       
        $results = $query
                ->take(self::ITEMS_PER_PAGE)
                ->skip(self::ITEMS_PER_PAGE * ($busqueda['page'] - 1))
                ->get();
                 
        $paginator = new Paginator($results, $totalCount, self::ITEMS_PER_PAGE, $busqueda['page']);
        return $paginator;   
    }


    static function getMiContacto($cliente, \App\Entity\Usuario $usuario) {
        $model = new mAccion();
        //Los EjecutivosProductoBE deben de poder buscar todos los clientes en actividades y contactos, mas no en su lista principal
        if (in_array($usuario->getValue('_rol'),Usuario::getEjecutivosProductoBE())){
            return $model->getClienteGerencia(self::sgetPeriodoBE(),['documento' => $cliente],null,true)->first();
        }
        return self::getClientesEjecutivo($usuario, ['documento' => $cliente],null,true)->first();        
    }

  
    static function getAtributoByEjecutivo($en,$columna){
        $model = new mLead();
        $atributo = '';
        $valor = '';
        switch ($columna){
            case 'etapa':
                $atributo = 'WET.ID_ETAPA';
                $valor = 'WET.NOMBRE';
                break;
            case 'estrategia':
                $atributo = 'WCE.ID_CAMP_EST';
                $valor = 'WCE.NOMBRE';
                break;
            case 'bcoPrincipal':
                $atributo = 'WL.BANCO_PRINCIPAL';
                $valor = 'WL.BANCO_PRINCIPAL';
                break;
            case 'bcoPrincipalGarantia':
                $atributo = 'WL.BANCO_GARANTIA';
                $valor = 'WL.BANCO_GARANTIA';
                break;
        }
        $list = self::getLeadsEjecutivo($en);
        $list->columns = null;
        $list = $list->addSelect($atributo . ' as VALOR',$valor. ' as NOMBRE')
            ->orderby($atributo)
            ->whereNotNull($atributo)
            ->get();

        $items = [];
        foreach ($list as $item) {
            $items[$item->VALOR] = $item->NOMBRE;
        }

        return $items;
    }

    function eliminarAccion($documento,$idAccion,$tooltip,$motivo,$detalle,$comentario){
        $model = new mAccion();
        $accionCliente=$model->getClienteEjecutivo(self::sgetPeriodoBE(),null,null,['documento' => $documento,
            'accion' => $idAccion,'tooltip'=>$tooltip],null,false)->first();

        //Verificar para el Motivo Otros
        if($motivo==MotivoEliminacion::MOTIVO_OTROS)  {
            $detalle=MotivoEliminacion::MOTIVO_OTROS;
        }
        //Conseguimos al Encargado (Cash o de Negocios)
        if($accionCliente->ENCARGADO_EN==1)
            $registroEjecutivo=$accionCliente->EN;      
        else if ($accionCliente->ENCARGADO_EC==1)
            $registroEjecutivo=$accionCliente->EC;      

        $leadEliminado=[
            'NUM_DOC' =>  $documento,
            'REGISTRO_EN' =>$registroEjecutivo,
            'FECHA_ELIMINACION' => Carbon::now(),
            'MOTIVO' => $motivo,
            'DETALLE' => $detalle,
            'COMENTARIO' => $comentario,
            'ID_CAMP_EST' => $idAccion,
            'PERIODO' => self::sgetPeriodoBE(),
            'TOOLTIP'=>$tooltip,
        ];


        $rMotivos = MotivoEliminacion::getAC($motivo,$detalle);

        $actividad = new Actividad();
        $actividad->setValues([
            '_numdoc' => $documento,
            '_ejNegocio' => $registroEjecutivo,
            '_fechaActividad' => Carbon::now(),
            '_titulo' => 'ELIMINADO',
            '_tipo' => ACTIVIDAD::TIPO_CAMBIO_ESTADO,
            '_temasComerciales' => 'Motivo: '. $rMotivos->DESC_MOTIVO .' - '. $rMotivos->DESC_DETALLE,
            '_temasCrediticios' => $comentario,
            '_fechaRegistro' => Carbon::now(),
            '_periodo' => self::sgetPeriodoBE(),
            '_idCampEst' => $idAccion
        ]);

        if ($model->eliminarAccion($leadEliminado,$actividad->setValueToTable())){
            return true;
        }else{
            $this->setMessage('Hubo un error en el servidor de base de datos');
            return false;
        }
    }

    function updateEstrella($data){

        $model = new mAccion();
        if ($model->updateEstrella($data)){
            return true;
        }else{
            $this->setMessage('Hubo un error al registrar su información. Inténtelo más tarde.');
            return false;
        }
    }

    function updateMes($data){
        
        $model=new mAccion();
        if ($model->updateMes($data)){
            return true;
        }
        else{
            return false;
        }
    }

    function updateKPI($data){
        
        $model=new mAccion();
        if ($model->updateKPI($data)){
            return true;
        }
        else{
            return false;
        }
    }


}