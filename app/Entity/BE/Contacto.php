<?php

namespace App\Entity\BE;

use App\Model\BE\Contacto as mContacto;
use App\Model\BE\AddContacto as mAddContacto;
use App\Entity\BE\AddContacto as AddContacto;

use Jenssegers\Date\Date as Carbon;

class Contacto extends \App\Entity\Base\Entity {

    protected $_id;
    protected $_numdoc;
    protected $_numdocContacto;
    protected $_nombre;
    protected $_apaterno;
    protected $_amaterno;
    protected $_cargo;
    protected $_email;
    protected $_direccion;
    protected $_flgVigente;
    protected $_ejecutivo;
    protected $_fechaRegistro;
    protected $_fechaActualizacion;
    protected $_visualizar;

    static function getByLead($documento = '') {


        $model = new mContacto();
        $data = $model->getByLead($documento)->get();

        //dd($data);
        /*$data = $model->getByLead(self::sgetPeriodoBE(), $documento)->get();*/
        

        foreach ($data as $key => $item) {
            $tipos = explode('|', $item->TIPO_CONTACTO);
            $flags = explode('|', $item->FLG_CONTACTO);
            $data[$key]->TIPO_CONTACTO = array_combine($tipos, $flags);
            
        }

        return $data;
    }

    function setValueToTable() {
        return $this->cleanArray([
            'ID_CONTACTO' => $this->_id,
            'NUM_DOC' => $this->_numdoc,
            'NUM_DOC_CONTACTO' => $this->_numdocContacto,
            'NOMBRE' => $this->_nombre,
            'APELLIDO_PATERNO' => $this->_apaterno,
            'APELLIDO_MATERNO' => $this->_amaterno,
            'CARGO' => $this->_cargo,
            'EMAIL' => $this->_email,
            'DIRECCION' => $this->_direccion,
            'FLG_VIGENTE' => $this->_flgVigente,
            'FECHA_REGISTRO' => ($this->_fechaRegistro)? $this->_fechaRegistro->toDateTimeString() : null, 
            'FECHA_ULTIMA_ACTUALIZACION' => ($this->_fechaRegistro)? $this->_fechaRegistro->toDateTimeString() : Carbon::now()->toDateTimeString(), 
            'REGISTRO_EN' => $this->_ejecutivo, 
            'VISUALIZAR'=>$this->_visualizar,         
        ]);
    }

    function actualizar() {
        $model = new mContacto();
        return $model->actualizar($this->setValueToTable());
    }

    function setContactoTipo($idcontacto,$tipo,$active){
        if ($active){

        }else{

        }
    }

    function setContacto(AddContacto $addContacto){

        $periodo = parent::getPeriodoBE();
        $this->_fechaRegistro = Carbon::now();
        $this->_visualizar=1;
        $addContacto->setValues([
            '_periodo' => $periodo,
        ]);
      

        $model = new mContacto;
        $this->_id = $model->setContacto($this->setValueToTable(),$addContacto->setValueToTable());
        if ($this->_id){
            return true;
        }else{
            $this->setMessage('Hubo un error al registrar su información. Inténtelo más tarde.');
            return false;
        }


    }

    function quitarContacto(){

        $model = new mContacto();
        
        if ($model->quitarContacto($this->setValueToTable())){
            $this->setMessage('Se elimino el contacto');
            return true;
        }else{
            $this->setMessage('Hubo un error al registrar su información. Inténtelo más tarde.');
            return false;
        }


    }
}
