<?php

namespace App\Entity\BE;

use App\Model\BE\AddContacto as mAddContacto;

use Jenssegers\Date\Date as Carbon;

class AddContacto extends \App\Entity\Base\Entity {

    protected $_periodo;
    protected $_idContacto;
    protected $_tipoRegistroContacto;
    protected $_tipoContacto;
    protected $_telefono;
    protected $_anexo;
    protected $_feedback;
    protected $_fechaRegistro;
    protected $_fechaRegistroFeedback;
    protected $_ejecutivo;
    
    const FEEDBACK_POSITIVO = 'POSITIVO';
    const FEEDBACK_NEGATIVO = 'NEGATIVO';
    
    static function getFeedbacks(){
        return [self::FEEDBACK_POSITIVO,self::FEEDBACK_NEGATIVO];
    }
    
    function setValueToTable() {
        return $this->cleanArray([
            'PERIODO' => $this->_periodo,
            'ID_CONTACTO' => $this->_idContacto,
            'TIPO_REGISTRO_CONTACTO' => $this->_tipoRegistroContacto,
            'TIPO_CONTACTO' => $this->_tipoContacto,
            'VALOR' => $this->_telefono,
            'ANEXO' => $this->_anexo,
            'FEEDBACK' => $this->_feedback,
            'FECHA_REGISTRO' => ($this->_fechaRegistro)? $this->_fechaRegistro->toDateTimeString() : null,
            'FECHA_REGISTRO_FEEDBACK' => ($this->_fechaRegistroFeedback)? $this->_fechaRegistroFeedback->toDateTimeString() : null,
            'REGISTRO_EN' => $this->_ejecutivo
        ]);
    }

    function registrar() {

        $this->_periodo = parent::getPeriodoBE();
        $this->_fechaRegistro = Carbon::now();
        $this->_fechaRegistroFeedback = Carbon::now();

        $model = new mAddContacto();

        if ($model->registrar($this->setValueToTable())){
            return true;
        } else {
            $this->setMessage('Hubo un error al registrar su información. Inténtelo más tarde.');
            return false;
        }

    }
    
    function actualizar() {
        //$model = new mContacto();
        //return $model->actualizar($this->setValueToTable());
    }
    
    static function getByContactos($contactos,$format = true){
        $model = new mAddContacto();

        $contactosid = array();
        if($format){
            foreach ($contactos as $contacto) {
                $contactosid[] = $contacto->ID_CONTACTO;
            }
        }

        return $model->getByContactos($contactosid)->get();
    }
    

    function setFeedback(){
        $this->_fechaRegistroFeedback = Carbon::now();
        $this->_periodo = parent::getPeriodoBE();

        $model = new mAddContacto();
        if ($model->actualizarFeedback($this->setValueToTable())){
            return true;
        } else {
            $this->setMessage('Hubo un error al registrar su información. Inténtelo más tarde.');
            return false;
        }
    }

    function quitarFeedback(){
        $this->_periodo = parent::getPeriodoBE();
        $this->_feedback = 'NULL';

        $model = new mAddContacto();
        if ($model->actualizarFeedback($this->setValueToTable())){
            return true;
        } else {
            $this->setMessage('Hubo un error al registrar su información. Inténtelo más tarde.');
            return false;
        }
    }
}