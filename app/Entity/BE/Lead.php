<?php

namespace App\Entity\BE;

use \Illuminate\Pagination\LengthAwarePaginator as Paginator;
use App\Model\BE\Lead as mLead;

use App\Entity\BE\LeadSectorista as LeadSectorista;
use App\Model\BE\EjecutivoSectorista as mEjecutivoSectorista;

use App\Entity\Usuario as Usuario;
use App\Entity\LeadCampanha as LeadCampanha;
use App\Entity\BE\LeadEliminado as LeadEliminado;
use App\Entity\Campanha as Estrategia;
use Jenssegers\Date\Date as Carbon;

class Lead extends \App\Entity\Base\Entity
{

    protected $_periodo;
    protected $_numdoc;
    protected $_codUnico;
    protected $_codSBS;
    protected $_departamento;
    protected $_provincia;
    protected $_distrito;
    protected $_direccion;
    protected $_telefono;
    protected $_tipoProspecto;
    protected $_flagCliente;
    protected $_deudaDirecta;
    protected $_nombre;
    protected $_segmento;
    protected $_estado;
    protected $_flgLinkedin;

    const ITEMS_PER_PAGE = 15;
    const REFERIDO = 'R';

    function setProperties($data)
    {
        $this->setValues([
            '_periodo' => $data->PERIODO,
            '_numdoc' => $data->NUM_DOC,
            '_codUnico' => $data->COD_UNICO,
            '_departamento' => $data->DEPARTAMENTO,
            '_provincia' => $data->PROVINCIA,
            '_distrito' => $data->DISTRITO,
            '_direccion' => $data->DIRECCION,
            '_telefono' => $data->TELEFONO,
        ]);
    }
    static function confirmarEliminar($coddoc,$registro,$id,$registro_EN,$empresa){
        $model = new mLead();
        $actividad = new Actividad();
        $actividad->setValues([
            '_numdoc' => $coddoc,
            '_ejNegocio' => $registro,
            '_fechaActividad' => Carbon::now(),
            '_titulo' => 'ELIMINACION DE LEAD',
            '_tipo' => ACTIVIDAD::TIPO_CAMBIO_ESTADO,
            '_temasComerciales' => '',
            '_temasCrediticios' => '',
            '_fechaRegistro' => Carbon::now(),
            '_periodo' => self::sgetPeriodoBE(),
            '_idCampEst' => $id,
            '_flgLinkedin' => null
        ]);
        return $model->confirmarEliminar($coddoc,self::sgetPeriodoBE(),$actividad->setValueToTable(),$registro_EN,$empresa);
    }
    static function getRegistroJefe($registro_EN)
    {
        $model = new mLead();
        return $model->getRegistroJefe($registro_EN);
    }
    static function updateVerificado($coddoc,$registro,$id,$registro_EN,$empresa)
    {
        $model = new mLead();
        $actividad = new Actividad();
        $actividad->setValues([
            '_numdoc' => $coddoc,
            '_ejNegocio' => $registro,
            '_fechaActividad' => Carbon::now(),
            '_titulo' => 'RECHAZO DE ELIMINACION',
            '_tipo' => ACTIVIDAD::TIPO_CAMBIO_ESTADO,
            '_temasComerciales' => '',
            '_temasCrediticios' => '',
            '_fechaRegistro' => Carbon::now(),
            '_periodo' => self::sgetPeriodoBE(),
            '_idCampEst' => $id,
            '_flgLinkedin' => null
        ]);
        $model->updateVerificado($coddoc,$actividad->setValueToTable(),$registro_EN,$empresa);
    }
    function setValueToTable()
    {
        return $this->cleanArray([
            'PERIODO' => $this->_periodo,
            'NUM_DOC' => $this->_numdoc,
            'DEPARTAMENTO' => $this->_departamento,
            'PROVINCIA' => $this->_provincia,
            'DISTRITO' => $this->_distrito,
            'DIRECCION' => $this->_direccion,
            'TELEFONO' => $this->_telefono,
            'NOMBRE' => $this->_nombre,
            'TIPO_PROSPECTO' => $this->_tipoProspecto,
            'FLG_ES_CLIENTE' => $this->_flagCliente,
            'DEUDA_DIRECTA' => $this->_deudaDirecta,
            'COD_UNICO' => $this->_codUnico,
            'COD_SBS' => $this->_codSBS,
            'SEGMENTO_BANCA' => $this->_segmento,
            'ESTADO_LEAD' => $this->_estado,
            'FLG_LINKEDIN' => $this->_flgLinkedin,
        ]);
    }

    function actualizar()
    {

        $this->_periodo = parent::getPeriodoBE();
        $model = new mLead();
        return $model->actualizar($this->setValueToTable());
    }

    static function autoCompleteLead(\App\Entity\Usuario $en, $lead, $regEjecutivo = null)
    {
        $model = new mLead();
        $query = '';
        //Si es gerente de división  o Ejecutivo de Producto
        if (in_array($en->getValue('_rol'), array_merge(Usuario::getDivisionBE(), Usuario::getEjecutivosProductoBE()))) {
            $query = $model->getAutocompleteGerencia(self::sgetPeriodoBE(), $lead);
        }
        //Si es Gerente de Banca
        elseif (in_array($en->getValue('_rol'), Usuario::getBanca())) {
            $query = $model->getAutocompleteBanca(self::sgetPeriodoBE(), $lead, $en->getValue('_banca'), $regEjecutivo);
            \Debugbar::info("2");
        }
        //Si es Jefe Zonal
        elseif (in_array($en->getValue('_rol'), Usuario::getZonalesBE())) {
            $query = $model->getAutocompleteZonal(self::sgetPeriodoBE(), $lead, $en->getValue('_zona'), $regEjecutivo);
            \Debugbar::info("3");
        }
        //Si es Jefatura
        elseif (in_array($en->getValue('_rol'), Usuario::getJefaturasBE())) {
            $query = $model->getAutocompleteJefatura(self::sgetPeriodoBE(), $lead, $en->getValue('_centro'), $regEjecutivo);
            \Debugbar::info("4");
        }
        //Si es Ejecutivo
        else {
            $query = $model->getAutocompleteEjecutivo(self::sgetPeriodoBE(), $lead, $en->getValue('_registro'));
            \Debugbar::info("5");
        }

        return $query->get();
    }
    static function getProductos($documento=null)
    {
        $model = new mLead();
        return $model->getProductos($documento);
    }
    static function getLead($nombre = '', $documento = '')
    {
        $model = new mLead();
        return $model->getLeadContactos(self::sgetPeriodoBE(), $lead, $ejecutivo)->first();
    }

    static function getLeadsEjecutivo(\App\Entity\Usuario $en, $busqueda = [], $prospecto = true, $orden = [])
    {

        $model = new mLead();
        //Si es gerente de división
        if (in_array($en->getValue('_rol'), array_merge(Usuario::getDivisionBE(), Usuario::getEjecutivosProductoBE()))) {
            $query = $model->getLeadGerencia(self::sgetPeriodoBE(), $prospecto, null, $busqueda, $orden);
        }
        //Si es Gerente de Banca
        elseif (in_array($en->getValue('_rol'), Usuario::getBanca())) {
            $query = $model->getLeadBanca(self::sgetPeriodoBE(), $en->getValue('_banca'), $prospecto, null, $busqueda, $orden);
        }
        //Si es Jefe Zonal
        elseif (in_array($en->getValue('_rol'), Usuario::getZonalesBE())) {
            $query = $model->getLeadZonal(self::sgetPeriodoBE(), $en->getValue('_zona'), $prospecto, null, $busqueda, $orden);
        }
        //Si es Jefatura
        elseif (in_array($en->getValue('_rol'), Usuario::getJefaturasBE())) {
            $query = $model->getLeadJefatura(self::sgetPeriodoBE(), $en->getValue('_centro'), $prospecto, null, $busqueda, $orden);
        }
        //Si es Ejecutivo
        else {
            $query = $model->getLeadEjecutivo(self::sgetPeriodoBE(), $en->getValue('_registro'), $prospecto, null, $busqueda, $orden);
        }

        return $query;
    }

    static function getMisProspectos(\App\Entity\Usuario $en, $busqueda, $orden = [])
    {

        //filtrar los no clientes
        $busqueda['cliente'] = 0;
        $query = self::getLeadsEjecutivo($en, $busqueda, true, $orden);

        $totalCount = $query->count();
        $start = microtime(true);
        $results = $query
            ->take(self::ITEMS_PER_PAGE)
            ->skip(self::ITEMS_PER_PAGE * ($busqueda['page'] - 1))
            ->get();
        $time_elapsed = microtime(true) - $start;
        //DD($query->toSql());
        //DD($time_elapsed);

        $paginator = new Paginator($results, $totalCount, self::ITEMS_PER_PAGE, $busqueda['page']);

        return $paginator;
    }

    static function getMiContacto($lead, \App\Entity\Usuario $en)
    {
        $model = new mLead();
        return self::getLeadsEjecutivo($en, ['documento' => $lead], false)->first();
    }


    static function getAtributoByEjecutivo($en, $columna)
    {
        $model = new mLead();
        $atributo = '';
        $valor = '';
        switch ($columna) {
            case 'etapa':
                $atributo = 'WET.ID_ETAPA';
                $valor = 'WET.NOMBRE';
                break;
            case 'estrategia':
                $atributo = 'WCE.ID_CAMP_EST';
                $valor = 'WCE.NOMBRE';
                break;
            case 'bcoPrincipal':
                $atributo = 'WL.BANCO_PRINCIPAL';
                $valor = 'WL.BANCO_PRINCIPAL';
                break;
            case 'bcoPrincipalGarantia':
                $atributo = 'WL.BANCO_GARANTIA';
                $valor = 'WL.BANCO_GARANTIA';
                break;
        }
        $list = self::getLeadsEjecutivo($en);
        $list->columns = null;
        $list = $list->addSelect($atributo . ' as VALOR', $valor . ' as NOMBRE')
            ->orderby($atributo)
            ->whereNotNull($atributo)
            ->get();

        $items = [];
        foreach ($list as $item) {
            $items[$item->VALOR] = $item->NOMBRE;
        }

        //dd($items);
        return $items;
    }

    static function buscarReferido($numdoc, $codunico = null)
    {
        $model = new mLead();
        return $model->getReferido(self::sgetPeriodoBE(), $numdoc, $codunico)->first();
    }

    function buscarDatosReferido($numdoc, $codunico = null)
    {
        $model = new mLead();
        return $model->buscarDatosReferido(self::sgetPeriodoBE(), $numdoc, $codunico)->first();
    }

    static function buscarReferidoExtra($numdoc)
    {
        $model = new mLead();
        return $model->getReferidoExtra(self::sgetPeriodoBE(), $numdoc)->first();
    }

    static function referidosMesActual($registro)
    {

        $model = new mLead();
        return $model->referidosMesActual(self::sgetPeriodoBE(), $registro)->get()->first();
    }

    function registrarReferido($documento, $ejecutivo, $segmento, $atributos)
    {
        $this->_numdoc = $documento;
        $this->_tipoProspecto = self::REFERIDO;
        $this->_flagCliente = '0';
        $this->_estado = 1;
        $this->_periodo = $this->getPeriodoBE();
        $this->_flgLinkedin = $atributos['flgLinkedin'];

        //Busco al referido
        $referido = self::buscarReferido($documento);

        //Busco al sectorista
        $modelEjecutivoSec = new mEjecutivoSectorista();
        $sectorista = $modelEjecutivoSec->getSectoristaByEjecutivo($ejecutivo);

        //Si existe en la tabla de referidos
        if ($referido) {
            $this->_nombre = $referido->NOMBRE;
            $this->_deudaDirecta = $referido->DEUDA_DIRECTA;
            $this->_codUnico = $referido->COD_UNICO;
            $this->_codSBS = $referido->COD_SBS;
            $this->_segmento = $segmento;
        } else {
            $this->_nombre = $atributos['nombre'];
        }

        $hoy = Carbon::now();

        //Agregando datos de Lead al Sectorista
        $leadSectorista = new leadSectorista();
        $leadSectorista->setValues([
            '_periodo' => self::sgetPeriodoBE(),
            '_numdoc' => $documento,
            '_ejecutivo' => $ejecutivo,
            //'_codigoUnicoSectorista' => $sectorista->COD_SECT_UNIQ_EN,
            '_flgUltimo' => '1',
            '_fechaRegistro' => $hoy
        ]);

        //Datos de Lead Etapa - Se registra el pendiente y el me interesa
        $leadEtapa = new LeadEtapa();
        $leadEtapa->setValues([
            '_periodo' => self::sgetPeriodoBE(),
            '_numdoc' => $documento,
            '_etapa' => Etapa::PENDIENTE,
            '_estrategia' => Estrategia::ESTRATEGIA_BE_REFERIDO,
            '_ejecutivoAsignado' => $ejecutivo,
            '_ejecutivo' => $ejecutivo,
            '_flgUltimo' => '0',
            '_fechaRegistro' => $hoy
        ]);

        $leadEtapa2 = new LeadEtapa();
        $leadEtapa2->setValues([
            '_periodo' => self::sgetPeriodoBE(),
            '_numdoc' => $documento,
            '_etapa' => Etapa::ME_INTERESA,
            '_estrategia' => Estrategia::ESTRATEGIA_BE_REFERIDO,
            '_ejecutivo' => $ejecutivo,
            '_ejecutivoAsignado' => $ejecutivo,
            '_flgUltimo' => '1',
            '_fechaRegistro' => $hoy
        ]);

        //Datos de Lead - Estrategia
        $leadCampanha = new LeadCampanha();
        $leadCampanha->setValues([
            '_periodo' => self::sgetPeriodoBE(),
            '_lead' => $documento,
            '_fechaActualizacion' => $hoy,
            '_fechaCarga' => $hoy,
            '_campanha' => Estrategia::ESTRATEGIA_BE_REFERIDO,

        ]);


        $actividad = new Actividad();
        $actividad->setValues([
            '_numdoc' => $documento,
            '_ejNegocio' => $ejecutivo,
            '_fechaActividad' => $hoy,
            '_titulo' => 'ASIGNADO',
            '_tipo' => ACTIVIDAD::TIPO_CAMBIO_ESTADO,
            '_fechaRegistro' => Carbon::now(),
            '_periodo' => self::sgetPeriodoBE(),
            '_idCampEst' => Estrategia::ESTRATEGIA_BE_REFERIDO,
        ]);


        //Agregar elemento en el timeline de actividades
        $actividad2 = new Actividad();
        $actividad2->setValues([
            '_numdoc' => $documento,
            '_ejNegocio' => $ejecutivo,
            '_fechaActividad' => $hoy,
            '_titulo' => 'ME INTERESA',
            '_tipo' => ACTIVIDAD::TIPO_CAMBIO_ESTADO,
            '_fechaRegistro' => Carbon::now(),
            '_periodo' => self::sgetPeriodoBE(),
            '_idCampEst' => Estrategia::ESTRATEGIA_BE_REFERIDO,
        ]);

        $model = new mLead();
        if ($model->insertReferido(
            $this->setValueToTable(),
            $leadEtapa->setValueToTable(),
            $leadEtapa2->setValueToTable(),
            $leadCampanha->setValueToTable(),
            $leadSectorista->setValueToTable(),
            $actividad->setValueToTable(),
            $actividad2->setValueToTable()
        )) {
            return true;
        } else {
            $this->setMessage('Hubo un error en el servidor de base de datos');
            return false;
        }
    }

    function eliminarLead($documento, $motivo, $detalle, $comentario, $flgLinkedin = 0)
    {
        $model = new mLead();
        $lead = $model->getLeadEjecutivo(self::sgetPeriodoBE(), null, null, $documento, null, null)->first();
        $registro_JEFE  = self::getRegistroJefe($lead->REGISTRO_EN);
        // $leadEliminado = new LeadEliminado();
        $leadEliminado = [
            'NUM_DOC' =>  $documento,
            'REGISTRO_EN' => $lead->REGISTRO_EN,
            
            'FECHA_ELIMINACION' => Carbon::now(),
            'MOTIVO' => $motivo,
            'DETALLE' => $detalle,
            'COMENTARIO' => $comentario,
            'ID_CAMP_EST' => $lead->ESTRATEGIA_ID,
            'PERIODO' => self::sgetPeriodoBE(),
            'FLG_LINKEDIN' => $flgLinkedin,
        ];
        $rMotivos = MotivoEliminacion::get($motivo, $detalle);

        $actividad = new Actividad();
        $actividad->setValues([
            '_numdoc' => $documento,
            '_ejNegocio' => $lead->REGISTRO_EN,
            '_fechaActividad' => Carbon::now(),
            '_titulo' => $registro_JEFE?'PENDIENTE DE ELIMINACION':'ELIMINADO',
            '_tipo' => ACTIVIDAD::TIPO_CAMBIO_ESTADO,
            '_temasComerciales' => 'Motivo: ' . $rMotivos->DESC_MOTIVO . ' - ' . $rMotivos->DESC_DETALLE,
            '_temasCrediticios' => $comentario,
            '_fechaRegistro' => Carbon::now(),
            '_periodo' => self::sgetPeriodoBE(),
            '_idCampEst' => $lead->ESTRATEGIA_ID,
            '_flgLinkedin' => $flgLinkedin
        ]);
        $result = $model->eliminarLead($leadEliminado, $actividad->setValueToTable(),$lead->EJECUTIVO_NOMBRE,$lead->NOMBRE,$registro_JEFE);
        if($result==0){
            $this->setMessage('Hubo un error en el servidor de base de datos');
        }   
        return $result;
       
    }
    static function BuscarReasignacion($numdoc)
    {
        $model = new mLead();
        return $model->getInfoReasignacion(self::sgetPeriodoBE(), $numdoc)->first();
    }

    static function getCantCliwithDatosOrdenantes($doc)
    {
        $model = new mLead();
        return $model->getCantCliwithDatosOrdenantes($doc);
    }

    static function getDetalleVolumenPAP($doc)
    {
        $model = new mLead();
        return $model->getDetalleVolumenPAP($doc);
    }
    static function getDetalleVolumenPAP2($doc)
    {
        $model = new mLead();
        return $model->getDetalleVolumenPAP2($doc);
    }

    static function getSectoristaArbol($login)
    {
        $model = new mLead();
        return $model->getSectoristaArbol($login);
    }

    function reasignar($data)
    {
        $lead = new mLead();
        $nuevolead = 0;
        if ($lead->getLeadByNumDesc($data->getValue('_num_doc')) == null || $lead->getLeadByNumDesc($data->getValue('_num_doc'))->PERIODO != self::sgetPeriodoBE()) {
            $nuevolead = 1;
            $leadnuevo = new Lead();
            $leadnuevo->setValues([
                '_periodo' => self::sgetPeriodoBE(),
                '_numdoc' => $data->getValue('_num_doc'),
                '_nombre' => !empty($lead->getDatosCLientesVPC($data->getValue('_num_doc'))->NOMBRE_COMPLETO) ? $lead->getDatosCLientesVPC($data->getValue('_num_doc'))->NOMBRE_COMPLETO : null,
                '_flagCliente' => '0',
                '_estado' => '1',
                '_tipoProspecto' => 'R'
            ]);
        }

        $nuevacampaña = 0;

        if ($lead->LeadCampEstByNum($data->getValue('_num_doc'), self::sgetPeriodoBE()) == null || $lead->LeadCampEstByNum($data->getValue('_num_doc'), self::sgetPeriodoBE())->PERIODO != self::sgetPeriodoBE()) {
            $nuevacampaña = 1;
            $nuevaleadcampaña = new LeadCampanha();
            $nuevaleadcampaña->setValues([
                '_periodo' => self::sgetPeriodoBE(),
                '_lead' => $data->getValue('_num_doc'),
                '_fechaActualizacion' => Carbon::now(),
                '_canalDespliegue' => 'EN',
                '_fechaCarga' => Carbon::now(),
                '_campanha' => $lead->LeadCampEstByNum($data->getValue('_num_doc'), self::sgetPeriodoBE()) ? $lead->LeadCampEstByNum($data->getValue('_num_doc'), self::sgetPeriodoBE())->ID_CAMP_est : 38,
            ]);
        }


        $leadsectorista = new LeadSectorista();
        $leadsectorista->setValues([
            '_periodo' => self::sgetPeriodoBE(),
            '_numdoc' => $data->getValue('_num_doc'),
            '_ejecutivo' => $data->getValue('_registroDest'),
            '_codigosectorista' => $lead->getSectoristaArbol($data->getValue('_registroDest'))->COD_SECT_LARGO,
            '_codigoUnicoSectorista' => $lead->getSectoristaArbol($data->getValue('_registroDest'))->COD_SECT_UNIQ_EN,
            '_flgUltimo' => '1',
            '_flgcliente' => !empty($lead->getLeadByNum($data->getValue('_num_doc'), self::sgetPeriodoBE())->FLG_ES_CLIENTE) ? $lead->getLeadByNum($data->getValue('_num_doc'), self::sgetPeriodoBE())->FLG_ES_CLIENTE : 0,
            '_fechaRegistro' => Carbon::now()
        ]);


        $leadEtapa = new LeadEtapa();
        $leadEtapa->setValues([
            '_periodo' => self::sgetPeriodoBE(),
            '_numdoc' => $data->getValue('_num_doc'),
            '_etapa' => Etapa::PENDIENTE,
            '_estrategia' => !empty(self::getCampaña($data->getValue('_num_doc'))->ID_CAMP_est) ? self::getCampaña($data->getValue('_num_doc'))->ID_CAMP_est : 38,
            '_ejecutivoAsignado' => $data->getValue('_registroDest'),
            '_ejecutivo' => $data->getValue('_registroDest'),
            '_flgUltimo' => '1',
            '_fechaRegistro' => Carbon::now()
        ]);

        $actividad = new Actividad();
        $actividad->setValues([
            '_numdoc' => $data->getValue('_num_doc'),
            '_ejNegocio' => $data->getValue('_registroDest'),
            '_fechaActividad' => Carbon::now(),
            '_titulo' => 'ASIGNADO',
            '_tipo' => ACTIVIDAD::TIPO_CAMBIO_ESTADO,
            '_fechaRegistro' => Carbon::now(),
            '_periodo' => self::sgetPeriodoBE(),
            '_idCampEst' => $lead->LeadCampEstByNum($data->getValue('_num_doc'), self::sgetPeriodoBE()) ? $lead->LeadCampEstByNum($data->getValue('_num_doc'), self::sgetPeriodoBE())->ID_CAMP_est : 38,
        ]);
        $model = new mLead();
        return $model->reasignar(
            $nuevolead,
            $nuevolead ? $leadnuevo->setValueToTable() : null,
            $nuevacampaña,
            $nuevacampaña ? $nuevaleadcampaña->setValueToTable() : null,
            $leadsectorista->setValueToTable(),
            $leadEtapa->setValueToTable(),
            $actividad->setValueToTable(),
            $data
        );
    }

    static function getCampaña($numdoc)
    {
        $model = new mLead();
        return $model->getCampaña($numdoc, self::sgetPeriodoBE());
    }

    function getLeadByNum($numdoc)
    {
        $model = new mLead();
        return $model->getLeadByNum($numdoc, self::sgetPeriodoBE());
    }

    function ClientesVPC($numdoc = null, $codunico = null)
    {
        $model = new mLead();
        return $model->ClientesVPC($numdoc, $codunico);
    }

    function Reasignaciones($numdoc = null, $codunico = null)
    {
        $model = new mLead();
        return $model->Reasignaciones($numdoc, $codunico);
    }
}
