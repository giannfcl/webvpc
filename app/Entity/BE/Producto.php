<?php

namespace App\Entity\BE;

use App\Model\BE\Producto as mProducto;

class Producto extends \App\Entity\Base\Entity {


	static function getAll(){
        $model = new mProducto();
        return $model->listar()->get();
    }
}