<?php

namespace App\Entity\BE;

use App\Model\BE\Producto as mProducto;

class EnoteMesCarga extends \App\Entity\Base\Entity {


	static function getAll(){
        return [
        	['key' => '201805' , 'value' => 'Mayo 2018']
        	['key' => '201806' , 'value' => 'Junio 2018']
        	['key' => '201807' , 'value' => 'Julio 2018']
        	['key' => '201808' , 'value' => 'Agosto 2018']
        ];
    }

}