<?php

namespace App\Entity\BE;


class SemaforosEnote extends \App\Entity\Base\Entity {


	const COLOR_SEMAFORO_ROJO = '#CC3300';
    const COLOR_SEMAFORO_VERDE = '#009900';
    const COLOR_SEMAFORO_AMBAR = '#FFCC00';

    const ENOTE_SEMAFORO_CONFIG2 = [
                                    ['min' => 1, 'max' => PHP_INT_MAX, 'color' => self::COLOR_SEMAFORO_VERDE],
                                    ['min' => 0.95, 'max' => 1, 'color' => self::COLOR_SEMAFORO_AMBAR],
                                    ['min' => -999999, 'max' => 0.95, 'color' => self::COLOR_SEMAFORO_ROJO],
                                ];                                

    static function getColorSemaforoSalAct($var){
        $color = '';
        if ($var>=1)
                return self::COLOR_SEMAFORO_VERDE;
        if ($var=0)
                return self::COLOR_SEMAFORO_AMBAR;
        if ($var<1)
                return self::COLOR_SEMAFORO_ROJO;    
        return $color;
    }


    static function getColorSemaforoCertero($var){
        $color = '';

        foreach (self::ENOTE_SEMAFORO_CONFIG2 as $rango) {
          
            if ($var >= $rango['min'] && $var < $rango['max'])
                return $rango['color'];
          
        }
        return $color;
    }

}


