<?php

namespace App\Entity\BE;

use App\Model\BE\NotaEjecutivo as mNotaEjecutivo;

use Jenssegers\Date\Date as Carbon;

class NotaEjecutivo extends \App\Entity\Base\Entity {

    protected $_id;
    protected $_periodo;
    protected $_numdoc;
    protected $_nota;
    protected $_fechaRegistro;
    protected $_ejecutivo;
    protected $_idAccion;
    protected $_operacion;
    protected $_tooltip;



    function setValueToTable() {
        return $this->cleanArray([
            'NOTA_ID' => $this->_id,
            'PERIODO' => $this->_periodo,
            'NUM_DOC' => $this->_numdoc,
            'REGISTRO_EN' => $this->_ejecutivo,
            'NOTA' => $this->_nota,
            'FECHA_REGISTRO' => ($this->_fechaRegistro) ? $this->_fechaRegistro->toDateTimeString() : null,
            'ID_CAMP_EST'=>$this->_idAccion,
            'ID_OPERACION'=>$this->_operacion,
            'TOOLTIP'=>$this->_tooltip,
        ]);
    }

    function agregar(){
        $this->_periodo = $this->getPeriodoBE();
        $this->_fechaRegistro = Carbon::now();

        $nota = new mNotaEjecutivo();
        if ($this->_id = $nota->guardar($this->setValueToTable())){
            return true;
        }else{
            $this->setMessage('Hubo un error en el servidor al registrar tu información. Intenta nuevamente');
            return false;
        }
    }

    static function listar($numdoc,$ejecutivo,$accionComercial=NULL,$tooltip=NULL){
        $model = new mNotaEjecutivo();
        return $model->listar($numdoc,$ejecutivo,$accionComercial,$tooltip)->get()->take(5);
    }

    static function listarByOperacion($operacion){
        $model = new mNotaEjecutivo();
        return $model->listarByOperacion($operacion)->get()->take(10);
    }

    function eliminar(){
        $model = new mNotaEjecutivo();

        if ($model->eliminar($this->_id)){
            return true;
        }else{
            $this->setMessage('Hubo un error en el servidor. Intenta nuevamente');
            return false;
        }
    }
}
