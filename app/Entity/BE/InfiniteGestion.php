<?php

namespace App\Entity\BE;

use \Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Jenssegers\Date\Date as Carbon;
use App\Model\BE\InfiniteBase as mBase;

class InfiniteBase extends \App\Entity\Base\Entity {

    protected $_fecha;
    protected $_idClie;
    protected $_numDoc;
    protected $_estado;

    function setValueToTable() {
        return $this->cleanArray([
                    'FECHA' => $this->$_fecha,
                    'ID_CLIE' => $this->$_idClie,
                    'NUM_DOCUMENTO' => $this->$_numDoc,
                    'ESTADO' => $this->$_estado,                    
        ]);
    }

}
