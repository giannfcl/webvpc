<?php

namespace App\Entity\BE;

use Jenssegers\Date\Date as Carbon;
use App\Model\BE\Caidas as mCaidas;

class Caidas extends \App\Entity\Base\Entity {


    static function getCaidasProyectadas($periodo,$jefatura = null,$zonal = null,$banca = null){
        
        $model = new mCaidas();
        return $model->getCaidasProyectadas($periodo,$jefatura,$zonal,$banca)->get();    
    }
}    