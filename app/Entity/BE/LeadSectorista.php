<?php

namespace App\Entity\BE;

use App\Model\BE\LeadEtapa as mLeadEtapa;
use App\Model\BE\Lead as mLead;
use Jenssegers\Date\Date as Carbon;

class LeadSectorista extends \App\Entity\Base\Entity {

    protected $_periodo;
    protected $_numdoc;
    protected $_ejecutivo;
    protected $_codigoUnicoSectorista;
    protected $_fechaRegistro;
    protected $_flgUltimo;
    protected $_flgcliente;
    protected $_codigosectorista;

    function setProperties($data) {
    }

    function setValueToTable() {
        return $this->cleanArray([
            'PERIODO' => $this->_periodo,
            'NUM_DOC' => $this->_numdoc,
            'COD_SECT_UNIQ_EN' => $this->_codigoUnicoSectorista,
            'FECHA_REGISTRO' => ($this->_fechaRegistro)? $this->_fechaRegistro->toDateTimeString() : null,
            'FLG_ULTIMO' => $this->_flgUltimo,
            'REGISTRO_EN' => $this->_ejecutivo,
            'FLG_CLIENTE' => $this->_flgcliente,
            'COD_SECTORISTA' => $this->_codigosectorista,
        ]);


    }

    function cambiarEtapa(){

        $this->_periodo = $this->getPeriodoBE();
        $this->_fechaRegistro = Carbon::now();

        $mLead = new mLead();
        $lead = $mLead->getLeadEjecutivo($this->_periodo,$this->_ejecutivo, $this->_numdoc)->first();

      
        if (!$lead){
            $this->_setMessage('Lead no disponible');
            return false;
        }

        $this->_estrategia = $lead->ESTRATEGIA_ID;

       

        $model = new mLeadEtapa();
        return $model->actualizar($this->setValueToTable());
    }

}