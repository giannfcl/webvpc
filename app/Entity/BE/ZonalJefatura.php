<?php

namespace App\Entity\BE;

use App\Model\BE\Zonal as mZonal;
use App\Model\BE\Jefatura as mJefatura;

class ZonalJefatura extends \App\Entity\Base\Entity {

    static function getZonales($banca=null,$zonal=null){
        //FALTA CACHE
        $model = new mZonal();
        return $model->getZonales($banca,$zonal)->get();
    }

    static function getJefaturas($zonal = null,$banca=null){
        //FALTA CACHE
        $model = new mJefatura();
        return $model->getJefaturas($zonal,$banca)->get();
    }

    static function getBancas(){
        $model = new mZonal();
        return $model->getBancas()->get();
    }
}