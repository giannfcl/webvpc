<?php

namespace App\Entity;

class CitaEstado extends \App\Entity\Base\Entity {


    const AGENDADO = 1;
    const REPROGRAMADO = 2;
    const ACTUALIZADO = 3;
    const REALIZADO = 4;
    const DESCARTADO = 5;
    const REASIGNADO = 7;


    /* ATENCION
        CUIDADO AL CAMBIAR EL DICCIONARIO DE ESTADOS PORQUE HAY ALGUNAS CONSULTAS QUE SON MANUALES
    */
    static function getEstadosParaReprogramacion(){
    	return [
    		self::AGENDADO,self::ACTUALIZADO,self::REASIGNADO
    	];
    }

    static function getEstadosSinGestionar(){
        return [
            self::AGENDADO,self::ACTUALIZADO,self::REPROGRAMADO,self::REASIGNADO
        ];
    }

}