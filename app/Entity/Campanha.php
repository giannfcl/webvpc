<?php

namespace App\Entity;
use App\Model\Campanha as mCampanha;
class Campanha extends \App\Entity\Base\Entity {

    protected $_id;
    protected $_abreviatura;
    protected $_nombre;
    protected $_fechaCreacion;
    protected $_estado;
    protected $_descripcion;

    const ACTIVO = 1;
    const INACTIVO = 0;

    const TIPO_CAMPANHA = 'Campaña';
    const TIPO_CARTERA = 'Cartera';

    const ESTRATEGIA_BE_REFERIDO = 38;
    const CAMPANHA_BPE_CRECER = 100;

    function get($id){
    	return $this;
    }

    function getAll(){

    }

    function registrar(){

    }

    static function formatAtributoCampanha($tipo,$valor){
        if(in_array($tipo,['Monto']))
            if ($valor!='') {
                return 'S/. '. number_format($valor,0,'.',',');
            }else{
                return '-';
            }

        if(in_array($tipo,['Tasa']))
            if ($valor!='') {
                return number_format($valor,2,'.','') . ' %';
            }else{
                return '-';
            }

        if(in_array($tipo,['Plazo']))
            return 'Hasta ' . $valor . ' meses';

        return $valor;
    }

    static function getCampanhasBE(){
        $model= new mCampanha();
        $campanhas=$model->getCampanhasBE()->get();
        return $campanhas;
    }

}