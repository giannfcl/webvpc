<?php

namespace App\Entity;

class Banca extends \App\Entity\Base\Entity {


    const BPE = 'BPE';
    const BE = 'BE';
    const BC = 'BC';
    const RETAIL = 'RETAIL';

    const CODIGO_BANCA = 'TIPO-BANCA';

    static function getAll(){
        return ['BPE' => 'BPE', 'BE' => 'BE','BC' => 'BC'];
    }

    static function getTiposBancas(){
        $model = new \App\Model\Catalogo();
        
        $data = $model->get(self::CODIGO_BANCA);
        
        return $data;
    }

}