<?php

namespace App\Entity;
use App\Model\BE\Lead as mLead;
use App\Model\Mundial as mMundial;
use Jenssegers\Date\Date as Carbon;

class Mundial extends \App\Entity\Base\Entity {

    protected $_codigo;
    protected $_nombre;
    const ITEMS_PER_PAGE = 25;    

    function getAll(){

    }

    function registrar(){

    }

    static function listarPartidosCercanos($registro){
        $model=new mMundial();
        return $model->listarPartidosCercanos($registro)->get();
    }

    static function listarPartidosRango($registro,$rango=NULL){
        $model=new mMundial();
        //dd($model->listarPartidosGrupos($registro,$grupo)->get()->keyBy('FECHA'));
         $partidos=$model->listarPartidosRango($registro,$rango)->get();
         $fpartidos = [];
            foreach ($partidos as $partido){
                $date = Carbon::createFromFormat('Y-m-d', $partido->FECHA);
                $fpartidos[$date->format('l j')][] = $partido;
            }
        return $fpartidos;
    }

    static function obtenerPuestoPuntaje($registro,$area){
        $model=new mMundial();
        $puestos=$model->obtenerPuestoPuntaje()->get();
        //dd($puestos);
        foreach($puestos as $puesto){
            if($puesto->REGISTRO==$registro)
                return $puesto;
        }

    }

    static function obtenerRankingArea($area){
        //dd($area);
        $model=new mMundial();
        return $model->obtenerPuestoPuntaje(null)->take(self::ITEMS_PER_PAGE)->get();
    }

    function insertarResultadoFecha($registro,$partidos){
        $model=new mMundial();
        $hoy = Carbon::now();
        //dd($partidos['partido'],$partidos['local'],$partidos['visitante']);  
        for($i=0;$i<count($partidos['partido']);$i++) {
        
            //Conseguir los datos de partido
            
            $idPartido=$partidos['partido'][$i];
            $golesLocal=$partidos['local'][$i];
            $golesVisitante=$partidos['visitante'][$i];
           
            $resultado='';
            
            if($golesLocal>$golesVisitante)
                $resultado='L';
            else if ($golesLocal<$golesVisitante)
                $resultado='V';
            else
                $resultado='E';
            if($golesLocal==NULL or $golesVisitante==NULL) $resultado=NULL;

            if($resultado!=NULL)
                $data[] =[
                    'REGISTRO'=> $registro,
                    'ID_PARTIDO'=> (int)$idPartido, 
                    'GOLES_LOCAL'=> (int)$golesLocal,
                    'GOLES_VISITANTE'=> (int)$golesVisitante,
                    'RESULTADO'=> $resultado,
                    'FECHA_CARGA'=> $hoy->toDateTimeString()
                ];
            //if($i==2)dd($registro,$partidos['partido'],$data);
        }
        
        if ($model->insertarResultadoPartido($registro,$partidos['partido'],$data)){
            $this->setMessage('Hubo un error en el servidor de base de datos');
            return true;
        }else{
            return false;    
        }
        
    }

}
