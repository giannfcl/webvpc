<?php
namespace App\Entity;

use App\Model\Catalogo as Model;


class Giro extends \App\Entity\Base\Entity {

    const CODIGO = 'GIRO-NEGOCIO';

    static function getAll($sector = null) {
        $model = new Model();
        if (!\Cache::has('giros'.$sector)){
            $data = $model->get(self::CODIGO,$sector);
            \Cache::put('giros'.$sector,$data,99999);
        }else{
            $data = \Cache::get('giros'.$sector);
        }
        return $data;
    }
}