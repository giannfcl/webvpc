<?php
namespace App\Entity;
use App\Model\Notification as mNotification;
use Jenssegers\Date\Date as Carbon;

class Notification extends \App\Entity\Base\Entity {

    protected $_id;
    protected $_idTipo;
    protected $_registro;
    protected $_contenido;
    protected $_fechaNotificacion;
    protected $_flgLeido;
    protected $_fechaLeido;
    protected $_valor1;
    protected $_valor2;
    protected $_valor3;
    protected $_valor4;
    protected $_valor5;
    protected $_valor6;
    protected $_valor7;
    protected $_valor8;
    protected $_valor9;
    protected $_valor10;
    protected $_valor11;
    protected $_valor12;
    protected $_valor13;
    protected $_valor14;
    protected $_valor15;
    protected $_valor16;
    protected $_valor17;
    protected $_valor18;
    protected $_valor19;
    protected $_valor20;
    protected $_url;

    function setValueToTable() {
       $data = [
           'ID_TIPO_NOTIFICACION' => $this->_idTipo,
           'REGISTRO_EN' => $this->_registro,
           'CONTENIDO' => $this->_contenido,
           'FECHA_NOTIFICACION' => ($this->_fechaNotificacion)? $this->_fechaNotificacion->format('Y-m-d H:i:s'):null,
           'FLG_LEIDO' => $this->_flgLeido,
           'FECHA_LEIDO' => $this->_fechaLeido ? $this->_fechaLeido->format('Y-m-d H:i:s'):null,
           'VALOR_1' => $this->_valor1,
           'VALOR_2' => $this->_valor2,
           'VALOR_3' => $this->_valor3,
           'VALOR_4' => $this->_valor4,
           'VALOR_5' => $this->_valor5,
           'VALOR_6' => $this->_valor6,
           'VALOR_7' => $this->_valor7,
           'VALOR_8' => $this->_valor8,
           'VALOR_9' => $this->_valor9,
           'VALOR_10' => $this->_valor10,
           'VALOR_11' => $this->_valor11,
           'VALOR_12' => $this->_valor12,
           'VALOR_13' => $this->_valor13,
           'VALOR_14' => $this->_valor14,
           'VALOR_15' => $this->_valor15,
           'VALOR_16' => $this->_valor16,
           'VALOR_17' => $this->_valor17,
           'VALOR_18' => $this->_valor18,
           'VALOR_19' => $this->_valor19,
           'VALOR_20' => $this->_valor20,
           'URL' => $this->_url,
       ];
       return $this->cleanArray($data);
    }

    function setProperties($data) {
        $this->setValues([
            '_idTipo' => $data->ID_TIPO_NOTIFICACION,
            '_registro' => $data->REGISTRO_EN,
            '_contenido' => $data->CONTENIDO,
            '_fechaNotificacion' => $data->FECHA_NOTIFICACION,
            '_flgLeido' => $data->FLG_LEIDO,
            '_fechaLeido' => $data->FECHA_LEIDO,
            '_valor1' => $data->VALOR_1,
            '_valor2' => $data->VALOR_2,
            '_valor3' => $data->VALOR_3,
            '_valor4' => $data->VALOR_4,
            '_valor5' => $data->VALOR_5,
            '_valor6' => $data->VALOR_6,
            '_valor7' => $data->VALOR_7,
            '_valor8' => $data->VALOR_8,
            '_valor9' => $data->VALOR_9,
            '_valor10' => $data->VALOR_10,
            '_valor11' => $data->VALOR_11,
            '_valor12' => $data->VALOR_12,
            '_url' => $data->URL,
        ]);
    }
    //Primer Filtro
    static function getCatalogoNotificaciones(){
      $model = new mNotification();
      return $model->getCatalogoNotificaciones();
    }
    static function getNotifications($registro)
    {
        $res = mNotification::getNotifications($registro);
        return count($res)>0?$res:[];
    }
    static function getNotificationsTOT($registro,$filtro=null)
    {
        $res = mNotification::getNotificationsTOT($registro,$filtro);
        return count($res)>0?$res:[];
    }
    static function updateVisto($registro)
    {
        return mNotification::updateVisto($registro);
    }
    static function updateVistoURL($registro, $id)
    {
        return mNotification::updateVistoURL($registro, $id);
    }
    function agregarnotificacion($data)
    {
      $model = new mNotification();
      return $model->agregarnotificacion($data);
    }
}
