<?php

namespace App\Entity;

use App\Model\Lead as modelLead;
use \Illuminate\Pagination\LengthAwarePaginator as Paginator;
use App\Entity\Canal as Canal;

class Leads extends \App\Entity\Base\Entity {

    const ITEMS_PER_PAGE = 25;
    

    static function getLead($nombre='',$documento=''){
        $model = new modelLead();
        $lead = $model->consulta($documento)->first();
        return $lead;
    }

    static function getLeadsEjecutivoNegocio($en,$busqueda,$orden){

        $model = new modelLead();
        $query = $model->getLeadEjecutivo(
            self::sgetPeriodo(),
            $en,
            null, //no es necesario especificar el lead porque traes todos
            Canal::getCanalesEjecutivo(),
            array_filter($busqueda),
            $orden);

        // Paginación
        $totalCount = $query->count();
        $results = $query
                ->take(self::ITEMS_PER_PAGE)
                ->skip(self::ITEMS_PER_PAGE * ($busqueda['page'] - 1))
                ->get();

        $paginator = new Paginator($results, $totalCount, self::ITEMS_PER_PAGE, $busqueda['page']);

        return $paginator;
    }

    static function getLeadsEjecutivoResumido($registro,$camp,$nombre){
        $model = new modelLead();
        return $model->getLeadsEjecutivoResumido(self::sgetPeriodo(),$registro,$camp,$nombre)
                                    ->get();
    }

    public function getLeadAsignacion($lead,$ejecutivo = null){
        $model = new modelLead();
        $result = $model->getLeadAsignacion($lead,self::sgetPeriodo())->first();
        if ($result){
            if ($result->EN_REGISTRO and $result->EN_REGISTRO != $ejecutivo){
                $this->setMessage('El lead seleccionado ya tiene un ejecutivo asignado');
                return false;
            }else{
                return $result;
            }
        }else{
            $this->setMessage('No existen datos para el número de documento seleccionado');
            return false;
        }
    }

    public function getLeadReasignacion($lead,$ejecutivo){
        $model = new modelLead();
        $result = $model->getLeadAsignacion($lead,self::sgetPeriodo())->first();
        
        if ($result){
            
            if (is_null($result->EN_REGISTRO)){
                $this->setMessage('El lead no está disponible para reasignación (sin ejecutivo)');
                return false;
            }
            
            if ($result->EN_REGISTRO == $ejecutivo){
                $this->setMessage('El lead ya está asignado al ejecutivo');
                return false;
            }else{
                return $result;
            }
        }else{
            $this->setMessage('No existen datos para el número de documento seleccionado');
            return false;
        }
    }

    static function imprimirLeadsEjecutivoNegocio($en,$busqueda,$orden){
        $model = new modelLead();
        return $model->getLeadEjecutivoFull(
            self::sgetPeriodo(),
            $en, 
            null, //no es necesario especificar el lead porque traes todos
            Canal::getCanalesEjecutivo(),
            array_filter($busqueda),
            $orden)->get();
    }

    static function getLeadEjecutivoNegocio($en,$lead){
        $model = new modelLead();
        return $model->getLeadEjecutivo(
            self::sgetPeriodo(),
            $en, 
            $lead,
            Canal::getCanalesEjecutivo())->first();
    }

    static function getDatosBasicosCliente($documento){
        $model = new modelLead();
        return $model->getDatosBasicosCliente($documento)->first();
    }

    static function getDireccion($documento=null)
    {
        $model = new modelLead();
        return $model ->getDireccion($documento);
    }

    
    static function getLeadCall($lead){
        $model = new modelLead();
        return $model->getLeadEjecutivo(
            self::sgetPeriodo(),
            null,
            $lead,
            Canal::getCanalesCall())
            ->first();
    }

    static function getLeadAsistenteComercial($ac,$lead,$en = null){
    	$model = new modelLead();
    	return $model->getLeadAsistente(
                self::sgetPeriodo(),
                $ac,
                $en,
                $lead,
                Canal::getCanalesAsistente())
            ->first();
    }
    
    static function getProductosByRuc($numdoc){
        $model = new modelLead();
    	return $model->getProductosByRuc($numdoc)
            ->get();
    }

    static function getLeadsAsistenteComercial($ac,$busqueda){
        $model = new modelLead();
        $query = $model->getLeadAsistente(
            self::sgetPeriodo(),
            $ac, 
            $busqueda['ejecutivo'], //no es necesario especificar el ejecutivo por que traes todos
            $busqueda['lead'], //no es necesario especificar el lead porque traes todos
            Canal::getCanalesAsistente()
            );

        // Paginación
        $totalCount = $query->count();
        $results = $query
                ->take(self::ITEMS_PER_PAGE)
                ->skip(self::ITEMS_PER_PAGE * ($busqueda['page'] - 1))
                ->get();

        $paginator = new Paginator($results, $totalCount, self::ITEMS_PER_PAGE, $busqueda['page']);

        return $paginator;
    }

    static function getLeadsCampanhasAsistenteComercial($ac = null,$lead = null){
        $model = new modelLead();
        return $model->getLeadsCampanhas(self::sgetPeriodo(),$lead)->get();
    }

    static function formatTelefonos($lead){
        $telefonos = [];
        if ($lead->TELEFONO1)
            $telefonos[] = $lead->TELEFONO1;
        if ($lead->TELEFONO2)
            $telefonos[] = $lead->TELEFONO2;
        if ($lead->TELEFONO3)
            $telefonos[] = $lead->TELEFONO3;
        if ($lead->TELEFONO4)
            $telefonos[] = $lead->TELEFONO4;
        return $telefonos;
    }

    function actualizarLead($data,$registro)
    {
        $numdoc=isset($data['numruc'])?$data['numruc']:null;
        // dd(strlen($data['departamento'])*strlen($data['provincia'])*strlen($data['distrito'])>0);
        $actualizar=[
            // 'DEPARTAMENTO'=>isset($data['departamento'])?$data['departamento']:null,
            // 'PROVINCIA'=>isset($data['provincia'])?$data['provincia']:null,
            // 'DISTRITO'=>isset($data['distrito'])?$data['distrito']:null,
            'DIR_UBIGEO'=>(strlen($data['departamento'])*strlen($data['provincia'])*strlen($data['distrito'])>0)?($data['departamento'].$data['provincia'].$data['distrito']):null,
            'EMAIL'=>isset($data['email'])?$data['email']:null,
            'NROEMPLEADOS'=>isset($data['empleados'])?$data['empleados']:null,
            'TIENDA'=>isset($data['tienda'])?$data['tienda']:null,
            'DIR_TIPO_VIA'=>isset($data['tipoVia'])?$data['tipoVia']:null,
            'DIR_DESCRIPCION_VIA'=>isset($data['direcVia'])?$data['direcVia']:null,
            'DIR_NRO'=>isset($data['direcNro'])?$data['direcNro']:null,
            'DIR_LOTE'=>isset($data['direcLote'])?$data['direcLote']:null,
            'DIR_URB_ZONA'=>isset($data['direcUrb'])?$data['direcUrb']:null,
            'DIR_MANZANA'=>isset($data['direcManz'])?$data['direcManz']:null,
            'DIR_INTERIOR'=>isset($data['direcInt'])?$data['direcInt']:null,
            'RRLL_DOCUMENTO_1'=>isset($data['rucrrll'])?$data['rucrrll']:null,
            'REPRESENTANTE_LEGAL'=>isset($data['rrll'])?$data['rrll']:null,
            'RRLL_APELLIDOS'=>isset($data['rrllApellidos'])?$data['rrllApellidos']:null,
            'RRLL_ESTADO_CIVIL'=>isset($data['rrllEstadoCivil'])?$data['rrllEstadoCivil']:null,
            'RRLL_DOCUMENTO_2'=>isset($data['rucrrll2'])?$data['rucrrll2']:null,
            'RRLL_NOMBRES_2'=>isset($data['rrll2'])?$data['rrll2']:null,
            'RRLL_APELLIDOS_2'=>isset($data['rrllApellidos2'])?$data['rrllApellidos2']:null,
            'RRLL_ESTADO_CIVIL_2'=>isset($data['rrllEstadoCivil2'])?$data['rrllEstadoCivil2']:null,
            'VENTAS_ANUALES'=>isset($data['ventas'])?$data['ventas']:null,
            'EXPORTACIONES_ANUALES'=>isset($data['exportaciones'])?$data['exportaciones']:null,
            'RRLL_CARGO_1'=>isset($data['rrllCargo'])?$data['rrllCargo']:null,
            'RRLL_CARGO_2'=>isset($data['rrllCargo2'])?$data['rrllCargo2']:null,
            'TIPO_CUENTA'=>isset($data['tipocuenta'])?$data['tipocuenta']:null,
            'NUM_CUENTA'=>isset($data['numcuenta'])?$data['numcuenta']:null,
        ];
        $nuevosavales = [];
        if (isset($data['aval'])) {
            foreach ($data['aval'] as $key => $aval) {
                if ( isset($aval) 
                    && (!empty($aval['num_doc_aval']) || !empty($aval['nombre_aval']) ) 
                ) {
                    $nuevosavales[$key]['NUM_DOC']=$data['numruc'];
                    $nuevosavales[$key]['NUM_DOC_AVAL']=$aval['num_doc_aval'];
                    $nuevosavales[$key]['NOMBRE']=$aval['nombre_aval'];
                    $nuevosavales[$key]['DIRECCION']=$aval['direccion_aval'];
                    $nuevosavales[$key]['ESTADO_CIVIL']=$aval['estado_civil_aval'];
                }
            }
        }
        // dd($data['aval'],1,$nuevosavales);
        // dd($numdoc,$data,$actualizar);
        if ($numdoc) {
            $model = new modelLead();
            return $model->updateLead($numdoc,$actualizar,$nuevosavales);
        }else{
            $this->setMessage('Hubo en error al conectarse con la Base de Datos. Contáctate con soporte.');
            return false;
        }
    }

    static function getAvalByLead($documento){
        $model = new modelLead();
        $avales = $model->getAvalByRuc($documento)->get();
        //$avales = [];
        $dataavales = [];
        for ($i=0; $i <3 ; $i++) {
            //$dataavales[$i]['NUM_DOC']=$documento;
            $dataavales[$i]['NUM_DOC_AVAL']=isset($avales[$i])?$avales[$i]->NUM_DOC_AVAL:'';
            $dataavales[$i]['DIRECCION']=isset($avales[$i])?$avales[$i]->DIRECCION:'';
            $dataavales[$i]['ESTADO_CIVIL']=isset($avales[$i])?$avales[$i]->ESTADO_CIVIL:'';
            $dataavales[$i]['NOMBRE']=isset($avales[$i])?$avales[$i]->NOMBRE:'';
        }
        //dd($dataavales,$avales);
        return $dataavales;
    }

}