<?php

namespace App\Entity;

use App\Model\Reciprocidad as mReciprocidad;

class Reciprocidad extends \App\Entity\Base\Entity
{
    static function buscarNumDoc($cuBuscar,$registro)
    {
        $datos = mReciprocidad::buscarNumDoc($cuBuscar,$registro)->get();
        return (isset($datos) ? $datos : null);
    }

    static function Resultados_simular($datos) 
    {
        $datos= mReciprocidad::Resultados_simular($datos);
        return $datos;
    }

    static function getUsuario2($datos = null)
    {
        $usuario = mReciprocidad::getUsuario2($datos['banca'], $datos['zonal'], $datos['jefe'], $datos['ejecutivo']);
        return $usuario;
    }

    static function Registros_finales($datos=null)
    {
        $modelo = new mReciprocidad;
        $datos=mReciprocidad::Registros_finales($datos);
        return $datos;
    }
    function getTiposCompromiso()
    {
    	$modelo = new mReciprocidad;
    	return $modelo->getTiposCompromiso();
    }

    static function Razones_sociales($registro)
    {
        $datos=mReciprocidad::Razones_sociales($registro);
        return  $datos;
    }

    function getTiposCompromiso2()
    {
    	$modelo = new mReciprocidad;
    	return $modelo->getTiposCompromiso2();
    }


    function Tabla_transacciones($datos=null,$registro)
    {
    	return mReciprocidad::Tabla_transacciones($datos!=null ? $datos : null,$registro);
    }

    function Tabla_transacciones2()
    {
    	return mReciprocidad::Tabla_transacciones2();
    }
    function Tabla_transacciones3()
    {
    	return mReciprocidad::Tabla_transacciones3();
    }


    function guardarDatos($datos,$registro)
    {
    	return mReciprocidad::guardarDatos($datos,$registro);
    }
        function guardarDatos2($datos,$registro)
    {
        return mReciprocidad::guardarDatos2($datos,$registro);
    }
    function rating($registro)
    {
        return mReciprocidad::rating($registro);
    }
}