<?php

namespace App\Entity;

use App\Model\Usuario as mUsuario;
use App\Model\Infinity\Alertacartera as mAlertacartera;
use App\Entity\BE\Lead as Lead;
use App\Model\LeadCampanha as mLeadCampanha;
use \Illuminate\Pagination\LengthAwarePaginator as Paginator;
use App\Entity\Canal as Canal;
use DB;

class Usuario extends \App\Entity\Base\Entity
{
    const ROL_ANALISTA_VPC = 100;
    const ROL_EJECUTIVO_NEGOCIO = 1;
    const ROL_ASISTENTE_COMERCIAL = 2;
    const ROL_RIESGO_BPE = 3;
    const ROL_SOPORTE = 4;
    const ROL_CALL = 5;
    const ROL_GERENTE_TIENDA = 6;
    const ROL_GERENTE_CENTRO = 7;
    const ROL_GERENTE_ZONA = 8;
    const ROL_JEFE_CALL = 9;
    const ROL_ADMINISTRADOR = 10;
    const ROL_EJECUTIVO_FIES = 11;
    const ROL_JEFE_FIES = 12;

    const ROL_RIESGOS_ME_ZONAL = 13;
    const ROL_RIESGOS_ME_JEFATURA = 14;
    const ROL_SUBGERENTE_BPE = 15;

    const ROL_EJECUTIVO_LA = 19;
    const ROL_EJECUTIVO_FARMER_BE = 20;
    const ROL_EJECUTIVO_HUNTER_BE = 21;
    const ROL_ANALISTA_NEGOCIO_JEFATURA_BE = 22;
    const ROL_ANALISTA_NEGOCIO_ZONAL_BE = 23;
    const ROL_JEFATURA_BE = 24;
    const ROL_GERENCIA_ZONAL_BE = 25;
    const ROL_GERENTE_DIVISION_BE = 26;
    const ROL_ANALISTA_EXTERNO_JEFATURA_BE = 27;
    const ROL_ANALISTA_EXTERNO_ZONAL_BE = 28;
    const ROL_ANALISTA_EXTERNO_DIVISION_BE = 29;
    const ROL_GERENTE_BANCA = 30;
    const ROL_EJECUTIVO_PRODUCTO = 31;
    const ROL_ENCUESTADOR = 32;
    const ROL_GERENTE_BEP = 41;

    //Roles Infinity
    const ROL_ANALISTA_RIESGOS = 33;
    const ROL_SUBGERENTE_RIESGOS = 34;
    const ROL_JEFE_RIESGOS = 35;
    const ROL_JEFE_RIESGOS_BPE = 43;
    const ROL_JEFE_ESCOM = 36;
    const ROL_EN_ESCOM = 37;
    const ROL_GYS = 38;
    const ROL_ANALISTA_GYS = 39;
    const ROL_SUBGERENTE_GYS = 40;
    const ROL_GERENTE_RIESGOS = 42;
    const ROL_CONTROL_CREDITOS = 120;
    const ROL_ANALISTA_RIESGOS_ADMISION = 49;
    const ROL_INTELIGENCIA_RIESGOS = 50;
    const ROL_EJECUTIVO_SEGUIMIENTO_CARTERA = 52;
    const ROL_EJECUTIVO_INNOVACION_CARTERA = 53;

    const ROL_ANALISTAS_WHATSAPP = 54;

    const ROL_ROBOT_TD = 999;
    const ROL_MUNDIAL = 666;

    const ROL_ASESOR_M = 51;
    const ITEMS_PER_PAGE = 15;

    const ROL_TELEVENTAS = 60;
    const ROL_SOLUCION_EMPRESA = 61;
    const ROL_SUPERVISOR_TELEVENTAS = 62;
    const ROL_ANALISTA_RIESGOS_COBRANZA = 63;
    const ROL_CANALES_DISTRIBUCION = 64;

    const ROL_MERCADO_CAPITALES = 70;

    const ROL_BACKOFFICE = 80;

    const ROL_GESTOR_CUENTA_SUELDO = 90;
    const ROL_ANALISTA_PRODUCTO = 91;
    const ROL_GESTOR_COBRANZA_BPE = 92;
    const VW_BLACKLISTBPE='VISTA_BLACKLIST_BPE';

    protected $_registro;
    // protected $_rol;
    protected $_nombre;
    // protected $_zona;
    // protected $_centro;
    // protected $_tienda;
    // protected $_banca;
    // protected $_idgc;
    protected $_correo;
    // protected $_flgAsistenteComercial;
    // protected $_segmento;
    // protected $_idla;

    public function getNombreByRegistro($registro){
        $usuario = new mUsuario();
        return $usuario->getNombreByRegistro($registro);
    }

    public function setFromAuth($user)
    {

        $this->setValue('_registro', $user->REGISTRO);
        // $this->setValue('_rol', $user->ROL);
        $this->setValue('_nombre', $user->NOMBRE);
        // $this->setValue('_zona', $user->ID_ZONA);
        // $this->setValue('_centro', $user->ID_CENTRO);
        // $this->setValue('_tienda', $user->ID_TIENDA);
        // $this->setValue('_banca', $user->BANCA);
        // $this->setValue('_idgc', $user->ID_GC);
        // $this->setValue('_idla', $user->ID_LA);
        $this->setValue('_correo', $user->CORREO);
        // $this->setValue('_flgAsistenteComercial', $user->FLAG_TIENE_ASISTENTE_COMERCIAL);
        // $this->setValue('_segmento', isset($user->SEGMENTO) ? $user->SEGMENTO : null);
    }
    public function getCargo($registro){
        $model = new mUsuario();
        return $model->getCargo($registro);
    }
    public function getUsuarioByRegistro()
    {
        $model = new mUsuario();
        return $model->getEjecutivoByRegistro($this->_registro);
    }

    static function getEjecutivoByRegistro($registro)
    {
        $model = new mUsuario();
        return $model->getEjecutivoByRegistro($registro);
    }

    static function getEjecutivosProductoByZonal($zonal)
    {

        $model = new mUsuario();
        return $model->getEjecutivosProductoByZonal($zonal);
    }

    static function UpdateLogeo($registro)
    {

        $model = new mUsuario();
        return $model->UpdateLogeo($registro);
    }

    static function getEjecutivosByAsistenteComercial($ac)
    {
        // AGREGAR CACHE

        $model = new mUsuario();
        return $model->getEjecutivoByAsistenteComercial($ac)->get();
    }

    static function getGerentes()
    {
        return [self::ROL_GERENTE_CENTRO, self::ROL_GERENTE_TIENDA,
            self::ROL_GERENTE_ZONA,
            self::ROL_ADMINISTRADOR];
    }

    static function getResumenEjecutivosGerente($rol, $busqueda, $order, $tienda = null, $centro = null, $zona = null)
    {
        $model = new mLeadCampanha();
        $query = '';
        $totalCount = 0;

        switch ($rol) {
            case Usuario::ROL_GERENTE_TIENDA:
                $query = $model->getResumenByEjecutivoV2(
                    self::sgetPeriodo(),
                    $busqueda['ejecutivo'],
                    Canal::getCanalesEjecutivo(),
                    null,
                    null,
                    null,
                    $tienda,
                    $order
                );
                $totalCount = $model->countResumenByEjecutivoV2(self::sgetPeriodo(), $busqueda['ejecutivo'], Canal::getCanalesEjecutivo(), null, null, null, $tienda);
                break;
            case Usuario::ROL_GERENTE_CENTRO:
                $query = $model->getResumenByEjecutivoV2(
                    self::sgetPeriodo(),
                    $busqueda['ejecutivo'],
                    Canal::getCanalesEjecutivo(),
                    null,
                    null,
                    $centro,
                    $busqueda['tienda'],
                    $order
                );
                $totalCount = $model->countResumenByEjecutivoV2(self::sgetPeriodo(), $busqueda['ejecutivo'], Canal::getCanalesEjecutivo(), null, null, $centro, $busqueda['tienda']);
                break;
            case Usuario::ROL_GERENTE_ZONA:
                $query = $model->getResumenByEjecutivoV2(
                    self::sgetPeriodo(),
                    $busqueda['ejecutivo'],
                    Canal::getCanalesEjecutivo(),
                    null,
                    $zona,
                    $busqueda['centro'],
                    $busqueda['tienda'],
                    $order
                );
                $totalCount = $model->countResumenByEjecutivoV2(self::sgetPeriodo(), $busqueda['ejecutivo'], Canal::getCanalesEjecutivo(), null, $zona, $busqueda['centro'], $busqueda['tienda']);
                break;
            case Usuario::ROL_ADMINISTRADOR:
                $query = $model->getResumenByEjecutivoV2(
                    self::sgetPeriodo(),
                    $busqueda['ejecutivo'],
                    Canal::getCanalesEjecutivo(),
                    null,
                    $busqueda['zonal'],
                    $busqueda['centro'],
                    $busqueda['tienda'],
                    $order
                );
                $totalCount = $model->countResumenByEjecutivoV2(self::sgetPeriodo(), $busqueda['ejecutivo'], Canal::getCanalesEjecutivo(), null, $busqueda['zonal'], $busqueda['centro'], $busqueda['tienda']);
                break;
        }


        $results = $query
            ->take(self::ITEMS_PER_PAGE)
            ->skip(self::ITEMS_PER_PAGE * ($busqueda['page'] - 1))
            ->get();



        $paginator = new Paginator($results, $totalCount, self::ITEMS_PER_PAGE, $busqueda['page']);

        return $paginator;
    }

    static function redirectRol($registro=null)
    {
        return 'bienvenida';
        // dd($registro);
        // $url1 = 'be.miprospecto.lista.index';
        // $url2 = 'be.resumen.index';
        // $url3 = 'infinity.alertascartera';
        // $url4 = 'infinity.me.clientes.control';
        // $url6 = 'bienvenida';
        // // $url7 = 'gestion.cartera';
        // if($banca=='BC') return $url6;

        // switch ($rol) {
        //     case self::ROL_ASESOR_M:
        //     case self::ROL_ANALISTA_VPC:
        //     case self::ROL_EJECUTIVO_PRODUCTO:
        //     case self::ROL_RIESGOS_ME_JEFATURA:
        //     case self::ROL_ASISTENTE_COMERCIAL:
        //     case self::ROL_CALL:
        //     case self::ROL_GERENTE_CENTRO:
        //     case self::ROL_GERENTE_ZONA:
        //     case self::ROL_ADMINISTRADOR:
        //     case self::ROL_SOPORTE:
        //     case self::ROL_JEFE_CALL:
        //     case self::ROL_RIESGOS_ME_ZONAL:
        //     case self::ROL_INTELIGENCIA_RIESGOS:
        //     case self::ROL_ANALISTA_RIESGOS:
        //     case self::ROL_EJECUTIVO_INNOVACION_CARTERA:
        //     case self::ROL_EJECUTIVO_SEGUIMIENTO_CARTERA:
        //     case self::ROL_GERENTE_RIESGOS:
        //     case self::ROL_ANALISTA_RIESGOS_ADMISION:
        //     case self::ROL_SUBGERENTE_RIESGOS:
        //     case self::ROL_JEFE_RIESGOS:
        //     case self::ROL_JEFE_ESCOM:
        //     case self::ROL_EN_ESCOM:
        //     case self::ROL_GYS:
        //     case self::ROL_ANALISTA_GYS:
        //     case self::ROL_SUBGERENTE_GYS:
        //     case self::ROL_ANALISTA_NEGOCIO_JEFATURA_BE:
        //     case self::ROL_ANALISTA_NEGOCIO_ZONAL_BE:
        //     case self::ROL_ANALISTA_EXTERNO_JEFATURA_BE:
        //     case self::ROL_ANALISTA_EXTERNO_ZONAL_BE:
        //     case self::ROL_ANALISTA_EXTERNO_DIVISION_BE:
        //     case self::ROL_JEFATURA_BE:
        //     case self::ROL_GERENCIA_ZONAL_BE:
        //     case self::ROL_GERENTE_BANCA:
        //     case self::ROL_GERENTE_DIVISION_BE:
        //     case self::ROL_ROBOT_TD:
        //     case self::ROL_TELEVENTAS:
        //     case self::ROL_JEFE_RIESGOS_BPE:
        //     case self::ROL_SUPERVISOR_TELEVENTAS:
        //     case self::ROL_CANALES_DISTRIBUCION:
        //     case self::ROL_SOLUCION_EMPRESA:
        //     case self::ROL_EJECUTIVO_NEGOCIO:
        //     case self::ROL_RIESGO_BPE:
        //     case self::ROL_ANALISTA_RIESGOS_COBRANZA:
        //     case self::ROL_GESTOR_COBRANZA_BPE:
        //     case self::ROL_EJECUTIVO_FARMER_BE:
        //     case self::ROL_EJECUTIVO_HUNTER_BE:
        //     case self::ROL_SUBGERENTE_BPE:
        //     case self::ROL_BACKOFFICE:
        //     case self::ROL_GESTOR_CUENTA_SUELDO:
        //     case self::ROL_ANALISTAS_WHATSAPP:
        //     case self::ROL_ENCUESTADOR:
        //     case self::ROL_GERENTE_TIENDA:
        //     case self::ROL_EJECUTIVO_FIES:
        //     case self::ROL_JEFE_FIES:
        //     case self::ROL_MERCADO_CAPITALES:
        //         return $url6;
        //     default:
        //         return $url6;
        // }
    }

    /*     * ********************************** */
    /*     * ** GRUPOS PARA BANCA EMPRESA ***** */
    /*     * ********************************** */

    static function getEquipoBE()
    {
        return [
            self::ROL_RIESGOS_ME_ZONAL,
            self::ROL_EJECUTIVO_FARMER_BE,
            self::ROL_EJECUTIVO_HUNTER_BE,
            self::ROL_ANALISTA_NEGOCIO_JEFATURA_BE,
            self::ROL_ANALISTA_NEGOCIO_ZONAL_BE,
            self::ROL_ANALISTA_EXTERNO_JEFATURA_BE,
            self::ROL_ANALISTA_EXTERNO_ZONAL_BE,
            self::ROL_JEFATURA_BE,
            self::ROL_GERENCIA_ZONAL_BE,
            self::ROL_GYS,
            self::ROL_ANALISTA_GYS,
            self::ROL_SUBGERENTE_GYS,
            self::ROL_GERENTE_DIVISION_BE,
            self::ROL_GERENTE_BANCA,
            self::ROL_GERENTE_RIESGOS,
            //self::ROL_EJECUTIVO_PRODUCTO,
            self::ROL_ANALISTA_EXTERNO_DIVISION_BE,
            self::ROL_EJECUTIVO_SEGUIMIENTO_CARTERA,
        ];
    }

    static function getEquipoBE_y_GYS()
    {
        return [
            self::ROL_RIESGOS_ME_ZONAL,
            self::ROL_EJECUTIVO_FARMER_BE,
            self::ROL_EJECUTIVO_HUNTER_BE,
            self::ROL_ANALISTA_NEGOCIO_JEFATURA_BE,
            self::ROL_ANALISTA_NEGOCIO_ZONAL_BE,
            self::ROL_ANALISTA_EXTERNO_JEFATURA_BE,
            self::ROL_ANALISTA_EXTERNO_ZONAL_BE,
            self::ROL_JEFATURA_BE,
            self::ROL_GERENCIA_ZONAL_BE,
            self::ROL_GERENTE_DIVISION_BE,
            self::ROL_GERENTE_BANCA,
            self::ROL_GYS,
            self::ROL_ANALISTA_GYS,
            self::ROL_SUBGERENTE_GYS,
            self::ROL_GERENTE_RIESGOS,
            //self::ROL_EJECUTIVO_PRODUCTO,
            self::ROL_ANALISTA_EXTERNO_DIVISION_BE,
            self::ROL_EJECUTIVO_INNOVACION_CARTERA,
            self::ROL_EJECUTIVO_SEGUIMIENTO_CARTERA,
        ];
    }

    static function getEquipoInfinity()
    {
        return [
            self::ROL_ANALISTA_RIESGOS,
            self::ROL_ANALISTA_RIESGOS_ADMISION,
            self::ROL_RIESGOS_ME_ZONAL,
            self::ROL_EJECUTIVO_INNOVACION_CARTERA,
            self::ROL_ANALISTA_EXTERNO_DIVISION_BE,
            self::ROL_EJECUTIVO_PRODUCTO,
            self::ROL_ANALISTA_VPC,
        ];
    }


    static function getFarmersHunters($banca = null, $zonal = null, $jefatura = null)
    {
        $model = new mUsuario();
        return $model->getEjecutivoByRol(self::getEjecutivosBE(), $banca, $zonal, $jefatura)->get();
    }

    static function getEjecutivosBE()
    {
        return [self::ROL_EJECUTIVO_FARMER_BE, self::ROL_EJECUTIVO_HUNTER_BE];
    }


    static function getEjecutivosProductoBE()
    {
        return [self::ROL_EJECUTIVO_PRODUCTO];
    }

    static function getAnalistasExternosBE()
    {
        return [self::ROL_ANALISTA_EXTERNO_JEFATURA_BE, self::ROL_ANALISTA_EXTERNO_ZONAL_BE, self::ROL_ANALISTA_EXTERNO_DIVISION_BE];
    }

    static function getAnalistasInternosBE()
    {
        return [self::ROL_ANALISTA_NEGOCIO_JEFATURA_BE, self::ROL_ANALISTA_NEGOCIO_ZONAL_BE];
    }

    static function getAnalistasEjecutivosBE($externo = false)
    {
        return array_merge([
            self::ROL_EJECUTIVO_FARMER_BE,
            self::ROL_EJECUTIVO_HUNTER_BE,
            self::ROL_ANALISTA_NEGOCIO_ZONAL_BE,
            self::ROL_ANALISTA_EXTERNO_ZONAL_BE,
            self::ROL_ANALISTA_EXTERNO_DIVISION_BE,
            self::ROL_EJECUTIVO_PRODUCTO,
            self::ROL_EJECUTIVO_SEGUIMIENTO_CARTERA,
            self::ROL_EJECUTIVO_INNOVACION_CARTERA,
            self::ROL_JEFATURA_BE,
            self::ROL_GERENCIA_ZONAL_BE,
            self::ROL_GYS,
            self::ROL_ANALISTA_GYS,
            self::ROL_SUBGERENTE_GYS,
            self::ROL_EN_ESCOM,
            self::ROL_JEFE_ESCOM,
            self::ROL_SUBGERENTE_RIESGOS,
            self::ROL_JEFE_RIESGOS,
            self::ROL_JEFE_RIESGOS_BPE,
            self::ROL_ANALISTA_RIESGOS_ADMISION,
            self::ROL_JEFE_FIES,
            self::ROL_ANALISTA_VPC,
            self::ROL_GERENTE_DIVISION_BE,
            self::ROL_GERENTE_BANCA,
            self::ROL_GERENTE_RIESGOS,
        ], $externo ? self::getAnalistasExternosBE() : []);
    }

    static function getJefesGerentesBE()
    {
        return [
            self::ROL_JEFATURA_BE,
            self::ROL_GERENCIA_ZONAL_BE,
            self::ROL_GERENTE_BANCA,
            self::ROL_GERENTE_DIVISION_BE,
            self::ROL_EJECUTIVO_INNOVACION_CARTERA,
            self::ROL_EJECUTIVO_SEGUIMIENTO_CARTERA,
        ];
    }

    static function getDivisionBE()
    {
        return [self::ROL_GERENTE_DIVISION_BE, self::ROL_ANALISTA_EXTERNO_DIVISION_BE, self::ROL_GERENTE_BANCA, self::ROL_ROBOT_TD, self::ROL_RIESGOS_ME_ZONAL, self::ROL_ANALISTAS_WHATSAPP];
    }

    static function getBanca()
    {
        return [self::ROL_GERENTE_BANCA];
    }
    static function getZonalesBE()
    {
        return [self::ROL_ANALISTA_EXTERNO_ZONAL_BE,
            self::ROL_ANALISTA_NEGOCIO_ZONAL_BE,
            self::ROL_GERENCIA_ZONAL_BE,
            self::ROL_EJECUTIVO_INNOVACION_CARTERA,
            self::ROL_EJECUTIVO_SEGUIMIENTO_CARTERA,
        ];
    }

    static function getJefaturasBE()
    {
        return [self::ROL_ANALISTA_EXTERNO_JEFATURA_BE, self::ROL_ANALISTA_NEGOCIO_JEFATURA_BE, self::ROL_JEFATURA_BE];
    }

    static function getEquipoRiesgosME()
    {
        return [self::ROL_RIESGOS_ME_ZONAL, self::ROL_RIESGOS_ME_JEFATURA];
    }

    public function changePassword($registro, $apassword, $npassword)
    {

        $model = new mUsuario();
        if (!($model->verifyPassword($registro, $apassword))) {
            $this->setMessage('La contraseña ingresada no coincide con la original');
            return false;
        } else {
            $model->updateNewPassword($registro, $npassword);
            $this->setMessage('La contraseña fue cambiada exitosamente');
            return true;
        }
    }
    function updateMasive()
    {
        $model = new mUsuario();
        return $model->updateMasive();
    }


    static function getUsuariosComunicacion()
    {

        $model = new mUsuario();
        $comunicacion = $model->getUsuariosComunicacion()->get();
        $usuarios = [];
        foreach ($comunicacion as $user) {
            $usuarios[] = $user->REGISTRO;
        }
        //dd($usuarios);
        return $usuarios;
    }



    static function getEncuestadores()
    {

        $model = new mUsuario();
        $encuestadores = $model->getEncuestadores()->get();
        $usuarios = [];
        foreach ($encuestadores as $user) {
            $usuarios[] = $user->REGISTRO_ENCUESTADOR;
        }
        //dd($usuarios);
        return $usuarios;
    }


    static function getEjecutivo($registro)
    {
        $model = new mUsuario();
        return $model->getEjecutivoCorreo($registro)->first();
    }

    static function getJefe($registro)
    {
        $model = new mUsuario();
        return $model->getJefe($registro)->first();
    }

    static function getGerenteDivisionBE()
    {
        $model = new mUsuario();
        return $model->getGerenteDivisionBE();
    }

    static function getGerenteZonalBE()
    {
        $model = new mUsuario();
        return $model->getGerenteZonalBE();
    }
    static function getAnalistaRiesgoBE()
    {
        $model = new mUsuario();
        return $model->getAnalistaRiesgoBE();
    }

    static function getUsuario($registro=null)
    {
        $model = new mUsuario();
        return $model->getUsuario($registro);
    }


    static function getUsuariosInfinity(){

        $model = new mUsuario();
        $usuarioinfinity=$model->getUsuarioInfinity();
        $usuarios=[];
        foreach ($usuarioinfinity as $user) {
            $usuarios[]=$user->REGISTRO;
        }
        return $usuarios;

    }

    static function getCantReferidos($registro)
    {
        $cantreferidos=Lead::referidosMesActual($registro);
        return $cantreferidos;
    }

    static function getGerente($registro)
    {
        $model = new mUsuario();
        return $model->getGerente($registro)->first();
    }

    //Prueba Piloto
    static function getAccesoSSRS()
    {
        /*$roles = array(10,13,14,20,21,22,23,24,25,26,27,28,29,30,31,666,999,41);
        return $roles;*/
        return [
            self::ROL_ANALISTA_GYS,
            self::ROL_GYS,
            self::ROL_SUBGERENTE_GYS,
            self::ROL_ADMINISTRADOR,
            self::ROL_RIESGOS_ME_ZONAL,
            self::ROL_RIESGOS_ME_JEFATURA,
            self::ROL_EJECUTIVO_FARMER_BE,
            self::ROL_ANALISTA_VPC,
            self::ROL_EJECUTIVO_HUNTER_BE,
            self::ROL_ANALISTA_NEGOCIO_JEFATURA_BE,
            self::ROL_ANALISTA_NEGOCIO_ZONAL_BE,
            self::ROL_JEFATURA_BE,
            self::ROL_GERENCIA_ZONAL_BE,
            self::ROL_GERENTE_DIVISION_BE,
            self::ROL_ANALISTA_EXTERNO_JEFATURA_BE,
            self::ROL_ANALISTA_EXTERNO_ZONAL_BE,
            self::ROL_ANALISTA_EXTERNO_DIVISION_BE,
            self::ROL_GERENTE_BANCA,
            self::ROL_EJECUTIVO_PRODUCTO,
            self::ROL_GERENTE_BEP,
            self::ROL_MUNDIAL,
            self::ROL_ROBOT_TD,
            self::ROL_ASESOR_M,
            self::ROL_ENCUESTADOR,
            self::ROL_ANALISTAS_WHATSAPP,
            self::ROL_EJECUTIVO_INNOVACION_CARTERA,
            self::ROL_EJECUTIVO_FIES,
            self::ROL_JEFE_FIES,
            self::ROL_EJECUTIVO_SEGUIMIENTO_CARTERA,
            self::ROL_EN_ESCOM,
            self::ROL_ANALISTA_RIESGOS_ADMISION,
            self::ROL_JEFE_ESCOM,
            self::ROL_GERENTE_RIESGOS,
            self::ROL_SUBGERENTE_RIESGOS,
            self::ROL_JEFE_RIESGOS,
            //self::ROL_JEFE_RIESGOS_BPE,
            self::ROL_MERCADO_CAPITALES,
            self::ROL_MERCADO_CAPITALES,
            self::ROL_ANALISTA_GYS,
            self::ROL_SUBGERENTE_GYS,
            self::ROL_ANALISTA_PRODUCTO,
        ];
    }
    static function getAccessCartera()
    {
        //20|21|24|23|25|26|30

        return [
            self::ROL_EJECUTIVO_FARMER_BE,
            self::ROL_EJECUTIVO_HUNTER_BE,
            self::ROL_EJECUTIVO_PRODUCTO,
            self::ROL_ANALISTA_VPC,
            self::ROL_JEFATURA_BE,
            self::ROL_GERENCIA_ZONAL_BE,
            self::ROL_GERENTE_DIVISION_BE,
            self::ROL_GERENTE_BANCA,
            self::ROL_ANALISTA_NEGOCIO_ZONAL_BE,
            self::ROL_ANALISTA_EXTERNO_DIVISION_BE,
            self::ROL_EJECUTIVO_INNOVACION_CARTERA,
            self::ROL_EJECUTIVO_SEGUIMIENTO_CARTERA,
        ];
    }
    static function getAccessReportes()
    {

        return [
            self::ROL_ADMINISTRADOR,
            self::ROL_ANALISTA_VPC,
            self::ROL_ANALISTA_EXTERNO_DIVISION_BE,
            self::ROL_ANALISTA_EXTERNO_JEFATURA_BE,
            self::ROL_ANALISTA_EXTERNO_ZONAL_BE,
            self::ROL_ANALISTA_NEGOCIO_JEFATURA_BE,
            self::ROL_ANALISTA_NEGOCIO_ZONAL_BE,
            self::ROL_ASESOR_M,
            self::ROL_EJECUTIVO_FARMER_BE,
            self::ROL_EJECUTIVO_HUNTER_BE,
            self::ROL_EJECUTIVO_PRODUCTO,
            self::ROL_GERENCIA_ZONAL_BE,
            self::ROL_GERENTE_BANCA,
            self::ROL_GERENTE_BEP,
            self::ROL_GERENTE_DIVISION_BE,
            self::ROL_JEFATURA_BE,
            self::ROL_MUNDIAL,
            self::ROL_RIESGOS_ME_JEFATURA,
            self::ROL_RIESGOS_ME_ZONAL,
            self::ROL_ROBOT_TD,
            self::ROL_ENCUESTADOR,
            self::ROL_EJECUTIVO_INNOVACION_CARTERA,
            self::ROL_EJECUTIVO_FIES,
            self::ROL_JEFE_FIES,
            self::ROL_EJECUTIVO_SEGUIMIENTO_CARTERA,
            self::ROL_EN_ESCOM,
            self::ROL_ANALISTA_RIESGOS_ADMISION,
            self::ROL_JEFE_ESCOM,
            self::ROL_GERENTE_RIESGOS,
            self::ROL_SUBGERENTE_RIESGOS,
            self::ROL_JEFE_RIESGOS,
            self::ROL_JEFE_RIESGOS_BPE,
            self::ROL_MERCADO_CAPITALES,
            self::ROL_ANALISTA_GYS,
            self::ROL_SUBGERENTE_GYS,
            self::ROL_ANALISTA_PRODUCTO,
        ];
    }
    static function getAccessProspectos()
    {
        //20|21|24|23|25|26|30

        return [
            self::ROL_ANALISTA_EXTERNO_DIVISION_BE,
            self::ROL_ANALISTA_EXTERNO_JEFATURA_BE,
            self::ROL_ANALISTA_VPC,
            self::ROL_ANALISTA_EXTERNO_ZONAL_BE,
            self::ROL_ANALISTA_NEGOCIO_JEFATURA_BE,
            self::ROL_ANALISTA_NEGOCIO_ZONAL_BE,
            self::ROL_EJECUTIVO_FARMER_BE,
            self::ROL_EJECUTIVO_HUNTER_BE,
            self::ROL_GERENCIA_ZONAL_BE,
            self::ROL_GERENTE_BANCA,
            self::ROL_GERENTE_BEP,
            self::ROL_GERENTE_DIVISION_BE,
            self::ROL_JEFATURA_BE,
            self::ROL_ANALISTAS_WHATSAPP,
            self::ROL_EJECUTIVO_INNOVACION_CARTERA,
            self::ROL_EJECUTIVO_SEGUIMIENTO_CARTERA,
        ];
    }
    static function getAccessAccComer()
    {

        return [
            self::ROL_ANALISTA_EXTERNO_DIVISION_BE,
            self::ROL_ANALISTA_EXTERNO_JEFATURA_BE,
            self::ROL_ANALISTA_VPC,
            self::ROL_ANALISTA_EXTERNO_ZONAL_BE,
            self::ROL_ANALISTA_NEGOCIO_JEFATURA_BE,
            self::ROL_ANALISTA_NEGOCIO_ZONAL_BE,
            self::ROL_EJECUTIVO_FARMER_BE,
            self::ROL_EJECUTIVO_HUNTER_BE,
            self::ROL_EJECUTIVO_PRODUCTO,
            self::ROL_GERENCIA_ZONAL_BE,
            self::ROL_GERENTE_BANCA,
            self::ROL_GERENTE_DIVISION_BE,
            self::ROL_JEFATURA_BE,
            self::ROL_EJECUTIVO_INNOVACION_CARTERA,
            self::ROL_EJECUTIVO_SEGUIMIENTO_CARTERA,
        ];
    }
    static function getAccessActividades()
    {

        return [
            self::ROL_ANALISTA_EXTERNO_DIVISION_BE,
            self::ROL_ANALISTA_EXTERNO_JEFATURA_BE,
            self::ROL_ANALISTA_EXTERNO_ZONAL_BE,
            self::ROL_ANALISTA_NEGOCIO_JEFATURA_BE,
            self::ROL_ANALISTA_NEGOCIO_ZONAL_BE,
            self::ROL_EJECUTIVO_FARMER_BE,
            self::ROL_EJECUTIVO_HUNTER_BE,
            self::ROL_ANALISTA_VPC,
            self::ROL_EJECUTIVO_PRODUCTO,
            self::ROL_GERENCIA_ZONAL_BE,
            self::ROL_GERENTE_BANCA,
            self::ROL_GERENTE_DIVISION_BE,
            self::ROL_JEFATURA_BE,
            self::ROL_ANALISTAS_WHATSAPP,
            self::ROL_EJECUTIVO_INNOVACION_CARTERA,
            self::ROL_EJECUTIVO_SEGUIMIENTO_CARTERA,
            self::ROL_JEFE_ESCOM,
            self::ROL_EN_ESCOM
        ];
    }
    static function getAccessEcosistema()
    {

        return [
            self::ROL_ANALISTA_EXTERNO_DIVISION_BE,
            self::ROL_ANALISTA_EXTERNO_JEFATURA_BE,
            self::ROL_ANALISTA_EXTERNO_ZONAL_BE,
            self::ROL_ANALISTA_VPC,
            self::ROL_ANALISTA_NEGOCIO_JEFATURA_BE,
            self::ROL_ANALISTA_NEGOCIO_ZONAL_BE,
            self::ROL_EJECUTIVO_FARMER_BE,
            self::ROL_EJECUTIVO_HUNTER_BE,
            self::ROL_EJECUTIVO_PRODUCTO,
            self::ROL_GERENCIA_ZONAL_BE,
            self::ROL_GERENTE_BANCA,
            self::ROL_GERENTE_DIVISION_BE,
            self::ROL_JEFATURA_BE
        ];
    }
    static function getAccessContactos()
    {


        return [
            self::ROL_ANALISTA_EXTERNO_DIVISION_BE,
            self::ROL_ANALISTA_EXTERNO_JEFATURA_BE,
            self::ROL_ANALISTA_EXTERNO_ZONAL_BE,
            self::ROL_ANALISTA_NEGOCIO_JEFATURA_BE,
            self::ROL_ANALISTA_NEGOCIO_ZONAL_BE,
            self::ROL_EJECUTIVO_FARMER_BE,
            self::ROL_EJECUTIVO_HUNTER_BE,
            self::ROL_EJECUTIVO_PRODUCTO,
            self::ROL_GERENCIA_ZONAL_BE,
            self::ROL_GERENTE_BANCA,
            self::ROL_ANALISTA_VPC,
            self::ROL_GERENTE_DIVISION_BE,
            self::ROL_JEFATURA_BE,
            self::ROL_EJECUTIVO_INNOVACION_CARTERA,
            self::ROL_EJECUTIVO_SEGUIMIENTO_CARTERA,
        ];
    }
    static function getAccessMenus()
    {

        return [
            self::ROL_ANALISTA_EXTERNO_DIVISION_BE,
            self::ROL_ANALISTA_EXTERNO_JEFATURA_BE,
            self::ROL_ANALISTA_EXTERNO_ZONAL_BE,
            self::ROL_ANALISTA_NEGOCIO_JEFATURA_BE,
            self::ROL_INTELIGENCIA_RIESGOS,
            self::ROL_ANALISTA_VPC,
            self::ROL_ANALISTA_NEGOCIO_ZONAL_BE,
            self::ROL_EJECUTIVO_FARMER_BE,
            self::ROL_EJECUTIVO_HUNTER_BE,
            self::ROL_EJECUTIVO_PRODUCTO,
            self::ROL_GERENCIA_ZONAL_BE,
            self::ROL_GERENTE_BANCA,
            self::ROL_GERENTE_DIVISION_BE,
            self::ROL_JEFATURA_BE,
            self::ROL_RIESGOS_ME_ZONAL,
            self::ROL_ANALISTA_RIESGOS,
            self::ROL_SUBGERENTE_RIESGOS,
            self::ROL_JEFE_RIESGOS,
            //self::ROL_JEFE_RIESGOS_BPE,
            self::ROL_JEFE_ESCOM,
            self::ROL_EN_ESCOM,
            self::ROL_ANALISTA_RIESGOS_ADMISION,
            self::ROL_EJECUTIVO_SEGUIMIENTO_CARTERA,
            self::ROL_EJECUTIVO_INNOVACION_CARTERA,
            self::ROL_GERENTE_RIESGOS,
            self::ROL_ANALISTAS_WHATSAPP,
            self::ROL_GYS,
            self::ROL_ANALISTA_GYS,
            self::ROL_SUBGERENTE_GYS,
            self::ROL_JEFE_ESCOM,
            self::ROL_EN_ESCOM
        ];
    }
    static function getAccessAlertasI()
    {

        return [
            self::ROL_ANALISTA_EXTERNO_DIVISION_BE,
            self::ROL_ANALISTA_EXTERNO_JEFATURA_BE,
            self::ROL_ANALISTA_EXTERNO_ZONAL_BE,
            self::ROL_ANALISTA_NEGOCIO_JEFATURA_BE,
            self::ROL_ANALISTA_NEGOCIO_ZONAL_BE,
            self::ROL_EJECUTIVO_FARMER_BE,
            self::ROL_EJECUTIVO_HUNTER_BE,
            self::ROL_EJECUTIVO_PRODUCTO,
            self::ROL_GERENCIA_ZONAL_BE,
            self::ROL_GERENTE_BANCA,
            self::ROL_GERENTE_DIVISION_BE,
            self::ROL_ANALISTA_VPC,
            self::ROL_JEFATURA_BE,
            self::ROL_RIESGOS_ME_JEFATURA,
            self::ROL_RIESGOS_ME_ZONAL
        ];
    }

	static function getUsuariosCovidguardar()
    {
        return [
            self::ROL_ANALISTA_NEGOCIO_ZONAL_BE,
            self::ROL_EJECUTIVO_FARMER_BE,
            self::ROL_EJECUTIVO_HUNTER_BE,
            self::ROL_JEFATURA_BE,
            self::ROL_GERENCIA_ZONAL_BE
        ];

    }
    static function getAccesoVista()
    {
        $model = new mUsuario();
        $usuariosvista = $model->getAccesoVista(self::VW_BLACKLISTBPE)->get();
        $usuarios = [];
        foreach ($usuariosvista as $user) {
            $usuarios[] = $user->REGISTRO;
        }
        return $usuarios;
    }

    static function usuariosvideosGF()
    {
        $model = new mAlertacartera();
        $usuariosvista = $model->usuariosvideosGF();
        $usuarios = [];
        foreach ($usuariosvista as $user) {
            $usuarios[] = $user->REGISTRO;
        }
        return $usuarios;
    }

    function addUsuario($data)
    {
        $model = new mUsuario();
        if ($model->getUsuario($data['registro'])) {
            $this->setMessage('Este usuario ya existe');
            return false;
        }else{
            $datos=[
                'REGISTRO'=>strtoupper($data['registro']),
                'NOMBRE'=>strtoupper($data['nombre']),
                'DNI'=>$data['dni'],
                'ROL'=>$data['rol'],
                'BANCA'=>$data['banca'],
                'FLAG_ACTIVO'=>1,
            ];

            if ($model->insertUsuario($datos)) {
                $this->setMessage('Se agrego con éxito');
                return true;
            }else{
                $this->setMessage('Hubo un error, comunicate con Soporte');
                return false;
            }
        }
    }

    function updateUsuario($data)
    {
        $model = new mUsuario();
        if ($model->getUsuario($data['registro'])) {
            $datos=[
                'NOMBRE'=>strtoupper($data['nombre']),
                'DNI'=>$data['dni'],
                'ROL'=>$data['rol'],
                'BANCA'=>$data['banca'],
            ];

            if ($model->updatetUsuario($datos,$data['registro'])) {
                $this->setMessage('Se actualizo con éxito');
                return true;
            }else{
                $this->setMessage('Hubo un error, comunicate con Soporte');
                return false;
            }
        }else{
            $this->setMessage('Este usuario no existe');
            return false;
        }
    }

    function getRoles($idrol=null)
    {
        $model = new mUsuario();
        return $model->getRoles($idrol);
    }

    function clickruta($data,$registro,$nombrepc=null)
    {
        $model = new mUsuario();
        $datos=[
            'REGISTRO'=>$registro,
            'NOMBREVISTA'=>$data['nombrevista'],
            'NOMBRE_PC'=>$nombrepc,
        ];

        if ($model->guardarvxu($datos)) {
            $this->setMessage('Se agrego con éxito');
            return true;
        }else{
            $this->setMessage('Hubo un error, comunicate con Soporte');
            return false;
        }
    }

}
