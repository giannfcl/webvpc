<?php

namespace App\Entity;

use App\Entity\Infinity\Gestion;
use Jenssegers\Date\Date as Carbon;
use App\Model\GestionLeadCampanha as mGestionLead;
use App\Model\Lead as mLead;

class GestionLead extends \App\Entity\Base\Entity {

    protected $_id;
    protected $_periodo;
    protected $_campanha;
    protected $_ejecutivo;
    protected $_lead;
    protected $_resultado;
    protected $_motivo;
    protected $_gestion;
    protected $_comentario;
    protected $_flgUltimo;
    protected $_fechaRegistro;
    protected $_fechaActualizacion;
    protected $_banca;
    protected $_rolGestion;
    protected $_fechaVolverLLamar;
    protected $_visitado;

    protected $_solicitudMonto;
    protected $_solicitudTasa;
    protected $_solicitudPlazo;
    protected $_solicitudGracia;

    const ULTIMO = 1;
    const NO_ULTIMO = 0;

    const VISITA_VISITADO = 'VISITADO';
    const VISITA_DESCARTADO = 'DESCARTADO';

    function setValueToTable() {
        return $this->cleanArray([
                    'PERIODO' => $this->_periodo,
                    'ID_CAMP_EST' => $this->_campanha,
                    'REGISTRO_EN' => $this->_ejecutivo,
                    'NUM_DOC' => $this->_lead,
                    'ID_RESULTADO_GESTION' => $this->_gestion,
                    'COMENTARIO' => $this->_comentario,
                    'FLG_ULTIMO' => $this->_flgUltimo,
                    'BANCA' => $this->_banca,
                    'ROL_GESTION' => $this->_rolGestion,
                    'FECHA_REGISTRO' => $this->_fechaRegistro,
                    'FECHA_ACTUALIZACION' => $this->_fechaActualizacion,
                    'FECHA_VOLVER_LLAMAR' => $this->_fechaVolverLLamar,
                    'VISITADO' => $this->_visitado,
                    'SOLICITUD_MONTO' => $this->_solicitudMonto,
                    'SOLICITUD_TASA' => $this->_solicitudTasa,
                    'SOLICITUD_PLAZO' => $this->_solicitudPlazo,
                    'SOLICITUD_GRACIA' => $this->_solicitudGracia,
        ]);
    }

    static function getByLead($lead,$cartera=NULL){
        $model = new mGestionLead();
        return $model->getByLead($lead,$cartera)->get();
    }

    function getById(){
        $model = new mGestionLead();
        return $model->get($this->_id)->first();
    }

    function registrar(){
        $hoy = Carbon::now();        

        
        // Si el resultado es lo pensará, validar que la fecha sea correcta y tenga comentario

        if (!in_array($this->_resultado,GestionResultado::getResultadosAcepta())){
            $this->_solicitudMonto == 'NULL';
            $this->_solicitudTasa == 'NULL';
            $this->_solicitudPlazo == 'NULL';
            $this->_solicitudGracia == 'NULL';
        }

        //Cambio
        if ($this->_resultado == GestionResultado::RESULTADO_LO_PENSARA_ID){
            // Revisar si comentario esta seteado
            if (strlen ($this->_comentario) < 10){
                $this->setMessage('Debes ingresar un comentario cuando el resultado es LO PENSARÁ');
                return false;
            }

            //Revisa que se haya ingresado una fecha
            if (!$this->_fechaVolverLLamar){
                $this->setMessage('Debes ingresar una fecha tentativa cuando el resultado es LO PENSARÁ');
                return false;
            }
            // Debido a que lo pensara no tiene motivos, forzamos a que setee uno por defecto
            //$this->_motivo = GestionResultado::MOTIVO_LO_PENSARA_ID;
            $diferencia = $hoy->diffInDays(Carbon::createFromFormat('Y-m-d', $this->_fechaVolverLLamar)); 
            if ($diferencia < 1 or $diferencia > 90){
                $this->setMessage('La fecha ingresada no es válida (El plazo es max. de 90 días)');
                return false;
            }
        }


        // Validar relacion lead - asistente
        $modelLead = new mLead();
        $lead = null;
        if ($this->_rolGestion == Canal::ASISTENTE_COMERCIAL){
            $lead = $modelLead->getLeadAsistente(
            $this->getPeriodo(),
            $this->_ejecutivo, 
            null, //no es necesario especificar el ejecutivo por que queremos traer de todos los asignados al AC
            $this->_lead,
            Canal::getCanalesAsistente())->first();
            if (!$lead){
                $this->setMessage('El lead no se encuentra disponible para la gestión');
                return false;
            }
        }

        // Validar relacion lead - ejecutivo
        if ($this->_rolGestion == Canal::EJECUTIVO_NEGOCIO){
            $lead = $modelLead->getLeadEjecutivo(
            self::sgetPeriodo(),
            $this->_ejecutivo, 
            $this->_lead,
            Canal::getCanalesEjecutivo())->first();
            if (!$lead){
                $this->setMessage('El lead no se encuentra disponible para la gestión');
                return false;
            }
        }
        //Validacion para campanha crecer, se requieren que ciertos datos del cliente sean obligatorios
        if ($this->_resultado == GestionResultado::RESULTADO_ENVIO_GTP && $this->_campanha == Campanha::CAMPANHA_BPE_CRECER){
            $camposVacios = [];
            if(!$lead->DIR_UBIGEO){
                $camposVacios[] = '<br>- Ubigeo';
            }
            if(!$lead->RRLL_DOCUMENTO_1 || !$lead->RRLL_APELLIDOS || !$lead->REPRESENTANTE_LEGAL){
                $camposVacios[] = '<br>- Representante legal';
            }
            if(!$lead->VENTAS_ANUALES  || $lead->VENTAS_ANUALES <= 0){
                $camposVacios[] = '<br>- Ventas';
            }
            if(!$lead->EXPORTACIONES_ANUALES){
                $camposVacios[] = '<br>- Exportaciones';
            }
            if(!$lead->NROEMPLEADOS || $lead->NROEMPLEADOS <= 0){
                $camposVacios[] = '<br>- Número de Empleados';
            }
            \Log::info($lead->TIPO_CUENTA);
            \Log::info($lead->NUM_CUENTA);
            if(!$lead->TIPO_CUENTA && !$lead->NUM_CUENTA){
                $camposVacios[] = '<br>- Tipo y numero de cuenta';
            }
            if ($camposVacios){
                $this->setMessage('Aún no has completado los siguientes datos del cliente: ' . implode(',',$camposVacios));
                return false;
            }
            
        }

        
        $this->_fechaRegistro = $this->_fechaActualizacion = $hoy->toDateTimeString();
        //$this->_periodo = $this->getPeriodo();
        $this->_flgUltimo = self::ULTIMO;
        $this->_gestion = $this->_motivo;

        //Excepcion ENVIO GTP
        if ($this->_resultado == GestionResultado::RESULTADO_ENVIO_GTP){
            $this->_gestion = GestionResultado::MOTIVO_ENVIO_GTP;
        }

        
        

        $model = new mGestionLead();
        $result = false;

        // Si no se ha seleccionado campaña, la gestión debe aplicarse para todas las campañas del cliente
        if (!$this->_campanha){
            $campanhas = LeadCampanha::getCampanhasByLead($this->_lead,Canal::getCanalesEjecutivo());
            if (count($campanhas) == 1){
                //Si solo tiene una campaña asignada, hacemos una asignación para un registro simple
                $this->_campanha = $campanhas[0]->ID_CAMP_EST;
                $this->_periodo = $campanhas[0]->PERIODO;
                $this->_id = $result = $model->registrar($this->setValueToTable());
            } else {
                $gestiones = [];
                foreach($campanhas as $cmp){
                    $this->_campanha = $cmp->ID_CAMP_EST;
                    $this->_periodo = $cmp->PERIODO;
                    $gestiones[] = $this->setValueToTable();
                }
                $result = $model->registrarLote($gestiones);
            }
        }else{
            //Si la gestión fue a un cliente con cita programada, hay que actualizar el estado de la cita
            $cita = new Cita();            

            if($cita->buscar($this->_ejecutivo,$this->_lead)){
                $cita->setValue('_estado',($this->_visitado == self::VISITA_VISITADO)? CitaEstado::REALIZADO : CitaEstado::DESCARTADO);

                //Set de datos del histórico de citas
                $citaEstadoHist = new CitaEstadoHistorico();
                $citaEstadoHist->setValues([
                    '_id' => $cita->getValue('_id'),
                    '_periodo' => $this->_periodo,
                    '_lead' => $this->_lead,
                    '_estado' => $cita->getValue('_estado'),
                    '_fechaCita' => $cita->getValue('_fechaCita'),
                    '_fechaRegistro' => $hoy,
                    '_usuario' => $this->_ejecutivo
                ]);
                $this->_id = $result = $model->registrar($this->setValueToTable(),$cita->getValue('_id'),$cita->setValueToTable(),$citaEstadoHist->setValueToTable());
            }else{
                $this->_id = $result = $model->registrar($this->setValueToTable());
            }

            
        }
        
        // Si solo hay una campaña asignada al cliente
        
        if($result){
            $this->setMessage("Se registró la gestión para ".$lead->NOMBRE_CLIENTE." de manera correcta");
            return true;
        } else{
            $this->setMessage("Error al registrar en base de datos");
            return false;
        }

    }



    function registrarCartera(){
        $hoy = Carbon::now();

        // Si el resultado es lo pensará, validar que la fecha sea correcta y tenga comentario
        if ($this->_resultado == GestionResultado::RESULTADO_LO_PENSARA_ID){
            // Revisar si comentario esta seteado
            if (strlen ($this->_comentario) < 10){
                $this->setMessage('Debes ingresar un comentario cuando el resultado es LO PENSARÁ');
                return false;
            }

            //Revisa que se haya ingresado una fecha
            if (!$this->_fechaVolverLLamar){
                $this->setMessage('Debes ingresar una fecha tentativa cuando el resultado es LO PENSARÁ');
                return false;
            }

            // Debido a que lo pensara no tiene motivos, forzamos a que setee uno por defecto
            $this->_motivo = GestionResultado::MOTIVO_LO_PENSARA_ID;
            $diferencia = $hoy->diffInDays(Carbon::createFromFormat('Y-m-d', $this->_fechaVolverLLamar)); 
            if ($diferencia < 1 or $diferencia > 90){
                $this->setMessage('La fecha ingresada no es válida (El plazo es max. de 90 días)');
                return false;
            }

        }

        if($this->_resultado==GestionResultado::RESULTADO_FUERA_DE_PERFIL_POR_VENTAS)
            $this->_motivo=GestionResultado::MOTIVO_FUERA_DE_PERFIL_POR_VENTAS;
       if($this->_resultado==GestionResultado::RESULTADO_MALAS_REFERENCIAS_CREDITICIAS)
            $this->_motivo=GestionResultado::MOTIVO_MALAS_REFERENCIAS_CREDITICIAS;
       if($this->_resultado==GestionResultado::RESULTADO_ACEPTA_GIRO)
            $this->_motivo=GestionResultado::MOTIVO_ACEPTA_GIRO;

        if($this->_resultado==GestionResultado::RESULTADO_CONTACTO_AVAL)
            $this->_motivo=GestionResultado::MOTIVO_CONTACTO_AVAL;
        if($this->_resultado==GestionResultado::RESULTADO_NO_CONTESTA)
            $this->_motivo=    GestionResultado::MOTIVO_NO_CONTESTA;
        if($this->_resultado==GestionResultado::RESULTADO_NRO_ERRADO)
            $this->_motivo=    GestionResultado::MOTIVO_NRO_ERRADO;
        if($this->_resultado==GestionResultado::RESULTADO_MALOGRADO_CORTADO)
            $this->_motivo=    GestionResultado::MOTIVO_MALOGRADO_CORTADO;
        if($this->_resultado==GestionResultado::RESULTADO_CONTACTO_TITULAR)
            $this->_motivo=    GestionResultado::MOTIVO_CONTACTO_TITULAR;
        if($this->_resultado==GestionResultado::RESULTADO_CONTACTO_FAMILIAR)
            $this->_motivo=    GestionResultado::MOTIVO_CONTACTO_FAMILIAR;
        if($this->_resultado==GestionResultado::RESULTADO_CONTACTO_VECINO)
            $this->_motivo=    GestionResultado::MOTIVO_CONTACTO_VECINO;



        // Validar relacion lead - asistente
        $modelLead = new mLead();
        $lead = null;

      
        if ($this->_rolGestion == Canal::ASISTENTE_COMERCIAL){
            $lead = $modelLead->getLeadAsistente(
            $this->getPeriodo(),
            $this->_ejecutivo, 
            null, //no es necesario especificar el ejecutivo por que queremos traer de todos los asignados al AC
            $this->_lead,
            Canal::getCanalesAsistente())->first();
            if (!$lead){
                $this->setMessage('El cliente no se encuentra disponible para la gestión');
                return false;
            }
        }

        // Validar relacion lead - ejecutivo
        if ($this->_rolGestion == Canal::EJECUTIVO_NEGOCIO){
            $lead = $modelLead->getLeadEjecutivo(
            self::sgetPeriodo(),
            $this->_ejecutivo, 
            $this->_lead,
            null)->first();
            if (!$lead){
                $this->setMessage('El cliente no se encuentra disponible para la gestión');
                return false;
            }
        }

        
        $this->_fechaRegistro = $this->_fechaActualizacion = $hoy->toDateTimeString();
        //$this->_periodo = $this->getPeriodo();
        $this->_flgUltimo = self::ULTIMO;
        $this->_gestion = $this->_motivo;  
        
        $model = new mGestionLead();
        $result = false;

        $this->_id = $result = $model->registrar($this->setValueToTable());
                       
        if($result){
            $this->setMessage("Se registró la gestión para ".$lead->NOMBRE_CLIENTE." de manera correcta");
            return true;
        } else{
            $this->setMessage("Error al registrar en base de datos");
            return false;
        }

    }

}