<?php

namespace App\Entity\AsignacionResectorizacion;

use Jenssegers\Date\Date as Carbon;
use App\Entity\BE\LeadEtapa as LeadEtapa;
use App\Entity\BE\Lead as Lead;
use App\Entity\BE\Etapa as Etapa;
use App\Entity\BE\leadSectorista as LeadSectorista;
use App\Entity\LeadCampanha as LeadCampanha;
use App\Entity\BE\Actividad as Actividad;
use App\Entity\Campanha as Estrategia;
use App\Model\AsignacionResectorizacion\Reasignacion as modelReasignacion;

class Asignacion extends \App\Entity\Base\Entity {

    protected $_num_doc;
    protected $_codUnico;
    protected $_registro;
    protected $_bancaOrigen;
    protected $_bancaDestino;
    protected $_registroAsignado;
    protected $_registroDest;
    protected $_codSectO;
    protected $_codSecDest;
    protected $_conformidad;
    protected $_archivo1;
    protected $_archivoNombre1;
    protected $_extension1;
    protected $_archivo2;
    protected $_archivoNombre2;
    protected $_extension2;
    protected $_registro_o;
    protected $_registro_d;
    protected $_caso;
    protected $_flgpendiente;
            
    
    const RUTA = '/asignacion/Conformidades';
    const RUTA2 = '/asignacion/Referidos';
    
    function setValueToTable() {
        $data = [                       
            'ADJUNTO1' => $this->_adjunto1,
            'ADJUNTO2' => $this->_adjunto2,
            'COD_UNICO' => $this->_codUnico,
            'NUM_DOC' => $this->_num_doc,
            'BANCA_O' => $this->_bancaOrigen,
            'BANCA_D' => $this->_bancaDestino,
            'REG_EJECUTIVO_O' => $this->_registroAsignado,
            'REG_EJECUTIVO_D' => $this->_registroDest,
            'COD_SECT_O' => $this->_codSectO,
            'COD_SECT_D' => $this->_codSecDest,
            'CONFORMIDAD'=> $this->_conformidad,
            'ARCHIVO1'=> $this->_archivoNombre1,
            'ARCHIVO2'=> $this->_archivoNombre2,
            'FECHA_REGISTRO'=> $this->_fechaRegistro,
            'CASO'=> $this->_caso,
            'FLG_PENDIENTE'=> $this->_flgpendiente,
        ];
        return $this->cleanArray($data);
    }

    function guardarCorreoReferido($numdoc,$usuario,$correo)
    {
        $hoy = Carbon::now();
        $data=[
            'PERIODO'=>self::sgetPeriodoBE(),
            'NUM_DOC'=>$numdoc,
            'REGISTRO'=>$usuario,
            'CONFORMIDAD_JEFE'=> '003_' . $numdoc . '_' . $usuario . '_'
                . $hoy->format('Ymd_Hi') . '.' . pathinfo($correo->getClientOriginalName())['extension'],
            'FECHA_REGISTRO'=>Carbon::now()
        ];
        $model=new modelReasignacion();
        return $model->guardarCorreoReferido($data,$correo,self::RUTA2);
    }

    function registrar() {
        $model = new modelReasignacion();
        $hoy = Carbon::now();
        $this->_periodo = self::sgetPeriodoBE();
        $this->_fechaRegistro = $hoy;
        
        //nombre y rutas de los archivos
        $this->_adjunto1 = $this->_archivo1? '001_' . $this->_codUnico . '_' . $this->_registroDest . '_'
                . $this->_fechaRegistro->format('Ymd_Hi') . '.' . $this->_extension1 : null;
        $this->_ruta1 = self::RUTA;
        
        $this->_adjunto2 = $this->_archivo2? '002_' . $this->_codUnico . '_' . $this->_registroDest . '_'
                . $this->_fechaRegistro->format('Ymd_Hi') . '.' . $this->_extension2 : null; 
        
        $this->_ruta2 = self::RUTA;
        //dd( $this->setValueToTable());
        return $model->guardarReasignacion( $this->setValueToTable() ,$this->_archivo1, $this->_ruta1,$this->_archivo2, $this->_ruta2);
    }

    function getCodSectorista($registro)
    {
        $model=new modelReasignacion();
        return $model->getCodSectorista($registro);
    }
    function getOneCodSectorista($registro)
    {
        $model=new modelReasignacion();
        return $model->getCodSectorista($registro)->first();
    }

    function Nuevoreferido($existelead,$nombre,$documento,$facturacion,$registro)
    {
        $lead = new Lead();
        $lead->setValues([
            '_periodo' => self::sgetPeriodoBE(),
            '_numdoc' => $documento,
            '_codUnico' => $lead->ClientesVPC($documento)!=null ? $lead->ClientesVPC($documento)->COD_UNICO : null,
            '_nombre' => $lead->ClientesVPC($documento)!=null ? $lead->ClientesVPC($documento)->NOMBRE_CLIENTE : $nombre,
            '_flagCliente' => '0',
            '_estado' => '1',
            '_tipoProspecto'=>'R'
        ]);

        $leadSectorista = new LeadSectorista();
        $leadSectorista->setValues([
            '_periodo' => self::sgetPeriodoBE(),
            '_numdoc' => $documento,
            '_ejecutivo' => $registro,
            '_codigosectorista' => Lead::getSectoristaArbol($registro) ? Lead::getSectoristaArbol($registro)->COD_SECT_LARGO : null,
            '_codigoUnicoSectorista' => Lead::getSectoristaArbol($registro) ? Lead::getSectoristaArbol($registro)->COD_SECT_UNIQ_EN : null,
            '_flgUltimo' => '1',
            '_flgcliente' => '0',
            '_fechaRegistro' => Carbon::now()
        ]);

        $leadEtapa = new LeadEtapa();
        $leadEtapa->setValues([
            '_periodo' => self::sgetPeriodoBE(),
            '_numdoc' => $documento,
            '_etapa' => Etapa::PENDIENTE,
            '_estrategia' => Estrategia::ESTRATEGIA_BE_REFERIDO,
            '_ejecutivoAsignado' => $registro,
            '_ejecutivo' => $registro,
            '_flgUltimo' => '1',
            '_fechaRegistro' => Carbon::now()
        ]);

        $leadCampanha = new LeadCampanha();
        $leadCampanha->setValues([
            '_periodo' => self::sgetPeriodoBE(),
            '_lead' => $documento,
            '_fechaActualizacion' => Carbon::now(),
            '_fechaCarga' => Carbon::now(),
            '_campanha' => Estrategia::ESTRATEGIA_BE_REFERIDO,
            
        ]);

        $actividad = new Actividad();
        $actividad->setValues([
            '_numdoc' => $documento,
            '_ejNegocio' => $registro,
            '_fechaActividad' => Carbon::now(),
            '_titulo' => 'ASIGNADO',
            '_tipo' => Actividad::TIPO_CAMBIO_ESTADO,
            '_fechaRegistro' => Carbon::now(),
            '_periodo' => self::sgetPeriodoBE(),
            '_idCampEst' => Estrategia::ESTRATEGIA_BE_REFERIDO,
        ]);

        $model=new modelReasignacion();
        return $model->guardarNuevoReferido($existelead,$lead->setValueToTable(),$leadCampanha->setValueToTable(),$leadSectorista->setValueToTable(),$leadEtapa->setValueToTable(),$actividad->setValueToTable());
    }
}
