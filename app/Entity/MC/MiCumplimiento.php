<?php

namespace App\Entity\MC;
use Jenssegers\Date\Date as Carbon;
use App\Model\MC\MiCumplimiento as MMicumplimiento;
use Illuminate\Support\Facades\Auth;
use App\Entity\Usuario as Usuario;
use App\Entity\Infinity\Semaforo as Semaforo;

class MiCumplimiento extends \App\Entity\Base\Entity {

    static function getPeriodos(){
        $query = MMicumplimiento::getPeriodos();

        return !empty($query) ? $query : '';
    }

    static function getBancas(){
        $query = MMicumplimiento::getBancas();

        return !empty($query) ? $query : '';
    }

    static function getZonales($banca=null){
        $query = MMicumplimiento::getZonales($banca);

        return !empty($query) ? $query : '';
    }

    static function lista($banca=null,$zonal=null,$periodo=null){
        if (!empty($zonal)) {
            $query = MMicumplimiento::lista($banca,$zonal,$periodo);
        }
        return !empty($query) ? $query : '';
    }

    static function getIndicadores($banca,$zonal,$periodo,$ejecutivos,$jefaturas){
        if (!empty($zonal)) {
            $query = MMicumplimiento::getIndicadores($banca,$zonal,$periodo,$ejecutivos);
        }
        return !empty($query) ? $query : '';       
    }

    static function getEjecutivos($banca=null,$cod_sect=null){
        $query = MMicumplimiento::getEjecutivos($banca,$cod_sect);

        return !empty($query) ? $query : '';
    }

    static function getJefaturas($banca=null,$cod_sect=null)
    {
        $query = MMicumplimiento::getJefaturas($banca,$cod_sect);

        return !empty($query) ? $query : '';
    }
}