<?php

namespace App\Entity;

use App\Entity\Canal as Canal;
use App\Entity\Leads as Leads;
use Jenssegers\Date\Date as Carbon;
use App\Model\Citas as mCitas;
use App\Model\GestionLeadCampanha as mGestionLead;
use App\Entity\Usuario;
use \Illuminate\Pagination\LengthAwarePaginator as Paginator;

class Cita extends \App\Entity\Base\Entity {

    protected $_id;
    protected $_periodo;
    protected $_lead;
    protected $_agendador;
    protected $_ejecutivo;
    protected $_fechaCita;
    protected $_hora;
    protected $_fecha;
    protected $_horario;
    protected $_contacto;
    protected $_estado;
    protected $_contactoTelefono;
    protected $_contactoDepartamento;
    protected $_contactoProvincia;
    protected $_contactoDistrito;
    protected $_contactoDireccion;
    protected $_contactoReferencia;
    protected $_fechaRegistro;
    protected $_autorizacionDatos;
    protected $_tipo;

    const HORARIO_1 = 1;
    const HORARIO_2 = 2;
    const HORARIO_3 = 3;

    const HORARIO_1_DESC = '9:00-10:00';
    const HORARIO_2_DESC = '11:00-12:30';
    const HORARIO_3_DESC = '15:00-16:30';

    const DIAS_HORARIO_EJECUTIVO = 7;
    const DIAS_CALENDARIO_AC = 2;
    const DIAS_CALENDARIO_CALL = 5;

    const ITEMS_PER_PAGE = 15;


    static function getAllDataTable(\App\Entity\Usuario $usuario){

        $model = new mCitas();
        if ($usuario->getValue('_rol') == Usuario::ROL_GERENTE_CENTRO){
            $result = $model->getCitas(self::sgetPeriodo(),null,null,$usuario->getValue('_centro'));
        }

        if ($usuario->getValue('_rol') == Usuario::ROL_GERENTE_ZONA){
            $result = $model->getCitas(self::sgetPeriodo(),null,$usuario->getValue('_zona'),null);
        }

        return $result;
    }

    function getAll(){
        $model = new mCitas();
        $query = $model->getCitas($this->getPeriodo());
        $count = $query->count();
        $results = $query->get();
        return $query->paginate(self::ITEMS_PER_PAGE);
    }

    function fechasStats() {
        $model = new mCitas();
        return $model->ultimos5Dias();
    }

    function setProperties($data){
        $this->setValues([
                '_id' => $data->ID_CITA, 
                '_periodo' => $data->PERIODO,
                '_lead' => $data->NUM_DOC,
                '_agendador' => $data->REGISTRO_EN_AGENDADOR,
                '_ejecutivo' => $data->REGISTRO_EN,
                '_fechaCita' => Carbon::createFromFormat('Y-m-d H:i:s', $data->FECHA_CITA),
                '_tipo' => $data->TIPO,
                '_estado' => $data->ESTADO,
                '_fechaRegistro' => Carbon::createFromFormat('Y-m-d H:i:s', $data->FECHA_REGISTRO),
        ]);
    }

    function getById($id){
        $model = new mCitas();
        $this->setProperties($model->getById($id)->first());
    }

    function buscar($en,$lead){
        $model = new mCitas();
        $data = $model->buscar($en,$lead)->first();
        if ($data){
            $this->setProperties($data);
            return true;
        }
        return false;
        
    }


    function setValueToTable() {
        return $this->cleanArray([
                    'ID_CITA' => $this->_id,
                    'PERIODO' => $this->_periodo,
                    'NUM_DOC' => $this->_lead,
                    'REGISTRO_EN_AGENDADOR' => $this->_agendador,
                    'REGISTRO_EN' => $this->_ejecutivo,
                    'FECHA_CITA' => ($this->_fechaCita)? $this->_fechaCita->toDateTimeString() : null,
                    'TIPO_HORARIO' => $this->_horario,
                    'PERSONA_CONTACTO' => $this->_contacto,
                    'TELEFONO_CONTACTO' => $this->_contactoTelefono,
                    'DEPARTAMENTO' => $this->_contactoDepartamento,
                    'PROVINCIA' => $this->_contactoProvincia,
                    'DISTRITO' => $this->_contactoDistrito,
                    'DIRECCION_CONTACTO' => $this->_contactoDireccion,
                    'REFERENCIA' => $this->_contactoReferencia,
                    'FECHA_REGISTRO' => ($this->_fechaRegistro)? $this->_fechaRegistro->toDateTimeString() : null,
                    'TIPO' => $this->_tipo,
                    'ESTADO' => $this->_estado,
                    'AUTORIZACION_DATOS' => $this->_autorizacionDatos,
        ]);
    }

    function registrar()
 {
        $hoy = Carbon::now();
        
        /* Seteos de Clase */
        $this->_periodo = $this->getPeriodo();
        $this->_horario = $this->getHorarioByHora($this->_hora);
        $this->_fechaRegistro = $hoy;
        $this->_estado = CitaEstado::AGENDADO;
        $this->_fechaCita = Carbon::createFromFormat('Y-m-d H:i', $this->_fecha . ' ' . $this->_hora);


        /****VALIDACIONES**/

        // Validar cita en dia de la semana y no es feriado
        if ($this->_fechaCita->isWeekend() or $this->esFeriado($this->_fechaCita)){
            $this->setMessage('El día seleccionado no es válido (fin de semana o feriado)');
            return false;
        }

        // Validar cita en un plazo de 2 días para AC
        if ($this->_tipo == Canal::ASISTENTE_COMERCIAL){
            $diasValidosCalendario = $this->getCalendarioEjecutivoAC();
            if (!isset($diasValidosCalendario[$this->_fechaCita->format('Y-m-d')])){
                $this->setMessage('La cita debe ser a lo máximo en '. self::DIAS_CALENDARIO_AC .' dias a partir de hoy');
                return false;
            }
        }

        // Validar mismo mes para el call
        if ($this->_tipo == Canal::CALL_CENTER){
            $diasValidosCalendario = $this->getCalendario(20);
            if (!isset($diasValidosCalendario[$this->_fechaCita->format('Y-m-d')])){
                $this->setMessage('La cita debe ser a lo máximo en 15 días a partir de hoy');
                return false;
            }
        }
        
        $lead = null;

        // Validar relacion asistente/lead para AC
        if ($this->_tipo == Canal::ASISTENTE_COMERCIAL){
            $lead = Leads::getLeadAsistenteComercial($this->_agendador,$this->_lead,$this->_ejecutivo);
            if (!$lead){
                $this->setMessage('El lead no se encuentra disponible para la gestión');
                return false;
            }
        }

        // Validar relacion call/lead para CALL
        if ($this->_tipo == Canal::CALL_CENTER){
            $lead = Leads::getLeadCall($this->_lead);
            if (!$lead){
                $this->setMessage('El lead no se encuentra disponible para la gestión');
                return false;
            }
        }

        
        // validar cliente-cita-periodo solo 1
        if ($lead->FECHA_CITA){
            $this->setMessage('El lead ya tiene una cita programada para el '. 
                Carbon::createFromFormat('Y-m-d H:i', $this->FECHA_CITA)->format("l j \\d\\e F"));
            return false;
        }

        // Validar horario disponible
        if (!$this->esDisponible($this->_ejecutivo,$this->_fecha,$this->_hora)){
            $this->setMessage('El ejecutivo ya tiene copado el horario seleccionado');
            return false;
        }
        
        $this->_ejecutivo = $lead->EN_REGISTRO;

        //Set de datos del histórico de movimiento de canal
        $canalhist = new CanalHistorico();
        $canalhist->setValues([
                '_periodo' => $this->_periodo,
                '_lead' => $this->_lead,
                '_canalOrigen' => $this->_tipo,
                '_canalDestino' => Canal::ASISTENTE_COMERCIAL,
                '_fechaActualizacion' => $hoy,
        ]);

        //Set de datos del histórico de citas
        $citaEstadoHist = new CitaEstadoHistorico();
        $citaEstadoHist->setValues([
            '_id' => $this->_id,
            '_periodo' => $this->_periodo,
            '_lead' => $this->_lead,
            '_estado' => $this->_estado,
            '_fechaCita' => $this->_fechaCita,
            '_fechaRegistro' => $hoy,
            '_usuario' => $this->_agendador
        ]);
        

        //Set de datos para actualizar el canal del Lead Campanha
        $leadCamp = new LeadCampanha();
        $leadCamp->setValues([
            '_periodo' => $this->_periodo,
            '_lead' => $this->_lead,
            '_fechaActualizacion' => $hoy->toDateTimeString(),
            '_canalActual' => Canal::EJECUTIVO_NEGOCIO,
        ]);


        //registrando
        $mCitas = new \App\Model\Citas();
        if ($mCitas->nuevo($this->setValueToTable(),$leadCamp->setValueToTable(),$canalhist->setValueToTable(),$citaEstadoHist->setValueToTable())){
            $this->setMessage('Cita agendada para el lead '. $lead->NOMBRE_CLIENTE . ' el ' . $this->_fechaCita->format("l j \\d\\e F"). ' a las '. $this->_fechaCita->format("H:i"));
            return true;
        }else{
            $this->setMessage('Error al registrar en la base de datos');
            return false;
        }
    }

    function actualizar(){
        $hoy = Carbon::now();
        $this->_horario = $this->getHorarioByHora($this->_hora);
        $this->_estado = CitaEstado::ACTUALIZADO;
        $this->_fechaCita = Carbon::createFromFormat('Y-m-d H:i', $this->_fecha . ' ' . $this->_hora);

        // Validar cita en dia de la semana y no es feriado
        if ($this->_fechaCita->isWeekend() or $this->esFeriado($this->_fechaCita)){
            $this->setMessage('El día seleccionado no es válido (fin de semana o feriado)');
            return false;
        }

        // Validar fecha reprogramación en el mismo mes
        $diasValidosCalendario = $this->getCalendario(15);
        if (!isset($diasValidosCalendario[$this->_fechaCita->format('Y-m-d')])){
            $this->setMessage('La cita debe ser programado como máximo para los siguientes 15 días');
            return false;
        }

        // Validar relacion asistente/lead para AC
        if ($this->_tipo != Canal::CALL_CENTER){
            $this->setMessage('El lead no está disponible para agendamiento de cita porque no pertenece al canal CALL');
            return false;
        }

        // Validar horario disponible
        if (!$this->esDisponible($this->_ejecutivo,$this->_fecha,$this->_hora,$this->_id)){
            $this->setMessage('El ejecutivo ya tiene copado el horario seleccionado');
            return false;
        }

        $citaEstadoHist = new CitaEstadoHistorico();
        $citaEstadoHist->setValues([
            '_id' => $this->_id,
            '_periodo' => $this->_periodo,
            '_lead' => $this->_lead,
            '_estado' => $this->_estado,
            '_fechaCita' => $this->_fechaCita,
            '_fechaRegistro' => $hoy,
            '_usuario' => $this->_agendador
        ]);

        // actualizar
        $mCitas = new \App\Model\Citas();
        if ($mCitas->reprogramar($this->_id,$this->setValueToTable(),$citaEstadoHist->setValueToTable())){
            $this->setMessage('Cita actualizada para el ' . $this->_fechaCita->format("l j \\d\\e F"). ' a las '. $this->_fechaCita->format("H:i"));
            return true;
        }else{
            $this->setMessage('Error al registrar en la base de datos');
            return false;
        }
    }

    function reprogramar($fechaReprogramacion){
        $hoy = Carbon::now();
        
        /****VALIDACIONES**/

        // Validar si ya ha sido reprogramada
        if ($this->_estado == CitaEstado::REPROGRAMADO){
            $this->setMessage('El día seleccionado no es válido (fin de semana o feriado)');
            return false;
        }


        // Validar cita en dia de la semana y no es feriado
        if ($this->_fechaCita->isWeekend() or $this->esFeriado($this->_fechaCita)){
            $this->setMessage('El día seleccionado no es válido (fin de semana o feriado)');
            return false;
        }

        // Validar fecha reprogramación en el mismo mes
        if ($hoy->month != $fechaReprogramacion->month || $hoy->year != $fechaReprogramacion->year || $hoy->diffInDays($fechaReprogramacion,false) < 0){
            $this->setMessage('La cita debe ser reprogramada con límites desde hoy hasta fin de mes');
            return false;
        }

        // Validar horario disponible
        if (!$this->esDisponible($this->_ejecutivo,$fechaReprogramacion->format('Y-m-d'),$fechaReprogramacion->format('H:i'),$this->_id)){
            $this->setMessage('El ejecutivo ya tiene copado el horario seleccionado');
            return false;
        }

        //Set de datos del histórico de citas
        $this->_fechaCita = $fechaReprogramacion;
        $this->_horario = $this->getHorarioByHora($fechaReprogramacion->format('H:i'));
        $this->_estado = CitaEstado::REPROGRAMADO;

        $citaEstadoHist = new CitaEstadoHistorico();
        $citaEstadoHist->setValues([
            '_id' => $this->_id,
            '_periodo' => $this->_periodo,
            '_lead' => $this->_lead,
            '_estado' => $this->_estado,
            '_fechaCita' => $this->_fechaCita,
            '_fechaRegistro' => $hoy,
            '_usuario' => $this->_agendador
        ]);

        // reprogramar cita
        $mCitas = new \App\Model\Citas();
        if ($mCitas->reprogramar($this->_id,$this->setValueToTable(),$citaEstadoHist->setValueToTable())){
            $this->setMessage('Cita reagendada para el ' . $this->_fechaCita->format("l j \\d\\e F"). ' a las '. $this->_fechaCita->format("H:i"));
            return true;
        }else{
            $this->setMessage('Error al registrar en la base de datos');
            return false;
        }
    }

    function getCitasEjecutivo($en){

        return [];
    }

    static function getSemanaEjecutivo($en){
        $today = Carbon::now();
        $mCitas = new mCitas();
        return $mCitas->getHorario(
            $en,
            $today->addDays(1)->toDateString(),
            $today->addDays(self::DIAS_HORARIO_EJECUTIVO)->toDateString()
            )->get();
    }

    static function getCitasPendientesByEjecutivo($en){
        $horario = new mCitas();
        return $horario->getHorario(
            $en,
            null, //sin inicio
            null, //sin fin
            null, //sin horario
            null, //sin excepcion cita
            CitaEstado::getEstadosSinGestionar()
            )->get();
    }

    static function getFormattedSemanaEjecutivo($citas){
        $semana = [];
        foreach ($citas as $cita){
            $semana[] = Carbon::createFromFormat('Y-m-d H:i:s', $cita->FECHA_CITA)->toDateString().'-'.$cita->TIPO_HORARIO;

        }
        return $semana;
    }

    static function getCalendarioEjecutivoAC(){
        return self::getCalendario(self::DIAS_CALENDARIO_AC);
    }

    static function getCalendarioEjecutivoCall(){
        return self::getCalendario(self::DIAS_CALENDARIO_CALL);
    }

    static function getCalendario($dias){
        $tomorrow = Carbon::now()->addDays(1);
        $i = 0; $calendario = [];
        while($i < $dias){
            if ($tomorrow->isWeekday() && !self::esFeriado($tomorrow)){
                $calendario[$tomorrow->toDateString()] = ucwords($tomorrow->format("j F"));
                $i++;
            }
            $tomorrow =  $tomorrow->addDays(1);
        }
        return $calendario;
    }

    static function getHorario(){
        return [
            self::HORARIO_1 => self::HORARIO_1_DESC,
            self::HORARIO_2 => self::HORARIO_2_DESC,
            self::HORARIO_3 => self::HORARIO_3_DESC
        ];
    }

    // FALTA FERIADOS
    static function esFeriado($fecha){
        
        $feriados = [
                '01-01', //año nuevo
                '05-01', //dia del trabajador
                '06-29', //san pedro
                '07-28', //fiesta patria
                '07-29', //fiesta patria
                '08-30', //santa rosa
                '10-08', //angamos
                '11-01', //muertos
                '12-08', //concepcion
                '12-25' //navidad
            ];

        return in_array($fecha->format('m-d'), $feriados); 
    }

    static function getHorasCitas(){
        return [
            '09:00' => '09:00',
            '09:30' => '09:30',
            '10:00' => '10:00',

            '11:00' => '11:00',
            '11:30' => '11:30',
            '12:00' => '12:00',
            '12:30' => '12:30',

            '15:00' => '15:00',
            '15:30' => '15:30',
            '16:00' => '16:00',
            '16:30' => '16:30',
        ];
    }

    static function esDisponible($en,$fecha,$hora,$cita = null){
        $horario = self::getHorarioByHora($hora);
        if ($en == '' || $fecha == '' || $hora == '' || $horario == '')
            return false;
        $tomorrow = Carbon::createFromFormat('Y-m-d', $fecha)->addDays(1)->toDateString();
        $citas = new mCitas();
        $citas_count = $citas->getHorario($en,$fecha,$tomorrow,$horario,$cita)->get();
        \Debugbar::info($citas);
        return count($citas_count)==0;
    }

    static function getHorarioByHora($hora){
        switch (substr($hora,0,2)) {
         case '09':
         case '10':
             return self::HORARIO_1;
         case '11':
         case '12':
             return self::HORARIO_2;
         case '15':
         case '16':
             return self::HORARIO_3;
        default:
            return '';
        } 
    }

    function getResumenCitas($rol, $busqueda, $zonal, $centro, $tienda) {

        $model = new mCitas();

        switch ($rol) {
            case Usuario::ROL_JEFE_CALL:
                $results = $model->getResumenByEjecutivo($busqueda['zonal'], $busqueda['centro'], $busqueda['tienda'], $busqueda['fechaIni'], $busqueda['fechaFin'])->paginate(self::ITEMS_PER_PAGE);
                break;

            case Usuario::ROL_ADMINISTRADOR:
                $results = $model->getResumenByEjecutivo($busqueda['zonal'], $busqueda['centro'], $busqueda['tienda'], $busqueda['fechaIni'], $busqueda['fechaFin'])->paginate(self::ITEMS_PER_PAGE);
                break;
            
            case Usuario::ROL_GERENTE_ZONA:
                $results = $model->getResumenByEjecutivo($zonal, $busqueda['centro'], $busqueda['tienda'], $busqueda['fechaIni'], $busqueda['fechaFin'])->paginate(self::ITEMS_PER_PAGE);
                break;

            case Usuario::ROL_GERENTE_CENTRO:
                $results = $model->getResumenByEjecutivo($zonal, $centro, $busqueda['tienda'], $busqueda['fechaIni'], $busqueda['fechaFin'])->paginate(self::ITEMS_PER_PAGE);
                break;

            case Usuario::ROL_GERENTE_TIENDA:
                $results = $model->getResumenByEjecutivo($zonal, $centro, $tienda, $busqueda['fechaIni'], $busqueda['fechaFin'])->paginate(self::ITEMS_PER_PAGE);
                break;

            default:
                $results = [];
                break;
        }

        return $results;
    }

    function getStatsCall() {
        $model = new mCitas();
        return $model->getStatsCall();
    }
}