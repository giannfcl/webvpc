<?php

namespace App\Entity;

use App\Model\Reportes as mReportes;
use Jenssegers\Date\Date as Carbon;

class Reportes extends \App\Entity\Base\Entity
{
    static function Reportes()
    {
        $reportes = mReportes::Reportes();
        return $reportes;
    }
    static function actualizar($datos)
    {
        return mReportes::actualizar($datos);
    }

    static function getInfoEditor($registro)
    {
    	$data=new mReportes();
    	return $data->getInfoEditor($registro);
    }

    function guardardatosdescarga($datos,$registro,$nombre)
    {
        $data = [
            'FECHA_HORA'=>Carbon::now(),
            'REGISTRO_EN'=>$registro,
            'NOMBRE_EN'=>$nombre,
            'NOMBRE_REPORTE'=>$datos['NOMBRE_REPORTE'],
            'NIVEL'=>$datos['NIVEL'],
            'CARTEGORIA'=>$datos['CATEGORIA']
        ];
        $modelo=new mReportes();
        return $modelo->guardardatosdescarga($data);
    }
}
