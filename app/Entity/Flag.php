<?php

namespace App\Entity;


class Flag extends \App\Entity\Base\Entity {
    
   	const FLAG_0 = 0;    
    const FLAG_1 = 1;

    const COLORES_FLAG = [self::FLAG_0 => '#D9534F',self::FLAG_1=>'#26B99A'];

    static function getHTML(){
        return [
            self::FLAG_0 => '<i id="alerta-'.self::FLAG_0.'" class="fas fa-times fa-lg" style="color:'.self::COLORES_FLAG[self::FLAG_0].'"></i>',
            self::FLAG_1 =>  '<i id="alerta-'.self::FLAG_1.'" class="fas fa-check fa-lg" style="color:'.self::COLORES_FLAG[self::FLAG_1].'"></i>',
        ];
    }

    static function getIconoFlag($flag=null){
        if ($flag==null) {
            return self::getHTML()[0];
        }
    	return self::getHTML()[$flag];
    }
}