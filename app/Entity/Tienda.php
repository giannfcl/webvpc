<?php

namespace App\Entity;

use App\Model\Tienda as mTienda;


class Tienda extends \App\Entity\Base\Entity {

    static function getZonales(){
        //FALTA CACHE
        $model = new mTienda();
        return $model->getZonales()->get();
    }

    static function getTiendas($centro = null, $zona = null){
        //FALTA CACHE
        $model = new mTienda();
        return $model->getTiendas($centro,$zona)->get();
    }

    static function getCentros($zona = null){
        //FALTA CACHE
        $model = new mTienda();
        return $model->getCentros($zona)->get();
    }

    static function getEjecutivos($tienda=null,$centro = null, $zona = null){
        //FALTA CACHE
        $model = new mTienda();
        return $model->getEjecutivos($tienda,$centro,$zona)->get();
    }

}