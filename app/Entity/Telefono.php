<?php

namespace App\Entity;

class Telefono {
    /* Los ubigeos top son aquellos que tienen la mayor cantidad de medicos - se traeran 5 */

    static function buscar($ruc,$razonSocial,$page = 1) {
        /* FALTA aplicar cache */
        $model = new \App\Model\Telefono();
        return $model->get($ruc, $razonSocial,$page);
    }

}
