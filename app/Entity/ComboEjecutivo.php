<?php

namespace App\Entity;

use App\Model\ComboEjecutivo as mComboEjecutivo;
use App\Entity\Usuario as Usuario;

class ComboEjecutivo extends \App\Entity\Base\Entity {
    CONST BE ="BE";
    CONST BEP ="BEP";
    CONST BEL ="BEL";
    CONST BPE ="BPE";

    CONST CHACARILLA ="CHACARILLA";
    CONST SANISIDRO ="SAN ISIDRO";
    CONST RICARDOPALMA ="RICARDO PALMA";
    CONST NORTE ="NORTE";
    CONST CENTRO ="CENTRO";
    CONST SUR ="SUR";

    CONST JEFATURASI ="JEFATURA SAN ISIDRO";

    CONST BELZONAL2_CODIGOS =[
        "BELZONAL1"=>"CHACARILLA",
        "BELZONAL2"=>"SAN ISIDRO",
        "BELZONAL3"=>"RICARDO PALMA"
    ];

    CONST JEFATURAS_COD = [
        "JEFE4"=>"MIRANDA ZAMBRANO JORGE ALFREDO",
        "JEFE1"=>"ARBULU ZUAZO INGRID ELIZABETH",
        "JEFE3"=>"JAVIER GONZALES VELARDE",
        "JEFE6"=>"RAVELLO JOO KARLA WENDY",
        "JEFE5"=>"ROBERTO CARLOS PALACIOS PANDO"
    ];

    static function getBancas($bpe = false,\App\Entity\Usuario $usuario = null){
        $model = new mComboEjecutivo();
        $banca = null;
        if ($usuario) {
            if ($usuario->getValue('_rol')==Usuario::ROL_JEFATURA_BE) {
                if (in_array($usuario->getValue('_zona'),["BELZONAL1","BELZONAL2","BELZONAL3"])) {
                    $banca = self::BEL;
                }
            }
            if (in_array($usuario->getValue('_rol'),[Usuario::ROL_EJECUTIVO_FARMER_BE,Usuario::ROL_EJECUTIVO_HUNTER_BE])) {
                if (in_array($usuario->getValue('_zona'),["BELZONAL1","BELZONAL2","BELZONAL3"])) {
                    $banca = self::BEL;
                }
            }
            if (in_array($usuario->getValue('_rol'),[Usuario::ROL_EJECUTIVO_FARMER_BE,Usuario::ROL_EJECUTIVO_HUNTER_BE])) {
                if (in_array($usuario->getValue('_zona'),["BEPZONAL1","BEPZONAL2","BEPZONAL3"])) {
                    $banca = self::BEP;
                }
            }
        }
        return $model->getBancas($bpe,$banca)->get();
    }
    
    static function getZonales($banca = null,\App\Entity\Usuario $usuario = null){
        //FALTA CACHE
        $model = new mComboEjecutivo();
        $zonal = null;
        if ($usuario) {
            if ($usuario->getValue('_rol')==Usuario::ROL_JEFATURA_BE) {
                if (in_array($usuario->getValue('_zona'),["BELZONAL1","BELZONAL2","BELZONAL3"])) {
                    $banca = self::BEL;
                    $zonal = self::BELZONAL2_CODIGOS[$usuario->getValue('_zona')];
                }
            }
            if (in_array($usuario->getValue('_rol'),[Usuario::ROL_EJECUTIVO_FARMER_BE,Usuario::ROL_EJECUTIVO_HUNTER_BE])) {
                if (in_array($usuario->getValue('_zona'),["BELZONAL1","BELZONAL2","BELZONAL3"])) {
                    $banca = ComboEjecutivo::BEL;
                    if (in_array($usuario->getValue('_zona'),["BELZONAL1"])) {
                        $zonal = ComboEjecutivo::CHACARILLA;
                    }
                    if (in_array($usuario->getValue('_zona'),["BELZONAL2"])) {
                        $zonal = ComboEjecutivo::SANISIDRO;
                    }
                    if (in_array($usuario->getValue('_zona'),["BELZONAL3"])) {
                        $zonal = ComboEjecutivo::RICARDOPALMA;
                    }
                }
            }
            if (in_array($usuario->getValue('_rol'),[Usuario::ROL_EJECUTIVO_FARMER_BE,Usuario::ROL_EJECUTIVO_HUNTER_BE])) {
                if (in_array($usuario->getValue('_zona'),["BEPZONAL1","BEPZONAL2","BEPZONAL3"])) {
                    $banca = ComboEjecutivo::BEP;
                    if (in_array($usuario->getValue('_zona'),["BEPZONAL1"])) {
                        $zonal = ComboEjecutivo::NORTE;
                    }
                    if (in_array($usuario->getValue('_zona'),["BEPZONAL2"])) {
                        $zonal = ComboEjecutivo::CENTRO;
                    }
                    if (in_array($usuario->getValue('_zona'),["BEPZONAL3"])) {
                        $zonal = ComboEjecutivo::SUR;
                    }
                }
            }
        }
        return $model->getZonales($banca,$zonal)->get();
    }
    
    static function getJefaturas($banca = null, $zonal = null,\App\Entity\Usuario $usuario = null) {
        //FALTA CACHE
        $jefatura = null;
        if ($usuario) {
            if (in_array($usuario->getValue('_rol'),[Usuario::ROL_EJECUTIVO_FARMER_BE,Usuario::ROL_EJECUTIVO_HUNTER_BE,Usuario::ROL_JEFATURA_BE])) {
                if (in_array($usuario->getValue('_zona'),["BELZONAL2"])) {
                    $jefatura = self::JEFATURAS_COD[$usuario->getValue('_centro')];
                }
            }
        }
        $model = new mComboEjecutivo();
        return $model->getJefaturas($banca, $zonal,$jefatura)->get();
    }
    
    static function getEjecutivos($banca = null, $zonal = null, $jefatura = null,\App\Entity\Usuario $usuario = null) {
        //FALTA CACHE
        $model = new mComboEjecutivo();
        return $model->getEjecutivos($banca, $zonal, $jefatura)->get();
    }
    
    static function getFecha() {
        //FALTA CACHE
        $model = new mComboEjecutivo();
        return $model->getFecha()->get();
    }
    
    static function getNombreEncargado($ejecutivo = null) {
        //FALTA CACHE
        $model = new mComboEjecutivo();
        return $model->getNombreEncargado($ejecutivo)->get();
        //return null;
    }

    static function getByCodsectU($ejecutivo = null) {
        $model = new mComboEjecutivo();
        return $model->getByCodsectU($ejecutivo)->get()->first();
    }

}