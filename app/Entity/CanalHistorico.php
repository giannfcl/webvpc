<?php

namespace App\Entity;

class CanalHistorico extends \App\Entity\Base\Entity {

	protected $_periodo;
	protected $_lead;
	protected $_canalOrigen;
	protected $_canalDestino;
	protected $_fechaActualizacion;
	protected $_usuario;


	function setValueToTable() {
        return $this->cleanArray([
	        'PERIODO' => $this->_periodo,
	        'NUM_DOC' => $this->_lead,
	        'CANAL_ORIGEN' => $this->_canalOrigen,
	        'CANAL_DESTINO' => $this->_canalDestino,
	        'REGISTRO' => $this->_usuario,
	        'FECHA_ACTUALIZACION' => ($this->_fechaActualizacion)? $this->_fechaActualizacion->toDateTimeString() : null,
        ]);
    }
}