<?php

namespace App\Entity;

use App\Model\HeatAnalytic as mHeatAnalytic;

class HeatAnalytic extends \App\Entity\Base\Entity
{
    static function insertHA($datos, $registro)
    {
        mHeatAnalytic::insertHA($datos, $registro);
    }
}
