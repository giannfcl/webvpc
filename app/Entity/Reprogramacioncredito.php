<?php

namespace App\Entity;

use Jenssegers\Date\Date as Carbon;
use App\Model\Reprogramacioncredito as mReprogramacioncredito;

class Reprogramacioncredito extends \App\Entity\Base\Entity {
    
    const PAGARE_TASA_CERO = 1;
    const DIAS_GRACIA = 2;
    const REVERTIR = 6;

    const EJECUTIVO_GESTION_MANTENER_CRONOGRAMA = 1;
    const EJECUTIVO_GESTION_REVERTIR_CRONOGRAMA_ORIGINAL = 10;
    const EJECUTIVO_GESTION_ACEPTAR_OPCION1 = 11;
    const EJECUTIVO_GESTION_ACEPTAR_OPCION2 = 12;
    const EJECUTIVO_GESTION_ACEPTAR_OPCION_RIESGOS = 13;
    const EJECUTIVO_GESTION_ENVIAR_RIESGOS = 14;


    const RIESGOS_GESTION_PENDIENTE = 1;
    const RIESGOS_GESTION_EDITAR_RESPUESTA = 2;
    const RIESGOS_GESTION_RECHAZAR = 3;

    const RIESGOS_GESTION_ENVIAR_PROPUESTA_EJECUTIVO = 1;

    static function getListaRP($data,$user){
        $model=new mReprogramacioncredito();
        if (in_array($user->getValue('_rol'),[Usuario::ROL_EJECUTIVO_NEGOCIO,Usuario::ROL_CALL])){
            return $model->getListaRP($data,$user->getValue('_registro'));
        }else if (in_array($user->getValue('_rol'),[Usuario::ROL_RIESGO_BPE])){
            return $model->getListaRP($data,null,$user->getValue('_registro'));
        }else if (in_array($user->getValue('_rol'),[Usuario::ROL_ANALISTA_RIESGOS_COBRANZA])){
            return $model->getListaRP($data,null,null,true);
        }else if (in_array($user->getValue('_rol'),[Usuario::ROL_GESTOR_COBRANZA_BPE])){
            return $model->getListaRP($data);
        }else{
            return $model->getListaRP($data);
        }
    }

    function gestionar($data,$user)
    {
        $model=new mReprogramacioncredito();

        $credito = $model->getListaRP($data,null)->first();
        //dd($data,$user,$credito->GESTION_RESPUESTA);
        // SI GESTIONA RIESGOS
        if(in_array($user->getValue('_rol'),[Usuario::ROL_RIESGO_BPE,Usuario::ROL_ANALISTA_RIESGOS_COBRANZA])){
            $actualizar["RIESGOS_GESTION"] = $data['mrespuesta'];

            $gestion["PROPUESTA_RIESGOS_COMENTARIOS"]=isset($data['rcomentario'])?$data['rcomentario']:null;
            $gestion["RIESGOS_GESTION"]=$actualizar["RIESGOS_GESTION"];
            $gestion["GESTION_ENCARGADO"]=$user->getValue('_registro');
            $gestion["COD_CREDITO"]=$data['mcodcredito'];
            $gestion["GESTION_FECHA"] = Carbon::now();

            $actualizar["PROPUESTA_RIESGOS_COMENTARIOS"]=$gestion["PROPUESTA_RIESGOS_COMENTARIOS"];
            // SI LE DA ENVIAR PROPUESTA
            if( in_array($actualizar["RIESGOS_GESTION"],[self::RIESGOS_GESTION_EDITAR_RESPUESTA,self::RIESGOS_GESTION_PENDIENTE]) ){
                if ($actualizar["RIESGOS_GESTION"] == self::RIESGOS_GESTION_EDITAR_RESPUESTA) {
                    $actualizar["GESTION_RESPUESTA"] = NULL;
                    $actualizar['OPCION_PAGO_RIESGOS_FLAG'] = self::RIESGOS_GESTION_ENVIAR_PROPUESTA_EJECUTIVO;
                }
                $actualizar["PROPUESTA_RIESGOS_MGRACIA"] = isset($data['propuestagracia'])?$data['propuestagracia']:null;
                $actualizar["PROPUESTA_RIESGOS_PRIMERAS_CUOTAS"] = isset($data['propuestaprimcuotas'])?$data['propuestaprimcuotas']:null;
                $actualizar["PROPUESTA_RIESGOS_RESTO_CUOTAS"] = isset($data['propuestarestocuotas'])?$data['propuestarestocuotas']:null;
                $actualizar["PROPUESTA_RIESGOS_UNA_CUOTA_ADICIONAL_BALL"] = isset($data['propuestacuotaadicball'])?$data['propuestacuotaadicball']:null;
                $actualizar['PROPUESTA_RIESGOS_CUOTAS_ADICIONALES'] = isset($data['propuestacuotaadic'])?$data['propuestacuotaadic']:null;
                $actualizar["PROPUESTA_RIESGOS_TOTAL_CUOTAS"] = isset($data['propuestatotalcuotas'])?$data['propuestatotalcuotas']:null;
                $actualizar["PROPUESTA_RIESGOS_TIPO"] = isset($data['dtipocuota'])?$data['dtipocuota']:null;
            }
        }else{
            // SI GESTIONA EJECUTIVO U OTRO
            //dd($data);
            $actualizar["GESTION_RESPUESTA"] = $data['mrespuesta'];
            $actualizar["SUBGESTION_RESPUESTA"]=$data['msubgestion'];

            $gestion["GESTION_RESPUESTA"]=$actualizar["GESTION_RESPUESTA"];
            $gestion["GESTION_COMENTARIO"]=isset($data['gescomentario'])?$data['gescomentario']:null;
            $gestion["GESTION_ENCARGADO"]=$user->getValue('_registro');
            $gestion["COD_CREDITO"]=$data['mcodcredito'];
            $gestion["GESTION_FECHA"] = Carbon::now();
            $actualizar["GESTION_COMENTARIO"]=$gestion["GESTION_COMENTARIO"];
            
            // SI ES ENVIAR A RIESGOS CREAR FUNCION QUE TRAIGA AL ANALISTA MAS LIBRE
            if ($actualizar["GESTION_RESPUESTA"] == self::EJECUTIVO_GESTION_ENVIAR_RIESGOS){
                //Busco si es que ya tiene un analista riesgos asignado previamente
                $ejecutivo = $model->getEjecutivoActual($gestion["COD_CREDITO"]);
                // Si no tiene un ejecutivo asignado
                $actualizar["RIESGOS_GESTION"] = self::RIESGOS_GESTION_PENDIENTE;
                $actualizar["RIESGOS_FECHA_ASIGNACION"] = Carbon::now();
                if (!$ejecutivo){
                    $ejecutivo = $model->getEjecutivoMenorCarga();
                    $actualizar["RIESGOS_RESPONSABLE"] = $ejecutivo->REGISTRO_RIESGOS;
                }else{
                    $actualizar["RIESGOS_RESPONSABLE"] = $ejecutivo->RIESGOS_RESPONSABLE;
                }
                // dd($ejecutivo);
                //CAMPOS ENVIADOS A DERIVAR A RIESGOS
                $actualizar["DERIVADO_RIESGOS_MGRACIA"] = isset($data['driesgosgracia'])?$data['driesgosgracia']:null;
                $actualizar["DERIVADO_RIESGOS_PRIMERAS_CUOTAS"] = isset($data['driesgosprimcuotas'])?$data['driesgosprimcuotas']:null;
                $actualizar["DERIVADO_RIESGOS_RESTO_CUOTAS"] = isset($data['driesgosrestocuotas'])?$data['driesgosrestocuotas']:null;
                $actualizar["DERIVADO_RIESGOS_UNA_CUOTA_ADICIONAL_BALL"] = isset($data['driesgoscuotaadicball'])?$data['driesgoscuotaadicball']:null;
                $actualizar['DERIVADO_RIESGOS_CUOTAS_ADICIONALES'] = isset($data['driesgoscuotaadic'])?$data['driesgoscuotaadic']:null;
                $actualizar["DERIVADO_RIESGOS_TOTAL_CUOTAS"] = isset($data['driesgostotalcuotas'])?$data['driesgostotalcuotas']:null;
            }
        }
        
        //dd($data,$actualizar,$gestion);
        
        return $model->gestionar($data,$actualizar,$gestion);
    }

    static function getRP($data,$user){
        $model=new mReprogramacioncredito();
        $rep = $model->getListaRP($data,null)->first();

        //Cuantas opciones de pago tiene disponible
        $rep->opcionesPago = (int)$rep->OPCION_PAGO_1_FLAG + (int)$rep->OPCION_PAGO_2_FLAG + (int)$rep->OPCION_PAGO_RIESGOS_FLAG;

        //las opciones de respuesta dependen de:
        if (in_array($user->getValue('_rol'),[Usuario::ROL_RIESGO_BPE,Usuario::ROL_ANALISTA_RIESGOS_COBRANZA])){
            $rep->opcionesResultado = [
                self::RIESGOS_GESTION_PENDIENTE => 'Pendiente', 
                self::RIESGOS_GESTION_EDITAR_RESPUESTA => 'Aprobar', 
                self::RIESGOS_GESTION_RECHAZAR => 'Rechazar'];
        }else if($rep->RIESGOS_GESTION == self::RIESGOS_GESTION_EDITAR_RESPUESTA){
            $rep->opcionesResultado = [
                self::EJECUTIVO_GESTION_MANTENER_CRONOGRAMA => 'Aceptar Cronograma Actual', 
                self::EJECUTIVO_GESTION_REVERTIR_CRONOGRAMA_ORIGINAL => 'Revertir Cronograma Original', 
                self::EJECUTIVO_GESTION_ACEPTAR_OPCION_RIESGOS => 'Aceptar Opción de Riesgos', 
            ];

            ($rep->OPCION_PAGO_1_FLAG)? $rep->opcionesResultado[self::EJECUTIVO_GESTION_ACEPTAR_OPCION1] = 'Aceptar Opción de Pago 1' : null;
            ($rep->OPCION_PAGO_2_FLAG)? $rep->opcionesResultado[self::EJECUTIVO_GESTION_ACEPTAR_OPCION2] = 'Aceptar Opción de Pago 2' : null;
        }else{
            if (!empty($rep->TIPO_FUENTE) && $rep->TIPO_FUENTE =="CIMA") {
                $rep->opcionesResultado = [
                    self::EJECUTIVO_GESTION_MANTENER_CRONOGRAMA => 'Aceptar Cronograma Actual', 
                    self::EJECUTIVO_GESTION_REVERTIR_CRONOGRAMA_ORIGINAL => 'Revertir Cronograma Original', 
                ];   
            }else{
                $rep->opcionesResultado = [
                    self::EJECUTIVO_GESTION_MANTENER_CRONOGRAMA => 'Aceptar Cronograma Actual', 
                    self::EJECUTIVO_GESTION_REVERTIR_CRONOGRAMA_ORIGINAL => 'Revertir Cronograma Original', 
                    self::EJECUTIVO_GESTION_ENVIAR_RIESGOS => 'Derivar a Riesgos', 
                ];
            }

            ($rep->OPCION_PAGO_1_FLAG)? $rep->opcionesResultado[self::EJECUTIVO_GESTION_ACEPTAR_OPCION1] = 'Aceptar Opción de Pago 1' : null;
            ($rep->OPCION_PAGO_2_FLAG)? $rep->opcionesResultado[self::EJECUTIVO_GESTION_ACEPTAR_OPCION2] = 'Aceptar Opción de Pago 2' : null;
        }

        if($rep->OPCION_PAGO_1_FLAG==1){
            $rep->OP1_TXT = "<b>Opcion 1 - ".$rep->OPCION_PAGO_1."</b>";
            switch ($rep->OPCION_PAGO_1) {
                case "CAMBIO DE FECHA":
                    $rep->OP1_TXT .= "<ul>
                        <li>Máximo Meses de Gracia: ".  $rep->OPCION_PAGO_1_MESES_GRACIA ."</li>
                    </ul>";
                    break;
                case "CRONOGRAMA ESPECIAL":
                    $rep->OP1_TXT .= "<ul>
                        <li>Meses de Gracia: ".  $rep->OPCION_PAGO_1_MESES_GRACIA ."</li>
                        <li>Primeras Cuotas '50% Cuota': ".  $rep->OPCION_PAGO_1_CUOTAS_SOLO_INTERES ."</li>
                        <li>Resto Cuotas Flat: ".  $rep->OPCION_PAGO_1_CUOTAS_FLAT ."</li>
                    </ul>";
                    break;
                case "CUOTA BALLÓN":
                    $rep->OP1_TXT .= "<ul>
                        <li>Meses de Gracia: ".  $rep->OPCION_PAGO_1_MESES_GRACIA ."</li>
                        <li>Primeras Cuotas '50% Cuota': ".  $rep->OPCION_PAGO_1_CUOTAS_SOLO_INTERES ."</li>
                        <li>Resto Cuotas Flat: ".  $rep->OPCION_PAGO_1_CUOTAS_FLAT ."</li>
                        <li>1 Cuota adicional final (Balloon): ".  $rep->OPCION_PAGO_1_CUOTA_BALLON ."</li>
                    </ul>";
                    break;
                case "PAGO":
                    $rep->OP1_TXT .= "";
                    break;
                case "REESTRUCTURACIÓN":
                    $rep->OP1_TXT .= "<ul>
                        <li>Meses de Gracia: ".  $rep->OPCION_PAGO_1_MESES_GRACIA ."</li>
                        <li>Total Cuotas: ".  $rep->OPCION_PAGO_1_TOTAL_CUOTAS ."</li>
                    </ul>";
                    break;
                case "REPROGRAMACIÓN":
                    $rep->OP1_TXT .= "<ul>
                        <li>Máximo Meses de Gracia: ".  $rep->OPCION_PAGO_1_MESES_GRACIA ."</li>
                        <li>Máximo Cuotas Adicionales: ".  $rep->OPCION_PAGO_1_CUOTAS_ADICIONALES ."</li>
                    </ul>";
                    break;
                default:
                    $rep->OP1_TXT .= "";
                    break;
            }
        }

        if($rep->OPCION_PAGO_2_FLAG==1){
            $rep->OP2_TXT = "<b>Opcion 2 - ".$rep->OPCION_PAGO_2."</b>";
            switch ($rep->OPCION_PAGO_2) {
                case "CAMBIO DE FECHA":
                    $rep->OP2_TXT .= "<ul>
                        <li>Máximo Meses de Gracia: ".  $rep->OPCION_PAGO_2_MESES_GRACIA ."</li>
                    </ul>";
                    break;
                case "CRONOGRAMA ESPECIAL":
                    $rep->OP2_TXT .= "<ul>
                        <li>Meses de Gracia: ".  $rep->OPCION_PAGO_2_MESES_GRACIA ."</li>
                        <li>Primeras Cuotas '50% Cuota': ".  $rep->OPCION_PAGO_2_CUOTAS_SOLO_INTERES ."</li>
                        <li>Resto Cuotas Flat: ".  $rep->OPCION_PAGO_2_CUOTAS_FLAT ."</li>
                    </ul>";
                    break;
                case "CUOTA BALLÓN":
                    $rep->OP2_TXT .= "<ul>
                        <li>Meses de Gracia: ".  $rep->OPCION_PAGO_2_MESES_GRACIA ."</li>
                        <li>Primeras Cuotas '50% Cuota': ".  $rep->OPCION_PAGO_2_CUOTAS_SOLO_INTERES ."</li>
                        <li>Resto Cuotas Flat: ".  $rep->OPCION_PAGO_2_CUOTAS_FLAT ."</li>
                        <li>1 Cuota adicional final (Balloon): ".  $rep->OPCION_PAGO_2_CUOTA_BALLON ."</li>
                    </ul>";
                    break;
                case "PAGO":
                    $rep->OP2_TXT .= "";
                    break;
                case "CONSOLIDACIÓN":
                case "REESTRUCTURACIÓN":
                    $rep->OP2_TXT .= "<ul>
                        <li>Meses de Gracia: ".  $rep->OPCION_PAGO_2_MESES_GRACIA ."</li>
                        <li>Total Cuotas: ".  $rep->OPCION_PAGO_2_TOTAL_CUOTAS ."</li>
                    </ul>";
                    break;
                case "REPROGRAMACIÓN":
                    $rep->OP2_TXT .= "<ul>
                        <li>Máximo Meses de Gracia: ".  $rep->OPCION_PAGO_2_MESES_GRACIA ."</li>
                        <li>Máximo Cuotas Adicionales: ".  $rep->OPCION_PAGO_2_CUOTAS_ADICIONALES ."</li>
                    </ul>";
                    break;
                default:
                    $rep->OP2_TXT .= "";
                    break;
            }
        }

        return $rep;
        

    }

    function editgestionar($data,$user)
    {
        // dd($data,$user);
        $actualizar=[
            "GESTION_RESPUESTA"=>$data['erespuesta'],
            "GESTION_ESTADO"=>isset($data['eestado'])?$data['eestado']:null,
            "GESTION_TIPO"=>isset($data['etipo'])?$data['etipo']:null,
            "GESTION_FECHA"=>Carbon::now(),
            "GESTION_ENCARGADO"=>$user,
            "GESTION_COMENTARIO"=>isset($data['egescomentario'])?$data['egescomentario']:null,
            "GESTION_PERIODO_GRACIA"=>isset($data['egestionperiodo'])?$data['egestionperiodo']:null,
            "GESTION_PLAZO_ADICIONAL"=>isset($data['egestionplazo'])?$data['egestionplazo']:null,
            "GESTION_TELEFONO"=>isset($data['egestiontelefono'])?$data['egestiontelefono']:null,
            "GESTION_EMAIL"=>isset($data['egestionemail'])?$data['egestionemail']:null,
            "GESTION_PLAZO_PAGARE"=>isset($data['eplazopagare'])?$data['eplazopagare']:null
        ];
        
        $ges["COD_CREDITO"]=$data['ecodcredito'];
        $gestion=array_merge($ges, $actualizar);
        // dd($gestion,$actualizar);
        $model=new mReprogramacioncredito();
        return $model->gestionar($data,$actualizar,$gestion);
    }

    

    function getEstadosGestionRC()
    {
        $model=new mReprogramacioncredito();
        return $model->getEstadosGestionRC();
    }
}