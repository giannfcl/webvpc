<?php

namespace App\Entity\Infinity;


class ConocemeOpciones extends \App\Entity\Base\Entity {

    static function getBacklogTexto(){
        return "Backlog: Empresa que se maneja mediante contratos de prestación de bienes o servicios únicos durante un periodo de tiempo determinado. Esto puede ser de dos tipos:
        <br/><br/>
Backlog de Proyectos: Empresa que mantiene lista de contratos en donde el bien o servicio es distinto para cada tipo de cliente, mostrando volatilidad en el registro de facturación debido a la entrega parcial del objeto del contrato.  
Ejemplo: Construcción del Hospital Dos de Mayo y Construcción del Hospital Alcides Carrión, la empresa construye hospitales pero cada hospital es un proyecto distinto.
implementación de un software a medida, construcción de estructuras metalmecánicas, servicios de supervisión, consultoría de proyectos, implementación de torres de alta tensión, entre otros.
 <br/><br/>
Backlog de Servicios: Empresa que mantiene lista de contratos en donde el bien o servicio es homogéneo para cada tipo de cliente y permite prever una estabilidad en las ventas del cliente.
Ejemplo: Alquiler de maquinaria, transporte de personal, entre otros.";
    }

    static function getActividades(){
        return ['Comercio','Servicios','Industria','Transporte','Construcción','Publicidad','Agrícola y Ganadero','Real Estate','Pesca','Minería','Energía'];
    }

    static function getSubsectores(){
        return ['Abarrotes','Acondicionamiento de Edificios','Agencia de Aduanas y servicios relacionados','Agrícola (Granos y Frutos)','Agrícola (Siembra)','Agua, hielo y derivados de agua','Alquiler de equipos','Artículos de acero','Artículos de limpieza','Artículos de lujo','Artículos de oficina','Artículos diversos','Artículos diversos','Bebidas alcohólicas','Catering','Clínicas','Construcción de Carreteras','Construcción de Edificios','Empresa de Transmisión','Equipos','Equipos computo','Estación de Servicio','Estudio Legal y contable','Fundición y Metales','Ganadero','Generadora','Gestión Empresarial','Hidrocarburos','Hotelería','Imprenta y papel','Juguetes','Limpieza','Madera','Mantenimiento','Maquinaria pesada y/o amarilla','Maquinaria verde','Materiales de construcción y ferretería','Medicamentos y equipos médicos','Metalmecánica','Minerales metálicos','Minería','Obras civiles','Plástico y derivados','Productos Eléctricos','Productos Marinos','Publicidad','Químicos','Real Estate','Restaurante y eventos sociales','Saneamiento','Servicios De Ingeniería','Servicios diversos','Servicios financieros','Subcontratistas mineros','Taller Automotriz','Tecnología y Telecomunicaciones','Textil y calzado','Transporte de Carga','Transporte de personal','Transporte interprovincial','Transporte turístico','Transporte Urbano','Turismo','Vehículos','Vehículos, repuestos y accesorios','Vigilancia y Seguridad Privada'];   
    }

    static function getBacklogs(){
        return ['Proyectos','Servicios','No Aplica'];       
    }

    static function getParticipacion(){
        return [ 4=>'Mayor 75%',3=>'50% - 75%',2=>'20% - 50%',1 =>'Menor 20%'];
    }

     static function getGestionesCompra(){
        return ['Contra órdenes de pedido','Me anticipo a las licitaciones','Manejo stock de seguridad','Otros'];
    }

    static function getFinancieroRol(){
        return ['G. Financiero','Asesor Finan.','Gerente General','Accionista'];
    }
    static function getZonas(){
        return ['Nacional','Exterior','Norte','Centro','Sur','Lima','Callao','Amazonas','Ancash','Apurimac','Arequipa','Ayacucho','Cajamarca','Cusco','Huancavelica','Huanuco','Ica','Junin','La Libertad','Lambayeque','Loreto','Madre de Dios','Moquegua','Pasco','Piura','Puno','San Martín','Tacna','Tumbes','Ucayali'];
    }
    
    static function getInversiones(){
        return ['Terreno','Planta/Almacén','Construcción/Remodelación','Implementación Nueva Línea','Inversiones en Relacionadas','Maquinaria','Equipos Diversos','Vehículos','Otros'];
    }

    static function getCargosEntrevistado(){
        return ['Accionista','Gerente General','Gerente Financiero'];  
    }

    static function getBancos(){
        return ['IBK' => 'IBK', 'BCP' => 'BCP','BBVA' => 'BBVA','SCOTIA' => 'SCOTIA','BIF' => 'BANBIF','FIN' => 'PICHINCHA','HSBC' => 'HSBC'];
    }

    static function getTiposGarantia(){
        return ['Casa','Terreno','Local','Stand By','Certificado Bancario'];
    }

    static function getCommodities(){
        return ['Acero','Algodón','Petróleo','Palta','Uva','Banano','Oro','Plata','Cobre','Polipropileno','Polietileno','Harina de Pescado','Quinua','Maiz','Otros'];
    }
}