<?php

namespace App\Entity\Infinity;

use Jenssegers\Date\Date as Carbon;
use App\Model\Infinity\Cliente as modelCliente;

class Control extends \App\Entity\Base\Entity {

    protected $_periodo;
    protected $_codUnico;
    protected $_registro;
    protected $_fechaRegistro;
    protected $_comentario;
    protected $_flgInfinityCambio;
    protected $_flgUltimo;   
    
 
    function setValueToTable() {
        $data = [
            'PERIODO' => $this->_periodo,
            'COD_UNICO' => $this->_codUnico,
            'REGISTRO' => $this->_registro,            
            'FECHA_REGISTRO' => $this->_fechaRegistro->format('Y-m-d H:i:s'),
            'COMENTARIO' => $this->_comentario,
            'FLG_INFINITY_CAMBIO' => $this->_flgInfinityCambio,
            'FLG_ULTIMO' => $this->_flgUltimo,

        ];
        return $this->cleanArray($data);
    }

    function registrar() {
        $model = new modelCliente();
        $hoy = Carbon::now();
        $this->_periodo = self::sgetPeriodoInfinity();
        $this->_fechaRegistro = $hoy;      
        $this->_flgUltimo = 1;
        return $model->guardarControl($this->setValueToTable());
    }
    

    
    

}
