<?php

namespace App\Entity\Infinity;

use App\Model\TestPolitica as mPoliticas;
use App\Model\Infinity\Cliente as modelCliente;
use App\Model\Usuario as mUsuario;
use App\Entity\Infinity\Visita as Visita;
use Jenssegers\Date\Date as Carbon;
use App\Entity\Infinity\ConocemeCliente as ConocemeCliente;
use App\Entity\Notificacion as Notificacion;

class PoliticaProceso extends \App\Entity\Base\Entity {

    //CONSTANTES LOGO
    const LOGO_1 = 1;
    const LOGO_2 = 2;
    const LOGO_3 = 3;
    //FECHAS NO APLICABLES A EEFF (NOVIEMBRE, DICIEMBRE)
    // const FECHAS_NO_APLICA = [09, 10];
    //
    const INCOMPLETO = 'I';
    const COMPLETO = 'C';
    //CONSTANTES LOGO CLIENTE

    const MONTO_VENCIDO = 15000;
    const DEUDA_LABORAL_COACTIVA = 50000;
    const PEOR_CLASIFICACION = 0;
    const NUEVO_PEOR_CLASIFICACION = 1;
    const PENDIENTE='PENDIENTE';
    const VENCIDA='VENCIDA';

    protected $_clientesPoliticas = [];

    //Trae las politicas vigentes de un cliente
    //TODO: revisar si esta funcion puede reemplazar getPoliticasClientesInfinity

    #function convertirPoliticaNotificacion($politica){
    #}

    function getPoliticasCliente($codunico,$flgInfinity = true) {
        // considerar de que $codunico puede ser string o array
        // independiente del caso el array debe tener este formato:
        // ['4541231' => [{politica1},{politica2}]]
        // ['2342344' => [{politica3},{politica5}]]
        $politicacliente = mPoliticas::getPoliticasInfinity(self::sgetPeriodoInfinity(), $codunico,$flgInfinity)->get();

        //$clientepolitica=[];
        foreach ($politicacliente as $key => $value) {
            if (!empty($value->ID_POLITICA_CLIENTE)) {
                $clientepolitica[$value->COD_UNICO][] = $value;
            } else {
                $clientepolitica[$value->COD_UNICO] = [];
            }
        }
        return (isset($clientepolitica) ? $clientepolitica : []);
    }

    //Trae las politicas vigentes de todos los clientes infinity. si cliente no tiene politica igual lo trae (left join)
    function getPoliticasClientesInfinity($hayseleccionados=false) {
        // considerar de que $codunico puede ser string o array
        // independiente del caso el array debe tener este formato:
        // ['4541231' => [{politica1},{politica2}]
        // '2342344' => [{politica4},{politica5}]]
        $politicas = mPoliticas::getPoliticasInfinity(self::sgetPeriodoInfinity(),null,true,$hayseleccionados)->get();
        $clientepolitica = [];

        foreach ($politicas as $key => $value) {
            if (!empty($value->ID_POLITICA_CLIENTE)) {
                $clientepolitica[$value->COD_UNICO][] = $value;
            } else {
                $clientepolitica[$value->COD_UNICO] = [];
            }
        }
        return $clientepolitica;
    }


    //Politicas cualitativas
    function politicaCualitativa($cliente, $original,$visita=null) {

        $cod_unico = $cliente->getValue('_codunico');
        $clientesPoliticas = $this->getPoliticasCliente($cod_unico);
        $politicas_cualitativas = [];
        if (is_array($clientesPoliticas)) {
            foreach ($clientesPoliticas as $clientes => $clientePolitica) {
                foreach ($clientePolitica as $i => $p) {
                    if ($p->ORIGEN == Politica::POLITICA_TIPO_CUALITATIVA) {
                        $politicas_cualitativas[$clientes][$i] = $p;
                    }
                }
            }
        }

        echo '$clientesPoliticas: <pre>';
        print_r($clientesPoliticas);
        echo '</pre>';
        $politicas_agregar = [];
        $politicas_archivar = [];
        $politicas_actualizar = [];

        $flg_cambio_clientes = false;
        $flg_cambio_proveedores = false;
        $flg_cambio_gerencia = false;
        $flg_cambio_accionista = false;
        $flg_inv50 = false;
        $flg_prest_desv = false;
        $flg_cambio_modelo_neg = false;

        if ($this->politicaCambioClientes($cliente, $original)) {
            //si es que ya tiene
            if (is_array($clientesPoliticas)) {
                foreach ($clientesPoliticas as $c => $clientePolitica) {
                    foreach ($clientePolitica as $i => $p) {
                        if ($p->POLITICA_ID == Politica::POLITICA_CAMBIO_CON_CLIENTES) {
                            $flg_cambio_clientes = true;
                        }
                    }
                }
            }
            if ($flg_cambio_clientes != true) {
                $objPolitica = new PoliticaCliente();
                $objPolitica->setValue('_codunico', $cod_unico);
                $objPolitica->setValue('_fechaCaida', Carbon::now());
                $objPolitica->setValue('_flgVigente', 1);
                $objPolitica->setValue('_politica', Politica::POLITICA_CAMBIO_CON_CLIENTES);
                $objPolitica->setValue('_nivel', Politica::get(Politica::POLITICA_CAMBIO_CON_CLIENTES)->NIVEL_1);
                $objPolitica->setValue('_diasGestion', Politica::get(Politica::POLITICA_CAMBIO_CON_CLIENTES)->DIAS_GESTION);
                $objPolitica->setValue('_estadoFinalGestion', self::PENDIENTE);

                $politicas_agregar[] = $objPolitica->setValueToTable();
                $clientesPoliticas[$cod_unico][] = $objPolitica->getObjeto();
            }
        } else {
            if (is_array($clientesPoliticas)) {
                foreach ($clientesPoliticas as $c => $clientePolitica) {
                    foreach ($clientePolitica as $i => $p) {
                        if ($p->POLITICA_ID == Politica::POLITICA_CAMBIO_CON_CLIENTES) {
                            $politicas_archivar[] = $p->ID_POLITICA_CLIENTE;
                            unset($clientesPoliticas[$c][$i]);
                        }
                    }
                }
            }
        }

        if ($this->politicaCambioProveedores($cliente, $original)) {
            //si es que ya tiene
            if (is_array($clientesPoliticas)) {
                foreach ($clientesPoliticas as $c => $clientePolitica) {
                    foreach ($clientePolitica as $i => $p) {
                        if ($p->POLITICA_ID == Politica::POLITICA_CAMBIO_CON_PROVEEDORES) {
                            $flg_cambio_proveedores = true;
                        }
                    }
                }
            }
            if ($flg_cambio_proveedores != true) {
                $objPolitica = new PoliticaCliente();
                $objPolitica->setValue('_codunico', $cod_unico);
                $objPolitica->setValue('_fechaCaida', Carbon::now());
                $objPolitica->setValue('_flgVigente', 1);
                $objPolitica->setValue('_politica', Politica::POLITICA_CAMBIO_CON_PROVEEDORES);
                $objPolitica->setValue('_nivel', Politica::get(Politica::POLITICA_CAMBIO_CON_PROVEEDORES)->NIVEL_1);
                $objPolitica->setValue('_diasGestion', Politica::get(Politica::POLITICA_CAMBIO_CON_PROVEEDORES)->DIAS_GESTION);
                $objPolitica->setValue('_estadoFinalGestion', self::PENDIENTE);

                $politicas_agregar[] = $objPolitica->setValueToTable();
                $clientesPoliticas[$cod_unico][] = $objPolitica->getObjeto();
            }
        } else {
            if (is_array($clientesPoliticas)) {
                foreach ($clientesPoliticas as $c => $clientePolitica) {
                    foreach ($clientePolitica as $i => $p) {
                        if ($p->POLITICA_ID == Politica::POLITICA_CAMBIO_CON_PROVEEDORES) {
                            $politicas_archivar[] = $p->ID_POLITICA_CLIENTE;
                            unset($clientesPoliticas[$c][$i]);
                        }
                    }
                }
            }
        }

        if ($this->politicaCambioGerenteGeneral($cliente, $original)) {
            //si es que ya tiene
            if (is_array($clientesPoliticas)) {
                foreach ($clientesPoliticas as $c => $clientePolitica) {
                    foreach ($clientePolitica as $i => $p) {
                        if ($p->POLITICA_ID == Politica::POLITICA_CAMBIO_GERENCIA_GENERAL) {
                            $flg_cambio_gerencia = true;
                        }
                    }
                }
            }
            if ($flg_cambio_gerencia != true) {
                $objPolitica = new PoliticaCliente();
                $objPolitica->setValue('_codunico', $cod_unico);
                $objPolitica->setValue('_fechaCaida', Carbon::now());
                $objPolitica->setValue('_flgVigente', 1);
                $objPolitica->setValue('_politica', Politica::POLITICA_CAMBIO_GERENCIA_GENERAL);
                $objPolitica->setValue('_nivel', Politica::get(Politica::POLITICA_CAMBIO_GERENCIA_GENERAL)->NIVEL_1);
                $objPolitica->setValue('_diasGestion', Politica::get(Politica::POLITICA_CAMBIO_GERENCIA_GENERAL)->DIAS_GESTION);
                $objPolitica->setValue('_estadoFinalGestion', self::PENDIENTE);

                $politicas_agregar[] = $objPolitica->setValueToTable();
                $clientesPoliticas[$cod_unico][] = $objPolitica->getObjeto();
            }
        } else {
            if (is_array($clientesPoliticas)) {
                foreach ($clientesPoliticas as $c => $clientePolitica) {
                    foreach ($clientePolitica as $i => $p) {
                        if ($p->POLITICA_ID == Politica::POLITICA_CAMBIO_GERENCIA_GENERAL) {
                            $politicas_archivar[] = $p->ID_POLITICA_CLIENTE;
                            unset($clientesPoliticas[$c][$i]);
                        }
                    }
                }
            }
        }

        if ($this->politicaCambioAccionistas($cliente, $original)) {
            //si es que ya tiene
            if (is_array($clientesPoliticas)) {
                foreach ($clientesPoliticas as $c => $clientePolitica) {
                    foreach ($clientePolitica as $i => $p) {
                        if ($p->POLITICA_ID == Politica::POLITICA_CAMBIO_ACCIONARADO) {
                            $flg_cambio_accionista = true;
                        }
                    }
                }
            }
            if ($flg_cambio_accionista != true) {
                $objPolitica = new PoliticaCliente();
                $objPolitica->setValue('_codunico', $cod_unico);
                $objPolitica->setValue('_fechaCaida', Carbon::now());
                $objPolitica->setValue('_flgVigente', 1);
                $objPolitica->setValue('_politica', Politica::POLITICA_CAMBIO_ACCIONARADO);
                $objPolitica->setValue('_nivel', Politica::get(Politica::POLITICA_CAMBIO_ACCIONARADO)->NIVEL_1);
                $objPolitica->setValue('_diasGestion', Politica::get(Politica::POLITICA_CAMBIO_ACCIONARADO)->DIAS_GESTION);
                $objPolitica->setValue('_estadoFinalGestion', self::PENDIENTE);

                $politicas_agregar[] = $objPolitica->setValueToTable();
                $clientesPoliticas[$cod_unico][] = $objPolitica->getObjeto();
            }
        } else {
            if (is_array($clientesPoliticas)) {
                foreach ($clientesPoliticas as $c => $clientePolitica) {
                    foreach ($clientePolitica as $i => $p) {
                        if ($p->POLITICA_ID == Politica::POLITICA_CAMBIO_ACCIONARADO) {
                            $politicas_archivar[] = $p->ID_POLITICA_CLIENTE;
                            unset($clientesPoliticas[$c][$i]);
                        }
                    }
                }
            }
        }

        if (!empty($visita)) {
            if ($visita['FLG_CAMBIO_INVERSION_ACTIVO_PATRIMONIO'] == 1) {
                //si es que ya tiene
                if (is_array($clientesPoliticas)) {
                    foreach ($clientesPoliticas as $c => $clientePolitica) {
                        foreach ($clientePolitica as $i => $p) {
                            if ($p->POLITICA_ID == Politica::POLITICA_INV_50) {
                                $flg_inv50 = true;
                            }
                        }
                    }
                }
                if ($flg_inv50 != true) {
                    $objPolitica = new PoliticaCliente();
                    $objPolitica->setValue('_codunico', $cod_unico);
                    $objPolitica->setValue('_fechaCaida', Carbon::now());
                    $objPolitica->setValue('_flgVigente', 1);
                    $objPolitica->setValue('_politica', Politica::POLITICA_INV_50);
                    $objPolitica->setValue('_nivel', Politica::get(Politica::POLITICA_INV_50)->NIVEL_1);
                    $objPolitica->setValue('_diasGestion', Politica::get(Politica::POLITICA_INV_50)->DIAS_GESTION);
                    $objPolitica->setValue('_estadoFinalGestion', self::PENDIENTE);

                    $politicas_agregar[] = $objPolitica->setValueToTable();
                    $clientesPoliticas[$cod_unico][] = $objPolitica->getObjeto();
                }
            } else {
                if (is_array($clientesPoliticas)) {
                    foreach ($clientesPoliticas as $c => $clientePolitica) {
                        foreach ($clientePolitica as $i => $p) {
                            if ($p->POLITICA_ID == Politica::POLITICA_INV_50) {
                                $politicas_archivar[] = $p->ID_POLITICA_CLIENTE;
                                unset($clientesPoliticas[$c][$i]);
                            }
                        }
                    }
                }
            }

            if ($visita['FLG_CAMBIO_PRESTAMO_DESVIO'] == 1) {
                //si es que ya tiene
                if (is_array($clientesPoliticas)) {
                    foreach ($clientesPoliticas as $c => $clientePolitica) {
                        foreach ($clientePolitica as $i => $p) {
                            if ($p->POLITICA_ID == Politica::POLITICA_PREST_DESV_FONDOS) {
                                $flg_prest_desv = true;
                            }
                        }
                    }
                }
                if ($flg_prest_desv != true) {
                    $objPolitica = new PoliticaCliente();
                    $objPolitica->setValue('_codunico', $cod_unico);
                    $objPolitica->setValue('_fechaCaida', Carbon::now());
                    $objPolitica->setValue('_flgVigente', 1);
                    $objPolitica->setValue('_politica', Politica::POLITICA_PREST_DESV_FONDOS);
                    $objPolitica->setValue('_nivel', Politica::get(Politica::POLITICA_PREST_DESV_FONDOS)->NIVEL_1);
                    $objPolitica->setValue('_diasGestion', Politica::get(Politica::POLITICA_PREST_DESV_FONDOS)->DIAS_GESTION);
                    $objPolitica->setValue('_estadoFinalGestion', self::PENDIENTE);

                    $politicas_agregar[] = $objPolitica->setValueToTable();
                    $clientesPoliticas[$cod_unico][] = $objPolitica->getObjeto();
                }
            } else {
                if (is_array($clientesPoliticas)) {
                    foreach ($clientesPoliticas as $c => $clientePolitica) {
                        foreach ($clientePolitica as $i => $p) {
                            if ($p->POLITICA_ID == Politica::POLITICA_PREST_DESV_FONDOS) {
                                $politicas_archivar[] = $p->ID_POLITICA_CLIENTE;
                                unset($clientesPoliticas[$c][$i]);
                            }
                        }
                    }
                }
            }

            if ($visita['FLG_CAMBIO_MODELO_NEGOCIO'] == 1) {
                //si es que ya tiene
                if (is_array($clientesPoliticas)) {
                    foreach ($clientesPoliticas as $c => $clientePolitica) {
                        foreach ($clientePolitica as $i => $p) {
                            if ($p->POLITICA_ID == Politica::POLITICA_CAMBIO_MODELO_NEG) {
                                $flg_cambio_modelo_neg = true;
                            }
                        }
                    }
                }
                if ($flg_cambio_modelo_neg != true) {
                    $objPolitica = new PoliticaCliente();
                    $objPolitica->setValue('_codunico', $cod_unico);
                    $objPolitica->setValue('_fechaCaida', Carbon::now());
                    $objPolitica->setValue('_flgVigente', 1);
                    $objPolitica->setValue('_politica', Politica::POLITICA_CAMBIO_MODELO_NEG);
                    $objPolitica->setValue('_nivel', Politica::get(Politica::POLITICA_CAMBIO_MODELO_NEG)->NIVEL_1);
                    $objPolitica->setValue('_diasGestion', Politica::get(Politica::POLITICA_CAMBIO_MODELO_NEG)->DIAS_GESTION);
                    $objPolitica->setValue('_estadoFinalGestion', self::PENDIENTE);

                    $politicas_agregar[] = $objPolitica->setValueToTable();
                    $clientesPoliticas[$cod_unico][] = $objPolitica->getObjeto();
                }
            } else {
                if (is_array($clientesPoliticas)) {
                    foreach ($clientesPoliticas as $c => $clientePolitica) {
                        foreach ($clientePolitica as $i => $p) {
                            if ($p->POLITICA_ID == Politica::POLITICA_CAMBIO_MODELO_NEG) {
                                $politicas_archivar[] = $p->ID_POLITICA_CLIENTE;
                                unset($clientesPoliticas[$c][$i]);
                            }
                        }
                    }
                }
            }
        }

        //Reprocesamos prioridades
        if (is_array($clientesPoliticas)) {
            $clientesActualizar = self::priorizacion($clientesPoliticas);
        }
       // dd();
        //Actualizamos en base de datos
        # $notificaciones_agregar = [];
        # foreach($politicas_agregar as $politica){
        #    $notificaciones_agregar[] = convertirPoliticaNotificacion($politica)->setValueToTable();
        #}
        $mPoliticas = new mPoliticas();
        return $mPoliticas->actualizar($politicas_agregar,$politicas_archivar,$clientesActualizar);
    }

    function politicaCualitativaDiaria($clientesPoliticas){
        $politicas_actualizar = [];
        $politicas_cualitativas = [];
        $hoy = Carbon:: now();

        foreach ($clientesPoliticas as $cliente => $clientePolitica) {
            foreach ($clientePolitica as $i => $p) {
                if ($p->ORIGEN == Politica::POLITICA_TIPO_CUALITATIVA) {
                    $politicas_cualitativas[$cliente][$p->ID_POLITICA_CLIENTE] = $p;
                }
            }
        }

        foreach ($politicas_cualitativas as $codunico => $cliente) {

            foreach ($cliente as $key => $policli) {
                $fecha_caida = Carbon::createFromFormat('Y-m-d H:i:s',$policli->FECHA_CAIDA);
                $politica = Politica::get($policli->POLITICA_ID);
                $dias = $policli->DIAS_GESTION;

                if ($hoy->greaterThanOrEqualTo($fecha_caida->addDays($dias))){
                    
                    $objPolitica = new PoliticaCliente();
                    $objPolitica->setValueForEntity($policli);
                    $objPolitica->setValue('_nivel',$politica->NIVEL_2);
                    $politicas_actualizar[] = $objPolitica->setValueToTable();


                    foreach ($clientesPoliticas[$codunico] as $a => $arr) {
                        if ($arr->POLITICA_ID == $policli->POLITICA_ID){
                            $clientesPoliticas[$codunico][$a]->NIVEL = $politica->NIVEL_2;
                        }
                    }
                }
            }
        }

        $clientesActualizar = self::priorizacion($clientesPoliticas);//dd($clientesActualizar);

        //Actualizamos en base de datos
        $model = new mPoliticas();
        return $model->actualizar([], [], $clientesActualizar, $politicas_actualizar);
    }

    function PoliticasVencidas()
    {
        $model = new mPoliticas();
        return $model->PoliticasVencidas();
    }

    //revisa todas las politicas de semoforo, el input es la lista de clientes con sus politicas
    function politicaSemaforo($clientesPoliticas,$hayseleccionados = false) {

        $clientes = [];
        foreach ($clientesPoliticas as $key => $value) {
            $clientes[] = $key;
        }

        //OBTENER LOS 3 ULTIMOS SEMAFOROS DE LA TABLA WEBBE_INFINITY_CLIENTE
        $model = new modelCliente();
        $clientesSemaforos = $model->getSemaforosCliente(self::sgetPeriodoInfinity(),$clientes,$hayseleccionados);

        $politicas_archivar = [];
        $politicas_agregar = [];
        $politicas_actualizar = [];

        //POR CADA CLIENTE
        echo "clientepolitica 1: <pre>";
        print_r($clientesPoliticas);
        echo "</pre>";


        foreach ($clientesSemaforos as $key => $cliente) {
            //si el cliente ya tuviera una politica roja (actualizar severidad)
            $politica_rojo = null;
            if (isset($clientesPoliticas[$cliente->COD_UNICO])) {
                foreach ($clientesPoliticas[$cliente->COD_UNICO] as $i => $value) {
                    if ($clientesPoliticas[$cliente->COD_UNICO][$i]->POLITICA_ID == Politica::POLITICA_1ROJO_2) {
                        $politica_rojo = new PoliticaCliente();
                        $politica_rojo->setValueForEntity($clientesPoliticas[$cliente->COD_UNICO][$i]);
                    }
                }
            }

            //Sacamos todas las politicas semaforo existentes
            if (isset($clientesPoliticas[$cliente->COD_UNICO])) {
                foreach ($clientesPoliticas[$cliente->COD_UNICO] as $i => $value) {
                    if ($clientesPoliticas[$cliente->COD_UNICO][$i]->ORIGEN == Politica::POLITICA_TIPO_SEMAFORO) {
                        $politicas_archivar[] = $clientesPoliticas[$cliente->COD_UNICO][$i]->ID_POLITICA_CLIENTE;
                        unset($clientesPoliticas[$cliente->COD_UNICO][$i]);
                    }
                }
            }

            $objPolitica = new PoliticaCliente();
            $objPolitica->setValue('_codunico', $cliente->COD_UNICO);
            $objPolitica->setValue('_fechaCaida', Carbon::now());
            $objPolitica->setValue('_flgVigente', 1);
            $objPolitica->setValue('_estadoFinalGestion', self::PENDIENTE);

			// dd($politica_rojo);
            //SI ULTIMO SEMAFORO ES NEGRO
            if (in_array($cliente->NIVEL_ALERTA, Semaforo::getSemaforosNegros()) && Politica::get(Politica::POLITICA_1NEGRO)->SITUACION_POLITICA!="NO") {

                $objPolitica->setValue('_politica', Politica::POLITICA_1NEGRO);
                $objPolitica->setValue('_nivel', Politica::get(Politica::POLITICA_1NEGRO)->NIVEL_1);
                $objPolitica->setValue('_diasGestion', Politica::get(Politica::POLITICA_1NEGRO)->DIAS_GESTION);

                //SI DOS PENULTIMOS SON AMBAR Y ULTIMO ES AMBAR O ROJO
            } elseif (in_array($cliente->NIVEL_ALERTA_ANTERIOR_2, Semaforo::getSemaforosAmbar()) && in_array($cliente->NIVEL_ALERTA_ANTERIOR, Semaforo::getSemaforosAmbar()) && in_array($cliente->NIVEL_ALERTA, array_merge(Semaforo::getSemaforosAmbar(), Semaforo::getSemaforosRojos())) && Politica::get(Politica::POLITICA_2AMBAR_AR)->SITUACION_POLITICA!="NO") {

                $objPolitica->setValue('_politica', Politica::POLITICA_2AMBAR_AR);
                $objPolitica->setValue('_nivel', Politica::get(Politica::POLITICA_2AMBAR_AR)->NIVEL_1);
                $objPolitica->setValue('_diasGestion', Politica::get(Politica::POLITICA_2AMBAR_AR)->DIAS_GESTION);

                //SI PENULTIMO ES ROJO -- AUMENTO DE SEVERIDAD
				
            } elseif (in_array($cliente->NIVEL_ALERTA_ANTERIOR, Semaforo::getSemaforosRojos()) && Politica::get(Politica::POLITICA_1ROJO_2)->SITUACION_POLITICA!="NO") {
                if ($politica_rojo) {
                    $politica_rojo->setValue('_nivel', Politica::get(Politica::POLITICA_1ROJO_2)->NIVEL_2);
                }
                //SI EL ULTIMO ES ROJO
            } elseif (in_array($cliente->NIVEL_ALERTA, Semaforo::getSemaforosRojos()) && Politica::get(Politica::POLITICA_1ROJO_2)->SITUACION_POLITICA!="NO") {

                $objPolitica->setValue('_politica', Politica::POLITICA_1ROJO_2);
                $objPolitica->setValue('_nivel', Politica::get(Politica::POLITICA_1ROJO_2)->NIVEL_1);
                $objPolitica->setValue('_diasGestion', Politica::get(Politica::POLITICA_1ROJO_2)->DIAS_GESTION);
                $objPolitica->setValue('_estadoFinalGestion', self::PENDIENTE);
                //SI DOS ULTIMOS SON AMBAR
            } elseif (in_array($cliente->NIVEL_ALERTA, Semaforo::getSemaforosAmbar()) && in_array($cliente->NIVEL_ALERTA_ANTERIOR, Semaforo::getSemaforosAmbar()) && Politica::get(Politica::POLITICA_2AMBAR)->SITUACION_POLITICA!="NO") {
                $objPolitica->setValue('_politica', Politica::POLITICA_2AMBAR);
                $objPolitica->setValue('_nivel', Politica::get(Politica::POLITICA_2AMBAR)->NIVEL_1);
                $objPolitica->setValue('_diasGestion', Politica::get(Politica::POLITICA_2AMBAR)->DIAS_GESTION);
                $objPolitica->setValue('_estadoFinalGestion', self::PENDIENTE);
                //SI  ULTIMO ES AMBAR
            } elseif (in_array($cliente->NIVEL_ALERTA, Semaforo::getSemaforosAmbar()) && Politica::get(Politica::POLITICA_1AMBAR)->SITUACION_POLITICA!="NO") {
                $objPolitica->setValue('_politica', Politica::POLITICA_1AMBAR);
                $objPolitica->setValue('_nivel', Politica::get(Politica::POLITICA_1AMBAR)->NIVEL_1);
                $objPolitica->setValue('_diasGestion', Politica::get(Politica::POLITICA_1AMBAR)->DIAS_GESTION);
                $objPolitica->setValue('_estadoFinalGestion', self::PENDIENTE);
            }

            //Si cayo en alguna politica
            if ($objPolitica->getValue('_politica')) {
                //agregar nueva politica a array
                $politicas_agregar[] = $objPolitica->setValueToTable();
                //agregar la politica nueva en una lista
                $clientesPoliticas[$cliente->COD_UNICO][] = $objPolitica->getObjeto();
            }

            //Si aumento la severidad de la politica
			// dd($politica_rojo);
            if ($politica_rojo) {
                //agregar nueva politica a array
                $politicas_actualizar[] = $politica_rojo->setValueToTable();
                //agregar la politica nueva en una lista
                $clientesPoliticas[$cliente->COD_UNICO][] = $politica_rojo->getObjeto();
            }
        }
        echo "clientepolitica 1: <pre>";
        print_r($clientesPoliticas);
        echo "</pre>";
        echo "politicas_archivar: <pre>";
        print_r($politicas_archivar);
        echo "</pre>";
        echo "politicas_agregar: <pre>";
        print_r($politicas_agregar);
        echo "</pre>";
        echo "politicas_actualizar: <pre>";
        print_r($politicas_actualizar);
        echo "</pre>";
        // die();

        $model = new mPoliticas();
        $model->actualizar([], [], [], $politicas_actualizar);

        //Reprocesamos prioridades
        $clientesActualizar = self::priorizacion($clientesPoliticas);

        echo "clientesActualizar: <pre>";
        print_r($clientesActualizar);
        echo "</pre>";

        //Actualizamos en base de datos
        
        $model->actualizar($politicas_agregar, [], $clientesActualizar, []);

        
        return $model->actualizar([], $politicas_archivar, [], []);
    }

    function politicaDocumentaria($clientesPoliticas,$hayseleccionados = false) {
        //OBTENER LOS EEFF DE LOS CLIENTES
        $clientesDocumentos = [];
        $politicas_archivar = [];
        $politicas_agregar = [];
        $model = new modelCliente();
        $clientesDocumentos = $model->getDocumentosCliente(self::sgetPeriodoInfinity(),null,$hayseleccionados);
        $clientesDocs=[];
        $polidocs=[];
        if (!empty($clientesDocumentos)) {
            foreach ($clientesDocumentos as $in => $dato) {
                $clientesDocs[$dato->COD_UNICO]=$dato;
            }
        }

        if (!empty($clientesPoliticas)) {
            foreach ($clientesPoliticas as $p) {
                foreach ($p as $val) {
                    if ($val->ORIGEN==Politica::POLITICA_TIPO_DOCUMENTARIA) {
                        $polidocs[$val->COD_UNICO][]=$val;
                    }
                }
            }
        }
        // dd($polidocs,$clientesDocs);

        foreach ($clientesDocs as $key => $cliente) {
            if (isset($polidocs[$cliente->COD_UNICO])) {
                foreach ($polidocs[$cliente->COD_UNICO] as $i => $value) {
                        if (mPoliticas::getClientePolitica($polidocs[$cliente->COD_UNICO][$i]->ID_POLITICA_CLIENTE)) {

                            if (self::evaluacionDocumento($cliente->FECHA_DDJJ,$cliente->FECHA_INGRESO_INFINITY,'DDJJ')){
                                $politicas_archivar[] = $polidocs[$cliente->COD_UNICO][$i]->ID_POLITICA_CLIENTE;
                                if (isset($clientesDocs[$key])) {
                                    unset($clientesDocs[$key]);
                                }
                            }
                            if (self::evaluacionDocumento($cliente->FECHA_EEFF,$cliente->FECHA_INGRESO_INFINITY,'EEFF')){
                                $politicas_archivar[] = $polidocs[$cliente->COD_UNICO][$i]->ID_POLITICA_CLIENTE;
                                if (isset($clientesDocs[$key])) {
                                    unset($clientesDocs[$key]);
                                }
                            }

                            if (self::evaluacionDocumento($cliente->FECHA_IBR,$cliente->FECHA_INGRESO_INFINITY,'IBR')){
                                $politicas_archivar[] = $polidocs[$cliente->COD_UNICO][$i]->ID_POLITICA_CLIENTE;
                                if (isset($clientesDocs[$key])) {
                                    unset($clientesDocs[$key]);
                                }
                            }
                            if (self::evaluacionDocumento($cliente->FECHA_F02,$cliente->FECHA_INGRESO_INFINITY,'F02')){
                                $politicas_archivar[] = $polidocs[$cliente->COD_UNICO][$i]->ID_POLITICA_CLIENTE;
                                if (isset($clientesDocs[$key])) {
                                    unset($clientesDocs[$key]);
                                }
                            }
                    }
                }
            }
        }
       
        if (count($clientesDocs)>0) {
            foreach ($clientesDocs as $i => $clientes) {
                //Si cae cumple la politica de fecha EEFF
                if (Politica::get(Politica::POLITICA_DDJJ)->SITUACION_POLITICA=='VIGENTE' && self::evaluacionDocumento($clientes->FECHA_DDJJ, $cliente->FECHA_INGRESO_INFINITY, 'DDJJ')) {
                    $objPolitica = new PoliticaCliente();
                    $objPolitica->setValue('_codunico', $clientes->COD_UNICO);
                    $objPolitica->setValue('_fechaCaida', Carbon::now());
                    $objPolitica->setValue('_flgVigente', 1);
                    $objPolitica->setValue('_politica', Politica::POLITICA_DDJJ);
                    $objPolitica->setValue('_nivel', Politica::get(Politica::POLITICA_DDJJ)->NIVEL_1);
                    $objPolitica->setValue('_diasGestion', Politica::get(Politica::POLITICA_DDJJ)->DIAS_GESTION);

 

                    $politicas_agregar[] = $objPolitica->setValueToTable();
                    $clientesPoliticas[$clientes->COD_UNICO][] = $objPolitica->getObjeto();
                }

                //Si cae cumple la politica de fecha EEFF
                if (Politica::get(Politica::POLITICA_EEFF)->SITUACION_POLITICA=='VIGENTE' && self::evaluacionDocumento($clientes->FECHA_EEFF, $cliente->FECHA_INGRESO_INFINITY, 'EEFF')) {
                    $objPolitica = new PoliticaCliente();
                    $objPolitica->setValue('_codunico', $clientes->COD_UNICO);
                    $objPolitica->setValue('_fechaCaida', Carbon::now());
                    $objPolitica->setValue('_flgVigente', 1);
                    $objPolitica->setValue('_politica', Politica::POLITICA_EEFF);
                    $objPolitica->setValue('_nivel', Politica::get(Politica::POLITICA_EEFF)->NIVEL_1);
                    $objPolitica->setValue('_diasGestion', Politica::get(Politica::POLITICA_EEFF)->DIAS_GESTION);

 

                    $politicas_agregar[] = $objPolitica->setValueToTable();
                    $clientesPoliticas[$clientes->COD_UNICO][] = $objPolitica->getObjeto();
                }

                //Si cae cumple la politica de fecha IBR
                if (Politica::get(Politica::POLITICA_IBR)->SITUACION_POLITICA=='VIGENTE' && self::evaluacionDocumento($clientes->FECHA_IBR, $cliente->FECHA_INGRESO_INFINITY, 'IBR')) {
                    $objPolitica = new PoliticaCliente();
                    $objPolitica->setValue('_codunico', $clientes->COD_UNICO);
                    $objPolitica->setValue('_fechaCaida', Carbon::now());
                    $objPolitica->setValue('_flgVigente', 1);
                    $objPolitica->setValue('_politica', Politica::POLITICA_IBR);
                    $objPolitica->setValue('_nivel', Politica::get(Politica::POLITICA_IBR)->NIVEL_1);
                    $objPolitica->setValue('_diasGestion', Politica::get(Politica::POLITICA_IBR)->DIAS_GESTION);

 

                    $politicas_agregar[] = $objPolitica->setValueToTable();
                    $clientesPoliticas[$clientes->COD_UNICO][] = $objPolitica->getObjeto();
                }

                //Si cae cumple la politica de fecha F02
                if (Politica::get(Politica::POLITICA_F02)->SITUACION_POLITICA=='VIGENTE' && self::evaluacionDocumento($clientes->FECHA_F02, $cliente->FECHA_INGRESO_INFINITY, 'F02')) {
                    $objPolitica = new PoliticaCliente();
                    $objPolitica->setValue('_codunico', $clientes->COD_UNICO);
                    $objPolitica->setValue('_fechaCaida', Carbon::now());
                    $objPolitica->setValue('_flgVigente', 1);
                    $objPolitica->setValue('_politica', Politica::POLITICA_F02);
                    $objPolitica->setValue('_nivel', Politica::get(Politica::POLITICA_F02)->NIVEL_1);
                    $objPolitica->setValue('_diasGestion', Politica::get(Politica::POLITICA_F02)->DIAS_GESTION);

 

                    $politicas_agregar[] = $objPolitica->setValueToTable();
                    $clientesPoliticas[$clientes->COD_UNICO][] = $objPolitica->getObjeto();
                }
            }
        }
        //Reprocesamos prioridades
        $clientesActualizar = self::priorizacion($clientesPoliticas);

        echo "clientesActualizar<pre>";
        print_r($clientesActualizar);
        echo "</pre>";
        echo "politicas_agregar<pre>";
        print_r($politicas_agregar);
        echo "</pre>";
        echo "politicas_archivar<pre>";
        print_r($politicas_archivar);
        echo "</pre>";
        //die();
        //Actualizamos en base de datos
        $mPoliticas = new mPoliticas();
        return $mPoliticas->actualizar($politicas_agregar, $politicas_archivar, $clientesActualizar);
    }

    function evaluacionDocumento($fechadocumento = null, $fechaingresolinea, $tipodoc) {
        $hoy = Carbon::now();
        $añopasado = Carbon::now()->addyear(-1);

        $jun_16 = Carbon::create(Carbon::now()->year,06,16);
        $nov_16 = Carbon::create(Carbon::now()->year,11,16);
        $pasadonov = Carbon::create(Carbon::now()->addyear(-1)->year,11,01);


        $findic = Carbon::create(Carbon::now()->year, 12, 31);
        $nov = Carbon::create(Carbon::now()->year, 11, 01);

        if ($tipodoc == 'DDJJ') {
            //NOS FIJAMOS SI LA FECHA DE DOCUMENTO DEL DJ ESTA COMPLETO O INCOMPLETO
            //SI NO TENGO DATO DE FECHA DE DOCUMENTO ESTA INCOMPLETO
            if (empty($fechadocumento)) {
                $estadodoc = self::INCOMPLETO;
            } else {
                $fdoc = Carbon::parse($fechadocumento);
                //SI EL AÑO DE LA FECHA DEL DOCUMENTO ES IGUAL AL DEL AÑO PASADO ESTA COMPLETO, SINO INCOMPLETO
                $estadodoc = ($fdoc->year==$añopasado->year) ? self::COMPLETO : self::INCOMPLETO;
            }

            if ($estadodoc==self::INCOMPLETO) {
            //SI EL ESTADO DEL DOCUMENTO ES INCOMPLETO Y ESTAMOS EN UNA FECHA MAYOR O IGUAL AL 16 DE JUNIO DE ESTE AÑO
                if ($hoy>=$jun_16)
                    return true;
                else
                    return false;
            }

            elseif ($estadodoc==self::COMPLETO) {
                //SI EL ESTADO DEL DOCUMENTO ES COMPLETO, NO SE APLICA
                return false;
            }
        }

        if ($tipodoc == 'EEFF') {
            $fingreso=Carbon::parse($fechaingresolinea);
            //NOS FIJAMOS SI LA FECHA DE INGRESO DEL CLIENTE A L.AUTOM. ES POSTERIOR A NOVIEMBRE DE ESTE AÑO
            $clientenuevo = ($fingreso>$pasadonov) ? 1 : 0;

            if ($clientenuevo==1) {
                //SI ES CLIENTE NUEVO, VEMOS SI ESTAMOS ENTRE NOVIEMBRE O DICIEMBRE
                return false;
            }elseif ($clientenuevo==0) {
                if (empty($fechadocumento)) {
                    $estadodoc = self::INCOMPLETO;
                } else {
                    $fdoc = Carbon::parse($fechadocumento);
                    //SI L.A. AÑO DE LA FECHA DEL DOCUMENTO ES DIFERENTE AL DE ESTE AÑO ESTA INCOMPLETO
                    $estadodoc = ($fdoc->year==$hoy->year) ? self::COMPLETO : self::INCOMPLETO;
                }
            }

            if (isset($estadodoc)) {
                if ($estadodoc==self::INCOMPLETO) {
                    if ($hoy>=$nov_16)
                        return false;
                    else
                        return true;
                }
                elseif ($estadodoc == self::COMPLETO) {
                    return false;
                }
            }
        }

        if ($tipodoc == 'IBR') {
            if (!empty($fechadocumento)) {
                $fdoc = Carbon::parse($fechadocumento);
                $difmes = $fdoc->diffInMonths($hoy);

                if (isset($difmes) && $difmes > 15) {
                    return true;
                } else {
                    return false;
                }
            }else{
                return true;
            }
        }

        if ($tipodoc == 'F02') {
            if (!empty($fechadocumento)) {
                $fdoc = Carbon::parse($fechadocumento);
                $difmes = $fdoc->diffInMonths($hoy);

                if (isset($difmes) && $difmes > 15) {
                    return true;
                } else {
                    return false;
                }
            }else{
                return true;
            }
        }
        return false;
    }

    function politicaPerformance($clientesPoliticas,$hayseleccionados = false) {
        //OBTENER EL PERFORMANCE DE LOS CLIENTES
        //dd($clientesPoliticas);
        // echo '$clientesPoliticas-ANTES: <pre>';
        // print_r($clientesPoliticas);
        // echo '</pre>';
        $model = new modelCliente();
        $hoy = Carbon::now();

        //recorrer $clientesPoliticas
        //identificar las de performance que ya estan registradas
        //revisar si se deben archivar
        $politicas_performance = [];
        foreach ($clientesPoliticas as $cliente => $clientePolitica) {
            foreach ($clientePolitica as $i => $p) {
                if ($p->ORIGEN == Politica::POLITICA_TIPO_PERFORMANCE) {
                    $politicas_performance[$cliente][$i] = $p;
                }
            }
        }

        // echo '$clientesPoliticas-performance: <pre>';
        // print_r($politicas_performance);
        // print_r($clientesPoliticas);
        // echo '</pre>';
        $clientesTotal = $model->getPerformanceCliente(self::sgetPeriodoInfinity(),null,$hayseleccionados);
        // dd($politicas_performance,$clientesTotal);
        $politicas_archivar = [];
        $politicas_agregar = [];
        $politicas_actualizar = [];

        //se recorren todos los clientes
        foreach ($clientesTotal as $evaluar) {
            $flg_monto_vencido = false;
            $flg_deuda_laboral_coactiva = false;
            $flg_peor_clasificacion = false;
            //dd($evaluar);
            //se valida si el cliente ya se encuentra con una politica de performance
            if (isset($politicas_performance[$evaluar->COD_UNICO])) {
               // echo "\tevaluar: " . $evaluar->COD_UNICO."\n";
               //  se recorren las politicas del clientes y se validan si cumplen las condiciones
                foreach ($politicas_performance[$evaluar->COD_UNICO] as $i => $p) {
                   // echo "\tp: " . $p->POLITICA_ID."\n";
                   // echo "\tDEUDA_LABORAL_COACTIVA: " . $evaluar->DEUDA_LABORAL_COACTIVA."\n";
                   // echo "\tMONTO_VENCIDO: " . $evaluar->MONTO_VENCIDO."\n";
                   // echo "\tPEOR_CLASIFICACION: " . $evaluar->PEOR_CLASIFICACION."\n";
                    // if ($p->POLITICA_ID == Politica::POLITICA_CRED_VENC_MAYOR_15M && !($evaluar->MONTO_VENCIDO > self::MONTO_VENCIDO)) {
                    //    // $politicas_archivar[] = ['ID_POLITICA_CLIENTE' => $p->ID_POLITICA_CLIENTE];
                    //     $politicas_archivar[] = $p->ID_POLITICA_CLIENTE;
                    //     unset($clientesPoliticas[$evaluar->COD_UNICO][$i]);
                    // }
                    if ($p->POLITICA_ID == Politica::POLITICA_CRED_VENC_MAYOR_15M && $evaluar->MONTO_VENCIDO =='0') {
                       // $politicas_archivar[] = ['ID_POLITICA_CLIENTE' => $p->ID_POLITICA_CLIENTE];
                        // print_r($evaluar);
                        $politicas_archivar[] = $p->ID_POLITICA_CLIENTE;
                        unset($clientesPoliticas[$evaluar->COD_UNICO][$i]);
                    }

                    // if ($p->POLITICA_ID == Politica::POLITICA_DEUD_LAB_COAC_MAYOR_50M && !($evaluar->DEUDA_LABORAL_COACTIVA > self::DEUDA_LABORAL_COACTIVA)) {
                    //    // $politicas_archivar[] = ['ID_POLITICA_CLIENTE' => $p->ID_POLITICA_CLIENTE];
                    //     $politicas_archivar[] = $p->ID_POLITICA_CLIENTE;
                    //     unset($clientesPoliticas[$evaluar->COD_UNICO][$i]);
                    // }
                    if ($p->POLITICA_ID == Politica::POLITICA_DEUD_LAB_COAC_MAYOR_50M && $evaluar->DEUDA_LABORAL_COACTIVA =='0') {
                       // $politicas_archivar[] = ['ID_POLITICA_CLIENTE' => $p->ID_POLITICA_CLIENTE];
                        $politicas_archivar[] = $p->ID_POLITICA_CLIENTE;
                        unset($clientesPoliticas[$evaluar->COD_UNICO][$i]);
                    }

                    if ($p->POLITICA_ID == Politica::POLITICA_CALIF_DIF_NORMAL && !($evaluar->PEOR_CLASIFICACION == self::NUEVO_PEOR_CLASIFICACION)) {
                        $politicas_archivar[] = $p->ID_POLITICA_CLIENTE;
                        unset($clientesPoliticas[$evaluar->COD_UNICO][$i]);
                    }
                }
            }

            if (empty(modelCliente::getClienteBlindadoxPerformance($evaluar->COD_UNICO))) {
                
                //se valida si el cliente cumple con condiciones
                if ($evaluar->MONTO_VENCIDO > self::MONTO_VENCIDO) {

                    //se valida si el cliente ya tiene politica ingresada del
                    if (isset($politicas_performance[$evaluar->COD_UNICO])) {
                        foreach ($politicas_performance[$evaluar->COD_UNICO] as $p) {
                            if ($p->POLITICA_ID == Politica::POLITICA_CRED_VENC_MAYOR_15M && !($evaluar->MONTO_VENCIDO>$evaluar->MONTO_VENCIDO_ANTERIOR)) {
                                $flg_monto_vencido = true;
                            }
                        }
                    }

                    if ($flg_monto_vencido !== true) {
                        $objPolitica = new PoliticaCliente();
                        $objPolitica->setValue('_codunico', $evaluar->COD_UNICO);
                        $objPolitica->setValue('_fechaCaida', Carbon::now());
                        $objPolitica->setValue('_flgVigente', 1);
                        $objPolitica->setValue('_politica', Politica::POLITICA_CRED_VENC_MAYOR_15M);
                        $objPolitica->setValue('_nivel', Politica::get(Politica::POLITICA_CRED_VENC_MAYOR_15M)->NIVEL_1);
                        $objPolitica->setValue('_diasGestion', Politica::get(Politica::POLITICA_CRED_VENC_MAYOR_15M)->DIAS_GESTION);
                        $objPolitica->setValue('_estadoFinalGestion', self::PENDIENTE);

                        $politicas_agregar[] = $objPolitica->setValueToTable();
                        $clientesPoliticas[$evaluar->COD_UNICO][] = $objPolitica->getObjeto();
                    }
                }

                //se valida si el cliente cumple con condiciones
                if ($evaluar->DEUDA_LABORAL_COACTIVA > self::DEUDA_LABORAL_COACTIVA) {
                    //se valida si el cliente ya tiene politica ingresada del
                    if (isset($politicas_performance[$evaluar->COD_UNICO])) {
                        foreach ($politicas_performance[$evaluar->COD_UNICO] as $p) {
                            if ($p->POLITICA_ID == Politica::POLITICA_DEUD_LAB_COAC_MAYOR_50M && !($evaluar->DEUDA_LABORAL_COACTIVA>$evaluar->DEUDA_LABORAL_COACTIVA_ANTERIOR)) {
                                $flg_deuda_laboral_coactiva = true;
                            }
                        }
                    }

                    if ($flg_deuda_laboral_coactiva !== true) {
                        $objPolitica = new PoliticaCliente();
                        $objPolitica->setValue('_codunico', $evaluar->COD_UNICO);
                        $objPolitica->setValue('_fechaCaida', Carbon::now());
                        $objPolitica->setValue('_flgVigente', 1);
                        $objPolitica->setValue('_politica', Politica::POLITICA_DEUD_LAB_COAC_MAYOR_50M);
                        $objPolitica->setValue('_nivel', Politica::get(Politica::POLITICA_DEUD_LAB_COAC_MAYOR_50M)->NIVEL_1);
                        $objPolitica->setValue('_diasGestion', Politica::get(Politica::POLITICA_DEUD_LAB_COAC_MAYOR_50M)->DIAS_GESTION);
                        $objPolitica->setValue('_estadoFinalGestion', self::PENDIENTE);

                        $politicas_agregar[] = $objPolitica->setValueToTable();
                        $clientesPoliticas[$evaluar->COD_UNICO][] = $objPolitica->getObjeto();
                    }
                }

                //se valida si el cliente cumple con condiciones
                if ($evaluar->FLG_POLITICA_GE == self::NUEVO_PEOR_CLASIFICACION) {
                   //  se valida si el cliente ya tiene politica ingresada del
                   // echo "\tevaluar: " . $evaluar->COD_UNICO."\n";
                    if (isset($politicas_performance[$evaluar->COD_UNICO])) {
                       // echo "\tp: " . $p->POLITICA_ID."\n";
                       // echo "\tPEOR_CLASIFICACION: " . $evaluar->PEOR_CLASIFICACION."\n";
                        foreach ($politicas_performance[$evaluar->COD_UNICO] as $p) {
                           // echo "\tPOLITICA_ID: " . $p->POLITICA_ID."\n";
                           // echo "\tPolitica: " . Politica::POLITICA_CALIF_DIF_NORMAL."\n";
                            if ($p->POLITICA_ID == Politica::POLITICA_CALIF_DIF_NORMAL) {
                                $flg_peor_clasificacion = true;
                            }
                        }
                       // dd($flg_peor_clasificacion);
                       // dd($politicas_performance[$evaluar->COD_UNICO]);
                    }

                    if ($flg_peor_clasificacion !== true) {
                        $objPolitica = new PoliticaCliente();
                        $objPolitica->setValue('_codunico', $evaluar->COD_UNICO);
                        $objPolitica->setValue('_fechaCaida', Carbon::now());
                        $objPolitica->setValue('_flgVigente', 1);
                        $objPolitica->setValue('_politica', Politica::POLITICA_CALIF_DIF_NORMAL);
                        $objPolitica->setValue('_nivel', Politica::get(Politica::POLITICA_CALIF_DIF_NORMAL)->NIVEL_1);
                        $objPolitica->setValue('_diasGestion', Politica::get(Politica::POLITICA_CALIF_DIF_NORMAL)->DIAS_GESTION);
                        $objPolitica->setValue('_estadoFinalGestion', self::PENDIENTE);

                        $politicas_agregar[] = $objPolitica->setValueToTable();
                        $clientesPoliticas[$evaluar->COD_UNICO][] = $objPolitica->getObjeto();
                    }
                }
            }
        }
       // echo '$politicas_performance: <pre>';
       // print_r($clientesTotal);
       // echo '</pre>';die();
        echo '$politicas_archivar<pre>';
        print_r($politicas_archivar);
        echo '</pre>';

        echo '$politicas_agregar: <pre>';
        print_r($politicas_agregar);
        echo '</pre>';

        // echo '$clientesPoliticas-Final: <pre>';
        // print_r($clientesPoliticas);
        // echo '</pre>';
        // die();
       // Reprocesamos prioridades
        $clientesActualizar = self::priorizacion($clientesPoliticas);
        echo '$clientesActualizar: <pre>';
        print_r($clientesActualizar);
        echo '</pre>';
        // die();
       // Actualizamos en base de datos
        $mPoliticas = new mPoliticas();
        return $mPoliticas->actualizar($politicas_agregar, $politicas_archivar, $clientesActualizar, $politicas_actualizar);
    }

    static function actualizar($clientesActualizar, $politicas_agregar = [], $politicas_archivar = [],$reingreso = null){
        $mPoliticas = new mPoliticas();
        return $mPoliticas->actualizar($politicas_agregar, $politicas_archivar, $clientesActualizar,[],$reingreso);
    }

    function actualizarNivelPoliticas()
    {
        $model = new mPoliticas();
        return $model->actualizarNivelPoliticas(self::sgetPeriodoInfinity());
    }

    static function priorizacion($clientesPoliticas) {

        $clientesActualizacion = [];
        //Recorremos todos los clientes

        foreach ($clientesPoliticas as $cu => $cliente) {
            //seteamos la maxima severidad y la fecha mas proxima de venc de gestion por cada cliente
            $minSeveridad = 999;
            $minFecha = Carbon::createFromFormat('Y-m-d', '2099-01-01');
            $maxPolitica = null;
            if (isset($cliente) && count($cliente)>0) {
                
                foreach ($cliente as $key => $politica) {

                    if (is_object($politica) && isset($politica->POLITICA_ID)) {

                        if ($politica->NIVEL < $minSeveridad) {
                            $maxPolitica = $politica->POLITICA_ID;
                            $minSeveridad = $politica->NIVEL;
                        }

                        $objPolitica = Politica::get($politica->POLITICA_ID);
                        $dias = $objPolitica->DIAS_GESTION;

                        if (is_string($politica->FECHA_CAIDA)) {
                            $fecha = Carbon::createFromFormat('Y-m-d H:i:s', $politica->FECHA_CAIDA);
                        } else {
                            $fecha = $politica->FECHA_CAIDA;
                        }

                        //AGREGAR DIAS
                        if ($dias > 0 && $fecha->addDays($dias)->lessThan($minFecha)) {
                            $minFecha = clone $fecha;
                        }
                    }
                }
            }
                $clientesActualizacion[] = [
                    'PERIODO' => self::sgetPeriodoInfinity(),
                    'COD_UNICO' => $cu,
                    'FLG_INFINITY' => self::getLogoBySeveridad($minSeveridad),
                    'FECHA_VENCIMIENTO_POLITICA' => ($minFecha->equalTo(Carbon::createFromFormat('Y-m-d', '2099-01-01'))) ? null : $minFecha->toDateTimeString(),
                    'SALIDA_POLITICA' => ($minSeveridad == Politica::NIVEL_SEVERIDAD_SALIDA) ? $maxPolitica : null,
                ];
        }

        return $clientesActualizacion;
    }

    static function getLogoBySeveridad($severidad) {
        $logo = Cliente::LOGO_AZUL;
        switch ($severidad) {
            case Politica::NIVEL_SEVERIDAD_SALIDA:
                $logo = Cliente::LOGO_SALIDA;
                break;

            case Politica::NIVEL_SEVERIDAD_COMITE:
                $logo = Cliente::LOGO_GRIS;
                break;

            case Politica::NIVEL_SEVERIDAD_OBSERVACION:
                $logo = Cliente::LOGO_AZUL;
                break;
        }
        return $logo;
    }

    function politicaCambioClientes(ConocemeCliente $nuevo, ConocemeCliente $original) {
        //$results = [];
        $results = false;
        //Por cada proveedores top que habia se setea verdadero todos, si en caso uno tiene una concentracion mayor o igual al 50%-75%, no se aplica la regla
        //comparo el original contra todos clientes de nuevo
        foreach ($original->getValue('_clientes') as $i => $ori) {
            if ($ori->getValue('_documento')!='') {
                $campo=$ori->getValue('_documento');
            } else {
                $campo=$ori->getValue('_nombre');
            }
            // echo '$campo: <pre>';print_r($campo);echo '</pre>';
            $encuentra = false;
            foreach ($nuevo->getValue('_clientes') as $j => $ult) {
                if ($campo == $ult->getValue('_documento') || $campo == $ult->getValue('_nombre')) {
                    $encuentra = true;
                    if (($ult->getValue('_concentracion')>= 3 && $ori->getValue('_concentracion')<3) ||
                            ($ult->getValue('_concentracion')<=2 && $ori->getValue('_concentracion')>2)) {
                       // echo 'encuentra proveedor: <pre>'; print_r($ult->getValue('_concentracion'));echo '</pre>';
                        $results = true;
                    } 
                } 
            }
            if (!$encuentra && $ori->getValue('_concentracion')>= 3){
                $results = true;
            }
        }
        
       // echo '$results: <pre>';print_r($results);echo '</pre>';
        //comparo el nuevo contra todos clientes de original
        foreach ($nuevo->getValue('_clientes') as $j => $ult) {
            if ($ult->getValue('_documento')!='') {
                $campo=$ult->getValue('_documento');
            } else {
                $campo=$ult->getValue('_nombre');
            }
            $encuentra = false;
            foreach ($original->getValue('_clientes') as $i => $ori) {
                if ($campo == $ori->getValue('_documento') || $campo == $ori->getValue('_nombre')) {
                    $encuentra = true;
                    if (($ult->getValue('_concentracion')>= 3 && $ori->getValue('_concentracion')<3) || 
                            ($ult->getValue('_concentracion')<=2 && $ori->getValue('_concentracion')>2)) {
                        $results = true;
                    }
                }
            }
            if (!$encuentra && $ult->getValue('_concentracion')>= 3){
                $results = true;
            }
        }
        return $results;
    }

    function politicaCambioProveedores(ConocemeCliente $nuevo, ConocemeCliente $original) {
        //$results = [];
        $results = false;
        //Por cada proveedores top que habia se setea verdadero todos, si en caso uno tiene una concentracion mayor o igual al 50%-75%, no se aplica la regla
        //comparo el original contra todos clientes de nuevo
        foreach ($original->getValue('_proveedores') as $i => $ori) {
            if ($ori->getValue('_documento')!='') {
                $campo=$ori->getValue('_documento');
            } else {
                $campo=$ori->getValue('_nombre');
            }
           // echo '$campo: <pre>'; print_r($campo); echo '</pre>';
            $encuentra = false;
            foreach ($nuevo->getValue('_proveedores') as $j => $ult) {
                
                if ($campo == $ult->getValue('_documento') || $campo == $ult->getValue('_nombre')) {
                    $encuentra = true;
                    if (($ult->getValue('_concentracion')>= 3 && $ori->getValue('_concentracion')<3) || 
                        ($ult->getValue('_concentracion')<=2 && $ori->getValue('_concentracion')>2)) {
                       // echo 'encuentra proveedor: <pre>'; print_r($ult->getValue('_concentracion')); echo '</pre>';
                        $results = true;
                    } 
                } 
            }
            if (!$encuentra && $ori->getValue('_concentracion')>= 3){
                $results = true;
            }
        }
        
       // echo '$results: <pre>'; print_r($results); echo '</pre>';
        //comparo el nuevo contra todos clientes de original
        foreach ($nuevo->getValue('_proveedores') as $j => $ult) {
            if ($ult->getValue('_documento')!='') {
                $campo=$ult->getValue('_documento');
            } else {
                $campo=$ult->getValue('_nombre');
            }
            $encuentra = false;
            foreach ($original->getValue('_proveedores') as $i => $ori) {
                if ($campo == $ori->getValue('_documento') || $campo == $ori->getValue('_nombre')) {
                    $encuentra = true;
                
                    if (($ult->getValue('_concentracion')>= 3 && $ori->getValue('_concentracion')<3) || 
                            ($ult->getValue('_concentracion')<=2 && $ori->getValue('_concentracion')>2)) {
                        $results = true;
                    }
                }
            }
            if (!$encuentra && $ult->getValue('_concentracion')>= 3){
                $results = true;
            }
        }
        return $results;
    }

    function politicaCambioGerenteGeneral(ConocemeCliente $nuevo, ConocemeCliente $original) {
        return strtolower($nuevo->getValue('_gerenteGeneral')) !== strtolower($original->getValue('_gerenteGeneral'));
    }

    function politicaCambioAccionistas(ConocemeCliente $nuevo, ConocemeCliente $original) {

        if (count($original->getValue('_accionistas')) != count($nuevo->getValue('_accionistas'))) {
            return true;
        }

        $results = [];
        foreach ($original->getValue('_accionistas') as $key => $orig) {
            $results[$key] = true;
            foreach ($nuevo->getValue('_accionistas') as $ult) {
                if ($orig->getValue('_documento') == $orig->getValue('_documento') && $ult->getValue('_concentracion') == $orig->getValue('_concentracion')) {
                    $results[$key] = false;
                }
            }
        }
        return $results ? max($results) : false;
    }

    function getJefeEjecutivo($registro)
    {
        $mUsuario = new mUsuario();
        return $mUsuario->getJefeEjecutivo($registro);
    }

    function actualizarblindaje()
    {
        $mPoliticas = new mPoliticas();
        return $mPoliticas->actualizarblindaje();
    }

}
