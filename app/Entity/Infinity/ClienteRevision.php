<?php
//WEBBE_INFINITY_CLIENTE_REVISION
namespace App\Entity\Infinity;

use Jenssegers\Date\Date as Carbon;
use App\Model\Infinity\Cliente as modelCliente;

class ClienteRevision extends \App\Entity\Base\Entity {

    protected $_fechafin;
    protected $_codUnico;
    protected $_tiempo;
    protected $_fecharegistro;
    protected $_Idpoliticacliente;
    protected $_Idpolitica;
    protected $_flgvigente;

    function setValueToTable() {
        return $this->cleanArray([
            'COD_UNICO' => $this->_codUnico,
            'FECHA_FIN' => $this->_fechafin->format('Y-m-d H:i:s'),
            'FECHA_REGISTRO' => $this->_fecharegistro->format('Y-m-d H:i:s'),
            'TIEMPO' => $this->_tiempo,
            'ID_POLITICACLIENTE' => $this->_Idpoliticacliente,
            'POLITICA_ID' => $this->_Idpolitica,
            'FLG_VIGENTE' => $this->_flgvigente,
        ]);
    }
    function setProperties($data) {
        $this->setValues([
            '_codUnico' => $data->COD_UNICO,
            '_fechafin' => $data->FECHA_FIN,
            '_fecharegistro' => $data->FECHA_REGISTRO,
            '_tiempo' => $data->TIEMPO,
            '_Idpoliticacliente' => $data->ID_POLITICACLIENTE,
            '_Idpolitica' => $data->POLITICA_ID,
            '_flgvigente' => $data->FLG_VIGENTE,
        ]);
    }
}