<?php

namespace App\Entity\Infinity;

use App\Model\Infinity\AlertaExplicacion as modelAlertaExplicacion;

class AlertaExplicacion extends \App\Entity\Base\Entity {
    
    const TIPO_CUALITATIVA = 'Cualitativa';
    const TIPO_PERFORMANCE = 'Performance';
    const TIPO_EXPLICATIVA = 'Explicativa';

    const HISTORIA_MESES = 4;
	
    static function getByCliente($codUnico){
        $model = new modelAlertaExplicacion();
        return $model->get($codUnico,self::getPeriodosPrevios(self::sgetPeriodoInfinity(),self::HISTORIA_MESES))->get();
    }

    static function getSalidas($codUnico){
        $model = new modelAlertaExplicacion();
        return $model->getSalidas($codUnico,self::getPeriodosPrevios(self::sgetPeriodoInfinity(),self::HISTORIA_MESES))->get();
    }
}