<?php

namespace App\Entity\Infinity;

use App\Model\Infinity\NotificacionInfinity as mNotificacion;
use Jenssegers\Date\Date as Carbon;

class NotificacionInfinity extends \App\Entity\Base\Entity {

    const DDJJ=1;
    const EEFF=2;
    const IBR=3;
    const F02=4;
    const VISITA_1=5;
    const VISITA_2=6;
    
    const EN=0;
    const JEFE=1;
    const GERENTE=2;
    //Carbon::create(year,month,day)
    //ALERTAS PARA EL EJECUTIVO DE NEGOCIO

    const REGLAS_EN=[
        self::DDJJ=>'01/04',
        self::EEFF=>'01/09',
        self::IBR=>'11', //Meses
        self::F02=>'11', //Meses
        self::VISITA_1=>'01/06',
        self::VISITA_2=>'01/12',
    ];

    //ALERTAS PARA EL JEFE ZONAL
    const REGLAS_JEFE=[
        self::DDJJ=>'01/05',
        self::EEFF=>'01/10',
        self::IBR=>'13',
        self::F02=>'13',
        self::VISITA_1=>'01/07',
        self::VISITA_2=>'01/01',
    ];
    
    //ALERTAS PARA EL GERENTE ZONAL
    const REGLAS_GERENTE=[
        self::DDJJ=>'01/05',
        self::EEFF=>'01/10',
        self::IBR=>'14',
        self::F02=>'14',
        self::VISITA_1=>'01/07',
        self::VISITA_2=>'01/01',
    ];

    const REGLAS=[
        self::EN=>self::REGLAS_EN,
        self::JEFE=>self::REGLAS_JEFE,
        self::GERENTE=>self::REGLAS_GERENTE,
    ];


    static function getClientesCorreos($filtros){
        $model=new mNotificacion();
        return $model->getClientesCorreos(self::sgetPeriodoInfinity(),$filtros)->get();
    }

    function getFechaCompleta($fecha){

        //$difAnhos me ayuda a saber dependiendo de la fecha actual a que fecha comparamos
        //Ejemplo, si nos encontramos en abril de 2019 y la fecha constante dice octubre, nos referimos a 2018
        $difAnhos=0;
        $dia=substr($fecha,0,2);
        $mes=substr($fecha,3,2);

        //(int)$mes>=8 de agosto en adelante
        if(Carbon::now()->month<(int)$mes and (int)$mes>=8){
            $difAnhos=1;
        }

        return Carbon::create(Carbon::now()->year-$difAnhos,$mes,$dia,0,0,0);
    }

    function calcularPorMeses($fecha,$constante,&$alertasUsuarios){

        $fechaHoy=Carbon::now();
        $diferenciaMeses=-1;

        //Fecha no puede ser NULL para el proceso de diferencia
        if($fecha!=NULL){
            $diferenciaMeses=$fecha->diffInMonths($fechaHoy,false);
        }
        //dd($diferenciaMeses);
        for($i=0;$i<=2;$i++){
            $mesesComparar=self::REGLAS[$i][$constante];           
            if($diferenciaMeses>=$mesesComparar or $diferenciaMeses==-1){
                $alertasUsuarios[$i][$constante]=1;
                $alertasUsuarios[$i]['CORREO']+=1;
            }
        }
    }

    function calcularPorFecha($fecha,$constante,&$alertasUsuarios){

        $fechaHoy=Carbon::now();

        for($i=0;$i<=2;$i++){
            $fechaComparar=self::getFechaCompleta(self::REGLAS[$i][$constante]); 
            //dd($fechaComparar);          
            if($fecha<=$fechaComparar and ($fechaComparar<= $fechaHoy or $fecha<=$fechaHoy)){
                $alertasUsuarios[$i][$constante]=1;
                $alertasUsuarios[$i]['CORREO']+=1;
            }
        }
    }

    function getAlertasCliente($cliente){
        $fechaHoy=Carbon::now();
        //Todas las alertas empiezan apagadas
        $alertas=[
            self::DDJJ=>0,
            self::EEFF=>0,
            self::IBR=>0,
            self::F02=>0,
            self::VISITA_1=>0,
            self::VISITA_2=>0,
            'CORREO'=>0,
        ];

        $alertasUsuarios=[
            self::EN=>$alertas,
            self::JEFE=>$alertas,
            self::GERENTE=>$alertas,
        ];

        //Hacemos filtros para cada elemento
        //DDJJ
        $fechaDDJJ=$cliente->FECHA_ULTIMO_EEFF1!=null?Carbon::parse($cliente->FECHA_ULTIMO_EEFF1):null;
        self::calcularPorFecha($fechaDDJJ,self::DDJJ,$alertasUsuarios);

        //EEFF        
        $fechaEEFF=$cliente->FECHA_ULTIMO_EEFF2!=null?Carbon::parse($cliente->FECHA_ULTIMO_EEFF2):null;
        self::calcularPorFecha($fechaEEFF,self::EEFF,$alertasUsuarios);
        
        //IBR
        $fechaIBR=$cliente->FECHA_ULTIMO_IBR!=null?Carbon::parse($cliente->FECHA_ULTIMO_IBR):null;
        self::calcularPorMeses($fechaIBR,self::IBR,$alertasUsuarios);        
        
        //F02
        $fechaF02=$cliente->FECHA_ULTIMO_F02!=null?Carbon::parse($cliente->FECHA_ULTIMO_F02):null;
        self::calcularPorMeses($fechaF02,self::F02,$alertasUsuarios); 

        //VISITA 1 y VISITA 2
        $fechaVisita=$cliente->FECHA_ULTIMA_VISITA!=null?Carbon::parse($cliente->FECHA_ULTIMA_VISITA):null;

        for($i=0;$i<=2;$i++){
            $fechaComparar1=self::getFechaCompleta(self::REGLAS[$i][self::VISITA_1]);           
            $fechaComparar2=self::getFechaCompleta(self::REGLAS[$i][self::VISITA_2]);         

            $diferencia1=$fechaComparar1->diffInMonths($fechaHoy,false);
            $diferencia2=$fechaComparar2->diffInMonths($fechaHoy,false);
            //dd($fechaVisita,$fechaComparar1,$fechaComparar2,$diferencia1,$diferencia2);
            //Comparar con el que tenga la diferencia de fechas más cercanas
            if($diferencia1>0){
                //dd("JPÑA");
                if($fechaVisita<=$fechaComparar1){
                    $alertasUsuarios[$i][self::VISITA_1]=1;
                    $alertasUsuarios[$i]['CORREO']+=1;
                }
            }
            if($diferencia2>0){             
                //dd("HOLA");   
                if($fechaVisita<=$fechaComparar2 and $fechaVisita<=$fechaComparar1->subYear(1)){
                    //dd($fechaVisita,$fechaComparar2,$fechaComparar1);
                    $alertasUsuarios[$i][self::VISITA_2]=1;
                    $alertasUsuarios[$i]['CORREO']+=1;
                }
            }
        }



        return $alertasUsuarios;
    }

    function getCadenaEmpresa($ejecutivo,&$correoDestinatario,&$destinatario,&$regDestinatario,$tipoUsuario){
        $cadenaEmpresa='';
        foreach($ejecutivo as $empresa){

            switch ($tipoUsuario) {
                case self::EN:
                    $correoDestinatario=$empresa->EMAIL_EN;
                    $destinatario=$empresa->NOMBRE_EN;
                    $regDestinatario=$empresa->REGISTRO_EN;
                    break;
                case self::JEFE:
                    $correoDestinatario=$empresa->EMAIL_JEFATURA_ZONAL;
                    $destinatario=$empresa->NOMBRE_JEFATURA;
                    $regDestinatario=$empresa->REGISTRO_JEFATURA;
                    break;
                case self::GERENTE:
                    $correoDestinatario=$empresa->EMAIL_GERENCIA_ZONAL;
                    $destinatario=$empresa->NOMBRE_GERENCIA;
                    $regDestinatario=$empresa->REGISTRO_GERENCIA;
                    break;
            }

            $correoDestinatario=$empresa->EMAIL_EN;
            $destinatario=$empresa->NOMBRE_EN;
            $regDestinatario=$empresa->REGISTRO_EN;

            $cadenaEmpresa.='Razón Social: '.$empresa->NOMBRE.PHP_EOL.'Código Único: '. $empresa->COD_UNICO.PHP_EOL;

            if($tipoUsuario!=self::EN)
                $cadenaEmpresa.='Ejecutivo de Negocio : '.$empresa->NOMBRE_EN.PHP_EOL;

            if($empresa->ALERTAS_CLIENTE[self::DDJJ])
                $cadenaEmpresa.='Fecha último DDJJ: '.$empresa->FECHA_ULTIMO_EEFF1.PHP_EOL;

            if($empresa->ALERTAS_CLIENTE[self::EEFF])
                $cadenaEmpresa.='Fecha último EEFF: '.$empresa->FECHA_ULTIMO_EEFF2.PHP_EOL;

            if($empresa->ALERTAS_CLIENTE[self::IBR])
                $cadenaEmpresa.='Fecha último IBR: '.$empresa->FECHA_ULTIMO_IBR.PHP_EOL;

            if($empresa->ALERTAS_CLIENTE[self::F02])
                $cadenaEmpresa.='Fecha último F02: '.$empresa->FECHA_ULTIMO_F02.PHP_EOL;

            if($empresa->ALERTAS_CLIENTE[self::VISITA_1])
                $cadenaEmpresa.='Fecha última visita: '.$empresa->FECHA_ULTIMA_VISITA.PHP_EOL;

            if($empresa->ALERTAS_CLIENTE[self::VISITA_2])
                $cadenaEmpresa.='Fecha última visita: '.$empresa->FECHA_ULTIMA_VISITA.PHP_EOL;
            $cadenaEmpresa.=PHP_EOL;
        }

        return $cadenaEmpresa;
    }

    function formatoCorreos($arreglo,$tipo){
        $correos=[];
        if(count($arreglo)==0) break;

        $correoDestinatario='';
        $destinatario='';
        $regDestinatario='';

        switch ($tipo) {
            case self::EN:
                //Para el ejecutivo le mandamos una lista de empresas
                foreach ($arreglo as $ejecutivo) {               
                    $cadenaEmpresa=self::getCadenaEmpresa($ejecutivo,$correoDestinatario,$destinatario,$regDestinatario,self::EN);                    
                    $textoCorreo='Estimad@ '.$destinatario.': '.PHP_EOL;
                    $textoCorreo.='Las siguientes empresas han generado alertas por su documentación: '.PHP_EOL.$cadenaEmpresa;
                    $textoCorreo.='Saludos,'.PHP_EOL;
                    $textoCorreo.='Inteligencia Comercial';

                    $correos[]=[
                        'REGISTRO'=>$regDestinatario,
                        'NOMBRE'=>$destinatario,
                        'CORREO'=>$correoDestinatario,
                        'TEXTO'=>$textoCorreo,
                    ];
                }
                break;
            case self::JEFE:
                # code...
                foreach ($arreglo as $jefe) {
                    $cadenaEmpresa='';
                    foreach ($jefe as $ejecutivo) {
                        //dd($ejecutivo);
                        $cadenaEmpresa.=self::getCadenaEmpresa($ejecutivo,$correoDestinatario,$destinatario,$regDestinatario,self::JEFE);                    
                        
                    }
                        $textoCorreo='Estimad@ '.$destinatario.': '.PHP_EOL;
                        $textoCorreo.='Las siguientes empresas han generado alertas por su documentación: '.PHP_EOL.PHP_EOL.$cadenaEmpresa.PHP_EOL;
                        $textoCorreo.='Saludos,'.PHP_EOL;
                        $textoCorreo.='Inteligencia Comercial';

                        $correos[]=[
                            'REGISTRO'=>$regDestinatario,
                            'NOMBRE'=>$destinatario,
                            'CORREO'=>$correoDestinatario,
                            'TEXTO'=>$textoCorreo,
                        ];

                }
                
                break;
            case self::GERENTE:

                foreach ($arreglo as $jefe) {
                    $cadenaEmpresa='';
                    foreach ($jefe as $ejecutivo) {
                        $cadenaEmpresa.=self::getCadenaEmpresa($ejecutivo,$correoDestinatario,$destinatario,$regDestinatario,self::GERENTE);                    
                        
                    }
                        $textoCorreo='Estimad@ '.$destinatario.': '.PHP_EOL;
                        $textoCorreo.='Las siguientes empresas han generado alertas por su documentación: '.PHP_EOL.PHP_EOL.$cadenaEmpresa.PHP_EOL;
                        $textoCorreo.='Saludos,'.PHP_EOL;
                        $textoCorreo.='Inteligencia Comercial';

                        $correos[]=[
                            'REGISTRO'=>$regDestinatario,
                            'NOMBRE'=>$destinatario,
                            'CORREO'=>$correoDestinatario,
                            'TEXTO'=>$textoCorreo,
                        ];

                }
                break;
            
        }
        return $correos;
    }

    public function notificar($filtros){
        $consultaCorreos=self::getClientesCorreos($filtros);
        //Armaremos el arreglo  
        $ejecutivos=[];
        $jefes=[];
        $gerentes=[];

        $eIni='';
        $jIni='';
        $gIni='';
        foreach ($consultaCorreos as $registro) {

            //Aquí debería de invocarse una función para medir los criterios de cada cliente
            //Dependiendo del tipo de alerta, se deberá insertar en los arreglos
            $alertasCliente=self::getAlertasCliente($registro);
            
            //Ejecutivos
            if($eIni!=$registro->REGISTRO_EN){
                $eIni=$registro->REGISTRO_EN;
            }
            if($alertasCliente[self::EN]['CORREO']>0){
                $registro->ALERTAS_CLIENTE=$alertasCliente[self::EN];
                $ejecutivos[$eIni][]=$registro;
            }
            
            //Jefes
            if($jIni!=$registro->REGISTRO_JEFATURA){
                $jIni=$registro->REGISTRO_JEFATURA;
            }
            if($alertasCliente[self::JEFE]['CORREO']>0){
                $registro->ALERTAS_CLIENTE=$alertasCliente[self::JEFE];
                //$jefes[$jIni][$eIni]['NOMBRE_EN']=$registro->NOMBRE_EN;
                //$jefes[$jIni][$eIni]['REGISTRO_EN']=$eIni;
                $jefes[$jIni][$eIni][]=$registro;
            }

            //Gerentes
            if($gIni!=$registro->REGISTRO_GERENCIA){
                $gIni=$registro->REGISTRO_GERENCIA;
            }
            if($alertasCliente[self::GERENTE]['CORREO']>0){
                $registro->ALERTAS_CLIENTE=$alertasCliente[self::GERENTE];
                //$gerentes[$jIni][$eIni]['NOMBRE_EN']=$registro->NOMBRE_EN;
                //$gerentes[$jIni][$eIni]['REGISTRO_EN']=$eIni;
                $gerentes[$gIni][$eIni][]=$registro;
            }
            
        }
        //dd($jefes);
        $ejecutivos=self::formatoCorreos($ejecutivos,self::EN);
        $jefes=self::formatoCorreos($jefes,self::JEFE);
        $gerentes=self::formatoCorreos($gerentes,self::GERENTE);
        dd($ejecutivos,$jefes,$gerentes);
    }
}

