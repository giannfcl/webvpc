<?php

namespace App\Entity\Infinity;

use App\Model\Infinity\Cliente as modelCliente;
use Illuminate\Support\Facades\Auth;
use App\Entity\Usuario as Usuario;

class ConocemeStakeHolder extends \App\Entity\Base\Entity {

    CONST TIPO_CLIENTE = 'cliente';
    CONST TIPO_PROVEEDOR = 'proveedor';
    CONST TIPO_ACCIONISTA = 'accionista';

    protected $_codunico;
    protected $_tipo;
    protected $_documento;
    protected $_nombre;
    protected $_concentracion;
    protected $_desde;
    protected $_exclusividad;
    protected $_flgContrato;
    protected $_contratofechaVencimiento;
    protected $_contratoAdjunto;
    protected $_nacimiento;

    function setValueToTable() {
        $data = [
            'COD_UNICO' => $this->_codunico,
            'TIPO' => $this->_tipo,
            'DOCUMENTO' => $this->_documento,
            'NOMBRE' => $this->_nombre,
            'CONCENTRACION' => $this->_concentracion,
            'DESDE' => $this->_desde,
            'EXCLUSIVIDAD' => $this->_exclusividad,
            'NACIMIENTO' => $this->_nacimiento,
            'CONTRATO_FECHA_VENCIMIENTO' => $this->_contratofechaVencimiento,
            'CONTRATO_ADJUNTO' => $this->_contratoAdjunto,
            'FLAG_CONTRATO' => $this->_flgContrato,
        ];
        return $data;
        //return $this->cleanArray($data);
    }

    function setValueForEntity($data) {
        $this->_codunico = $data->COD_UNICO;
        $this->_tipo = $data->TIPO;
        $this->_documento = $data->DOCUMENTO;
        $this->_nombre = $data->NOMBRE;
        $this->_concentracion = $data->CONCENTRACION;
        $this->_desde = $data->DESDE;
        $this->_exclusividad = $data->EXCLUSIVIDAD;
        $this->_contratofechaVencimiento = $data->CONTRATO_FECHA_VENCIMIENTO;
        $this->_contratoAdjunto = $data->CONTRATO_ADJUNTO;
        $this->_nacimiento = $data->NACIMIENTO;
        $this->_flgContrato = $data->FLAG_CONTRATO;
        return $data;
    }

    function getByCodUnico($codunico) {
        
    }

    static function getConocemeStakeHolder($conoceme_hist_id,$nombre,$tipo){
        $stakeholder=modelCliente::getConocemeStakeHolder($conoceme_hist_id,$nombre,$tipo);
        return $stakeholder;
    }

}
