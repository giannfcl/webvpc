<?php

namespace App\Entity\Infinity;

use App\Model\Infinity\Alertacartera as mAlertacartera;
use App\Model\Infinity\Cliente as modelCliente;
use App\Model\Infinity\GrupoEconomico as modelGrupoEconomico;
use App\Model\Infinity\ConocemeClientePolitica as modelPolitica;
use Illuminate\Support\Facades\Auth;
use App\Entity\Usuario as Usuario;
use App\Entity\Infinity\Semaforo as Semaforo;
use Jenssegers\Date\Date as Carbon;
use App\Model\TestPolitica as mPoliticas;
use App\Entity\Infinity\PoliticaProceso as PoliticaProceso;
use App\Entity\BE\SegActividad as SegActividad;

class Alertacartera extends \App\Entity\Base\Entity {
	static function getResumenAlertas($data){
		$model = new mAlertacartera();
		$consulta = $model->getResumenAlertas($data);
		\Debugbar::info('antes de ejecutar');
		$data = $consulta->get();
		\Debugbar::info('despues de ejecutar');
		return $data;
		//return $model->getResumenAlertas($data)->get();
	}
	static function bancas($banca=null)
	{
		return mAlertacartera::getBancas($banca);
	}

	static function zonales($data)
	{
		return mAlertacartera::getZonales($data);
	}
	static function jefaturas($data)
	{
		return mAlertacartera::getJefaturas($data);		
	}

	static function get_datosPdf($mes)
	{
		return mAlertacartera::get_datosPdf($mes);
	}
	static function getPDF_Meses()
	{
		return mAlertacartera::getPDF_Meses();
	}
	static function getPDF_Semanas($mes)
	{
		return mAlertacartera::getPDF_Semanas($mes);
	}
	static function ejecutivos($data)
	{
		return mAlertacartera::getEjecutivos($data);
	}

	static function getUsuarioTWarbol($data)
	{
		return mAlertacartera::getUsuarioTWarbol($data);
	}

	static function getJefeRegistro($data)
	{
		return mAlertacartera::getJefeRegistro($data);
	}

	static function getByID($data)
	{
		return mAlertacartera::getByID($data);
	}
	static function getProdDiario($cu)
	{
		return mAlertacartera::getProdDiario($cu);
	}
	static function getSegActividad($cu)
	{
		return mAlertacartera::getSegActividad($cu);
	}

	static function getfechaSegActividad()
	{
		return mAlertacartera::getfechaSegActividad();
	}

	static function Seguimientodiario($data)
	{
		$entidad = new mAlertacartera();
        return $entidad->getListaClientesSeguimiento($data);
	}
	static function tablaCompromisos($data)
	{
		$entidad = new mAlertacartera();
        return $entidad->tablaCompromisos($data);
	}

	static function DatoJefeEjecutivo($registroejecutivo)
	{
		$entidad = new mAlertacartera();
        return $entidad->DatoJefeEjecutivo($registroejecutivo);
	}

	static function CombosParaActividad($tipo,$rol=null)
	{
		$entidad = new mAlertacartera();
        return $entidad->CombosParaActividad($tipo,$rol);
	}

	static function saveActividad($data,$usuario)
	{
		$actividad = new SegActividad();
        $actividad->setValues([
            '_registroen' => $usuario,
            '_fecharegistro' => Carbon::now(),
            '_codunico' => $data['cod_unico'],
            '_fechaactividad' => $data['fechaActividad'],
            '_responsable' => $data['responsable'],
            '_tipo' => $data['tipo'],
            '_comentario' => $data['comentario'],
            '_accion' => $data['accion'],
            '_compromisopago' => $data['compromisopago'],
            '_fechainicioatraso' => mAlertacartera::getDatosSegActividad($data['cod_unico'])->FECHA_VENCIMIENTO,
            '_fechaactualizaciondoc'=>mAlertacartera::getDatosSegActividad($data['cod_unico'])->FECHA_ACTUALIZACION_DOC,
            '_deudatotal'=> mAlertacartera::getDatosSegActividad($data['cod_unico'])->DEUDA_TOTAL,
            '_deudaatrasada'=> mAlertacartera::getDatosSegActividad($data['cod_unico'])->DEUDA_ATRASADA,
            '_deudavencida'=> mAlertacartera::getDatosSegActividad($data['cod_unico'])->DEUDA_VENCIDA,
            '_dias'=> mAlertacartera::getDatosSegActividad($data['cod_unico'])->DIAS,
            '_pctdeudaproblema'=> mAlertacartera::getDatosSegActividad($data['cod_unico'])->PCT_DEUDA_PROBLEMA,
            '_motivoimpago' => $data['motivoimpago'],
        ]);
        // dd($actividad->setValueToTable());
		$entidad = new mAlertacartera();
        return $entidad->saveActividad($actividad->setValueToTable());
	}

	function getSegActividadByid($id)
	{
		$entidad = new mAlertacartera();
        return $entidad->getSegActividadByid($id);
	}

	function actualizaractividadcompromiso($id,$data)
	{
		$entidad = new mAlertacartera();
        return $entidad->actualizaractividadcompromiso($id,$data);
	}

	static function getDiasAtrasosmaximo($cu)
	{
		$model = new mAlertacartera();
		return $model->getDiasAtrasosmaximo($cu);
	}

	function guardarvistaVideoGF($data)
	{
		$datosvistavideo=[
			'REGISTRO'=>$data['REGISTRO'],
			'VIDEO'=>$data['VIDEO'],
			'VECES'=>$data['VECES'],
			'FECHA_REGISTRO'=>Carbon::now(),
		];
		$model = new mAlertacartera();
		return $model->guardarvistaVideoGF($datosvistavideo);
	}

	function buscarusuarioGF($data)
	{
		$model = new mAlertacartera();
		return $model->buscarusuarioGF($data);
	}
}
