<?php

namespace App\Entity\Infinity;

use App\Model\Infinity\Cliente as modelCliente;
use App\Model\Infinity\Visita as modelVisita;
use App\Entity\Infinity\PoliticaProceso as PoliticaProceso;
use Illuminate\Support\Facades\Auth;
use App\Entity\Usuario as Usuario;
use App\Entity\Infinity\FichaCovid as FichaCovid;
use Jenssegers\Date\Date as Carbon;

class Visita extends \App\Entity\Base\Entity {

    protected $_id;
    protected $_codunico;
    protected $_flagCambioModeloNegocio;
    protected $_flagCambioMixVentas;
    protected $_flagCambioProcesoIntegracion;
    protected $_flagCambioConcentracionVentas;
    protected $_flagCambioConcentracionProveedores;
    protected $_flagCambioOperaciones;
    protected $_flagCambioZonaClientes;
    protected $_flagCambioGerenciaGeneral;
    protected $_flagCambioGestionFinanciera;
    protected $_flagCambioAccionistas;
    protected $_flagCambioLineas;
    protected $_flagCambioInversionActivoPatrimonio;
    protected $_flagCambioPrestamoDesvio;
    protected $_flagCambioBacklog;
    protected $_cambioInversionActivoPatrimonio;
    protected $_cambioPrestamoDesvio;

    protected $_entrevistadoCargo;
    protected $_entrevistadoNombre;
    protected $_comentarios;
    protected $_comentariosRV;
    protected $_fechaVisita;

    protected $_fechaActualizacion;
    protected $_registro;
    protected $_registroNombre;
    protected $_registroObj;

    protected $_revisionFecha;
    protected $_revisionRegistro;
    function getValidadorPOST($data=null,$registro,$rol,$privilegios){
        $model = new modelVisita();
        return $model->getValidadorPOST($data,$registro,$rol,$privilegios);
    }
    function getValidador($cu,$registro,$rol,$datoscovid){
        $model = new modelVisita();
        return $model->getValidador($cu,$registro,$rol,$datoscovid);
    }
    function getValidadores($registro,$cu,$datoscovid){
        $model = new modelVisita();
        return $model->getValidadores($registro,$cu,$datoscovid);
    }

    function setValueToTable() {
        //parent::setValueToTable();
        $data = [
            'ID' => $this->_id,
            'COD_UNICO' => $this->_codunico,
            'FLG_CAMBIO_MODELO_NEGOCIO' => $this->_flagCambioModeloNegocio,
            'FLG_CAMBIO_MIX_VENTAS' => $this->_flagCambioMixVentas,
            'FLG_CAMBIO_PROCESO_INTEGRACION' => $this->_flagCambioProcesoIntegracion,
            'FLG_CAMBIO_CONCENTRACION_VENTAS' => $this->_flagCambioConcentracionVentas,
            'FLG_CAMBIO_CONCENTRACION_PROVEEDORES' => $this->_flagCambioConcentracionProveedores,
            'FLG_CAMBIO_OPERACIONES' => $this->_flagCambioOperaciones,
            'FLG_CAMBIO_ZONA_CLIENTES' => $this->_flagCambioZonaClientes,
            'FLG_CAMBIO_GERENCIA_GENERAL' => $this->_flagCambioGerenciaGeneral,
            'FLG_CAMBIO_GESTION_FINANCIERA' => $this->_flagCambioGestionFinanciera,
            'FLG_CAMBIO_ACCIONISTAS' => $this->_flagCambioAccionistas,
            'FLG_CAMBIO_LINEAS' => $this->_flagCambioLineas,
            'FLG_CAMBIO_INVERSION_ACTIVO_PATRIMONIO' => $this->_flagCambioInversionActivoPatrimonio,
            'FLG_CAMBIO_PRESTAMO_DESVIO' => $this->_flagCambioPrestamoDesvio,
            'FLG_CAMBIO_BACKLOG' => $this->_flagCambioBacklog,
            'REGISTRO' => $this->_registro,
            'COMENTARIOS' => $this->_comentarios,
            'COMENTARIO_ROL_VALIDADOR' => $this->_comentariosRV,
            'ENTREVISTADO_CARGO' => $this->_entrevistadoCargo,
            'ENTREVISTADO_NOMBRE' => $this->_entrevistadoNombre,
            'FECHA_REGISTRO' => ($this->_fechaActualizacion)?$this->_fechaActualizacion->format('Y-m-d H:i:s'):'',
            'FECHA_VISITA' => $this->_fechaVisita,
            'REVISION_FECHA' => $this->_revisionFecha,
            'REVISION_USUARIO' => $this->_revisionRegistro,
            'CAMBIO_INVERSION_ACTIVO_PATRIMONIO' => $this->_cambioInversionActivoPatrimonio,
            'CAMBIO_PRESTAMO_DESVIO' => $this->_cambioPrestamoDesvio,

        ];
        return $this->cleanArray($data);
    }
    function deleteVisita($id){
        $model = new modelVisita();
        $model->deleteVisita($id);
    }
    function setValueForEntity($data) {
        $this->_id = $data->ID;
        $this->_codunico = $data->COD_UNICO;
        $this->_flagCambioModeloNegocio = $data->FLG_CAMBIO_MODELO_NEGOCIO;
        $this->_flagCambioMixVentas = $data->FLG_CAMBIO_MIX_VENTAS;
        $this->_flagCambioConcentracionVentas = $data->FLG_CAMBIO_CONCENTRACION_VENTAS;
        $this->_flagCambioConcentracionProveedores = $data->FLG_CAMBIO_CONCENTRACION_PROVEEDORES;
        $this->_flagCambioOperaciones = $data->FLG_CAMBIO_OPERACIONES;
        $this->_flagCambioZonaClientes = $data->FLG_CAMBIO_ZONA_CLIENTES;
        $this->_flagCambioGerenciaGeneral = $data->FLG_CAMBIO_GERENCIA_GENERAL;
        $this->_flagCambioGestionFinanciera = $data->FLG_CAMBIO_GESTION_FINANCIERA;
        $this->_flagCambioAccionistas = $data->FLG_CAMBIO_ACCIONISTAS;
        $this->_flagCambioProcesoIntegracion = $data->FLG_CAMBIO_PROCESO_INTEGRACION;
        $this->_flagCambioLineas = $data->FLG_CAMBIO_LINEAS;
        $this->_flagCambioInversionActivoPatrimonio = $data->FLG_CAMBIO_INVERSION_ACTIVO_PATRIMONIO;
        $this->_flagCambioPrestamoDesvio = $data->FLG_CAMBIO_PRESTAMO_DESVIO;
        $this->_flagCambioBacklog = $data->FLG_CAMBIO_BACKLOG;
        $this->_registro = $data->REGISTRO;
        $this->_registroNombre = $data->NOMBRE_USUARIO;
        $this->_comentarios = $data->COMENTARIOS;
        $this->_comentariosRV = $data->COMENTARIO_ROL_VALIDADOR;
        $this->_entrevistadoCargo = $data->ENTREVISTADO_CARGO;
        $this->_entrevistadoNombre = $data->ENTREVISTADO_NOMBRE;
        $this->_fechaActualizacion = $data->FECHA_REGISTRO;
        $this->_fechaVisita = ($data->FECHA_VISITA)? new Carbon($data->FECHA_VISITA) : null;
        $this->_revisionFecha = $data->REVISION_FECHA;
        $this->_revisionRegistro = $data->REVISION_USUARIO;
        $this->_cambioInversionActivoPatrimonio = $data->CAMBIO_INVERSION_ACTIVO_PATRIMONIO;
        $this->_cambioPrestamoDesvio = $data->CAMBIO_PRESTAMO_DESVIO;
        
        return $data;
    }

    static function getVisitas($codUnico){
        $model= new modelVisita();
        return $model->getVisitas($codUnico);
    }

    public function registrar(\App\Entity\Infinity\ConocemeCliente $cliente,$data=null,$registro,$alertas,$nombreUsuario,$notificar=true){
        $model = new modelVisita();
        $this->_fechaActualizacion = Carbon::now();
        $cliente->setValue('_fechaActualizacion', $this->_fechaActualizacion);
        $entidad = new FichaCovid();
        $objCovid = $entidad->save2($data,$registro,false);
        $result = $model->registrar($this->setValueToTable(),false,$cliente->setValueToTable(),$objCovid,$alertas,$nombreUsuario,$notificar);
        $this->_id = $result[0];
        // if($this->_id !== false){
        //     //Envío de Email
        //     //     $mail = new \App\Mail\Infinity\ConfirmacionValidacionVisita(
        //     //         $this->_registroObj
        //     //         ,$cliente->getValue('_nombre')
        //     //         ,$cliente->getValue('_codunico')
        //     //         ,route('infinity.me.cliente.visita',['cu' => $cliente->getValue('_codunico')])
        //     //         );
        //     // dispatch(new \App\Jobs\MailQueue($mail));
        //     //Exito de visita
        //     $this->setMessage('La primera parte se ha guardado correctamente');
        // }else{
        //     $this->setMessage('Hubo un error al registrar sus datos');
        // }
        return $result;

    }
    public function getAreasRevision($rol){
        $model = new modelVisita();
        $revision = $model->getAreasRevision($rol);
        return $revision;
    }
    public function confirmar(\App\Entity\Infinity\ConocemeCliente $cliente,\App\Entity\Infinity\ConocemeCliente $original,
                                $idVisita,$historiaId,$data=null,$registro,$alertas,$nombreUsuario){
        
        /*Revisando Políticas y Niveles de explicación*/        

        $model = new modelVisita();
        $this->_revisionFecha = Carbon::now();
        $entidad = new FichaCovid();
        $objCovid = $entidad->save2($data,$registro,true,$idVisita);
        // dd($cliente->setValueToTable());
        if($model->confirmar(parent::sgetPeriodoInfinity(),$this->setValueToTable(),$cliente->setValueToTable()
                ,$idVisita,$historiaId,$objCovid,$alertas,$nombreUsuario)){
                $politicacliente=new PoliticaProceso();
                $politicacliente->politicaCualitativa($cliente, $original,$this->setValueToTable());
            //REVISION DE POLITICAS CUALITATIVAS
            //     $mail = new \App\Mail\Infinity\ConfirmacionVisitaJefe(
            //         $cliente->getValue('_usuarioRegistro')
            //         ,$cliente->getValue('_nombre')
            //         ,$cliente->getValue('_codunico')
            //         ,route('infinity.me.cliente.visita',['cu' => $cliente->getValue('_codunico')])
            //         );
            // dispatch(new \App\Jobs\MailQueue($mail));
            $this->setMessage('La visita se confirmó correctamente');
            return true;
        }else{
            $this->setMessage('Hubo un error al confirmar los datos del cliente');
            return false;
        }
    }

    public function getUltimaVisita($cu){
        $model = new modelVisita();
        $data = $model->getUltimaVisita($cu)->first();
        if ($data){
            $this->setValueForEntity($data);
            return $data;
        }else{
            return false;
        }
    }


    public function getbyId($idVisita){
        $model= new modelVisita();
        $data=$model->getbyId($idVisita) -> first();
        $this->setValueForEntity($data);
        return $data;
    }


    public function getUltimaVisitaCliente($cu){
        $model = new modelVisita();
        $data = $model->getUltimaVisitaCliente($cu)->first();
        return $data;

    }
}