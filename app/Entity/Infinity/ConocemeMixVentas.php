<?php

namespace App\Entity\Infinity;

use App\Model\Infinity\Cliente as modelCliente;
use Illuminate\Support\Facades\Auth;
use App\Entity\Usuario as Usuario;

class ConocemeMixVentas extends \App\Entity\Base\Entity {

    protected $_codunico;
    protected $_productoServicio;
    protected $_participacion;

    function setValueToTable() {
        $data = [
            'COD_UNICO' => $this->_codunico,
            'PRODUCTO' => $this->_productoServicio,
            'PARTICIPACION' => $this->_participacion,
        ];
        //return $this->cleanArray($data);
        return $data;
    }

    function setValueForEntity($data) {
        $this->_codunico = $data->COD_UNICO;
        $this->_productoServicio = $data->PRODUCTO;
        $this->_participacion = $data->PARTICIPACION;
        return $data;
    }

}
