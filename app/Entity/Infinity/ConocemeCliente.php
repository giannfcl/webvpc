<?php

namespace App\Entity\Infinity;

use App\Model\Infinity\ConocemeCliente as modelConoceme;
use App\Model\Infinity\ConocemeClienteHistoria as modelConocemeHistoria;
use App\Model\Infinity\ConocemeStakeholder as modelConocemeStakeholder;
use App\Model\Infinity\ConocemeLinea as modelConocemeLinea;
use App\Model\Infinity\ConocemeZona as modelConocemeZona;
use App\Model\Infinity\ConocemeCommoditie as modelConocemeCommoditie;
use App\Model\Infinity\ConocemeMixVentas as modelConocemeMixVentas;
use App\Model\Infinity\ConocemeInversiones as modelConocemeInversiones;
use App\Model\Infinity\ConocemeCanalVentas as modelConocemeCanalVentas;
use App\Model\Infinity\ConocemeClienteRcc as modelConocemeClienteRcc;
use App\Entity\Usuario as Usuario;
use Jenssegers\Date\Date as Carbon;

class ConocemeCliente extends Cliente {

    protected $_codunicoConoceme;

    protected $_actividad;
    protected $_subsector;
    protected $_backlog;
    protected $_modeloNegocio;
    protected $_ventajaCompetitiva;
    protected $_fortalezasRiesgos;
    protected $_zonaOperaciones = [];
    protected $_zonaClientes = [];

    protected $_mixVentas = [];
    protected $_canalVentas = [];

    protected $_flagIntegracionVertical;
    protected $_gestionesCompra;
    protected $_procedenciaMateriaPrima;
    protected $_commodities = [];

    protected $_clientes = [];
    protected $_proveedores = [];

    protected $_gerenteGeneral;
    protected $_financieroRol;
    protected $_financieroNombre;
    protected $_accionistas = [];
    protected $_tipoContabilidad;


    protected $_montoLineaProveedores;
    protected $_activoLibreGravamen;
    protected $_proyeccionInversion;
    protected $_proyeccionVentas;
    protected $_flgLineaSucesion;
    protected $_lineaSucesion;
    protected $_cambioGerenciaGeneralAnnio;
    protected $_cambioAccionistasAnnio;

    protected $_lineas = [];
    protected $_registro;
    protected $_fechaActualizacion;
    protected $_idHistorico;

    protected $_distrito;
    protected $_provincia;
    protected $_inicioIbk;
    protected $_inicioOperacion;

    protected $_clasificacion;
    protected $_saldoIbk;
    protected $_saldoRcc;

    protected $_idVisita;


    protected $_inversiones = [];

    //Mover a otra entidad
    const MAXIMO_VENCIDO=15000;
    const MAXIMO_LABORAL_COACTIVA=50000;
    const CLASIFICACION_NORMAL=1;

    function setValueToTable() {
        $data = [
            'COD_UNICO' => $this->_codunico,
            'INICIO_IBK' => $this->_inicioIbk,
            'INICIO_OP' => $this->_inicioOperacion,
            'SUBSECTOR' => $this->_subsector,
            'ACTIVIDAD' => $this->_actividad,
            'SUBSECTOR' => $this->_subsector,
            'BACKLOG' => $this->_backlog,
            'MODELO_NEGOCIO' => $this->_modeloNegocio,
            'VENTAJA_COMPETITIVA' => $this->_ventajaCompetitiva,
            'FORTALEZAS_RIESGOS' => $this->_fortalezasRiesgos,
            'INTEGRACION_VERTICAL' => $this->_flagIntegracionVertical,
            'GESTION_COMPRAS' => $this->_gestionesCompra,
            'GERENTE_GENERAL' => $this->_gerenteGeneral,
            'GERENTE_FINANCIERO_TIPO' => $this->_financieroRol,
            'GERENTE_FINANCIERO_NOMBRE' => $this->_financieroNombre,
            'TIPO_CONTABILIDAD' => $this->_tipoContabilidad,
            'REGISTRO' => $this->_registro,
            'FECHA_ACTUALIZACION' => $this->_fechaActualizacion?$this->_fechaActualizacion->format('Y-m-d H:i:s'):null,
            'PROCEDENCIA_MPRIMA' => $this->_procedenciaMateriaPrima,
            'LINEA_SUCESION' => $this->_lineaSucesion,
            'PROYECCION_INVERSION' => $this->_proyeccionInversion,
            'PROYECCION_VENTAS' => $this->_proyeccionVentas,

            'MONTO_LINEA_PROVEEDORES' => $this->_montoLineaProveedores,
            'ACTIVO_LIBRE_GRAVAMEN' => $this->_activoLibreGravamen,
            'LINEA_SUCESION_FLAG' => $this->_flgLineaSucesion,
            'CAMBIO_GERENCIA_GENERAL_ANNIO' => $this->_cambioGerenciaGeneralAnnio,
            'CAMBIO_ACCIONISTAS_ANNIO' => $this->_cambioAccionistasAnnio,
        ];

        $data['stakeholders'] = [];
        $data['lineas'] = [];
        $data['commodities'] = [];
        $data['zonas'] = [];
        $data['mix'] = [];
        $data['canales'] = [];
        $data['inversiones'] = [];

        foreach ($this->_clientes as $cliente) {
            $data['stakeholders'][] = $cliente->setValueToTable();
        }

        foreach ($this->_accionistas as $accionista) {
            $data['stakeholders'][] = $accionista->setValueToTable();
        }
        foreach ($this->_proveedores as $proveedor) {
            $data['stakeholders'][] = $proveedor->setValueToTable();
        }

        foreach ($this->_lineas as $linea) {
            $data['lineas'][] = $linea->setValueToTable();
        }

        foreach ($this->_commodities as $com) {
            $data['commodities'][] = $com->setValueToTable();
        }

        foreach ($this->_zonaClientes as $zona) {
            $data['zonas'][] = $zona->setValueToTable();
        }

        foreach ($this->_zonaOperaciones as $zona) {
            $data['zonas'][] = $zona->setValueToTable();
        }

        foreach ($this->_mixVentas as $mix) {
            $data['mix'][] = $mix->setValueToTable();
        }

        foreach ($this->_inversiones as $inv) {
            $data['inversiones'][] = $inv->setValueToTable();
        }

        foreach ($this->_canalVentas as $can) {
            $data['canales'][] = $can->setValueToTable();
        }

        return $this->cleanArray($data);
    }

    function setValueForEntity($data,$stakeholders,$lineas,$zonas,$commodities,$mixes,$canales,$inversiones) {

        $this->_codunicoConoceme = $data->COD_UNICO_CONOCEME;
        $this->_codunico = $data->COD_UNICO;
        $this->_nombre = $data->NOMBRE;
        $this->_actividad = $data->ACTIVIDAD;
        $this->_subsector = $data->SUBSECTOR;
        $this->_backlog = $data->BACKLOG;
        $this->_modeloNegocio = $data->MODELO_NEGOCIO;
        $this->_ventajaCompetitiva = $data->VENTAJA_COMPETITIVA;
        $this->_fortalezasRiesgos = $data->FORTALEZAS_RIESGOS;
        $this->_flagIntegracionVertical = $data->INTEGRACION_VERTICAL;
        $this->_gestionesCompra = $data->GESTION_COMPRAS;
        $this->_gerenteGeneral = $data->GERENTE_GENERAL;
        $this->_financieroRol = $data->GERENTE_FINANCIERO_TIPO;
        $this->_financieroNombre = $data->GERENTE_FINANCIERO_NOMBRE;
        $this->_tipoContabilidad = $data->TIPO_CONTABILIDAD;
        $this->_procedenciaMateriaPrima = $data->PROCEDENCIA_MPRIMA;
        $this->_proyeccionInversion = $data->PROYECCION_INVERSION;
        $this->_proyeccionVentas = $data->PROYECCION_VENTAS;

        $this->_montoLineaProveedores = $data->MONTO_LINEA_PROVEEDORES;
        $this->_activoLibreGravamen = $data->ACTIVO_LIBRE_GRAVAMEN;
        $this->_flgLineaSucesion = $data->LINEA_SUCESION_FLAG;
        $this->_lineaSucesion = $data->LINEA_SUCESION;

        $this->_cambioGerenciaGeneralAnnio = $data->CAMBIO_GERENCIA_GENERAL_ANNIO;
        $this->_cambioAccionistasAnnio = $data->CAMBIO_ACCIONISTAS_ANNIO;

        $this->_fechaActualizacion = ($data->FECHA_ACTUALIZACION)? new Carbon($data->FECHA_ACTUALIZACION) : null;
        $this->_usuarioRegistro = $data->USUARIOR_REGISTRO;
        $this->_usuarioNombre = $data->USUARIOR_NOMBRE;
        $this->_ventas = $data->VENTAS;

        $this->_distrito = $data->DISTRITO;
        $this->_provincia = $data->PROVINCIA;
        $this->_inicioIbk = $data->INICIO_IBK;
        $this->_inicioOperacion = $data->INICIO_OP;

        $this->_clasificacion = $data->CLASIFICACION;
        $this->_saldoIbk = $data->SALDO_INTERBANK;
        $this->_saldoRcc = $data->SALDO_RCC;
        $this->_cefDJFecha = ($data->METODIZADO_1_FECHA)? new Carbon($data->METODIZADO_1_FECHA) : null;

        $this->_idVisita = isset($data->CONOCEME_VISITA_ID)? $data->CONOCEME_VISITA_ID: null;

        //seteo de sectorista
        $sectorista = new Usuario();
        $sectorista->setValues([
            '_registro' => $data->SECTORISTA_REGISTRO,
            '_nombre' => $data->SECTORISTA_NOMBRE,
            '_rol' => $data->SECTORISTA_ROL,
            '_zona' => $data->SECTORISTA_ZONAL,
            '_centro' => $data->SECTORISTA_JEFATURA,
        ]);
        $this->_ejecutivoSectorista = $sectorista;

        //Seteo de stakeholders
        foreach($stakeholders as $sk){
            $objStake = new ConocemeStakeholder();
            $objStake->setValueForEntity($sk);
            switch ($sk->TIPO) {
                case ConocemeStakeholder::TIPO_CLIENTE:
                    $this->_clientes[] = $objStake;
                    break;
                case ConocemeStakeholder::TIPO_PROVEEDOR:
                    $this->_proveedores[] = $objStake;
                    break;
                case ConocemeStakeholder::TIPO_ACCIONISTA:
                    $this->_accionistas[] = $objStake;
                    break;
            }
        }

        //seteo de lineas
        foreach($lineas as $lin){
            $objLinea = new ConocemeLinea();
            $objLinea->setValueForEntity($lin);
            $this->_lineas[$lin->BANCO] = $objLinea;
        }

        //seteo de commodities
        foreach($commodities as $com){
            $objCommoditie = new ConocemeCommoditie();
            $objCommoditie->setValueForEntity($com);
            $this->_commodities[$com->NOMBRE] = $objCommoditie;
        }

        //seteo de zonas
        foreach($zonas as $zon){
            $objZona = new ConocemeZona();
            $objZona->setValueForEntity($zon);
            switch ($zon->TIPO) {
                case ConocemeZona::TIPO_OPERACIONES:
                    $this->_zonaOperaciones[$zon->ZONA] = $objZona;
                    break;
                case ConocemeZona::TIPO_CLIENTES:
                    $this->_zonaClientes[$zon->ZONA] = $objZona;
                    break;
            }
        }

        foreach($mixes as $mix){
            $objMix = new ConocemeMixVentas();
            $objMix->setValueForEntity($mix);
            $this->_mixVentas[] = $objMix;
        }

        foreach($inversiones as $inv){
            $objInv = new ConocemeInversiones();
            $objInv->setValueForEntity($inv);
            $this->_inversiones[$inv->TIPO_INVERSION] = $objInv;
        }

        foreach($canales as $canal){
            $objCanal = new ConocemeCanalVentas();
            $objCanal->setValueForEntity($canal);
            $this->_canalVentas[] = $objCanal;
        }

        return $data;
    }

    public function getByCodunico($codunico){
        //data de la tabla principal
        $model = new modelConoceme();
        $data = $model->get(parent::sgetPeriodoInfinity(),$codunico)->first();

        //data de stakeholders
        $model2 = new modelConocemeStakeholder();
        $stakeholders = $model2->get($codunico)->get();

        //data de lineas
        $model3 = new modelConocemeLinea();
        $lineas = $model3->get($codunico)->get();

        //data de zonas
        $model4 = new modelConocemeZona();
        $zonas = $model4->get($codunico)->get();

        //data de zonas
        $model5 = new modelConocemeCommoditie();
        $commodities = $model5->get($codunico)->get();

        //data de mix ventas
        $model6 = new modelConocemeMixVentas();
        $mixes = $model6->get($codunico)->get();

        //data canal ventas
        $model7 = new modelConocemeCanalVentas();
        $canales = $model7->get($codunico)->get();

        //data de mix inversiones
        $model8 = new modelConocemeInversiones();
        $inversiones = $model8->get($codunico)->get();

        $this->setValueForEntity($data,$stakeholders,$lineas,$zonas,$commodities,$mixes,$canales,$inversiones);

        return $data;
    }

    static function getHistoriaByCliente($codunico){
        $model = new modelConocemeHistoria();
        return $model->getByCliente(parent::sgetPeriodoInfinity(),$codunico);
    }

    public function getByHistoriaVisita($visita){

        //data de la tabla principal
        $model = new modelConocemeHistoria();
        $data = $model->getByVisita(parent::sgetPeriodoInfinity(),$visita)->first();
        $this->_idHistorico = $data->CONOCEME_HIST_ID;
        //data de stakeholders
        $stakeholders = $model->getStakeholders($data->CONOCEME_HIST_ID)->get();

        $lineas = $model->getLineas($data->CONOCEME_HIST_ID)->get();
        $zonas = $model->getZonas($data->CONOCEME_HIST_ID)->get();
        $commodities = $model->getCommodities($data->CONOCEME_HIST_ID)->get();
        $mixes = $model->getMixVentas($data->CONOCEME_HIST_ID)->get();
        $canales = $model->getCanales($data->CONOCEME_HIST_ID)->get();
        $inversiones = $model->getInversiones($data->CONOCEME_HIST_ID)->get();

        $this->setValueForEntity($data,$stakeholders,$lineas,$zonas,$commodities,$mixes,$canales,$inversiones);
        return $data;
    }

    public function getByHistoriaId($id){
        //data de la tabla principal
        $model = new modelConocemeHistoria();
        $data = $model->getByHistoriaId(parent::sgetPeriodoInfinity(),$id)->first();
        $this->_idHistorico = $data->CONOCEME_HIST_ID;

        //data de stakeholders
        $stakeholders = $model->getStakeholders($data->CONOCEME_HIST_ID)->get();
        $lineas = $model->getLineas($data->CONOCEME_HIST_ID)->get();
        $zonas = $model->getZonas($data->CONOCEME_HIST_ID)->get();
        $commodities = $model->getCommodities($data->CONOCEME_HIST_ID)->get();
        $mixes = $model->getMixVentas($data->CONOCEME_HIST_ID)->get();
        $canales = $model->getCanales($data->CONOCEME_HIST_ID)->get();
        $inversiones = $model->getInversiones($data->CONOCEME_HIST_ID)->get();

        $this->setValueForEntity($data,$stakeholders,$lineas,$zonas,$commodities,$mixes,$canales,$inversiones);

        return $data;
    }

    public function registrar(){
        $model = new modelConoceme();
        $this->_fechaActualizacion = Carbon::now();
        $registrar=is_null($this->_codunicoConoceme);
        $this->_id = $model->registrar($registrar,$this->setValueToTable());

        if($this->_id !== false){
            //Envío de Email
            // if($registrar){
            //     $mail = new \App\Mail\Infinity\CreacionConoceme(
            //         $this->_ejecutivoSectorista
            //         ,$this->getValue('_nombre')
            //         ,$this->getValue('_codunico')
            //         ,route('infinity.me.cliente.conoceme',['cu' => $this->getValue('_codunico')])
            //         );
            // }
            // else{
            //     //Actualización
            //     $mail = new \App\Mail\Infinity\ActualizacionConoceme(
            //         $this->_ejecutivoSectorista
            //         ,$this->getValue('_nombre')
            //         ,$this->getValue('_codunico')
            //         ,route('infinity.me.cliente.conoceme',['cu' => $this->getValue('_codunico')])
            //         );
            // }
            // dispatch(new \App\Jobs\MailQueue($mail));
            //Exito de visita
            $this->setMessage('La ficha conóceme se registró correctamente');
            return true;
        }else{
            $this->setMessage('Hubo un error al registrar sus datos');
            return false;
        }
    }

    public function getRcc(){
        $model = new modelConocemeClienteRcc();
        $data = $model->get(parent::sgetPeriodoInfinity(), $this->_codunico)->get();
        $result = [];
        foreach ($data as $key => $value) {
            $result[$value->TIPO][$value->BANCO] = $value;
        }
        return $result;
    }

    public function politicaCambioClientes(ConocemeCliente $original){

        $clientesTopOriginales = [];
        //si habian clientes que tenian concentracion mayor a 50%-75% se guardan en un clientesTopOriginal
        foreach ($original->_clientes as $c){
            if($c->getValue('_concentracion') >= 3){
                $clientesTopOriginales[] = $c;
            }
        }
        //si no habian clientes top, no aplica la regla
        if (!$clientesTopOriginales){
            return false;
        }

        $results = [];
        //Por cada cliente top que habia se setea verdadero todos, si en caso uno tiene una concentracion mayor o igual al 50%-75%, no se aplica la regla
        foreach ($clientesTopOriginales as $key => $orig) {
            $results[$key]= true;
            foreach ($this->_clientes as $ult) {
                if ($orig->getValue('_documento') == $orig->getValue('_documento') && $ult->getValue('_concentracion') >= 3){
                    $results[$key] = false;
                }
            }
        }
        return $results? max($results):false ;

    }

    public function politicaCambioProveedores(ConocemeCliente $original){
        $clientesTopOriginales = [];
        //si habian proveedores que tenian concentracion mayor a 50%-75% se guardan en un clientesTopOriginal
        foreach ($original->_proveedores as $c){
            if($c->getValue('_concentracion') >= 3){
                $clientesTopOriginales[] = $c;
            }
        }
        //si no habian proveedores top, no aplica la regla
        if (!$clientesTopOriginales){
            return false;
        }


        $results = [];
        //Por cada proveedores top que habia se setea verdadero todos, si en caso uno tiene una concentracion mayor o igual al 50%-75%, no se aplica la regla
        foreach ($clientesTopOriginales as $key => $orig) {
            $results[$key]= true;
            foreach ($this->_proveedores as $ult) {
                if ($orig->getValue('_documento') == $orig->getValue('_documento') && $ult->getValue('_concentracion') >= 3){
                    $results[$key] = false;
                }
            }
        }
        return $results? max($results):false ;
    }

    public function politicaCambioGerenteGeneral(ConocemeCliente $original){
        return strtolower($this->_gerenteGeneral) !== strtolower($original->getValue('_gerenteGeneral'));
    }

    public function politicaCambioAccionistas(ConocemeCliente $original){

        if (count($original->_accionistas) != count($this->_accionistas)){
            return true;
        }

        $results = [];
        foreach ($original->_accionistas as $key => $orig) {
            $results[$key]= true;
            foreach ($this->_accionistas as $ult) {
                if ($orig->getValue('_documento') == $orig->getValue('_documento') && $ult->getValue('_concentracion') == $orig->getValue('_concentracion')){
                    $results[$key] = false;
                }
            }
        }
        return $results? max($results):false ;
    }

    //Políticas de Performance
    //Falta calcular
    //Mejorar lógica de conseguir grupo económico (colocandolo como atributo del objeto ConocemeCliente)
    public function politicaCreditosVencidos(ConocemeCliente $original){
        $model=new modelGrupoEconomico();
        $grupoEconomico=$model->getGrupoEconomicoClienteRelacionado(self::sgetPeriodoInfinity(),$original->_codunico)->get();
        foreach ($grupoEconomico as $registro) {
            if($registro->MONTO_VENCIDO>=self::MAXIMO_VENCIDO)
                return true;
        }
        return false;
    }

    public function politicaLaboralCoactiva(ConocemeCliente $original){
        $model=new modelGrupoEconomico();
        $grupoEconomico=$model->getGrupoEconomicoClienteRelacionado(self::sgetPeriodoInfinity(),$original->_codunico)->get();
        foreach ($grupoEconomico as $registro) {
            if($registro->MONTO_COACTIVA>=self::MAXIMO_LABORAL_COACTIVA and $registro->MONTO_LABORAL>=self::MAXIMO_LABORAL_COACTIVA)
                return true;
        }
        return false;
    }

    public function politicaCalificacionDiferente(ConocemeCliente $original){
        $model=new modelGrupoEconomico();
        $grupoEconomico=$model->getGrupoEconomicoClienteRelacionado(self::sgetPeriodoInfinity(),$original->_codunico)->get();
        foreach ($grupoEconomico as $registro) {
            if($registro->FLG_CLASIFICACION!=self::CLASIFICACION_NORMAL)
                return true;
        }
        return true;
    }

}
