<?php

namespace App\Entity\Infinity;

use App\Model\Infinity\FichaCovid as mFichaCovid;
use Jenssegers\Date\Date as Carbon;
use App\Entity\Usuario as Usuario;

class FichaCovid extends \App\Entity\Base\Entity {

	//NA->ejecutivo, analista O A->jefe o gerencia
	const APROBADO = '1';
	const NECESITA_APROBACION = '0';
	const bajo = 'bajo';
	const medio = 'medio';
	const alto = 'alto';
	const sinImpacto = 'sin impacto';
	const NUEVO = 1;

	function getCumplimientoCategoria($id){
		$model = new mFichaCovid();
		return $model->getCumplimientoCategoria($id);
	}

	function updateEstrategia($data,$registro,$nombre){
		$model = new mFichaCovid();
		return $model->updateEstrategia($data,$registro,$nombre);
	}

	function updateCumplimiento($data,$registro,$nombre,$eliminar){
		$model = new mFichaCovid();
		return $model->updateCumplimiento($data,$registro,$nombre,$eliminar);
	}
	function addCumplimiento($data,$registro,$rol,$nombre){
		$model = new mFichaCovid();
		return $model->addCumplimiento($data,$registro,$rol,$nombre);
	}
	function getDatosByCliente2byID($id){
		$model = new mFichaCovid();
		return $model->getDatosByCliente2byID($id);
	}
	function getDatosCumplimientobyID($id){
		$model = new mFichaCovid();
		return $model->getDatosCumplimientobyID($id);
	}
	function getCumplimientoById($id){
		$model = new mFichaCovid();
		return $model->getCumplimientoById($id);
	}
	
	function save2($data,$registro,$validador,$idVisita=null)
	{
		$model = new mFichaCovid();
		$estadoaprobacion='';
		$valor=NULL;

		if ($validador) {
			$estadoaprobacion=self::APROBADO;
		}else{
			$estadoaprobacion=self::NECESITA_APROBACION;
		}

		if(isset($data['slcQ13_1']) == 'bajo'){
			$valor=self::bajo;
		}
		else if (isset($data['slcQ13_2']) == 'medio'){
			$valor=self::medio;
		}
		else if(isset($data['slcQ13_0']) == 'sin impacto'){
			$valor=self::sinImpacto;
		}else{
			$valor=self::alto;
		}
		$now = Carbon::today()->toDateString();
		$covid=array(
			"COD_UNICO"=>!empty($data['codunico'])?$data['codunico']:NULL,
			"FECHA_REGISTRO"=>!empty($data['fechaVisita'])?$data['fechaVisita']:NULL,
			"ESTADO_APROBACION"=>$estadoaprobacion,
			"USUARIO_CREACION"=>!empty($data['registro'])?$data['registro']:$registro,
			"USUARIO_MODIFICACION"=>$estadoaprobacion==self::APROBADO?$registro:null,
			"ESTADO_REGISTRO"=>self::NUEVO,
			'USUARIO_NOTIFICADO'=>isset($data['rolvalidador'])?$data['rolvalidador']:NULL,
			'USUARIO_ACOMP'=>isset($data['rolvalidador2'])?$data['rolvalidador2']:NULL,
			//'USUARIO_GYS'=>isset($data['analistaGYS'])?$data['analistaGYS']:NULL,
			//'USUARIO_ADMISION'=>isset($data['jefeAdmision'])?$data['jefeAdmision']:NULL,
			'PREGUNTA_1'=>isset($data['slcQ1'])?$data['slcQ1']:NULL,
			'PREGUNTA_2_1'=>isset($data['slcQ2_1'])?$data['slcQ2_1']:NULL,
			'PREGUNTA_2_2'=>isset($data['slcQ2_2'])?$data['slcQ2_2']:NULL,
			'PREGUNTA_2_3_1'=>isset($data['slcQ2_3_1'])?$data['slcQ2_3_1']:NULL,
			'PREGUNTA_2_3_2'=>isset($data['slcQ2_3_2'])?$data['slcQ2_3_2']:NULL,
			'PREGUNTA_3'=>isset($data['slcQ3'])?$data['slcQ3']:NULL,
			'PREGUNTA_4'=>isset($data['slcQ4'])?$data['slcQ4']:NULL,
			'PREGUNTA_5_1'=>isset($data['slcQ5_1'])?$data['slcQ5_1']:NULL,
			'PREGUNTA_5_2'=>isset($data['slcQ5_2'])?$data['slcQ5_2']:NULL,
			'PREGUNTA_6_1'=>isset($data['slcQ6_1'])?$data['slcQ6_1']:NULL,
			'PREGUNTA_6_2'=>isset($data['slcQ6_2'])?$data['slcQ6_2']:NULL,
			'PREGUNTA_7'=>isset($data['slcQ7'])?$data['slcQ7']:NULL,
			'PREGUNTA_7_1'=>isset($data['slcQ7_1'])?$data['slcQ7_1']:NULL,
			'PREGUNTA_7_2_1'=>isset($data['slcQ7_2_1'])?$data['slcQ7_2_1']:NULL,
			'PREGUNTA_7_2_2'=>isset($data['slcQ7_2_2'])?$data['slcQ7_2_2']:NULL,
			'PREGUNTA_7_3'=>isset($data['slcQ7_3'])?$data['slcQ7_3']:NULL,
			'PREGUNTA_7_4'=>isset($data['slcQ7_4'])?$data['slcQ7_4']:NULL,
			'PREGUNTA_8_1'=>isset($data['slcQ8_1'])?$data['slcQ8_1']=='on'?1:0:NULL,
			'PREGUNTA_8_1_1_1'=>isset($data['slcQ8_1_1_1'])?$data['slcQ8_1_1_1']:NULL,
			'PREGUNTA_8_1_1_2'=>isset($data['slcQ8_1_1_2'])?$data['slcQ8_1_1_2']:NULL,
			'PREGUNTA_8_1_2'=>isset($data['slcQ8_1_2'])?$data['slcQ8_1_2']:NULL,
			'PREGUNTA_8_2'=>isset($data['slcQ8_2'])?$data['slcQ8_2']=='on'?1:0:NULL,
			'PREGUNTA_8_3'=>isset($data['slcQ8_3'])?$data['slcQ8_3']=='on'?1:0:NULL,
			'PREGUNTA_8_3_1'=>isset($data['slcQ8_3_1'])?$data['slcQ8_3_1']:NULL,
			'PREGUNTA_8_3_2'=>isset($data['slcQ8_3_2'])?$data['slcQ8_3_2']:NULL,
			'PREGUNTA_9_1'=>isset($data['slcQ9_1'])?$data['slcQ9_1']:NULL,
			'PREGUNTA_9_2'=>isset($data['slcQ9_2'])?$data['slcQ9_2']:NULL,
			'PREGUNTA_10_1'=>isset($data['slcQ10_1'])?$data['slcQ10_1']:NULL,
			'PREGUNTA_10_2'=>isset($data['slcQ10_2'])?$data['slcQ10_2']:NULL,
			'PREGUNTA_11'=>isset($data['slcQ11'])?$data['slcQ11']:NULL,
			'PREGUNTA_11_1_1'=>isset($data['slcQ11_1_1'])?$data['slcQ11_1_1']=='on'?1:0:NULL,
			'PREGUNTA_11_1_2'=>isset($data['slcQ11_1_2'])?$data['slcQ11_1_2']:NULL,
			'PREGUNTA_11_2_1'=>isset($data['slcQ11_2_1'])?$data['slcQ11_2_1']=='on'?1:0:NULL,
			'PREGUNTA_11_2_2'=>isset($data['slcQ11_2_2'])?$data['slcQ11_2_2']:NULL,
			'PREGUNTA_11_3_1'=>isset($data['slcQ11_3_1'])?$data['slcQ11_3_1']=='on'?1:0:NULL,
			'PREGUNTA_11_3_2'=>isset($data['slcQ11_3_2'])?$data['slcQ11_3_2']:NULL,
			'PREGUNTA_11_4_1'=>isset($data['slcQ11_4_1'])?$data['slcQ11_4_1']=='on'?1:0:NULL,
			'PREGUNTA_11_4_2'=>isset($data['slcQ11_4_2'])?$data['slcQ11_4_2']:NULL,
			'PREGUNTA_11_5_1'=>isset($data['slcQ11_5_1'])?$data['slcQ11_5_1']=='on'?1:0:NULL,
			'PREGUNTA_11_5_2'=>isset($data['slcQ11_5_2'])?$data['slcQ11_5_2']:NULL,
			'PREGUNTA_11_6_1'=>isset($data['slcQ11_6_1'])?$data['slcQ11_6_1']=='on'?1:0:NULL,
			'PREGUNTA_11_6_2'=>isset($data['slcQ11_6_2'])?$data['slcQ11_6_2']:NULL,
			'PREGUNTA_12'=>isset($data['slcQ12'])?$data['slcQ12']:NULL,
			'PREGUNTA_12_1'=>isset($data['slcQ12_1'])?$data['slcQ12_1']:NULL,
			'PREGUNTA_12_2'=>isset($data['slcQ12_2'])?$data['slcQ12_2']:NULL,
			'PREGUNTA_12_2_1'=>isset($data['slcQ12_2_1'])?$data['slcQ12_2_1']:NULL,
			'PREGUNTA_12_2_2_1'=>isset($data['slcQ12_2_2_1'])?$data['slcQ12_2_2_1']:NULL,
			'PREGUNTA_12_2_2_2'=>isset($data['slcQ12_2_2_2'])?$data['slcQ12_2_2_2']:NULL,
			'PREGUNTA_13'=>$valor,
			'PREGUNTA_14_1'=>isset($data['slcQ14_1'])?$data['slcQ14_1']:NULL,
			'PREGUNTA_14_2'=>isset($data['slcQ14_2'])?$data['slcQ14_2']:NULL,
			'PREGUNTA_14_3'=>isset($data['slcQ14_3'])?$data['slcQ14_3']:NULL,
			'PREGUNTA_14_4'=>isset($data['slcQ14_4'])?$data['slcQ14_4']:NULL,
			'PREGUNTA_14_5'=>isset($data['slcQ14_5'])?$data['slcQ14_5']:NULL,
			/////////////////////////////////////////////////////////////////////////////////////
			'PREGUNTA2_2_1'=>isset($data['slc2Q2_1'])?$data['slc2Q2_1']:NULL,
			'PREGUNTA2_2_2'=>isset($data['slc2Q2_2'])?$data['slc2Q2_2']:NULL,
			'PREGUNTA2_2_3'=>isset($data['slc2Q2_3'])?$data['slc2Q2_3']=='on'?1:0:NULL,
			'PREGUNTA2_3_1'=>isset($data['slc2Q3_1'])?$data['slc2Q3_1']:NULL,
			'PREGUNTA2_3_2'=>isset($data['slc2Q3_2'])?$data['slc2Q3_2']:NULL,
			'PREGUNTA2_4'=>isset($data['slc2Q4'])?$data['slc2Q4']:NULL,
			'PREGUNTA2_4_1'=>isset($data['slc2Q4_1'])?$data['slc2Q4_1']:NULL,
			'PREGUNTA2_5'=>isset($data['slc2Q5'])?$data['slc2Q5']:NULL,
			'PREGUNTA2_6'=>isset($data['slc2Q6'])?$data['slc2Q6']:NULL,
			'PREGUNTA2_7'=>isset($data['slc2Q7'])?$data['slc2Q7']:NULL,
			'PREGUNTA2_8'=>isset($data['slc2Q8'])?$data['slc2Q8']:NULL,
			'PREGUNTA2_9_1'=>isset($data['slc2Q9_1'])?$data['slc2Q9_1']:NULL,
			'PREGUNTA2_9_2'=>isset($data['slc2Q9_2'])?$data['slc2Q9_2']:NULL,
			'PREGUNTA2_9_3'=>isset($data['slc2Q9_3'])?$data['slc2Q9_3']:NULL,
			'PREGUNTA2_10'=>isset($data['slc2Q10'])?$data['slc2Q10']:NULL,
			'PREGUNTA2_11_1'=>isset($data['slc2Q11_1'])?$data['slc2Q11_1']:NULL,
			'PREGUNTA2_11_2'=>isset($data['slc2Q11_2'])?$data['slc2Q11_2']:NULL,
			'PREGUNTA2_12'=>isset($data['slc2Q12'])?$data['slc2Q12']:NULL,
			'PREGUNTA2_13'=>isset($data['slc2Q13'])?$data['slc2Q13']:NULL,
			'PREGUNTA2_13_1'=>isset($data['slc2Q13_1'])?$data['slc2Q13_1']:NULL,
			'PREGUNTA2_14_1'=>isset($data['slc2Q14_1'])?$data['slc2Q14_1']=='on'?1:0:NULL,
			'PREGUNTA2_14_1_1_1'=>isset($data['slc2Q14_1_1_1'])?$data['slc2Q14_1_1_1']:NULL,
			'PREGUNTA2_14_1_1_2'=>isset($data['slc2Q14_1_1_2'])?$data['slc2Q14_1_1_2']:NULL,
			'PREGUNTA2_14_1_2'=>isset($data['slc2Q14_1_2'])?$data['slc2Q14_1_2']:NULL,
			'PREGUNTA2_14_1_3'=>isset($data['slc2Q14_1_3'])?$data['slc2Q14_1_3']:NULL,
			'PREGUNTA2_14_1_4'=>isset($data['slc2Q14_1_4'])?$data['slc2Q14_1_4']:NULL,
			'PREGUNTA2_14_1_4_1'=>isset($data['slc2Q14_1_4_1'])?$data['slc2Q14_1_4_1']:NULL,
			'PREGUNTA2_14_2'=>isset($data['slc2Q14_2'])?$data['slc2Q14_2']=='on'?1:0:NULL,
			'PREGUNTA2_14_3'=>isset($data['slc2Q14_3'])?$data['slc2Q14_3']=='on'?1:0:NULL,
			'PREGUNTA2_14_3_1'=>isset($data['slc2Q14_3_1'])?$data['slc2Q14_3_1']:NULL,
			'PREGUNTA2_14_3_2'=>isset($data['slc2Q14_3_2'])?$data['slc2Q14_3_2']:NULL,
			'PREGUNTA2_15'=>isset($data['slc2Q15'])?$data['slc2Q15']:NULL,
			'PREGUNTA2_15_1'=>isset($data['slc2Q15_1'])?$data['slc2Q15_1']:NULL,
			'PREGUNTA2_15_2'=>isset($data['slc2Q15_2'])?$data['slc2Q15_2']:NULL,
			'PREGUNTA2_15_2_1'=>isset($data['slc2Q15_2_1'])?$data['slc2Q15_2_1']:NULL,
			'PREGUNTA2_16_1_1'=>isset($data['slc2Q16_1_1'])?$data['slc2Q16_1_1']:NULL,
			'PREGUNTA2_16_1_2'=>isset($data['slc2Q16_1_2'])?$data['slc2Q16_1_2']:NULL,
			'PREGUNTA2_16_2'=>isset($data['slc2Q16_2'])?$data['slc2Q16_2']:NULL,
			'PREGUNTA2_17_1_1'=>isset($data['slc2Q17_1_1'])?$data['slc2Q17_1_1']:NULL,
			'PREGUNTA2_17_1_2'=>isset($data['slc2Q17_1_2'])?$data['slc2Q17_1_2']:NULL,
			'PREGUNTA2_17_2'=>isset($data['slc2Q17_2'])?$data['slc2Q17_2']:NULL,
			'PREGUNTA2_19'=>isset($data['slc2Q19'])?$data['slc2Q19']:NULL,
			'MONTO_CRECER'=>isset($data['montoCrecer'])?$data['montoCrecer']:NULL,
			'DETALLE_CRECER'=>isset($data['detalleCrecer'])?$data['detalleCrecer']:NULL,
			'ID_VISITA'=> isset($idVisita)?$idVisita:null
		);
		if($validador){
			//Se esta aprobando
			$covid['FECHA_MODIFICACION']=$now;
			$covid['FECHA_APROBACION']=$now;
		}else{
			//Se esta guardando
			$covid['FECHA_CREACION']=$now;
			$covid['FECHA_MODIFICACION']=null;
			$covid['FECHA_APROBACION']=null;
		}
		$cumplimiento = [];
		if($data['cumplimientosCounter']>0){
			$array = range(0,$data['cumplimientosCounter']-1);
			foreach ($array as $i) {
				$detalle = null;
				if($data['TIPO_COMPROMISO'.$i]=='Comerciales'){
					$detalle = $data['TIPO_COMPROMISO_DETALLE_Comerciales'.$i];
				}else if($data['TIPO_COMPROMISO'.$i]=='Canalización de flujos IN'){
					$detalle = $data['TIPO_COMPROMISO_DETALLE_Canalización_de_flujos_IN'.$i];
				}else if($data['TIPO_COMPROMISO'.$i]=='Incremento de flujos OUT'){
					$detalle = $data['TIPO_COMPROMISO_DETALLE_Canalización_de_flujos_OUT'.$i];
				}else if($data['TIPO_COMPROMISO'.$i]=='Garantías'){
					$detalle = $data['TIPO_COMPROMISO_DETALLE_Garantías'.$i];
				}else if($data['TIPO_COMPROMISO'.$i]=='Estados Financieros'){
					$detalle = $data['TIPO_COMPROMISO_DETALLE_Estados_Financieros'.$i];
				}else{
					$detalle = $data['TIPO_COMPROMISO_DETALLE_Otros'.$i];
				}
				array_push($cumplimiento,[
					'ID_COVID'=>!empty($data['id'])?$data['id']:NULL,
					'COD_UNICO'=>!empty($data['codunico'])?$data['codunico']:NULL,
					'FECHA_REGISTRO'=>$now,
					'ID_VISITA'=>isset($idVisita)?$idVisita:null,
					'REGISTRO_INFORMACION'=>!empty($data['registro'.$i])?$data['registro'.$i]:$registro,
					'USUARIO_MODIFICACION'=>$estadoaprobacion==self::APROBADO?$registro:null,
					'TIPO_GESTION'=>isset($data['TIPO_GESTION'.$i])?$data['TIPO_GESTION'.$i]=='-Seleccionar-'?NULL:$data['TIPO_GESTION'.$i]:NULL,
					'TIPO_COMPROMISO'=>isset($data['TIPO_COMPROMISO'.$i])?$data['TIPO_COMPROMISO'.$i]=='-Seleccionar-'?NULL:$data['TIPO_COMPROMISO'.$i]:NULL,
					'TIPO_COMPROMISO_DETALLE'=> $detalle,
					'DETALLE_COMPRA'=>isset($data['DETALLE_COMPRA'.$i])?$data['DETALLE_COMPRA'.$i]:NULL,
					'INDICADOR'=>isset($data['INDICADOR'.$i])?$data['INDICADOR'.$i]:NULL,
					'FECHA_COMPROMISO'=>isset($data['FECHA_COMPROMISO'.$i])?$data['FECHA_COMPROMISO'.$i]=='-Seleccionar-'?NULL:$data['FECHA_COMPROMISO'.$i]:NULL,
					'GESTOR'=>isset($data['GESTOR'.$i])?$data['GESTOR'.$i]:NULL,
					'STATUS_COMPROMISO'=>isset($data['STATUS_COMPROMISO'.$i])?$data['STATUS_COMPROMISO'.$i]:NULL,
					'ORDEN'=>$i
				]);
			}
		}
 		return array("covid"=>$covid,"cumplimiento"=>$cumplimiento);
	}

	function getDatosByCliente($codunico)
	{
		$model = new mFichaCovid();	
		return $model->getDatosByCliente($codunico);
	}
	function getDatosByCliente2($id)
	{
		$model = new mFichaCovid();
		return $model->getDatosByCliente2($id);
	}
	static function diasVencimiento($fecha,$status){
		$respuesta = '';
		if($status=='CUMPLIO'){
			$respuesta = 'Compromiso Cumplido';
		}elseif($status=='NO CUMPLIO'){
			$respuesta = 'Compromiso No Cumplido';
		}else{
			$now = Carbon::today();
			$difference = $now->diffInDays(\Carbon\Carbon::parse($fecha), false);
			if($difference==0){
				$respuesta ='Hoy dia vence el compromiso';
			}elseif($difference==1){
				$respuesta = 'Mañana vence el compromiso';
			}elseif($difference==-1){
				$respuesta = 'Ayer se venció el compromiso';
			}elseif($difference>0){
				$respuesta = 'Quedan '.$difference.' días para el vencimiento del compromiso';
			}else{
				$respuesta = 'Han pasado '.$difference.' días desde el vencimiento del compromiso';
			}
		}
		return $respuesta;		
	}
	static function semaforoVencimiento($fecha,$status){
		$respuesta = '';
		if($status!='CUMPLIO'){
			$now = Carbon::today();
			$difference = $now->diffInDays(\Carbon\Carbon::parse($fecha), false);
			$color = '';
			if($difference>=30){
				$color = '#36D152';

			}elseif($difference>0){
				$color = '#FACA26';
			}else{
				$color = '#F92222';
			}
		}else{
			$color = '#36D152';
		}
		return $color;		
	}
	function getDatosByClienteAprobado($cu)
	{
		$model = new mFichaCovid();
		return $model->getDatosByClienteAprobado($cu);
	}
	function ultimoCambioEstrategiaRiesgos($id){
		$model = new mFichaCovid();
		return $model->ultimoCambioEstrategiaRiesgos($id);
	}
	function getCambios($id){
		$model = new mFichaCovid();
		return $model->getCambios($id);
	}
	function getDatosCumplimiento($id,$codunico){
		$model = new mFichaCovid();
		return $model->getDatosCumplimiento($id,$codunico);
	}
	function getDatosCumplimientoIncompletos($id,$codunico){
		$model = new mFichaCovid();
		return $model->getDatosCumplimientoIncompletos($id,$codunico);
	}
}