<?php

namespace App\Entity\Infinity;

use App\Model\Infinity\Cliente as modelCliente;
use App\Model\Infinity\GrupoEconomico as modelGrupoEconomico;
use App\Model\Infinity\ConocemeClientePolitica as modelPolitica;
use Illuminate\Support\Facades\Auth;
use App\Entity\Usuario as Usuario;
use App\Entity\Infinity\Semaforo as Semaforo;
use Jenssegers\Date\Date as Carbon;
use App\Model\TestPolitica as mPoliticas;
use App\Entity\Infinity\PoliticaProceso as PoliticaProceso;

class Cliente extends \App\Entity\Base\Entity {

    protected $_periodo;
    protected $_codunico;
    protected $_nombre;
    protected $_ventas;
    protected $_cefDJFecha;
    protected $_ejecutivoSectorista;

    const HISTORIA_MESES = 4;

    const ID_DDJJ=1;
    const ID_EEFF=2;
    const ID_IBR=3;
    const ID_F02=4;
    const ID_RECALCULO=5;

    const LOGO_SALIDA = 0;
    const LOGO_AZUL = 1;
    const LOGO_GRIS = 2;

    static function getListaClientesInfinity(){
        $model = new modelCliente();

        //dd($model->getListaClientesInfinity()->get());

        return $model->getListaClientesInfinity();
    }

    static function getClienteInfinity(\App\Entity\Usuario $ejecutivo,$cliente){
        $model = new modelCliente();
        return $model->getListaClientesAlerta(self::sgetPeriodoInfinity(),$cliente)->get()->first();
    }

    static function getListaGestiones($cliente){
        $model = new modelCliente();
        return $model->getListaGestiones($cliente)->get();
    }

    static function getListaGestionesComite($cliente){
        $model = new modelCliente();
        return $model->getListaGestionesComite($cliente)->get();
    }

    static function getListaDocumentos($cliente){
        $model = new modelCliente();
        return $model->getListaDocumentos($cliente)->get();
    }

    function guardarGestion($datosGestion,$nombreArchivo,$extension, \App\Entity\Usuario  $usuario,$archivo){
        $model = new modelCliente();
        $hoy=Carbon::now();
        $data=[
            'PERIODO'=>self::sgetPeriodoInfinity(),
            'COD_UNICO'=>$datosGestion['codUnico'],
            'REGISTRO_EN'=>$usuario->getValue('_registro'),
            'FECHA_GESTION'=>$datosGestion['fechaGestion'],
            'FECHA_REGISTRO'=>$hoy,
            'COMENTARIO'=>$datosGestion['comentarioGestion'],
            'ADJUNTO'=>$nombreArchivo.$hoy->format('Y-m-d_H-i-s').'.'.$extension,
            'ESTADO_GESTION'=>$datosGestion['estadoGestion']
        ];

        if ($model->guardarAdjunto($data,$archivo,'WEBBE_INFINITY_GESTION','upload/',null)){
            return true;
        }else{

            $this->setMessage('Hubo un error al registrar su información. Inténtelo más tarde.');
            return false;
        }
    }

    function guardarDocumentacionRecalculo($datosDocumento,$nombreArchivo,$extension, \App\Entity\Usuario  $usuario,$archivo){
        $model = new modelCliente();
        $hoy=Carbon::now();

        //El tipo de documento ==5 es el de recálculo
        if($datosDocumento['tipoDocumento']<=self::ID_EEFF)
            $datosDocumento['fechaFirma']=null;
        
        $data=[
            'COD_UNICO'=>$datosDocumento['codUnico'],
            'NOMBRE_DOCUMENTO'=>$datosDocumento['nombreTipoDocumento'],
            'TIPO_DOCUMENTO'=>$datosDocumento['tipoDocumento'],
            'FECHA_REGISTRO'=>$hoy,
            'FECHA_DOCUMENTO'=>$datosDocumento['tipoDocumento']==self::ID_RECALCULO?$datosDocumento['fechaRecalculo']:$datosDocumento['fechaFirma'],
            'COMENTARIOS'=>$datosDocumento['nombreTipoDocumento'],
            'REGISTRO'=>$usuario->getValue('_registro'),
            'ADJUNTO'=>$nombreArchivo.$hoy->format('Y-m-d_H-i-s').'.'.$extension,
            'MDL'=>$datosDocumento['tipoDocumento']==self::ID_RECALCULO?$datosDocumento['mdlRecalculo']:null,
        ];

        $ruta=$datosDocumento['tipoDocumento']==self::ID_RECALCULO?'recalculo/':'documentacion-detalle/';

        if ($model->guardarAdjunto($data,$archivo,'WEBBE_INFINITY_DOCUMENTACION',$ruta,self::sgetPeriodoInfinity())){
            return true;
        }else{

            $this->setMessage('Hubo un error al registrar su información. Inténtelo más tarde.');
            return false;
        }
    }
    

    static function getEstadosGestion($esJefe){
        $model = new modelCliente();
        return $model->getEstadosGestion($esJefe)->get();
    }

    static function getRecalculo($codunico){
        $model = new modelCliente();
        return $model->getRecalculo($codunico)->first();
    }

    static function getIdPoliticaCliente($id){
        $model = new mPoliticas();
        return $model->getClientePolitica($id);
    }
    
    static function getHistoriaPoliticas($codunico){
        $model = new modelPolitica();
        return $model->get($codunico)->get();
    }

    static function getHistoriaPoliticasSinGestionar($codunico){
        $model = new modelPolitica();
        return $model->get($codunico)->where('WICP.DIAS_GESTION','>',0)->get();
    }
    
    static function getHistoriaClientes($codunico){
        $model = new modelCliente();
        return $model->getHistoriaAlertas(self::getPeriodosPrevios(self::sgetPeriodoInfinity(),self::HISTORIA_MESES),$codunico)->get();
    }

    static function getMeses(){
        return self::getMesesPrevios(self::sgetPeriodoInfinity(),self::HISTORIA_MESES);
    }

    static function getListaClientesAlerta(\App\Entity\Usuario $en,$data = null,$segmento = null,$cliente = null) {
        if ($data!=null) {
            $filtros['banca'] = (!empty($data['banca']) ? $data['banca'] : null);
            $filtros['zonal'] = (!empty($data['zonal']) ? $data['zonal'] : null);
            $filtros['jefatura'] = (!empty($data['jefatura']) ? $data['jefatura'] : null);
            $filtros['ejecutivo'] = (!empty($data['ejecutivo']) ? $data['ejecutivo'] : null);
        }else{
            $filtros=null;
        }
        // dd($filtros);

        $model = new modelCliente();

        // Visibilidad Total
        if (in_array($en->getValue('_rol'),array_merge(Usuario::getDivisionBE(),Usuario::getEquipoInfinity()))){
            $query = $model->getListaClientesAlerta(self::sgetPeriodoInfinity(),$cliente,null,null,null,1,$filtros);
        }

        //Si es Jefe Zonal
        elseif(in_array($en->getValue('_rol'),Usuario::getZonalesBE()) || $en->getValue('_idla')==Usuario::ROL_EJECUTIVO_LA){
            $query = $model->getListaClientesAlerta(self::sgetPeriodoInfinity(),$cliente, $en->getValue('_zona'),null,null,1,$filtros);
        } 

        //Si es Jefatura
        elseif(in_array($en->getValue('_rol'),Usuario::getJefaturasBE())){
            //Requisito es que el jefe pueda ver info de toda la zonal por defecto
            $query = $model->getListaClientesAlerta(self::sgetPeriodoInfinity(),$cliente, $en->getValue('_zona'),$en->getValue('_centro'),null,1,$filtros);
        }

        //Si es Ejecutivo
        elseif($en->getValue('_idla')!=Usuario::ROL_EJECUTIVO_LA || in_array($en->getValue('_rol'), Usuario::getEjecutivosBE())){
            $query = $model->getListaClientesAlerta(self::sgetPeriodoInfinity(),$cliente, $en->getValue('_zona'),$en->getValue('_centro'), $en->getValue('_registro'),1,$filtros);
        }

        if ($segmento!=null) {
            $query = $query->where('WIC.SEGMENTO','=',$segmento);
        }
        $query = $query->where('WIC.FLG_VISIBLE','=','1');
        // $query = $query->where('WIC.FLG_VISIBLE','=','1')->where('WIC.SEGMENTO','=','MEDIANA EMPRESA');

        return $query;
    }

    static function getListaClientesControl(\App\Entity\Usuario $en,$cliente = null) {

        $model = new modelCliente();
        $query = $model->getListaClientesControl(self::sgetPeriodoInfinity(),$cliente);     
        
        return $query;
    }

    static function getListaClientesStats(\App\Entity\Usuario $en,$filtro=null ,$segmento = null) {

        $model = new modelCliente();

        // Visibilidad Total
        if (in_array($en->getValue('_rol'),array_merge(Usuario::getDivisionBE(),Usuario::getEquipoInfinity()))){
            $query = $model->getIndicadoresInfinity(self::sgetPeriodoInfinity(),$filtro,$segmento,null);
        }

        //Si es Jefe Zonal
        elseif(in_array($en->getValue('_rol'),Usuario::getZonalesBE()) || $en->getValue('_idla')==Usuario::ROL_EJECUTIVO_LA){
            $query = $model->getIndicadoresInfinity(self::sgetPeriodoInfinity(),$filtro,$segmento,null, $en->getValue('_zona'));
        } 

        //Si es Jefatura
        elseif(in_array($en->getValue('_rol'),Usuario::getJefaturasBE())){
            //$query = $model->getListaClientesAlerta(self::sgetPeriodoInfinity(),$cliente, $en->getValue('_zona'),$en->getValue('_centro'));
            //Requisito es que el jefe pueda ver info de toda la zonal por defecto
            $query = $model->getIndicadoresInfinity(self::sgetPeriodoInfinity(),$filtro,$segmento,null, $en->getValue('_zona'),$en->getValue('_centro'));
        }

        //Si es Ejecutivo
        elseif ($en->getValue('_idla')!=Usuario::ROL_EJECUTIVO_LA || in_array($en->getValue('_rol'), Usuario::getEjecutivosBE())) {
            $query = $model->getIndicadoresInfinity(self::sgetPeriodoInfinity(),$filtro,$segmento,null, $en->getValue('_zona'),$en->getValue('_centro'), $en->getValue('_registro'));
        }

        //return $query->get()->keyBy('NIVEL_ALERTA');
        return $query;
    }

    static function getListaClientesStats_2(\App\Entity\Usuario $en,$filtro=null ,$segmento = null)
    {
        $model = new modelCliente();

        // Visibilidad Total
        if (in_array($en->getValue('_rol'),array_merge(Usuario::getDivisionBE(),Usuario::getEquipoInfinity()))){
            $query = $model->getIndicadoresInfinity_2(self::sgetPeriodoInfinity(),$filtro,$segmento,null);
        }

        //Si es Jefe Zonal
        elseif(in_array($en->getValue('_rol'),Usuario::getZonalesBE()) || $en->getValue('_idla')==Usuario::ROL_EJECUTIVO_LA){
            $query = $model->getIndicadoresInfinity_2(self::sgetPeriodoInfinity(),$filtro,$segmento,null, $en->getValue('_zona'));
        } 

        //Si es Jefatura
        elseif(in_array($en->getValue('_rol'),Usuario::getJefaturasBE())){
            //$query = $model->getListaClientesAlerta(self::sgetPeriodoInfinity(),$cliente, $en->getValue('_zona'),$en->getValue('_centro'));
            //Requisito es que el jefe pueda ver info de toda la zonal por defecto
            $query = $model->getIndicadoresInfinity_2(self::sgetPeriodoInfinity(),$filtro,$segmento,null, $en->getValue('_zona'),$en->getValue('_centro'));
        }

        //Si es Ejecutivo
        elseif ($en->getValue('_idla')!=Usuario::ROL_EJECUTIVO_LA || in_array($en->getValue('_rol'), Usuario::getEjecutivosBE())) {
            $query = $model->getIndicadoresInfinity_2(self::sgetPeriodoInfinity(),$filtro,$segmento,null, $en->getValue('_zona'),$en->getValue('_centro'), $en->getValue('_registro'));
        }

        //return $query->get()->keyBy('NIVEL_ALERTA');
        return $query;
    }

    //Grupo Económico del Cliente
    static function getGrupoEconomicoCliente ($codUnico){
        $model= new modelGrupoEconomico();
        $grupoCliente=$model->getGrupoEconomicoCliente(self::sgetPeriodoInfinity(),$codUnico)->get();
        $relacionados=$model->getGrupoEconomicoClienteRelacionado(self::sgetPeriodoInfinity(),$codUnico)->get();
        //dd($consultaA,$consultaB);

        $infoGrupoEconomico=array();
        
        if(count($grupoCliente)>0){
            $infoGrupoEconomico=[
                'PERIODO' =>$grupoCliente[0]->PERIODO,
                'COD_UNICO' =>$grupoCliente[0]->COD_UNICO,
                'FLG_AUTONOMIA' =>$grupoCliente[0]->FLG_AUTONOMIA,
                'FLG_CLASIFICACION' =>$grupoCliente[0]->FLG_CLASIFICACION,
                'FLG_VENCIDO' =>$grupoCliente[0]->FLG_VENCIDO,
                'FLG_FEVE' =>$grupoCliente[0]->FLG_FEVE,
                'FLG_REFINANCIADO' =>$grupoCliente[0]->FLG_REFINANCIADO,
                'FLG_REESTRUCTURADO' =>$grupoCliente[0]->FLG_REESTRUCTURADO,
                'FLG_JUDICIAL' =>$grupoCliente[0]->FLG_JUDICIAL,
                'FLG_COBRANZA' =>$grupoCliente[0]->FLG_COBRANZA,
                'RELACIONADOS' => [] 
            ];

            foreach ($relacionados as $registro) {
                $infoGrupoEconomico['RELACIONADOS'][]=$registro;
            }
        }
        //dd($infoGrupoEconomico);

        return $infoGrupoEconomico;
    }
    
    static function proceso_politicas()
    {
        $proceso=modelCliente::proceso_politicas();
        return $proceso;
    }

    static function getVisitasConfirmadas()
    {
        $visitasConfirmadas=modelCliente::getVisitasConfirmadas();
        return $visitasConfirmadas;
    }

    static function getGestionCambiosLA($cu)
    {
        $data=modelCliente::getGestionCambiosLA($cu);
        return $data;
    }

    static function getDatosCliente($codcliente)
    {
        $data=modelCliente::getDatosCliente($codcliente,self::sgetPeriodoInfinity());
        return $data;
    }

    static function getDatosClienteVISITAMECOVID($codcliente)
    {
        $data=modelCliente::getDatosClienteVISITAMECOVID($codcliente);
        return $data;
    }

    static function PoliticasComiteCliente($codcliente)
    {
        $data=modelCliente::PoliticasComiteCliente(self::sgetPeriodoInfinity(),$codcliente);
        return $data;
    }

    static function getUsuarioComite($rmg,$rating)
    {
        $data=modelCliente::getUsuarioComite($rmg,$rating);
        return $data;
    }
}