<?php

namespace App\Entity\Infinity;

use App\Model\Infinity\VariableInformativa as modelVariableInformativa;

class VariableInformativa extends \App\Entity\Base\Entity {

    const TIPO_DATO_NUMERICO = 'numerico';

    static function getNegativos($codunico) {
        $model = new modelVariableInformativa();
        $data = $model->get($codunico, array(parent::sgetPeriodoInfinity()))->get();
        $result = [];
        foreach ($data as $item) {
            // if ($item->TIPO_DATO == self::TIPO_DATO_NUMERICO) {
            //     if (($item->FLG_POSITIVO == 1 && $item->VALOR > 0) ||
            //             ($item->FLG_POSITIVO == 0 && $item->VALOR < 0)) {
            // if ($item->FLG_POSITIVO == 2) {
            //     $result[$item->GRUPO][$item->PERIODO][] = $item->DESCRIPCION.' ('.$item->VALOR_2.')';
            // }else{
                if ($item->FLG_POSITIVO=='1') {
                    $result[$item->GRUPO][$item->PERIODO][] = $item->DESCRIPCION;
                }
            // }
            //     }
            // }
        }
        // dd($result);
        return $result;
    }
}