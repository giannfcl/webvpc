<?php

namespace App\Entity\Infinity;

use Jenssegers\Date\Date as Carbon;
use App\Model\Infinity\Cliente as modelCliente;
use App\Model\TestPolitica as mPoliticaProceso;

class Salida extends \App\Entity\Base\Entity {

    protected $_politicaClienteId;
    protected $_codunico;
    protected $_fechaReingreso;
    protected $_fechaSalida;
    protected $_tipoSalida;
    protected $_registro;
    protected $_politica;
    protected $_lineas;

    const TIPO_COMITE = 'Comite';
    
    function setValueToTable() {
        $data = [
            'POLITICA_CLIENTE_ID' => $this->_politicaClienteId,
            'COD_UNICO' => $this->_codunico,
            'FECHA_REINGRESO' => ($this->_fechaReingreso)? $this->_fechaReingreso->format('Y-m-d H:i:s') : null,
            'FECHA_SALIDA' => ($this->_fechaSalida)? $this->_fechaSalida->format('Y-m-d H:i:s'): null,
            'TIPO_SALIDA' => $this->_tipoSalida,
            'REGISTRO_EN' => $this->_registro,
            'LINEAS' => $this->_lineas,
        ];
        return $this->cleanArray($data);
    }

    function setValueForEntity($data) {
        $this->_politicaClienteId = $data->POLITICA_CLIENTE_ID;
        $this->_tipoSalida = $data->TIPO_SALIDA;
        $this->_fechaSalida = Carbon::createFromFormat('Y-m-d H:i:s',substr($data->FECHA_SALIDA,0,19));
        $this->_fechaReingreso = $data->FECHA_REINGRESO? Carbon::createFromFormat('Y-m-d H:i:s',substr($data->FECHA_REINGRESO,0,19)) : null;
        $this->_registro = $data->REGISTRO_EN;
        $this->_codunico = $data->COD_UNICO;
        $this->_politica = $data->POLITICA_ID;
        $this->_lineas = $data->LINEAS;
        return $data;
    }

    function buscarUltimaByCodunico($codUnico){
        $model = new mPoliticaProceso();
        $data = $model->getUltimaByCodunico($codUnico);
        $this->setValueForEntity($data);
        return $data;
    }

    static function getLineas($codUnico)
    {
        $model = new modelCliente();
        return $model->getLineas($codUnico);
    }

    function getSalidaByClientePolitica($id)
    {
        $model = new mPoliticaProceso();
        return $model->getSalidaByClientePolitica($id);
    }

}
