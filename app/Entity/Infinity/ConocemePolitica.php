<?php

namespace App\Entity\Infinity;
class ConocemePolitica extends \App\Entity\Base\Entity {
    
    protected $_periodo;
    protected $_codUnico;
    //protected $_tipo;
    protected $_nombre;
    protected $_fechaRegistro;
    protected $_fechaVencimiento;
    protected $_gestionado;
    
    const CONCENTRACION_CLIENTES = 1;
    const CONCENTRACION_PROVEEDORES = 2;
    const CAMBIO_GERENTE_GENERAL = 3;
    
    const CAMBIO_ACCIONISTA = 4;
    const CAMBIO_CALIFICACION_ACCIONISTA = 5;
    const INVERSIONES_RELEVANTES = 6;
    const DESVIO_FONDOS = 7;
    const CAMBIO_MODELO_NEGOCIO = 11;

    //Políticas de Performance
    const CREDITOS_VENCIDOS = 8;
    const LABORAL_COACTIVA = 9;
    const CALIFICACION_DIFERENTE = 10;

    const DIAS_POLITICAS_JEFE=9;
    const DIAS_POLITICAS_COMITE=9;

    const POL_CUALITATIVA="CUALITATIVA";
    const POL_PERFORMANCE="PERFORMANCE";
    const POL_SEMAFORO="SEMAFORO";
    
    function setValueToTable() {
        $data = [
            'PERIODO' => $this->_periodo,
            'COD_UNICO' => $this->_codUnico,
            //'TIPO_POLITICA' => $this->_tipo,
            'POLITICA_ID' => $this->_nombre,
            'FECHA_REGISTRO' => $this->_fechaRegistro->format('Y-m-d H:i:s'),
            'FECHA_VENCIMIENTO' => $this->_fechaVencimiento->format('Y-m-d H:i:s'),
            'FLG_GESTIONADO'=>$this->_gestionado,
        ];
        return $this->cleanArray($data);
    }
    
    function getNivelPolitica(){
        switch ($this->_nombre) {
            case self::CAMBIO_GERENTE_GENERAL:
            case self::CONCENTRACION_CLIENTES:
            case self::CONCENTRACION_PROVEEDORES:
            case self::CREDITOS_VENCIDOS:
            case self::LABORAL_COACTIVA:
            case self::CALIFICACION_DIFERENTE:
                return Gestion::NIVEL_EXPLICACION_OBSERVACION;
            case self::CAMBIO_CALIFICACION_ACCIONISTA:
            case self::INVERSIONES_RELEVANTES:
            case self::DESVIO_FONDOS:
            case self::CAMBIO_ACCIONISTA:
            case self::CAMBIO_MODELO_NEGOCIO:
            default:
                return Gestion::NIVEL_EXPLICACION_COMITE;
        }
    }   

}