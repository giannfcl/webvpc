<?php
namespace App\Entity\Infinity;

use App\Model\TestPolitica as modelPolitica;


class Politica extends \App\Entity\Base\Entity {

    //Constantes Politicas
    
    const POLITICA_CRED_VENC_MAYOR_15M = 8;
    const POLITICA_DEUD_LAB_COAC_MAYOR_50M = 9;
    const POLITICA_CALIF_DIF_NORMAL = 10;
    
    const POLITICA_1AMBAR = 12;
    const POLITICA_2AMBAR = 13;
    const POLITICA_1NEGRO = 15;
    const POLITICA_1ROJO = 14;
    const POLITICA_DDJJ = 16;
    const POLITICA_EEFF = 17;
    const POLITICA_IBR = 18;
    const POLITICA_F02 = 19;
    const POLITICA_2AMBAR_AR = 20;
    const POLITICA_1ROJO_2 = 23;
    
    const POLITICA_TIPO_SEMAFORO = 'Semáforo';
    const POLITICA_TIPO_PERFORMANCE = 'Performance';
    const POLITICA_TIPO_DOCUMENTARIA = 'documentaria';
    const POLITICA_TIPO_CUALITATIVA = 'Cualitativa';
    
    const POLITICA_CAMBIO_CON_CLIENTES = 1;
    const POLITICA_CAMBIO_CON_PROVEEDORES = 2;
    const POLITICA_CAMBIO_GERENCIA_GENERAL = 3;
    const POLITICA_CAMBIO_ACCIONARADO = 4;
    const POLITICA_CAMBIO_CALIF_ACCS = 5;   //TODO
    const POLITICA_INV_50 = 6;
    const POLITICA_PREST_DESV_FONDOS = 7;
    const POLITICA_CAMBIO_MODELO_NEG = 11;

   
    //CONSTANTES SEVERIDAD
    const NIVEL_SEVERIDAD_SALIDA = 1;
    const NIVEL_SEVERIDAD_COMITE = 2;
    const NIVEL_SEVERIDAD_OBSERVACION = 3;


    static function get($id) {
        $model = new modelPolitica();
        //if (!\Cache::has('politica-'.$id)){
            $data = $model->getPolitica($id)->first();
            //\Cache::put('politica-'.$id,$data,999999);
        //}else{
            //$data = \Cache::get('politica-'.$id);
        //}
        return $data;
    }

    static function politcasComiteCualitativas(){
        return [
            self::POLITICA_CAMBIO_ACCIONARADO,
            self::POLITICA_CAMBIO_CALIF_ACCS,
            self::POLITICA_INV_50,
            self::POLITICA_PREST_DESV_FONDOS,
            self::POLITICA_CAMBIO_MODELO_NEG,
            self::POLITICA_1ROJO_2];
    }

    static function politicasExplicativas(){
        return [
            self::POLITICA_CAMBIO_CON_CLIENTES,
            self::POLITICA_CAMBIO_CON_PROVEEDORES,
            self::POLITICA_CAMBIO_GERENCIA_GENERAL,
            self::POLITICA_CRED_VENC_MAYOR_15M,
            self::POLITICA_DEUD_LAB_COAC_MAYOR_50M,
            self::POLITICA_CALIF_DIF_NORMAL,
            self::POLITICA_1AMBAR,
        ];
    }

    static function politcasDocumentarias(){
        return [
            self::POLITICA_DDJJ,
            self::POLITICA_EEFF,
            self::POLITICA_IBR,
            self::POLITICA_F02];
    }
}