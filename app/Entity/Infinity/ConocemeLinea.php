<?php

namespace App\Entity\Infinity;

use App\Model\Infinity\Cliente as modelCliente;
use Illuminate\Support\Facades\Auth;
use App\Entity\Usuario as Usuario;

class ConocemeLinea extends \App\Entity\Base\Entity {

    protected $_codunico;
    protected $_banco;
    protected $_linea;
    protected $_tipoGarantia;

    function setValueToTable() {
        $data = [
            'COD_UNICO' => $this->_codunico,
            'BANCO' => $this->_banco,
            'LINEA' => $this->_linea,
            'TIPO_GARANTIA' => $this->_tipoGarantia,
        ];
        //return $this->cleanArray($data);
        return $data;
    }

    function setValueForEntity($data) {
        $this->_codunico = $data->COD_UNICO;
		$this->_banco = $data->BANCO;
		$this->_linea = $data->LINEA; 
		$this->_tipoGarantia = $data->TIPO_GARANTIA;
        return $data;
    }

}