<?php

namespace App\Entity\Infinity;

use App\Model\Infinity\Resumen as mResumen;
use Illuminate\Support\Facades\Auth;
use App\Entity\Usuario as Usuario;

class Resumen extends \App\Entity\Base\Entity {

    static function graficarGestion($busqueda=[]){
        $model = new mResumen();
        return $model->graficarGestion(self::sgetPeriodoInfinity(),$busqueda)->get();    
    }

    static function graficarMora($busqueda=[]){
        $model = new mResumen();
        return $model->graficarMora(self::sgetPeriodoInfinity(),self::sgetPeriodoInfinityAnt(),$busqueda)->get();    
    }

    static function graficarLineal($busqueda=[]){
        $model = new mResumen();
        return $model->graficarLineal($busqueda)->get();    
    }

    static function getMovimientos($busqueda=[]){
        $model = new mResumen();
        return $model->getMovimientos(self::sgetPeriodoInfinity(),self::sgetPeriodoInfinityAnt(),$busqueda);        
    }

    static function getMigraciones($busqueda=[]){
        $model = new mResumen();
        return $model->getMigraciones(self::sgetPeriodoInfinity(),$busqueda)->first();        
    }

    static function getComposicionCartera($busqueda=[]){
        $model = new mResumen();
        $composicion=$model->getComposicionCartera(self::sgetPeriodoInfinity(),self::sgetPeriodoInfinityAnt(),$busqueda)->first();       

        $arregloComposicion=[];
        $arregloComposicion[]=['COLOR'=>'green',
            'CLIENTE_ACT'=>$composicion->CLIENTES_VERDE_ACTUAL,
            'CLIENTE_ANT'=>$composicion->CLIENTES_VERDE_ANT,
            'SALDO_ACT'=>$composicion->SALDO_VERDE_ACTUAL,
            'SALDO_ANT'=>$composicion->SALDO_VERDE_ANT
            ];
        $arregloComposicion[]=['COLOR'=>'#fc3',
            'CLIENTE_ACT'=>$composicion->CLIENTES_AMBAR_ACTUAL,
            'CLIENTE_ANT'=>$composicion->CLIENTES_AMBAR_ANT,
            'SALDO_ACT'=>$composicion->SALDO_AMBAR_ACTUAL,
            'SALDO_ANT'=>$composicion->SALDO_AMBAR_ANT
            ];
        $arregloComposicion[]=['COLOR'=>'red',
            'CLIENTE_ACT'=>$composicion->CLIENTES_ROJO_ACTUAL,
            'CLIENTE_ANT'=>$composicion->CLIENTES_ROJO_ANT,
            'SALDO_ACT'=>$composicion->SALDO_ROJO_ACTUAL,
            'SALDO_ANT'=>$composicion->SALDO_ROJO_ANT
            ];

        return $arregloComposicion;
    }


}