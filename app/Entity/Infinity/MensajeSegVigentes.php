<?php

namespace App\Entity\Infinity;

use App\Model\Infinity\Alertacartera as mAlertacartera;
use App\Model\Infinity\AlertasVigente as mAlertaVigente;
use App\Entity\Notification as eNotification;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Date\Date as Carbon;
use App\Entity\Infinity\ClienteRevision as ClienteRevision;

class MensajeSegVigentes extends \App\Entity\Base\Entity 
{
    protected $_id_caso;
    protected $_orden;
    protected $_tipo;
    protected $_gestion;
    protected $_accion;
    protected $_mensaje;
    protected $_registro;
    protected $_fecha_registro;
    protected $_estado;

    function setValueToTable() {
        $data = [
            'ID_CASO' => $this->_id_caso,
            'ORDEN' => $this->_orden,
            'TIPO' => $this->_tipo,
            'GESTION' => $this->_gestion,
            'ACCION' => $this->_accion,
            'MENSAJE' => $this->_mensaje,
            'REGISTRO' => $this->_registro,
            'FECHA_REGISTRO' => $this->_fecha_registro->format('Y-m-d H:i:s'),
            'ESTADO' => $this->_estado,
        ];
        return $this->cleanArray($data);
    }

    static function getAntGestiones($cod_unico)
	{
		return mAlertaVigente::getAntGestiones($cod_unico, self::sgetPeriodoInfinity());
    }
    
    static function getGestionActual($cod_unico)
    {
        return mAlertaVigente::getGestionActual($cod_unico, self::sgetPeriodoInfinity());
    }
    static function getMensajes($id)
    {
        return mAlertaVigente::getMensajes($id);
    }

    static function getUltMensajes($cod_unico)
    {
        if (isset(mAlertaVigente::getGestionActual($cod_unico, self::sgetPeriodoInfinity())[0])) {
            return mAlertaVigente::getMensajes(mAlertaVigente::getGestionActual($cod_unico, self::sgetPeriodoInfinity())[0]->ID);
        }
        return null;
    }

    static function getMensajesAntiguos($id)
    {
        return mAlertaVigente::getMensajesAntiguos($id);
    }

    function nuevomensaje($datos,$usuario)
    {
        // dd($datos['noti_codunico'],$datos['noti_idcaso'],$usuario->getValue('_rol'));
        $model = new mAlertaVigente();
        $notificaciones = $model->getNotificacion($datos['noti_codunico'],$datos['noti_idcaso'],$usuario->getValue('_rol'));
        // dd($notificaciones);
        
        foreach ($notificaciones as $key => $value) {
            $objnotificacion = new eNotification();
            $objnotificacion->setValue('_idTipo', $value->ID_TIPO_NOTIFICACION);
            $objnotificacion->setValue('_registro', $value->REGISTRO_EN);
            $objnotificacion->setValue('_contenido', $value->CONTENIDO);
            $objnotificacion->setValue('_fechaNotificacion', Carbon::now());
            $objnotificacion->setValue('_flgLeido', $value->FLG_LEIDO);
            $objnotificacion->setValue('_fechaLeido', $value->FECHA_LEIDO);
            $objnotificacion->setValue('_valor1', $value->VALOR_1);
            $objnotificacion->setValue('_valor2', $value->VALOR_2);
            $objnotificacion->setValue('_valor3', $value->VALOR_3);
            $objnotificacion->setValue('_valor4', $value->VALOR_4);
            $objnotificacion->setValue('_url', $value->URL);

            $notificaciones_agregar[] = $objnotificacion->setValueToTable();
        }
        // dd($notificaciones,$notificaciones_agregar);
        $this->_registro = $usuario->getValue('_registro');
        $this->_id_caso = $datos['noti_idcaso'];
        $this->_orden = $datos['sigorden'];
        $this->_gestion = NULL;
        $this->_accion = $datos['accion'];
        $this->_mensaje = $datos['mensaje'];
        $this->_fecha_registro = Carbon::now();
        $this->_estado = in_array($usuario->getValue('_rol'), array('20','21')) ? 0 : (isset($datos['aprobacion']) ? $datos['aprobacion'] : null);
        $this->_tipo = $datos['tocaresponder'];
        if (in_array($usuario->getValue('_rol'), array('20','21'))) {
            $this->_tipo = 'EN';
        }elseif (in_array($usuario->getValue('_rol'), array('25','30','34','35','38','42','49','52'))) {
            $this->_tipo = 'COMITE';
        }
        if (isset($datos['tiempoespera']) && $datos['aprobacion']==1) {
            $clirevision = new ClienteRevision();
            $clirevision->setValues([
                '_fechafin' => Carbon::now()->addMonths($datos['tiempoespera']),
                '_codUnico' => $datos['noti_codunico'],
                '_fecharegistro' => Carbon::now(),
                '_tiempo' => $datos['tiempoespera'],
                '_Idpoliticacliente' => null,
                '_Idpolitica' => 24,
                '_flgvigente' => 1
            ]);
        }
        // dd($datos,$usuario->getValue('_registro'),$notificaciones_agregar,$this->setValueToTable(),isset($clirevision) ? $clirevision->setValueToTable() : null);
        return $model->addMensaje($this->setValueToTable(),$notificaciones_agregar,isset($clirevision) ? $clirevision->setValueToTable() : null);
    }

    static function getdatosCaso($idcaso)
    {
        $model = new mAlertaVigente();
        return $model->getdatosCaso($idcaso);
    }
}
?>