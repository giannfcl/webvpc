<?php

namespace App\Entity\Infinity;

use App\Model\Infinity\Cliente as modelCliente;
use Illuminate\Support\Facades\Auth;
use App\Entity\Usuario as Usuario;

class ConocemeZona extends \App\Entity\Base\Entity {

    const TIPO_OPERACIONES = 'OPERACIONES';
    const TIPO_CLIENTES = 'CLIENTES';

    protected $_codunico;
    protected $_tipo;
    protected $_zona;

    function setValueToTable() {
        $data = [
            'COD_UNICO' => $this->_codunico,
            'TIPO' => $this->_tipo,
            'ZONA' => $this->_zona,
        ];
        //return $this->cleanArray($data);
        return $data;
    }

    function setValueForEntity($data) {
        $this->_codunico = $data->COD_UNICO;
        $this->_tipo = $data->TIPO;
        $this->_zona = $data->ZONA;
        return $data;
    }

}
