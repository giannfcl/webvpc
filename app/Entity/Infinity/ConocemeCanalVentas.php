<?php

namespace App\Entity\Infinity;

use App\Model\Infinity\Cliente as modelCliente;
use Illuminate\Support\Facades\Auth;
use App\Entity\Usuario as Usuario;

class ConocemeCanalVentas extends \App\Entity\Base\Entity {

    protected $_codunico;
    protected $_canal;
    protected $_participacion;

    static function getCanales() {
        return ['Minorista', 'Mayorista', 'Exportador', 'Cliente Final'];
    }

    function setValueToTable() {
        $data = [
            'COD_UNICO' => $this->_codunico,
            'CANAL_VENTAS' => $this->_canal,
            'PARTICIPACION' => $this->_participacion,
        ];


        //return $this->cleanArray($data);
        return $data;
    }

    function setValueForEntity($data) {
        $this->_codunico = $data->COD_UNICO;
        $this->_canal = $data->CANAL_VENTAS;
        $this->_participacion = $data->PARTICIPACION;
        return $data;
    }

}
