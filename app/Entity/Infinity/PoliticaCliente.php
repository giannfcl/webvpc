<?php
namespace App\Entity\Infinity;
use App\Model\TestPolitica as mPoliticas;
use App\Model\Infinity\Cliente as modelCliente;
use Jenssegers\Date\Date as Carbon;

class PoliticaCliente extends \App\Entity\Base\Entity {

    protected $_id;
    protected $_codunico;
    protected $_fechaCaida;
    protected $_politica;
    protected $_nivel;
    protected $_diasGestion;
    protected $_estadoGestion;
    protected $_fechaGestion;
    protected $_registro;
    protected $_estadoFinalGestion;
    protected $_flgVigente;

    function setValueToTable() {
        $data = [
            'ID_POLITICA_CLIENTE' => $this->_id,
            'COD_UNICO' => $this->_codunico,
            'FECHA_CAIDA' => ($this->_fechaCaida)? $this->_fechaCaida->format('Y-m-d H:i:s'):null,
            'POLITICA_ID' => $this->_politica,
            'NIVEL' => $this->_nivel,
            'DIAS_GESTION' => $this->_diasGestion,
            'ESTADO_GESTION' => $this->_estadoGestion,
            'FECHA_GESTION' => ($this->_fechaGestion)? $this->_fechaGestion->format('Y-m-d H:i:s'):null,
            'REGISTRO_EN' => $this->_registro,
            'ESTADO_FINAL_GESTION' => $this->_estadoFinalGestion,
            'FLG_VIGENTE' => $this->_flgVigente,
        ];
        return $this->cleanArray($data);
    }

    function getObjeto(){
        return (object) [
            'COD_UNICO' => $this->_codunico,
            'FECHA_CAIDA' => ($this->_fechaCaida)? $this->_fechaCaida->format('Y-m-d H:i:s'):null,
            'POLITICA_ID' => $this->_politica,
            'NIVEL' => $this->_nivel,
            'DIAS_GESTION' => $this->_diasGestion,
        ];
    }

    function setValueForEntity($data){
        $this->_id = $data->ID_POLITICA_CLIENTE;
        $this->_codunico = $data->COD_UNICO;
        $this->_fechaCaida = Carbon::createFromFormat('Y-m-d H:i:s',$data->FECHA_CAIDA);
        $this->_politica = $data->POLITICA_ID;
        $this->_nivel = $data->NIVEL;
        return $data;
    }
}