<?php

namespace App\Entity\Infinity;

use App\Model\Infinity\Cliente as modelCliente;
use Illuminate\Support\Facades\Auth;
use App\Entity\Usuario as Usuario;

class ConocemeInversiones extends \App\Entity\Base\Entity {

    protected $_codunico;
    protected $_tipoInversion;

    function setValueToTable() {
        $data = [
            'COD_UNICO' => $this->_codunico,
            'TIPO_INVERSION'=>$this->_tipoInversion           
        ];
        //return $this->cleanArray($data);
        return $data;
    }

    function setValueForEntity($data) {
        $this->_codunico = $data->COD_UNICO;
        $this->_tipoInversion=$data->TIPO_INVERSION;       
        return $data;
    }

}
            