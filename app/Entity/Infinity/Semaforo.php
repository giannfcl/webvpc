<?php

namespace App\Entity\Infinity;


class Semaforo extends \App\Entity\Base\Entity {

	const SEMAFORO_NEGRO_1 = 99;
	const SEMAFORO_NEGRO_2 = 999;

	const SEMAFORO_VERDE_1 = 1;
	const SEMAFORO_VERDE_2 = 11;

	const SEMAFORO_AMBAR_1 = 2;
	const SEMAFORO_AMBAR_2 = 22;

	const SEMAFORO_ROJO_1 = 3;
	const SEMAFORO_ROJO_2 = 33;

    
    const SEMAFOROS_ME_NOMBRES = [1 => 'Verde',2=>'Ambar',3=>'Rojo',11=>'Verde',22=>'Ambar',33=>'Rojo',99=>'Negro',999=>'Negro'];
    const SEMAFOROS_ME_COLORES = [1 => '#26B99A',2=>'#F0AD4E',3=>'#D9534F',11 => '#26B99A',22=>'#F0AD4E',33=>'#D9534F',99=>'#000000',999=>'#000000'];

    static function getColor($id){
        return $id? self::SEMAFOROS_ME_COLORES[$id] : '#FFFFFF';
    }

    static function getNombre($id){
        return self::SEMAFOROS_ME_NOMBRES[$id];
    }

    static function getSemaforosNegros(){
    	return [self::SEMAFORO_NEGRO_1,self::SEMAFORO_NEGRO_2];
    }

    static function getSemaforosRojos(){
    	return [self::SEMAFORO_ROJO_1,self::SEMAFORO_ROJO_2];
    }

    static function getSemaforosAmbar(){
    	return [self::SEMAFORO_AMBAR_1,self::SEMAFORO_AMBAR_2];
    }

    static function getSemaforosVerde(){
    	return [self::SEMAFORO_VERDE_1,self::SEMAFORO_VERDE_2];
    }
}