<?php

namespace App\Entity\Infinity;

use App\Model\Infinity\Cliente as modelCliente;
use Illuminate\Support\Facades\Auth;
use App\Entity\Usuario as Usuario;

class ConocemeCommoditie extends \App\Entity\Base\Entity {

    protected $_codunico;
    protected $_nombre;

    function setValueToTable() {
        $data = [
            'COD_UNICO' => $this->_codunico,
            'NOMBRE' => $this->_nombre,
        ];
        //return $this->cleanArray($data);
        return $data;
    }

    function setValueForEntity($data) {
        $this->_codunico = $data->COD_UNICO;
        $this->_nombre = $data->NOMBRE;
        return $data;
    }

}
