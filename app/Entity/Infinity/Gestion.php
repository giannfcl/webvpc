<?php

namespace App\Entity\Infinity;

use Jenssegers\Date\Date as Carbon;
use App\Model\Infinity\Cliente as modelCliente;

class Gestion extends \App\Entity\Base\Entity {

    protected $_periodo;
    protected $_codUnico;
    protected $_registro;
    protected $_fechaGestion;
    protected $_fechaRegistro;
    protected $_comentario;
    protected $_adjunto;
    protected $_archivo;
    protected $_extension;
    protected $_estado;
    protected $_ruta;
    protected $_PoliticaAsociada;
    protected $_DecisionComite;
    protected $_Idpoliticacliente;
    
    const RUTA = '/infinity/gestiones/';

    const ESTADO_REVISADO = 1;
    const ESTADO_GESTIONADO = 2;
    const ESTADO_VENCIDO=5;

    const ESTADO_COMENTARIO = 3;
    const ESTADO_COMITE = 4;
    
    
    const NIVEL_EXPLICACION_OBSERVACION = 1;
    const NIVEL_EXPLICACION_COMITE = 2;

    const DECISION_APROBADO = 1;
    const DECISION_RECHAZADO = 0;
    
    static function getEstadoNombre($flgInfinity,$flgGestionable,$estado){
        if($flgInfinity == 0 || $flgGestionable == 0 || $flgGestionable==null){
            return '';
        }
        
        switch ($estado) {
            case self::ESTADO_REVISADO:
                return 'Revisado';
            case self::ESTADO_GESTIONADO:
                return 'Gestionado';
            case self::ESTADO_VENCIDO:
                return 'Vencido';
            default:
                return 'Pendiente';
        }
    }
    
    static function getNivelNombre($flgInfinity,$estado){
        if($flgInfinity == 0 || $estado == null){
            return '';
        }
        
        switch ($estado) {
            case self::NIVEL_EXPLICACION_OBSERVACION:
            case self::NIVEL_EXPLICACION_COMITE:
                return 'Con Observación';
            default:
                return '';
        }
    }
    
    function setValueToTable() {
        $data = [
            'COD_UNICO' => $this->_codUnico,
            'REGISTRO_EN' => $this->_registro,
            'FECHA_GESTION' => $this->_fechaGestion->format('Y-m-d H:i:s'),
            'FECHA_REGISTRO' => $this->_fechaRegistro->format('Y-m-d H:i:s'),
            'COMENTARIO' => $this->_comentario,
            'ADJUNTO' => $this->_adjunto,
            'ESTADO_GESTION' => $this->_estado,
            'POLITICA_ID' => $this->_PoliticaAsociada,
            'DECISION' => $this->_DecisionComite,
            'ID_POLITICA_CLIENTE' => $this->_Idpoliticacliente,
        ];
        return $this->cleanArray($data);
    }

    function registrar($clirevision=null) {
        $model = new modelCliente();
        $hoy = Carbon::now();
        $this->_fechaRegistro = $hoy;
        $this->_adjunto = $this->_archivo? 'Adjunto-' . $this->_codUnico . '-' . $this->_registro . '-'. $this->_fechaRegistro->format('Y-m-d_H-i-s') . '.' . $this->_extension : null;
        $this->_ruta = self::RUTA;

        // Revisar si se archiva la politica
        $flgEliminarPolitica = false;

        if( ($this->_estado == self::ESTADO_REVISADO) && in_array($this->_PoliticaAsociada,Politica::politicasExplicativas())){
            $flgEliminarPolitica = $this->_Idpoliticacliente;
        }
        // LOGICA DE DECISION DE COMITE
        $objSalida = null;
        $clienteSalida = false;
        $eliminarpoliticasvigentes = false;
        $reingreso = false;
        
        if (isset($this->_DecisionComite)){
            \Debugbar::info("GESTION TIPO COMITE");
            $model = new modelCliente();
            $cliente = $model->getListaClientesAlerta(self::sgetPeriodoInfinity(),$this->_codUnico)->first();

            //Si es Infinity
            if ($cliente->FLG_INFINITY >= 1){

                //Si es aprobado y una politica cualitativa comite

                \Debugbar::info("DECISION: " . $this->_DecisionComite);
                \Debugbar::info("POLI CUALI: ".$this->_PoliticaAsociada.'-'. in_array($this->_PoliticaAsociada,Politica::politcasComiteCualitativas()));

                if($this->_DecisionComite == self::DECISION_APROBADO && in_array($this->_PoliticaAsociada,Politica::politcasComiteCualitativas())){
                    \Debugbar::info("INFINITY APROBADO LEVANTAMIENTO POLITICA CUALITATIVA");
                    //se archiva politica
                    $flgEliminarPolitica = $this->_Idpoliticacliente;
                }

                //Caso contrario sale de infinity
                else{
                    if ($this->_DecisionComite == self::DECISION_RECHAZADO) {
                        \Debugbar::info("INFINITY CAIDA CLIENTE");
                        $objSalida = new Salida();
                        $objSalida->setValues([
                            '_politicaClienteId' => $this->_Idpoliticacliente,
                            '_codunico' => $this->_codUnico,
                            '_fechaSalida' => $hoy,
                            '_tipoSalida' => Salida::TIPO_COMITE,
                            '_registro' => $this->_registro,
                            '_lineas' => $objSalida->getLineas($this->_codUnico)
                        ]);
                        $clienteSalida = true;
                        $eliminarpoliticasvigentes = true;
                    }

                    $flgEliminarPolitica = $this->_Idpoliticacliente;
                }
                
            }
            //SI NO ES INFINITY
            else{
                //Buscamos su salida
                $objSalida = new Salida();
                $objSalida->buscarUltimaByCodunico($this->_codUnico);
                // dd($objSalida->buscarUltimaByCodunico($this->_codUnico));

                // SI es aprobado, si salio por una politica cualitativa por falta de gestión y esta dentro de los 45 dias desde la salida
                \Debugbar::info("DECISION: ".$this->_DecisionComite);
                \Debugbar::info("SALIDA: ".$objSalida->getValue('_tipoSalida'));
                \Debugbar::info("POLITICA: ".$objSalida->getValue('_politica'));
                \Debugbar::info("DIAS DIFF: ". $objSalida->getValue('_fechaSalida')->diffInDays($hoy,false));

                if($this->_DecisionComite == self::DECISION_APROBADO &&
                    $objSalida->getValue('_tipoSalida') !== Salida::TIPO_COMITE &&
                    in_array($objSalida->getValue('_politica'),Politica::politcasComiteCualitativas()) &&
                    $objSalida->getValue('_fechaSalida')->diffInDays($hoy,false) <= 45
                ){
                    \Debugbar::info("NO INFINITY - LEVANTAMIENTO POLITICA");
                    //se archiva politica
                    $flgEliminarPolitica = $this->_Idpoliticacliente;
                    $objSalida->setValue('_fechaReingreso',$hoy);

                    $reingreso = true;
 
                }else{
                    if ($this->_DecisionComite == self::DECISION_RECHAZADO) {
                        \Debugbar::info("NO INFINITY - CAIDA CLIENTE");
                            $objSalidabyId= new Salida();
                        if ($objSalidabyId->getSalidaByClientePolitica($this->_Idpoliticacliente)) {
                            $clienteSalida = false;
                        }else{
                            $objSalida = new Salida();
                            $objSalida->setValues([
                                '_politicaClienteId' => $this->_Idpoliticacliente,
                                '_codunico' => $this->_codUnico,
                                '_fechaSalida' => $hoy,
                                '_tipoSalida' => Salida::TIPO_COMITE,
                                '_registro' => $this->_registro,
                                '_lineas' => $objSalida->getLineas($this->_codUnico)
                            ]);
                            $clienteSalida = true;
                        }
                            $eliminarpoliticasvigentes = true;
                    }
                }
            }
        }
// dd($this->setValueToTable(), $this->_archivo, $this->_ruta,$objSalida? $objSalida->setValueToTable(): null,$clienteSalida,$flgEliminarPolitica,self::sgetPeriodoInfinity(),$this->_DecisionComite == self::DECISION_APROBADO && $this->_PoliticaAsociada == Politica::POLITICA_1ROJO_2 ? $clirevision->setValueToTable() : null);
        if ($model->guardarGestion($this->setValueToTable(), $this->_archivo, $this->_ruta,
            $objSalida? $objSalida->setValueToTable(): null,
            $clienteSalida,
            $flgEliminarPolitica,self::sgetPeriodoInfinity(),$this->_DecisionComite == self::DECISION_APROBADO && $this->_PoliticaAsociada == Politica::POLITICA_1ROJO_2 ? $clirevision->setValueToTable() : null)){
            //Si se archivo una politica, volvemos a priorizar
            if ($flgEliminarPolitica !== false){
                \Debugbar::info("REPRIORIZANDO");
                $pol_proceso = new PoliticaProceso();
                $politicas = $pol_proceso->getPoliticasCliente($this->_codUnico,false);
                \Debugbar::info($politicas);
                \Debugbar::info(PoliticaProceso::priorizacion($politicas));
                PoliticaProceso::actualizar(PoliticaProceso::priorizacion($politicas),[],[]
                    ,$objSalida && $reingreso ? $objSalida->setValueToTable(): null);
            }
            if ($eliminarpoliticasvigentes) {
                $entidad = new modelCliente();
                $entidad->eliminarPoliticas($this->_codUnico);
            }
            return true;
        }
    }
    
    function updateGestion(){
        return in_array($this->_estado,[self::ESTADO_GESTIONADO,self::ESTADO_REVISADO,self::ESTADO_COMITE])? $this->_estado : null;
    }
}