<?php

namespace App\Entity\Infinity;

use App\Entity\Usuario as Usuario;
use Jenssegers\Date\Date as Carbon;
use App\Model\Infinity\Cliente as modelCliente;

class ConocemeDocumentacion extends Cliente {

    protected $_codUnico;
    protected $_nombre;
    protected $_tipo;
    protected $_fechaRegistro;
    protected $_fechaDocumento;
    protected $_comentarios;
    protected $_archivo;
    protected $_nombreAdjunto;
    protected $_extension;
    protected $_registro;
    protected $_mdl;
    protected $_ruta;
    
    const TIPO_DDJJ = 1;
    const TIPO_EEFF = 2;
    const TIPO_IBR = 3;
    const TIPO_F02 = 4;
    const TIPO_MDL = 5;
    
    const RUTA_DOCS = 'infinity/documentacion/';
    const RUTA_MDL = '/infinity/recalculo/';

    function setValueToTable() {
        $data = [
            'COD_UNICO' => $this->_codUnico,
            'NOMBRE_DOCUMENTO' => $this->_nombre,
            'TIPO_DOCUMENTO' => $this->_tipo,
            'FECHA_REGISTRO' => $this->_fechaRegistro->format('Y-m-d H:i:s'),
            'FECHA_DOCUMENTO' => $this->_fechaDocumento,
            'COMENTARIOS' => $this->_comentarios,
            'ADJUNTO' => $this->_nombreAdjunto,
            'REGISTRO' => $this->_registro,
            'MDL' => $this->_mdl,
            
        ];
        return $this->cleanArray($data);
    }
    
    static function getTipos(){
        return [self::TIPO_DDJJ=>'DDJJ',self::TIPO_EEFF=> 'EEFF',self::TIPO_IBR=>'IBR',self::TIPO_F02=>'F02',self::TIPO_MDL=>'MDL'];
    }
    
    static function getNombre($id){
        $data = self::getTipos();
        return isset($data[$id])? $data[$id] : null;
    }
    
    public function getColumnaActualizable(){
        switch ($this->_tipo) {
            case self::TIPO_MDL:
                return 'FECHA_ULTIMO_RECALCULO';
            case self::TIPO_IBR:
                return 'FECHA_ULTIMO_IBR';
            case self::TIPO_F02:
                return 'FECHA_ULTIMO_F02';
            default:
                return null;
        }
    }

    public function registrar(){
        $model = new modelCliente();
        $hoy = Carbon::now();
        $this->_fechaRegistro = $hoy;
        $this->_nombreAdjunto = self::getNombre($this->_tipo).'-'.$this->_codUnico.'-'.$this->_registro.'-'
                .$this->_fechaRegistro->format('Y-m-d_H-i-s').'.'.$this->_extension;
        $this->_ruta = $this->_tipo == self::TIPO_MDL ? self::RUTA_MDL : self::RUTA_DOCS;
        $this->_nombre = self::getNombre($this->_tipo);
        return $model->guardarAdjunto(parent::sgetPeriodoInfinity(),$this->setValueToTable(),$this->_archivo,$this->_ruta,$this->getColumnaActualizable());

    }
    
}
