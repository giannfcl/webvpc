<?php

namespace App\Entity\Infinity;
use App\Entity\Infinity\PoliticaProceso as PoliticaProceso;
use Jenssegers\Date\Date as Carbon;


class AlertaDocumentacion extends \App\Entity\Base\Entity {
    
    const ALERTA_VERDE = 1;
    const ALERTA_AMBAR = 2;
    const ALERTA_ROJA = 3;
    const ALERTA_VERDE_1 = 11;
    const ALERTA_AMBAR_1 = 22;
    const ALERTA_ROJA_1 = 33;

    const COLORES_ALERTA = [self::ALERTA_VERDE => '#26B99A',  self::ALERTA_AMBAR=>'#F0AD4E',self::ALERTA_ROJA=>'#D9534F'];

    const ID_DDJJ=1;
    const ID_EEFF=2;
    const ID_IBR=3;
    const ID_F02=4;
    const ID_RECALCULO=5;
    const ID_VISITA=6;

    //Mes máximo para mostrar cierto elemento
    const REGLAS_ALERTA=[
                        self::ID_DDJJ=>['CHECK'=>4,'ALERTA'=>5,'CRUZ'=>6],
                        self::ID_EEFF=>['CHECK'=>4,'ALERTA'=>5,'CRUZ'=>6],
                        self::ID_IBR=>['CHECK'=>11,'ALERTA'=>14,'CRUZ'=>15],
                        self::ID_F02=>['CHECK'=>11,'ALERTA'=>14,'CRUZ'=>15],
                        self::ID_RECALCULO=>['CHECK'=>5,'ALERTA'=>14,'CRUZ'=>15],
                        self::ID_VISITA=>['CHECK'=>4,'ALERTA'=>7,'CRUZ'=>7],
                        ];
   
    static function getHTML(){
        return [
            self::ALERTA_VERDE => '<i id="alerta-'.self::ALERTA_VERDE.'" class="fas fa-check fa-lg" style="color:'.self::COLORES_ALERTA[self::ALERTA_VERDE].'"></i>',
            self::ALERTA_AMBAR => '<i id="alerta-'.self::ALERTA_AMBAR.'" class="fas fa-exclamation fa-lg" style="color:'.self::COLORES_ALERTA[self::ALERTA_AMBAR].'"></i>',
            self::ALERTA_ROJA =>  '<i id="alerta-'.self::ALERTA_ROJA.'" class="fas fa-times fa-lg" style="color:'.self::COLORES_ALERTA[self::ALERTA_ROJA].'"></i>',
            self::ALERTA_VERDE_1 =>  '<i id="alerta-'.self::ALERTA_VERDE.'" class="fas fa-times fa-lg" style="color:'.self::COLORES_ALERTA[self::ALERTA_VERDE].'"></i>',
            self::ALERTA_AMBAR_1 =>  '<i id="alerta-'.self::ALERTA_AMBAR.'" class="fas fa-times fa-lg" style="color:'.self::COLORES_ALERTA[self::ALERTA_AMBAR].'"></i>',
            self::ALERTA_ROJA_1 =>  '<i id="alerta-'.self::ALERTA_ROJA.'" class="fas fa-times fa-lg" style="color:'.self::COLORES_ALERTA[self::ALERTA_ROJA].'"></i>',
        ];
    }
    
    static function getIcono($tipoDocumento,$fechaUltimo=null,$infinity = 1,$fechaingresolA=null){
        $html = self::getHTML();
        $alerta = self::getNivelAlerta($tipoDocumento,$fechaUltimo,$infinity,$fechaingresolA);
        if ($alerta){
            return $html[$alerta];
        }else{
            return '';
        }
    }
    
    static function estadoDoc($fechadocumento = null, $fechaingresolinea, $tipodoc)
    {
        $hoy = Carbon::now();
        $añopasado = Carbon::now()->addyear(-1);

        $pasadonov = Carbon::create(Carbon::now()->addyear(-1)->year,11,01);

        $findic = Carbon::create(Carbon::now()->year, 12, 31);
        $nov = Carbon::create(Carbon::now()->year, 11, 01);

        if ($tipodoc == 'DDJJ') {
            //NOS FIJAMOS SI LA FECHA DE DOCUMENTO DEL DJ ESTA COMPLETO O INCOMPLETO
            //SI NO TENGO DATO DE FECHA DE DOCUMENTO ESTA INCOMPLETO
            if (empty($fechadocumento)) {
                return PoliticaProceso::INCOMPLETO;
            } else {
                $fdoc = Carbon::parse($fechadocumento);
                //SI EL AÑO DE LA FECHA DEL DOCUMENTO ES IGUAL AL DEL AÑO PASADO ESTA COMPLETO, SINO INCOMPLETO
                if ($fdoc->year==$añopasado->year) {
                    return PoliticaProceso::COMPLETO;
                }else{
                    return PoliticaProceso::INCOMPLETO;
                }
            }
        }

        if ($tipodoc == 'EEFF') {
            $fingreso=Carbon::parse($fechaingresolinea);
            //NOS FIJAMOS SI LA FECHA DE INGRESO DEL CLIENTE A L.AUTOM. ES POSTERIOR A NOVIEMBRE DE ESTE AÑO
            $clientenuevo = ($fingreso>$pasadonov) ? 1 : 0;

            if ($clientenuevo) {
                //SI ES CLIENTE NUEVO, VEMOS SI ESTAMOS ENTRE NOVIEMBRE O DICIEMBRE
                if ($hoy>=$nov && $hoy<=$findic)
                    return PoliticaProceso::COMPLETO;
            }else {
                if (empty($fechadocumento)) {
                    return PoliticaProceso::INCOMPLETO;
                } else {
                    $fdoc = Carbon::parse($fechadocumento);

                    //SI L.A. AÑO DE LA FECHA DEL DOCUMENTO ES DIFERENTE AL DE ESTE AÑO ESTA INCOMPLETO
                    if ($fdoc->year==$hoy->year) {
                        return PoliticaProceso::COMPLETO;
                    }else{
                        return PoliticaProceso::INCOMPLETO;

                    }
                }
            }
        }
    }

    static function getNivelAlerta($tipoDocumento,$fechaUltimo=null,$infinity = 1,$fechaingresolA=null){
        //Cuando no haya fecha
        // if (!$infinity) {
        //     return self::ALERTA_ROJA;
        // }
        if ($tipoDocumento==self::ID_DDJJ) {
            $estado = self::estadoDoc($fechaUltimo,$fechaingresolA,'DDJJ');
        }elseif ($tipoDocumento==self::ID_EEFF) {
            $estado = self::estadoDoc($fechaUltimo,$fechaingresolA,'EEFF');
        }else{
            if($fechaUltimo==null){
                return self::ALERTA_ROJA;
            }
        }

        $fechaUltimo=Carbon::parse($fechaUltimo);
        

        //PRIMERO CALCULAMOS LA FECHA DENTRO DEL TIEMPO NECESARIO PARA CONSIDERAR A PARTIR DE LA FECHA DEL DOCUMENTO
        //Validez hasta 1 año depues
        if ($tipoDocumento == self::ID_DDJJ){
            $fechaUltimo=Carbon::create($fechaUltimo->addYear(2)->year,01,01);
        }

        //Validez hasta junio de 1 año depues
        if ($tipoDocumento == self::ID_EEFF){
            if ($fechaUltimo<=Carbon::create(2019,01,01) || $fechaUltimo>=Carbon::create(2019,07,01)) {
                $fechaUltimo=Carbon::create(2020,06,01);
            }
            if ($fechaUltimo->month==6) {
                $fechaUltimo=Carbon::create($fechaUltimo->addYear()->year,07,01);
            }
        }

        //Validez hasta junio de 1 año depues
        // if ($tipoDocumento == self::ID_VISITA){
        //     if ($fechaUltimo->month <= 6){
        //         $fechaUltimo=Carbon::create($fechaUltimo->year,07,01);
        //     }else{
        //         $fechaUltimo=Carbon::create($fechaUltimo->year,12,31);
        //     }
            
        // }

        $fechaHoy=Carbon::today();
        $diferenciaMeses=$fechaUltimo->diffInMonths($fechaHoy,false);
        //print_r($diferenciaMeses);print_r(self::REGLAS_ALERTA[$tipoDocumento]['CHECK']);print_r(self::REGLAS_ALERTA[$tipoDocumento]['ALERTA']);print_r(self::REGLAS_ALERTA[$tipoDocumento]['CRUZ']);

        //DEPENDIENDO DEL TIPO DE DOCUMENTO MOSTRAMOS EL VISTO, LA ALERTA O LA EQUIS
        if ($diferenciaMeses<=self::REGLAS_ALERTA[$tipoDocumento]['CHECK']){
            return self::ALERTA_VERDE;
        }
        else if ($diferenciaMeses>self::REGLAS_ALERTA[$tipoDocumento]['CHECK'] && $diferenciaMeses<self::REGLAS_ALERTA[$tipoDocumento]['ALERTA']){
            return self::ALERTA_AMBAR;
        }
        else if ($diferenciaMeses>=self::REGLAS_ALERTA[$tipoDocumento]['ALERTA']){
            return self::ALERTA_ROJA;
        }
    }
    
    static function getNivelAlertaDocumentacionTotal($infinity,$fechaDDJJ,$fechaEEFF,$fechaIBR,$fechaf02,$fechaingresolA=null){

        //Si las 4 fechas estan seteadas
        if ($fechaDDJJ||$fechaEEFF||$fechaIBR||$fechaf02){
            $nivel[] = self::getNivelAlerta(self::ID_DDJJ,$fechaDDJJ,$infinity,$fechaingresolA);
            $nivel[] = self::getNivelAlerta(self::ID_EEFF,$fechaEEFF,$infinity,$fechaingresolA);
            $nivel[] = self::getNivelAlerta(self::ID_IBR,$fechaIBR,$infinity,$fechaingresolA);
            $nivel[] = self::getNivelAlerta(self::ID_F02,$fechaf02,$infinity,$fechaingresolA);
            
            //El icono del que tenga peor status
            return max($nivel);
            
        }else{
            return self::ALERTA_ROJA;
        }
    }
    
}