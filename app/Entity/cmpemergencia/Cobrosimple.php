<?php

namespace App\Entity\cmpemergencia;

use App\Model\cmpemergencia\Lead as mLead;

class Cobrosimple extends \App\Entity\Base\Entity {

    static function getComboCantClientes(){
        return [
            "1"=>"Más de 8,000",
            "2"=>"Menos de 8,000",
        ];
    }

    static function getComboControlCobranza(){
        return [
            "1"=>"Manual o Excel",
            "2"=>"Sistema o ERP",
        ];
    }

    static function getComboTicketPromedio(){
        return [
            "1"=>"Menos de S/ 100,000",
            "2"=>"Más de S/ 100,000",
        ];
    }
}
