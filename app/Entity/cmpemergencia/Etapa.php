<?php

namespace App\Entity\cmpemergencia;

use App\Model\cmpemergencia\Lead as mLead;

class Etapa extends \App\Entity\Base\Entity {

    const ASIGNADO = 1;
    const CONTACTADO = 2;
    const INTERESADO = 3;
    const DOCUMENTOS_EVALUACION = 4;
    const DOCUMENTOS_DESEMBOLSO = 5;
    const DERIVADO_GTP = 6;
    const DESEMBOLSADO = 7;
    const RECHAZADO = 8;
    const REVISAR = 9;
    const ENVIO_SUBASTA = 10;
    const VALIDACION_DOCUMENTOS = 11;
    const WIO_INGRESADO = 12;
    const WIO_COFIDE = 13;
    const ENVIADO_COFIDE = 14;


    static function getAll($etapa){
        $model = new mLead();
        return $model->listar($etapa,null,null);
    }

    static function getByEtapa($ejecutivo){
        $model = new mLead();
        return $model->getByEtapa($ejecutivo->getValue('_registro'));
    }

    static function getByEtapaValidacionDocumento(){
        $model = new mLead();
        return $model->getByEtapa(null,null,self::VALIDACION_DOCUMENTOS);
    }

}
