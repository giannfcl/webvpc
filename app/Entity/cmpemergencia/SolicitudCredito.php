<?php

namespace App\Entity\cmpemergencia;

class SolicitudCredito extends \App\Entity\Base\Entity {

    protected $_ruc;
    protected $_monto;
    protected $_cobertura;
    protected $_subasta;
    protected $_fecha;
    protected $_ejecutivo;
    protected $_tasa;
    protected $_banca;
    

    function setValueToTable() {
        return $this->cleanArray([
            'NUM_RUC' => $this->_ruc,
            'MONTO_SOLICITADO' => $this->_monto,
            'COBERTURA' => $this->_cobertura,
            'SUBASTA' => $this->_subasta,
            'FECHA' => ($this->_fecha)? $this->_fecha->toDateTimeString() : null,
            'EJECUTIVO' => $this->_ejecutivo,
            'TASA' => $this->_tasa,
            'BANCA' => $this->_banca,
        ]);
    }

}
