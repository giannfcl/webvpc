<?php

namespace App\Entity\cmpemergencia;

use App\Model\cmpemergencia\Lead as mLead;

class GestionFile extends \App\Entity\Base\Entity {

    const PENDIENTE = "0";
    const EN_VALIDACION = "1";
    const DEVUELTO = "2";
    const ENVIADO_A_GTP = "3";

    const GF1_BE = ["ID"=>"0","NOMBRE"=>"PENDIENTE"];
    const GF2_BE = ["ID"=>"1","NOMBRE"=>"EN VALIDACION"];
    const GF3_BE = ["ID"=>"2","NOMBRE"=>"DEVUELTO"];
    const GF4_BE = ["ID"=>"3","NOMBRE"=>"ENVIADO A GTP"];

    static function getGestionFileBE(){
        return [
            self::GF1_BE,
            self::GF2_BE,
            self::GF3_BE,
            self::GF4_BE,
        ];
    }
}
