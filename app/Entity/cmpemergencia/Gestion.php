<?php

namespace App\Entity\cmpemergencia;

use App\Entity\BE\LeadEtapa as LeadEtapa;
use App\Model\cmpemergencia\Gestion as mGestion;
use App\Entity\cmpemergencia\Lead as Lead;
use App\Model\cmpemergencia\Lead as mLead;
use App\Entity\cmpemergencia\Etapa as Etapa;
use Jenssegers\Date\Date as Carbon;

class Gestion extends \App\Entity\Base\Entity {

    const LO_PENSARA = 1;
    const SIN_CONTACTO = 2;
    const NO_ACEPTA = 3;
    const NUMERO_EQUIVOCADO = 4;
    const INTERESADO = 5;
    const COMPLETO = 6;
    const ENVIAR_RIESGOS = 7;
    const NO_CALIFICA = 8;
    const RIESGOS_CONFORME = 9;
    const RIESGOS_RECHAZO = 14;
    const RIESGOS_OBSERVACION = 10;
    const VALIDACION_APROBADO = 11;
    const VALIDACION_REVISION = 12;
    const VALIDACION_RECHAZADO = 13;
    const RIESGOS_PENDIENTE = 19;
    const ENVIO_VALORADOS = 20;
    const ENVIO_GTP = 21;
    const DESEMBOLSADO = 30;

    static function getAllBpe(){
        $model = new mGestion();
        return $model->listarBpe()->get();
    }

    static function getAllBe(){
        $model = new mGestion();
        return $model->listarBe()->get();
    }

    static function getAllRiegos(){
        $model = new mGestion();
        return $model->listarRiesgos()->get();
    }

    static function getAllValidacion(){
        $model = new mGestion();
        return $model->listarValidacion()->get();
    }    

    static function getSubRiesgosObservados(){
        $model = new mGestion();
        return $model->listarSubRiesgos(self::RIESGOS_OBSERVACION)->get();
    }

    function gestionar($data,$registro)
    {
        // dd($data,$registro);
        $hoy = Carbon::now();
        $gplazogracia = isset($data['gplazogracia'])?explode('-',$data['gplazogracia']):false;
        
        $lead = new Lead();
        $lead->setValues([
            '_ruc' => $data['gnumruc']?$data['gnumruc']:null,
            '_telefono1' => isset($data['gtelefono'])?$data['gtelefono']:null,
            '_correo' => isset($data['gcorreo'])?$data['gcorreo']:null,
            '_campanha' => isset($data['gcampana'])?$data['gcampana']:null,
            '_solicitudVolumenVenta' => isset($data['gventasa'])?str_replace(',','',$data['gventasa']):null,
            '_solicitudEssalud' => isset($data['gessalud'])?str_replace(',','',$data['gessalud']):null,
            '_solicitudOferta' => isset($data['gmontosolicitado'])?str_replace(',','',$data['gmontosolicitado']):null,
            '_tasafinalcliente' => isset($data['tasatentativa'])?$data['tasatentativa']:null,
            '_contactoComentario' => isset($data['gcomentario'])?$data['gcomentario']:null,
            '_solicitudGracia' => $gplazogracia?$gplazogracia[1]:null,
            '_solicitudPlazo' => $gplazogracia?$gplazogracia[0]:null,
            '_contactoResultado' => isset($data['ggestion'])?$data['ggestion']:null,
            '_contactoFecha' => $hoy,
            '_flgcomunicorechazo' => isset($data['flgcomunicorechazo'])?1:null,
            '_fechacomunicorechazo' => isset($data['flgcomunicorechazo'])?$hoy:null,
            '_flgReactiva1' => isset($data['gflgReactiva1'])?$data['gflgReactiva1']:null,
            '_montoReactiva1' => isset($data['gmontoReactiva1'])?str_replace(',','',$data['gmontoReactiva1']):null,
            '_solicitudTasa' => isset($data['gtasaTentativa'])?$data['gtasaTentativa']:null,
            '_bancoReactiva1' => isset($data['bancor1'])?$data['bancor1']:null,
        ]);
        
        /* Logica para guardar el lead-etapa*/
        $leadEtapa = new EtapaLead();
        $leadEtapa->setValue('_ruc',$data['gnumruc']);
        $leadEtapa->setValue('_responsable',$registro);
        $leadEtapa->setValue('_gestion',isset($data['ggestion'])?$data['ggestion']:null);
        $leadEtapa->setValue('_comentario',isset($data['gcomentario'])?$data['gcomentario']:null);
        $leadEtapa->setValue('_fecha',$hoy);
        $cambio = false;
        
        if($data['ggestion'] == self::INTERESADO){
            //CONDICION EXTRA
            if ($lead->getValue('_solicitudOferta') > 0 
                && $lead->getValue('_solicitudOferta') <= 10000000
                && $lead->getValue('_solicitudGracia')
                && $lead->getValue('_solicitudPlazo')
            ){
                if ($data['etapa_actual']==Etapa::REVISAR) {
                    $lead->setValue('_etapa',Etapa::REVISAR);
                    $leadEtapa->setValue('_etapa',Etapa::REVISAR);
                }else{
                    $lead->setValue('_etapa',Etapa::INTERESADO);
                    $leadEtapa->setValue('_etapa',Etapa::INTERESADO);
                    $cambio = true;
                }
            }
            
        }elseif($data['ggestion'] == self::NO_ACEPTA){
            if ($data['etapa_actual']==Etapa::REVISAR) {
                $lead->setValue('_etapa',Etapa::REVISAR);
                $lead->setValue('_rechazoFecha',$hoy);
                $lead->setValue('_rechazoMotivo',Lead::MOTIVO_RECHAZO_CLIENTE);
                $leadEtapa->setValue('_etapa',Etapa::REVISAR);
            }else{
                $lead->setValue('_etapa',Etapa::ASIGNADO);
                $lead->setValue('_rechazoFecha',$hoy);
                $lead->setValue('_rechazoMotivo',Lead::MOTIVO_RECHAZO_CLIENTE);
                $leadEtapa->setValue('_etapa',Etapa::ASIGNADO);
            }
        }else{
            $leadEtapa->setValue('_etapa',Etapa::ASIGNADO);
        }

        $model = new mLead();
        // dd($lead->setValueToTable(),$leadEtapa->setValueToTable(),$cambio);
        if ($model->gestionar($lead->setValueToTable(),$leadEtapa->setValueToTable())){
            if ($cambio) {
                $this->setMessage('El cliente (' . $data['gnumruc'] . ') ha pasado a la etapa Interesado');
            }else{
                $this->setMessage('Se actualizó la información del cliente (' . $data['gnumruc'] . ')');
            }
            return true;
        }else{
            $this->setMessage('Hubo en error al conectarse con la Base de Datos. Contáctate con soporte.');
            return false;
        }
    }

}
