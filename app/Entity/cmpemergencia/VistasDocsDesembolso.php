<?php

namespace App\Entity\cmpemergencia;

use App\Model\cmpemergencia\Lead as mLead;

class VistasDocsDesembolso extends \App\Entity\Base\Entity {

    const V1_BE = ["ID"=>"1","NOMBRE"=>"Desembolsó RP1 con ​IBK​"];
    const V2_BE = ["ID"=>"2","NOMBRE"=>"Desembolsó RP1 con ​OTRO BANCO / NO TOMÓ​"];

    static function getNombresVistas(){
        return [
            self::V1_BE,
            self::V2_BE,
        ];
    }
}
