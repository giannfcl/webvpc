<?php

namespace App\Entity\cmpemergencia;

use App\Entity\Usuario;
use App\Model\cmpemergencia\Priorizacion as mPriorizacion;
use Jenssegers\Date\Date as Carbon;

class Priorizacion extends \App\Entity\Base\Entity {

    protected $_ruc;
    protected $_fecha;
    protected $_registro;
    protected $_flujointerno;
    protected $_resultadoweb;
    protected $_condicion;

    function setValueToTable() {
        return $this->cleanArray([
            'NUM_RUC' => $this->_ruc,
            'FECHA_REGISTRO' => $this->_fecha->toDateTimeString(),
            'REGISTRO' => $this->_registro,
            'FLUJO_INTERNO' => $this->_flujointerno,
            'RESULTADO_WEB' => $this->_resultadoweb,
            'CONDICION' => $this->_condicion,
        ]);
    }

    function agregar($ruc,Usuario $usuario,$data){
        $hoy = Carbon::now();
        $this->_ruc = $ruc;
        $this->_registro = $usuario->getValue('_registro');
        $this->_fecha = $hoy;
        $this->_flujointerno = isset($data['flujointerno'])?$data['flujointerno']:null;
        $this->_resultadoweb = isset($data['resultadoweb'])?$data['resultadoweb']:null;
        $this->_condicion = $data['condicion']=='true'?1:0;

        // dd($this->setValueToTable());
        $model = new mPriorizacion();
        if ($model->agregar($this->setValueToTable())){
            return true;
        }else{
            $this->setMessage('Hubo un error en base de datos. Por favor contáctese con el administrador');
            return false;
        }
    }
}
