<?php

namespace App\Entity\cmpemergencia;

use App\Model\cmpemergencia\Lead as mLead;

class GestionSubasta extends \App\Entity\Base\Entity {

    const ACEPTA = "0";
    const LO_PENSARA = "1";
    const NO_ACEPTA = "2";

    const GS1_BE = ["ID"=>"0","NOMBRE"=>"ACEPTA"];
    const GS2_BE = ["ID"=>"1","NOMBRE"=>"LO PENSARA"];
    const GS3_BE = ["ID"=>"2","NOMBRE"=>"NO ACEPTA"];

    static function GestionSubastaBE(){
        return [
            self::GS1_BE,
            self::GS2_BE,
            self::GS3_BE,
        ];
    }
}
