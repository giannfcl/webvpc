<?php

namespace App\Entity\cmpemergencia;

use App\Model\cmpemergencia\Lead as mLead;

class PlazoGracia extends \App\Entity\Base\Entity {

    const PG1_BE = ["36-12","36 meses - 12 meses gracia"];//12 meses de gracia y 36 cuotas a pagar
    const PG2_BE = ["24-12","24 meses - 12 meses gracia"];//12 meses de gracia y 24 cuotas a pagar
    
    const PG1_BPE = ["33-3","Gracia 3 meses + 33 cuotas"];
    const PG2_BPE = ["30-6","Gracia 6 meses + 30 cuotas"];
    const PG3_BPE = ["24-12","Gracia 12 meses + 24 cuotas"];
    
    static function getPlazosGraciasBE(){
        return [
            self::PG1_BE,
            self::PG2_BE,
        ];
    }

    static function getPlazosGraciasBPE(){
        return [
            self::PG1_BPE,
            self::PG2_BPE,
            self::PG3_BPE,
        ];
    }
}
