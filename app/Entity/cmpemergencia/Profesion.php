<?php

namespace App\Entity\cmpemergencia;

use App\Model\cmpemergencia\Maestras as mMaestra;

class Profesion extends \App\Entity\Base\Entity {

    static function getAll(){
        $model = new mMaestra();
        return $model->getProfesiones()->get();
    }

}
