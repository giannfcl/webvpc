<?php

namespace App\Entity\cmpemergencia;

use App\Entity\BE\LeadEtapa;
use App\Entity\Usuario;
use App\Model\cmpemergencia\Lead as mLead;
use App\Model\cmpemergencia\Subasta as mSubasta;
use App\Entity\cmpemergencia\GestionSubasta as GestionSubasta;
use App\Entity\cmpemergencia\GestionFile as GestionFile;
use App\Entity\Banca as Banca;
use Jenssegers\Date\Date as Carbon;

class Lead extends \App\Entity\Base\Entity {

    const NUEVO = 'Nuevo Referido';
    const MOTIVO_RECHAZO_CLIENTE = 'Cliente no interesado';
    const MOTIVO_RECHAZO_NO_CALIFICA = 'Ejecutivo considera que cliente no califica';
    const MOTIVO_RECHAZO_CLIENTE_TASA = 'Cliente al final no acepto la tasa/monto';
    const BANCA_BE = "BE";
    const CLIENTES_CUENTA_SUELDO = 1;
    const CLIENTES_COBROSIMPLE = 1;
    const fechaPrimerContacto = ['Sin Contacto','Menor a 2','Entre 2 y 5','Entre 5 y 8','Entre 8 y 11','Mayor 11 dias'];
    
    protected $_ruc;
    protected $_nombre;
    protected $_flujo;
    protected $_telefono1;
    protected $_telefono2;
    protected $_telefono3;
    protected $_correo;
    protected $_correoConfirmacion;
    protected $_direccion;

    protected $_urbZona;
    protected $_tipoVia;
    protected $_direcNro;
    protected $_direcLote;
    protected $_direcManzana;
    protected $_direcInterior;
    protected $_direcDescripVia;
    protected $_direcUrb;

    protected $_nroEmpleados;
    protected $_banca;

    protected $_rrllNombres;
    protected $_rrllDocumento;
    protected $_rrllApellidos;
    protected $_rrllEstadoCivil;

    protected $_rrllNombres2;
    protected $_rrllDocumento2;
    protected $_rrllApellidos2;
    protected $_rrllEstadoCivil2;

    protected $_tipoSociedad;
    protected $_ubigeo;

    protected $_solicitudVolumenVenta;
    protected $_solicitudEssalud;
    protected $_solicitudOferta;
    protected $_solicitudPlazo;
    protected $_solicitudGracia;
    protected $_solicitudTasa;
    protected $_campanha;
    protected $_canalActual;
    protected $_canalOrigen;
    protected $_etapa;
    protected $_etapa_obj;
    protected $_asignadoReferido;
    protected $_asignadoReferidoEjecutivo;
    protected $_contactoResultado;
    protected $_contactoFecha;
    protected $_contactoComentario;
    protected $_productoMonto;
    protected $_productoTasa;
    protected $_documentoessalud;
    protected $_documentodjpdt;
    protected $_documentocompleto;
    protected $_rechazoFecha;
    protected $_rechazoMotivo;
    protected $_riesgosMonto;
    protected $_dato;
    protected $_profesion;
    protected $_grandoInstrruccion;
    protected $_fechadesembolsotentativa;
    protected $_estadosubasta;
    protected $_tasamaximabcr;
    protected $_tasafinalcliente;
    protected $_gestionsubasta;
    protected $_gestionsubastacomentario;
    protected $_aprobacionSubastaFecha;
    protected $_riesgosGestion;
    protected $_riesgosSubGestion;
    protected $_cuentaNumero;
    protected $_cuentaTipo;
    protected $_riesgosComentario;
    protected $_estadofile;
    protected $_gestionValidacionDocumento;
    protected $_flgcomunicorechazo;
    protected $_fechacomunicorechazo;
    protected $_wiofechaobservacion;
    protected $_sector;
    protected $_giro;
    protected $_flgReactiva1;
    protected $_montoReactiva1;
    protected $_solicitudOfertaCobertura;
    protected $_flgRTributarioTerceros;
    protected $_bancoReactiva1;
    protected $_flgTopeMaximo;
    protected $_topeMaximo;
    protected $_flagReporteTributario;
    protected $_fechaAsignadoBackoffice;
    protected $_subastaTasa;
    protected $_tipoEnvioGTP;
    protected $_tienda;
    protected $_flgdestinocompradolares;
    protected $_montocompradolares;
    protected $_flgaceptocuentasueldo;
    protected $_montoessalud;
    protected $_nrotrabajadoresdependientes;
    protected $_sueldopromedio;
    protected $_flgaceptacobrosimple;
    protected $_cantclientespagan;
    protected $_controlcobranza;
    protected $_ticketpromcobranza;

    function setValueToTable() {
        return $this->cleanArray([
            'NUM_RUC' => $this->_ruc,
            'NOMBRE' => $this->_nombre,
            'FLUJO' => $this->_flujo,
            'TELEFONO1' => $this->_telefono1,
            'TELEFONO2' => $this->_telefono2,
            'TELEFONO3' => $this->_telefono3,
            'CORREO' => $this->_correo,
            'CORREO_CONFIRMACION' => $this->_correoConfirmacion,
            'DIRECCION' => $this->_direccion,
            'NRO_EMPLEADOS' => $this->_nroEmpleados,
            'BANCA' => $this->_banca,

            'RRLL_NOMBRES' => $this->_rrllNombres,
            'RRLL_DOCUMENTO' => $this->_rrllDocumento,
            'RRLL_APELLIDOS' => $this->_rrllApellidos,
            'RRLL_ESTADO_CIVIL' => $this->_rrllEstadoCivil,

            'RRLL_NOMBRES_2' => $this->_rrllNombres2,
            'RRLL_DOCUMENTO_2' => $this->_rrllDocumento2,
            'RRLL_APELLIDOS_2' => $this->_rrllApellidos2,
            'RRLL_ESTADO_CIVIL_2' => $this->_rrllEstadoCivil2,

            'DIR_TIPO_VIA' => $this->_tipoVia,
            'DIR_NRO' => $this->_direcNro,
            'DIR_INTERIOR' => $this->_direcInterior,
            'DIR_DESCRIPCION_VIA' => $this->_direcDescripVia,
            'DIR_LOTE' => $this->_direcLote,
            'DIR_MANZANA' => $this->_direcManzana,
            'DIR_URB_ZONA' => $this->_direcUrb,

            'TIPO_SOCIEDAD' => $this->_tipoSociedad,
            'DIR_UBIGEO' => $this->_ubigeo,

            'SOLICITUD_VOLUMEN_VENTA' => $this->_solicitudVolumenVenta,
            'SOLICITUD_ESSALUD' => $this->_solicitudEssalud,
            'SOLICITUD_OFERTA' => $this->_solicitudOferta,
            'SOLICITUD_PLAZO' => $this->_solicitudPlazo,
            'SOLICITUD_GRACIA' => $this->_solicitudGracia,
            'SOLICITUD_TASA' => $this->_solicitudTasa,
            'CAMPANIA' => $this->_campanha,
            'CANAL_ACTUAL' => $this->_canalActual,
            'CANAL_ORIGEN' => $this->_canalOrigen,
            'ETAPA_ID' => $this->_etapa,
            'ASIGNADO_EJECUTIVO' => $this->_asignadoReferido,
            'ASIGNADO_REFERIDO_EJECUTIVO' => $this->_asignadoReferidoEjecutivo,
            'CONTACTO_RESULTADO' => $this->_contactoResultado,
            'CONTACTO_FECHA' => ($this->_contactoFecha instanceof Carbon)? $this->_contactoFecha->toDateTimeString() : null,
            'CONTACTO_COMENTARIO' => $this->_contactoComentario,
            'PRODUCTO_MONTO' => $this->_productoMonto,
            'PRODUCTO_TASA' => $this->_productoTasa,
            'DOCUMENTACION_PDTDJ' =>$this->_documentodjpdt,
            'DOCUMENTACION_ESSALUD'=>$this->_documentoessalud,
            'DOCUMENTACION_COMPLETA'=>$this->_documentocompleto,
            'RECHAZO_FECHA'=>($this->_rechazoFecha instanceof Carbon)? $this->_rechazoFecha->toDateTimeString() : null,
            'RECHAZO_MOTIVO'=>$this->_rechazoMotivo,
            'RIESGOS_MONTO'=>$this->_riesgosMonto,
            'PROFESION'=>$this->_profesion,
            'GRADO_INSTRUCCION'=>$this->_grandoInstrruccion,
			      'FECHA_DESEMBOLSO_TENTATIVA'=>($this->_fechadesembolsotentativa instanceof Carbon)? $this->_fechadesembolsotentativa->toDateTimeString() : null,
            'ESTADO_SUBASTA'=>$this->_estadosubasta,
            'TASA_MAXIMA_BCR'=>$this->_tasamaximabcr,
            'TASA_FINAL_CLIENTE'=>$this->_tasafinalcliente,
            'GESTION_SUBASTA'=>$this->_gestionsubasta,
            'GESTION_SUBASTA_COMENTARIO'=>$this->_gestionsubastacomentario,
            'FECHA_APROBACIO_SUBASTA'=>($this->_aprobacionSubastaFecha instanceof Carbon)? $this->_aprobacionSubastaFecha->toDateTimeString() : null,
            'RIESGOS_GESTION'=>$this->_riesgosGestion,
            'RIESGOS_GESTION_SUB'=>$this->_riesgosSubGestion,
            'RIESGOS_COMENTARIO'=>$this->_riesgosComentario,
            'CUENTA_TIPO'=>$this->_cuentaTipo,
            'CUENTA_NUMERO'=>$this->_cuentaNumero,
            'ESTADO_FILE'=>$this->_estadofile,
            'VALIDACION_DOCUMENTACION_GESTION'=>$this->_gestionValidacionDocumento,
            'FLG_COMUNICO_RECHAZO'=>$this->_flgcomunicorechazo,
            'FECHA_COMUNICO_RECHAZO'=>$this->_fechacomunicorechazo,
            'WIO_OBSERVACION_FECHA'=>$this->_wiofechaobservacion,
            'SECTOR'=>$this->_sector,
            'GIRO'=>$this->_giro,
            'FLG_TOMO_REACTIVA_1'=>$this->_flgReactiva1,
            'MONTO_PRESTAMO_REACTIVA_1'=>$this->_montoReactiva1,
            'SOLICITUD_OFERTA_COBERTURA'=>$this->_solicitudOfertaCobertura,
            'FLG_REPORTE_TRIBUTARIO_TERCEROS'=>$this->_flgRTributarioTerceros,
            'BANCO_REACTIVA_1'=>$this->_bancoReactiva1,
            'FLAG_REPORTE_TRIBUTARIO' =>$this->_flagReporteTributario,
            'FECHA_ASIGNADO_BACKOFFICE' => $this->_fechaAsignadoBackoffice,
            'SUBASTA_TASA' => $this->_subastaTasa,
            'TIPO_ENVIO_GTP' => $this->_tipoEnvioGTP,
            'TIENDA' => $this->_tienda,
            'FLG_DESTINO_COMPRA_DOLARES' => $this->_flgdestinocompradolares,
            'MONTO_COMPRA_DOLARES' => $this->_montocompradolares,
            'FLG_ACEPTO_CUENTA_SUELDO' => $this->_flgaceptocuentasueldo,
            'MONTO_PAGO_ESSALUD' => $this->_montoessalud,
            'NRO_TRABAJADORES_QUINTA_CATEGORIA' => $this->_nrotrabajadoresdependientes,
            'SUELDO_PROMEDIO' => $this->_sueldopromedio,
            'FLG_COBRO_SIMPLE' =>$this->_flgaceptacobrosimple,
            'CS_CANT_CLIENTES_PAGAN' =>$this->_cantclientespagan,
            'CS_CONTROL_COBRANZA' =>$this->_controlcobranza,
            'CS_TICKET_PROM_COBRANZA' =>$this->_ticketpromcobranza,
        ]);
    }

    function setProperties($data){
        $this->setValues([
            '_ruc' => $data->NUM_RUC,
            '_nombre' => $data->NOMBRE,
            '_flujo' => isset($data->FLUJO)?$data->FLUJO:null,
            '_telefono1' => isset($data->TELEFONO1)?$data->TELEFONO1:null,
            '_telefono2' => isset($data->TELEFONO2)?$data->TELEFONO2:null,
            '_telefono3' => isset($data->TELEFONO3)?$data->TELEFONO3:null,
            '_correo' => isset($data->CORREO)?$data->CORREO:null,
            '_correoConfirmacion' => isset($data->CORREO_CONFIRMACION)?$data->CORREO_CONFIRMACION:null,
            '_direccion' => isset($data->DIRECCION)?$data->DIRECCION:null,
            '_nroEmpleados' => isset($data->NRO_EMPLEADOS)?$data->NRO_EMPLEADOS:null,
            '_banca' => isset($data->BANCA)?$data->BANCA:null,

            '_rrllNombres' => isset($data->RRLL_NOMBRES)?$data->RRLL_NOMBRES:null,
            '_rrllDocumento' => isset($data->RRLL_DOCUMENTO)?$data->RRLL_DOCUMENTO:null,
            '_rrllApellidos' => isset($data->RRLL_APELLIDOS)?$data->RRLL_APELLIDOS:null,
            '_rrllEstadoCivil' => isset($data->RRLL_ESTADO_CIVIL)?$data->RRLL_ESTADO_CIVIL:null,

            '_rrllNombres2' => isset($data->RRLL_NOMBRES2)?$data->RRLL_NOMBRES2:null,
            '_rrllDocumento2' => isset($data->RRLL_DOCUMENTO2)?$data->RRLL_DOCUMENTO2:null,
            '_rrllApellidos2' => isset($data->RRLL_APELLIDOS2)?$data->RRLL_APELLIDOS2:null,
            '_rrllEstadoCivil2' => isset($data->RRLL_ESTADO_CIVIL2)?$data->RRLL_ESTADO_CIVIL2:null,

            '_tipoVia' => isset($data->DIR_TIPO_VIA)?$data->DIR_TIPO_VIA:null,
            '_direcNro' => isset($data->DIR_NRO)?$data->DIR_NRO:null,
            '_direcInterior' => isset($data->DIR_INTERIOR)?$data->DIR_INTERIOR:null,
            '_direcDescripVia' => isset($data->DIR_DESCRIPCION_VIA)?$data->DIR_DESCRIPCION_VIA:null,
            '_direcLote' => isset($data->DIR_LOTE)?$data->DIR_LOTE:null,
            '_direcManzana' => isset($data->DIR_MANZANA)?$data->DIR_MANZANA:null,
            '_direcUrb' => isset($data->DIR_URB_ZONA)?$data->DIR_URB_ZONA:null,

            '_solicitudVolumenVenta' => isset($data->SOLICITUD_VOLUMEN_VENTA)?$data->SOLICITUD_VOLUMEN_VENTA:null,
            '_solicitudEssalud' => isset($data->SOLICITUD_ESSALUD)?$data->SOLICITUD_ESSALUD:null,
            '_solicitudOferta' => isset($data->SOLICITUD_OFERTA)?$data->SOLICITUD_OFERTA:null,
            '_solicitudPlazo' => isset($data->SOLICITUD_PLAZO)?$data->SOLICITUD_PLAZO:null,
            '_solicitudGracia' => isset($data->SOLICITUD_GRACIA)?$data->SOLICITUD_GRACIA:null,
            '_campanha' => isset($data->CAMPANIA)?$data->CAMPANIA:null,
            '_canalActual' => isset($data->CANAL_ACTUAL)?$data->CANAL_ACTUAL:null,
            '_canalOrigen' => isset($data->CANAL_ORIGEN)?$data->CANAL_ORIGEN:null,
            '_etapa' => isset($data->ETAPA_ID)?$data->ETAPA_ID:null,
            '_asignadoReferido' => isset($data->ASIGNADO_EJECUTIVO)?$data->ASIGNADO_EJECUTIVO:null,
            '_asignadoReferidoEjecutivo' => isset($data->ASIGNADO_REFERIDO_EJECUTIVO)?$data->ASIGNADO_REFERIDO_EJECUTIVO:null,
            '_contactoResultado' => isset($data->CONTACTO_RESULTADO)?$data->CONTACTO_RESULTADO:null,
            '_contactoFecha' => isset($data->CONTACTO_FECHA) && $data->CONTACTO_FECHA? new Carbon($data->CONTACTO_FECHA):null,
            '_rechazoFecha' => isset($data->RECHAZO_FECHA) && $data->RECHAZO_FECHA? new Carbon($data->CONTACTO_FECHA):null,
            '_contactoComentario' => isset($data->CONTACTO_COMENTARIO)?$data->CONTACTO_COMENTARIO:null,
            '_productoMonto' => isset($data->PRODUCTO_MONTO)?$data->PRODUCTO_MONTO:null,
            '_productoTasa' => isset($data->PRODUCTO_TASA)?$data->PRODUCTO_TASA:null,
            '_documentodjpdt' =>isset($data->DOCUMENTACION_PDTDJ)?$data->DOCUMENTACION_PDTDJ:null,
            '_documentoessalud' =>isset($data->DOCUMENTACION_ESSALUD)?$data->DOCUMENTACION_ESSALUD:null,
            '_documentocompleto' =>isset($data->DOCUMENTACION_COMPLETA)?$data->DOCUMENTACION_COMPLETA:null,
            '_rechazoMotivo' =>isset($data ->RECHAZO_MOTIVO)?$data->RECHAZO_MOTIVO:null,
            '_riesgosMonto' =>isset($data->RIESGOS_MONTO)?$data->RIESGOS_MONTO:null,
            '_profesion'=>isset($data->PROFESION)?$data->PROFESION:null,
            '_grandoInstrruccion'=>isset($data->GRADO_INSTRUCCION)?$data->GRADO_INSTRUCCION:null,
            '_fechadesembolsotentativa'=>isset($data->FECHA_DESEMBOLSO_TENTATIVA)?$data->FECHA_DESEMBOLSO_TENTATIVA:null,
            '_riesgosGestion'=>isset($data->RIESGOS_GESTION)?$data->RIESGOS_GESTION:null,
            '_riesgosSubGestion'=>isset($data->RIESGOS_GESTION_SUB)?$data->RIESGOS_GESTION_SUB:null,
            '_riesgosComentario'=>isset($data->RIESGOS_COMENTARIO)?$data->RIESGOS_COMENTARIO:null,
            '_estadosubasta'=>isset($data->ESTADO_SUBASTA)?$data->ESTADO_SUBASTA:null,
            '_tasamaximabcr'=>isset($data->TASA_MAXIMA_BCR)?$data->TASA_MAXIMA_BCR:null,
            '_tasafinalcliente'=>isset($data->TASA_FINAL_CLIENTE)?$data->TASA_FINAL_CLIENTE:null,
            '_gestionsubasta'=>isset($data->GESTION_SUBASTA)?$data->GESTION_SUBASTA:null,
            '_gestionsubastacomentario'=>isset($data->GESTION_SUBASTA_COMENTARIO)?$data->GESTION_SUBASTA_COMENTARIO:null,
            '_aprobacionSubastaFecha'=>isset($data->FECHA_APROBACIO_SUBASTA)?$data->FECHA_APROBACIO_SUBASTA:null,
            '_cuentaTipo'=>isset($data->CUENTA_TIPO)?$data->CUENTA_TIPO:null,
            '_cuentaNumero'=>isset($data->CUENTA_NUMERO)?$data->CUENTA_NUMERO:null,
            '_estadofile'=>isset($data->ESTADO_FILE)?$data->ESTADO_FILE:null,
            '_gestionValidacionDocumento'=>isset($data->VALIDACION_DOCUMENTACION_GESTION)?$data->VALIDACION_DOCUMENTACION_GESTION:null,
            '_flgcomunicorechazo'=>isset($data->FLG_COMUNICO_RECHAZO)?$data->FLG_COMUNICO_RECHAZO:null,
            '_fechacomunicorechazo'=>isset($data->FECHA_COMUNICO_RECHAZO)?$data->FECHA_COMUNICO_RECHAZO:null,
            '_wiofechaobservacion'=>isset($data->WIO_OBSERVACION_FECHA)?$data->WIO_OBSERVACION_FECHA:null,
            '_sector'=>isset($data->SECTOR)?$data->SECTOR:null,
            '_giro'=>isset($data->GIRO)?$data->GIRO:null,
            '_flgReactiva1'=>isset($data->FLG_TOMO_REACTIVA_1)?$data->FLG_TOMO_REACTIVA_1:null,
            '_montoReactiva1'=>isset($data->MONTO_PRESTAMO_REACTIVA_1)?$data->MONTO_PRESTAMO_REACTIVA_1:null,
            '_solicitudOfertaCobertura'=>isset($data->SOLICITUD_OFERTA_COBERTURA)?$data->SOLICITUD_OFERTA_COBERTURA:null,
            '_flgRTributarioTerceros'=>isset($data->FLG_REPORTE_TRIBUTARIO_TERCEROS)?$data->FLG_REPORTE_TRIBUTARIO_TERCEROS:null,
            '_bancoReactiva1'=>isset($data->BANCO_REACTIVA_1)?$data->BANCO_REACTIVA_1:null,
            '_flgTopeMaximo'=>isset($data->FLG_TOPE_MAXIMO)?$data->FLG_TOPE_MAXIMO:null,
            '_topeMaximo'=>isset($data->TOPE_MAXIMO)?$data->TOPE_MAXIMO:null,
            '_flagReporteTributario'=>isset($data->FLAG_REPORTE_TRIBUTARIO)?$data->FLAG_REPORTE_TRIBUTARIO:null,
            '_subastaTasa'=>isset($data->SUBASTA_TASA)?$data->SUBASTA_TASA:null,
            '_tipoEnvioGTP' => isset($data->TIPO_ENVIO_GTP)?$data->TIPO_ENVIO_GTP:null,
            '_tienda' => isset($data->TIENDA)?$data->TIENDA:null,
            '_flgdestinocompradolares' => isset($data->FLG_DESTINO_COMPRA_DOLARES)?$data->FLG_DESTINO_COMPRA_DOLARES:null,
            '_montocompradolares' => isset($data->MONTO_COMPRA_DOLARES)?$data->MONTO_COMPRA_DOLARES:null,
            '_flgaceptocuentasueldo' => isset($data->FLG_ACEPTO_CUENTA_SUELDO)?$data->FLG_ACEPTO_CUENTA_SUELDO:null,
            '_montoessalud' => isset($data->MONTO_PAGO_ESSALUD)?$data->MONTO_PAGO_ESSALUD:null,
            '_nrotrabajadoresdependientes' => isset($data->NRO_TRABAJADORES_QUINTA_CATEGORIA)?$data->NRO_TRABAJADORES_QUINTA_CATEGORIA:null,
            '_sueldopromedio' => isset($data->SUELDO_PROMEDIO)?$data->SUELDO_PROMEDIO:null,
            '_flgaceptacobrosimple' =>isset($data->FLG_COBRO_SIMPLE)?$data->FLG_COBRO_SIMPLE:null,
            '_cantclientespagan' =>isset($data->CS_CANT_CLIENTES_PAGAN)?$data->CS_CANT_CLIENTES_PAGAN:null,
            '_controlcobranza' =>isset($data->CS_CONTROL_COBRANZA)?$data->CS_CONTROL_COBRANZA:null,
            '_ticketpromcobranza' =>isset($data->CS_TICKET_PROM_COBRANZA)?$data->CS_TICKET_PROM_COBRANZA:null,
        ]);
    }

    function getByRuc($ruc){

        if (!$ruc){
            return null;
        }

        $model = new mLead();
        $data = $model->getLead($ruc)->first();
        if($data){
            $this->setProperties($data);
        }
        return $data;
    }

    function gestionarBpe($usuario,$data,$gestion,$telefono,$correo,$direccion,$solicitudVenta,$solicitudEssalud,
                          $solicitudMonto,$solicitudTasa,$comentario,$solicitudGracia,$empleados,
                          $cuentaTipo,$cuentaNumero,$sector,$giro,$flgReactiva1,$montoReactiva1,$cobertura
                        )
    {
        if (in_array($gestion,[Gestion::COMPLETO,Gestion::ENVIAR_RIESGOS]) &&
            (int)$solicitudVenta + (int)$this->_solicitudEssalud <= 0
            ){
            $this->setMessage('No has ingresado un monto valido para las ventas');
            return false;
        }

        //Fix para el envio gtp digital
        $solicitudMonto = $solicitudMonto? $solicitudMonto: $this->_solicitudOferta;

        if (in_array($gestion,[Gestion::COMPLETO,Gestion::ENVIAR_RIESGOS,Gestion::ENVIO_VALORADOS,Gestion::ENVIO_GTP]) &&
            ($solicitudMonto <= 1000 || ($this->_flgTopeMaximo == 1 && $solicitudMonto > (int)$this->_topeMaximo))
            ){
            $this->setMessage('La solicitud ingresada no es válida (debe ser entre 1,000 y ' . $this->_topeMaximo . ' soles)');
            return false;
        }

        // Si esta en riesgos, no puede ser gestionado cuando esta pendiente o conforme por riesgos
        if ($this->_contactoResultado == Gestion::ENVIAR_RIESGOS && (!$this->_riesgosGestion || $this->_riesgosGestion == Gestion::RIESGOS_PENDIENTE) ){
            $this->setMessage('El Lead no puede ser gestionado ya que esta en revisión por Riesgos');
            return false;
        }

        $hoy = Carbon::now();
        $model = new mLead();

        //En estos estados ya no deberia cambiarse la información personal del cliente
        if (!in_array($this->_contactoResultado, [Gestion::DESEMBOLSADO])){

            $this->_rrllDocumento   = isset($data['rucrrll'])?$data['rucrrll']:'NULL';
            $this->_rrllNombres     = isset($data['rrll'])?$data['rrll']:'NULL';
            $this->_rrllApellidos   = isset($data['rrllApellidos'])?$data['rrllApellidos']:'NULL';
            $this->_rrllEstadoCivil = isset($data['rrllEstadoCivil'])?$data['rrllEstadoCivil']:'NULL';

            $this->_rrllDocumento2   = isset($data['rucrrll2'])?$data['rucrrll2']:'NULL';
            $this->_rrllNombres2     = isset($data['rrll2'])?$data['rrll2']:'NULL';
            $this->_rrllApellidos2   = isset($data['rrllApellidos2'])?$data['rrllApellidos2']:'NULL';
            $this->_rrllEstadoCivil2 = isset($data['rrllEstadoCivil2'])?$data['rrllEstadoCivil2']:'NULL';
            $this->_telefono1        = $telefono;

            $this->_tipoVia         = isset($data['tipoVia'])?$data['tipoVia']:NULL;
            $this->_direcNro        = isset($data['direcNro'])?$data['direcNro']:NULL;
            $this->_direcInterior   = isset($data['direcInt'])?$data['direcInt']:NULL;
            $this->_direcDescripVia = isset($data['direcVia'])?$data['direcVia']:NULL;
            $this->_direcLote       = isset($data['direcLote'])?$data['direcLote']:NULL;
            $this->_direcManzana    = isset($data['direcManz'])?$data['direcManz']:NULL;
            $this->_direcUrb        = isset($data['direcUrb'])?$data['direcUrb']:NULL;

            if (isset($data['departamento']) && !empty($data['departamento']) && isset($data['provincia']) && !empty($data['provincia']) && isset($data['distrito']) && !empty($data['distrito'])) {
                $this->_ubigeo = $data['departamento'].$data['provincia'].$data['distrito'];
            }

            $this->_tienda                    = isset($data['tienda'])?$data['tienda']:'NULL';
            $this->_tipoSociedad              = isset($data['tipoSociedad'])?$data['tipoSociedad']:NULL;
            $this->_correo                    = $correo;
            $this->_solicitudVolumenVenta     = ($solicitudVenta)?$solicitudVenta:$this->_solicitudVolumenVenta;
            $this->_solicitudEssalud          = $solicitudEssalud;
            $this->_solicitudOferta           = ($solicitudMonto)?$solicitudMonto:$this->_solicitudOferta;
            $this->_contactoComentario        = $comentario;
            $this->_solicitudGracia           = ($solicitudGracia)?$solicitudGracia:$this->_solicitudGracia;
            $this->_nroEmpleados              = $empleados;
            $this->_contactoFecha             = $hoy;
            $this->_cuentaTipo                = $cuentaTipo;
            $this->_cuentaNumero              = $cuentaNumero;
            $this->_solicitudOfertaCobertura  = $cobertura;

        }

        


        /*Seteo de Solciitud Plazo simplificado, se basa en el textbox */
        switch ($this->_solicitudGracia) {
            case 3:
                $this->_solicitudPlazo = 33;
                break;
            case 6:
                $this->_solicitudPlazo = 30;
                break;
            case 12:
                $this->_solicitudPlazo = 24;
                break;
        }

        /* Logica para guardar el lead-etapa*/
        $leadEtapa = new EtapaLead();
        $leadEtapa->setValue('_ruc',$this->_ruc);
        $leadEtapa->setValue('_responsable',$usuario->getValue('_registro'));
        $leadEtapa->setValue('_comentario',$comentario);
        $leadEtapa->setValue('_fecha',$hoy);
        $leadEtapa->setValue('_montoSolicitud',$this->_solicitudOferta);
        $leadEtapa->setValue('_gestion',($gestion)?$gestion:$this->_contactoResultado);
        $leadEtapa->setValue('_ventas',$this->_solicitudVolumenVenta);
        $leadEtapa->setValue('_banca',$this->_banca);

        $ejecutivoLibre = null;
        $solicitudCredito = new SolicitudCredito();
        if($gestion == Gestion::INTERESADO || $gestion == Gestion::COMPLETO){

            $this->_etapa = Etapa::INTERESADO;
            $leadEtapa->setValue('_etapa',Etapa::INTERESADO);

        }elseif($gestion == Gestion::NO_ACEPTA){

            $this->_etapa = Etapa::RECHAZADO;
            $this->_rechazoFecha = $hoy;
            $leadEtapa->setValue('_etapa',Etapa::RECHAZADO);
            if ($this->_contactoResultado == Gestion::ENVIO_GTP || $this->_contactoResultado == Gestion::ENVIO_VALORADOS){
                $this->_rechazoMotivo = self::MOTIVO_RECHAZO_CLIENTE_TASA;
                $leadEtapa->setValue('_comentario',self::MOTIVO_RECHAZO_CLIENTE_TASA);
            }else{
                $this->_rechazoMotivo = self::MOTIVO_RECHAZO_CLIENTE;
            }

        }elseif($gestion == Gestion::NO_CALIFICA){

            $this->_etapa = Etapa::RECHAZADO;
            $this->_rechazoFecha = $hoy;
            $this->_rechazoMotivo = self::MOTIVO_RECHAZO_NO_CALIFICA;
            $leadEtapa->setValue('_etapa',Etapa::RECHAZADO);

        }elseif($gestion == Gestion::ENVIAR_RIESGOS){
            // Lógica de pase automático - Si pasa
            if ($solicitudMonto <= 750000 && $this->_riesgosGestion == Gestion::RIESGOS_CONFORME){
                $this->_etapa = Etapa::DOCUMENTOS_EVALUACION;
            }else{
                $this->_riesgosComentario = 'NULL';
                $this->_riesgosSubGestion = 'NULL';
                $this->_riesgosGestion = Gestion::RIESGOS_PENDIENTE;

                //SI NO TIENE UN EJECUTIVO ASIGNADO
                $ejecutivo = $model->getEjecutivoRiesgo($this->_ruc);
                if (!$ejecutivo){
                    $ejecutivo = $model->getEjecutivoMenorCarga($this->_banca);
                    $ejecutivoLibre['NUM_RUC'] = $this->_ruc;
                    $ejecutivoLibre['REGISTRO_RIESGOS'] = $ejecutivo->REGISTRO_RIESGOS;
                    $ejecutivoLibre['FECHA_ASIGNACION'] = $hoy->toDateTimeString();
                }

                //reenvio a riesgos
                if ($this->_etapa == Etapa::DOCUMENTOS_EVALUACION){
                    $leadEtapa->setValue('_gestion',Gestion::RIESGOS_PENDIENTE);
                }
                $leadEtapa->setValue('_etapa',Etapa::DOCUMENTOS_EVALUACION);
                $this->_etapa = Etapa::DOCUMENTOS_EVALUACION;
            }

            //reenvio a riesgos
            if ($this->_etapa == Etapa::DOCUMENTOS_EVALUACION){
                $leadEtapa->setValue('_gestion',Gestion::RIESGOS_PENDIENTE);
            }
            $leadEtapa->setValue('_etapa',Etapa::DOCUMENTOS_EVALUACION);
            $this->_etapa = Etapa::DOCUMENTOS_EVALUACION;

        }elseif($gestion == Gestion::ENVIO_VALORADOS){
            if ($this->_riesgosGestion == Gestion::RIESGOS_CONFORME
                && ($this->_riesgosMonto == 0 || $this->_solicitudOferta <= $this->_riesgosMonto) ){
                $this->_etapa = Etapa::DOCUMENTOS_EVALUACION;
                $leadEtapa->setValue('_etapa',Etapa::DOCUMENTOS_EVALUACION);

                //Si no tiene tasa, trabaja en base a la lógica de subasta
                if (!$this->_tasafinalcliente && !$this->_subastaTasa && $data['solicitudTasa']){
                    $this->_subastaTasa = $data['solicitudTasa'];
                    $solicitudCredito->setValues([
                        '_ruc' => $this->_ruc,
                        '_monto' => $solicitudMonto,
                        '_cobertura' => $cobertura,
                        '_tasa' => $data['solicitudTasa'],
                        '_fecha' => $hoy,
                        '_ejecutivo' => $usuario->getValue('_registro'),
                    ]);
                }

            }else{
                $this->setMessage('No se han cumplido los requisitos de riesgos (aprobación y monto)');
                return false;
            }
        }elseif($gestion == Gestion::ENVIO_GTP){
            if ($this->_riesgosGestion == Gestion::RIESGOS_CONFORME
                && (is_null($this->_riesgosMonto) || $this->_riesgosMonto == 0 || $this->_solicitudOferta <= $this->_riesgosMonto)
                && in_array($this->_contactoResultado,[Gestion::ENVIO_VALORADOS,Gestion::ENVIO_GTP])){
                    //Si no tiene tasa, trabaja en base a la lógica de subasta
                    if (!$this->_tasafinalcliente && $this->_contactoResultado == Gestion::ENVIO_VALORADOS && $data['solicitudTasa']
                            && !$this->_subastaTasa){
                        $this->_subastaTasa = $data['solicitudTasa'];
                        $solicitudCredito->setValues([
                            '_ruc' => $this->_ruc,
                            '_monto' => $solicitudMonto,
                            '_cobertura' => $cobertura,
                            '_tasa' => $data['solicitudTasa'],
                            '_fecha' => $hoy,
                            '_ejecutivo' => $usuario->getValue('_registro'),
                        ]);
                    }
                    $this->_etapa = Etapa::DERIVADO_GTP;
                    $leadEtapa->setValue('_etapa',Etapa::DERIVADO_GTP);
            }else{
                $this->setMessage('No se han cumplido los requisitos de riesgos (aprobación y monto)');
                return false;
            }
            $this->_tipoEnvioGTP = $data['tipoEnvioGTP'];
        }

        $this->_contactoResultado  = ($gestion)?$gestion:$this->_contactoResultado;
        if (isset($data['flg_creditocomprasdolares'])) {
          $this->_flgdestinocompradolares       = $data['flg_creditocomprasdolares'];
          $this->_montocompradolares            = ($this->_flgdestinocompradolares==1 && $data['montodolarescompra'])?$data['montodolarescompra']:0;
        }
        $this->_flgaceptocuentasueldo         = isset($data['flgcuensueldo'])?$data['flgcuensueldo']:null;
        $this->_montoessalud                  = isset($data['montopagoessalud'])?$data['montopagoessalud']:null;
        $this->_nrotrabajadoresdependientes   = isset($data['numtrabajadores'])?$data['numtrabajadores']:null;
        $this->_sueldopromedio                = isset($data['sueldopromedio'])?$data['sueldopromedio']:null;

        $this->_flgaceptacobrosimple          = isset($data['cs_flgcobrosimple'])?$data['cs_flgcobrosimple']:null;
        $this->_cantclientespagan             = isset($data['cs_cantclientespagan'])?$data['cs_cantclientespagan']:0;
        $this->_controlcobranza               = isset($data['cs_controlcobranza'])?$data['cs_controlcobranza']:0;
        $this->_ticketpromcobranza            = isset($data['cs_ticketpromediocobranza'])?$data['cs_ticketpromediocobranza']:0;

        //dd($data,$this->setValueToTable(),$leadEtapa->setValueToTable());
        if ($model->gestionar($this->setValueToTable(),$leadEtapa->setValueToTable()
            ,$ejecutivoLibre,$solicitudCredito->setValueToTable())){
            return true;
        }else{
            $this->setMessage($model->_message);
            return false;
        }

    }

    /*GESTION PARA BACKOFFICE*/

    function gestionarAsignacion($ruc,$flag,$usuario){
        $this->_ruc  = $ruc;
        $model = new mLead();
        $hoy = Carbon::now();

        //Si marco con check la cajita
        if ($flag == "true") {

            //Buscamos al cliente
            $this->getByRuc($ruc);

            $flag = "1";

            //Si no tiene ejecutivo asignado
            if (!$this->_asignadoReferido){
                //Obtenemos al ejecutivo de menor carga
                $ejecutivo = $model->getEjecutivoNegocioMenorCarga($this->_banca);
                if ($ejecutivo){
                    //$this->_asignadoReferido= $ejecutivo->REGISTRO;
                    $this->_fechaAsignadoBackoffice = $hoy->toDateTimeString();
                }else{
                    //Si no encuentra un ejecutivo
                    //$this->_asignadoReferido= null;
                    $this->_fechaAsignadoBackoffice = null;
                    return false;
                }

            //Si ya tuviera uno asignado, solo actualizamos la fecha
            }else{
                $this->_fechaAsignadoBackoffice = $hoy->toDateTimeString();
            }

            //Seteamos la información para el log
            $leadEtapa = new EtapaLead();
            $leadEtapa->setValue('_ruc',$this->_ruc);
            //$leadEtapa->setValue('_responsable',$usuario->getValue('_registro'));
            $leadEtapa->setValue('_fecha',$hoy);
            $leadEtapa->setValue('_banca',$this->_banca);
            $leadEtapa->setValue('_etapa',Etapa::ASIGNADO);

            //Actualizamos la información
            $model->updateFlag(
                $this->_ruc = $ruc,
                $this->_flagReporteTributario = $flag,
                $this->_fechaAsignadoBackoffice,
                //$this->_asignadoReferido,
                $leadEtapa->setValueToTable()
            );

            return true;

        }else{
            $flag="0";
            return false;
        }
    }

    function envioGTpBpe($usuario,$gestion,$monto){
        if ($this->_riesgosMonto >= 0 && ($monto > $this->_riesgosMonto)){
            $this->setMessage('El monto solicitado es mayor al admitido por riesgos');
            return false;
        }

        if ($this->_etapa == Etapa::DOCUMENTOS_EVALUACION &&
            $this->_riesgosGestion == Gestion::RIESGOS_CONFORME){
                $this->_solicitudOferta = $monto;

                $hoy = Carbon::now();
                /* Logica para guardar el lead-etapa*/
                $leadEtapa = new EtapaLead();

                if ($gestion == 1){
                    $this->_etapa = Etapa::DERIVADO_GTP;
                    $leadEtapa->setValue('_comentario','Se cerró la tasa acordada');
                }else if($gestion == 0){
                    $this->_etapa = Etapa::RECHAZADO;
                    $this->_rechazoMotivo = 'Cliente no acepto tasa';
                    $this->_rechazoFecha = $hoy;
                    $leadEtapa->setValue('_comentario','Cliente no acepto tasa');
                }else{
                    $this->setMessage('No se gestionó correctamente');
                    return false;
                }

                $leadEtapa->setValue('_ruc',$this->_ruc);
                $leadEtapa->setValue('_responsable',$usuario->getValue('_registro'));
                //$leadEtapa->setValue('_gestion',$gestion);

                $leadEtapa->setValue('_fecha',$hoy);
                $leadEtapa->setValue('_etapa',$this->_etapa);
                $model = new mLead();

                if ($model->gestionar($this->setValueToTable(),$leadEtapa->setValueToTable())){
                    return true;
                }else{
                    $this->setMessage('Hubo un error en base de datos. Por favor contáctese con el administrador');
                    return false;
                }
        }else{
            $this->setMessage('El cliente no se encuentra en la etapa adecuada');
            return false;
        }
    }

    function enviarInteresado($usuario,$data)
    {
        if ($this->_etapa == Etapa::INTERESADO){
            $this->_flgRTributarioTerceros = isset($data['flgrtterceros'])?(int)$data['flgrtterceros']:null;
            $this->_flgReactiva1 = isset($data['flgReactiva1'])?(int)$data['flgReactiva1']:null;
            $this->_montoReactiva1 = isset($data['montorp1'])?$data['montorp1']:null;
            $this->_solicitudVolumenVenta = isset($data['volventa'])?$data['volventa']:0;
            $this->_solicitudOferta = isset($data['soloferta'])?$data['soloferta']:null;
            $this->_solicitudTasa = isset($data['tasatentativa'])?$data['tasatentativa']:null;
            $this->_fechadesembolsotentativa = isset($data['fechadesempeniotentativa'])?$data['fechadesempeniotentativa']:null;
            $this->_correo = isset($data['email'])?$data['email']:null;
            $this->_telefono1 = isset($data['telefono'])?$data['telefono']:null;
            $this->_flgReactiva1 = isset($data['gflgReactiva1'])?$data['gflgReactiva1']:null;
            $this->_bancoReactiva1 = isset($data['bancor1'])?$data['bancor1']:null;

            $leadEtapa = new EtapaLead();

            $leadEtapa->setValue('_ruc',$this->_ruc);
            $leadEtapa->setValue('_responsable',$usuario->getValue('_registro'));
            $leadEtapa->setValue('_fecha',Carbon::now());

            if ($this->_flgRTributarioTerceros == 1 && (int)$data['condicion'] == 1){
                $this->_etapa = Etapa::DOCUMENTOS_EVALUACION;

                $leadEtapa->setValue('_etapa',Etapa::DOCUMENTOS_EVALUACION);
                $cambio = true;
            }else{
                $leadEtapa->setValue('_etapa',Etapa::INTERESADO);
                $cambio = false;
            }
            // dd($this->setValueToTable(),$leadEtapa->setValueToTable());
            $model = new mLead();
            if ($model->gestionar($this->setValueToTable(),$leadEtapa->setValueToTable())){
                if ($cambio) {
                    $this->setMessage('El cliente (' . $this->_ruc . ')' . $this->_nombre .' ha pasado a etapa de Evaluacion de Documentos');
                }else{
                    $this->setMessage('Los datos del cliente (' . $this->_ruc . ')' . $this->_nombre .' se han actualizado');
                }
                return true;
            }else{
                $this->setMessage('Hubo en error al conectarse con la Base de Datos. Contáctate con soporte.');
                return false;
            }

        }else{
            $this->setMessage('El lead no se encuentra en la etapa adecuada. Inténtalo nuevamente');
            return false;
        }

    }

    function documentacionCompletaBE (
        $accion,
        $usuario,$flagDJPDT,$flagESSALUD
        ,$volventa,$essalud,$soloferta,$cemail,$flgConfirmar,$tasaTentativa,$fechaTentativa
        ,$data)
    {
        // dd($usuario,$data);
        if ($this->_etapa == Etapa::DOCUMENTOS_EVALUACION){
           // $this->_documentocompleto = $respuesta;
            $this->_telefono1 = isset($data['telefono'])?$data['telefono']:null;
            $this->_correo = isset($data['email'])?$data['email']:null;
            $this->_flgRTributarioTerceros = isset($data['flgrtterceros'])?$data['flgrtterceros']:null;
            $this->_solicitudVolumenVenta = isset($data['volventa'])?$data['volventa']:null;
            $this->_flgReactiva1 = isset($data['gflgReactiva1'])?(int)$data['gflgReactiva1']:null;
            $this->_bancoReactiva1 = isset($data['bancor1'])?$data['bancor1']:null;
            $this->_montoReactiva1 = isset($data['gmontoReactiva1'])?$data['gmontoReactiva1']:null;
            $this->_solicitudOferta = isset($data['soloferta'])?$data['soloferta']:null;
            $this->_correoConfirmacion = isset($data['cemail'])?$data['cemail']:null;
            // dd($data,$this->setValueToTable());

            if ($accion == 1){
                if (
                    $flgConfirmar == 1 && ($this->_flgRTributarioTerceros == 1)
                    && $this->_correoConfirmacion
                ) {
                    $this->_etapa = Etapa::ENVIO_SUBASTA;
                    $hoy = Carbon::now();

                    $leadEtapa = new EtapaLead();
                    $leadEtapa->setValue('_ruc',$this->_ruc);
                    $leadEtapa->setValue('_responsable',$usuario->getValue('_registro'));
                    $leadEtapa->setValue('_fecha',$hoy);
                    $leadEtapa->setValue('_etapa',Etapa::ENVIO_SUBASTA);

                    $model = new mLead();
                    if ($model->gestionar($this->setValueToTable(),$leadEtapa->setValueToTable())){
                        $this->setMessage('El cliente (' . $this->_ruc . ')' . $this->_nombre .' ha pasado a etapa de envio de subasta');
                        return true;
                    }else{
                        $this->setMessage('Hubo en error al conectarse con la Base de Datos. Contáctate con soporte.');
                        return false;
                    }
                }else{
                    $this->setMessage('No se cumplieron las condiciones para enviar a subasta');
                        return false;
                }
            }else{
                $model = new mLead();
                if ($model->gestionar($this->setValueToTable(),null)){
                    $this->setMessage('Los datos del cliente (' . $this->_ruc . ')' . $this->_nombre .' se han actualizado');
                    return true;
                }else{
                    $this->setMessage('Hubo en error al conectarse con la Base de Datos. Contáctate con soporte.');
                    return false;
                }
            }

        }else{
            $this->setMessage('El lead no se encuentra en la etapa adecuada. Inténtalo nuevamente');
            return false;
        }
    }

    function enviarValDocumentacion($usuario,$flgCompleto,$flgenvrevision,$data){
        // dd($usuario,$flgCompleto,$flgenvrevision,$data);
        if ($flgenvrevision == 1 && $flgCompleto == 0){
            $this->setMessage('No puedes enviar un cliente sin la documentación completa');
            return false;
        }

        $this->_ruc = $data['numruc'];
        $this->_correo = isset($data['email'])?$data['email']:null;
        $this->_solicitudOferta = isset($data['soloferta'])?$data['soloferta']:null;
        $plazogracia = isset($data['plazogracia'])?explode('-',$data['plazogracia']):false;
        $this->_solicitudGracia = $plazogracia?$plazogracia[1]:null;
        $this->_solicitudPlazo = $plazogracia?$plazogracia[0]:null;

        $this->_flgReactiva1 = isset($data['gflgReactiva1'])?(int)$data['gflgReactiva1']:null;
        $this->_solicitudVolumenVenta = isset($data['volventa'])?$data['volventa']:null;
        $this->_bancoReactiva1 = isset($data['bancor1'])?$data['bancor1']:null;
        $this->_telefono1 = isset($data['telefono'])?$data['telefono']:null;
        $this->_montoReactiva1 = isset($data['gmontoReactiva1'])?$data['gmontoReactiva1']:null;

        $this->_documentocompleto = $flgCompleto;

        // dd($data,$this->setValueToTable());

        $model = new mLead();
        if ($this->_etapa == Etapa::DOCUMENTOS_DESEMBOLSO){

                if ($flgenvrevision == 1){
                    $this->_estadofile = GestionFile::EN_VALIDACION;
                    $this->_etapa = Etapa::WIO_INGRESADO;

                    $leadEtapa = new EtapaLead();
                    $leadEtapa->setValue('_ruc',$this->_ruc);
                    $leadEtapa->setValue('_responsable',$usuario->getValue('_registro'));
                    $leadEtapa->setValue('_fecha',Carbon::now());
                    $leadEtapa->setValue('_etapa',Etapa::WIO_INGRESADO);

                    if ($model->gestionar($this->setValueToTable(),$leadEtapa->setValueToTable())){
                        $this->setMessage('El cliente (' . $this->_ruc . ')' . $this->_nombre .' ha pasado a etapa de WIO ingresado');
                        return true;
                    }else{
                        $this->setMessage('Hubo en error al conectarse con la Base de Datos. Contáctate con soporte.');
                        return false;
                    }
                }else{
                    $this->_estadofile = GestionFile::PENDIENTE;
                    // dd($this->setValueToTable(),$data);
                    if ($model->gestionar($this->setValueToTable(),null)){
                        $this->setMessage('Se han actualizado los datos del cliente (' . $this->_ruc . ')' . $this->_nombre);
                        return true;
                    }else{
                        $this->setMessage('Hubo en error al conectarse con la Base de Datos. Contáctate con soporte.');
                        return false;
                    }
                }
        }else{
            $this->setMessage('El lead no se encuentra en la etapa adecuada. Inténtalo nuevamente');
            return false;
        }
    }

    static function listar($data = null,$banca = null,\App\Entity\Usuario $usuario = null, $todoBPE = false){
        $model = new mLead();
        if ($banca == \App\Entity\Banca::BE){
            return $model->listar($data['etapa'],$banca,$usuario->getValue('_registro'));
        }else if ($banca == \App\Entity\Banca::BC){
            return $model->listar($data['etapa'],$banca,$usuario->getValue('_registro'));
        }else{
            if ($todoBPE){
                return $model->listarBpe($data['etapa'],[Banca::BPE,Banca::RETAIL],null,$data);
            }

            if (in_array($usuario->getValue('_rol'),[Usuario::ROL_EJECUTIVO_NEGOCIO,Usuario::ROL_BACKOFFICE,Usuario::ROL_CANALES_DISTRIBUCION])){
                //return $model->listarBpe($data['etapa'],[Banca::BPE,Banca::RETAIL],null,$data);
                return $model->listarBpe($data['etapa'],[Banca::BPE,Banca::RETAIL],$usuario->getValue('_registro'),$data);
            }elseif(in_array($usuario->getValue('_rol'),[Usuario::ROL_TELEVENTAS,Usuario::ROL_SOLUCION_EMPRESA,Usuario::ROL_GERENTE_ZONA,Usuario::ROL_GERENTE_CENTRO,Usuario::ROL_ADMINISTRADOR,Usuario::ROL_GERENTE_TIENDA,Usuario::ROL_ANALISTA_RIESGOS_COBRANZA])){
                return $model->listarBpe($data['etapa'],[Banca::BPE,Banca::RETAIL],null,$data);
            }elseif (in_array($usuario->getValue('_rol'), [Usuario::ROL_SUBGERENTE_BPE,Usuario::ROL_GESTOR_CUENTA_SUELDO])) {
                if (Usuario::ROL_GESTOR_CUENTA_SUELDO) {
                  return $model->listarBpe($data['etapa'],Banca::BPE,null,$data,self::CLIENTES_CUENTA_SUELDO);
                }
                return $model->listarBpe($data['etapa'],Banca::BPE,null,$data);
            }elseif ($usuario->getValue('_rol') == \App\Entity\Usuario::ROL_SUPERVISOR_TELEVENTAS) {
                return $model->listarBpe($data['etapa'],$usuario->getValue('_banca')?$usuario->getValue('_banca'):[Banca::BPE,Banca::RETAIL],null,$data);
            }elseif (in_array($usuario->getValue('_rol'),[Usuario::ROL_EJECUTIVO_PRODUCTO,Usuario::ROL_ANALISTA_PRODUCTO])) {
                return $model->listarBpe($data['etapa'],Banca::BPE,null,$data,null,self::CLIENTES_COBROSIMPLE);
            }
        }
        return null;
    }



    function gestionsubasta($data=[],$registro)
    {
        $this->_ruc = isset($data['numruc'])?$data['numruc']:null;
        $this->_solicitudVolumenVenta = isset($data['volventa'])?$data['volventa']:null;
        $this->_flgReactiva1 = isset($data['gflgReactiva1'])?$data['gflgReactiva1']:null;
        $this->_bancoReactiva1 = isset($data['bancor1'])?$data['bancor1']:null;
        $this->_montoReactiva1 = isset($data['gmontoReactiva1'])?$data['gmontoReactiva1']:null;
        $this->_solicitudOferta = isset($data['soloferta'])?$data['soloferta']:null;

        $plazogracia = isset($data['plazogracia'])?explode('-',$data['plazogracia']):false;
        $this->_solicitudGracia = $plazogracia?$plazogracia[1]:null;
        $this->_solicitudPlazo = $plazogracia?$plazogracia[0]:null;

        $this->_tasafinalcliente = isset($data['tasafinal'])?$data['tasafinal']:null;
        $this->_gestionsubasta = isset($data['gestionsubasta'])?$data['gestionsubasta']:null;
        $this->_gestionsubastacomentario = isset($data['comentariosubasta'])?$data['comentariosubasta']:null;
        $this->_estadosubasta = 1;


        // dd($data,$this->setValueToTable());

        $model = new mLead();
        if(in_array($this->_gestionsubasta,array(GestionSubasta::ACEPTA))){
            $this->_etapa = Etapa::DOCUMENTOS_DESEMBOLSO;
            $this->_aprobacionSubastaFecha = Carbon::now();

            $leadEtapa = new EtapaLead();
            $leadEtapa->setValue('_ruc',$this->_ruc);
            $leadEtapa->setValue('_responsable',$registro);
            $leadEtapa->setValue('_fecha',Carbon::now());
            $leadEtapa->setValue('_etapa',Etapa::DOCUMENTOS_DESEMBOLSO);

            if ($model->gestionar($this->setValueToTable(),$leadEtapa->setValueToTable())){
                $this->setMessage('El cliente (' . $this->_ruc . ')' . $this->_nombre .' ha pasado a etapa de Documentos Desembolso');
                return true;
            }else{
                $this->setMessage('Hubo en error al conectarse con la Base de Datos. Contáctate con soporte.');
                return false;
            }

        }else{
            if($model->actualizarlead($this->setValueToTable(),$this->_ruc)){
                $this->setMessage('Se actualizo los datos del cliente (' . $this->_ruc . ')' . $this->_nombre);
                return true;
            }else{
                $this->setMessage('Hubo en error al conectarse con la Base de Datos. Contáctate con soporte.');
                return false;
            }
        }
    }

    function asignar($data=[],$registro)
    {
        $plazogracia = isset($data['plazogracia'])?explode('-',$data['plazogracia']):false;

        $this->_ruc = isset($data['numruc'])?$data['numruc']:null;
        $this->_nombre = isset($data['nempresa'])?$data['nempresa']:null;
        $this->_telefono1 = isset($data['telefono'])?$data['telefono']:null;
        $this->_correo = isset($data['correo'])?$data['correo']:null;
        $this->_solicitudVolumenVenta = isset($data['ventasa'])?$data['ventasa']:null;
        $this->_flgReactiva1 = isset($data['flgReactiva1'])?$data['flgReactiva1']:null;
        $this->_bancoReactiva1 = isset($data['addbancor1'])?$data['addbancor1']:null;
        $this->_montoReactiva1 = isset($data['addmontoReactiva1'])?$data['addmontoReactiva1']:null;
        $this->_contactoResultado = isset($data['gestion'])?$data['gestion']:null;
        $this->_solicitudOferta = isset($data['montosolicitado'])?str_replace(',','',$data['montosolicitado']):null;
        $this->_solicitudTasa = isset($data['tasa'])?$data['tasa']:null;
        $this->_banca = \App\Entity\Banca::BE;
        $this->_solicitudGracia = $plazogracia?$plazogracia[1]:null;
        $this->_solicitudPlazo = $plazogracia?$plazogracia[0]:null;
        $this->_asignadoReferido = $registro;
        $this->_campanha = isset($data['campana'])?$data['campana']:null;
        $this->_etapa = Etapa::REVISAR;
        // $this->_flujo = 'REFERIDO';
        $this->_canalActual = 'REFERIDO';
        // $this->_canalOrigen = 'REFERIDO';

        $leadEtapa = new EtapaLead();
        $leadEtapa->setValue('_ruc',$this->_ruc);
        $leadEtapa->setValue('_responsable',$registro);
        $leadEtapa->setValue('_fecha',Carbon::now());
        $leadEtapa->setValue('_etapa',Etapa::REVISAR);
        $leadEtapa->setValue('_comentario',isset($data['comentario'])?$data['comentario']:null);

        $model = new mLead();
        // dd($data,$this->setValueToTable(),$leadEtapa->setValueToTable());
        if ($model->asignar($this->setValueToTable(),$leadEtapa->setValueToTable())){
            $this->setMessage('El cliente (' . $this->_ruc . ')' . $this->_nombre .' ha sido agregado como referido');
            return true;
        }else{
            $this->setMessage('Hubo en error al conectarse con la Base de Datos. Contáctate con soporte.');
            return false;
        }

    }

    static function listarRiesgos(\App\Entity\Usuario $usuario = null,$estado = null){
        $model = new mLead();
        if ($usuario->getValue('_rol') == Usuario::ROL_RIESGO_BPE){
            return $model->listarRiesgos($usuario->getValue('_registro'),[Gestion::ENVIAR_RIESGOS,Gestion::ENVIO_VALORADOS,Gestion::ENVIO_GTP],null,$estado);
        }else{
            return $model->listarRiesgos(null,[Gestion::ENVIAR_RIESGOS,Gestion::ENVIO_VALORADOS,Gestion::ENVIO_GTP],null,$estado);
        }

    }

    static function listarSolEmp(\App\Entity\Usuario $usuario = null,$data = null){
        $model = new mLead();
        return $model->listarSolEmp($usuario->getValue('_registro'),$data);
    }

    function getByRiesgos(\App\Entity\Usuario $usuario,$ruc){
        $model = new mLead();
        if ($usuario->getValue('_rol') == Usuario::ROL_RIESGO_BPE){
            $data = $model->listarRiesgos($usuario->getValue('_registro'),[Gestion::ENVIAR_RIESGOS,Gestion::ENVIO_VALORADOS,Gestion::ENVIO_GTP],$ruc)->first();
        }else{
            $data = $model->listarRiesgos(null,[Gestion::ENVIAR_RIESGOS,Gestion::ENVIO_VALORADOS,Gestion::ENVIO_GTP],$ruc)->first();
        }

        $this->setProperties($data);
        return $data;
    }

    static function listarValidacion(\App\Entity\Usuario $usuario,$banca,$zonal,$ejecutivo){
        $model = new mLead();
        return $model->listarValidacionDocumentos(Etapa::VALIDACION_DOCUMENTOS,$banca,$zonal,$ejecutivo);
    }

    function gestionarValidacionDocumentos(\App\Entity\Usuario $usuario,$gestion){

        $this->_gestionValidacionDocumento = $gestion;

        if ($this->_etapa == Etapa::VALIDACION_DOCUMENTOS){

            if ($this->_gestionValidacionDocumento == Gestion::VALIDACION_APROBADO){
                $this->_etapa = Etapa::DERIVADO_GTP;
            }else if(in_array($this->_gestionValidacionDocumento,[Gestion::VALIDACION_REVISION])){
                $this->_etapa = Etapa::VALIDACION_DOCUMENTOS;
            }else if(in_array($this->_gestionValidacionDocumento,[Gestion::VALIDACION_RECHAZADO])){
                $this->_etapa = Etapa::DOCUMENTOS_DESEMBOLSO;
                $this->_estadofile = GestionFile::DEVUELTO;
            }else{
                $this->setMessage('No has ingresado una gestión válida');
                return false;
            }

            $hoy = Carbon::now();

            $leadEtapa = new EtapaLead();
            $leadEtapa->setValue('_ruc',$this->_ruc);
            $leadEtapa->setValue('_responsable',$usuario->getValue('_registro'));
            $leadEtapa->setValue('_fecha',$hoy);
            $leadEtapa->setValue('_etapa',$this->_etapa);
            $leadEtapa->setValue('_gestion',$this->_gestionValidacionDocumento);

            $model = new mLead();
            if ($model->gestionar($this->setValueToTable(),$leadEtapa->setValueToTable())){
                $this->setMessage('El cliente (' . $this->_ruc . ')' . $this->_nombre .' ha sido gestionado correctamente');
                return true;
            }else{
                $this->setMessage('Hubo en error al conectarse con la Base de Datos. Contáctate con soporte.');
                return false;
            }

        }else{
            $this->setMessage('El cliente no se encuentra en la etapa adecuada');
            return false;
        }
    }

    function gestionarRiesgos(\App\Entity\Usuario $usuario,$gestion,$subgestion,$comentario, $mtolimite){
        $this->_riesgosGestion = $gestion;
        $this->_riesgosComentario = $comentario;
        $this->_riesgosMonto = $mtolimite;
        $hoy = Carbon::now();

        if ($this->_riesgosGestion == Gestion::RIESGOS_OBSERVACION){
            $this->_etapa = Etapa::DOCUMENTOS_EVALUACION;
            $this->_riesgosSubGestion = $subgestion;
        }else if ($this->_riesgosGestion == Gestion::RIESGOS_CONFORME){
            $this->_etapa = Etapa::DOCUMENTOS_EVALUACION;
            $this->_riesgosSubGestion == 'NULL';
        }else if($this->_riesgosGestion == Gestion::RIESGOS_RECHAZO){
            $this->_riesgosSubGestion == 'NULL';
            $this->_etapa = Etapa::RECHAZADO;
            $this->_rechazoMotivo = 'Rechazado por Riesgos';
            $this->_rechazoFecha = $hoy;
        }

        $leadEtapa = new EtapaLead();
        $leadEtapa->setValue('_ruc',$this->_ruc);
        $leadEtapa->setValue('_responsable',$usuario->getValue('_registro'));
        $leadEtapa->setValue('_fecha',$hoy);
        $leadEtapa->setValue('_etapa',$this->_etapa);
        $leadEtapa->setValue('_gestion',$this->_riesgosGestion);
        $leadEtapa->setValue('_comentario',$this->_riesgosComentario);
        $leadEtapa->setValue('_banca',$this->_banca);

        $model = new mLead();
        if ($model->gestionar($this->setValueToTable(),$leadEtapa->setValueToTable())){
            $this->setMessage('El cliente (' . $this->_ruc . ')' . $this->_nombre .' ha sido gestionado correctamente');
            return true;
        }else{
            $this->setMessage('Hubo en error al conectarse con la Base de Datos. Contáctate con soporte.');
            return false;
        }

        return true;
    }



    function wioparacofide(\App\Entity\Usuario $usuario,$data)
    {
        $this->_ruc = isset($data['numruc'])?$data['numruc']:null;
        $this->_solicitudVolumenVenta = isset($data['volventa'])?$data['volventa']:0;
        $this->_montoReactiva1 = isset($data['gmontoReactiva1'])?$data['gmontoReactiva1']:0;
        // $this->_wiofechaobservacion = Carbon::now();
        // dd($data,$this->setValueToTable());
        $model = new mLead();
        if ($model->gestionar($this->setValueToTable(),null)){
            $this->setMessage('Se actualizó la información de la empresa (' . $this->_ruc . ')' . $this->_nombre);
            return true;
        }else{
            $this->setMessage('Hubo en error al conectarse con la Base de Datos. Contáctate con soporte.');
            return false;
        }
    }

    function acuerdos($ruc=null)
    {
        $model = new mLead();
        $data=$model ->datosLlenado($ruc);
        if ($data != null){
            return $data;
        }else{
            $this->setMessage('Hubo en error al conectarse con la Base de Datos. Contáctate con soporte.');
            return false;
        }
    }
    function datosLlenado($ruc=null){
        $model = new mLead();
        $data=$model ->datosLlenado($ruc);
        if ($data != null){
            return $data;
        }else{
            $this->setMessage('Hubo en error al conectarse con la Base de Datos. Contáctate con soporte.');
            return false;
        }
    }

    function getTasas(\App\Entity\Usuario $usuario,$ruc,$monto,$cobertura){
        $model = new mSubasta();
        $data=$model->getTasas($monto,$cobertura,[\App\Entity\Banca::RETAIL,\App\Entity\Banca::BPE]);
        return $data->first();
    }

    function direccion($ruc=null)
    {
        $model = new mLead();
        $data=$model ->direccionByRucLeads($ruc);
        if ($data != null){
            return $data;
        }else{
            $this->setMessage('Hubo en error al conectarse con la Base de Datos. Contáctate con soporte.');
            return false;
        }
    }
}
