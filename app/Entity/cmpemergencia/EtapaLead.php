<?php

namespace App\Entity\cmpemergencia;

class EtapaLead extends \App\Entity\Base\Entity {

    protected $_ruc;
    protected $_etapa;
    protected $_fecha;
    protected $_comentario;
    protected $_gestion;
    protected $_responsable;
    protected $_montoSolicitud;
    protected $_ventas;
    protected $_banca;
    

    function setValueToTable() {
        return $this->cleanArray([
            'NUM_RUC' => $this->_ruc,
            'ETAPA_ID' => $this->_etapa,
            'FECHA' => ($this->_fecha)? $this->_fecha->toDateTimeString() : null,
            'RESPONSABLE' => $this->_responsable,
            'COMENTARIO' => $this->_comentario,
            'GESTION_ID' => $this->_gestion,
            'SOLICITUD_MONTO' => $this->_montoSolicitud,
            'VENTAS' => $this->_ventas,
            'BANCA' => $this->_banca,
            
        ]);
    }

}
