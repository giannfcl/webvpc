<?php

namespace App\Entity\cmpemergencia;

use App\Model\cmpemergencia\Maestras as mMaestra;

class GradoInstruccion extends \App\Entity\Base\Entity {

    static function getAll(){
        $model = new mMaestra();
        return $model->getGradosInstruccion()->get();
    }
}