<?php

namespace App\Entity;
use App\Model\EncuestaInterna as mEncuesta;
use Jenssegers\Date\Date as Carbon;

class EncuestaInterna extends \App\Entity\Base\Entity {


    function getAll(){

    }

    function registrar(){

    }

    static function getAnalistasUsuario($registroUsuario){
            $model= new mEncuesta();
            return $model->getAnalistasUsuario($registroUsuario)->get();
    }

    static function getAnalistasUsuarioData($registroUsuario){
            $model= new mEncuesta();
            return $model->getAnalistasUsuarioData($registroUsuario)->get();
    }

    static function getPreguntas(){
            $model= new mEncuesta();
            return $model->getPreguntas()->get();
    }

    static function getPuntaje(){
            $model= new mEncuesta();
            return $model->getPuntaje()->get();
    }

    static function getPreguntaGerencia($registroUsuario){
            $model= new mEncuesta();
            return $model->getPreguntaGerencia($registroUsuario);
    }

    static function getResumen(){
            $model= new mEncuesta();
            return $model->getResumen()->get();
    }

    static function getPromedioGeneral(){
            $model= new mEncuesta();
            return $model->getPromedioGeneral()->first();
    }

    static function getAvance(){
            $model= new mEncuesta();
            return $model->getAvance()->get();
    }
  
    function guardarResultados($registroEvaluador,$preguntas,$vacio,$puntajeGerencia){
        //dd($puntajeGerencia[1]);
        $hoy=Carbon::now();

        $analistasConocidos=[];
        $analistasRelacionados=[];
        $encuestaAnalista=[];
        $consulta=mEncuesta::getRegistroAnalistasUsuario($registroEvaluador)->toArray();    
        foreach ($consulta as $analista) $analistasRelacionados[]=$analista->REGISTRO_ANALISTA;

        //dd($analistasRelacionados);
        if(!$vacio){
            foreach ($preguntas as $pregunta) {
                $analistasConocidos[]=$pregunta['registro'];
                $encuestaAnalista[]=[
                    'REGISTRO_JEFE'=>$registroEvaluador,
                    'REGISTRO_ANALISTA'=>$pregunta['registro'],
                    'FLG_RECONOCE'=>1,
                    'PREGUNTA_1'=>$pregunta[1],
                    'PREGUNTA_2'=>$pregunta[2],
                    'PREGUNTA_3'=>$pregunta[3],
                    'SUGERENCIA'=>$pregunta['sugerencia'],
                    'FECHA_REGISTRO'=>$hoy,
                    'FLG_ULTIMO'=>1
                ];
            }
        }
        
        foreach($analistasRelacionados as $key){
            if(!in_array($key,$analistasConocidos)){
                    $encuestaAnalista[]=[
                    'REGISTRO_JEFE'=>$registroEvaluador,
                    'REGISTRO_ANALISTA'=>$key,
                    'FLG_RECONOCE'=>0,
                    'PREGUNTA_1'=>NULL,
                    'PREGUNTA_2'=>NULL,
                    'PREGUNTA_3'=>NULL,
                    'SUGERENCIA'=>NULL,
                    'FECHA_REGISTRO'=>$hoy,
                    'FLG_ULTIMO'=>1
                ];
            }
        }

        //dd("ALTO");
        //dd($analistasRelacionados);

        $encuestaGerencia=[
            'REGISTRO_EVALUADOR'=>$registroEvaluador,
            //'PUNTAJE'=>$puntajeGerencia[1],
            //'PUNTAJE_2'=>$puntajeGerencia[2],
            'FECHA_REGISTRO'=>$hoy,
            'FLG_ULTIMO'=>1
        ];
        //dd($encuestaGerencia);
        $model=new mEncuesta();
        if ($model->guardarResultados($registroEvaluador,$analistasRelacionados,$encuestaAnalista,$encuestaGerencia)){
            return true;
        }else{
            return false;    
        }
        

    }

}
