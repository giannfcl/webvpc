<?php

namespace App\Entity;

class CitaEstadoHistorico extends \App\Entity\Base\Entity {

	protected $_id;
	protected $_periodo;
	protected $_lead;
	protected $_estado;
	protected $_fechaCita;
	protected $_fechaRegistro;
	protected $_usuario;


	function setValueToTable() {
        return $this->cleanArray([
        	'ID_CITA' => $this->_id,
	        'PERIODO' => $this->_periodo,
	        'ESTADO' => $this->_estado,
	        'FECHA_CITA' => ($this->_fechaCita)? $this->_fechaCita->toDateTimeString() : null,
	        'NUM_DOC' => $this->_lead,
	        'FECHA_REGISTRO' => ($this->_fechaRegistro)? $this->_fechaRegistro->toDateTimeString() : null,
	        'USUARIO' => $this->_usuario,
        ]);
    }
}