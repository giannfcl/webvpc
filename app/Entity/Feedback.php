<?php

namespace App\Entity;

use Jenssegers\Date\Date as Carbon;
use App\Model\Feedback as mFeedback;

class Feedback extends \App\Entity\Base\Entity {

    protected $_periodo;
    protected $_fechaRegistro;
    protected $_ejecutivo;
    protected $_lead;
    protected $_tipoRegistro;
    protected $_feedback;
    protected $_valor;
    protected $_flgUltimo;

    const TREGISTRO_TELEFONO = 1;
    const TREGISTRO_DIRECCION = 2;
    const TREGISTRO_EMAIL = 3;

    const TREGISTRO_TELEFONO_TEXTO = 'TELEFONO';
    const TREGISTRO_DIRECCION_TEXTO = 'DIRECCION';
    const TREGISTRO_EMAIL_TEXTO = 'EMAIL';

    const FEEDBACK_POSITIVO = 'POSITIVO';
    const FEEDBACK_NEGATIVO = 'NEGATIVO';
    const FEEDBACK_NEUTRO = 'NEUTRO';

    const FLG_ULTIMO = '1';

    function setValueToTable() {
        return $this->cleanArray([
            'PERIODO' => $this->_periodo,
            'NUM_DOC' => $this->_lead,
            'REGISTRO_EN' => $this->_ejecutivo,
            'FECHA_REGISTRO' => ($this->_fechaRegistro)? $this->_fechaRegistro->toDateTimeString() : null,
            'TIPO_REGISTRO' => $this->_tipoRegistro,
            'VALOR' => $this->_valor,
            'FEEDBACK' => $this->_feedback,
            'FLG_ULTIMO' => $this->_flgUltimo,
        ]);
    }

    function registrar() {
        $this->_fechaRegistro = Carbon::now();
        $this->_periodo = $this->getPeriodo();
        $this->_flgUltimo = self::FLG_ULTIMO;

        $model = new mFeedback();
        if ($model->insert($this->setValueToTable())){
            return true;
        }else{
            $this->setMessage('Hubo un error al registrar su información. Inténtelo más tarde.');
            return false;
        }
    }

    function eliminar() {
        $model = new mFeedback();
        $this->_periodo = $this->getPeriodo();
        if ($model->eliminar($this->setValueToTable())){
            return true;
        }else{
            $this->setMessage('Hubo un error al procesar la información. Inténtelo más tarde.');
            return false;
        }
    }

    static function listar($lead){
        $model = new mFeedback();
        $result = $model->listar(self::sgetPeriodo(),$lead)->get();
        $feedback = [];
        foreach ($result as $key => $value) {
            $feedback[$value->VALOR] = $value->FEEDBACK;
        }
        return $feedback;
    }
}