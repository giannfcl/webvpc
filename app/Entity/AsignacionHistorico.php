<?php

namespace App\Entity;

class AsignacionHistorico extends \App\Entity\Base\Entity {

    protected $_periodo;
    protected $_reasignador;
    protected $_ejecutivoOrigen;
    protected $_ejecutivoDestino;
    protected $_fechaRegistro;
    protected $_lead;
    protected $_tipo;

    const TIPO_ASIGNACION = 'A';
    const TIPO_REASIGNACION = 'R';

    function setValueToTable() {
        return $this->cleanArray([
                    'PERIODO' => $this->_periodo,
                    'REGISTRO_REASIGNADOR' => $this->_reasignador,
                    'REGISTRO_EN_ORIGEN' => $this->_ejecutivoOrigen,
                    'REGISTRO_EN_DESTINO' => $this->_ejecutivoDestino,
                    'NUM_DOC' => $this->_lead,
                    'FECHA_REGISTRO' => $this->_fechaRegistro->toDateTimeString(),
                    'TIPO_ASIGNACION' => $this->_tipo,
        ]);
    }

}

