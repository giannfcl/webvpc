<?php

namespace App\Entity;

use App\Model\LeadsEjecutivo as mLeadEjecutivo;
use App\Model\Lead as mLead;
use Jenssegers\Date\Date as Carbon;
use App\Entity\Cita as Cita;
use App\Entity\CitaEstadoHistorico as CitaEstadoHistorico;
use App\Entity\CitaEstado as CitaEstado;
use App\Entity\AsignacionHistorico as AsignacionHistorico;
use App\Entity\canalHistorico as CanalHistorico;

class LeadEjecutivo extends \App\Entity\Base\Entity {

	protected $_periodo;
	protected $_lead;
	protected $_ejecutivo;
	protected $_fechaAsignacion;
	protected $_flgUltimo;
	protected $_marcaAsistenteComercial;
	protected $_etiquetaEjecutivo;
    protected $_leads;

	const TIPO_ASIGNACION= "A";
	const TIPO_RSIGNACION= "R";

	function setValueToTable() {
        return $this->cleanArray([
             'PERIODO' => $this->_periodo,
             'NUM_DOC' => $this->_lead,
             'REGISTRO_EN' => $this->_ejecutivo,
             'FECHA_ASIGNACION' => $this->_fechaAsignacion,
             'FLG_ULTIMO'=>$this->_flgUltimo,
             'MARCA_ASISTENTE_COMERCIAL'=>$this->_marcaAsistenteComercial,
             'ETIQUETA_EJECUTIVO'=>$this->_etiquetaEjecutivo,
         ]);
    }

    function actualizarFlags($numdoc){
        $lEcjecutvio = new \App\Model\LeadsEjecutivo();
        if($lEjecutivo->actualizarFlags(numdoc)){
            $this->setMessage('Leads actualizado');
            return true;
        } else{
          $this->setMessage('Error al actualizar en la base de datos');
          return false;
        }
    }

    function insertarRegistro(){
        $lEcjecutvio = new \App\Model\LeadsEjecutivo();
        if($lEjecutivo->setLeadUltimoSoporte($this->setValueToTable())){
            $this->setMessage('Leads actualizado');
            return true;
        }
        else{
            $this->setMessage('Error al actualizar en la base de datos');
            return false;
        }
    }

    function updateEtiqueta(){
        //$this->_periodo = $this->getPeriodo();
        $model = new mLeadEjecutivo();
        if ($model->updateMarca($this->setValueToTable(), $this->getPeriodo())){
            return true;
        }else{
            $this->setMessage('Hubo un error al registrar su información. Inténtelo más tarde.');
            return false;
        }
    }

    function updateMarca(){
        //$this->_periodo = $this->getPeriodo();
        $model = new mLeadEjecutivo();
        if ($model->updateEtiqueta($this->setValueToTable())){
            return true;
        }else{
            $this->setMessage('Hubo un error al registrar su información. Inténtelo más tarde.');
            return false;
        }
    }

    static function asignarLeads($leads_array){
        $num_docs = array();
        $data = array();
        $model = new mLeadEjecutivo();

        foreach ($leads_array as $lead) {
            \Debugbar::info($lead);
            array_push($num_docs, $lead->_lead);
            array_push($data, $lead->setValueToTable());
        }

        $model->actualizarFlags($num_docs);
        $model->asignarLeads($data);
    }

    public function asignacionMasiva($asignador){

        $hoy = Carbon::now();

        $lModel = new mLead();
        $cleads = $lModel->getLeadEjecutivo(null,null,$this->_leads)->get();

        //Revision de existencia de leads
        $lead_canal = array();
        $lead_ejecutivo = array();
        $lead_periodo = array();
        foreach($cleads as $lead){

            $canales = array_filter(explode('|',$lead->CANALES));
            $lead_canal[$lead->NUM_DOC] = isset($canales[0])? $canales[0]:null ;
            $lead_ejecutivo[$lead->NUM_DOC] = $lead->EN_REGISTRO;
            $lead_periodo[$lead->NUM_DOC] = $lead->PERIODO;
        }

        $asignaciones = [];
        foreach($this->_leads as $lead){
            if ($lead_ejecutivo[$lead] != $this->_ejecutivo)
                $asignaciones[] = [
                     'PERIODO' => $lead_periodo[$lead],
                     'NUM_DOC' => $lead,
                     'REGISTRO_EN' => $this->_ejecutivo,
                     'FECHA_ASIGNACION' => $hoy->toDateTimeString(),
                     'FLG_ULTIMO'=> 1
                ];
        }


        $canalHistorico= [];
        foreach($this->_leads as $lead){
            $objCanalHist = new CanalHistorico();
            $objCanalHist->setValues([
                '_periodo' => $lead_periodo[$lead],
                '_lead' => $lead,
                '_canalOrigen' => $lead_canal[$lead],
                '_canalDestino' => Canal::EJECUTIVO_NEGOCIO,
                '_fechaActualizacion' => $hoy,
                '_usuario' => $asignador,
            ]);
            $canalHistorico[] = $objCanalHist->setValueToTable();
        }


        $asignacionHistorico = [];
        foreach($this->_leads as $lead){
            $objAsigHist = new AsignacionHistorico();
            $objAsigHist->setValues([
                '_periodo' => $this->getPeriodo(),
                '_reasignador' => $asignador,
                '_ejecutivoOrigen' => $lead_ejecutivo[$lead]? $lead_ejecutivo[$lead]:'NULL',
                '_ejecutivoDestino' => $this->_ejecutivo,
                '_fechaRegistro' => $hoy,
                '_lead' => $lead,
                '_tipo' => AsignacionHistorico::TIPO_ASIGNACION,
            ]);
            $asignacionHistorico[] = $objAsigHist->setValueToTable();
        }

        $model = new mLeadEjecutivo();
        if ($model->asignarLeads($this->_leads,$asignaciones,$canalHistorico,$asignacionHistorico)){
            $this->setMessage('Se asignó ' . count($this->_leads) . ' lead(s) de manera correcta');
            return true;
        }else{
            $this->setMessage('Hubo un error al registrar su información. Inténtelo más tarde.');
            return false;
        }
    }

    public function reasignacionMasiva($reasignador,$leads,$citas,$fechas,$horas,$ejecutivo){
        
        $hoy = Carbon::now();

        $reasignaciones = [];
        $reasigHistorico = [];
        
        
        //Revision de existencia de leads
        $lModel = new mLead();
        $cleads = $lModel->getLeadEjecutivo(null,null,$leads)->get();
        //Formateo de xistentes
        $lead_periodo = array();
        $lead_ejecutivo = array();
        foreach($cleads as $lead){

            $canales = array_filter(explode('|',$lead->CANALES));
            $lead_periodo[$lead->NUM_DOC] = $lead->PERIODO;
            $lead_ejecutivo[$lead->NUM_DOC] = $lead->EN_REGISTRO;
        }
        
        foreach($leads as $lead){
            $reasignaciones[] = [
                 'PERIODO' => $lead_periodo[$lead],
                 'NUM_DOC' => $lead,
                 'REGISTRO_EN' => $ejecutivo,
                 'FECHA_ASIGNACION' => Carbon::now()->toDateTimeString(),
                 'FLG_ULTIMO'=> 1
            ];
            $objReasigHist = new AsignacionHistorico();
            $objReasigHist->setValues([
                '_periodo' => $lead_periodo[$lead],
                '_reasignador' => $reasignador,
                '_ejecutivoOrigen' => null,
                '_ejecutivoDestino' => $ejecutivo,
                '_fechaRegistro' => Carbon::now(),
                '_lead' => $lead,
                '_tipo' => AsignacionHistorico::TIPO_REASIGNACION,
            ]);
            $reasigHistorico[] = $objReasigHist->setValueToTable();
        }

        // Si hay cambio de fecha de citas
        $citasFields = [];
        $historicoCitasFields = [];
        foreach ($citas as $key => $cita) {
            
            $objCita = new Cita();
            $objCita->setValues([
                '_id' => $citas[$key],
                '_ejecutivo' => $ejecutivo,
                //'_fechaCita' => $fechaCita,
            ]);



            $objCitaHist = new CitaEstadoHistorico();
            $objCitaHist->setValues([
                '_id' => $citas[$key],
                '_periodo' => $this->getPeriodo(),
                '_estado' => CitaEstado::REASIGNADO,
                //'_fechaCita' => $fechaCita,
                '_lead' => $leads[$key],
                '_fechaRegistro' => $hoy,
                '_usuario' => $reasignador,
            ]);

            if (isset($fechas[$key]) && isset($horas[$key])){
                $fechaCita = Carbon::createFromFormat('Y-m-d H:i',$fechas[$key] . ' ' . $horas[$key]);
                $objCita->setValue('_fechaCita',$fechaCita);
                $objCitaHist->setValue('_fechaCita',$fechaCita);
            }

            $citasFields[] = $objCita->setValueToTable();
            $historicoCitasFields[] = $objCitaHist->setValueToTable();
        }
        $model = new mLeadEjecutivo();
        if ($model->reasignarLeads($reasignaciones,$reasigHistorico,$citasFields,$historicoCitasFields)){
            $this->setMessage('Se reasignaron ' . count($leads) . ' lead(s) de manera correcta');
            return true;
        }else{
            $this->setMessage('Hubo un error al registrar su información. Inténtelo más tarde.');
            return false;
        }
    }
}

