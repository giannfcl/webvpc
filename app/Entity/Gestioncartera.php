<?php

namespace App\Entity;

use App\Model\Gestioncartera as mCartera;

class Gestioncartera extends \App\Entity\Base\Entity
{
    //Primer Filtro
    static function getArbol($id)
    {
        $arboll = mCartera::getArbol($id);
        return $arboll;
    }
    static function insertHA($datos, $registro, $id)
    {
        mCartera::heatanalytic($datos, $registro, $id);
    }
    static function getBanca($registro)
    {
        $banca = mCartera::getBanca($registro);
        return (isset($banca) ? $banca : null);
    }
    static function getZonal($registro)
    {
        $zonal = mCartera::getZonal($registro);
        return (isset($zonal) ? $zonal : null);
    }
    static function DetalleClienteFOTO($datos){
        $tabla = mCartera::DetalleClienteFOTO($datos);
        return (isset($tabla) ? $tabla : null);
    }
    static function DetalleClienteEvolutivo($datos)
    {
        $evolutivo = mCartera::DetalleClienteEvolutivo($datos);
        return (isset($evolutivo) ? $evolutivo : null);
    }
    static function getJefe($registro)
    {
        $jefe = mCartera::getJefe($registro);
        return (count($jefe)>0 ? $jefe : null);
    }
    static function getEjecutivo($registro)
    {
        $ejecutivo = mCartera::getEjecutivo($registro);
        return (isset($ejecutivo) ? $ejecutivo : null);
    }

    //Filtro UI
    static function getBancas()
    {
        $bancas = mCartera::getBancas();
        return (isset($bancas) ? $bancas : null);
    }
    static function getZonales($banca)
    {
        $zonales = mCartera::getZonales($banca);
        return (isset($zonales) ? $zonales : null);
    }
    static function getJefes($banca, $zonal)
    {
        $jefes = mCartera::getJefes($banca, $zonal);
        return (isset($jefes) ? $jefes : null);
    }
    static function getEjecutivos($banca, $zonal, $jefe)
    {
        $ejecutivos = mCartera::getEjecutivos($banca, $zonal, $jefe);
        return (isset($ejecutivos) ? $ejecutivos : null);
    }


    static function coldirectas($datos = null)
    {
        $coldirectas = mCartera::coldirectas($datos);
        return $coldirectas;
    }
    static function colindirectas($datos = null)
    {
        $colindirectas = mCartera::colindirectas($datos);
        return $colindirectas;
    }
    static function depositos($datos = null)
    {
        $depositos = mCartera::depositos($datos);
        return $depositos;
    }
    static function tipoProducto($banca = null, $zonal = null, $jefe = null, $encargado = null)
    {
        $tipoProducto = mCartera::Tipo_productos($banca, $zonal, $jefe, $encargado);
        return $tipoProducto;
    }
    static function VarXProducto($datos = null)
    {
        $VarXProducto = mCartera::VarXProducto($datos);
        return $VarXProducto;
    }

    static function getCumplimiento($datos = null)
    {
        $Cumplimiento = mCartera::getCumplimiento($datos);
        return $Cumplimiento;
    }
    static function getUsuario2($datos = null)
    {

        $usuario = mCartera::getUsuario2($datos['banca'], $datos['zonal'], $datos['jefe'], $datos['ejecutivo']);
        return $usuario;
    }
    static function getTopBtm($datos = null)
    {
        $datosTB = mCartera::getTopBtm($datos);
        return $datosTB;
    }
    static function getTopBottomMERCADO($datos = null)
    {
        $datosTB = mCartera::getTopBottomMERCADO($datos);
        return $datosTB;
    }
    static function getTopBottomMERCADOFILTRO1($datos = null)
    {
        $datosTB = mCartera::getTopBottomMERCADOFILTRO1($datos);
        return $datosTB;
    }
    static function getTopBottomMERCADOFILTRO2($datos = null)
    {
        $datosTB = mCartera::getTopBottomMERCADOFILTRO2($datos);
        return $datosTB;
    }
    static function tipoFrecuencia($datos = null)
    {
        $list = mCartera::tipoFrecuencia($datos);
        return $list;
    }
    static function MercadoTabla($datos = null)
    {
        $datosTabla = mCartera::MercadoTabla($datos);
        return $datosTabla;
    }
    static function LineaTabla($datos = null)
    {
        $datosTabla = mCartera::LineaTabla($datos);
        return $datosTabla;
    }

    static function MercadoPieGrafico($datos = null)
    {
        $datosGrafico = mCartera::MercadoPieGrafico($datos);
        return $datosGrafico;
    }
    static function MercadoLineGrafico($datos = null)
    {
        $datosGrafico = mCartera::MercadoLineGrafico($datos);
        return $datosGrafico;
    }
    static function tipoProductosBtm($datos = null)
    {
        $list = mCartera::tipoProductosBtm($datos);
        return $list;
    }
    static function Descuentos($datos = null)
    {
        $arr = mCartera::Descuentos($datos);
        return $arr;
    }
    static function PAP($datos = null)
    {
        $arr = mCartera::PAP($datos);
        return $arr;
    }
    static function CSUELDO($datos = null)
    {
        $arr = mCartera::CSUELDO($datos);
        return $arr;
    }
    static function CUENTASUELDO($datos = null)
    {
        $arr = mCartera::CUENTASUELDO($datos);
        return $arr;
    }
}
