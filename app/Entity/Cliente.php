<?php

namespace App\Entity;

use App\Model\Cliente as modelCliente;
use \Illuminate\Pagination\LengthAwarePaginator as Paginator;
use App\Entity\Canal as Canal;

class Cliente extends \App\Entity\Base\Entity {

    const ITEMS_PER_PAGE = 25;

    static function getClientesEjecutivoNegocio($en,$busqueda,$orden){
        $model= new modelCliente();
        $query= $model->getClienteEjecutivo(
            self::sgetPeriodo(),
            $en, 
            null, //no es necesario especificar el lead porque traes todos
            array_filter($busqueda),
            $orden);

        //dd($query->get());


        $totalCount = $query->count();
        $results = $query
                ->take(self::ITEMS_PER_PAGE)
                ->skip(self::ITEMS_PER_PAGE * ($busqueda['page'] - 1))
                ->get();

        $paginator = new Paginator($results, $totalCount, self::ITEMS_PER_PAGE, $busqueda['page']);

        return $paginator;
    }

    static function imprimirClientes($en,$busqueda,$orden){
        $model= new modelCliente();
        $query= $model->getClienteEjecutivo(
            self::sgetPeriodo(),
            $en, 
            null, //no es necesario especificar el lead porque traes todos
            array_filter($busqueda),
            $orden);

        return $query->get();
    }



    static function getProductosBPE(){
        $model=new modelCliente();
        $query=$model->getProductosBPE();
        return $query->get();
    }

    static function getCanalesCartera(){
        $model=new modelCliente();
        $query=$model->getCanalesCartera();
        return $query->get();
    }

    static function getCanalesAtencionCartera(){
        $model=new modelCliente();
        $query=$model->getCanalesAtencionCartera();
        return $query->get();
    }

    static function getMotivosBloqueoBPE(){
        $model=new modelCliente();
        $query=$model->getMotivosBloqueoBPE();
        return $query->get();
    }

    static function getClientesAsistenteComercial($as,$busqueda,$orden){
        $model= new modelCliente();
        $query= $model->getClientesAsistenteComercial(
            self::sgetPeriodo(),
            $as, 
            null, //no es necesario especificar el lead porque traes todos
            array_filter($busqueda),
            $orden);

        //dd($query->get());

        $totalCount = $query->count();
        $results = $query
                ->take(self::ITEMS_PER_PAGE)
                ->skip(self::ITEMS_PER_PAGE * ($busqueda['page'] - 1))
                ->get();

        $paginator = new Paginator($results, $totalCount, self::ITEMS_PER_PAGE, $busqueda['page']);

        return $paginator;


    }



    static function getClienteEjecutivoNegocio($en,$cliente){
        $model= new modelCliente();
        $query= $model->getClienteEjecutivo(
            self::sgetPeriodo(),
            $en, 
           $cliente);
        //dd($query->get());
        return $query->first();
    }

}