<?php
namespace App\Entity;

use App\Model\Usuario as mUsuario;
use App\Model\Landing as mLanding;
use Jenssegers\Date\Date as Carbon;
use DB;

class Landing extends \App\Entity\Base\Entity
{

	static function getClientesLanding($data)
	{
		return mLanding::getClientesLanding($data);
	}

	static function gestionarLanding($data)
	{
		// dd(Carbon::now()->toString());
		$actualizar=[
			'OTROS_COMENTARIOS'=>$data['formotroscomentarios'],
			'ACUERDOS'=>$data['formacuerdos'],
			'FECHA_GESTION'=>Carbon::now()
		];
		return mLanding::gestionarLanding($data,$actualizar);
	}
}

?>