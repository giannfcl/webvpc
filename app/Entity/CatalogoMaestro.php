<?php
namespace App\Entity;

use App\Model\Catalogo as Model;


class CatalogoMaestro extends \App\Entity\Base\Entity {

    static function getCatalogo($sector = null,$catalogo,$name) {
        $model = new Model();
        if (!\Cache::has($name.$sector))
        {
            $data = $model->get($catalogo,$sector);
            \Cache::put($name.$sector,$data,99999);
        }
        else
        {
            $data = \Cache::get($name.$sector);
        }
        return $data;
    }
}