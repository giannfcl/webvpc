<?php
namespace App\Entity;

use App\Model\Catalogo as Model;


class Sector extends \App\Entity\Base\Entity {

    const CODIGO = 'SECTOR';

    static function getAll() {
        $model = new Model();
        if (!\Cache::has('sectores')){
            $data = $model->get(self::CODIGO);
            \Cache::put('sectores',$data,99999);
        }else{
            $data = \Cache::get('sectores');
        }
        return $data;
    }
}