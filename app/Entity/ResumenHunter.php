<?php

namespace App\Entity;

use App\Model\ResumenHunter as mResumenHunter;
use Jenssegers\Date\Date as Carbon;

class ResumenHunter extends \App\Entity\Base\Entity
{
    static function getJefes($banca, $zonal)
    {
        $jefes = mResumenHunter::getJefes($banca, $zonal);
        return $jefes;
    }
    static function getJefesResumen($jefes)
    {
        $jefes = mResumenHunter::getJefesResumen($jefes);
        return $jefes;
    }
    static function getBEResumenBanca($id_centro)
    {
        $data = mResumenHunter::getBEResumenBanca(self::sgetPeriodoBE(), $id_centro);
        return $data;
    }
    static function getRankingVisitas($id_centro)
    {
        $data = mResumenHunter::getRankingVisitas(self::sgetPeriodoBE(), $id_centro);
        return $data;
    }
    static function getRankingEmpresas($id_centro, $top)
    {
        $data = mResumenHunter::getRankingEmpresas(self::sgetPeriodoBE(), $id_centro, $top);
        return $data;
    }
    static function getRankingVisitasEliminado($id_centro)
    {
        $data = mResumenHunter::getRankingVisitasEliminado(self::sgetPeriodoBE(), $id_centro);
        return $data;
    }
    static function getRankingEmpresasEliminado($id_centro, $top)
    {
        $data = mResumenHunter::getRankingEmpresasEliminado(self::sgetPeriodoBE(), $id_centro, $top);
        return $data;
    }
    static function getFecha()
    {
        $data = mResumenHunter::getFecha();
        return $data;
    }
    static function getCantLeads($id_centro)
    {
        $data = mResumenHunter::getCantLeads(self::sgetPeriodoBE(), $id_centro);
        return $data;
    }
}
