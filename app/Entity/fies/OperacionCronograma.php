<?php

namespace App\Entity\fies;


use App\Model\fies\OperacionCronogramas as OperacionCronogramas;
use App\Entity\Usuario;
use \Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Jenssegers\Date\Date as Carbon;

class OperacionCronograma extends \App\Entity\Base\Entity {
	protected $_fechaRegistro;    
    protected $_codOperacion;
    protected $_tipo;
    protected $_mes;
    protected $_monto;
    protected $_fecha;
    protected $_estatus;   
    protected $_flgUltimo;    



    /*function setCronograma($data){

    	  $this->setValues([
                '_fechaRegistro' => "",//Carbon::createFromFormat('Y-m-d', $data->FECHA_REGISTRO), 
                '_fechaActualizacion' => "",//Carbon::createFromFormat('Y-m-d', $data->FECHA_REGISTRO), 
                '_codOperacion' =>(string) $data->COD_OPERACION,
                '_tipo' => (string)$data->TIPO,
                '_mes' => (string)$data->MES,
                '_monto' => (string)$data->MONTO,
                '_fecha' => (string)$data->FECHA,               
                '_estatus' => (string)$data->ESTATUS,                
        ]);
    }*/

      function setValueToTable() {
        return $this->cleanArray([
                    'FECHA_REGISTRO' => $this->_fechaRegistro,                    
                    'COD_OPERACION' => $this->_codOperacion,
                    'TIPO' =>$this->_tipo,
                    'MES' => $this->_mes,
                    'FECHA' => $this->_fecha,
                    'MONTO' => $this->_monto,
                    'ESTATUS' => $this->_estatus,
                    'FLG_ULTIMO' => $this->_flgUltimo,
        ]);
    }

    static function setCronogramaListar($codOperacion,$tipoCronograma){
    	
        $listaCronograma = new OperacionCronogramas();

    	$listaCronogramaOperacion = $listaCronograma->getCronograma($codOperacion,$tipoCronograma);
    	return $listaCronogramaOperacion;
    }

    function registrarNuevaCuota() {

        $this->_fechaRegistro = Carbon::now();
        //$this->_fechaActualizacion =Carbon::now();
        $this->_flgUltimo = 1;
      
        
        

        $model = new OperacionCronogramas();
        if ($model->setNuevaCuota($this->setValueToTable())){                        
            return true;
        }else{  
            $this->setMessage('Hubo un error al registrar su información. Inténtelo más tarde.');
            return false;
        }
    }

    function setFlag(){
        

        
        $model = new OperacionCronogramas();
        if ($model->setFlagCero($this->setValueToTable())){
            return true;
        }else{  
            $this->setMessage('Hubo un error al registrar su información. Inténtelo más tarde.');
            return false;
        }
    }

    function setCronogramaNuevo(){
        $this->_tipo = "DESEMBOLSO";
     
        $model = new OperacionCronogramas();
        if ($model->setCronogramaNuevo($this->setValueToTable())){
            return true;
        }else{  
            $this->setMessage('Hubo un error al registrar su información. Inténtelo más tarde.');
            return false;
        }
    }
     function updateCronograma(){        
              
        $model = new OperacionCronogramas();         
        if ($model->updateCronograma($this->setValueToTable())){
            return true;
        }else{  
            $this->setMessage('Hubo un error al registrar su información. Inténtelo más tarde.');
            return false;
        }
    }

    static function OperacionCuotaDefault($cuota,$prospecto){
        $cuota->_fechaRegistro = Carbon::now();
        //$this->_fechaActualizacion =Carbon::now();
        $cuota->_flgUltimo = 1;
      
        $model = new OperacionCronogramas();
        if ($model->setCuotaDefault($cuota->setValueToTable(),$prospecto->setValueToTable())){                        
            return true;
        }else{  
            $cuota->setMessage('Hubo un error al registrar su información. Inténtelo más tarde.');
            return false;
        }
    }
}	