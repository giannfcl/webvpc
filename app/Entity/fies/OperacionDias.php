<?php

namespace App\Entity\fies;

use App\Model\fies\OperacionDias as mOperacionDias;
use App\Model\fies\OperacionEstacion as mOperacionesEstaciones;
use App\Entity\Usuario;
use \Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Jenssegers\Date\Date as Carbon;

class OperacionDias extends \App\Entity\Base\Entity {

    protected $_fechaReg;
    protected $_codOperacion;
    protected $_diasCliente;
    protected $_diasFies;
    protected $_diasBanca;
    protected $_diasRiesgos;




    function getOperacionDias($codOperacion)
        {
             $model = new mOperacionesEstaciones();             
            $array= $model->getOperacionDias($codOperacion)->get()->toArray();     
            \Debugbar::info($array);  
          
            return $array;
         }

     function setOperacionDias($codOperacion)
        {
            
            $array=$this->getOperacionDias($codOperacion);
          
            $this->_diasFies=0;
            $this->_diasBanca=0;
            $this->_diasCliente=0;
            $this->_diasRiesgos=0;

                $dif=0;
                $total=COUNT($array);
       
             for ($i = 0; $i < $total-1; $i++) {
                    $dif=0;
              //$array[$i]->FECHA
                    $inicio=Carbon::createFromFormat('Y-m-d',$array[$i]->FECHA );
                    $fin=Carbon::createFromFormat('Y-m-d',$array[$i+1]->FECHA );
                    while ($inicio < $fin) {
                            if( $inicio->isWeekday()){

                                 $dif= $dif+1;
                            }
                            $inicio->addDays(1);
                        }

                        if($array[$i]->ESTATUS=='Revision FIES')   {

                               $this->_diasFies= $this->_diasFies+$dif;
                            } 

                        if($array[$i]->ESTATUS=='En consulta')   {

                               $this->_diasCliente= $this->_diasCliente+$dif;
                            } 

                        if($array[$i]->ESTATUS=='Revisión Banca')   {

                               $this->_diasBanca= $this->_diasBanca+$dif;
                            } 

                        if($array[$i]->ESTATUS=='Fecha Recepcion')   {

                               $this->_diasRiesgos= $this->_diasRiesgos+$dif;
                            } 
                        if($array[$i]->ESTATUS=='Revisión Riesgos')   {

                               $this->_diasRiesgos= $this->_diasRiesgos+$dif;
                            } 
                    }
                     $hoy = Carbon::now()->toDateString();
                     return $this->cleanArray([
                        'FECHA_REGISTRO' => (string)$hoy,
                        'COD_OPERACION' => (string)$codOperacion,
                        'TIEMPO_FIES' => $this->_diasFies,
                        'TIEMPO_CLIENTE' =>  $this->_diasCliente,
                        'TIEMPO_BANCA' =>  $this->_diasBanca,
                        'TIEMPO_RIESGOS'=> $this->_diasRiesgos
                        ]);

         }

    function registrarOperacionesDias($codOperacion)
        {
             $model = new mOperacionDias();             
            
           // \Debugbar::info("hola");  
            if ($model->registrarDiasOperacion($codOperacion,$this->setOperacionDias($codOperacion))){                        
            return true;
            }else{  
            $this->setMessage('Hubo un error al registrar su información. Inténtelo más tarde.');
            return false;
            }
         }

    function getDiasByOperacion($codOperacion)
        {
             $model = new mOperacionDias();      
             $model->getDiasOperacion($codOperacion)->first();     
              \Debugbar::info($model->getDiasOperacion($codOperacion)->first());    
            return $model->getDiasOperacion($codOperacion)->first();
      
            
         }
       

}