<?php

namespace App\Entity\fies;


use App\Model\fies\OperacionProductoEstado as mOperacionesProductoEstado;
use App\Entity\Usuario;
use \Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Jenssegers\Date\Date as Carbon;

class OperacionProductoEstado extends \App\Entity\Base\Entity {


	protected $_fechaReg;
    protected $_codOperacion;
    protected $_producto;
    protected $_regEjecFies;
    protected $_ultimaEstacion;



        function setProperties($data){
        $this->setValues([
                '_fechaReg' => Carbon::createFromFormat('Y-m-d', $data->FECHA_REGISTRO), 
                '_codOperacion' => (string)$data->COD_OPERACION,
                '_producto' =>(string) $data->PRODUCTO,
                '_regEjecFies' => (string)$data->REG_EJECUTIVO_FIES,
                '_ultimaEstacion' => (string)$data->ULTIMO_ESTACION,
                
        ]);

     
         
        }

         function setValueToTable() {
	        return $this->cleanArray([
	                    'FECHA_REGISTRO' => $this->_fechaReg,
	                    'COD_OPERACION' => $this->_codOperacion,
	                    'PRODUCTO' => $this->_producto,
	                    'REG_EJECUTIVO_FIES' => (string) $this->_regEjecFies,
	                    'ULTIMO_ESTACION' => (string) $this->_ultimaEstacion,
	        
	        ]);

    	}


    	function actualizarProducto(){
              
                $mOperacionesProductoEstado	 = new \App\Model\fies\OperacionProductoEstado();
                if ($mOperacionesProductoEstado->actualizarProducto($this->_codOperacion,$this->setValueToTable() )) {
                    $this->setMessage('Producto Actualizado' );
                    return true;
                }else{
                    $this->setMessage('No se registro');
                    return false;
                }
            }

        function actualizaEstacion(){
              
                $mOperacionesProductoEstado  = new \App\Model\fies\OperacionProductoEstado();
                if ($mOperacionesProductoEstado->actualizarProducto($this->_codOperacion,$this->setValueToTable() )) {
                    $this->setMessage('Producto Actualizado' );
                    return true;
                }else{
                    $this->setMessage('No se registro');
                    return false;
                }
            }

        function getProductoEstadoByCodOpe($codOperacion)
         {
             $model = new mOperacionesProductoEstado();

        
                $this->setProperties($model->getProductoEstado($codOperacion)->first());   
                  
         }

}


