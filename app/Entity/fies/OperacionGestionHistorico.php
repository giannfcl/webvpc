<?php

namespace App\Entity\fies;


use App\Model\fies\OperacionEstacion as mOperacionEstacion;
use App\Entity\Usuario;
use \Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Jenssegers\Date\Date as Carbon;

class OperacionGestionHistorico extends \App\Entity\Base\Entity {
	protected $_fecha;
    protected $_user;
    

         function setProperties($data){
        $this->setValues([
                
                '_user' => (string)$data->ID_ESTACION,
                '_fecha' =>(string) $data->COD_OPERACION,
               
               
        ]);
          }


        function setValueToTable() {
        return $this->cleanArray([
                    'FECHA_REGISTRO' => $this->_user,
                    'REGISTRO' => $this->_fecha,
                    
        ]);
    }
}