<?php

namespace App\Entity\fies;


use App\Model\fies\OperacionesProspectos as mOperacionesProspectos;
use App\Entity\Usuario;
use \Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Jenssegers\Date\Date as Carbon;

class OperacionProspecto extends \App\Entity\Base\Entity {

    protected $_fechaReg;
    protected $_codOperacion;
    protected $_codUnico;
    protected $_numdoc;
    protected $_nomCli;
    protected $_grupoEconomico;
    protected $_sector;
    protected $_banca;
    protected $_grupoZonal;
    protected $_segmento;
    protected $_regEjeNegocio;


         function setProperties($data){
        $this->setValues([
                '_fechaReg' => Carbon::createFromFormat('Y-m-d', $data->FECHA_REGISTRO), 
                '_codOperacion' => (string)$data->COD_OPERACION,
                '_codUnico' =>(string) $data->COD_UNICO,
                '_numdoc' => (string)$data->NUM_DOC,
                '_nomCli' => (string)$data->NOMBRE_CLIENTE,
                '_grupoEconomico' =>  (string)$data->NOMBRE_CLIENTE,
                '_sector' => (string)$data->SECTOR,
                '_banca' => (string)$data->BANCA,
                '_grupoZonal' => (string) $data->GRUPO_ZONAL,
                '_segmento' => (string) $data->SEGMENTO,
                '_regEjeNegocio' =>  (string)$data->REG_EJECUTIVO_NEGOCIO,
        ]);
          }


        function setValueToTable() {
        return $this->cleanArray([
                    'FECHA_REGISTRO' => $this->_fechaReg,
                    'COD_OPERACION' => $this->_codOperacion,
                    'COD_UNICO' => $this->_codUnico,
                    'NUM_DOC' => (string) $this->_numdoc,
                    'NOMBRE_CLIENTE' => (string) $this->_nomCli,
                    'GRUPO_ECONOMICO' => (string) $this->_grupoEconomico,
                    'SECTOR' => (string) $this->_sector,
                    'BANCA' => (string) $this->_banca,
                    'GRUPO_ZONAL' => (string) $this->_grupoZonal,
                    'SEGMENTO' => (string) $this->_segmento,
                    'REG_EJECUTIVO_NEGOCIO' => (string) $this->_regEjeNegocio,
    
        ]);
    }


		function actualizarOperacionDatosGenerales(){
   
                // actualizar
                $mOperacionesProspectos = new \App\Model\fies\OperacionesProspectos();
                if ($mOperacionesProspectos->actualizar($this->_codOperacion,$this->setValueToTable() )) {
                    $this->setMessage('Producto Actualizado' );
                    return true;
                }else{
                    $this->setMessage('No se registro');
                    return false;
                }
            }

        function getOperacionGeneralByCodOpe($codOperacion)
         {
             $model = new mOperacionesProspectos();
             
                $this->setProperties($model->getOperacionDatosGenerales($codOperacion)->first());     
         }


        function setProspectos()
         {  
                              
             
              $mOperacionesProspectos = new \App\Model\fies\OperacionesProspectos();
              $codOperacion = $mOperacionesProspectos->registrarOperacion($this->setValueToTable(),$this->_fechaReg );
              return $codOperacion;
              
         }

}