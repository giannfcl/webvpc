<?php

namespace App\Entity\fies;

use App\Model\fies\Operacion as modelOperacion;
use App\Model\fies\OperacionesProspectos as mOperacionProspecto;
use \Illuminate\Pagination\LengthAwarePaginator as Paginator;

use Jenssegers\Date\Date as Carbon;
class Operaciones extends \App\Entity\Base\Entity {

    const ITEMS_PER_PAGE = 25;

    protected $_fechaRegistro;    
    protected $_codOperacion;
    protected $_producto;
    protected $_probabilidad;
    protected $_regEjecutivoFies;
    protected $_UltimoEstacion;
    
    function setValueToTable() {
        return $this->cleanArray([
            'FECHA_REGISTRO' => $this->_fechaRegistro,
            'COD_OPERACION' => $this->_codOperacion,
            'PRODUCTO' => $this->_producto,
            'PROBABILIDAD' => (string) $this->_probabilidad,
            'REG_EJECUTIVO_FIES' => (string) $this->_regEjecutivoFies,
            'ULTIMO_ESTACION' => (string) $this->_UltimoEstacion,
            
        ]);
    }

    static function getOperacion($codOperacion){
        $model = new modelOperacion();
        $lead = $model->consulta($codOperacion)->first();
        return $lead;
    }

    static function getOperacionesEjecutivoFies($ef,$busqueda,$orden){

        $model = new modelOperacion();
        $query = $model->getOperacionEjecutivo(
            $ef, 
            null, //no es necesario especificar el lead porque traes todos
            array_filter($busqueda),
            $orden);

        // Paginación
        $totalCount = $query->count();
        $results = $query
        ->take(self::ITEMS_PER_PAGE)
        ->skip(self::ITEMS_PER_PAGE * ($busqueda['page'] - 1))
        ->get();

        $paginator = new Paginator($results, $totalCount, self::ITEMS_PER_PAGE, $busqueda['page']);

        return $paginator;
    }

    static function getOperacionesFies($ef,$busqueda,$orden){

        $model = new modelOperacion();
        $query = $model->getOperacionEjecutivo(
            null, 
            null, //no es necesario especificar el lead porque traes todos
            array_filter($busqueda),
            $orden);

        // Paginación
        $totalCount = $query->count();
        $results = $query
        ->take(self::ITEMS_PER_PAGE)
        ->skip(self::ITEMS_PER_PAGE * ($busqueda['page'] - 1))
        ->get();

        $paginator = new Paginator($results, $totalCount, self::ITEMS_PER_PAGE, $busqueda['page']);

        return $paginator;
    }
    static function getGrupoZonalByEjecutivo($ejecutivo){
        // FALTA CACHE
        $model = new modelOperacion();
        return $model->getGrupoZonalByEjecutivo($ejecutivo)->get();
    }

    

    static function getRelacionFiesByEjecutivo($ejecutivo){
        // FALTA CACHE
        $model = new modelOperacion();
        return $model->getRelacionFiesByEjecutivo($ejecutivo)->get();
    }

    static function getResumenByEjecutivoFies($ejecutivo,$rol){

        $model = new modelOperacion();
        
        return $model->getResumenByEjecutivoFies($ejecutivo,$rol)->first();
    }
    static function getOperacionDatosGenerales($codOperacion)
    {
        $model = new mOperacionProspecto();
        return $model->getOperacionDatosGenerales($codOperacion)->first();     
    }

    static function getUltimaEstacion($codOperacion)
    {
        $model = new mOperacionProspecto();
        return $model->getUltimaEstacion($codOperacion);     
    }

    
    function setNuevaOperacion()
    {
        $this->_fechaRegistro = Carbon::now();  
        $mOperaciones = new modelOperacion();
        if ($mOperaciones->setNuevaOperacion($this->setValueToTable() )) {
            $this->setMessage('Producto Ingresado' );
            return true;
        }else{
            $this->setMessage('No se registro');
            return false;
        }    
    }

}