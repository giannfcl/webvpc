<?php

namespace App\Entity\fies;

use App\Model\fies\Cliente as Cliente;
use \Illuminate\Pagination\LengthAwarePaginator as Paginator;

class Clientes extends \App\Entity\Base\Entity {

	static function getCliente($ruc){
        $model = new Cliente();
        $cliente = $model->getCliente($ruc)->first();
        return $cliente;
    }
}