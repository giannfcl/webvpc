<?php

namespace App\Entity\fies;


use App\Model\fies\OperacionEstacion as mOperacionEstacion;
use App\Entity\Usuario;
use \Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Jenssegers\Date\Date as Carbon;

class OperacionAtributo extends \App\Entity\Base\Entity {

 	protected $_fechaReg;
    protected $_idEstacion;
    protected $_idGestion;
    protected $_codOperacion;
    protected $_atributo;
    protected $_valor;
    protected $_flgUltimo;
    

         function setProperties($data){
        $this->setValues([
                '_fechaReg' => Carbon::createFromFormat('Y-m-d', $data->FECHA_REGISTRO), 
                '_idEstacion' => (string)$data->ID_ESTACION,
                '_codOperacion' =>(string) $data->COD_OPERACION,
                '_atributo' => (string)$data->ATRIBUTO,
                '_valor' => (string)$data->VALOR,
                '_flgUltimo' =>  (string)$data->FLG_ULTIMO,
               
        ]);
          }


        function setValueToTable() {
        return $this->cleanArray([
                    'FECHA_REGISTRO' => $this->_fechaReg,
                    'ID_ESTACION' => $this->_idEstacion,
                    'COD_OPERACION' => $this->_codOperacion,
                    'ATRIBUTO' => (string) $this->_atributo,
                    'VALOR' => (string) $this->_valor,
                    'FLG_ULTIMO' => (string) $this->_flgUltimo,
                    'ID_GESTION' => (string) $this->_idGestion,
        ]);
    }


     function actualizaFlagUltimo(){

             $model = new mOperacionEstacion();

             $model->actualizaFlagUltimo($this->_codOperacion,$this->_idEstacion);
   
             
            }

    function actualizaEstacion(){

             $model = new mOperacionEstacion();

             $model->actualizaMaxEstacion($this->_codOperacion);
   
             
            }

    function registrarAtributos($codOperacion){

             $model = new mOperacionEstacion();

             $model->guardarAtributo($codOperacion,$this->setValueToTable());
 
             
            }
    
    function registrargestion($fecha,$user){

            $model = new mOperacionEstacion();

             return  $model->registrarHisGestion($fecha,$user);
            }

    function validaFecha($fecha=null,$id_estacion,$codOperacion,$estatus=null,$fechaEstatus=null){

            if(is_null($fecha)){
                return false;

            }

            $model = new mOperacionEstacion();
            $fecha_maxima= Carbon::createFromFormat('Y-m-d',  $model->validaFecha($id_estacion,$codOperacion));
            $fecha=Carbon::createFromFormat('Y-m-d',  $fecha);
            if( !is_null($fechaEstatus) && !is_null($model->validaFechaEstatus($id_estacion+1,$codOperacion)) ){
                $fechaEstatus=Carbon::createFromFormat('Y-m-d',  $fechaEstatus);
                $fecha_maximaEstatus= Carbon::createFromFormat('Y-m-d',  $model->validaFechaEstatus($id_estacion+1,$codOperacion));
                $ultimoEstatus= $model->validaEstatus($id_estacion+1,$codOperacion);

                    if( ($fechaEstatus>=$fecha_maximaEstatus && $ultimoEstatus <> $estatus) ||  ($fechaEstatus==$fecha_maximaEstatus && $ultimoEstatus == $estatus) ){
                        
                            return true;
                        }else{
                          
                          $this->setMessage('La fecha del estatus debe ser mayor o igual a la fecha del estatus anterior');
                                       
                         return false;
                        }


                }
            \Debugbar::info($fecha);
            \Debugbar::info($fechaEstatus);

            if(!is_null($fechaEstatus))
                 {  if($fechaEstatus>=$fecha || $fechaEstatus=$fecha ){
                       
                        return true;
                    }else{
                      $this->setMessage('La fecha del estatus debe ser mayor o igual a la fecha de la estacion');
                      
                     return false;
                    }
                } 

            if($fecha>=$fecha_maxima || $fecha_maxima=$fecha  ){
               
                return true;
            }else{
              $this->setMessage('No se guardo.La fecha debe ser mayor  o iguala la fecha de la estacion anterior');
              
             return false;
            }


    }
}