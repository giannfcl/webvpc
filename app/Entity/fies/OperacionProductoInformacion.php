<?php

namespace App\Entity\fies;


use App\Model\fies\OperacionInformacion as OperacionInformacion;
use App\Entity\Usuario;
use \Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Jenssegers\Date\Date as Carbon;

class OperacionProductoInformacion extends \App\Entity\Base\Entity {
	protected $_fechaRegistro;    
    protected $_codOperacion;
    protected $_tipo;
    protected $_moneda;
    protected $_monto;
    protected $_mesProbable;
    protected $_flgUltimo; 


      function setValueToTable() {
        return $this->cleanArray([
                    'FECHA_REGISTRO' => $this->_fechaRegistro,                    
                    'COD_OPERACION' => $this->_codOperacion,
                    'TIPO' =>$this->_tipo,
                    'MONEDA' => $this->_moneda,
                    'MONTO' => $this->_monto,
                    'MES_PROBABLE' => $this->_mesProbable,
                    'FLG_ULTIMO' => $this->_flgUltimo,
        ]);
    }



    static function getOperacionProducto($codOperacion,$tipo){
         $productoInformacion = new OperacionInformacion();
    	 $resultadoProductoInformacion = $productoInformacion->getoOperacionDesembolso($codOperacion,$tipo);
    	return $resultadoProductoInformacion;
    }


    function actualizarOperacionProducto(){

    	$this->_fechaRegistro = Carbon::now();
        $this->_flgUltimo = 1;

    	$model = new OperacionInformacion();
        if ($model->updateOperacion($this->setValueToTable())){                        
            return true;
        }else{  
            $this->setMessage('Hubo un error al registrar su información. Inténtelo más tarde.');
            return false;
        }
    }

    function registrarNuevoInfoProducto(){
        $this->_fechaRegistro = Carbon::now();
        $this->_flgUltimo = 1;

       $model = new OperacionInformacion();
        if ($model->registrarNuevoInfoProducto($this->setValueToTable())){                        
            return true;
        }else{  
            $this->setMessage('Hubo un error al registrar su información. Inténtelo más tarde.');
            return false;
        }
    }

}