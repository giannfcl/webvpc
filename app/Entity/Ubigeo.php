<?php

namespace App\Entity;

use App\Model\Ubigeo as mUbigeo;
use App\Model\Catalogo as mCatalogo;


class Ubigeo extends \App\Entity\Base\Entity {

    CONST U_DEPARTAMENTO = "U_DEPARTAMENTO";
    CONST U_PROVINCIA = "U_PROVINCIA";
    CONST U_DISTRITO = "U_DISTRITO";
    CONST U_COMBO_DEPARTAMENTO = "U_COMBO_DEPARTAMENTO";
    CONST U_COMBO_PROVINCIA = "U_COMBO_PROVINCIA";
    CONST U_COMBO_DISTRITO = "U_COMBO_DISTRITO";
    CONST U_TIENDAS_UBIGEO = "U_TIENDAS_UBIGEO";

    static function getDepartamento(){
        //FALTA CACHE
        $model = new mUbigeo();
        if (!\Cache::has(self::U_DEPARTAMENTO))
        {
            $data = $model->getDepartamento()->get();
            \Cache::put(self::U_DEPARTAMENTO,$data,99999);
        }
        else
        {
            $data = \Cache::get(self::U_DEPARTAMENTO);
        }
        return $data;
    }

    static function getProvincia($departamento = null){
        //FALTA CACHE
        $model = new mUbigeo();
        if (!\Cache::has(self::U_PROVINCIA))
        {
            $data = $model->getProvincia($departamento)->get();
            \Cache::put(self::U_PROVINCIA,$data,99999);
        }
        else
        {
            $data = \Cache::get(self::U_PROVINCIA);
        }
        return $data;
    }

    static function getDistrito($provincia = null, $departamento = null){
        //FALTA CACHE
        $model = new mUbigeo();
        if (!\Cache::has(self::U_DISTRITO))
        {
            $data = $model->getDistrito($provincia,$departamento)->get();
            \Cache::put(self::U_DISTRITO,$data,99999);
        }
        else
        {
            $data = \Cache::get(self::U_DISTRITO);
        }
        return $data;
    }

    //Métodos que devuelven el código y descripción para los combos Departamento,Provincia,Distrito
    static function getComboDepartamento(){
        $model = new mUbigeo();
        if (!\Cache::has(self::U_COMBO_DEPARTAMENTO))
        {
            $data = $model->getComboDepartamento()->get();
            \Cache::put(self::U_COMBO_DEPARTAMENTO,$data,99999);
        }
        else
        {
            $data = \Cache::get(self::U_COMBO_DEPARTAMENTO);
        }
        return $data;
    }

    static function getComboProvincia($departamento = null){
        $model = new mUbigeo();
        if (!\Cache::has(self::U_COMBO_PROVINCIA."-".$departamento))
        {
            $data = $model->getComboProvincia($departamento)->get();
            \Cache::put(self::U_COMBO_PROVINCIA."-".$departamento,$data,99999);
        }
        else
        {
            $data = \Cache::get(self::U_COMBO_PROVINCIA."-".$departamento);
        }
        return $data;
    }

    static function getComboDistrito($provincia = null, $departamento = null){
        $model = new mUbigeo();
        if (!\Cache::has(self::U_COMBO_DISTRITO."-".$departamento."-".$provincia))
        {
            $data = $model->getComboDistrito($provincia,$departamento)->get();
            \Cache::put(self::U_COMBO_DISTRITO."-".$departamento."-".$provincia,$data,99999);
        }
        else
        {
            $data = \Cache::get(self::U_COMBO_DISTRITO."-".$departamento."-".$provincia);
        }
        return $data;
    }

    static function getTiendasByUbigeo($distrito = null, $provincia = null, $departamento = null){
        $model = new mCatalogo();
        if (!\Cache::has(self::U_TIENDAS_UBIGEO."-".$distrito."-".$provincia."-".$departamento)){
            $data = $model->get('TIENDA',$departamento.$provincia.$distrito);
            \Cache::put(self::U_TIENDAS_UBIGEO."-".$distrito."-".$provincia."-".$departamento,$data,99999);
        }
        else{
            $data = \Cache::get(self::U_TIENDAS_UBIGEO."-".$distrito."-".$provincia."-".$departamento);
        }
        return $data;
    }
}
