<?php

namespace App\Entity;

use Jenssegers\Date\Date as Carbon;
use App\Model\AddContacto as mAddContacto;

class AddContacto extends \App\Entity\Base\Entity {

    protected $_periodo;
    protected $_fechaRegistro;
    protected $_ejecutivo;
    protected $_lead;
    protected $_tipoContacto;
    protected $_valor;

    const TREGISTRO_TELEFONO_TEXTO = 'TELEFONO';
    const TREGISTRO_DIRECCION_TEXTO = 'DIRECCION';
    const TREGISTRO_EMAIL_TEXTO = 'EMAIL';

    function setValueToTable() {
        return $this->cleanArray([
                    'PERIODO' => $this->_periodo,
                    'NUM_DOC' => $this->_lead,
                    'REGISTRO_EN' => $this->_ejecutivo,
                    'FECHA_REGISTRO' => ($this->_fechaRegistro)? $this->_fechaRegistro->toDateTimeString() : null,
                    'TIPO_CONTACTO' => $this->_tipoContacto,
                    'VALOR' => $this->_valor,
        ]);
    }

    function registrar() {
        $this->_fechaRegistro = Carbon::now();
        $this->_periodo = $this->getPeriodo();

        $model = new mAddContacto();
        if ($model->nuevo($this->setValueToTable())){
            return true;
        }else{
            $this->setMessage('Hubo un error al registrar su información. Inténtelo más tarde.');
            return false;
        }
    }

    function eliminar() {
        $model = new mFeedback();
        $this->_periodo = $this->getPeriodo();
        if ($model->eliminar($this->setValueToTable())){
            return true;
        }else{
            $this->setMessage('Hubo un error al procesar la información. Inténtelo más tarde.');
            return false;
        }
    }

    static function getContactos($lead){
        /* FALTA CACHE */
        $model = new mAddContacto();
        return $model->listar(self::sgetPeriodo(),$lead)->get();
    }
}