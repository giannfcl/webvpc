<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entity\Infinity\NotificacionInfinity as nInfinity;

use Validator;

class NotificacionesController extends Controller
{

    public function procesoInfinity(Request $request){
        $filtros=[
            'zonal'=>'BEL ZONAL 2',
            'flgInfinity'=>1,
        ];

        $notificacionInfinity=new nInfinity();      
        $notificacionInfinity->notificar($filtros);
        
        return back();
    }
}
