<?php

namespace App\Http\Controllers;

use App\Entity\Usuario as Usuario;

class ToolsController extends Controller {


    public function updateMasive() {
        $usuario = new Usuario();
        return response()->json($usuario->updateMasive());
    }
}
