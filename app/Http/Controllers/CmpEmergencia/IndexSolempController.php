<?php

namespace App\Http\Controllers\CmpEmergencia;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables as Datatables;
use Illuminate\Support\Facades\Auth;
use App\Entity\Usuario;
use App\Entity\cmpemergencia\Priorizacion as Priorizacion;
use App\Entity\cmpemergencia\Lead as Lead;
use Validator;

class IndexSolempController extends Controller {

	public function index(Request $request){
		return view('cmpemergencia.index-solemp');
	}

	public function listar(Request $request){
		//dd($request->all());
		$leads = Lead::listarSolEmp($this->user,$request->all());
		return Datatables::of($leads)->make(true);
	}
	
	public function priorizar(Request $request){
		$entidad = new Priorizacion();
		if ($entidad->agregar(
			$request->get('ruc'),
			$this->user,
			$request->all()
		)){
        	return response()->json(['status' => 'ok']);
		}else{
			return response()->json(['status' => 'error','message'=> $entidad->getMessage()]);
		}
		
		return response()->json(['status' => 'error','message'=> 'Error en ingreso de datos']);
	}

}