<?php

namespace App\Http\Controllers\CmpEmergencia;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables as Datatables;
use Illuminate\Support\Facades\Auth;
use App\Entity\Usuario;
use App\Entity\cmpemergencia\Etapa as Etapa;
use App\Entity\cmpemergencia\Lead as Lead;
use App\Entity\Tienda;
use SebastianBergmann\Environment\Console;
use Jenssegers\Date\Date as Carbon;
use App\Entity\Banco as Banco;
use Validator;
use PDF;

class IndexBackofficeController extends Controller {

	public function index(Request $request){

		$etapa = $request->get('etapa',null);

		return view('\cmpemergencia\index-backoffice')
			->with('request',$request->all())
			->with('gestiones',\App\Entity\cmpemergencia\Gestion::getAllBpe())
			->with('campanhas',\App\Entity\cmpemergencia\Campanha::getAll())
			->with('esejecutivo',in_array($this->user->getValue('_rol'), [Usuario::ROL_EJECUTIVO_NEGOCIO]) ? 1 : 0)
			->with('editarrll',in_array($this->user->getValue('_rol'), [Usuario::ROL_EJECUTIVO_NEGOCIO,Usuario::ROL_TELEVENTAS]) ? 1 : 0)
			->with('zonales',Tienda::getZonales())
			->with('sectores',\App\Entity\Sector::getAll())
			->with('giros',\App\Entity\Giro::getAll())
			->with('estadoCivil',\App\Entity\CatalogoMaestro::getCatalogo(null,'ESTADO-CIVIL','estadoCivil'))
			->with('tipoSociedad',\App\Entity\CatalogoMaestro::getCatalogo(null,'TIPO-SOCIEDAD','tipoSociedad'))
			->with('tipoVia',\App\Entity\CatalogoMaestro::getCatalogo(null,'TIPO-VIA','tipoVia'))
			->with('departamentos',\App\Entity\Ubigeo::getComboDepartamento())
			->with('provincias',\App\Entity\Ubigeo::getComboProvincia())
			->with('distritos',\App\Entity\Ubigeo::getComboDistrito())
			->with('bancos',\App\Entity\Banco::getBancosReactiva());

	}

	public function listar(Request $request){
		// dd($request->all());
		$leads = Lead::listar($request->all(),\App\Entity\Banca::BPE,$this->user,true); //etapa
        return Datatables::of($leads)->make(true); //$lista
	}

	public function actualizar(request $request){
    	$lead = new lead(); 
    	if($lead->gestionarAsignacion($request->get('ruc'), $request->get('condicion'),$this->user)){
    		flash('El cliente fue asignado correctamente')->success();
    		
		}else{
			flash('El cliente no fue asignado correctamente')->error();
    	}
	}
   	
    

	
}