<?php

namespace App\Http\Controllers\CmpEmergencia;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables as Datatables;
use Illuminate\Support\Facades\Auth;
use App\Entity\Usuario;
use App\Entity\cmpemergencia\Etapa as Etapa;
use App\Entity\cmpemergencia\GestionFile as GestionFile;
use App\Entity\cmpemergencia\Lead as Lead;
use App\Entity\cmpemergencia\Gestion as Gestion;
use App\Entity\cmpemergencia\VistasDocsDesembolso as VistasDocsDesembolso;
use App\Entity\BE\ZonalJefatura;
use App\Entity\Banca;
use Validator;

class ValidacionController extends Controller {

	public function index(Request $request){
		$vista = 'cmpEmergencia.validacion-documentos';
		return view($vista)
			->with('menu',$this->getMenu())
			->with('gestionesfiles',GestionFile::getGestionFileBE())
			->with('vistasdocs',VistasDocsDesembolso::getNombresVistas())
			->with('gestiones',Gestion::getAllValidacion())
			->with('zonales',ZonalJefatura::getZonales())
			->with('bancas',Banca::getAll())
			->with('ejecutivos',Usuario::getFarmersHunters());
	}

	private function getMenu(){
		$etapas = Etapa::getByEtapaValidacionDocumento();
		return view('cmpEmergencia.etapas-fincorp')
			->with('etapas',$etapas);
	}

	public function listar(Request $request){
		\Log::info($request->get('banca'));
		\Log::info($request->get('zonal'));
		\Log::info($request->get('ejecutivo'));
		$leads = Lead::listarValidacion(
			$this->user,
			$request->get('banca',null),
			$request->get('zonal',null),
			$request->get('ejecutivo',null)
		); //etapa
		return Datatables::of($leads)->make(true); //$lista
	}

	public function buscar(Request $request){
		$entidad = new Lead();
		return response()->json($entidad->getByRuc($request->get('numruc',null)));
	}
	
	public function gestionar(Request $request){

		$lead = new Lead();

		if ($lead->getByRuc($request->get('numruc'))){
			if ($lead->gestionarValidacionDocumentos(
				$this->user,
				(int)$request->get('gestion'),
			)){
				flash('El lead fue gestionado correctamente')->success();
            	return redirect()->route('cmpEmergencia.validacion.index');
			}else{
				flash('error')->error($lead->getMessage());
				return redirect()->route('cmpEmergencia.validacion.index');
			}
		}else{
			flash('error')->error('El lead que has seleccionado no esta disponible');
			return redirect()->route('cmpEmergencia.validacion.index');
		}
	}

}