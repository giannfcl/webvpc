<?php

namespace App\Http\Controllers\CmpEmergencia;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables as Datatables;
use Illuminate\Support\Facades\Auth;
use App\Entity\Usuario;
use App\Entity\cmpemergencia\Etapa as Etapa;
use App\Entity\cmpemergencia\Lead as Lead;
use App\Entity\cmpemergencia\Gestion as Gestion;
use Validator;
use App\Entity\cmpemergencia\PlazoGracia as PlazoGracia;
use App\Entity\cmpemergencia\GestionSubasta as GestionSubasta;
use App\Entity\cmpemergencia\GestionFile as GestionFile;
use App\Entity\cmpemergencia\VistasDocsDesembolso as VistasDocsDesembolso;

class IndexController extends Controller {

	public function index(Request $request){
		$etapa = $request->get('etapa',null);
		$vista = 'CmpEmergencia/';
		switch ($etapa) {
			case Etapa::ASIGNADO:
				$vista .= 'asignado';
				break;
			case Etapa::CONTACTADO:
				$vista .= 'contactado';
				break;
			case Etapa::INTERESADO:
				$vista .= 'interesado';
				break;
			case Etapa::DOCUMENTOS_EVALUACION:
				$vista .= 'evaluacion_riesgo';
				break;
			case Etapa::DOCUMENTOS_DESEMBOLSO:
				$vista .= 'docdesembolso';
				break;
			case Etapa::DERIVADO_GTP:
				$vista .= 'derivado';
				break;
			case Etapa::DESEMBOLSADO:
				$vista .= 'desembolsado';
				break;
			case Etapa::RECHAZADO:
				$vista .= 'rechazado';
				break;
			case Etapa::REVISAR:
				$vista .= 'por-revisar';
				break;
			case Etapa::ENVIO_SUBASTA:
				$vista .= 'envio_subasta';
				break;
			case Etapa::WIO_INGRESADO:
				$vista .= 'wio_ingresado';
				break;
			case Etapa::WIO_COFIDE:
				$vista .= 'wio_cofide';
				break;
			case Etapa::ENVIADO_COFIDE:
				$vista .= 'enviado_cofide';
				break;
			default:
			return redirect()->route('cmpEmergencia.index', ['etapa' => Etapa::ASIGNADO]);
		}
		
		return view($vista)
			->with('menu',$this->getMenu($this->user))
			->with('gestiones',Gestion::getAllBe())
			->with('etapas',Etapa::getByEtapa($this->user))
			->with('bancos',\App\Entity\Banco::getBancosReactiva())
			->with('request',$request->all())
			->with('registro',$this->user->getValue('_registro'))
			->with('plazosgracias',PlazoGracia::getPlazosGraciasBE())
			->with('gestionessubasta',GestionSubasta::GestionSubastaBE())
			->with('ratioRiesgos',$this->user->getValue('_banca') == 'BE'? 1.05:1)
			->with('gestionesfiles',GestionFile::getGestionFileBE())
			->with('vistasdocs',VistasDocsDesembolso::getNombresVistas());
	}

	
	public function listar(Request $request){
		// dd($request->all());
		$leads = Lead::listar($request->all(),$this->user->getValue('_banca'), $this->user); //etapa
		return Datatables::of($leads)->make(true); //$lista
	}

	private function getMenu($ejecutivo){
		$etapas = Etapa::getByEtapa($ejecutivo);
		return view('cmpEmergencia.etapas')
				->with('etapas',$etapas);
	}

	public function nuevoasignado(Request $request)
	{
		$entidad = new Lead();
		if($entidad->asignar($request->all(),$this->user->getValue('_registro'))){
			flash($entidad->getMessage())->success();
		}else{
			flash($entidad->getMessage())->error();
		}
		return redirect()->route('cmpEmergencia.index', ['etapa' => Etapa::ASIGNADO]);
	}

	public function gestionsubasta(Request $request)
	{
		$entidad = new Lead();
		if($entidad->gestionsubasta($request->all(),$this->user->getValue('_registro'))){
			flash($entidad->getMessage())->success();
		}else{
			flash($entidad->getMessage())->error();
		}
		return redirect()->route('cmpEmergencia.index', ['etapa' => Etapa::ENVIO_SUBASTA]);
	}

	public function buscarinfo(Request $request)
	{
		$entidad = new Lead();
		return response()->json($entidad->getByRuc($request->get('numruc',null))==null?false:$entidad->getByRuc($request->get('numruc',null)));
	}

	public function gestionar(Request $request)
	{
		$entidad = new Gestion();
		if($entidad->gestionar($request->all(),$this->user->getValue('_registro'))){
			flash($entidad->getMessage())->success();
		}else{
			flash($entidad->getMessage())->error();
		}
		return redirect()->route('cmpEmergencia.index', ['etapa' => $request->get('etapa_actual')==null?Etapa::ASIGNADO:$request->get('etapa_actual')]);
	}

	public function validarInteresado(Request $request){

  		$lead = new Lead();	
  		if ($lead->getByRuc($request->get('numruc'))){
			if ($lead->enviarInteresado(
				$this->user,
				$request->all()
			)){
				flash($lead->getMessage())->success();
			}else{
				flash($lead->getMessage())->error();
			}
		}else{
			flash('error')->error('El lead que has seleccionado no esta disponible');
		}
		return redirect()->route('cmpEmergencia.index', ['etapa' => Etapa::INTERESADO]);
	}

	public function documentosCompletos(Request $request){
		$lead = new Lead();
		
		if ($lead->getByRuc($request->get('numruc'))){
			if ($lead->documentacionCompletaBE (
				(int)$request->get('accion',0),
				$this->user,
				//(int)$request->get('flgcompleto'),
				(int)$request->get('flgdjpdt'),
				(int)$request->get('flgessalud'),
				$request->get('volventa'),
				$request->get('essalud'),
				$request->get('soloferta'),
				$request->get('cemail'),
				(int)$request->get('flgConfirmar',0),
				$request->get('tasatentativa',0),
				$request->get('fechadesempeniotentativa',null),
				$request->all()
			)){
				flash($lead->getMessage())->success();
			}else{
				flash($lead->getMessage())->error();
			}
		}else{
			flash('error')->error('El lead que has seleccionado no esta disponible');
		}
		return redirect()->route('cmpEmergencia.index', ['etapa' => Etapa::DOCUMENTOS_EVALUACION]);
	}

	public function enviarValDocumentacion(Request $request){
		$lead = new Lead();
		if ($lead->getByRuc($request->get('numruc'))){
			if ($lead->enviarValDocumentacion(
				$this->user
				,(int) $request->get('flgcompleto',0)
				,(int) $request->get('flgenvrevision',0)
				,$request->all()
			)){
				flash($lead->getMessage())->success();
			}else{
				flash($lead->getMessage())->error();
			}
		}else{
			flash('error')->error('El lead que has seleccionado no esta disponible');
		}
		return redirect()->route('cmpEmergencia.index', ['etapa' => Etapa::DOCUMENTOS_DESEMBOLSO]);
	}

	public function wioparacofide(Request $request)
	{
		$lead = new Lead();
		if ($lead->getByRuc($request->get('numruc'))){
			if ($lead->wioparacofide(
				$this->user
				,$request->all()
			)){
				flash($lead->getMessage())->success();
			}else{
				flash($lead->getMessage())->error();
			}
		}else{
			flash('error')->error('Error comunicate con Soporte');
		}
		return redirect()->route('cmpEmergencia.index', ['etapa' => Etapa::WIO_COFIDE]);
	}
}