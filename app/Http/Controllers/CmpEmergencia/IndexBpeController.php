<?php

namespace App\Http\Controllers\CmpEmergencia;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables as Datatables;
use Illuminate\Support\Facades\Auth;
use App\Entity\Usuario;
use App\Entity\cmpemergencia\Etapa as Etapa;
use App\Entity\cmpemergencia\Lead as Lead;
use App\Entity\Tienda;
use SebastianBergmann\Environment\Console;
use Jenssegers\Date\Date as Carbon;
use App\Entity\Banco as Banco;
use App\Entity\cmpemergencia\CobroSimple as CobroSimple;
use Validator;
use PDF;

class IndexBpeController extends Controller {

	public function index(Request $request){

		$etapa = $request->get('etapa',null);

		return view('\cmpemergencia\index-bpe')
			->with('request',$request->all())
			->with('gestiones',\App\Entity\cmpemergencia\Gestion::getAllBpe())
			->with('griesgos',\App\Entity\cmpemergencia\Gestion::getAllRiegos())
			->with('campanhas',\App\Entity\cmpemergencia\Campanha::getAll())
			->with('esejecutivo',in_array($this->user->getValue('_rol'), [Usuario::ROL_EJECUTIVO_NEGOCIO]) ? 1 : 0)
			->with('editarrll',in_array($this->user->getValue('_rol'), [Usuario::ROL_EJECUTIVO_NEGOCIO,Usuario::ROL_TELEVENTAS,Usuario::ROL_CANALES_DISTRIBUCION]) ? 1 : 0)
			->with('rol',$this->user->getValue('_rol'))
			->with('zonales',Tienda::getZonales())
			->with('sectores',\App\Entity\Sector::getAll())
			->with('giros',\App\Entity\Giro::getAll())
			->with('estadoCivil',\App\Entity\CatalogoMaestro::getCatalogo(null,'ESTADO-CIVIL','estadoCivil'))
			->with('tipoSociedad',\App\Entity\CatalogoMaestro::getCatalogo(null,'TIPO-SOCIEDAD','tipoSociedad'))
			->with('tipoVia',\App\Entity\CatalogoMaestro::getCatalogo(null,'TIPO-VIA','tipoVia'))
			->with('departamentos',\App\Entity\Ubigeo::getComboDepartamento())
			->with('provincias',\App\Entity\Ubigeo::getComboProvincia())
			->with('distritos',\App\Entity\Ubigeo::getComboDistrito())
			->with('bancos',\App\Entity\Banco::getBancosReactiva())
			->with('tipoEnvioGtp',[1 => 'Digital',2=> 'Físico'])
			->with('clientecanales',['REGULAR','CARTERA','CALL','CORPORATIVO'])
			->with('prioridades',Lead::fechaPrimerContacto);;

	}

	public function listar(Request $request){
		// dd($request->all());
		$leads = Lead::listar($request->all(),\App\Entity\Banca::BPE,$this->user); //etapa
		return Datatables::of($leads)->make(true); //$lista

	}


	public function indexproducto(Request $request){

		$etapa = $request->get('etapa',null);

		return view('\cmpemergencia\indexbpe-producto')
			->with('request',$request->all())
			->with('rol',$this->user->getValue('_rol'))
			->with('rol',$this->user->getValue('_rol'))
			->with('combocantidadclientes',CobroSimple::getComboCantClientes())
			->with('combocontrolcobranza',CobroSimple::getComboControlCobranza())
			->with('comboticketpromedio',CobroSimple::getComboTicketPromedio())
			->with('tipoSociedad',\App\Entity\CatalogoMaestro::getCatalogo(null,'TIPO-SOCIEDAD','tipoSociedad'));

	}

	public function listarproducto(Request $request){
		// dd($request->all());
		$leads = Lead::listar($request->all(),\App\Entity\Banca::BPE,$this->user); //etapa
		return Datatables::of($leads)->make(true); //$lista

	}

	public function getByRuc(Request $request){
		$lead = new Lead();
		$result = $lead->getByRuc($request->get('ruc'));
		return response()->json($result);
	}

	public function gestionar(Request $request){
		$lead = new Lead();

		if ($lead->getByRuc($request->get('numruc')))
		{
			if ($lead->gestionarBPE(
				$this->user,
				$request->all(),
				$request->get('gestion'),
				$request->get('telefono',null),
				$request->get('email',null),
				$request->get('direccion',null),
				$request->get('ventas')?(int)str_replace(',','',$request->get('ventas')):null,
				$request->get('essalud')?(int)str_replace(',','',$request->get('essalud')):null,
				$request->get('solicitudMonto')?(int)str_replace(',','',$request->get('solicitudMonto')):null,
				null,
				$request->get('comentario',null),
				$request->get('solicitudGracia',null),
				$request->get('empleados',null),
				$request->get('cuentaTipo',null),
				$request->get('cuentaNumero',null),
				$request->get('sector',null),
				$request->get('giro',null),
				$request->get('flgReactiva1',null),
				$request->get('montoReactiva1')?(int)str_replace(',','',$request->get('montoReactiva1')):null,
				$request->get('solicitudCobertura')?(int)str_replace('%','',$request->get('solicitudCobertura')):null
			)){
				flash('El lead fue gestionado correctamente')->success();
            	return redirect()->route('cmpEmergencia.bpe.index');
			}else{
				flash($lead->getMessage())->error();
				return redirect()->route('cmpEmergencia.bpe.index');
			}
		}else{
			flash('El lead que has seleccionado no esta disponible')->error();
			return redirect()->route('cmpEmergencia.bpe.index');
		}
	}

	public function getTasas(Request $request){
		$lead = new Lead();

		$lead->getByRuc($request->get('ruc'));

		if ($tasas = $lead->getTasas(
			$this->user,
			$request->get('ruc',null),
			$request->get('monto')?(int)str_replace(',','',$request->get('monto')):null,
			$request->get('cobertura')?(int)str_replace('%','',$request->get('cobertura')):null
		)){
			return response()->json(['status'=>'ok', 'tasas' => [$tasas]]);
		}else{
			return response()->json(['status'=>'error', 'msg' => 'No hay subastas disponibles para cubrir el requerimiento']);
		}

	}

	public function enviarGtp(Request $request){
		$lead = new Lead();

		if ($lead->getByRuc($request->get('numruc'))){
			if ($lead->envioGTpBpe(
				$this->user,
				(int)$request->get('gestionGtp',0),
				(int)str_replace(',','',$request->get('solicitudMonto'))
			)){
				flash('El lead fue gestionado correctamente')->success();
            	return redirect()->route('cmpEmergencia.bpe.index');
			}else{
				flash($lead->getMessage())->error();
				return redirect()->route('cmpEmergencia.bpe.index');
			}
		}else{
			flash('El lead que has seleccionado no está disponible')->error();
			return redirect()->route('cmpEmergencia.bpe.index');
		}
	}

	public function acuerdocreditopdf(Request $request)
	{
		$entidad = new Lead();
		$usuario = new Usuario();
		$banco = new Banco();

		$rgistro = Auth::user()->REGISTRO;
		// $cargo = $usuario->getCargo($rgistro);
		$ruc = $request->get('ruc',null);
		$datos = $entidad->acuerdos($ruc);
		$direccion = $entidad->direccion($ruc);
		$bancos = $banco->getBancosReactiva();
		$fecha= Carbon::now();
		$fechaStr= $fecha->toDateString();
		$fechaAR = explode("-",$fechaStr);
		$fecha =$fechaAR[2].' de '.ucwords($fecha->format('F')).' de '.$fechaAR[0];
		$pdf = PDF::loadView('cmpEmergencia.acuerdocredito',compact('datos','fecha','bancos','direccion'));
        return $pdf->download('Acuerdo-Crédito.pdf');
	}
	public function pagarepdf(Request $request)
	{
		$entidad = new Lead();
		$ruc = $request->get('ruc',null);
		$acuerdos = $entidad->acuerdos($ruc);
		$direccion = $entidad->direccion($ruc);
		$pdf = PDF::loadView('cmpEmergencia.pagare',compact('acuerdos','direccion'));
        return $pdf->download('Pagaré.pdf');
	}

	public function llenadopagarepdf(Request $request)
	{
		$entidad = new Lead();
		$ruc = $request->get('ruc',null);
		$datos = $entidad->datosLlenado($ruc);
		$direccion = $entidad->direccion($ruc);
		$pdf = PDF::loadView('cmpEmergencia.llenadopagare',compact('datos','direccion'));
        return $pdf->download('Llenado.pdf');
	}

	public function cartapresentacionpdf(Request $request)
	{
		$entidad = new Lead();
		$ruc = $request->get('ruc',null);
		$acuerdos = $entidad->acuerdos($ruc);
		$fecha= Carbon::now();
		$fechaStr= $fecha->toDateString();
		$fechaAR = explode("-",$fechaStr);
		$fecha =$fechaAR[2].' de '.ucwords($fecha->format('F')).' de '.$fechaAR[0];
		$pdf = PDF::loadView('cmpEmergencia.cartapresentacion',compact('acuerdos','fecha'));
        return $pdf->download('CartaPresentacion.pdf');
	}

	public function pdfTotal(Request $request){

		$entidad = new Lead();
		$datos = $entidad->datosLlenado($request->get('ruc',null));
		$fecha= Carbon::now();
		$fechaStr= $fecha->toDateString();
		$fechaAR = explode("-",$fechaStr);
		$fecha =$fechaAR[2].' de '.ucwords($fecha->format('F')).' de '.$fechaAR[0];

		$view1 = view('cmpEmergencia.acuerdocredito')->with('datos',$datos)->with('fecha',$fecha);
		$view2 = view('cmpEmergencia.pagare')->with('')->with('acuerdos',$datos);
		$view3 = view('cmpEmergencia.llenadopagare')->with('datos',$datos);
		$view4 = view('cmpEmergencia.cartapresentacion')->with('acuerdos',$datos)->with('fecha',$fecha);

		return $view1.$view2.$view3.$view4;
	}
}
