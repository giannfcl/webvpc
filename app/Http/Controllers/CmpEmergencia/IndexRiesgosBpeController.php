<?php

namespace App\Http\Controllers\CmpEmergencia;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables as Datatables;
use Illuminate\Support\Facades\Auth;
use App\Entity\Usuario;
use App\Entity\cmpemergencia\Etapa as Etapa;
use App\Entity\cmpemergencia\Lead as Lead;
use App\Entity\cmpemergencia\Gestion as Gestion;
use App\Entity\Tienda;
use Validator;
use App\Model\cmpEmergencia\Lead as mLead;

class IndexRiesgosBPEController extends Controller {

	public function index(Request $request){
		return view('\cmpemergencia\riesgos')
		->with('gestiones',Gestion::getAllRiegos())
		->with('subgestiones',Gestion::getSubRiesgosObservados())
		->with('bancos',\App\Entity\Banco::getBancosReactiva());
	}

	public function listar(Request $request){
		$leads = Lead::listarRiesgos($this->user,$request->get('estado',null)); //etapa
		return Datatables::of($leads)->make(true); //$lista
	}

	public function buscar(Request $request){
		$lead = new Lead();
		return response()->json($lead->getByRiesgos($this->user,$request->get('ruc')));
	}
	
	public function gestionar(Request $request){
		$lead = new Lead();
		$model = new mLead();
		if ($lead->getByRiesgos($this->user,$request->get('numruc'))){
			if ($lead->gestionarRiesgos(
				$this->user,
				$request->get('gestion'),
				$request->get('subgestion'),
				$request->get('comentario'),
				$request->get('mtolimite',null)?(int)str_replace(',','',$request->get('mtolimite')):null,
				
			)) 
			{
				flash('El lead fue gestionado correctamente')->success();
            	return redirect()->route('cmpEmergencia.riesgos.bpe.index');
			}else{
				flash($lead->getMessage())->error();
				return redirect()->route('cmpEmergencia.riesgos.bpe.index');
			}
		}else{
			flash('El lead que has seleccionado no esta disponible')->error();
			return redirect()->route('cmpEmergencia.riesgos.bpe.index');
		}
	}

}