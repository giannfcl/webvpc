<?php

namespace App\Http\Controllers\BE;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entity\BE\Lead as Lead;
use App\Entity\BE\Contacto as Contacto;
use App\Entity\BE\AccionComercial as AccionComercial;
use App\Entity\BE\ContactoEncuesta as ContactoEncuesta;
use App\Entity\BE\AddContacto as AddContacto;
use App\Entity\Ubigeo as Ubigeo;
use Validator;
use Jenssegers\Date\Date as Carbon;

class MiContactoController extends Controller
{

    public function index(Request $request)
    {
        $busqueda = [
            'documento' => $request->get('documento', null),
        ];

        if ($busqueda['documento']) {
            $lead = Lead::getMiContacto($busqueda['documento'], $this->user);

            $cliente = AccionComercial::getMiContacto($busqueda['documento'], $this->user);
            $contactos = [];
            $productos = [];
            $addContactos = [];

            if ($lead or $cliente) {
                $contactos = Contacto::getByLead($busqueda['documento']);
                $addContactos = AddContacto::getByContactos($contactos);
                $idContacto = $request->get('idContacto', null);
                // dd($contactos);
                for ($i = 0; $i < count($contactos); $i++) {
                    $contacto = $contactos[$i];
                    $productosUni = Lead::getProductos($contacto->NUM_DOC_CONTACTO) ? Lead::getProductos($contacto->NUM_DOC_CONTACTO) : false;
                    if ($productosUni) {
                        array_push($productos, ['PRODUCTOS' => $productosUni, 'ID_CONTACTO' => ($contacto->ID_CONTACTO)]);
                    }
                }
                
                $vista = view('be.micontacto.index')
                    ->with('lead', $lead)
                    ->with('cliente', $cliente)
                    ->with('contactos', $contactos)
                    ->with('addContactos', $addContactos)
                    ->with('busqueda', $busqueda)
                    ->with('idContacto', $idContacto)
                    ->with('usuario', $this->user)
                    ->with('productos', $productos)
                    ->with('departamentos', Ubigeo::getDepartamento());

                if ($lead)
                    $vista = $vista->with('provincias', Ubigeo::getProvincia($lead->DEPARTAMENTO))
                        ->with('distritos', Ubigeo::getDistrito($lead->PROVINCIA));
                else if ($cliente)
                    $vista = $vista->with('provincias', Ubigeo::getProvincia($cliente->DEPARTAMENTO))
                        ->with('distritos', Ubigeo::getDistrito($cliente->PROVINCIA));
                return $vista;
            } else {
                return view('be.micontacto.index')
                    ->with('busqueda', $busqueda)
                    ->with('cliente', null)
                    ->with('lead', null);
            }
        } else {

            return view('be.micontacto.index')
                ->with('busqueda', $busqueda)
                ->with('cliente', null)
                ->with('lead', null);
        }
    }
    public function autocomplete(Request $request)
    {
        return Lead::autoCompleteLead($this->user, $request->get('termino', ''));
    }

    public function updateDatos(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'numdoc' => 'required',
            'departamento' => 'nullable|max:75',
            'provincia' => 'nullable|max:75',
            'distrito' => 'nullable|max:75',
            'telefono' => 'nullable|digits_between:6,9',
            'direccion' => 'nullable|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json(array_flatten($validator->errors()->getMessages()), 404);
        }

        $lead = new Lead();
        $lead->setValues([
            '_numdoc' => $request->get('numdoc', null),
            '_departamento' => $request->get('departamento', null),
            '_provincia' => $request->get('provincia', null),
            '_distrito' => $request->get('distrito', null),
            '_telefono' => $request->get('telefono', null),
            '_direccion' => $request->get('direccion', null)
        ]);

        if ($lead->actualizar()) {
            return response()->json('ok', 200);
        } else {
            return response()->json(['msg' => $lead->getMessage()], 404);
        }
    }

    public function quitarContacto(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'idContacto' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(array_flatten($validator->errors()->getMessages()), 404);
        }

        $contacto = new contacto();
        $contacto->setValues([
            '_id' => $request->get('idContacto', null)
        ]);

        if ($contacto->quitarContacto()) {
            //return 1;
            flash('Se elimino el contacto')->success();
            return redirect()->route('be.micontacto.index', ['documento' => $request->get('numdoc', null)]);
        } else {
            //return 0;

            return redirect()->route('be.micontacto.index', ['documento' => $request->get('numdoc', null)]);
        }
    }

    public function agregarContacto(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'nombres' => 'required',
            'apepat' => 'required',
            'cargo' => 'required',
            'telefono' => 'required',
        ]);

        if ($validator->fails()) {
            flash(array_flatten($validator->errors()->getMessages()))->error();
            return back()->withInput($request->input());
        }

        $contacto = new Contacto();
        $contacto->setValues([
            '_numdoc' => $request->get('numdoc', null),
            '_numdocContacto' => $request->get('numdoccontacto', null),
            '_nombre' => $request->get('nombres', null),
            '_apaterno' => $request->get('apepat', null),
            '_amaterno' => $request->get('apemat', null),
            '_cargo' => $request->get('cargo', null),
            '_direccion' => $request->get('direccion', null),
            '_email' => $request->get('email', null),
            '_flgVigente' => "1",
            '_ejecutivo' => $this->user->getValue('_registro')
        ]);


        $addContacto = new AddContacto();
        $addContacto->setValues([
            '_telefono' => $request->get('telefono', null),
            '_ejecutivo' => $request->get('eNegocio', null),
            '_tipoRegistroContacto' => 'E',
            '_fechaRegistro' => Carbon::now(),
        ]);

        $response = $contacto->setContacto($addContacto);

        if ($request->get('actividades', null) == 'actividades') {
            return response()->json($contacto->getValues());
        }

        if ($response) {
            flash('Se agrego el contacto de manera correcta')->success();
            return redirect()->route('be.micontacto.index', ['documento' => $request->get('numdoc', null), 'idContacto' => $contacto->getValue('_id')]);
        } else {
            flash($request->getMessage())->error();
            return redirect()->route('be.micontacto.index', ['documento' => $request->get('numdoc', null)]);
        }
    }

    public function updateContacto(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'idcontacto' => 'required',
            'numdoc' => 'required',
            'numdoccontacto' => 'nullable|digits:8',
            'nombre' => 'max:50',
            'apaterno' => 'max:50',
            'amaterno' => 'nullable|max:50',
            'cargo' => 'nullable|max:50',
            'direccion' => 'nullable|max:255',
            'email' => 'nullable|max:75',
        ]);

        if ($validator->fails()) {
            return response()->json(array_flatten($validator->errors()->getMessages()), 404);
        }

        $contacto = new Contacto();
        $contacto->setValues([
            '_id' => $request->get('idcontacto', null),
            '_numdoc' => $request->get('numdoc', null),
            '_numdocContacto' => $request->get('numdoccontacto', null),
            '_nombre' => $request->get('nombre', null),
            '_apaterno' => $request->get('apaterno', null),
            '_amaterno' => $request->get('amaterno', null),
            '_cargo' => $request->get('cargo', null),
            '_direccion' => $request->get('direccion', null),
            '_email' => $request->get('email', null),
            '_ejecutivo' => $this->user->getValue('_registro')
        ]);

        if ($contacto->actualizar()) {
            //return response()->json($contacto, 200);
            // return  redirect()->route('be.micontacto.index',['documento'=>$request->get('numdoc', null)]);
            return 0;
        } else {
            // return response()->json(['msg' => $contacto->getMessage()], 404);
            return redirect()->route('be.micontacto.index', ['documento' => $request->get('numdoc', null)]);
        }
    }

    public function addFeedback(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'lead' => 'required',
            'valor' => 'required',
            'feedback' => 'required|in:' . implode(',', AddContacto::getFeedbacks()),
        ]);

        if ($validator->fails()) {
            return response()->json(array_flatten($validator->errors()->getMessages()), 404);
        }

        $contacto = new AddContacto();
        $contacto->setValues([
            '_idContacto' => $request->get('lead', null),
            '_telefono' => $request->get('valor', null),
            '_feedback' => $request->get('feedback', null),
        ]);

        if ($contacto->setFeedback()) {
            return response()->json($contacto, 200);
        } else {
            return response()->json(['msg' => $contacto->getMessage()], 404);
        }
    }

    public function quitarFeedback(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'lead' => 'required',
            'valor' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(array_flatten($validator->errors()->getMessages()), 404);
        }

        $contacto = new AddContacto();
        $contacto->setValues([
            '_idContacto' => $request->get('lead', null),
            '_telefono' => $request->get('valor', null),
        ]);

        if ($contacto->quitarFeedback()) {
            return response()->json($contacto, 200);
        } else {
            return response()->json(['msg' => $contacto->getMessage()], 404);
        }
    }

    public function addContactoData(Request $request)
    {

        $validator = null;
        $validator = Validator::make($request->all(), [
            'lead' => 'required',
            'telefono' => 'required|digits_between:6,9',
            'anexo' => 'nullable|digits_between:1,7',
        ]);


        if (!$validator || $validator->fails()) {
            return response()->json(['msg' => 'Input incorrecto'], 404);
        }

        $addContacto = new AddContacto();
        $addContacto->setValues([
            '_idContacto' => $request->get('lead', null),
            '_ejecutivo' => $this->user->getValue('_registro'),
            '_tipoContacto' => $request->get('tipocontacto'),
            '_tipoRegistroContacto' => 'E',
            '_telefono' => $request->get('telefono', null),
            '_anexo' => $request->get('anexo', null),
            '_feedback' => AddContacto::FEEDBACK_POSITIVO,
        ]);

        \Debugbar::info('despues: ' . $addContacto->getValue('_telefono'));

        if ($addContacto->registrar()) {

            return response()->json(['msg' => 'ok', 'telefono' => $addContacto->getValue('_telefono'), 'anexo' => $addContacto->getValue('_anexo')], 200);
        } else {
            return response()->json(['msg' => $addContacto->getMessage()], 404);
        }
    }

    public function setContactoTipo(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'idcontacto' => 'required',
            'tipo' => 'required',
            'active' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(array_flatten($validator->errors()->getMessages()), 404);
        }

        $contacto = new ContactoEncuesta();

        $contacto->setValues([
            '_id' => $request->get('idcontacto', null),
            '_tipo' => $request->get('tipo', null),
        ]);

        $result = false;
        if ($request->get('active', null) == "true") {
            $result = $contacto->agregar();
        } else {
            $result = $contacto->quitar();
        }

        if ($result) {
            return response()->json($contacto, 200);
        } else {
            return response()->json(['msg' => $contacto->getMessage()], 404);
        }
    }

    public function setContactoEncuesta(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'idcontacto' => 'required',
            'tipo' => 'required',
            'active' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(array_flatten($validator->errors()->getMessages()), 404);
        }

        $contacto = new ContactoEncuesta();

        $contacto->setValues([
            '_id' => $request->get('idcontacto', null),
            '_tipo' => $request->get('tipo', null),
            '_flag' => $request->get('active', 0) == "true" ? 1 : 0,
        ]);

        if ($contacto->actualizar()) {
            return response()->json($contacto, 200);
        } else {
            return response()->json(['msg' => $contacto->getMessage()], 404);
        }
    }
}
