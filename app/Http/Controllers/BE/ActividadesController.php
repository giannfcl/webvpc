<?php

namespace App\Http\Controllers\BE;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Entity\BE\Lead as Lead;
use App\Entity\BE\AccionComercial as AccionComercial;
use App\Entity\BE\Contacto as Contacto;
use App\Entity\Usuario as Usuario;
use App\Entity\BE\Actividad as Actividad;
use App\Entity\BE\ActividadParticipante as aParticipante;
use Validator;

class ActividadesController extends Controller
{

    public function index(Request $request)
    {
        $busqueda = [
            'documento' => $request->get('documento', null),
            'page' => $request->get('page', 1),
        ];
        $acciones = [];
        $accionAvanzada = [];
        $flgLinkedinMostrar = NULL;
        $volumenpap = Lead::getDetalleVolumenPAP2(array('documento' => $busqueda['documento']));
        $registro_JEFE = NULL;

        if ($busqueda['documento']) {
            $lead = Lead::getMiContacto($busqueda['documento'], $this->user);
            if ($lead) {
                $registro_EN = $lead->REGISTRO_EN;
                $registro_JEFE = Lead::getRegistroJefe($registro_EN);
            }
            if (isset($lead) and $lead->FLAG_ES_CLIENTE == 1) $lead = null;
            $cliente = AccionComercial::getMiContacto($busqueda['documento'], $this->user);
            if ($lead or $cliente) {
                $contactos = Contacto::getByLead($busqueda['documento']);
                $actividades = Actividad::listarActividades($busqueda);
                $comunicaciones = Actividad::listarComunicaciones($busqueda['documento']);
                if ($cliente) $acciones = AccionComercial::getNombreAcciones();
                if ($acciones) $lead = [];
            } else {
                return view('be.actividades.index')
                    ->with('busqueda', $busqueda)
                    ->with('flgLinkedinMostrar', $flgLinkedinMostrar)
                    ->with('cliente', null)
                    ->with('lead', null)
                    ->with('comunicaciones', [])
                    ->with('accionesEstrategia', [])
                    ->with('volumenpap', $volumenpap);
            }
            if ($cliente and AccionComercial::getAccionAvanzada($busqueda['documento']) != null)
                $accionAvanzada = AccionComercial::getAccionAvanzada($busqueda['documento'])[0];
            return view('be.actividades.index')
                ->with('lead', $lead)
                ->with('flgLinkedinMostrar', $flgLinkedinMostrar)
                ->with('registro_JEFE', $registro_JEFE)
                ->with('cliente', $cliente)
                ->with('comunicaciones', $comunicaciones)
                ->with('accionesEstrategia', AccionComercial::getAccionesEstrategias())
                ->with('eProductos', Usuario::getEjecutivosProductoByZonal($this->user->getValue('_zona')))
                ->with('mesesActivacion', AccionComercial::getMesesActivacion())
                ->with('accionAvanzada', $accionAvanzada)
                ->with('busqueda', $busqueda)
                ->with('contactos', $contactos)
                ->with('usuario', $this->user)
                ->with('listaAcciones', $acciones)
                ->with('actividades', $actividades)
                ->with('volumenpap', $volumenpap);
        } else {
            //Vacío 
            return view('be.actividades.index')
                ->with('busqueda', $busqueda)
                ->with('flgLinkedinMostrar', $flgLinkedinMostrar)
                ->with('cliente', null)
                ->with('lead', null)
                ->with('comunicaciones', [])
                ->with('mesesActivacion', AccionComercial::getMesesActivacion())
                ->with('accionesEstrategia', AccionComercial::getAccionesEstrategias())
                ->with('eProductos', Usuario::getEjecutivosProductoByZonal($this->user->getValue('_zona')))
                ->with('volumenpap', $volumenpap);
        }
    }
    public function confirmarEliminar(Request $request)
    {
        $data = $request->all();
        Lead::confirmarEliminar($data['NUMDOC'],$data['REGISTRO_JEFE'],$data['ESTRATEGIA_ID'],$data['REGISTRO_EN'],$data['EMPRESA']);
    }
    public function updateVerificado(Request $request)
    {
        $data = $request->all();
        Lead::updateVerificado($data['NUMDOC'],$data['REGISTRO_JEFE'],$data['ESTRATEGIA_ID'],$data['REGISTRO_EN'],$data['EMPRESA']);
    }
    public function autocomplete(Request $request)
    {
        return Lead::autoCompleteLead($this->user, $request->get('termino', ''));
    }

    public function autocompleteParticipantes(Request $request)
    {
        return aParticipante::autocompleteParticipantes($request->get('termino', ''), $this->user->getValue('_registro'));
    }

    public function historialActividades(Request $request)
    {
        $busqueda = [
            'documento' => $request->get('documento', null),
            'page' => $request->get('page', 1),
        ];
        $actividades = Actividad::listarActividades($busqueda);

        return view('be.actividades.historia')
            ->with('actividades', $actividades);
    }

    public function agregarActividad(Request $request)
    {

        $acciones = array();
        if (Actividad::buscarVisitasRepetidas($request->get('numdoc'), $request->get('ejeNegocio'), $request->get('fActividad'))) {
            flash('No puedes registar más de una visita para el mismo día.')->error();
            return back();
        }

        if ($request->get('flgAccion', null)) {

            //DEBEMOS DE PASAR EL FILTRO DEL CLIENTE A REGISTRAR
            $atributos = [
                'rol' => $this->user->getValue('_rol'),
                'banca' => $this->user->getValue('_banca'),
                'zonal' => $this->user->getValue('_zona'),
                'jefatura' => $this->user->getValue('_centro')
            ];

            $cliente = AccionComercial::buscarCliente(AccionComercial::getCodUnico($request->get('numdoc')));

            //Vemos los ejecutivos que son de nuestra zonal (en caso seamos Analista o cash)
            $regEjecutivos = [];
            if (in_array($atributos['rol'], array_merge(Usuario::getAnalistasInternosBE(), Usuario::getEjecutivosProductoBE()))) {
                $ejecutivos = Usuario::getFarmersHunters($atributos['banca'], $atributos['zonal'], $atributos['jefatura']);

                for ($i = 0; $i < $ejecutivos->count(); $i++) {
                    $regEjecutivos[$i] = $ejecutivos[$i]->REGISTRO;
                }

                //El cliente también puede estar sectorizado a un analista
                $regEjecutivos[$ejecutivos->count()] = $this->user->getValue('_registro');
                if (!(in_array($cliente->REGISTRO_EN, $regEjecutivos) or $cliente->ZONA == $this->user->getValue('_zona'))) {
                    flash('No puedes registrarle acciones comerciales a un cliente que no sea de tu zonal.')->error();
                    return back();
                }
            } else if (in_array($atributos['rol'], Usuario::getEjecutivosBE()) and $cliente->REGISTRO_EN != $this->user->getValue('_registro')) {
                flash('No puedes agregarle acciones comerciales a un cliente que no sea de tu cartera.')->error();
                return back();
            }

            if (!$cliente->FLG_ES_CLIENTE) {
                //Validar que pertenezca a una campaña            
                if (AccionComercial::tieneCampanhaActiva(AccionComercial::getNumDoc($request->get('codUnico'))) < 1) {
                    //flash('No se puede ingresar acción comercial a un prospecto.')->error();
                    flash('No se puede ingresar acción comercial a un prospecto que no se encuentre en campaña.')->error();
                    return back();
                }
            }

            //Aquí registramos las acciones comerciales
            $idAcciones = explode(',', $request->get('idAccionActividad')[0]);
            $cboDelegado = explode(',', $request->get('cboDelegadoActividad')[0]);
            $cboMesActiv = explode(',', $request->get('cboMesActivActividad')[0]);
            $kpiAccion = explode(',', $request->get('kpiAccionActividad')[0]);
            $fFin = explode(',', $request->get('fFinActividad')[0]);
            $notaRentabilizar = $request->get('notaRentabilizar', null);
            $notaBrindar = $request->get('notaBrindar', null);

            for ($i = 0; $i < count($idAcciones); $i++) {
                $acc['idAccion'] = $idAcciones[$i];
                $acc['cboDelegado'] = $cboDelegado[$i] == "" ? NULL : $cboDelegado[$i];
                $acc['cboMesActiv'] = $cboMesActiv[$i] == "" ? NULL : $cboMesActiv[$i];
                $acc['kpiAccion'] = $kpiAccion[$i] == "" ? NULL : $kpiAccion[$i] * 1000;
                $acc['fFin'] = $fFin[$i] == "" ? NULL : $fFin[$i];
                $acc['nota'] = (AccionComercial::getEstrategias($idAcciones[$i])->first()->ESTRATEGIA == "RENTABILIZAR CLIENTES") ? $notaRentabilizar : $notaBrindar;
                $acciones[] = $acc;
            }
        }

        $nuevaActividad = new Actividad();
        $nuevaActividad->setValues([
            '_numdoc' => $request->get('numdoc', null),
            '_ejNegocio' => $request->get('ejeNegocio', null),
            '_fechaActividad' => $request->get('fActividad', null),
            '_titulo' => $request->get('titulo', null),
            '_tipo' => $request->get('tipo', null),
            '_flgRenoviacionLinea' => $request->get('flgRenovacion', 0),
            '_ubicacion' => $request->get('ubicacion', null),
            '_temasComerciales' => $request->get('tComerciales', null),
            '_temasCrediticios' => $request->get('tCrediticios', null),
            //'_idCampEst' => $request->get('campEst', null),
            '_participantesIbk' => $request->get('patInterbank', []),
            '_participantesCliente' => $request->get('partCliente', []),
            '_accionesComerciales' => $acciones,
            '_flgLinkedin' => $request->get('flgLinkedin', null),
            // '_flgLinkedin2'=>$request->get('flgLinkedin2',null),
        ]);

        if ($nuevaActividad->agregarActividad($this->user)) {
            flash('La actividad fue registrada correctamente')->success();
            return redirect()->route('be.actividades.index', ['documento' => $request->get('numdoc', null)]);
        } else {
            flash($nuevaActividad->getMessage())->error();
            return redirect()->route('be.actividades.index', ['documento' => $request->get('numdoc', null)]);
        }
    }

    public function get(Request $request)
    {
    }
}
