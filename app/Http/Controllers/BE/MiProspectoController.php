<?php

namespace App\Http\Controllers\BE;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entity\BE\Lead as Lead;
use App\Entity\BE\leadEtapa as LeadEtapa;
use App\Entity\BE\Etapa as Etapa;
use App\Entity\Campanha as Campanha;
use App\Entity\Banco as Banco;
use App\Entity\BE\MantenerLead as MantenerLead;
use App\Entity\BE\ZonalJefatura as ZonalJefatura;
use App\Entity\Usuario as Usuario;
use App\Entity\BE\NotaEjecutivo as NotaEjecutivo;
use Validator;

class MiProspectoController extends Controller {

    public function index(Request $request) {

    	$busqueda = [
			'page' => $request->get('page',1),
			'documento' => $request->get('documento',null),
            'razonSocial' => $request->get('razonSocial',null),
            'semaforo' => $request->get('semaforo',null),
            'estrategia' => $request->get('estrategia',null),
            'etapa' => $request->get('etapa',null),
            'verificado' => $request->get('verificado',null),
            'bcoPrincipal' => $request->get('bcoPrincipal',null),
            'minDeudaDirecta' => $request->get('minDeudaDirecta',null),
            'maxDeudaDirecta' => $request->get('maxDeudaDirecta',null),
            'minDeudaIndirecta' => $request->get('minDeudaIndirecta',null),
            'maxDeudaIndirecta' => $request->get('maxDeudaIndirecta',null),
            'bcoPrincipalGarantia' => $request->get('bcoPrincipalGarantia',null),
            'minGarantia' => $request->get('minGarantia',null),
            'maxGarantia' => $request->get('maxGarantia',null),
            'ejecutivo' => $request->get('ejecutivo',null),
            'jefatura' => $request->get('jefatura',null),
            'zonal' => $request->get('zonal',null),
            'banca'=>$request->get('banca',null),
            'volumenPAP'=>$request->get('volumenPAP',null),
		];

        $orden = [
            'sort' => $request->get('sort',null),
            'order' => $request->get('order',null),
        ];
        $entrada=Lead::getMisProspectos($this->user,$busqueda,$orden)
            ->setPath(config('app.url').'be/miprospecto/lista');

        
        $ejecutivos = [];
        $jefaturas = [];
        $zonales = [];
        $bancas=[];

       
        if (in_array($this->user->getValue('_rol'),Usuario::getJefaturasBE())){
            $ejecutivos = Usuario::getFarmersHunters(null,null,$this->user->getValue('_centro'));
        }
        if (in_array($this->user->getValue('_rol'),Usuario::getZonalesBE())){
            $jefaturas = ZonalJefatura::getJefaturas($this->user->getValue('_zona'));
            $ejecutivos = Usuario::getFarmersHunters(null,$this->user->getValue('_zona'));
        }

         if(in_array($this->user->getValue('_rol'),Usuario::getBanca())){
            $zonales = ZonalJefatura::getZonales($this->user->getValue('_banca'));
            $jefaturas = ZonalJefatura::getJefaturas(null,$this->user->getValue('_banca'));
            $ejecutivos=Usuario::getFarmersHunters($this->user->getValue('_banca'));
        }

        if (in_array($this->user->getValue('_rol'),Usuario::getDivisionBE())){
            $bancas= ZonalJefatura::getBancas();
            $zonales = ZonalJefatura::getZonales();
            $jefaturas = ZonalJefatura::getJefaturas();
            $ejecutivos = Usuario::getFarmersHunters();
        }
        $cantreferidos=(!empty(\App\Entity\Usuario::getCantReferidos($this->user->getValue('_registro'))) ? \App\Entity\Usuario::getCantReferidos($this->user->getValue('_registro'))->NUM_REFERIDOS : 0);
        
        return view('be.miprospecto.index')
            ->with('busqueda',$busqueda)
            ->with('leads', $entrada)
            ->with('etapasHabilitadas',Etapa::getHabilitadas())
            ->with('ejecutivos',$ejecutivos)
            ->with('jefaturas',$jefaturas)
            ->with('zonales',$zonales)
            ->with('bancas',$bancas)
            ->with('usuario',$this->user)
            ->with('orden',$orden)
            ->with('etapas',Etapa::getAll('PROSPECTOS'))
            ->with('estrategias',Campanha::getCampanhasBE())
            ->with('bancos',  Banco::getBancos())
            ->with('bancosGarantias',Banco::getBancos());
    }

    public function updateEtapa(Request $request){
        $validator = Validator::make($request->all(), [
                'lead' => 'required',
                'etapa' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(array_flatten($validator->errors()->getMessages()), 404);
        }

        $leadEtapa = new leadEtapa();
        $leadEtapa->setValues([
            '_numdoc' => $request->get('lead', null),
            '_etapa' => $request->get('etapa', null),
            '_ejecutivo' => $this->user->getValue('_registro'),
        ]);

        if ($leadEtapa->cambiarEtapa($this->user)) {
            return response()->json($leadEtapa, 200);
        } else {
            return response()->json(['msg' => $leadEtapa->getMessage()], 404);
        }
    }

    public function consultaReferido(Request $request) {
        $validator = Validator::make($request->all(), [
                'documento' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(array_flatten($validator->errors()->getMessages()), 404);
        }

        $data = Lead::buscarReferidoExtra($request->get('documento'));
        //dd($data);
        if ($data) {
            return response()->json(['existe' => 'si', 'data' => $data], 200);
        } else {
            return response()->json(['existe' => 'no'], 200);
        }
    }

    public function registroReferido(Request $request){
        //dd($request->all());
        $validator = Validator::make($request->all(), [
            'documento' => 'required',
            'flgLinkedin'=>'required'
        ],['flgLinkedin.required' => 'Es necesario especificar si se contactó al cliente por LinkedIn o no']);

        $cantreferidos=false;
        if ($validator->fails()) {
            flash(response()->json(array_flatten($validator->errors()->getMessages()), 404)->original[0])->error();
            return back();
        }

        //dd($request->all());
        if(in_array($this->user->getValue('_rol'),array_merge(Usuario::getEjecutivosBE(),Usuario::getAnalistasInternosBE()))){
            $referidosMes=Lead::referidosMesActual($this->user->getValue('_registro'));
            if($referidosMes !=NULL){
                if($referidosMes->NUM_REFERIDOS>=5){
                    return response()->json(['cantreferidos' => $referidosMes->NUM_REFERIDOS]);
                    // flash('El número máximo de referidos al mes es 5.')->error();
                    // return back();
                    // return redirect()->route('be.miprospecto.lista.index', ['documento' => $request->get('numdoc', null)]);
                }
            }
        }
        
        $lead = new Lead();
        $result = $lead->registrarReferido($request->get('documento'),$this->user->getValue('_registro'),$this->user->getValue('_segmento'),
            ['nombre' => $request->get('nombreNuevo',null),
             'facturacion' => $request->get('facturacionNuevo',null),
             'flgLinkedin'=> $request->get('flgLinkedin',null)
             ]
        );

        if ($result) {
            return response()->json(['cantreferidos' => isset($referidosMes->NUM_REFERIDOS) ? $referidosMes->NUM_REFERIDOS : 0]);
            // flash('Se agregó al referido ' . $request->get('documento') . ' de manera correcta')->success();
        } else {
            // flash($lead->getMessage())->error();
        }
        // return back();

    }

    public function eliminarProspecto(Request $request){
        //dd($request->all());
        $validator = Validator::make($request->all(), [           
            'flgLinkedin'=>'required'
        ],['flgLinkedin.required' => 'Es necesario especificar si se contactó al cliente por LinkedIn o no']);

        if ($validator->fails()) {
            flash(response()->json(array_flatten($validator->errors()->getMessages()), 404)->original[0])->error();
            return back();
        }
        //dd($request->all());
        $documento=$request->get('documentoE');
        $tipo=$request->get('eliminar');
        $motivo="";
        $lead = new Lead();
        
        if($tipo<>"5"){
            $motivo=$request->get('xmotivo');
            $comentario=$request->get('eliminarComentario');
            $result = $lead->eliminarLead($documento,$tipo,$motivo,$comentario,$request->get('flgLinkedin',0));
        }

        if($tipo=="5"){
            $motivo="1"; //Detalle por defecto
            $comentario=$request->get('eliminarComentario');
            $result = $lead->eliminarLead($documento,$tipo,$motivo,$comentario);
        }
        switch ($result) {
            case 0:
                flash($lead->getMessage())->error();
                break;
            case 1:
                flash('Se envió a validación la eliminiación del lead ' . $request->get('documento') . ' de manera correcta')->success();
                break;  
            case 2:
                flash('Se eliminó el lead ' . $request->get('documento') . ' de manera correcta')->success();
                break;            
        }
           
        return back();
    }

    public function mantenerLead(Request $request){
        $validator = Validator::make($request->all(), [
            'lead' => 'required',
            'marca' => 'required',
        ]);

        $mlead = new MantenerLead();
        $mlead->setValues([
            '_numdoc' => $request->get('lead', null),
        ]);

        $result = false;
        if($request->get('marca') == '1'){
            $result = $mlead->insert($this->user);
        }elseif($request->get('marca') == '0'){
            $result = $mlead->delete($this->user);
        }

        if($result){
            return response()->json(['msg' => 'ok'], 200);
        }else{
            return response()->json(['msg' => $mlead->getMessage()], 404);
        }
    }

    public function agregarNota(Request $request){

        $validator = Validator::make($request->all(), [
            'lead' => 'required',
            'nota' => 'required|max:500',
        ]);

        if ($validator->fails()) {
            return response()->json(array_flatten($validator->errors()->getMessages()), 404);
        }

        $nota = new NotaEjecutivo();
        $nota->setValues([
            '_ejecutivo' => $this->user->getValue('_registro'),
            '_numdoc' => $request->get('lead', null),
            '_nota' => $request->get('nota', null),
        ]);

        if ($nota->agregar()) {
            return response()->json($nota->setValueToTable(), 200);
        } else {
            return response()->json(['msg' => $nota->getMessage()], 404);
        }

    }

    public function eliminarNota(Request $request){

        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(array_flatten($validator->errors()->getMessages()), 404);
        }

        $nota = new NotaEjecutivo();
        $nota->setValues([
            '_ejecutivo' => $this->user->getValue('_registro'),
            '_numdoc' => $request->get('lead', null),
            '_id' => $request->get('id', null),
        ]);

        if ($nota->eliminar()) {
            return response()->json('ok', 200);
        } else {
            return response()->json(['msg' => $nota->getMessage()], 404);
        }
        
    }

    public function detalleVpap(Request $request)
    {
        $result=Lead::getDetalleVolumenPAP($request->all());
        return response()->json($result);
    }

    public function listarNotas(Request $request){
        return NotaEjecutivo::listar($this->user->getValue('_registro'),$request->get('lead', null));
    }

    public function reasignaciones()
    {
        return view('be.reasignaciones.index');
    }

    public function getLeadsAsignadas()
    {
        $leads=Lead::getLeadsVPC();
        return response()->json($leads);
    }
    
}