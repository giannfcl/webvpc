<?php

namespace App\Http\Controllers\BE\Infinity;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entity\Usuario;
use App\Entity\Infinity\Cliente as Cliente;
use App\Model\Infinity\Visita as mVisita;
use App\Entity\Infinity\ConocemeCliente as Conoceme;
use App\Entity\Infinity\AlertaExplicacion as AlertaExplicacion;
use App\Entity\Infinity\AlertaDocumentacion as AlertaDocumentacion;
use App\Entity\Infinity\ConocemeOpciones as ConocemeOpciones;
use App\Entity\Infinity\VariableInformativa as VariableInformativa;
use Yajra\Datatables\Datatables as Datatables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Jenssegers\Date\Date as Carbon;
use App\Entity\Infinity\Visita as Visita;
use App\Entity\Infinity\Alertacartera as Alertacartera;
use App\Entity\Infinity\MensajeSegVigentes as MensajeSegVigentes;
use App\Entity\BE\SegVigCasos as SegVigCasos;
use App\Entity\Gestioncartera as Gestioncartera;
use App\Entity\Infinity\FichaCovid as FichaCovid;
use App\Entity\BE\Lead;
use PDF;
use Validator;

class AlertascarteraController extends Controller
{

    const MEDIANA_EMPRESA = 'MEDIANA EMPRESA';
    const ZONAL_SAN_ISIDRO = 'ZONAL SAN ISIDRO';

    const GRAN_EMPRESA = 'GRAN EMPRESA';
    const ZONAL_CHACARILLA = 'ZONAL CHACARILLA';
    const ZONAL_MIRAFLORES = 'ZONAL MIRAFLORES';

    const PROVINCIAS = 'PROVINCIAS';
    const ZONAL_SUR = 'ZONAL SUR';
    const ZONAL_NORTE = 'ZONAL NORTE';
    const ZONAL_CENTRO_ORIENTE = 'ZONAL CENTRO ORIENTE';

    const NO = false;
    const SI = true;
    
    const TIPO_ALERTA = '1';
    const TIPO_SEGUIMIENTO = '2';

    public function index(Request $request)
    {
        $data=array('banca'=>null,'zonal'=>null,'jefatura'=>null,'ejecutivo'=>null,'registro'=>null);
        $usuariogc = null;
        $id = $this->user->getValue('_idgc');
        if($id){
            $usuariogc = Gestioncartera::getArbol($this->user->getValue('_idgc'));
            $data['banca']=$usuariogc->TIPO_BANCA;
            $data['zonal']=$usuariogc->NOMBRE_ZONAL;
            $data['ejecutivo']=$usuariogc->ENCARGADO;
            $data['registro']=$usuariogc->REGISTRO;
        }
        $vista = $this-> visibilidadFiltro($usuariogc);
        $esjefe = ($this->user->getValue('_rol') == Usuario::ROL_JEFATURA_BE)?'1':'0';
        if($esjefe){
            $data['jefatura']=$data['registro'];
        }
        $vista['bancas_data'] =  $vista['bancas']==self::SI?Alertacartera::bancas('MEDIANA EMPRESA'):'<option value="'.$data['banca'].'" selected>'.$data['banca'].'</option>';
        $vista['zonales_data'] =  $vista['zonales']==self::SI?Alertacartera::zonales($data):'<option value="'.$data['zonal'].'" selected>'.$data['zonal'].'</option>';
        $vista['jefaturas_data'] =  $vista['jefaturas']==self::SI?Alertacartera::jefaturas($data):'<option value="'.$data['jefatura'].'" selected>'.$data['jefatura'].'</option>';
        $vista['ejecutivos_data'] =  $vista['ejecutivos']==self::SI?Alertacartera::ejecutivos($data):'<option value="'.$data['registro'].'" selected>'.$data['ejecutivo'].'</option>';
        $meses=Alertacartera::getPDF_Meses();
        $visita = new Visita();
        $revision = $visita ->getAreasRevision($this->user->getValue('_rol'));
        return view('be.infinity.index')
            ->with('cliente',null)
            ->with('rol',$this->user->getValue('_rol'))
            ->with('twebarbol', $usuariogc)
            ->with('revision', $revision)
            ->with('vista', $vista)
            ->with('esjefe', $esjefe)
            ->with('iconoAlertas', AlertaDocumentacion::getHTML())
            ->with('registro', $this->user->getValue('_registro'))
            ->with('meses', $meses);
    }

    public function visibilidadFiltro($usuariogc=null){
        switch ($this->user->getValue('_rol')) {
            case Usuario::ROL_EJECUTIVO_FARMER_BE:
            case Usuario::ROL_EJECUTIVO_HUNTER_BE:
                $vista['bancas'] = self::NO;
                $vista['zonales'] = self::NO;
                $vista['jefaturas'] = self::NO;
                $vista['ejecutivos'] = self::NO;
                break;
            case Usuario::ROL_JEFATURA_BE:
                $vista['bancas'] = self::NO;
                $vista['zonales'] = self::NO;
                $vista['jefaturas'] = self::NO;
                $vista['ejecutivos'] = self::SI;
                break;
            case Usuario::ROL_ANALISTA_NEGOCIO_ZONAL_BE:
                if($usuariogc && $usuariogc->TIPO_BANCA == self::MEDIANA_EMPRESA){
                    $vista['bancas'] = self::NO;
                    $vista['zonales'] = self::NO;
                    $vista['jefaturas'] = self::SI;
                    $vista['ejecutivos'] = self::SI;
                }else{
                    $vista['bancas'] = self::NO;
                    $vista['zonales'] = self::NO;
                    $vista['jefaturas'] = self::NO;
                    $vista['ejecutivos'] = self::SI;
                }
                break;
            case Usuario::ROL_ANALISTA_EXTERNO_ZONAL_BE:
                $vista['bancas'] = self::NO;
                $vista['zonales'] = self::NO;
                $vista['jefaturas'] = self::NO;
                if ($usuariogc && $usuariogc->NOMBRE_ZONAL == self::ZONAL_SAN_ISIDRO) {
                    $vista['jefaturas'] = self::SI;
                }
                $vista['ejecutivos'] = self::SI;
                break;
            case Usuario::ROL_EJECUTIVO_SEGUIMIENTO_CARTERA:
            case Usuario::ROL_EJECUTIVO_INNOVACION_CARTERA:
                $vista['bancas'] = self::NO;
                $vista['zonales'] = self::SI;
                $vista['jefaturas'] = self::SI;
                $vista['ejecutivos'] = self::SI;
                break;
            case Usuario::ROL_GERENCIA_ZONAL_BE:
                $flgVisibilidadZonal = '1';
                $vista['bancas'] = self::NO;
                $vista['zonales'] = self::SI;
                $vista['jefaturas'] = self::SI;
                $vista['ejecutivos'] = self::SI;
                break;
            case Usuario::ROL_RIESGOS_ME_ZONAL:
                $flgVisibilidadZonal = '1';
                $vista['bancas'] = self::SI;
                $vista['zonales'] = self::SI;
                $vista['jefaturas'] = self::SI;
                $vista['ejecutivos'] = self::SI;
            case Usuario::ROL_GERENTE_DIVISION_BE:
                $flgVisibilidadBanca = '1';
                $vista['bancas'] = self::SI;
                $vista['zonales'] = self::SI;
                $vista['jefaturas'] = self::SI;
                $vista['ejecutivos'] = self::SI;
            default:
                $vista['bancas'] = self::SI;
                $vista['zonales'] = self::SI;
                $vista['jefaturas'] = self::SI;
                $vista['ejecutivos'] = self::SI;
        }
        return $vista;
    }

    public function combos(Request $request){
        $data=$request->all();
        $vista =  $this-> visibilidadFiltro();
        $bancas =  $vista['bancas']==self::SI?Alertacartera::bancas($data['banca']):'<option selected>'.$data['banca'].'</option>';
        $zonales =  $vista['zonales']==self::SI?Alertacartera::zonales($data):'<option selected>'.$data['zonal'].'</option>';
        $jefaturas =  $vista['jefaturas']==self::SI?Alertacartera::jefaturas($data):'<option selected>'.$data['jefatura'].'</option>';
        $ejecutivos =  $vista['ejecutivos']==self::SI?Alertacartera::ejecutivos($data):'<option selected>'.$data['ejecutivo'].'</option>';
        $result = array($bancas,$zonales,$jefaturas,$ejecutivos);
        return json_encode($result,true);
    }
    public function Seguimientodiario(Request $request)
    {
        return Datatables::of(Alertacartera::Seguimientodiario($request->all()))->make(true);
    }

    public function getResumenAlertas(Request $request)
    {
        return Alertacartera::getResumenAlertas($request->all());
    }
    public function pdfseguimiento(Request $request)
    {
        $datos = Alertacartera::get_datosPdf($request->get('mes'));
        $semanas = Alertacartera::getPDF_Semanas($request->get('mes'));
        $date = date('d-m-Y');
        $pdf = PDF::loadView('be.infinity.pdfseguimiento', compact('datos', 'date', 'semanas'));

        return $pdf->stream('PdfSeguimiento.pdf');
    }

    public function getListaClientes(Request $request)
    {
        return Datatables::of(Cliente::getListaClientesAlerta($this->user, $request->all()))
            ->addColumn('ALERTA_VISITA', function ($row) {
                return AlertaDocumentacion::getNivelAlerta(AlertaDocumentacion::ID_VISITA, $row->FECHA_ULTIMA_VISITA, $row->FLG_INFINITY, $row->FECHA_INGRESO_INFINITY);
            })
            ->addColumn('ALERTA_RECALCULO', function ($row) {
                return AlertaDocumentacion::getNivelAlerta(AlertaDocumentacion::ID_RECALCULO, $row->FECHA_ULTIMO_RECALCULO, $row->FLG_INFINITY, $row->FECHA_INGRESO_INFINITY);
            })
            ->addColumn('ALERTA_DOCUMENTACION', function ($row) {
                return AlertaDocumentacion::getNivelAlertaDocumentacionTotal(
                    $row->FLG_INFINITY,
                    $row->FECHA_ULTIMO_EEFF_1,
                    $row->FECHA_ULTIMO_EEFF_2,
                    $row->FECHA_ULTIMO_IBR,
                    $row->FECHA_ULTIMO_F02,
                    $row->FECHA_INGRESO_INFINITY
                );
            })
            ->addColumn('TIMER', function ($row) {
                if ($row->FECHA_VENCIMIENTO_POLITICA) {
                    $hoy = Carbon::now();
                    $vencimiento = new Carbon($row->FECHA_VENCIMIENTO_POLITICA);
                    return $vencimiento->diffInDaysFiltered(function (Carbon $date) {
                        return true;
                    }, $hoy, false);
                } else {
                    return null;
                }
            })
            ->make(true);
    }

    public function getListaClientesTable(Request $request)
    {
        return Datatables::of(Cliente::getListaClientesAlerta($this->user, $request->all(), 'MEDIANA EMPRESA'))
            ->addColumn('ALERTA_VISITA', function ($row) {
                return AlertaDocumentacion::getNivelAlerta(AlertaDocumentacion::ID_VISITA, $row->FECHA_ULTIMA_VISITA, $row->FLG_INFINITY, $row->FECHA_INGRESO_INFINITY);
            })
            ->addColumn('ALERTA_RECALCULO', function ($row) {
                return AlertaDocumentacion::getNivelAlerta(AlertaDocumentacion::ID_RECALCULO, $row->FECHA_ULTIMO_RECALCULO, $row->FLG_INFINITY, $row->FECHA_INGRESO_INFINITY);
            })
            ->addColumn('ALERTA_DOCUMENTACION', function ($row) {
                return AlertaDocumentacion::getNivelAlertaDocumentacionTotal(
                    $row->FLG_INFINITY,
                    $row->FECHA_ULTIMO_EEFF_1,
                    $row->FECHA_ULTIMO_EEFF_2,
                    $row->FECHA_ULTIMO_IBR,
                    $row->FECHA_ULTIMO_F02,
                    $row->FECHA_INGRESO_INFINITY
                );
            })
            ->addColumn('TIMER', function ($row) {
                if ($row->FECHA_VENCIMIENTO_POLITICA) {
                    $hoy = Carbon::now();
                    $vencimiento = new Carbon($row->FECHA_VENCIMIENTO_POLITICA);
                    return $vencimiento->diffInDaysFiltered(function (Carbon $date) {
                        return true;
                    }, $hoy, false);
                } else {
                    return null;
                }
            })
            ->make(true);
    }

    public function getListaClientesTableProv(Request $request)
    {
        return Datatables::of(Cliente::getListaClientesAlerta($this->user, $request->all(), 'PROVINCIA'))
            ->addColumn('ALERTA_VISITA', function ($row) {
                return AlertaDocumentacion::getNivelAlerta(AlertaDocumentacion::ID_VISITA, $row->FECHA_ULTIMA_VISITA, $row->FLG_INFINITY, $row->FECHA_INGRESO_INFINITY);
            })
            ->addColumn('ALERTA_RECALCULO', function ($row) {
                return AlertaDocumentacion::getNivelAlerta(AlertaDocumentacion::ID_RECALCULO, $row->FECHA_ULTIMO_RECALCULO, $row->FLG_INFINITY, $row->FECHA_INGRESO_INFINITY);
            })
            ->addColumn('ALERTA_DOCUMENTACION', function ($row) {
                return AlertaDocumentacion::getNivelAlertaDocumentacionTotal(
                    $row->FLG_INFINITY,
                    $row->FECHA_ULTIMO_EEFF_1,
                    $row->FECHA_ULTIMO_EEFF_2,
                    $row->FECHA_ULTIMO_IBR,
                    $row->FECHA_ULTIMO_F02,
                    $row->FECHA_INGRESO_INFINITY
                );
            })
            ->addColumn('TIMER', function ($row) {
                if ($row->FECHA_VENCIMIENTO_POLITICA) {
                    $hoy = Carbon::now();
                    $vencimiento = new Carbon($row->FECHA_VENCIMIENTO_POLITICA);
                    return $vencimiento->diffInDaysFiltered(function (Carbon $date) {
                        return true;
                    }, $hoy, false);
                } else {
                    return null;
                }
            })
            ->make(true);
    }

    public function getListaClientesTableGe(Request $request)
    {
        return Datatables::of(Cliente::getListaClientesAlerta($this->user, $request->all(), 'GRAN EMPRESA'))
            ->addColumn('ALERTA_VISITA', function ($row) {
                return AlertaDocumentacion::getNivelAlerta(AlertaDocumentacion::ID_VISITA, $row->FECHA_ULTIMA_VISITA, $row->FLG_INFINITY, $row->FECHA_INGRESO_INFINITY);
            })
            ->addColumn('ALERTA_RECALCULO', function ($row) {
                return AlertaDocumentacion::getNivelAlerta(AlertaDocumentacion::ID_RECALCULO, $row->FECHA_ULTIMO_RECALCULO, $row->FLG_INFINITY, $row->FECHA_INGRESO_INFINITY);
            })
            ->addColumn('ALERTA_DOCUMENTACION', function ($row) {
                return AlertaDocumentacion::getNivelAlertaDocumentacionTotal(
                    $row->FLG_INFINITY,
                    $row->FECHA_ULTIMO_EEFF_1,
                    $row->FECHA_ULTIMO_EEFF_2,
                    $row->FECHA_ULTIMO_IBR,
                    $row->FECHA_ULTIMO_F02,
                    $row->FECHA_INGRESO_INFINITY
                );
            })
            ->addColumn('TIMER', function ($row) {
                if ($row->FECHA_VENCIMIENTO_POLITICA) {
                    $hoy = Carbon::now();
                    $vencimiento = new Carbon($row->FECHA_VENCIMIENTO_POLITICA);
                    return $vencimiento->diffInDaysFiltered(function (Carbon $date) {
                        return true;
                    }, $hoy, false);
                } else {
                    return null;
                }
            })
            ->make(true);
    }

    public function getListaClientesControl()
    {
        return view('be.infinity.me.clientes-control')
            ->with('flgVisibilidadGeneral', !in_array($this->user->getValue('_rol'), Usuario::getEjecutivosBE()) ? 1 : 0);
    }

    public function getListaClientesControlTable(Request $request)
    {
        //dd(Cliente::getListaClientesControl($this->user));
        return Datatables::of(Cliente::getListaClientesControl($this->user))
            ->make(true);
    }

    public function getHistoriaClientes(Request $request)
    {
        return Cliente::getHistoriaClientes($request->get('cu'));
    }

    public function Saveactividad(Request $request)
    {
        $entidad = new Alertacartera();
        if ($entidad->saveActividad($request->all(), $this->user->getValue('_registro'))) {
            flash('Éxito !')->success();
            return back();
        } else {
            flash('No se pudo lograr')->error();
            return back();
        }
    }

    public function getIconoAlerta(Request $request)
    {
        return AlertaDocumentacion::getIcono($request->get('idDocumento'), $request->get('fechaUltimo'));
    }

    public function getVisitasConfirmadas()
    {
        $visitasConfirmadas = Cliente::getVisitasConfirmadas();
        return $visitasConfirmadas;
    }

    public function getdetactividad(Request $request)
    {
        $data = new Alertacartera();
        return $data->getSegActividadByid($request->get('id'));
    }

    public function actualizaractividadcompromiso(Request $request)
    {
        $data = new Alertacartera();
        if ($data->actualizaractividadcompromiso($request->get('d_id'), $request->get('d_cumpliocompromiso'))) {
            flash('Se actualizo con éxito')->success();
        } else {
            flash('No se pudo actualizar')->error();
        }
        return back();
    }
    public function guardarEstrategia(Request $request){
        $visita = new Visita();
        $entidad = new FichaCovid();
        $dataString = $request->get('data');
        $data = json_decode($dataString);
        $cu = $data->cu;
        $nombre = $this->user->getValue('_nombre');
        $privilegios = $visita -> getValidador($cu,$this->user->getValue('_registro'),$this->user->getValue('_rol'),null);
        $result = false;
        $status= false;
        if($privilegios=='3'){
            $status = $entidad->updateEstrategia($data,$this->user->getValue('_registro'),$nombre);
            if($status){
                $result= 'Se guardó los cambios exitosamente';
            }else{
                $result= 'Hubo un error.';
            }
        }else{
            $result = "No tienes privilegios para poder cambiar la estrategia";
        }
        return json_encode(array('message'=>$result,'result'=>$status),null);
    }
    public function addCumplimiento(Request $request){
        $visita = new Visita();
        $entidad = new FichaCovid();
        $dataString = $request->get('data');
        $data = json_decode($dataString);
        $result = false;
        $status= false;
        $registro = $this->user->getValue('_registro');
        $rol = $this->user->getValue('_rol');
        $nombre = $this->user->getValue('_nombre');
        $status = $entidad->addCumplimiento($data,$registro,$rol,$nombre);
        if($status['status']){
            $result= 'Se guardó los cambios exitosamente';
        }else{
            $result= 'Hubo un error.';
        }
        return json_encode(array('message'=>$result,'result'=>$status['status'],'data'=>$status['data']),null);
    }
    public function guardarCumplimiento(Request $request){
        $entidad = new FichaCovid();
        $eliminar = $this->validadorEliminar($request);
        $data = (Object) $request->all();
        $result = false;
        $status= false;
        $registro = $this->user->getValue('_registro');
        $nombre = $this->user->getValue('_nombre');
        $status = $entidad->updateCumplimiento($data,$registro,$nombre,$eliminar=='1');
        if(!$status){
            $result= 'Hubo un error.';
        }else{
            $result= 'Se guardó los cambios exitosamente';
        }
        return json_encode(array('message'=>$result,'result'=>$status),null);
    }

    public function validadorEliminar(Request $request){
        $respuesta = false;
        $id = $request->get('ID',null);
        $rol = $this->user->getValue('_rol');
        $entidad = new FichaCovid();
        $cumplimiento = $entidad -> getCumplimientoById($id);
        $categoria = $cumplimiento->TIPO_GESTION;
        
        //ROL VALIDADOR RANGO: INDISTINTO DEL AREA A LA QUE PERTENECE
            if($categoria=='Comercial'){
                $respuesta = in_array($rol,array(
                    Usuario::ROL_EJECUTIVO_SEGUIMIENTO_CARTERA,
                    Usuario::ROL_JEFATURA_BE,
                    Usuario::ROL_GERENCIA_ZONAL_BE,
                    Usuario::ROL_GERENTE_BANCA,
                    Usuario::ROL_ANALISTA_GYS,
                    Usuario::ROL_SUBGERENTE_GYS,
                    Usuario::ROL_JEFE_RIESGOS,
                    Usuario::ROL_ANALISTA_RIESGOS_ADMISION,
                    Usuario::ROL_GERENTE_RIESGOS,
                    Usuario::ROL_SUBGERENTE_RIESGOS));
            }else if($categoria=='Admisión'){
                $respuesta = in_array($rol,array(
                    Usuario::ROL_JEFE_RIESGOS,
                    Usuario::ROL_ANALISTA_RIESGOS_ADMISION,
                    Usuario::ROL_GERENTE_RIESGOS,
                    Usuario::ROL_SUBGERENTE_RIESGOS
                ));
            }else if($categoria=='Seguimiento comercial'){
                $respuesta = in_array($rol,array(
                    Usuario::ROL_EJECUTIVO_SEGUIMIENTO_CARTERA,
                    Usuario::ROL_GERENCIA_ZONAL_BE,
                    Usuario::ROL_GERENTE_BANCA,
                    Usuario::ROL_ANALISTA_GYS,
                    Usuario::ROL_SUBGERENTE_GYS,
                    Usuario::ROL_JEFE_RIESGOS,
                    Usuario::ROL_ANALISTA_RIESGOS_ADMISION,
                    Usuario::ROL_GERENTE_RIESGOS,
                    Usuario::ROL_SUBGERENTE_RIESGOS

                ));
            }else if($categoria=='GYS'){
                $respuesta = in_array($rol,array(
                    Usuario::ROL_ANALISTA_GYS,
                    Usuario::ROL_SUBGERENTE_GYS,
                ));
            }
        return $respuesta?'1':'0';

    }
    public function tablaCompromisos(Request $request){
        return Datatables::of(Alertacartera::tablaCompromisos($request->all()))->make(true);
    }
    public function detalle(Request $request)
    {
        //Obligatorio ingresar cliente
        if (!$request->get('cu')) {
            flash('Cliente no seleccionado')->error();
            return redirect()->route('infinity.me.clientes');
        }
        $now = Carbon::now();
        $mesesBlindaje = 0;
        $datosCumplimiento = [];
        $revision = [];
        $esRiesgos=false;
        $rol = $this->user->getValue('_rol');
        $esRiesgosUltCamb=false;
        //Si encuentra el cliente
        if ($data = Cliente::getClienteInfinity($this->user, $request->get('cu', null))) {
            $visita = new Visita();
            //Se creará variable de gestiones para listarla aquí
            $revision = $visita ->getAreasRevision($rol);
            $dataseguimiento = Alertacartera::Seguimientodiario(array('cod_unico' => $request->get('cu', null)))->get()->first();
            $gestiones = Cliente::getListaGestiones($request->get('cu', null));
            $documentos = Cliente::getListaDocumentos($request->get('cu', null));
            $productosdiarios = Alertacartera::getProdDiario($request->get('cu', null));
            $fechaproductosdiarios = Alertacartera::getfechaSegActividad();
            $segmentoactividades = Alertacartera::getSegActividad($request->get('cu', null));
            $gestanteriores = MensajeSegVigentes::getAntGestiones($request->get('cu', null));
            $gestactual = MensajeSegVigentes::getGestionActual($request->get('cu', null));
            $ultimosmensajes = MensajeSegVigentes::getUltMensajes($request->get('cu', null));
            $getdatosCaso = null;
            if (count($gestactual) > 0) {
                $getdatosCaso = MensajeSegVigentes::getdatosCaso($gestactual[0]->ID);
                // dd($getdatosCaso);
            }
            $entidad = new FichaCovid();
            $privilegios = $visita -> getValidador($request->get('cu', null),$this->user->getValue('_registro'),$this->user->getValue('_rol'),null);
            $datoscovid = $entidad->getDatosByClienteAprobado($request->get('cu', null));
            $cambios=[];
            if(!empty($datoscovid)){
                $cambios = $entidad->getCambios($datoscovid->ID);
                $datosCumplimiento = $entidad->getDatosCumplimiento(isset($datoscovid)?$datoscovid->ID:null,$request->get('cu', null))->toArray();
                if($datosCumplimiento==null || empty($datosCumplimiento)){
                    $datosCumplimiento = [];
                }
            }
            // dd(!empty(Alertacartera::getDiasAtrasosmaximo($request->get('cu', null))) ? Alertacartera::getDiasAtrasosmaximo($request->get('cu', null)) : null);
            // dd(count($gestactual),$gestactual,$ultimosmensajes,$gestanteriores,$getdatosCaso);
            if($datoscovid!=null){
                $esRiesgosUltCamb = $entidad->ultimoCambioEstrategiaRiesgos($datoscovid->ID);
            }
            $esRiesgos = in_array($this->user->getValue('_rol'),array(
                Usuario::ROL_JEFE_RIESGOS,
                Usuario::ROL_ANALISTA_RIESGOS_ADMISION,
                Usuario::ROL_GERENTE_RIESGOS,
                Usuario::ROL_SUBGERENTE_RIESGOS,
                Usuario::ROL_ANALISTA_GYS,
                Usuario::ROL_SUBGERENTE_GYS
            ));
            $cambioEstrategia = $esRiesgos || (!$esRiesgos && !$esRiesgosUltCamb);
            $esJefe = in_array($this->user->getValue('_rol'), Usuario::getJefesGerentesBE());
            return view('be.infinity.detalle')
                ->with('usuario', Auth::user())
                ->with('cliente', $data)
                ->with('gestiones', $gestiones)
                ->with('cambioEstrategia',$cambioEstrategia)
                ->with('datoscovid', $datoscovid)
                ->with('cambios', $cambios)
                ->with('revision',$revision)
                ->with('privilegios',$privilegios)
                ->with('rol', $this->user->getValue('_rol'))
                ->with('now', $now)
                ->with('cumplimientos', $datosCumplimiento)
                ->with('documentos', $documentos)
                ->with('grupoEconomico', Cliente::getGrupoEconomicoCliente($request->get('cu', null)))
                ->with('politicas', Cliente::getHistoriaPoliticas($request->get('cu', null)))
                ->with('combopoliticas', Cliente::getHistoriaPoliticasSinGestionar($request->get('cu', null)))
                ->with('salidas', AlertaExplicacion::getSalidas($request->get('cu', null)))
                ->with('recalculo', Cliente::getRecalculo($request->get('cu', null)))
                ->with('meses', Cliente::getMeses())
                ->with('estados', Cliente::getEstadosGestion($esJefe))
                ->with('semaforos', Cliente::getHistoriaClientes($request->get('cu')))
                ->with('explicaciones', AlertaExplicacion::getByCliente($request->get('cu')))
                ->with('variables', VariableInformativa::getNegativos($request->get('cu')))
                ->with('fecha_actividad',$fechaproductosdiarios->FECHA_ACT)
                ->with('productosdiarios', $productosdiarios)
                ->with('segmentoactividades', $segmentoactividades)
                ->with('clienteseguimiento', $dataseguimiento)
                ->with('comboact', Alertacartera::CombosParaActividad('GESTION'))
                ->with('comboacc', Alertacartera::CombosParaActividad('ACCION'))
                ->with('combomotimp', Alertacartera::CombosParaActividad('MOTIVO_IMPAGO'))
                ->with('comborespon', Alertacartera::CombosParaActividad('RESPONSABLE', $this->user->getValue('_rol')))
                ->with('combocumpliorespon', Alertacartera::CombosParaActividad('CUMPLIO_COMPROMISO'))
                ->with('gestactual', $gestactual)
                ->with('gestanteriores', $gestanteriores)
                ->with('ultimosmensajes', $ultimosmensajes)
                ->with('datoscaso', $getdatosCaso)
                ->with('maxdatoatrasos', !empty(Alertacartera::getDiasAtrasosmaximo($request->get('cu', null))) ? Alertacartera::getDiasAtrasosmaximo($request->get('cu', null)) : null);
            //Si no encuentra a cliente
        } else {
            flash('Cliente ' . $request->get('cu') . ' no encontrado/disponible')->error();
            return redirect()->route('infinity.alertascartera');
        }
    }
    public function getCambios(Request $request){
        $id = $request->get('id');
        $entidad = new FichaCovid();
        return $entidad->getCambios($id);
    }
    public function getMensajesAntiguos(Request $request)
    {
        $id = $request->get('id');
        return MensajeSegVigentes::getMensajesAntiguos($id);
    }

    public function nuevomensaje(Request $request)
    {
        // dd($request->all());
        $entidad = new MensajeSegVigentes();
        $exito = 0;
        if ($entidad->nuevomensaje($request->all(), $this->user)) {
            $exito = 1;
            flash("Exito!")->success();
        } else {
            flash("Hubo un problema!")->error();
        }
        return back();
    }

    public function getIndicadores(Request $request)
    {
        $twebarbol = Alertacartera::getUsuarioTWarbol($this->user->getValue('_registro'));
        $data = Cliente::getListaClientesStats($this->user, $request->all(), !empty($twebarbol) && $twebarbol->TIPO_BANCA ? $twebarbol->TIPO_BANCA : 'MEDIANA EMPRESA');
        $data_2 = Cliente::getListaClientesStats_2($this->user, $request->all(), !empty($twebarbol) && $twebarbol->TIPO_BANCA ? $twebarbol->TIPO_BANCA : 'MEDIANA EMPRESA');
        // dd($data,$data_2);
        $datos['cartera'] = 0;
        $datos['clientesla'] = 0;
        $datos['gestionespendientes'] = 0;
        $datos['documentacioncompleta'] = 0;
        $datos['visitaspendientes'] = 0;
        $datos['recalculospendientes'] = 0;

        $datos['vencidosrcc'] = 0;
        $datos['vencidosibk'] = 0;

        $datos['verdela'] = 0;
        $datos['rojola'] = 0;
        $datos['ambarla'] = 0;
        $datos['negrola'] = 0;
        $datos['grisla'] = 0;

        $datos['verdecartera'] = 0;
        $datos['rojocartera'] = 0;
        $datos['ambarcartera'] = 0;
        $datos['negrocartera'] = 0;
        $datos['griscartera'] = 0;
        
        if (!empty($data_2)) {
            $datos['documentacioncompleta'] = $data_2['documentarion_completa'];
            $datos['visitaspendientes'] = $data_2['visita_check'];
        }
        
        foreach ($data as $key => $value) {
            $datos['cartera'] += $value->TOTAL;
            $datos['clientesla'] += $value->INFINITY;
            $datos['gestionespendientes'] += $value->GESTIONES_PENDIENTES;
            $datos['recalculospendientes'] += $value->RECALCULOS_PENDIENTES;
            $datos['vencidosrcc'] += $value->VENCIDO_RCC;
            $datos['vencidosibk'] += $value->VENCIDO_IBK;
        }
        $datos['cartera']=$datos['cartera']>0?$datos['cartera']:1;

        $datos['gestionespendientes'] = number_format($datos['gestionespendientes'] * 100 / $datos['cartera'], 0);
        $datos['documentacioncompleta'] = number_format($datos['documentacioncompleta'] * 100 / $datos['cartera'], 0);
        $datos['visitaspendientes'] = number_format($datos['visitaspendientes'] * 100 / $datos['cartera'], 0);
        $datos['recalculospendientes'] = number_format($datos['recalculospendientes'] * 100 / $datos['cartera'], 0);

        if (isset($data['1'])) {
            $datos['verdela'] += $data['1']->INFINITY;
            $datos['verdecartera'] += $data['1']->TOTAL;
        }
        if (isset($data['3'])) {
            $datos['rojola'] += $data['3']->INFINITY;
            $datos['rojocartera'] += $data['3']->TOTAL;
        }
        if (isset($data['2'])) {
            $datos['ambarla'] += $data['2']->INFINITY;
            $datos['ambarcartera'] += $data['2']->TOTAL;
        }
        if (isset($data['999'])) {
            $datos['negrola'] += $data['999']->INFINITY;
            $datos['negrocartera'] += $data['999']->TOTAL;
        }
        if (isset($data[''])) {
            $datos['grisla'] = 1;
            $datos['griscartera'] = 1;
        }
        // dd($data,$datos);
        return response()->json($datos);
    }

    public function vistoVideoGF(Request $request)
    {
        $entidad = new Alertacartera();
        return response()->json($entidad->guardarvistaVideoGF($request->all()) ? "exito" : "error");
    }

    public function buscarusuarioGF(Request $request)
    {
        $entidad = new Alertacartera();
        return response()->json($entidad->buscarusuarioGF($request->all()) ? false : true);
    }

    public function agregargestiongys(Request $request)
    {
        $entidad = new SegVigCasos();
        if ($entidad->agregargestiongys($request->all())) {
            flash('Se ha creado la gestión correctamente.')->success();
        } else {
            flash('No se ha podido crear la gestión. Inténtelo nuevamente.')->error();
        }
        return redirect()->route('infinity.alertascartera.detalle',['cu' => $request->get('codunico_gesgys',null)]);
    }
}
