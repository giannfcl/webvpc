<?php 
namespace App\Http\Controllers\BE\Infinity\GE;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entity\Usuario;
use App\Entity\Infinity\Cliente as Cliente;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use Validator;

class IndexController extends Controller{

	public function getListaClientes(Request $request){
    	return view('be.infinity.ge.clientes')
        ->with('stats',Cliente::getListaClientesStats($this->user))
        ->with('flgVisiblidadBanca',in_array($this->user->getValue('_rol'),[Usuario::ROL_GERENTE_DIVISION_BE])? 1 : 0)
        ->with('flgVisibilidadZonal',in_array($this->user->getValue('_rol'),[Usuario::ROL_GERENCIA_ZONAL_BE])? 1 : 0)
        ->with('flgVisiblidadJefatura',in_array($this->user->getValue('_rol'),[Usuario::ROL_JEFATURA_BE])? 1 : 0);
    }

    public function getListaClientesTable(Request $request){
    	return Datatables::of(Cliente::getListaClientesAlerta($this->user))->make(true);
    }


}