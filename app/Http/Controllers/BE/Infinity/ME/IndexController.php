<?php

namespace App\Http\Controllers\BE\Infinity\ME;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entity\Usuario;
use App\Entity\BE\ZonalJefatura as ZonalJefatura;
use App\Entity\Infinity\Cliente as Cliente;
use App\Entity\Infinity\FichaCovid as FichaCovid;
use App\Entity\Infinity\ConocemeCliente as Conoceme;
use App\Entity\Infinity\ConocemeDocumentacion as ConocemeDocumentacion;
use App\Entity\Infinity\AlertaExplicacion as AlertaExplicacion;
use App\Entity\Infinity\AlertaDocumentacion as AlertaDocumentacion;
use App\Entity\Infinity\ConocemeOpciones as ConocemeOpciones;
use App\Entity\Infinity\Resumen as Resumen;
use App\Entity\Infinity\Gestion as Gestion;
use App\Entity\Infinity\VariableInformativa as VariableInformativa;
use App\Entity\Infinity\PoliticaProceso as Politica;
use Yajra\Datatables\Datatables as Datatables;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Date\Date as Carbon;
use App\Entity\Infinity\Visita as Visita;
use App\Entity\Infinity\ClienteRevision as ClienteRevision;
use PDF;


use Validator;

class IndexController extends Controller {

    public function getListaClientes(Request $request) {
        return view('be.infinity.me.clientes')
                        ->with('stats', Cliente::getListaClientesStats($this->user,'MEDIANA EMPRESA'))
                        ->with('flgVisibilidadBanca', in_array($this->user->getValue('_rol'), [Usuario::ROL_GERENTE_DIVISION_BE]) ? 1 : 0)
                        ->with('flgVisibilidadZonal', in_array($this->user->getValue('_rol'), [Usuario::ROL_GERENCIA_ZONAL_BE,Usuario::ROL_RIESGOS_ME_ZONAL]) ? 1 : 0)
                        ->with('flgVisibilidadJefatura', in_array($this->user->getValue('_rol'), [Usuario::ROL_JEFATURA_BE]) ? 1 : 0)
                        ->with('flgVisibilidadGeneral', !in_array($this->user->getValue('_rol'), Usuario::getEjecutivosBE()) ? 1 : 0)
                        ->with('flgVisibilidadControl', in_array($this->user->getValue('_rol'), [Usuario::ROL_CONTROL_CREDITOS]) ? 1 : 0)
                        ->with('iconoAlertas',AlertaDocumentacion::getHTML())
                        ->with('registro',$this->user->getValue('_registro'))
                        ->with('GZBE',Usuario::getUsuario('B10238'))
                        ->with('GDBE',Usuario::getUsuario('B27462'))
                        ->with('ARBE',Usuario::getUsuario('B22777'))
                        ->with('JR',Usuario::getUsuario(array('B6674','B9804')))
                        ->with('SR',Usuario::getUsuario('B10813'))
                        ->with('GR',Usuario::getUsuario('B10304'));
    }
    
    public function getListaClientesGE(Request $request) {
        
        return view('be.infinity.me.clientes-ge')
                    ->with('stats', Cliente::getListaClientesStats($this->user))
                    ->with('flgVisibilidadBanca', in_array($this->user->getValue('_rol'), [Usuario::ROL_GERENTE_DIVISION_BE]) ? 1 : 0)
                    ->with('flgVisibilidadZonal', in_array($this->user->getValue('_rol'), [Usuario::ROL_GERENCIA_ZONAL_BE,Usuario::ROL_RIESGOS_ME_ZONAL]) ? 1 : 0)
                    ->with('flgVisibilidadJefatura', in_array($this->user->getValue('_rol'), [Usuario::ROL_JEFATURA_BE]) ? 1 : 0)
                    ->with('flgVisibilidadGeneral', !in_array($this->user->getValue('_rol'), Usuario::getEjecutivosBE()) ? 1 : 0)
                    ->with('flgVisibilidadControl', in_array($this->user->getValue('_rol'), [Usuario::ROL_CONTROL_CREDITOS]) ? 1 : 0)
                    ->with('iconoAlertas',AlertaDocumentacion::getHTML())
                    ->with('registro',$this->user->getValue('_registro'))
                    ->with('GZBE',Usuario::getUsuario('B10238'))
                    ->with('GDBE',Usuario::getUsuario('B27462'))
                    ->with('ARBE',Usuario::getUsuario('B22777'))
                    ->with('JR',Usuario::getUsuario(array('B6674','B9804')))
                    ->with('SR',Usuario::getUsuario('B11400'))
                    ->with('GR',Usuario::getUsuario('B10304'));
    }
    

    public function getListaClientesTable(Request $request) {
        
        return Datatables::of(Cliente::getListaClientesAlerta($this->user,'MEDIANA EMPRESA'))
                    ->addColumn('ALERTA_VISITA', function($row) {
                        return AlertaDocumentacion::getNivelAlerta(AlertaDocumentacion::ID_VISITA, $row->FECHA_ULTIMA_VISITA, $row->FLG_INFINITY,$row->FECHA_INGRESO_INFINITY);
                    })
                    ->addColumn('ALERTA_RECALCULO', function($row) {
                        return AlertaDocumentacion::getNivelAlerta(AlertaDocumentacion::ID_RECALCULO, $row->FECHA_ULTIMO_RECALCULO, $row->FLG_INFINITY,$row->FECHA_INGRESO_INFINITY);
                    })
                    ->addColumn('ALERTA_DOCUMENTACION', function($row) {
                        return AlertaDocumentacion::getNivelAlertaDocumentacionTotal($row->FLG_INFINITY
                                        , $row->FECHA_ULTIMO_EEFF_1
                                        , $row->FECHA_ULTIMO_EEFF_2
                                        , $row->FECHA_ULTIMO_IBR
                                        , $row->FECHA_ULTIMO_F02
                                        , $row->FECHA_INGRESO_INFINITY);
                    })
                    ->addColumn('TIMER',function($row){
                        if ($row->FECHA_VENCIMIENTO_POLITICA){
                            $hoy = Carbon::now();
                            $vencimiento = new Carbon($row->FECHA_VENCIMIENTO_POLITICA);
                            return $vencimiento->diffInDaysFiltered(function(Carbon $date) {
                                return true;
                            },$hoy,false);
                        }else{
                            return null;
                        }

                    })
                    ->make(true);
    }
    
    public function getListaClientesTableGE(Request $request) {
        
        return Datatables::of(Cliente::getListaClientesAlerta($this->user))
                ->addColumn('ALERTA_MONITORING', function($row) {
                    return $row->FLG_INFINITY;
                })
                ->addColumn('ALERTA_VISITA', function($row) {
                    return AlertaDocumentacion::getNivelAlerta(AlertaDocumentacion::ID_VISITA, $row->FECHA_ULTIMA_VISITA, $row->FLG_INFINITY,$row->FECHA_INGRESO_INFINITY);
                })
                ->addColumn('ALERTA_METODIZADO', function($row) {
                    return AlertaDocumentacion::getNivelAlerta(AlertaDocumentacion::ID_RECALCULO, $row->FECHA_ULTIMO_RECALCULO, $row->FLG_INFINITY,$row->FECHA_INGRESO_INFINITY);
                })
                ->addColumn('ALERTA_DOCUMENTACION', function($row) {
                    return AlertaDocumentacion::getNivelAlertaDocumentacionTotal($row->FLG_INFINITY
                                    , $row->FECHA_ULTIMO_EEFF_1
                                    , $row->FECHA_ULTIMO_EEFF_2
                                    , $row->FECHA_ULTIMO_IBR
                                    , $row->FECHA_ULTIMO_F02
                                    , $row->FECHA_INGRESO_INFINITY);
                })
                ->make(true);
    }

    public function resumen(Request $request) {

        $busqueda = [
            'jefatura' => $request->get('jefatura', $this->user->getValue('_centro')),
            'zonal' => $request->get('zonal', $this->user->getValue('_zona')),
            'flgInfinity' => [0, 1],
        ];

        $jefaturas = [];
        $zonales = [];

        if (in_array($this->user->getValue('_rol'), Usuario::getZonalesBE())) {
            $jefaturas = ZonalJefatura::getJefaturas($this->user->getValue('_zona'));
        } else if (in_array($this->user->getValue('_rol'), Usuario::getBanca())) {
            $zonales = ZonalJefatura::getZonales($this->user->getValue('_banca'));
            $jefaturas = ZonalJefatura::getJefaturas(null, $this->user->getValue('_banca'));
        } else {
            $zonales = ZonalJefatura::getZonales();
            $jefaturas = ZonalJefatura::getJefaturas();
        }

        $movimientos = Resumen::getMovimientos($busqueda);
        $migraciones = Resumen::getMigraciones($busqueda);
        $composicionCartera = Resumen::getComposicionCartera($busqueda);
        //dd($composicionCartera);
        $zonales = ZonalJefatura::getZonales('BE', 'BELZONAL2');

        return view('be.infinity.me.resumen')
                        ->with('movimientos', $movimientos)
                        ->with('migraciones', $migraciones)
                        ->with('composicionCartera', $composicionCartera)
                        ->with('busqueda', $busqueda)
                        ->with('jefaturas', $jefaturas)
                        ->with('zonales', $zonales)
                        ->with('usuario', $this->user);
    }

    public function resumenGraficarGestion(Request $request) {

        $busqueda = [
            'jefatura' => $request->get('jefatura', $this->user->getValue('_centro')),
            'zonal' => $request->get('zonal', $this->user->getValue('_zona')),
            'flgInfinity' => [0, 1],
        ];

        return Resumen::graficarGestion($busqueda);
    }

    public function resumenGraficarMora(Request $request) {

        $busqueda = [
            'jefatura' => $request->get('jefatura', $this->user->getValue('_centro')),
            'zonal' => $request->get('zonal', $this->user->getValue('_zona')),
            'flgInfinity' => [0, 1],
        ];

        return Resumen::graficarMora($busqueda);
    }

    public function resumenGraficarLineal(Request $request) {

        $busqueda = [
            'jefatura' => $request->get('jefatura', $this->user->getValue('_centro')),
            'zonal' => $request->get('zonal', $this->user->getValue('_zona')),
            'flgInfinity' => [0, 1],
        ];

        return Resumen::graficarLineal($busqueda);
    }

    public function detalle(Request $request) {

        //Obligatorio ingresar cliente
        if (!$request->get('cu')) {
            flash('Cliente no seleccionado')->error();
            return redirect()->route('infinity.me.clientes');
        }
        //Si encuentra el cliente
        if ($data = Cliente::getClienteInfinity($this->user, $request->get('cu', null))) {
            //Se creará variable de gestiones para listarla aquí
            $gestiones = Cliente::getListaGestiones($request->get('cu', null));
            $documentos = Cliente::getListaDocumentos($request->get('cu', null));
            $esJefe = in_array($this->user->getValue('_rol'), Usuario::getJefesGerentesBE());
            return view('be.infinity.me.detalle')
                        ->with('usuario', Auth::user())
                        ->with('cliente', $data)
                        ->with('gestiones', $gestiones)
                        ->with('documentos', $documentos)
                        ->with('grupoEconomico', Cliente::getGrupoEconomicoCliente($request->get('cu', null)))
                        ->with('politicas', Cliente::getHistoriaPoliticas($request->get('cu', null)))
                        ->with('combopoliticas', Cliente::getHistoriaPoliticasSinGestionar($request->get('cu', null)))
                        ->with('salidas', AlertaExplicacion::getSalidas($request->get('cu', null)))
                        ->with('recalculo', Cliente::getRecalculo($request->get('cu', null)))
                        ->with('meses', Cliente::getMeses())
                        ->with('estados', Cliente::getEstadosGestion($esJefe))
                        ->with('semaforos', Cliente::getHistoriaClientes($request->get('cu')))
                        ->with('explicaciones', AlertaExplicacion::getByCliente($request->get('cu')))
                        ->with('variables', VariableInformativa::getNegativos($request->get('cu')));
            //Si no encuentra a cliente
        } else {
            flash('Cliente ' . $request->get('cu') . ' no encontrado/disponible')->error();
            return redirect()->route('infinity.me.clientes');
        }
    }
    
    public function detalleGE(Request $request) {

        //Obligatorio ingresar cliente
        if (!$request->get('cu')) {
            flash('Cliente no seleccionado')->error();
            return redirect()->route('infinity.me.clientes');
        }
        
        //Si encuentra el cliente
        if ($data = Cliente::getClienteInfinity($this->user, $request->get('cu', null))) {
            //Se creará variable de gestiones para listarla aquí
            $gestiones = Cliente::getListaGestiones($request->get('cu', null));
            $documentos = Cliente::getListaDocumentos($request->get('cu', null));

            $esJefe = in_array($this->user->getValue('_rol'), Usuario::getJefesGerentesBE());

            return view('be.infinity.me.detalle-ge')
                        ->with('usuario', Auth::user())
                        ->with('cliente', $data)
                        ->with('gestiones', $gestiones)
                        ->with('documentos', $documentos)
                        ->with('grupoEconomico', Cliente::getGrupoEconomicoCliente($request->get('cu', null)))
                        ->with('politicas', Cliente::getHistoriaPoliticas($request->get('cu', null)))
                        ->with('combopoliticas', Cliente::getHistoriaPoliticasSinGestionar($request->get('cu', null)))
                        ->with('salidas', AlertaExplicacion::getSalidas($request->get('cu', null)))
                        ->with('recalculo', Cliente::getRecalculo($request->get('cu', null)))
                        ->with('meses', Cliente::getMeses())
                        ->with('estados', Cliente::getEstadosGestion($esJefe))
                        ->with('semaforos', Cliente::getHistoriaClientes($request->get('cu')))
                        ->with('explicaciones', AlertaExplicacion::getByCliente($request->get('cu')))
                        ->with('variables', VariableInformativa::getNegativos($request->get('cu')));
            //Si no encuentra a cliente
        } else {
            flash('Cliente ' . $request->get('cu') . ' no encontrado/disponible')->error();
            return redirect()->route('infinity.me.clientes');
        }
    }

    
    public function getHistoriaClientes(Request $request) {
        return Cliente::getHistoriaClientes($request->get('cu'));
    }

    public function guardarGestion(Request $request) {

        $adjunto = $request->file('adjuntoGestion');
        $gestion = new Gestion();
        $gestion->setValues([
            '_codUnico' => $request->get('codUnico'),
            '_registro' => $this->user->getValue('_registro'),
            '_fechaGestion' => new Carbon($request->get('fechaGestion', null)),
            '_PoliticaAsociada' => $request->get('PoliticaAsociada') ? Cliente::getIdPoliticaCliente($request->get('PoliticaAsociada'))->POLITICA_ID : null,
            '_comentario' => $request->get('comentarioGestion'),
            '_archivo' => $adjunto,
            '_extension' => $adjunto ? pathinfo($request->file('adjuntoGestion')->getClientOriginalName(), PATHINFO_EXTENSION) : null,
            '_estado' => $request->get('estadoGestion'),
            '_DecisionComite' => $request->get('DecisionComite'),
            '_Idpoliticacliente'=> $request->get('PoliticaAsociada',null),
        ]);

        if ($request->get('tiempo',null)) {
            $clirevision = new ClienteRevision();
            $clirevision->setValues([
                '_fechafin' => Carbon::now()->addMonths($request->get('tiempo',null)),
                '_codUnico' => $request->get('codUnico'),
                '_fecharegistro' => Carbon::now(),
                '_tiempo' => $request->get('tiempo',null),
                '_Idpoliticacliente' => $request->get('PoliticaAsociada',null),
                '_Idpolitica' => $request->get('PoliticaAsociada') ? Cliente::getIdPoliticaCliente($request->get('PoliticaAsociada'))->POLITICA_ID : null,
                '_flgvigente' => 1
            ]);
        }
        // dd($gestion,isset($clirevision) ? $clirevision : null);
        if ($gestion->registrar(isset($clirevision) ? $clirevision : null)) {
            flash('Se ha creado la gestión correctamente.')->success();
        } else {
            flash('No se ha podido crear la gestión. Inténtelo nuevamente.')->error();
        }
        return back();
    }

    public function guardarDocumentacion(Request $request) {

        $adjunto = $request->file('adjuntoDocumento');

        if ($adjunto == null) {
            flash('No ha seleccionado un archivo, inténtelo nuevamente')->error();
            return back();
        }

        $documento = new ConocemeDocumentacion();
        $documento->setValues([
            '_tipo' => $request->get('tipoDocumento'),
            '_codUnico' => $request->get('codUnico'),
            '_fechaDocumento' => $request->get('fechaFirma', 'NULL'),
            '_registro' => $this->user->getValue('_registro'),
            '_archivo' => $request->file('adjuntoDocumento'),
            '_extension' => pathinfo($request->file('adjuntoDocumento')->getClientOriginalName(), PATHINFO_EXTENSION)
        ]);

        if ($documento->registrar()) {
            flash('Se ha guardado el documento correctamente')->success();
        } else {
            flash('No se ha podido guardar el documento. Inténtelo nuevamente.')->error();
        }
        return back();
    }

    public function guardarRecalculo(Request $request) {

        $adjunto = $request->file('adjuntoRecalculo');

        if ($adjunto == null) {
            flash('No ha seleccionado un archivo, inténtelo nuevamente')->error();
            return back();
        }

        $documento = new ConocemeDocumentacion();
        $documento->setValues([
            '_tipo' => \App\Entity\Infinity\ConocemeDocumentacion::TIPO_MDL,
            '_codUnico' => $request->get('codUnico'),
            '_fechaDocumento' => $request->get('fechaRecalculo', 'NULL'),
            '_mdl' => $request->get('mdlRecalculo'),
            '_registro' => $this->user->getValue('_registro'),
            '_archivo' => $request->file('adjuntoRecalculo'),
            '_extension' => pathinfo($request->file('adjuntoRecalculo')->getClientOriginalName(), PATHINFO_EXTENSION)
        ]);

        if ($documento->registrar()) {
            flash('Se ha guardado el recálculo correctamente')->success();
        } else {
            flash('No se ha podido guardar el recálculo. Inténtelo nuevamente.')->error();
        }
        return back();
    }

    public function getIconoAlerta(Request $request) {
        return AlertaDocumentacion::getIcono($request->get('idDocumento'), $request->get('fechaUltimo'));
    }

    public function getClienteHistoricoTable(Request $request) {
        return Datatables::of(\App\Entity\Infinity\ConocemeCliente::getHistoriaByCliente($request->get('cliente')))->make(true);
    }
    
    public function visitaHistoria(Request $request){
        //jalamos data
        $id = $request->get('id');
        $conoceme = new Conoceme();
        $conoceme->getByHistoriaId($id);
        $visita = new Visita();
        $visita->getbyId($conoceme ->getValue ('_idVisita'));
        $idVisita = $conoceme->getValue('_idVisita');
        $cu = $conoceme->getValue('_codunico');
        return view('be.infinity.me.visita-historia')
            ->with('cliente',$conoceme)
            ->with('idHistorico', $id)
            ->with('visita',$visita)
            ->with('idVisita',$idVisita)
            ->with('cu',$cu)
            ->with('participaciones', \App\Entity\Infinity\ConocemeOpciones::getParticipacion())
            ->with('bancos', ConocemeOpciones::getBancos())
            ->with('rcc', $conoceme->getRcc());
    }
    
    public function conocemeHistoria(Request $request){
        
        $id = $request->get('id');
        $conoceme = new Conoceme();
        $conoceme->getByHistoriaId($id);
        
        // YA NO SE NECESITARIA DE LA VISITA
        $visita = new Visita();
        
    
    //dd($conoceme);

        return view('be.infinity.me.conoceme-historia')
            ->with('cliente',$conoceme)
            ->with('visita',$visita)
            ->with('participaciones', \App\Entity\Infinity\ConocemeOpciones::getParticipacion())
            ->with('bancos', ConocemeOpciones::getBancos())
            ->with('rcc', $conoceme->getRcc())
            ->with('id',$id);
    }

    public function PoliticasComiteCliente(Request $request)
    {
        $cu = $request->get('cu');
        $rmg = $request->get('rmg');
        $rating = $request->get('rating');
        $politicas_SE=Cliente::PoliticasComiteCliente($cu);
        $autonomia=Cliente::getUsuarioComite($rmg,$rating);
        $data['politica_se']=$politicas_SE;
        $data['autonomias']=$autonomia['autonomias'];
        $data['todos']=$autonomia['todos'];
        return $data;
    }

    public function PDF_conoceme(Request $request)
    {
        $id = $request->get('id');
        $conoceme = new Conoceme();
        $conoceme->getByHistoriaId($id);
        $cliente = $conoceme;
        $bancos =ConocemeOpciones::getBancos();
        $participaciones = \App\Entity\Infinity\ConocemeOpciones::getParticipacion();
        $rcc = $conoceme->getRcc();

        $pdf = PDF::loadView('be.infinity.me.conoceme-historia_pdf',compact('cliente','participaciones','bancos','rcc'));
  
        return $pdf->stream('Conoceme.pdf');
    }

    public function proceso_politicas()
    {
        $proceso=Cliente::proceso_politicas();
        return $proceso;
    }

    public function getVisitasConfirmadas()
    {
        $visitasConfirmadas=Cliente::getVisitasConfirmadas();
        return $visitasConfirmadas;
    }

}