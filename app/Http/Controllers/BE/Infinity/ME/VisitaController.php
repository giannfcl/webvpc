<?php

namespace App\Http\Controllers\BE\Infinity\Me;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entity\Usuario as Usuario;
use App\Entity\Infinity\Cliente as Cliente;
use App\Entity\Infinity\ConocemeCliente as Conoceme;
use App\Entity\Infinity\ConocemeOpciones as ConocemeOpciones;
use App\Entity\Infinity\ConocemeStakeholder as ConocemeStakeholder;
use App\Entity\Infinity\ConocemeLinea as ConocemeLinea;
use App\Entity\Infinity\ConocemeZona as ConocemeZona;
use App\Entity\Infinity\ConocemeMixVentas as ConocemeMixVentas;
use App\Entity\Infinity\ConocemeInversiones as ConocemeInversiones;
use App\Entity\Infinity\Visita as Visita;
use App\Model\Infinity\Visita as mVisita;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Storage;
use App\Entity\Infinity\Cliente as eCliente;
use Validator;
use Jenssegers\Date\Date as Carbon;
use App\Entity\Infinity\FichaCovid as FichaCovid;

class VisitaController extends Controller {

    public function index(Request $request) {
        $condicion = false;
        $revision = [];
        $mesesBlindaje = 0;
        $bucket = 0;
        $datoscovid = null;
        $rolesvalidadores = [];
        $datosCumplimiento = [];
        //Conexion a la entidad de Ficha Covid
        $entidad = new FichaCovid();
        $now = Carbon::now();
        //Visita vacia
        $visita = new Visita();
        //Conoceme vacio
        $conoceme = new Conoceme();
        //i es que es cu o codunico el parámetro
        $cu = ($request->get('cu', null)==null)?$request->get('codunico', null):$request->get('cu', null);
        //Se comprueba si la redireccion es por un error
        $error = $request->get('error');
        $rol = $this->user->getValue('_rol');
        $flgNotificado = $request->get('flg_notificado', null);
        
        if($error=='1'){
            $res = $entidad->save2($request->all(),$this->user->getValue('_registro'),true,null);
            $datoscovid = (object) $res['covid'];
            $datosCumplimiento = array_map(function($element) {
                return (object) $element;
            }, $res['cumplimiento']);
            $datosTransformados = $this->transformarDatos($request);
            //Conoceme Actualizado
            $conoceme = $datosTransformados['conoceme'];
            //Datos de la visita actual
            $visita = $datosTransformados['visita'];
            $entidadVisita = new Visita();
            $condicion = $entidadVisita->getUltimaVisita($request->get('codunico')) && $entidadVisita->getValue('_revisionFecha') == null;
        }else{
            //Verificar si hay alguna visita
            if($visita->getUltimaVisita($cu)){
                //Hay una visita guardada => No tener fecha de revision 
                $condicion = $visita->getUltimaVisita($cu) && $visita->getValue('_revisionFecha') == null;
                //Última Ficha Covid
                $datoscovid = $entidad->getDatosByCliente2($visita->getValue('_id'));
            }
            if($condicion){
                //Si es que esta por aprobar la ficha se necesita las modificaciones al conoceme
                $conoceme->getByHistoriaVisita($visita->getValue('_id'));
                $datosCumplimiento = $entidad->getDatosCumplimiento(isset($datoscovid)?$datoscovid->ID:null,$cu)->toArray();
                if($datosCumplimiento==null || empty($datosCumplimiento)){
                    $datosCumplimiento = [];
                }
            }else{
                //Si no tiene una ficha por aprobar se blanquea. Si es que no tiene un conoceme se redirige al conoceme.
                $datosCumplimiento = $entidad->getDatosCumplimientoIncompletos(isset($datoscovid)?$datoscovid->ID:null,$cu)->toArray();
                if($datosCumplimiento==null || empty($datosCumplimiento)){
                    $datosCumplimiento = [];
                }
                
                $visita = new Visita();
                if (!$conoceme->getByCodunico($cu)) {
                    flash('Cliente: ' . $request->get('cu') . ' no encontrado/disponible')->error();
                    return redirect()->route('infinity.me.clientes');
                }
                if (!$conoceme->getValue('_codunicoConoceme')) {
                    flash('Primero debes completar la ficha conóceme antes de ingresar una visita')->error();
                    return redirect()->route('infinity.me.cliente.conoceme', ['cu' => $request->get('cu', null)]);
                }    
                
            }
        }    
        
        $entity = new Visita();
        //Permisos de Lectura/Escritura/Aprobacion 
        $privilegios = $entity -> getValidador($cu,$this->user->getValue('_registro'),$this->user->getValue('_rol'),$datoscovid);

        $rolesvalidadores = $entity -> getValidadores($this->user->getValue('_registro'),$cu,$datoscovid);
        if($privilegios==mVisita::LLENADOR_BLINDAJE){
            $meses = $datoscovid->PREGUNTA_14_5;
            $fechallenado =  new Carbon($datoscovid->FECHA_CREACION);
            $mesesBlindaje = $now->diffInMonths($fechallenado);
            $mesesBlindaje = $meses - $mesesBlindaje;
        }
        $revision = $entity ->getAreasRevision($rol);
        if($rolesvalidadores==null){
            $segmento = 'Otro';
        }else{
            $segmento = $rolesvalidadores['segmento']!=null?$rolesvalidadores['segmento']:'Otro';
        }
        return view('be.infinity.me.visita')
                        ->with('mesesBlindaje',$mesesBlindaje)
                        //INFO DEL ULTIMO CONOCEME O DE LA MODIFICACION DEL CONOCEME EN LA ULTIMA VISITA
                        ->with('cliente', $conoceme)
                        //GRADO DE PRIVILEGIOS DE CAMBIO
                        ->with('privilegios',$privilegios)
                        //VISITA A ESPERA DE APROBAR
                        ->with('flgGuardado',$condicion?"1":"0")
                        //INFO DE LA ULTIMA VISITA
                        ->with('visita', $visita)
                        //INFO DEL USUARIO
                        ->with('usuario', $this->user)
                        //VIENE DE UNA NOTIFICACION
                        ->with('flgNotificado', $flgNotificado)
                        //FICHA COVID
                        ->with('datoscovid',$datoscovid)
                        //FICHA COVID - CUMPLIMIENTOS
                        ->with('cumplimientos',$datosCumplimiento)
                        ->with('segmento',$segmento)
                        ->with('rolesvalidadores',$rolesvalidadores['validadores'])
                        ->with('bucket',$rolesvalidadores['bucket'])
                        //ETC
                        ->with('revision',$revision)
                        ->with('vistaHistorica',0)
                        ->with('now',$now)
                        ->with('infoCliente', Cliente::getClienteInfinity($conoceme->getValue('_ejecutivoSectorista'), $cu))
                        ->with('actividades', ConocemeOpciones::getActividades())
                        ->with('subsectores', ConocemeOpciones::getSubsectores())
                        ->with('backlog', ConocemeOpciones::getBacklogs())
                        ->with('participacion', ConocemeOpciones::getParticipacion())
                        ->with('bancos', ConocemeOpciones::getBancos())
                        ->with('tiposGarantia', ConocemeOpciones::getTiposGarantia())
                        ->with('gestionesCompra', ConocemeOpciones::getGestionesCompra())
                        ->with('financieroRol', ConocemeOpciones::getFinancieroRol())
                        ->with('zonas', ConocemeOpciones::getZonas())
                        ->with('inversiones', ConocemeOpciones::getInversiones())
                        ->with('cargosEntrevistado', ConocemeOpciones::getCargosEntrevistado())
                        ->with('rcc', $conoceme->getRcc());
    }

    public function transformarDatos($request){
        //Se obtiene el ultimo conoceme
        $conoceme = new Conoceme();
        $conoceme->getByCodunico($request->get('codunico'));
        //Se hace una copia
        $original = clone $conoceme;
        //Se hace una visita vacia
        $visita = new Visita();
        //Se llena la visita vacia con los valores nuevos
        $visita->setValues([
            '_codunico' => $request->get('codunico'),
            '_flagCambioMixVentas' => $request->get('flagCambioMixVentas') == 'on' ? 1 : 0,
            '_flagCambioProcesoIntegracion' => $request->get('flagCambioProcesoIntegracion') == 'on' ? 1 : 0,
            '_flagCambioConcentracionVentas' => $request->get('flagCambioConcentracionVentas') == 'on' ? 1 : 0,
            '_flagCambioConcentracionProveedores' => $request->get('flagCambioConcentracionProveedores') == 'on' ? 1 : 0,
            '_flagCambioOperaciones' => $request->get('flagCambioOperaciones') == 'on' ? 1 : 0,
            '_flagCambioZonaClientes' => $request->get('flagCambioZonaClientes') == 'on' ? 1 : 0,
            '_flagCambioGerenciaGeneral' => $request->get('flagCambioGerenciaGeneral') == 'on' ? 1 : 0,
            '_flagCambioGestionFinanciera' => $request->get('flagCambioGestionFinanciera') == 'on' ? 1 : 0,
            '_flagCambioAccionistas' => $request->get('flagCambioAccionistas') == 'on' ? 1 : 0,
            '_flagCambioLineas' => $request->get('flagCambioLineas') == 'on' ? 1 : 0,
            '_flagCambioInversionActivoPatrimonio' => $request->get('flagCambioInversionActivoPatrimonio'),
            '_flagCambioPrestamoDesvio' => $request->get('flagCambioPrestamoDesvio'),
            '_flagCambioBacklog' => $request->get('flagBacklog') == 'on' ? 1 : 0,
            '_entrevistadoCargo' => $request->get('cargoVisita'),
            '_entrevistadoNombre' => $request->get('nombreVisita'),
            '_comentarios' => $request->get('comentarioVisita'),
            '_comentariosRV' => $request->get('comentarioVisitaRV'),
            '_fechaVisita' => $request->get('fechaVisita') ? new Carbon($request->get('fechaVisita')) : null,
            '_flagCambioModeloNegocio' => $request->get('flagCambioModelo') == 'on' ? 1 : 0,
            '_registroObj' => $this->user
        ]);
        //Se actualiza el conoceme con los nuevos valores
        $conoceme->setValues([
            '_montoLineaProveedores' => $request->get('montoLineaProveedores', 'NULL') ? str_replace(",", "", $request->get('montoLineaProveedores', 'NULL')) : null,
            '_proyeccionInversion' => $request->get('proyeccionInversion', 'NULL') ? str_replace(",", "", $request->get('proyeccionInversion', 'NULL')) : null,
            '_proyeccionVentas' => $request->get('proyeccionVentas', 'NULL') ? str_replace(",", "", $request->get('proyeccionVentas', 'NULL')) : null,
            '_activoLibreGravamen' => $request->get('flgGravamen') == 'on' ? 1 : 0,
            '_lineaSucesion' => $request->get('flgLineaSucesion') == 'on' ? 1 : 0,
            '_idHistorico' => $request->get('idHistorico'),
            '_cambioGerenciaGeneralAnnio' => $request->get('cambioGerenciaGeneralAnnio'),
            '_cambioAccionistasAnnio' => $request->get('cambioAccionistasAnnio')
        ]);

        //Cambios en Inversion
        if ($request->get('flagCambioInversionActivoPatrimonio') == '1') {
            $visita->setValue('_cambioInversionActivoPatrimonio', $request->get('cambioInversionActivoPatrimonio', 'NULL'));
        }

        //Cambios en Prestamos y desvios
        if ($request->get('flagCambioPrestamoDesvio') == '1') {
            $visita->setValue('_cambioPrestamoDesvio', $request->get('cambioPrestamoDesvio', 'NULL'));
        }

        //Cambios en modelo de negocio
        if ($request->get('flagCambioModelo') == 'on') {
            $conoceme->setValue('_modeloNegocio', $request->get('modeloNegocio', 'NULL'));
            $conoceme->setValue('_ventajaCompetitiva', $request->get('ventajaCompetitiva', 'NULL'));
            $conoceme->setValue('_fortalezasRiesgos', $request->get('fortalezasRiesgos', 'NULL'));
        }

        //Cambios en concentración de Ventas
        if ($request->get('flagCambioConcentracionVentas') == 'on') {
            //Carga de clientes
            $clientes = [];
            $archivos_cli=[];
            if ($request->file('cliente')) {
                $archivos_cli = $request->file('cliente');
            }
            foreach ($request->get('cliente', []) as $key => $cliente) {
                //dd($cliente);
                if (isset($cliente['documento']) || isset($cliente['nombre'])) {
                    if (isset($archivos_cli[$key])) {
                        $cliente['archivoAdjunto']=$archivos_cli[$key];
                    }
                    $stakeholder = new ConocemeStakeholder();
                    $stakeholder->setValues([
                        '_codunico' => $request->get('codunico'),
                        '_tipo' => ConocemeStakeholder::TIPO_CLIENTE,
                        '_nombre' => isset($cliente['nombre'])?$cliente['nombre']:null,
                        '_documento' => isset($cliente['documento'])?$cliente['documento']:null,
                        '_concentracion' => isset($cliente['participacion'])?$cliente['participacion']:null,
                        '_desde' => isset($cliente['desde'])?$cliente['desde']:null,
                        '_contratoAdjunto'=>isset($cliente['archivoAdjunto']) ? $cliente['archivoAdjunto'] : null,
                        '_flgContrato' => isset($cliente['flgContrato']) ? 1 : 0,
                        '_contratofechaVencimiento' => isset($cliente['contratofechaVencimiento']) ? $cliente['contratofechaVencimiento'] : null,
                    ]);
                    $clientes[] = $stakeholder;
                }
            }
            $conoceme->setValue('_clientes', $clientes);
        } else {
            //Se agrega la data de contrato a lo ya existente
            foreach ($conoceme->getValue('_clientes') as $key => $cliente) {
                $cliente->setValues([
                    '_flgContrato' => isset($request->get('cliente')[$key]['flgContrato']) ? 1 : '0',
                    '_contratofechaVencimiento' => isset($request->get('cliente')[$key]['contratofechaVencimiento']) ? $request->get('cliente')[$key]['contratofechaVencimiento'] : '',
                ]);
            }
        }

        //Cambios en concentración de Proveedores
        if ($request->get('flagCambioConcentracionProveedores') == 'on') {
            $proveedores = [];
            $archivos_pro=[];
            if ($request->file('proveedor')) {
                $archivos_pro = $request->file('proveedor');
            }
            foreach ($request->get('proveedor', []) as $key => $proveedor) {
                if (isset($archivos_pro[$key])) {
                    $proveedor['archivoAdjunto']=$archivos_pro[$key];
                }
                //if (!is_null($proveedor['documento']) || !is_null($proveedor['nombre'])) {
                if(isset($proveedor['nombre'])){
                    if (isset($proveedor['nombre'])) {
                        $stakeholder = new ConocemeStakeholder();
                        $stakeholder->setValues([
                            '_codunico' => $request->get('codunico'),
                            '_tipo' => ConocemeStakeholder::TIPO_PROVEEDOR,
                            '_nombre' => $proveedor['nombre'],
                            '_documento' => !empty($proveedor['documento']) ? $proveedor['documento'] : null,
                            '_concentracion' => isset($proveedor['participacion'])?$proveedor['participacion']:null,
                            '_desde' => isset($proveedor['desde'])?$proveedor['desde']:null,
                            '_contratoAdjunto'=>isset($proveedor['archivoAdjunto']) ? $proveedor['archivoAdjunto'] : null,
                            '_exclusividad' => isset($proveedor['exclusividad']) ? 1 : 0,
                            '_flgContrato' => isset($proveedor['flgContrato']) ? 1 : 0,
                            '_contratofechaVencimiento' => isset($proveedor['contratofechaVencimiento']) ? $proveedor['contratofechaVencimiento'] : null,
                        ]);
                        $proveedores[] = $stakeholder;
                    }
                }
            }
            $conoceme->setValue('_proveedores', $proveedores);
        } else {
            foreach ($conoceme->getValue('_proveedores') as $key => $proveedor) {
                $proveedor->setValues([
                    '_flgContrato' => isset($request->get('proveedor')[$key]['flgContrato']) ? 1 : '0',
                    '_contratofechaVencimiento' => isset($request->get('proveedor')[$key]['contratofechaVencimiento']) ? $request->get('proveedor')[$key]['contratofechaVencimiento'] : '',
                ]);
            }
        }

        //Cambio de Gerencia General
        if ($request->get('flagCambioGerenciaGeneral') == 'on') {
            $conoceme->setValue('_gerenteGeneral', $request->get('gerenteGeneral', 'NULL'));
        }

        //Cambio de Gerencia Financiera
        if ($request->get('flagCambioGestionFinanciera') == 'on') {
            $conoceme->setValue('_financieroRol', $request->get('financieroRol', 'NULL'));
            $conoceme->setValue('_financieroNombre', $request->get('financieroNombre', 'NULL'));
            $conoceme->setValue('_tipoContabilidad', $request->get('tipoContabilidad', 'NULL'));
        }

        //Cambio de Accionistas
        if ($request->get('flagCambioAccionistas') == 'on') {
            //Carga de accionistas
            $accionistas = [];
            foreach ($request->get('accionista', []) as $accionista) {
                if (isset($accionista['documento']) || isset($accionista['nombre'])) {
                    $stakeholder = new ConocemeStakeholder();
                    $stakeholder->setValues([
                        '_codunico' => $request->get('codunico'),
                        '_tipo' => ConocemeStakeholder::TIPO_ACCIONISTA,
                        '_nombre' => isset($accionista['nombre'])?$accionista['nombre']:null,
                        '_documento' => isset($accionista['documento'])?$accionista['documento']:null,
                        '_concentracion' => isset($accionista['participacion']) ? 1 : 0,
                        '_nacimiento' => isset($accionista['nacimiento'])?$accionista['nacimiento']:null,
                    ]);
                    $accionistas[] = $stakeholder;
                }
            }
            $conoceme->setValue('_accionistas', $accionistas);
        }


        //Cambio en Lineas
        if ($request->get('flagCambioLineas') == 'on') {
            //Carga de lineas
            $lineas = [];
            foreach ($request->get('linea', []) as $key => $linea) {
                if ($linea['monto'] !== null || isset($linea['tipoGarantia'])) {
                    $objLinea = new ConocemeLinea();
                    $objLinea->setValues([
                        '_codunico' => $request->get('codunico'),
                        '_banco' => $key,
                        '_linea' => $linea['monto'],
                        '_tipoGarantia' => $linea['tipoGarantia'],
                    ]);
                    $lineas[] = $objLinea;
                }
            }
            $conoceme->setValue('_lineas', $lineas);
        }

        //Cambio Zona Operacion
        if ($request->get('flagCambioOperaciones') == 'on') {
            $zonasOperacion = [];
            foreach ($request->get('zonaOperacion', []) as $zonaOperacion) {
                $objCZona = new ConocemeZona();
                $objCZona->setValues([
                    '_codunico' => $request->get('codunico'),
                    '_tipo' => ConocemeZona::TIPO_OPERACIONES,
                    '_zona' => $zonaOperacion,
                ]);
                $zonasOperacion[] = $objCZona;
            }
            $conoceme->setValue('_zonaOperaciones', $zonasOperacion);
        }

        //Carga de Zona Clientes
        if ($request->get('flagCambioZonaClientes') == 'on') {
            $zonasCliente = [];
            foreach ($request->get('zonaCliente', []) as $zonaCliente) {
                $objCZona = new ConocemeZona();
                $objCZona->setValues([
                    '_codunico' => $request->get('codunico'),
                    '_tipo' => ConocemeZona::TIPO_CLIENTES,
                    '_zona' => $zonaCliente,
                ]);
                $zonasCliente[] = $objCZona;
            }
            $conoceme->setValue('_zonaClientes', $zonasCliente);
        }

        //Carga de Mix de Ventas
        if ($request->get('flagCambioMixVentas') == 'on') {
            $mixVentas = [];
            foreach ($request->get('mixVenta', []) as $mixVenta) {
                if (isset($mixVenta['productoServicio'])) {
                    $stakeholder = new ConocemeMixVentas();
                    $stakeholder->setValues([
                        '_codunico' => $request->get('codunico'),
                        '_productoServicio' => $mixVenta['productoServicio'],
                        '_participacion' => $mixVenta['participacion'],
                    ]);
                    $mixVentas[] = $stakeholder;
                }
            }
            $conoceme->setValue('_mixVentas', $mixVentas);
        }

        //Carga de Inversiones
        //dd($cu = $request->get('inversionCliente'));
        if ($request->get('proyeccionInversion',0) > 0) {
            $inversiones = [];
            //dd($request->get('inversionCliente'));
            if (!empty($request->get('inversionCliente'))) {
                foreach ($request->get('inversionCliente') as $elem) {

                    $inversion = new ConocemeInversiones();
                    $inversion->setValues([
                        '_codunico' => $request->get('codunico'),
                        '_tipoInversion' => $elem,            
                    ]);    
                    $inversiones[] = $inversion;
                }

                $conoceme->setValue('_inversiones', $inversiones);
            }
        }
        return array('conoceme'=>$conoceme,'original'=>$original,'visita'=>$visita);
    }

    public function guardar(Request $request) {
        $entidad = new FichaCovid();
        $visita = new Visita();
        $nombreUsuario = $this->user->getValue('_nombre');
        //Ultima Visita
        $visita->getUltimaVisita($request->get('codunico'));
        //Ultima Ficha covid
        $datoscovid = $entidad->getDatosByCliente2($visita->getValue('_id'));
        //Permisos de Lectura/Escritura/Aprobacion 
        $privilegios = $visita -> getValidador($request->get('codunico'),$this->user->getValue('_registro'),$this->user->getValue('_rol'),$datoscovid);
        //Se obtiene los datos en un array
        $data = $request->all();
        //Validacion para guardar FALSE/aprobar TRUE
        $validadorPost = $visita -> getValidadorPOST($data,$this->user->getValue('_registro'),$this->user->getValue('_rol'),$privilegios);
        $responsePost = $validadorPost['response'];
        $alertasCuali = $validadorPost['alertas'];
        //Transformar datos en el formato para que se pueda guardar
        $datosTransformados = $this->transformarDatos($request);
        //Conoceme Actualizado
        $conoceme = $datosTransformados['conoceme'];
        //Ultimo Conoceme
        $original = $datosTransformados['original'];
        //Datos de la visita actual
        $visita = $datosTransformados['visita'];
        $result = false;
        $registro=$this->user->getValue('_registro');
        $entidadVisita = new Visita();
        $flgGuardado = $entidadVisita->getUltimaVisita($request->get('codunico')) && $entidadVisita->getValue('_revisionFecha') == null;
        //Ultima ficha registrada esta aprobada pero esta volviendo a mandar para ser aprobada
        if(!$flgGuardado && $request->get('flgGuardado')=='1'){
            flash("Esta ficha ya se aprobó.")->error();
            return redirect()->route('infinity.me.cliente.visita', ['cu'=>$request->get('codunico')]);
        }
        if($flgGuardado && $request->get('flgGuardado')=='0'){
            flash("Esta ficha ya se guardó.")->error();
            return redirect()->route('infinity.me.cliente.visita',['cu'=>$request->get('codunico')]);
        }

        //SE EVALUAN LOS ESCENARIOS
        if ($responsePost) {
            //SE APRUEBA
            if($flgGuardado){
                //SE ESTA APROBANDO ALGO QUE YA ESTABA GUARDADO
                $visita->setValues([
                    '_registro' =>$request->get('registro',null) ,
                    '_id' => $request->get('visita'),
                    '_revisionRegistro' => $registro,
                ]);
                $result = $visita->confirmar($conoceme, $original,$request->get('visita'),$request->get('idHistorico'),$data,$registro,$alertasCuali,$nombreUsuario);
                if($result){
                    flash("Se aprobó con Exito!")->success();
                    return redirect()->route('infinity.alertascartera');
                }else{
                    flash("Hubo un problema")->error();
                    $request->merge(["error"=>"1"]);
                    return $this->index($request);
                }
            }else{
                //SE ESTA AGUARDANDO Y APROBANDO AL MISMO TIEMPO
                $visita->setValues([
                    '_registro' => $registro,
                    '_revisionRegistro' => $registro,
                ]);
                $conoceme->setValues([
                    '_registro' => $registro,
                ]);
                $result = $visita->registrar($conoceme,$data,$registro,$alertasCuali,$nombreUsuario,false);
                $idVisita = $result[0];
                $historiaId = $result[1];
                $original = clone $conoceme;
                $result = $visita->confirmar($conoceme, $original,$idVisita,$historiaId,$data,$registro,$alertasCuali,$nombreUsuario);
                if($result){
                    flash("Se aprobó con Exito!")->success();
                    return redirect()->route('infinity.alertascartera');
                }else{
                    flash("Hubo un problema")->error();
                    $request->merge(["error"=>"1"]);
                    return $this->index($request);
                }
           }
        } else {
            //SE GUARDA
            if(!$flgGuardado){
                $visita->setValues([
                    '_registro' => $this->user->getValue('_registro'),
                ]);
                $conoceme->setValues([
                    '_registro' => $this->user->getValue('_registro'),
                ]);
                $result = $visita->registrar($conoceme,$data,$registro,$alertasCuali,$nombreUsuario);
                if($result!=null){
                    flash("Se guardó con Exito! La ficha esta a espera de ser validada")->success();
                    return redirect()->route('infinity.me.cliente.visita',['cu'=>$request->get('codunico')]);
                }else{
                    flash("Hubo un problema")->error();
                    $request->merge(["error"=>"1"]);
                    return $this->index($request);
                }
            }else{
                flash("Esta tratando de cambiar una ficha que esta guardada. Debe comunicarse con el validador correspondiente")->error();
                return redirect()->route('infinity.me.cliente.visita',['cu'=>$request->get('codunico')]);
            }
        }
        return $result;
    }

    
    
    public function deleteVisita($id){
        $visita = new Visita();
        $visita->deleteVisita($id);
    }
    public function getVisitas(Request $request) {
        return Datatables::of(Visita::getVisitas($request->get('cu', null)))->make(true);
    }

}
