<?php

namespace App\Http\Controllers\BE\Infinity\Me;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entity\Infinity\FichaCovid as FichaCovid;
use App\Entity\Infinity\CLiente as Cliente;
use App\Entity\Usuario as Usuario;


class CovidController extends Controller {

    ##Función que carga la vista y la información necesaria para mostrar.##
    public function index(Request $request) {

        $cu = $request->get('cu', null);
        if (!empty($cu)) {
            $entidad = new FichaCovid();
            $datoscovid = $entidad->getDatosByCliente($cu);
            
            if (!empty($datoscovid)) {
                return view('be.infinity.accion-formulario.covid')
                        ->with('datoscovid',$datoscovid)
                        ->with('seguardo',1)
                        ->with('code', $cu)
                        ->with('cliente', Cliente::getClienteInfinity($this->user,$cu))
                        ->with('username', $this->user->getValue('_nombre'))
                        ->with('puedeguardar',in_array($this->user->getValue('_rol'), Usuario::getUsuariosCovidguardar()));
            }
        }
        return view('be.infinity.accion-formulario.covid')
            ->with('code', $cu)
            ->with('seguardo',0)
            ->with('cliente', Cliente::getClienteInfinity($this->user,$cu))
            ->with('username', $this->user->getValue('_nombre'))
            ->with('puedeguardar',in_array($this->user->getValue('_rol'), Usuario::getUsuariosCovidguardar()));
    }

    ##Función que guarda los datos de la vista##
    public function save(Request $request) {
        $ficha = new FichaCovid();
        if ($ficha->save($request->all(),$this->user)) {
            flash("Se guardo con Exito!")->success();
        }else{
            flash("Hubo un problema")->error();
        }
        return redirect()->route('infinity.me.cliente.covid', ['cu' => $request->get('codunico')]);
    }
}