<?php

namespace App\Http\Controllers\BE\Infinity\Me;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entity\Infinity\FichaCovid as FichaCovid;
use App\Entity\Infinity\CLiente as Cliente;
use App\Entity\Usuario as Usuario;
use Jenssegers\Date\Date as Carbon;
use App\Entity\Infinity\Visita as Visita;
use App\Entity\Infinity\Cliente as eCliente;

class CovidController2 extends Controller {

    public function historia(Request $request){
        $cu = $request->get('cu', null);
        $id = $request->get('id', null);
        $idHistorica = $request->get('idHistorico', null);
        $now = Carbon::now();
        if($cu==null){
            $cu = $request->get('codunico', null);
        }
        $entidad = new FichaCovid();
        $datoscovid = $entidad->getDatosByCliente2byID($id);
        $datosCumplimiento = $entidad->getDatosCumplimientobyID($id)->toArray();
        $visita = new Visita();
        $privilegios = $visita -> getValidador($cu,$this->user->getValue('_registro'),$this->user->getValue('_rol'),$datoscovid);
        if($datosCumplimiento==null || empty($datosCumplimiento)){
            $datosCumplimiento = [];
        }
        $datosCliente = eCliente::getDatosClienteVISITAMECOVID($cu);
        if($datosCliente==null){
            $segmento = 'Otro';
        }else{
            $segmento = $datosCliente->SEGMENTO;
        }
        $usuario = new Usuario();
        $rolvalidador = $usuario ->getNombreByRegistro($datoscovid->USUARIO_MODIFICACION);
        return view('be.infinity.accion-formulario.covid2')
            ->with('datoscovid',$datoscovid)
            ->with('request',$request)
            ->with('cumplimientos',$datosCumplimiento)
            ->with('privilegios',$privilegios)
            ->with('segmento',$segmento)
            ->with('idHistorica',$idHistorica)
            ->with('now',$now)
            ->with('code', $cu)
            ->with('cliente', Cliente::getClienteInfinity($this->user,$cu)) // nombre de la empresa
            ->with('rolvalidador', $rolvalidador!=null?$rolvalidador->NOMBRE:''); //nombre del usuario loggeado
    }
}