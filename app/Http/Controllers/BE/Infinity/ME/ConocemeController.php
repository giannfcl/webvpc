<?php

namespace App\Http\Controllers\BE\Infinity\Me;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entity\Usuario;
use App\Entity\Infinity\ConocemeCliente as Conoceme;
use App\Entity\Infinity\ConocemeOpciones as ConocemeOpciones;
use App\Entity\Infinity\ConocemeStakeholder as ConocemeStakeholder;
use App\Entity\Infinity\ConocemeLinea as ConocemeLinea;
use App\Entity\Infinity\ConocemeZona as ConocemeZona;
use App\Entity\Infinity\ConocemeCommoditie as ConocemeCommoditie;
use App\Entity\Infinity\ConocemeCanalVentas as ConocemeCanalVentas;
use App\Entity\Infinity\ConocemeMixVentas as ConocemeMixVentas;
use App\Entity\Infinity\AlertaExplicacion as AlertaExplicacion;
use Illuminate\Support\Facades\Auth;
use Validator;

class ConocemeController extends Controller {

    public function index(Request $request) {

        $conoceme = new Conoceme();
        $cu = $request->get('cu', null);
        if ($conoceme->getByCodunico($cu)) {
            return view('be.infinity.me.conoceme')
                            ->with('cliente', $conoceme)
                            ->with('actividades', ConocemeOpciones::getActividades())
                            ->with('subsectores', ConocemeOpciones::getSubsectores())
                            ->with('backlog', ConocemeOpciones::getBacklogs())
                            ->with('participacion', ConocemeOpciones::getParticipacion())
                            ->with('bancos', ConocemeOpciones::getBancos())
                            ->with('tiposGarantia', ConocemeOpciones::getTiposGarantia())
                            ->with('gestionesCompra', ConocemeOpciones::getGestionesCompra())
                            ->with('financieroRol', ConocemeOpciones::getFinancieroRol())
                            ->with('zonas', ConocemeOpciones::getZonas())
                            ->with('commodities', ConocemeOpciones::getCommodities())
                            ->with('usuario', $this->user)
                            ->with('canales', ConocemeCanalVentas::getCanales())
                            ->with('txtBacklog', ConocemeOpciones::getBacklogTexto())
                            ->with('rcc', $conoceme->getRcc());
        } else {
            flash('Cliente: ' . $request->get('cu') . ' no encontrado/disponible')->error();
            return redirect()->route('infinity.me.clientes');
        }
    }

    public function guardar(Request $request) {


        $conoceme = new Conoceme();
        $conoceme->getByCodunico($request->get('codunico'));
        

        //si es registro
        //if ($request->get('codunicoConoceme', null) == null) {
        if(true){
            //Si es registro
            $conoceme->setValues([
                '_codunicoConoceme' => $request->get('codunicoConoceme'),
                '_codunico' => $request->get('codunico'),
                '_modeloNegocio' => $request->get('modeloNegocio', 'NULL'),
                '_ventajaCompetitiva' => $request->get('ventajaCompetitiva', 'NULL'),
                '_fortalezasRiesgos' => $request->get('fortalezasRiesgos', 'NULL'),
                //'_flagIntegracionVertical' => $request->get('flagIntegracionVertical', 'NULL'),
                '_gerenteGeneral' => $request->get('gerenteGeneral', 'NULL'),
                '_financieroRol' => $request->get('financieroRol', 'NULL'),
                '_financieroNombre' => $request->get('financieroNombre', 'NULL'),
                '_tipoContabilidad' => $request->get('tipoContabilidad', 'NULL'),
            ]);

            //Carga de clientes
            $clientes = [];
            foreach ($request->get('cliente', []) as $cliente) {
                if (!is_null($cliente['documento']) || !is_null($cliente['nombre'])) {
                    $stakeholder = new ConocemeStakeholder();
                    $stakeholder->setValues([
                        '_codunico' => $request->get('codunico'),
                        '_tipo' => ConocemeStakeholder::TIPO_CLIENTE,
                        '_nombre' => $cliente['nombre'],
                        '_documento' => $cliente['documento'],
                        '_concentracion' => $cliente['participacion'],
                        '_desde' => $cliente['desde'],
                    ]);
                    $clientes[] = $stakeholder;
                }
            }
            $conoceme->setValue('_clientes', $clientes);

            //Carga de proveedores
            $proveedores = [];
            foreach ($request->get('proveedor', []) as $proveedor) {
                if (!is_null($proveedor['documento']) || !is_null($proveedor['nombre'])) {
                    $stakeholder = new ConocemeStakeholder();
                    $stakeholder->setValues([
                        '_codunico' => $request->get('codunico'),
                        '_tipo' => ConocemeStakeholder::TIPO_PROVEEDOR,
                        '_nombre' => $proveedor['nombre'],
                        '_documento' => $proveedor['documento'],
                        '_concentracion' => $proveedor['participacion'],
                        '_desde' => $proveedor['desde'],
                        '_exclusividad' => isset($proveedor['exclusividad']) ? 1 : 0,
                    ]);
                    $proveedores[] = $stakeholder;
                }
            }
            $conoceme->setValue('_proveedores', $proveedores);

            //Carga de accionistas
            $accionistas = [];
            foreach ($request->get('accionista', []) as $accionista) {
                if (!is_null($accionista['documento']) || !is_null($accionista['nombre'])) {
                    $stakeholder = new ConocemeStakeholder();
                    $stakeholder->setValues([
                        '_codunico' => $request->get('codunico'),
                        '_tipo' => ConocemeStakeholder::TIPO_ACCIONISTA,
                        '_nombre' => $accionista['nombre'],
                        '_documento' => $accionista['documento'],
                        '_concentracion' => $accionista['participacion'],
                    ]);
                    $accionistas[] = $stakeholder;
                }
            }
            $conoceme->setValue('_accionistas', $accionistas);

            //Carga de lineas
            $lineas = [];
            foreach ($request->get('linea', []) as $key => $linea) {
                if ($linea['monto'] !== null || !is_null($linea['tipoGarantia'])) {
                    $objLinea = new ConocemeLinea();
                    $objLinea->setValues([
                        '_codunico' => $request->get('codunico'),
                        '_banco' => $key,
                        '_linea' => $linea['monto'],
                        '_tipoGarantia' => $linea['tipoGarantia'],
                    ]);
                    $lineas[] = $objLinea;
                }
            }
            $conoceme->setValue('_lineas', $lineas);

            //Carga de Zona Operacion
            $zonasOperacion = [];
            foreach ($request->get('zonaOperacion', []) as $zonaOperacion) {
                $objCZona = new ConocemeZona();
                $objCZona->setValues([
                    '_codunico' => $request->get('codunico'),
                    '_tipo' => ConocemeZona::TIPO_OPERACIONES,
                    '_zona' => $zonaOperacion,
                ]);
                $zonasOperacion[] = $objCZona;
            }
            $conoceme->setValue('_zonaOperaciones', $zonasOperacion);

            //Carga de Zona Clientes
            $zonasCliente = [];
            foreach ($request->get('zonaCliente', []) as $zonaCliente) {
                $objCZona = new ConocemeZona();
                $objCZona->setValues([
                    '_codunico' => $request->get('codunico'),
                    '_tipo' => ConocemeZona::TIPO_CLIENTES,
                    '_zona' => $zonaCliente,
                ]);
                $zonasCliente[] = $objCZona;
            }
            $conoceme->setValue('_zonaClientes', $zonasCliente);

            //Carga de Mix de Ventas
            $mixVentas = [];
            foreach ($request->get('mixVenta', []) as $mixVenta) {
                if (!is_null($mixVenta['productoServicio'])) {
                    $stakeholder = new ConocemeMixVentas();
                    $stakeholder->setValues([
                        '_codunico' => $request->get('codunico'),
                        '_productoServicio' => $mixVenta['productoServicio'],
                        '_participacion' => $mixVenta['participacion'],
                    ]);
                    $mixVentas[] = $stakeholder;
                }
            }
            $conoceme->setValue('_mixVentas', $mixVentas);
        }

        $conoceme->setValues([
            '_actividad' => $request->get('actividad', 'NULL'),
            '_subsector' => $request->get('subsector', 'NULL'),
            '_backlog' => $request->get('backlog', 'NULL'),
            '_gestionesCompra' => $request->get('gestionesCompra', 'NULL'),
            '_procedenciaMateriaPrima' => $request->get('materiaPrima', 'NULL'),
            '_registro' => $this->user->getValue('_registro'),
            '_inicioIbk' => $request->get('inicioIBK', 'NULL'),
            '_inicioOperacion' => $request->get('inicioOperacion', 'NULL'),
        ]);

        //Carga de Canal de Ventas
        $canalVentas = [];
        foreach ($request->get('canalVenta', []) as $canalVenta) {
            if (!is_null($canalVenta['canales'])) {
                $stakeholder = new ConocemeCanalVentas();
                $stakeholder->setValues([
                    '_codunico' => $request->get('codunico'),
                    '_canal' => $canalVenta['canales'],
                    '_participacion' => $canalVenta['participacion'],
                ]);
                $canalVentas[] = $stakeholder;
            }
        }
        $conoceme->setValue('_canalVentas', $canalVentas);

        //Commodities
        $commodities = [];
        foreach ($request->get('commodity', []) as $commoditie) {
            $objCommoditie = new ConocemeCommoditie();
            $objCommoditie->setValues([
                '_codunico' => $request->get('codunico'),
                '_nombre' => $commoditie
            ]);
            $commodities[] = $objCommoditie;
        }
        $conoceme->setValue('_commodities', $commodities);

        if ($conoceme->registrar()) {
            flash('Datos registrados/actualizados correctamente')->success();
            return redirect()->route('infinity.me.cliente.conoceme', ['cu' => $request->get('codunico')]);
        } else {
            flash('Cliente ' . $request->get('cu') . ' no encontrado/disponible')->error();
            return redirect()->route('infinity.me.clientes');
        }
    }

}
