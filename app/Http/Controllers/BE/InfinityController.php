<?php 
namespace App\Http\Controllers\BE;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entity\Usuario;
use App\Entity\Infinity\Cliente as Cliente;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use Validator;

class InfinityController extends Controller{

	public function getListaClientes(Request $request){
    	return view('be.infinity.clientes');
    }

    public function getListaClientesTable(Request $request){
    	return Datatables::of(Cliente::getListaClientesInfinity(Auth::user()))->make(true);
    }

    public function resumen(Request $request){

    }

    public function detalle(Request $request){

    	//Obligatorio ingresar cliente
    	if(!$request->get('cliente')){
    		flash('Cliente no seleccionado')->error();
            return redirect()->route('infinity.clientes');
    	}

    	//Si encuentra el cliente
    	if($data = Cliente::getListaClientesInfinity(Auth::user(),$request->get('cliente',null))){
    		return view('be.infinity.detalle')
    		->with('usuario',Auth::user())
    		->with('cliente',$data);
    	//Si no encuentra a cliente
    	}else{
    		flash('Cliente '. $request->get('cliente') .' no encontrado/disponible')->error();
            return redirect()->route('infinity.clientes');
    	}

    }

}