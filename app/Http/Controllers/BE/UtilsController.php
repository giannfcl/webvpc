<?php 
namespace App\Http\Controllers\BE;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entity\BE\ZonalJefatura;
use App\Entity\BE\AccionComercial;
use App\Entity\ComboEjecutivo;
use App\Entity\Usuario;
use Validator;

class UtilsController extends Controller{

    public function getEjecutivosByJefatura(Request $request){

        $zonal = $request->get('zonal',$this->user->getValue('_zona'));
        $jefatura = $request->get('jefatura',$this->user->getValue('_centro'));
        $banca =$request->get('banca',$this->user->getValue('_banca'));
        return Usuario::getFarmersHunters($banca,$zonal,$jefatura);
    }

    public function getJefaturasByZonal(Request $request){

        $zonal = $request->get('zonal',$this->user->getValue('_zona'));
        $banca =$request->get('banca',$this->user->getValue('_banca'));

        return ZonalJefatura::getJefaturas($zonal,$banca);
    }

    public function getProductosByZonal(Request $request){

        $zonal = $request->get('zonal',$this->user->getValue('_zona'));

        return Usuario::getEjecutivosProductoByZonal($zonal);
    }

    public function getZonalesByBanca(Request $request){

        $banca = $request->get('banca',$this->user->getValue('_banca'));

        return ZonalJefatura::getZonales($banca);
    }

    //Acciones Comerciales
    public function getEtapasByAccion(Request $request){

        $accion = $request->get('accion',null);
        $estrategia =$request->get('estrategia',null);

        return AccionComercial::getEtapas($accion,$estrategia);
    }

    public function getAccionesByEstrategia(Request $request){

        $estrategia = $request->get('estrategia',null);

        return AccionComercial::getAcciones($estrategia);
    }

    //SSRS
    public function getArbolZonales(Request $request){
        $banca = $request->get('banca', null);
        return ComboEjecutivo::getZonales($banca,$this->user);
    }
    
    public function getArbolJefaturas(Request $request){
        $banca = $request->get('banca', null);
        $zonal = $request->get('zonal', null);
        return ComboEjecutivo::getJefaturas($banca, $zonal,$this->user);
    }
    
    public function getArbolEjecutivos(Request $request){
        $banca = $request->get('banca', null);
        $zonal = $request->get('zonal', null);
        $jefatura = $request->get('jefatura', null);
        return ComboEjecutivo::getEjecutivos($banca, $zonal, $jefatura);
    }
    
}