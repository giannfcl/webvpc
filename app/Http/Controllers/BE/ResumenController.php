<?php

namespace App\Http\Controllers\BE;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entity\BE\ZonalJefatura as ZonalJefatura;
use App\Entity\Usuario as Usuario;
use App\Entity\BE\ReporteEjecutivos as Reporte;
use Validator;

class ResumenController extends Controller {

    public function index(Request $request) {
        //Filtros de búsqueda
        $busqueda = [
            'page' => $request->get('page',1),
            'banca'=>$request->get('banca',null),
            'zonal' => $request->get('zonal',$this->user->getValue('_zona')),
            'jefatura' => $request->get('jefatura',$this->user->getValue('_centro')),
            'avance' => $request->get('avance',null),
        ];

        $orden = [
            'sort' => $request->get('sort',null),
            'order' => $request->get('order',null),
        ];

		//Esto es la consulta
		$entrada=Reporte::getReporteEjecutivo($busqueda,$orden)
			->setPath(config('app.url').'be/resumen');

        $jefaturas = [];
        $zonales = [];
        $bancas=[];

        if (in_array($this->user->getValue('_rol'),Usuario::getZonalesBE())){
            $jefaturas = ZonalJefatura::getJefaturas($this->user->getValue('_zona'));
        }

        if(in_array($this->user->getValue('_rol'),Usuario::getBanca())){
            $zonales = ZonalJefatura::getZonales($this->user->getValue('_banca'));
            $jefaturas = ZonalJefatura::getJefaturas(null,$this->user->getValue('_banca'));
        }

        if (in_array($this->user->getValue('_rol'),Usuario::getDivisionBE()) && $this->user->getValue('_rol')!=Usuario::ROL_GERENTE_BANCA){
            $zonales = ZonalJefatura::getZonales();
            $jefaturas = ZonalJefatura::getJefaturas();
            $bancas= ZonalJefatura::getBancas();
        }

        return view('be.resumen.index')
        		->with('busqueda',$busqueda)
        		->with('ejecutivos',$entrada)
        		->with('jefaturas',$jefaturas)
            	->with('zonales',$zonales)
            	->with('usuario',$this->user)
                ->with('orden',$orden)
                ->with('bancas',$bancas);
    }

   

}
