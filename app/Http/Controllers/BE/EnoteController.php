<?php

namespace App\Http\Controllers\BE;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Jenssegers\Date\Date as Carbon;
use App\Entity\BE\Lead as Lead;
use App\Entity\BE\OperacionesNuevas as OperacionesNuevas;
use App\Entity\BE\VencimientoAmortizacion as VencimientoAmortizacion;
use App\Entity\BE\SaldosDiarios as SaldosDiarios;
use Yajra\DataTables\DataTables as Datatables;
use App\Entity\BE\NotaEjecutivo as NotaEjecutivo;
use App\Entity\Usuario as Usuario;
use App\Entity\BE\ZonalJefatura as ZonalJefatura;
use Validator;

class EnoteController extends Controller {

    public function index(Request $request) {

        $ejecutivos = [];
        $jefaturas = [];
        $zonales = [];
        $bancas=[];

        if (in_array($this->user->getValue('_rol'),Usuario::getJefaturasBE())){
            $ejecutivos = Usuario::getFarmersHunters(null,null,$this->user->getValue('_centro'));
        }
        if (in_array($this->user->getValue('_rol'),Usuario::getZonalesBE())){
            $jefaturas = ZonalJefatura::getJefaturas($this->user->getValue('_zona'));
            $ejecutivos = Usuario::getFarmersHunters(null,$this->user->getValue('_zona'));
            //dd($jefaturas,$ejecutivos);
        }

         if(in_array($this->user->getValue('_rol'),Usuario::getBanca())){
            $zonales = ZonalJefatura::getZonales($this->user->getValue('_banca'));
            $jefaturas = ZonalJefatura::getJefaturas(null,$this->user->getValue('_banca'));
            $ejecutivos=Usuario::getFarmersHunters($this->user->getValue('_banca'));
        }

        if (in_array($this->user->getValue('_rol'),Usuario::getDivisionBE())){
            $bancas= ZonalJefatura::getBancas();
            $zonales = ZonalJefatura::getZonales();
            $jefaturas = ZonalJefatura::getJefaturas();
            $ejecutivos = Usuario::getFarmersHunters();
        }

        $busqueda = [
            'periodo' => $request->get('periodo',\App\Entity\BE\SaldosDiarios::getParametros('PERIODO',null)),
            'ejecutivo' => $request->get('ejecutivo',$this->user->getValue('_registro')),
            'jefatura' => $request->get('jefatura',null),
            'zonal' => $request->get('zonal',null),
            'banca'=>$request->get('banca',null),
        ];

        \Debugbar::info($busqueda);
        return view ('be.enote.index')
            ->with('productos',\App\Entity\BE\Producto::getAll())
            ->with('meses',OperacionesNuevas::getMesesOperacion())
            ->with('periodos',OperacionesNuevas::getMesesOperacion())
            ->with('busqueda',$busqueda)            
            ->with('bancas',$bancas)
            ->with('zonales',$zonales)
            ->with('jefaturas',$jefaturas)
            ->with('ejecutivos',$ejecutivos);
    }   

    public function resumen(Request $request) {

        /*Recibiras dos parámentros:*/
        //1: registro de ejecutivo
        //2: mes de consulta
        $busqueda = [
            'periodo' => $request->get('periodo',\App\Entity\BE\SaldosDiarios::getParametros('PERIODO',null)),
            'ejecutivo' => $request->get('ejecutivo',$this->user->getValue('_registro')),
            'jefatura' => $request->get('jefatura',$this->user->getValue('_centro')),
            'zonal' => $request->get('zonal',null),
            'banca'=>$request->get('banca',null),
        ];   

        //dd($this->user->getValue('_rol'));
        
        return view ('be.enote.resumen')
            ->with('busqueda',$busqueda) 
            ->with('resumenSaldos',VencimientoAmortizacion::getResumenSaldos($this->user,$request->get('periodo',\App\Entity\BE\SaldosDiarios::getParametros('PERIODO',null)),$request->get('ejecutivo',null),$request->get('banca',null),$request->get('zonal',null)))
            ->with('resumenDesembolsos',VencimientoAmortizacion::getResumenDesembolsos($this->user,$request->get('periodo',\App\Entity\BE\SaldosDiarios::getParametros('PERIODO',null)),$request->get('ejecutivo',null),$request->get('banca',null),$request->get('zonal',null)))
            ->with('resumenMetas',VencimientoAmortizacion::getResumenMeta($this->user,$request->get('periodo',\App\Entity\BE\SaldosDiarios::getParametros('PERIODO',null)),$request->get('ejecutivo',null),$request->get('banca',null),$request->get('zonal',null)))
            ->with('resumenProyeccInicial',VencimientoAmortizacion::getResumenProyeccInicial($this->user,$request->get('periodo',\App\Entity\BE\SaldosDiarios::getParametros('PERIODO',null)),$request->get('ejecutivo',null),$request->get('banca',null),$request->get('zonal',null)))
            ->with('resumenCaidas',VencimientoAmortizacion::getResumenCaidas($this->user,$request->get('periodo',\App\Entity\BE\SaldosDiarios::getParametros('PERIODO',null)),$request->get('ejecutivo',null),$request->get('banca',null),$request->get('zonal',null)));
    }

    public function getAmortizaciones(Request $request){                
        return Datatables::of(VencimientoAmortizacion::getAmortizaciones($this->user,$request->get('periodo',\App\Entity\BE\SaldosDiarios::getParametros('PERIODO',null)),$request->get('ejecutivo',null)))->make(true);
    }

    public function getVencimientos(Request $request){
        \Debugbar::info($request->get('periodo'));
        return Datatables::of(VencimientoAmortizacion::getVencimientos($this->user,$request->get('periodo',\App\Entity\BE\SaldosDiarios::getParametros('PERIODO',null)),$request->get('ejecutivo',null)))->make(true);   
    }

    public function getOperaciones(Request $request){
        return Datatables::of(OperacionesNuevas::getOperacionesNuevas($this->user,$request->get('periodo',\App\Entity\BE\SaldosDiarios::getParametros('PERIODO',null)),$request->get('ejecutivo',null)))->make(true);   
    }

    public function updateVencAmort(Request $request){

        $validator = Validator::make($request->all(),[
                'items' => 'required | array',
                'items.*.fecha' => 'date',
                'items.*.codUnico' => 'required',
                'items.*.idProducto' => 'required',
                'items.*.fechaVencimiento' => 'required | date',
                'items.*.flag' => 'required | boolean',
                'items.*.porcRenovacion' => 'required|numeric|between:0,100'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'msg' => array_flatten($validator->errors()->getMessages())]);
        }

        $lista = array();


        foreach ($request->get('items') as $item) {
            $obj = new VencimientoAmortizacion();
            $obj->setValues([
                '_fecha' => $item['fecha'],
                '_codUnico' => $item['codUnico'],
                '_idProducto' => $item['idProducto'],
                '_fechaVencimiento' => $item['fechaVencimiento'],
                '_flagRenovacion' => $item['flag'],
                '_porcRenovacion' => $item['flag'] == '0'? 0 :$item['porcRenovacion']/100,
            ]);
            $lista[] = $obj;
        }
        
        $oVencAmort = new VencimientoAmortizacion();
        if ($oVencAmort->massiveUpdate($lista)){
            return ['status' => 'ok'];
        }else{
              return ['status' => 'error', 'msg' => $oVencAmort->getMessage()];
        }
    }

    public function updateOperaciones(Request $request){
        
        $validator = Validator::make($request->all(),[
                'items' => 'required | array',
                'items.*.id' => 'required',
                'items.*.MontoCert' => 'required',
                'items.*.MontoEncot' => 'required',
                'items.*.flagDesembolso' => 'required | boolean',
                'items.*.flagPerdida' => 'required | boolean',
                'items.*.fechaDesembolso' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'msg' => array_flatten($validator->errors()->getMessages())]);
        }

        $lista = array();


        foreach ($request->get('items') as $item) {
            $obj = new OperacionesNuevas();
            $obj->setValues([
                '_id' => $item['id'],
                '_montoCertero' => $item['MontoCert'],
                '_montoCotizacion' => $item['MontoEncot'],
                '_flagDesembolso' => $item['flagDesembolso'],
                '_flagPerdida' => $item['flagPerdida'],
                '_fechaDesembolso' => $item['fechaDesembolso'],
            ]);
            $lista[] = $obj;
        }
        \Debugbar::info($obj);
        
        $oOperacion = new OperacionesNuevas();
        if ($oOperacion->massiveUpdate($lista)){
            return ['status' => 'ok'];
        }else{
              return ['status' => 'error', 'msg' => $oVencAmort->getMessage()];
        }
    }

    public function buscarCliente(Request $request){        
        $cliente =  \App\Entity\BE\AccionComercial::getClientesEjecutivo($this->user,['documento' => $request->get('documento')],false)->get();
        if (count($cliente) >= 1){
            $acciones = array();
            foreach ($cliente as $item) {
                if ($item->ID_ACCION){
                    $acciones[] = ['key' => $item->ID_ACCION, 'value' => $item->ACCION];    
                }
            }
            \Debugbar::info(array_unique($acciones, SORT_REGULAR));
            return response()->json(['existe' => 'si', 'acciones' => $acciones, 'cliente' => $cliente[0]->NOMBRE,'codunico' => $cliente[0]->COD_UNICO], 200);
        }else{
            return response()->json(['existe' => 'no'], 200);
        }

    }

    public function getColDirectas(Request $request){
        return SaldosDiarios::getGraficoDirectas($this->user,$request->get('periodo',201803),$request->get('ejecutivo',null))->get();
    }

    public function getColIndirectas(Request $request){
        return SaldosDiarios::getGraficoIndirectas($this->user,$request->get('periodo',201803),$request->get('ejecutivo',null))->get();
    }

    public function insertOperacion(Request $request){
        \Debugbar::info($request);
        $validator = Validator::make($request->all(),[
                'documento' => 'required',
                'codunico' => 'required',
                'producto' => 'required',
                'accionComercial' => 'nullable',
                'tipoOperacion' => 'required|in:1,2,3',
                'tipoMonto' => 'required|in:certero,cotizacion',
                'monto' => 'required|numeric|min:1'
                //'fecha' => 'date_format:Y-m-d'
                
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'msg' => array_flatten($validator->errors()->getMessages())]);
        }

        $operacion = new OperacionesNuevas();
        
        $periodo = $request->get('periodo');

        $fechaSd = \App\Entity\BE\SaldosDiarios::getParametros('FECHA_ACT_SD',$periodo);
        $fechaCarbon = Carbon::createFromFormat('Y-m-d', $fechaSd);

        if($request->get('tipoMonto')=='cotizacion'){
            if(!$request->get('fechaCotizacion') or $request->get('fechaCotizacion')==''){                
                $fecha= $fechaCarbon->startOfMonth();
            }else{                
                $fecha= $request->get('fechaCotizacion');
            }          
        }else{            
            $fecha= $request->get('fechaCertero');
        }
        
        \Debugbar::info($fecha);
        $operacion->setValues([
            '_fechaDesembolso' => $fecha,
            '_documento' => $request->get('documento'),
            '_registro' => $request->get('ejecutivo',$this->user->getValue('_registro')),
            '_producto' => $request->get('producto'),
            '_monto' => $request->get('monto') * 1000,
            '_tipoOperacion' => $request->get('tipoOperacion'),
            '_accionComercial' => $request->get('accionComercial'),
            '_codUnico' => $request->get('codunico'),
            '_tipoMonto' => $request->get('tipoMonto'),
            '_usuario' => $this->user->getValue('_registro')
        ]);
        
        if($operacion->insert()){
            switch($request->get('tipoOperacion')){
                case 1:
                    return response()->json(['status' => 'ok','operacion' =>1]);
                    break;
                case 2:
                    return response()->json(['status' => 'ok','operacion' =>2]);
                    break;
                case 3:
                    return response()->json(['status' => 'ok','operacion' =>3]);
                    break;
            }            
        }else{            
            return response()->json(['status' => 'error', 'msg' => $operacion->getMessage()]);
        }
    }

    public function listarNotas(Request $request){
        return NotaEjecutivo::listarByOperacion($request->get('operacion', null));
    }

    public function agregarNota(Request $request){
        $validator = Validator::make($request->all(), [
            'operacion' => 'required',
            'nota' => 'required|max:500',
        ]);

        if ($validator->fails()) {
            return response()->json(array_flatten($validator->errors()->getMessages()), 404);
        }

        $nota = new NotaEjecutivo();
        $nota->setValues([
            '_ejecutivo' => $this->user->getValue('_registro'),
            '_operacion' => $request->get('operacion', null),
            '_nota' => $request->get('nota', null),
        ]);

        if ($nota->agregar()) {
            return response()->json($nota->setValueToTable(), 200);
        } else {
            return response()->json(['msg' => $nota->getMessage()], 404);
        }
    }

    public function eliminarNota(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(array_flatten($validator->errors()->getMessages()), 404);
        }

        $nota = new NotaEjecutivo();
        $nota->setValues([
            '_id' => $request->get('id', null),
        ]);

        if ($nota->eliminar()) {
            return response()->json('ok', 200);
        } else {
            return response()->json(['msg' => $nota->getMessage()], 404);
        }
        
    }

    public function graficar(Request $request){        
        return SaldosDiarios::process($request->get('periodo',201803),$this->user->getValue('_registro'),$request->get('ejecutivo',null),$request->get('jefatura',null),$request->get('banca',null),$request->get('zonal',null));
    }

    public function autocomplete(Request $request) {
        /*set usuario de busqueda*/
        $regEjecutivo = $request->get('ejecutivo',null);         
        return Lead::autoCompleteLead($this->user,$request->get('termino', ''), $regEjecutivo);
    }
}