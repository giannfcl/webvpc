<?php

namespace App\Http\Controllers\BE;

use \Illuminate\Pagination\LengthAwarePaginator as Paginator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entity\BE\InfiniteBase as mBase;
use App\Entity\BE\ZonalJefatura as ZonalJefatura;
use App\Entity\Usuario as Usuario;
use Jenssegers\Date\Date as Carbon;
use Validator;

class InfiniteController extends Controller {

    public function index(Request $request) {


        if (in_array($this->user->getValue('_rol'), array_merge(Usuario::getDivisionBE(),[Usuario::ROL_EN_ESCOM]))) {
            $busqueda = [
                'page' => $request->get('page', 1),
                'ejecutivo' => $request->get('ejecutivo', null),
                'jefatura' => $request->get('jefatura', null),
                'zonal' => $request->get('zonal', null),
                'banca' => $request->get('banca', null),
            ];
        }

        if (in_array($this->user->getValue('_rol'), array_merge(Usuario::getBanca(),[Usuario::ROL_RIESGOS_ME_JEFATURA]))) {
            $busqueda = [
                'page' => $request->get('page', 1),
                'ejecutivo' => $request->get('ejecutivo', null),
                'jefatura' => $request->get('jefatura', $this->user->getValue('_centro')),
                'zonal' => $request->get('zonal', $this->user->getValue('_zona')),
                'banca' => $request->get('banca', null),
            ];
        }

        if (in_array($this->user->getValue('_rol'), array_merge(Usuario::getJefaturasBE(),[Usuario::ROL_RIESGOS_ME_JEFATURA]))) {
            $busqueda = [
                'page' => $request->get('page', 1),
                'ejecutivo' => $request->get('ejecutivo', null),
                'jefatura' => $request->get('jefatura', $this->user->getValue('_centro')),
                'zonal' => $request->get('zonal', $this->user->getValue('_zona')),
            ];
        }
        if (in_array($this->user->getValue('_rol'), array_merge(Usuario::getZonalesBE(),[Usuario::ROL_RIESGOS_ME_ZONAL]))) {
            $busqueda = [
                'page' => $request->get('page', 1),
                'ejecutivo' => $request->get('ejecutivo', null),
                'jefatura' => $request->get('jefatura', null),
                'zonal' => $request->get('zonal', $this->user->getValue('_zona')),
                'banca' => $request->get('banca', $this->user->getValue('_banca')),
            ];
        }
        if (in_array($this->user->getValue('_rol'), array_merge(Usuario::getEjecutivosBE()))) {
            $busqueda = [
                'page' => $request->get('page', 1),
                'ejecutivo' => $request->get('ejecutivo', $this->user->getValue('_registro')),
                //'ejecutivo' => $request->get('ejecutivo', null),
                'jefatura' => $request->get('jefatura', $this->user->getValue('_centro')),
                'zonal' => $request->get('zonal', $this->user->getValue('_zona')),
            ];
        }

        $entrada = mBase::getByLead($busqueda['page'], $busqueda['zonal'], $busqueda['jefatura'], $busqueda['ejecutivo'])
                ->setPath(config('app.url') . 'be/creditosvencidos');

        $ejecutivos = [];
        $jefaturas = [];
        $zonales = [];
        $bancas=[];

        if (in_array($this->user->getValue('_rol'),array_merge(Usuario::getJefaturasBE(),[Usuario::ROL_RIESGOS_ME_JEFATURA]))){
            $ejecutivos = Usuario::getFarmersHunters(null,null,$this->user->getValue('_centro'));
        }
        if (in_array($this->user->getValue('_rol'),array_merge(Usuario::getZonalesBE(),[Usuario::ROL_RIESGOS_ME_ZONAL]))){
            $jefaturas = ZonalJefatura::getJefaturas($this->user->getValue('_zona'));
            $ejecutivos = Usuario::getFarmersHunters(null,$this->user->getValue('_zona'));
            //dd($jefaturas,$ejecutivos);
        }

         if(in_array($this->user->getValue('_rol'),Usuario::getBanca())){
            $zonales = ZonalJefatura::getZonales($this->user->getValue('_banca'));
            $jefaturas = ZonalJefatura::getJefaturas(null,$this->user->getValue('_banca'));
            $ejecutivos=Usuario::getFarmersHunters($this->user->getValue('_banca'));
        }

        if (in_array($this->user->getValue('_rol'),array_merge(Usuario::getDivisionBE(),[Usuario::ROL_EN_ESCOM]))){
            $bancas= ZonalJefatura::getBancas();
            $zonales = ZonalJefatura::getZonales();
            $jefaturas = ZonalJefatura::getJefaturas();
            $ejecutivos = Usuario::getFarmersHunters();
        }


        /*
          if (in_array($this->user->getValue('_rol'), Usuario::getDivisionBE())) {
          $zonales = ZonalJefatura::getZonales();
          $jefaturas = ZonalJefatura::getJefaturas();
          $ejecutivos = Usuario::getFarmersHunters();

          }
          if (in_array($this->user->getValue('_rol'), Usuario::getJefaturasBE())) {
          $ejecutivos = Usuario::getFarmersHunters(null, $this->user->getValue('_centro'));

          }
          if (in_array($this->user->getValue('_rol'), Usuario::getZonalesBE())) {
          $jefaturas = ZonalJefatura::getJefaturas($this->user->getValue('_zona'));
          $ejecutivos = Usuario::getFarmersHunters($this->user->getValue('_zona'));
          }
          if (in_array($this->user->getValue('_rol'), Usuario::getEjecutivosBE())) {
          $zonales = ZonalJefatura::getZonales();
          $jefaturas = ZonalJefatura::getJefaturas();
          $ejecutivos = Usuario::getFarmersHunters();
          }
         */
        $base = new mBase();
        //\Debugbar::info($base->getZonal($busqueda['banca']));
        //$ejecutivos = $base->getEjecutivos();
        //$zonal = $base->getZonal();


        return view('be.mora.index')
                        ->with('leads', $entrada)
                        ->with('busqueda', $busqueda)
                        ->with('ejecutivos', $ejecutivos)
                        ->with('jefaturas', $jefaturas)
                        ->with('zonales', $zonales)
                        ->with('bancas', $bancas)
                        ->with('usuario', $this->user);
        //->with('ejecutivos', $base->getEjecutivos($busqueda['banca'], $busqueda['zonal']))
        //->with('zonales', $base->getZonal($busqueda['banca']));
    }

    public function getZonales(Request $request) {
        $base = new mBase();
        $zonales = $base->getZonal($request->get('banca', 'Todos'));

        return $zonales;
    }

    public function guardarGestion(Request $request) {
        \Debugbar::info($request->get('fecha'));
        $data = [
            'FECHA' => $request->get('fecha'), //carbon::now()->format('Ymd'),
            'ID_CLIE' => $request->get('idCliente'),
            'NUM_DOCUMENTO' => $request->get('numDocumento'),
            'ESTADO' => $request->get('estado'),
            'COMENTARIO' => $request->get('comentario'),
            'MARCA' => $request->get('marca'),
        ];

        if (mBase::updateGestion($data)) {
            //return 0;
            return response()->json(['msg' => 'ok'], 200);
        } else {
            return 1;
            //return response()->json(['msg' => $data->getMessage()], 404);
        }
    }

}
