<?php

namespace App\Http\Controllers\BE;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entity\LeadCampanha as LeadCampanha;
use App\Entity\BE\AccionComercial as AccionComercial;
use App\Entity\BE\leadEtapa as LeadEtapa;
use App\Entity\BE\AccionResponsable as AccionResponsable;
use App\Entity\BE\Etapa as Etapa;
use App\Entity\Campanha as Campanha;
use App\Entity\Banco as Banco;
use App\Entity\BE\MantenerLead as MantenerLead;
use App\Entity\BE\ZonalJefatura as ZonalJefatura;
use App\Entity\Usuario as Usuario;
use App\Entity\BE\NotaEjecutivo as NotaEjecutivo;
use Validator;

class AccionesComercialesController extends Controller {

    public function index(Request $request) {

        //dd($request->all());
        $busqueda = [
            'page' => $request->get('page',1),
            'codUnico' => $request->get('codUnico',null),
            'documento' => $request->get('documento',null),
            'razonSocial' => $request->get('razonSocial',null),
            'categoria' => $request->get('categoria',null),
            'estrategia' => $request->get('estrategia',null),
            'accion' => $request->get('accion',null),
            'etapa' => $request->get('etapa',null),
            'semaforo'=>$request->get('semaforo',null),
            'ejecutivoProducto' => $request->get('ejecutivoProducto',null),
            'ejecutivo' => $request->get('ejecutivo',null),
            'jefatura' => $request->get('jefatura',null),
            'zonal' => $request->get('zonal',null),
            'banca'=>$request->get('banca',null),
            'flgAccion'=>$request->get('flgAccion',$request->get('flgRequest',0))
        ];

        $orden = [
            'sort' => $request->get('sort',null),
            'order' => $request->get('order',null),
        ];
        
        // Validamos los parametros de ordenamiento

        if (is_null($orden['sort']) || !in_array($orden['sort'], ['deudaRCC','deudaIBK','categoria','ventas','volumen','mesActivacion','etapa','accion'])){
            $orden['sort'] = null;
            if (is_null($orden['order']) || !in_array($orden['order'], ['asc','desc'])){
                $orden['order'] = null;
            }
        }

        $entrada=AccionComercial::getMisClientes($this->user,$busqueda,$orden)
            ->setPath(config('app.url').'be/misacciones/lista');
        
        
        $ejecutivos = [];
        $jefaturas = [];
        $zonales = [];
        $bancas=[];

        if (in_array($this->user->getValue('_rol'),Usuario::getEjecutivosProductoBE())){
            $ejecutivos = Usuario::getFarmersHunters(null,$this->user->getValue('_zona'));
        }
        if (in_array($this->user->getValue('_rol'),Usuario::getJefaturasBE())){
            $ejecutivos = Usuario::getFarmersHunters(null,null,$this->user->getValue('_centro'));
        }
        if (in_array($this->user->getValue('_rol'),Usuario::getZonalesBE())){
            $jefaturas = ZonalJefatura::getJefaturas($this->user->getValue('_zona'));
            $ejecutivos = Usuario::getFarmersHunters(null,$this->user->getValue('_zona'));
        }

         if(in_array($this->user->getValue('_rol'),Usuario::getBanca())){
            $zonales = ZonalJefatura::getZonales($this->user->getValue('_banca'));
            $jefaturas = ZonalJefatura::getJefaturas(null,$this->user->getValue('_banca'));
            $ejecutivos=Usuario::getFarmersHunters($this->user->getValue('_banca'));
        }

        if (in_array($this->user->getValue('_rol'),Usuario::getDivisionBE())){
            $bancas= ZonalJefatura::getBancas();
            $zonales = ZonalJefatura::getZonales();
            $jefaturas = ZonalJefatura::getJefaturas();
            $ejecutivos = Usuario::getFarmersHunters();
        }        
        
        return view('be.acciones.index')
            ->with('visualizacion',(in_array($this->user->getValue('_rol'),Usuario::getEjecutivosProductoBE())?1:($busqueda['flgAccion']==1?1:0)))
            ->with('busqueda',$busqueda)
            ->with('acciones', $entrada)
            //->with('listaAcciones',AccionComercial::getNombreAcciones())
            ->with('accionesEstrategia',AccionComercial::getAccionesEstrategias())
            ->with('eProductos',Usuario::getEjecutivosProductoByZonal($this->user->getValue('_zona')))
            ->with('ejecutivos',$ejecutivos)
            ->with('jefaturas',$jefaturas)
            ->with('zonales',$zonales)
            ->with('bancas',$bancas)
            ->with('semaforo',['Rojo','Ámbar','Verde'])
            ->with('usuario',$this->user)
            ->with('orden',$orden)
            ->with('categorias',AccionComercial::getCategorias())
            ->with('estrategias', AccionComercial::getEstrategias())
            ->with('accionesE', AccionComercial::getAcciones())
            ->with('etapasAcciones', AccionComercial::getEtapas())
            //->with('tiposAccion',AccionComercial::getTipos())
            ->with('mesesActivacion',AccionComercial::getMesesActivacion())
            ->with('flgAccion',$busqueda['flgAccion'])
            ->with('etapas',AccionComercial::getEtapas());           
    }


    public function updateEtapa(Request $request){
        
        $validator = Validator::make($request->all(), [
                'cliente' => 'required',
                'etapa' => 'required',
                'accion' => 'required',
                'tooltip' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(array_flatten($validator->errors()->getMessages()), 404);
        }

        $leadEtapa = new leadEtapa();
        $leadEtapa->setValues([
            '_numdoc' => $request->get('cliente', null),
            '_etapa' => $request->get('etapa', null),
            '_estrategia' => $request->get('accion', null),
            '_tooltip'=>$request->get('tooltip', null),
        ]);

        
        if ($leadEtapa->cambiarEtapa($this->user,'accion')) {
            flash('Se actualizó la etapa de manera correcta')->success();
            return back();
        } else {
            return response()->json(['msg' => $leadEtapa->getMessage()], 404);
        }
    }

    public function updateMesActiv(Request $request){
        $validator = Validator::make($request->all(), [
                'cliente' => 'required',
                'mesActivCambio' => 'required',
                'accion' => 'required',
                'tooltip' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(array_flatten($validator->errors()->getMessages()), 404);
        }

        $accionComercial = new AccionComercial();        

        if ($accionComercial->updateMes($request->all())) {
            flash('Se actualizó el mes de activación de manera correcta')->success();
            return back();
        } else {
            flash('Hubo un error al actualizar el mes de activación')->error();
            return back();
        }
    }

    public function updateKPI(Request $request){
        $validator = Validator::make($request->all(), [
                'cliente' => 'required',
                'kpi' => 'required',
                'accion' => 'required',
                'tooltip' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(array_flatten($validator->errors()->getMessages()), 404);
        }

        $accionComercial = new AccionComercial();        

        if ($accionComercial->updateKPI($request->all())) {
            flash('Se actualizó el volumen proyectado de manera correcta')->success();
            return back();
        } else {
            flash('Hubo un error al actualizar el volumen proyectado')->error();
            return back();
        }
    }

    public function updateFechaFin(Request $request){
        $validator = Validator::make($request->all(), [
                'cliente' => 'required',
                'fInicio' => 'required',
                'fFin' => 'required',
                'accion' => 'required',
                'tipo' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(array_flatten($validator->errors()->getMessages()), 404);
        }

        $leadCampanha = new LeadCampanha();
        $leadCampanha->setValues([
            '_lead' => $request->get('cliente', null),   
            '_fechaFin' => $request->get('fFin', null),
            '_fechaInicio' => $request->get('fInicio', null),
            '_campanha' => $request->get('accion', null),
            '_registroResponsable' => $this->user->getValue('_registro'),
            '_tipoAsignacion'=>$request->get('tipo', null),
        ]);

        if ($leadCampanha->cambiarFechaFin($this->user)==1) {
            flash('Se actualizó la fecha de manera correcta')->success();
            return back();
        } else if ($leadCampanha->cambiarFechaFin($this->user)==-1){
            flash('La fecha fin no puede ser menor a la fecha de inicio')->error();
            return back();
        } else{
            return response()->json(['msg' => $leadCampanha->getMessage()], 404);
        }
    }

    public function consultaCliente(Request $request) {
        $validator = Validator::make($request->all(), [
                'codUnico' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(array_flatten($validator->errors()->getMessages()), 404);
        }

        $data = AccionComercial::buscarCliente($request->get('codUnico'));

        if ($data) {
            return response()->json(['existe' => 'si', 'data' => $data], 200);
        } else {
            return response()->json(['existe' => 'no'], 200);
        }
    }

    public function listaIngresar(Request $request) {
        return AccionComercial::getNombreLista($request->get('accionesIngresar'));
    }

    public function updateEstrella(Request $request){

         $validator = Validator::make($request->all(), [
            'cliente' => 'required',              
            'accion' => 'required',
            'tooltip' => 'required',
        ]);

        if(!in_array($this->user->getValue('_rol'),Usuario::getEjecutivosBE())){
            return;
        }

        $data=['cliente' => $request->get('cliente', null),          
                'accion' => $request->get('accion', null),
                'tooltip'=>$request->get('tooltip', null)];


        $accionComercial = new AccionComercial(); 
        if($accionComercial->updateEstrella($data)){
            return response()->json(['msg' => 'ok'], 200);
        }else{
            return response()->json(['msg' => $gestion->getMessage()], 404);
        }
    }

    public function registroAccion(Request $request){
        $validator = Validator::make($request->all(), [
            'codUnico' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(array_flatten($validator->errors()->getMessages()), 404);
        }

        //DEBEMOS DE PASAR EL FILTRO DEL CLIENTE A REGISTRAR
        $atributos=['rol'=>$this->user->getValue('_rol'),
                    'banca'=>$this->user->getValue('_banca'),
                    'zonal'=>$this->user->getValue('_zona'),
                    'jefatura'=>$this->user->getValue('_centro')];

        $cliente = AccionComercial::buscarCliente($request->get('codUnico')); 
        //dd($cliente);
        //Vemos los ejecutivos que son de nuestra zonal (en caso seamos Analista o cash)
        $regEjecutivos=[];
        if(in_array($atributos['rol'],array_merge(Usuario::getAnalistasInternosBE(),Usuario::getEjecutivosProductoBE()))){
            $ejecutivos=Usuario::getFarmersHunters($atributos['banca'],$atributos['zonal'],$atributos['jefatura']);
       
            for($i=0;$i<$ejecutivos->count();$i++){
                $regEjecutivos[$i]=$ejecutivos[$i]->REGISTRO;
            }

            //El cliente también puede estar sectorizado a un analista
            $regEjecutivos[$ejecutivos->count()]=$this->user->getValue('_registro');
           
            if(!(in_array($cliente->REGISTRO_EN,$regEjecutivos) or $cliente->ZONA==$this->user->getValue('_zona'))){
                flash('No puedes registrarle acciones comerciales a un cliente que no sea de tu zonal.')->error();
                return back(); 
            }

        }
        else if (in_array($atributos['rol'],Usuario::getEjecutivosBE()) and $cliente->REGISTRO_EN!=$this->user->getValue('_registro')){
            flash('No puedes agregarle acciones comerciales a un cliente que no sea de tu cartera.')->error();
            return back();
        }
        //dd($atributos,$cliente);
        //return back();
        if(!$cliente->FLG_ES_CLIENTE){
                //Validar que pertenezca a una campaña            
                if(AccionComercial::tieneCampanhaActiva(AccionComercial::getNumDoc($request->get('codUnico')))<1){
                    //flash('No se puede ingresar acción comercial a un prospecto.')->error();
                    flash('No se puede ingresar acción comercial a un prospecto que no se encuentre en campaña.')->error();
                    return back();
                }
        }

        $acciones=[];   
        $resultTotal=true;
        $insertar=0;
        for ($insertar=0; $insertar <2 ; $insertar++) {            
            foreach ($request->get('checkAccion') as $check){           
                $nombreEstrategia=AccionComercial::getEstrategias($check)->first()->ESTRATEGIA;
                $delegado=(isset($request->get('cboDelegado',null)[$check])?$request->get('cboDelegado',null)[$check]:NULL);
                $kpi=(isset($request->get('kpiAccion',null)[$check])?$request->get('kpiAccion',null)[$check]*1000:NULL);
                $fFin=(isset($request->get('fFin',null)[$check])?$request->get('fFin',null)[$check]:NULL);
                $mesActiv=(isset($request->get('cboMesActiv',null)[$check])?$request->get('cboMesActiv',null)[$check]:NULL);
             
                $datosAccion=['idAccion'=>$check,
                                'delegado'=>$delegado,
                                'kpi'=>$kpi,
                                'fFin'=>$fFin,
                                'mesActiv'=>$mesActiv];
                //dd($datosAccion);
                $accion = new AccionComercial();            
                $result = $accion->registrarAccion(
                    $request->get('codUnico'),
                    $this->user->getValue('_registro'),
                    $datosAccion, $atributos,$insertar);
                //dd("SUAVE");
                if(!$result) {
                    $resultTotal=false;
                    break;
                }   

                if ($result==true and $insertar==1) {          

                    $nota = new NotaEjecutivo();               
                    $nota->setValues([
                            '_ejecutivo' => $this->user->getValue('_registro'),
                            '_numdoc' => AccionComercial::getNumDoc($request->get('codUnico')),
                            '_idAccion'=>$check,
                            '_tooltip'=>AccionComercial::TOOLTIP,
                    ]); 

                    if($nombreEstrategia=='RENTABILIZAR CLIENTES' and isset($request->get('notaAccion')['RENTABILIZAR CLIENTES']) 
                        and $request->get('notaAccion')['RENTABILIZAR CLIENTES']!=NULL){                        
                            $nota->setValues([
                                '_nota' => $request->get('notaAccion')['RENTABILIZAR CLIENTES'],
                            ]);
                            $nota->agregar();               
                    }      
                    else if ($nombreEstrategia=='BRINDAR LA MEJOR EXPERIENCIA' and isset($request->get('notaAccion')['BRINDAR LA MEJOR EXPERIENCIA'])
                        and $request->get('notaAccion')['BRINDAR LA MEJOR EXPERIENCIA']!=NULL){
                            $nota->setValues([
                                '_nota' => $request->get('notaAccion')['BRINDAR LA MEJOR EXPERIENCIA'],
                            ]);
                            $nota->agregar();
                    }

                }
            } 
            if(!$resultTotal) break;   
        }

        if($resultTotal){
            flash('Las acciones comerciales se registraron correctamente')->success();
        } else {
            flash('Hubo un error al registrar las acciones comerciales, vuelva a intentarlo')->error();
        }
        return back();

    }

    public function eliminarAccion(Request $request){
        //En este caso se asume $tipo= MOTIVO $motivo= DETALLE
        $documento=$request->get('documentoE');
        $idAccion=$request->get('accionE');
        $tooltip=$request->get('tooltip');
        $tipo=$request->get('eliminar');
        $motivo="";
        $accion = new AccionComercial();
        $nombreAccion=AccionComercial::getNombreAcciones($idAccion)[0]->NOMBRE;

        //$result=1;
        if($tipo<>"1"){
            $motivo=$request->get('xmotivo');
        }

        $comentario=$request->get('eliminarComentario');
        
        //dd($request->all());
        $result = $accion->eliminarAccion($documento,$idAccion,$tooltip,$tipo,$motivo,$comentario);

        if ($result) {            
            flash('Se elimino la acción comercial '.$nombreAccion. ' del cliente ' . $request->get('documentoE') . ' de manera correcta')->success();
        }else{
            flash($accion->getMessage())->error();
        }
        
        return back();
    }

 
    public function agregarNota(Request $request){

        $validator = Validator::make($request->all(), [
            'lead' => 'required',
            'idAccion'=>'required',
            'nota' => 'required|max:500',
            'tooltip'=>'required'
        ]);

        if ($validator->fails()) {
            return response()->json(array_flatten($validator->errors()->getMessages()), 404);
        }

        $nota = new NotaEjecutivo();
        $nota->setValues([
            '_ejecutivo' => $this->user->getValue('_registro'),
            '_numdoc' => $request->get('lead', null),
            '_nota' => $request->get('nota', null),
            '_idAccion'=>$request->get('idAccion',null),
            '_tooltip'=>$request->get('tooltip',null),
        ]);

        if ($nota->agregar()) {
            return response()->json(array_merge($nota->setValueToTable(),['NOMBRE_EJECUTIVO'=>$this->user->getValue('_nombre')]), 200);
        } else {
            return response()->json(['msg' => $nota->getMessage()], 404);
        }

    }

    public function eliminarNota(Request $request){

        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(array_flatten($validator->errors()->getMessages()), 404);
        }

        $nota = new NotaEjecutivo();
        $nota->setValues([
            '_ejecutivo' => $this->user->getValue('_registro'),
            '_numdoc' => $request->get('lead', null),
            '_id' => $request->get('id', null),
        ]);

        if ($nota->eliminar()) {
            return response()->json('ok', 200);
        } else {
            return response()->json(['msg' => $nota->getMessage()], 404);
        }
        
    }

    public function listarNotas(Request $request){
        return NotaEjecutivo::listar($this->user->getValue('_registro'),$request->get('lead', null),
            $request->get('idAccion',null),$request->get('tooltip',null));
    }

    public function getAccionesCliente(Request $request){
        return AccionComercial::getAccionesExistentes($request->get('numDoc',null),$request->get('checks',null));
    }
      
}