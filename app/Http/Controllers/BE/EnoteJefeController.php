<?php

namespace App\Http\Controllers\BE;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Jenssegers\Date\Date as Carbon;
use App\Entity\BE\OperacionesNuevas as OperacionesNuevas;
use App\Entity\BE\VencimientoAmortizacion as VencimientoAmortizacion;
use App\Entity\BE\Caidas as Caidas;
use App\Entity\BE\SaldosDiarios as SaldosDiarios;
use Yajra\DataTables\DataTables as Datatables;
use App\Entity\BE\NotaEjecutivo as NotaEjecutivo;
use App\Entity\Usuario as Usuario;
use App\Entity\BE\ZonalJefatura as ZonalJefatura;
use Validator;

class EnoteJefeController extends Controller {

    public function menu(Request $request){
        return view ('be.enote.menu-jefe');
    }

    public function index(Request $request) {
        $ejecutivos = [];
        $jefaturas = [];
        $zonales = [];
        $bancas=[];


        if (in_array($this->user->getValue('_rol'),Usuario::getZonalesBE())){
            $jefaturas = ZonalJefatura::getJefaturas($this->user->getValue('_zona'));
        }

         if(in_array($this->user->getValue('_rol'),Usuario::getBanca())){
            $zonales = ZonalJefatura::getZonales($this->user->getValue('_banca'));
            $jefaturas = ZonalJefatura::getJefaturas(null,$this->user->getValue('_banca'));
        }

        if (in_array($this->user->getValue('_rol'),Usuario::getDivisionBE())){
            $bancas= ZonalJefatura::getBancas();
            $zonales = ZonalJefatura::getZonales();
            $jefaturas = ZonalJefatura::getJefaturas();                       
        }

        


        $busqueda = [
            'periodo' => $request->get('periodo',\App\Entity\BE\SaldosDiarios::getParametros('PERIODO',null)),
            'ejecutivo' => $request->get('ejecutivo',null),
            'jefatura' => $request->get('jefatura',null),
            'zonal' => $request->get('zonal',$this->user->getValue('_zona')),
            'banca'=>$request->get('banca',$this->user->getValue('_banca')),
        ];

        \Debugbar::info($this->user);
        return view ('be.enote.index-jefe')
            ->with('productos',\App\Entity\BE\Producto::getAll())
            ->with('meses',OperacionesNuevas::getMesesOperacion())
            ->with('periodos',OperacionesNuevas::getMesesOperacion())
            ->with('busqueda',$busqueda)            
            ->with('bancas',$bancas)
            ->with('zonales',$zonales)
            ->with('jefaturas',$jefaturas)
            ->with('ejecutivos',$ejecutivos);        
    }

    public function resumen(Request $request) {

        /*Recibiras dos parámentros:*/
        //1: registro de ejecutivo
        //2: mes de consulta

        $jefatura = $request->get('jefatura');
        $zonal = $request->get('zonal');
        $banca = $request->get('banca');

 
        if($jefatura||$zonal||$banca){       
            if($jefatura){               
               $rol = \App\Entity\Usuario::ROL_JEFATURA_BE;//24
               //$jefatura= $this->user->getValue('_centro');     
               //dd($jefatura);
            }else{
                if($zonal){
                    $rol = \App\Entity\Usuario::ROL_GERENCIA_ZONAL_BE;//25;               
                }else{
                    if($banca){
                      $rol = \App\Entity\Usuario::ROL_GERENTE_DIVISION_BE;//26;
                    }else{
                      $rol = $this->user->getValue('_rol');
                    }        
                }    
            }             
        }else{
            $jefatura= $this->user->getValue('_centro');     
            $rol = $this->user->getValue('_rol');   
        }                                    
        //DD($jefatura);
        //TRAER VARIABLES DETALLE POR JEFES/ZONALES/DIVISION
          $busqueda = [
            'periodo' => $request->get('periodo',\App\Entity\BE\SaldosDiarios::getParametros('PERIODO',null)),
            'ejecutivo' => $request->get('ejecutivo',$this->user->getValue('_registro')),
            'jefatura' => $jefatura,
            'zonal' => $request->get('zonal',null),
            'banca'=>$request->get('banca',null),
            'rol'=>$rol,
        ];        

        //dd($busqueda);
        /*resultado resumen*/
        $resumenJefes = VencimientoAmortizacion ::getResumenJefes($this->user,$busqueda['periodo'],$request->get('ejecutivo',null),$busqueda['banca'],$busqueda['zonal'],$busqueda['jefatura'],$busqueda['rol']);
        \Debugbar::info($resumenJefes);
        return view ('be.enote.resumen-jefe')
            ->with('busqueda',$busqueda) 
            ->with('resumenJefes',$resumenJefes);            
    }
    
    function getDesembolsosCerteros(Request $request){
        return Datatables::of(OperacionesNuevas::getDesembolsosCerteros(
            $request->get('periodo'),
            $request->get('jefatura',null),
            $request->get('zonal',null),
            $request->get('banca',null)
        ))->make(true);
    }

     public function graficar(Request $request){        
        return SaldosDiarios::process($request->get('periodo',201803),$this->user->getValue('_registro'),$request->get('ejecutivo',null),$request->get('jefatura',null),$request->get('banca',null),$request->get('zonal',null),'jefe');
    }

    function getDesembolsosCotizacion(Request $request){
        return Datatables::of(OperacionesNuevas::getDesembolsosCotizacion(
            $request->get('periodo'),
            $request->get('jefatura',null),
            $request->get('zonal',null),
            $request->get('banca',null)
        ))->make(true);    
    }

    function getCaidasProyectadas(Request $request){
        return Datatables::of(Caidas::getCaidasProyectadas(
            $request->get('periodo'),
            $request->get('jefatura',null),
            $request->get('zonal',null),
            $request->get('banca',null)
        ))->make(true); 
        
    }

    function getOperacionesPerdidas(Request $request){
        return Datatables::of(OperacionesNuevas::getOperacionesPerdidas(
            $request->get('periodo'),
            $request->get('jefatura',null),
            $request->get('zonal',null),
            $request->get('banca',null)
        ))->make(true); 
    }

    function getDesembolsosCotizacionFuturos(Request $request){
        return Datatables::of(OperacionesNuevas::getDesembolsosCotizacionFuturos(
            $request->get('periodo'),
            $request->get('jefatura',null),
            $request->get('zonal',null),
            $request->get('banca',null)
        ))->make(true);
    } 
}