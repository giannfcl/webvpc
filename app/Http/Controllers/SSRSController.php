<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entity\Usuario as Usuario;
use App\Entity\BE\Lead as Lead;
use App\Entity\ComboEjecutivo as ComboEjecutivo;


class SSRSController extends Controller {
    
    public function saldos(Request $request) {
        
        $origen = 'VPCONNECT';
        $registro = $this->user->getValue('_registro');
        
        $fechaSD = ComboEjecutivo::getFecha()[0]->FECHA;
        
        $busqueda = [
            //'fecha' => $request->get('fecha', date('d/m/Y',strtotime("-2 days"))),
            'fecha' => $request->get('fecha', date('d/m/Y',strtotime($fechaSD))),
            'tipo' => $request->get('tipo','NEGOCIO'),
            'estado' => $request->get('estado','TODOS'),
            
            'ejecutivoProducto' => $request->get('ejecutivoProducto',null),
            
            'banca' => $request->get('banca',null),
            'zonal' => $request->get('zonal',null),
            'jefatura' => $request->get('jefatura',null),
            'ejecutivo' => $request->get('ejecutivo',null),
            
            'origen' => $origen,

        ];

        $tipos = array('NEGOCIO','RECUPERO');
        $estados = array('TODOS','VIGENTE','ATRASADA','REFINANCIADA');
        // print_r($busqueda);echo "<br>";
        $bancas = ComboEjecutivo::getBancas(false,$this->user);
        $zonales = ComboEjecutivo::getZonales(false,$this->user);
        $jefaturas = ComboEjecutivo::getJefaturas(false,false,$this->user);
        $ejecutivos = ComboEjecutivo::getEjecutivos(false,false,false,$this->user);
        // dd($bancas,$zonales,$jefaturas,$ejecutivos);
        if ($busqueda['ejecutivo']==NULL
            && in_array($this->user->getValue('_zona'),["BELZONAL1","BELZONAL2","BELZONAL3","BEPZONAL1","BEPZONAL2","BEPZONAL3"])
            && in_array($this->user->getValue('_rol'),[Usuario::ROL_EJECUTIVO_FARMER_BE,Usuario::ROL_EJECUTIVO_HUNTER_BE])
        ) {
            $busqueda['ejecutivo']=Lead::getSectoristaArbol($registro) ? Lead::getSectoristaArbol($registro)->COD_SECT_UNIQ : null;
            if ($busqueda['ejecutivo']) {
                $datosejecutivo = ComboEjecutivo::getByCodsectU($busqueda['ejecutivo']);
                $busqueda['banca']=$datosejecutivo->BANCA;
                $busqueda['zonal']=$datosejecutivo->ZONAL;
                if (in_array($this->user->getValue('_zona'),["BELZONAL2"]) ) {
                    $busqueda['jefatura']=$datosejecutivo->JEFATURA;
                }
            }
        }
        // print_r($busqueda);echo "<br>";
        // dd(1);
        $nomEjec = null;
        
        /*$o = \App\Model\ComboEjecutivo::select('ENCARGADO')->where('COD_SECT_UNIQ', $busqueda['ejecutivo'])->first();
        if (!is_null($o)) {
            $nomEjec = $o->ENCARGADO;
        }*/
        if ($busqueda['ejecutivo']){
            $nomEjec = ComboEjecutivo::getNombreEncargado($busqueda['ejecutivo'])[0]->ENCARGADO;
        }
        
        if ($this->user->getValue('_rol')==Usuario::ROL_JEFATURA_BE) {
            if (in_array($this->user->getValue('_zona'),["BELZONAL1","BELZONAL2","BELZONAL3"])) {
                $busqueda['banca'] = ComboEjecutivo::BEL;
                $busqueda['zonal'] = ComboEjecutivo::BELZONAL2_CODIGOS[$this->user->getValue('_zona')];
                $busqueda['jefatura'] = $this->user->getValue("_nombre");
            }
        }
        if (in_array($this->user->getValue('_rol'),[Usuario::ROL_EJECUTIVO_FARMER_BE,Usuario::ROL_EJECUTIVO_HUNTER_BE])) {
            if (in_array($this->user->getValue('_zona'),["BELZONAL1","BELZONAL2","BELZONAL3"])) {
                $busqueda['banca'] = ComboEjecutivo::BEL;
                if (in_array($this->user->getValue('_zona'),["BELZONAL1"])) {
                    $busqueda['zonal'] = ComboEjecutivo::CHACARILLA;
                    if (!isset($busqueda['ejecutivo']) || $busqueda['ejecutivo']==null) {
                        $busqueda['ejecutivo'] = Lead::getSectoristaArbol($registro) ? Lead::getSectoristaArbol($registro)->COD_SECT_UNIQ : null;
                    }
                }
                if (in_array($this->user->getValue('_zona'),["BELZONAL2"])) {
                    $busqueda['zonal'] = ComboEjecutivo::SANISIDRO;
                    if (!isset($busqueda['jefatura'])) {
                        $busqueda['jefatura'] = ComboEjecutivo::JEFATURASI;
                    }
                }
                if (in_array($this->user->getValue('_zona'),["BELZONAL3"])) {
                    $busqueda['zonal'] = ComboEjecutivo::RICARDOPALMA;
                    if (!isset($busqueda['ejecutivo']) || $busqueda['ejecutivo']==null) {
                        $busqueda['ejecutivo'] = Lead::getSectoristaArbol($registro) ? Lead::getSectoristaArbol($registro)->COD_SECT_UNIQ : null;
                    }
                }
            }
        }
        if (in_array($this->user->getValue('_rol'),[Usuario::ROL_EJECUTIVO_FARMER_BE,Usuario::ROL_EJECUTIVO_HUNTER_BE])) {
            if (in_array($this->user->getValue('_zona'),["BEPZONAL1","BEPZONAL2","BEPZONAL3"])) {
                $busqueda['banca'] = ComboEjecutivo::BEP;
                if (in_array($this->user->getValue('_zona'),["BEPZONAL1"])) {
                    $busqueda['zonal'] = ComboEjecutivo::NORTE;
                    if (!isset($busqueda['ejecutivo']) || $busqueda['ejecutivo']==null) {
                        $busqueda['ejecutivo'] = Lead::getSectoristaArbol($registro) ? Lead::getSectoristaArbol($registro)->COD_SECT_UNIQ : null;
                    }
                }
                if (in_array($this->user->getValue('_zona'),["BEPZONAL2"])) {
                    $busqueda['zonal'] = ComboEjecutivo::CENTRO;
                    if (!isset($busqueda['ejecutivo']) || $busqueda['ejecutivo']==null) {
                        $busqueda['ejecutivo'] = Lead::getSectoristaArbol($registro) ? Lead::getSectoristaArbol($registro)->COD_SECT_UNIQ : null;
                    }
                }
                if (in_array($this->user->getValue('_zona'),["BEPZONAL3"])) {
                    $busqueda['zonal'] = ComboEjecutivo::SUR;
                    if (!isset($busqueda['ejecutivo']) || $busqueda['ejecutivo']==null) {
                        $busqueda['ejecutivo'] = Lead::getSectoristaArbol($registro) ? Lead::getSectoristaArbol($registro)->COD_SECT_UNIQ : null;
                    }
                }
            }
        }
        $agrupacion = $busqueda['zonal'];

        if($busqueda['banca'] == 'BC') { //Con filtro de banca con el valor de BC
            //$reporte = "rptSaldosDiariosBC";
            $reporte = "http://b29310t16w7/Reports_SQL2008/Pages/Report.aspx?ItemPath=%2fReportesGestion%2frptSaldosDiariosBC";
            
            if($busqueda['ejecutivo'] != NULL) { //banca con el valor de BC y con el filtro que seleccionó ejecutivo
                //$reporte = "rptSaldosDiariosBCEjecutivo";
                $reporte = "http://b29310t16w7/Reports_SQL2008/Pages/Report.aspx?ItemPath=%2fReportesGestion%2frptSaldosDiariosBCEjecutivo";
            }
            
        } else if(in_array ($busqueda['banca'], array('BE'))) { //Con filtro de banca con el valor de BE (no BEL ni BEP)
            //$reporte = "rptSaldosDiariosBE";
            $reporte = "http://b29310t16w7/Reports_SQL2008/Pages/Report.aspx?ItemPath=%2fReportesGestion%2frptSaldosDiariosBE";
            
        } else if(in_array ($busqueda['banca'], array('BEL'))) { //Con filtro de banca con el valor de BEL
            //$reporte = "rptSaldosDiariosBEL";
            $reporte = "http://b29310t16w7/Reports_SQL2008/Pages/Report.aspx?ItemPath=%2fReportesGestion%2frptSaldosDiariosBEL";
            
            if($busqueda['ejecutivo'] != NULL && $busqueda['jefatura'] == NULL){
                //$reporte = "rptSaldosDiariosBEEjecutivo";
                $reporte = "http://b29310t16w7/Reports_SQL2008/Pages/Report.aspx?ItemPath=%2fReportesGestion%2frptSaldosDiariosBEEjecutivo";
            
            } else if($busqueda['ejecutivo'] != NULL && $busqueda['jefatura'] != NULL){
                //$reporte = "rptSaldosDiariosBEJefeEjecutivo";
                $agrupacion = $busqueda['jefatura'];
                $reporte = "http://b29310t16w7/Reports_SQL2008/Pages/Report.aspx?ItemPath=%2fReportesGestion%2frptSaldosDiariosBEJefeEjecutivo";
                
            } else if($busqueda['jefatura'] != NULL){
                //$reporte = "rptSaldosDiariosBEJefe";
                $nomEjec = $busqueda['jefatura'];
                $reporte = "http://b29310t16w7/Reports_SQL2008/Pages/Report.aspx?ItemPath=%2fReportesGestion%2frptSaldosDiariosBEJefe";
            }
            
        } else if(in_array ($busqueda['banca'], array('BEP'))) { //Con filtro de banca con el valor de BEP
            //$reporte = "rptSaldosDiariosBEP";
            $reporte = "http://b29310t16w7/Reports_SQL2008/Pages/Report.aspx?ItemPath=%2fReportesGestion%2frptSaldosDiariosBEP";
            
            if($busqueda['jefatura'] != NULL){
                //$reporte = "rptSaldosDiariosBEJefe";
                $nomEjec = $busqueda['jefatura'];
                $reporte = "http://b29310t16w7/Reports_SQL2008/Pages/Report.aspx?ItemPath=%2fReportesGestion%2frptSaldosDiariosBEJefe";
            }
            
            if($busqueda['ejecutivo'] != NULL){
                //$reporte = "rptSaldosDiariosBEEjecutivo";
                $reporte = "http://b29310t16w7/Reports_SQL2008/Pages/Report.aspx?ItemPath=%2fReportesGestion%2frptSaldosDiariosBEEjecutivo";
            }            
            
        } else {
            //$reporte = "rptSaldosDiariosPrincipal";
            $reporte = "http://b29310t16w7/Reports_SQL2008/Pages/Report.aspx?ItemPath=%2fReportesGestion%2frptSaldosDiariosPrincipal";
        }
        // dd($busqueda,$reporte);
        return view('ssrs.saldos')
            ->with('usuario', $this->user)

            ->with('busqueda', $busqueda)

            ->with('tipos', $tipos)
            ->with('estados', $estados)
            ->with('bancas', $bancas)
            ->with('zonales', $zonales)
            ->with('jefaturas', $jefaturas)
            ->with('ejecutivos', $ejecutivos)

            ->with('agrupacion', $agrupacion)
            ->with('nomEjec', $nomEjec)

            ->with('url', $reporte);
        
    }
    
    public function variaciones(Request $request) {
        
        $origen = 'VPCONNECT';
        $fechaSD = ComboEjecutivo::getFecha()[0]->FECHA;
        
        $busqueda = [
            //'fecha' => $request->get('fecha', date('d/m/Y',strtotime("-2 days"))),
            'fecha' => $request->get('fecha', date('d/m/Y',strtotime($fechaSD))),
            'tipoVar' => $request->get('tipoVar','Mensual'),
            'segmento' => $request->get('segmento','TODOS'),
            'banca' => $request->get('banca','VPC'),
            'zonal' => $request->get('zonal','TODOS'),
            'jefatura' => $request->get('jefatura','TODOS'),
            'ejecutivo' => $request->get('ejecutivo','TODOS'),
            
            'origen' => $origen,
        ];
        
        $tipoVars = array('Mensual','Diario','Semanal','Anual');

        $bancas = ComboEjecutivo::getBancas(TRUE);
        $zonales = ComboEjecutivo::getZonales();
        $jefaturas = ComboEjecutivo::getJefaturas();
        $ejecutivos = ComboEjecutivo::getEjecutivos();
        
        if ($busqueda['banca']=='VPC' || $busqueda['banca']=='') {
            $b = 'VP';
        } else {
            $b = $busqueda['banca'];
        }
        
        if ($busqueda['zonal']=='') {
            $z = 'TODOS';
        } else {
            $z = $busqueda['zonal'];
        }
        
        if ($busqueda['ejecutivo']=='') {
            $e = 'TODOS';
        } else {
            $e = $busqueda['ejecutivo'];
        }
            
        $ssrs = [
            'tipoVar' => $busqueda['tipoVar'],
            'banca' => $b,
            'zonal' => $z,
            'segmento' => 'TODOS',
            'ejecutivo' => $e,
        ];
        
        //$reporte = "rptSaldosDiariosVar";
        $reporte = "http://b29310t16w7/Reports_SQL2008/Pages/Report.aspx?ItemPath=%2fReportesGestion%2frptSaldosDiariosVar";
        
        return view('ssrs.variaciones')
            ->with('usuario',$this->user)
                
            ->with('busqueda',$busqueda)
            
            ->with('tipoVars', $tipoVars)
            ->with('bancas', $bancas)
            ->with('zonales', $zonales)
            ->with('jefaturas', $jefaturas)
            ->with('ejecutivos', $ejecutivos)
                
            ->with('ssrs',$ssrs)

            ->with('url',$reporte);
    }
    
    public function miCliente(Request $request) {
        
        $url_powerbi = config('app.micliente');
        
        return view('ssrs.micliente')
            ->with('url', $url_powerbi);
    }

}