<?php 
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Entity\Ecosistema as Ecosistema;
use App\Entity\Reciprocidad as Reciprocidad;
use App\Entity\BE\ZonalJefatura as ZonalJefatura;
use App\Entity\Gestioncartera as Gestioncartera;
use App\Entity\Usuario as Usuario;
use Jenssegers\Date\Date as Carbon;
use Yajra\DataTables\DataTables as Datatables;
use App\Entity\BE\SaldosDiarios as SaldosDiarios; //Se borrrará
use DB;

class EcosistemaController extends Controller{

public function ecosistemaPrincipal(Request $request){
    $entidad = new Reciprocidad();
   return view('ecosistema')
          ->with('tiposcompromiso',$entidad->getTiposCompromiso());
 }

 public function ReciprocidadPrincipal(Request $request){
    $entidad = new Reciprocidad();
    
    $elementostablatop_pri=[];
    $elementostablatop_det=[];
    $elementostablabutton_pri=[];
    $elementostablabutton_det=[];
    $registros_filtrar=null;
    $bancas     =   null;
    $zonales    =   null;
    $jefes      =   null;
    $ejecutivos =   null;
    $valores=[
          'ROL'=>$this->user->getValue('_rol'),
          'REGISTRO'=>$this->user->getValue("_registro"),
          'RazonSocial'=>$request->get('RazonSocial',null),
          'COD_UNICO'=>$request->get('codUnico',null),
          'FILTRO1' =>$request->get('filtro1',null),
          'FILTRO2' =>$request->get('filtro2',null),
          'FILTRO3' =>$request->get('filtro3',null),
          'FILTRO4' =>$request->get('filtro4',null),
    ]  ;

    
    $id   =  $this->user->getValue('_idgc');
    $ROL = $valores['ROL'] ;
    $usuario = Gestioncartera::getArbol($id);

    if(!isset($usuario)){
      return redirect()->route('saldos');
    }
    if (!isset($banca)) {
        $banca      =   json_decode(Gestioncartera::getBanca($usuario->ID), true)['0']['BANCA'];
    }
    if (!isset($zonal)) {
        $zonal      =   json_decode(Gestioncartera::getZonal($usuario->ID), true)['0']['NOMBRE_ZONAL'];
    }
    if (!isset($jefe)) {
        $jefe       =   json_decode(Gestioncartera::getJefe($usuario->ID), true)['0']['NOMBRE_JEFE'];
    }
    if (!isset($ejecutivo)) {
        $ejecutivo  =   json_decode(Gestioncartera::getEjecutivo($usuario->ID), true)['0']['ENCARGADO'];
    }
    $stop = false;
    if ($banca == null) {
        $bancas     =   json_decode(Gestioncartera::getBancas(), true);
    }
    if ($zonal == null && !$stop) {
        $banca = $banca == null?'BC':$banca;
        $zonales    =   json_decode(Gestioncartera::getZonales($banca), true);
        $stop = true;
    }
    if ($jefe == null && !$stop) {
        $jefes      =   json_decode(Gestioncartera::getJefes($banca, $zonal), true);
    }
    if ($ejecutivo == null && !$stop) {
        $ejecutivos =   json_decode(Gestioncartera::getEjecutivos($banca, $zonal, $jefe), true);
        $stop = true;
    }
   

    if ($valores['FILTRO1']==null) {
      $valores['FILTRO1']=$banca;
    };

    if ($valores['FILTRO2']==null) {
      $valores['FILTRO1']=$banca;
      $valores['FILTRO2']=$zonal;
    };

    if ($valores['FILTRO3']==null) {
      $valores['FILTRO1']=$banca;
      $valores['FILTRO2']=$zonal;
      $valores['FILTRO3']=$jefe;
    };

    if ($valores['FILTRO4']==null) {
      $valores['FILTRO1']=$banca;
      $valores['FILTRO2']=$zonal;
      $valores['FILTRO3']=$jefe;
      $valores['FILTRO4']=$ejecutivo;
    };



    $valores_filtro=[
       "banca"=>$valores['FILTRO1'],
       "zonal"=>$valores['FILTRO2'],
       "jefe"=>$valores['FILTRO3'],
       "ejecutivo"=>$valores['FILTRO4'],
    ];

    //dd($valores_filtro);

    $usuario_filtro = Reciprocidad::getUsuario2($valores_filtro);
    $usuario_filtro=$usuario_filtro->REGISTRO;

    $registros=Reciprocidad::Registros_finales($usuario_filtro);

    if (is_array($registros)){
      foreach ($registros as $value) {
        $registros_filtrar[]=$value->REGISTRO;
      }
    }

    $datos_filtros_tablas=[
      'codUnico' =>$valores['COD_UNICO'],
      'RazonSocial' =>$valores['RazonSocial'],
    ];

    if ($registros_filtrar) {
      if(sizeof($registros_filtrar)<1)
      {
          $registro_f=[$valores['REGISTRO']];
      }
      else
      {
          $registro_f=$registros_filtrar;
      }
    }else{
      $registro_f=[$valores['REGISTRO']];
    }

    $entidad->rating($registro_f);

    foreach ($entidad->Tabla_transacciones2() as $arr) {
      if ($arr->FLG_INICIAL==1) {
        $elementostablatop_pri[$arr->COD_UNICO]=$arr;
      } else {
        $elementostablatop_det[$arr->COD_UNICO][]=$arr;
      }
    }
    foreach ($entidad->Tabla_transacciones3() as $arr) {
      if ($arr->FLG_INICIAL==1) {
        $elementostablabutton_pri[$arr->COD_UNICO]=$arr;
      } else {
        $elementostablabutton_det[$arr->COD_UNICO][]=$arr;
      }
    }
    
    //$zonal=$valores_filtro['zonal'];
   
    return view('reciprocidad')
        ->with('bancas', $bancas)
        ->with('zonales', $zonales)
        ->with('jefes', $jefes)
        ->with('ejecutivos', $ejecutivos)
        ->with('banca', $banca)
        ->with('zonal', $zonal)
        ->with('jefe', $jefe)
        ->with('ejecutivo', $ejecutivo)
        ->with('usuario', $usuario)
        ->with('ROL', $ROL)
        ->with('valores_filtro', $valores_filtro)
        ->with('tiposcompromiso',$entidad->getTiposCompromiso())
        ->with('tiposcompromiso2',$entidad->getTiposCompromiso2())  
        ->with('lead',$entidad->Razones_sociales($registro_f))
        ->with('elementostabla',$entidad->Tabla_transacciones($datos_filtros_tablas,$registro_f))
        ->with('elementostablatop',$elementostablatop_pri)
        ->with('elementostablatopdet',$elementostablatop_det)
        ->with('elementostablabutton',$elementostablabutton_pri)
        ->with('elementostablabuttondet',$elementostablabutton_det);
 }

  public function getUsuarioReciprocidad(Request $request)
    {
        $datos = $request->all();
        $usuario = Reciprocidad::getUsuario2($datos);
        $usuario=$usuario->REGISTRO;
        $usuario2=Reciprocidad::Registros_finales($usuario);
        return $usuario2;
    }


public function guardarDatos(Request $request)
 {
  $entidad = new Reciprocidad();
  $registro   =  $this->user->getValue('_registro');
  return $entidad->guardarDatos($request->all(),$registro);
 }

public function Resultados_simular(Request $request)
{
  $entidad = new Reciprocidad();
  //dd($entidad ->Resultados_simular($request->all()));
  return($entidad ->Resultados_simular($request->all()));
}

public function guardarDatos2(Request $request)
 {
  $entidad = new Reciprocidad();
  $registro   =  $this->user->getValue('_registro');
  return $entidad->guardarDatos2($request->all(),$registro);
 }

 public function seguimientoIngresado(Request $request){    

   $fechaFinal=new Carbon('last day of last month');
   $busqueda = [
    'page' => $request->get('page',1),
    'ejecutivo' => $request->get('ejecutivo',null),
    'jefatura' => $request->get('jefatura',null),
    'zonal' => $request->get('zonal',null),
    'banca'=>$request->get('banca',null),
    'fechaAvance'=>$request->get('fechaAvance',Carbon::now()->subDays(1)->format('Y-m-d')),
    'fechaCorte'=>$fechaFinal->format('Y-m-d')
  ];


  $resumen=Ecosistema::seguimientoIngresado($this->user,$busqueda);
  $resumenAgrupado=Ecosistema::seguimientoIngresadoAgrupado($this->user,$busqueda);

  $resumenTotales=Ecosistema::seguimientoIngresadoTotales($this->user,$busqueda);


  $ejecutivos = [];
  $jefaturas = [];
  $zonales = [];
  $bancas=[];

  /*if (in_array($this->user->getValue('_rol'),Usuario::getEjecutivosProductoBE())){
    $ejecutivos = Usuario::getFarmersHunters(null,$this->user->getValue('_zona'));
  }
  if (in_array($this->user->getValue('_rol'),Usuario::getJefaturasBE())){
    $ejecutivos = Usuario::getFarmersHunters(null,null,$this->user->getValue('_centro'));
  }
  if (in_array($this->user->getValue('_rol'),Usuario::getZonalesBE())){
    $jefaturas = ZonalJefatura::getJefaturas($this->user->getValue('_zona'));
    $ejecutivos = Usuario::getFarmersHunters(null,$this->user->getValue('_zona'));
  }

  if(in_array($this->user->getValue('_rol'),Usuario::getBanca())){
    $zonales = ZonalJefatura::getZonales($this->user->getValue('_banca'));
    $jefaturas = ZonalJefatura::getJefaturas(null,$this->user->getValue('_banca'));
    $ejecutivos=Usuario::getFarmersHunters($this->user->getValue('_banca'));
  }

  if (in_array($this->user->getValue('_rol'),Usuario::getDivisionBE())){*/
    $bancas= ZonalJefatura::getBancas();
    $zonales = ZonalJefatura::getZonales();
    $jefaturas = ZonalJefatura::getJefaturas();
    $ejecutivos = Usuario::getFarmersHunters();
    /*}*/        

        //dd($busqueda);
    return view('ecosistema.seguimiento-ingresado')
    ->with('busqueda',$busqueda)
    ->with('resumen',$resumen)
    ->with('resumenAgrupado',$resumenAgrupado)
    ->with('totales',$resumenTotales)
    ->with('ejecutivos',$ejecutivos)
    ->with('jefaturas',$jefaturas)
    ->with('zonales',$zonales)
    ->with('bancas',$bancas)
    ->with('usuario',$this->user);       
  }

  function getStatusProveedores(Request $request){  

    $fechaFinal=new Carbon('last day of last month');
    $busqueda = [
      'jefatura' => $request->get('jefatura',null),
      'zonal' => $request->get('zonal',null),
      'banca'=>$request->get('banca',null),
      'fechaAvance'=>$request->get('fechaAvance',Carbon::now()->subDays(1)->format('Y-m-d')),
      'fechaCorte'=>$fechaFinal->format('Y-m-d')
    ];

    $consulta=Ecosistema::getStatusProveedoresIngresados($busqueda);

    return Datatables::of($consulta)->make(true);    
  }

  public function graficarIngresados(Request $request){

    $fechaFinal=new Carbon('last day of last month');
    $busqueda = [
      'jefatura' => $request->get('jefatura',null),
      'zonal' => $request->get('zonal',null),
      'banca'=>$request->get('banca',null),
      'fechaAvance'=>$request->get('fechaAvance',Carbon::now()->subDays(1)),
      'fechaCorte'=>$fechaFinal
    ];        

    return Ecosistema::graficarIngresados($busqueda);
  }


  public function seguimientoRecibido(Request $request){    

    $fechaFinal=new Carbon('last day of last month');
   $busqueda = [
    'page' => $request->get('page',1),
    'ejecutivo' => $request->get('ejecutivo',null),
    'jefatura' => $request->get('jefatura',null),
    'zonal' => $request->get('zonal',null),
    'banca'=>$request->get('banca',null),
    'fechaAvance'=>$request->get('fechaAvance',Carbon::now()->subDays(1)->format('Y-m-d')),
    'fechaCorte'=>$fechaFinal->format('Y-m-d')
  ];


  $resumen=Ecosistema::seguimientoIngresado($this->user,$busqueda);
  $resumenTotales=Ecosistema::seguimientoIngresadoTotales($this->user,$busqueda);

  //dd($resumenTotales);
  $ejecutivos = [];
  $jefaturas = [];
  $zonales = [];
  $bancas=[];

  /*if (in_array($this->user->getValue('_rol'),Usuario::getEjecutivosProductoBE())){
    $ejecutivos = Usuario::getFarmersHunters(null,$this->user->getValue('_zona'));
  }
  if (in_array($this->user->getValue('_rol'),Usuario::getJefaturasBE())){
    $ejecutivos = Usuario::getFarmersHunters(null,null,$this->user->getValue('_centro'));
  }
  if (in_array($this->user->getValue('_rol'),Usuario::getZonalesBE())){
    $jefaturas = ZonalJefatura::getJefaturas($this->user->getValue('_zona'));
    $ejecutivos = Usuario::getFarmersHunters(null,$this->user->getValue('_zona'));
  }

  if(in_array($this->user->getValue('_rol'),Usuario::getBanca())){
    $zonales = ZonalJefatura::getZonales($this->user->getValue('_banca'));
    $jefaturas = ZonalJefatura::getJefaturas(null,$this->user->getValue('_banca'));
    $ejecutivos=Usuario::getFarmersHunters($this->user->getValue('_banca'));
  }

  if (in_array($this->user->getValue('_rol'),Usuario::getDivisionBE())){*/
    $bancas= ZonalJefatura::getBancas();
    $zonales = ZonalJefatura::getZonales();
    $jefaturas = ZonalJefatura::getJefaturas();
    $ejecutivos = Usuario::getFarmersHunters();
    /*}*/        

        //dd($busqueda);
    return view('ecosistema.seguimiento-recibido')
    ->with('busqueda',$busqueda)
    ->with('resumen',$resumen)
    ->with('totales',$resumenTotales)
    ->with('ejecutivos',$ejecutivos)
    ->with('jefaturas',$jefaturas)
    ->with('zonales',$zonales)
    ->with('bancas',$bancas)
    ->with('usuario',$this->user);       
}

public function getZonales(Request $request)
    {
        $datos = $request->all();
        $zonales = json_decode(Gestioncartera::getZonales($datos['banca']), true);
        echo json_encode($zonales);
    }
public function getJefes(Request $request)
    {
        $datos = $request->all();

        $FILTRO1 =$request->get('filtro1',null);
          $FILTRO2 =$request->get('filtro2',null);
          $FILTRO3 =$request->get('filtro3',null);
          $FILTRO4 =$request->get('filtro4',null);

          dd($FILTRO2);

        $jefes = json_decode(Gestioncartera::getJefes(isset($datos['banca'],$FILTRO1), isset($datos['zonal'],$FILTRO2)), true);
        echo json_encode($jefes);
    }

public function obtenerEcosistema(Request $request){ 		     
  return Ecosistema::getGrafoEcosistema();
}

public function obtenerEcosistemaNodo(Request $request){    
  $ruc=$request->get('rucNodo');
  return Ecosistema::getNodoEcosistema($ruc);
}  

public function buscarRucEcosistema(Request $request){ 		
  $cuBuscar=$request->get('cuBuscar');
  $razonBuscar=$request->get('razonBuscar');
      //dd($cuBuscar);
  return Ecosistema::buscarRucEcosistema($cuBuscar,$razonBuscar);
}

public function obtenerEmpresas(Request $request){    
  return Ecosistema::obtenerEmpresas();
}

public function buscarNumDoc(Request $request){
     $datos = $request->all();
      $registro   =  $this->user->getValue('_registro');
      $resul = Reciprocidad::buscarNumDoc($datos['codUnico'],$registro);
      // echo json_encode($resul);
      return $resul;
}
  /*public function enviarMail(){
      $data=array(
          'nombresCompletos'=>'Luis Arana Motta',
          'email'=>'luis.arana@pucp.pe',          
      );)
      \Mail::send('emails.ecosistema',$data,function($message)use($data){
          $message->from('luis.arana@pucp.pe','Buzón de Desarrollo e Inteligencia Comercial');
          $message->to($data['email'])->subject('Nuevo Prospecto Ecosistema IBK');
    });
  }*/
}