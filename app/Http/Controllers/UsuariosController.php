<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Entity\Usuario as Usuario;
use App\Entity\Banca as Banca;

use Yajra\Datatables\Datatables as Datatables;
use Excel;
use DB;

class UsuariosController extends Controller {


    public function index() {
    	$entidad = new Usuario();
    	$roles = $entidad->getRoles();
    	$jsroles=[];
    	foreach ($roles as $key => $value) {
    		$jsroles[$value->ID_ROL]=$value->NOMBRE;
    	}
    	// dd($roles,$jsroles);
    	return view("usuarios.index")
    			->with('roles',$roles)
    			->with('jsroles',$jsroles)
    			->with('tiposbancas',Banca::getTiposBancas());
    }

    public function getAllUsuarios(Request $request)
    {
        $usuario = $request->get("registro",null);
    	return Datatables::of(Usuario::getUsuario($usuario))->make(true);
    }

    public function getinfousuario(Request $request)
    {
        return response()->json(Usuario::getUsuario($request->get("registro",null)));
    }

    public function addUsuario(Request $request)
    {
    	$entidad = new Usuario();
    	return response()->json([$entidad->addUsuario($request->all()),$entidad->getMessage()]);
    }

    public function updateUsuario(Request $request)
    {
    	$entidad = new Usuario();
    	return response()->json([$entidad->updateUsuario($request->all()),$entidad->getMessage()]);
    }

    public function clickruta(Request $request)
    {
        //dd($request->header('User-Agent'));
        $entidad = new Usuario();
        return response()->json([$entidad->clickruta($request->all(),$this->user->getValue('_registro'),$request->header('User-Agent')),$entidad->getMessage()]);
    }
}