<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entity\Gestioncartera as Gestioncartera;
use App\Entity\ResumenHunter as ResumenHunter;
use App\Entity\BE\ReporteEjecutivos as Reporte;


class ResumenHunterController extends Controller
{

    public function index()
    {
        //Listas para los combos
        $jefes      =   null;
        $jefesResumen = null;
        //Obtener registro para filtro inicial
        $id_centro   =  $this->user->getValue('_centro');
        $id_gc   =  $this->user->getValue('_idgc');
        $jefe       =   Gestioncartera::getJefe($id_gc)?json_decode(Gestioncartera::getJefe($id_gc), true)['0']['NOMBRE_JEFE']:null;
        if ($jefe == null) {
            $jefes      =   json_decode(ResumenHunter::getJefes('BE', 'ZONAL SAN ISIDRO'), true);
            $jefesResumen      =   json_decode(ResumenHunter::getJefesResumen($jefes), true);
            array_unshift($jefesResumen, array("ID_CENTRO" => $id_centro, "NOMBRE" => "TODOS"));
        }
        $entidad = new ResumenHunter();
        $fecha = $entidad->getFecha();
        return view('ResumenHunter')
            ->with('jefes', $jefesResumen)
            ->with('fecha', $fecha)
            ->with('id_centro', $id_centro);
    }
    public function index2(Request $request)
    {
        $data = $request->all();
        $id_centro = $data['ID_CEMTRO'];
        $entidad = new ResumenHunter();
        return json_encode($entidad->getBEResumenBanca($id_centro), true);
    }
    public function resumenTopGraphics(Request $request)
    {
        $data = $request->all();
        $id_centro = $data['ID_CENTRO'];
        $entidad = new ResumenHunter();
        return json_encode($entidad->getBEResumenBanca($id_centro), true);
    }
    public function resumenVisitas(Request $request)
    {
        $data = $request->all();
        $id_centro = $data['ID_CENTRO'];
        $entidad = new ResumenHunter();
        return json_encode($entidad->getRankingVisitas($id_centro), true);
    }
    public function resumenVisitasFecha(Request $request)
    {
        $entidad = new ResumenHunter();
        return json_encode($entidad->getFecha(), true);
    }
    public function getRankingEmpresas(Request $request)
    {
        $data = $request->all();
        $id_centro = $data['ID_CENTRO'];
        $top = $data['TOP'];
        $login = $data['LOGIN'];
        if ($login == "true") {
            $id_centro =  $this->user->getValue('_centro');
        }
        $entidad = new ResumenHunter();
        $res = json_encode($entidad->getRankingEmpresas($id_centro, $top), true);
        return $res;
    }
    public function resumenVisitasEliminado(Request $request)
    {
        $data = $request->all();
        $id_centro = $data['ID_CENTRO'];
        $entidad = new ResumenHunter();
        return json_encode($entidad->getRankingVisitasEliminado($id_centro), true);
    }
    public function getRankingEmpresasEliminado(Request $request)
    {
        $data = $request->all();
        $id_centro = $data['ID_CENTRO'];
        $login = $data['LOGIN'];
        $top = $data['TOP'];
        if ($login == "true") {
            $id_centro =  $this->user->getValue('_centro');
        }
        $entidad = new ResumenHunter();
        $res = json_encode($entidad->getRankingEmpresasEliminado($id_centro, $top), true);
        return $res;
    }
    public function getCantLeads(Request $request)
    {
        $data = $request->all();
        $id_centro = $data['ID_CENTRO'];
        $entidad = new ResumenHunter();
        $res = json_encode($entidad->getCantLeads($id_centro), true);
        return $res;
    }
}
