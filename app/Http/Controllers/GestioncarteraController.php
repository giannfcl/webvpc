<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entity\Gestioncartera as Gestioncartera;


class GestioncarteraController extends Controller
{

    public function index()
    {
        //Listas para los combos
        $bancas     =   null;
        $zonales    =   null;
        $jefes      =   null;
        $ejecutivos =   null;

        //Obtener registro para filtro inicial
        $id   =  $this->user->getValue('_idgc');
        $ROL =  $this->user->getValue('_rol');
        $usuario = Gestioncartera::getArbol($id);
        if(!isset($usuario)){
          return redirect()->route('saldos');
        }
        if (!isset($banca)) {
            $banca      =   json_decode(Gestioncartera::getBanca($usuario->ID), true)['0']['BANCA'];
        }
        if (!isset($zonal)) {
            $zonal      =   json_decode(Gestioncartera::getZonal($usuario->ID), true)['0']['NOMBRE_ZONAL'];
        }
        if (!isset($jefe)) {
            $jefe       =   json_decode(Gestioncartera::getJefe($usuario->ID), true)['0']['NOMBRE_JEFE'];
        }
        if (!isset($ejecutivo)) {
            $ejecutivo  =   json_decode(Gestioncartera::getEjecutivo($usuario->ID), true)['0']['ENCARGADO'];
			// dd($ejecutivo);
        }
        //Obtener los campos segun el registro
        $stop = false;
        if ($banca == null) {
            $bancas     =   json_decode(Gestioncartera::getBancas(), true);
        }
        if ($zonal == null && !$stop) {
            $banca = $banca == null?'BC':$banca;
            $zonales    =   json_decode(Gestioncartera::getZonales($banca), true);
            // $stop=true;
        }
        if ($jefe == null && !$stop) {
            $jefes      =   json_decode(Gestioncartera::getJefes($banca, $zonal), true);
            // $stop=true;
        }
        if ($ejecutivo == null && !$stop) {
            $ejecutivos =   json_decode(Gestioncartera::getEjecutivos($banca, $zonal, $jefe), true);
			// dd($ejecutivos);
        }
        $id = json_decode(Gestioncartera::getUsuario2(array('banca' => $banca, 'zonal' => $zonal, 'jefe' => $jefe, 'ejecutivo' => $ejecutivo)), true)[0]['ID'];
        
        $id = ($id=='VPC'?'BC':$id);
        $tipoFrecuencia =  json_decode(Gestioncartera::tipoFrecuencia($id), true);
        $tipoProductos =  json_decode(Gestioncartera::tipoProducto($banca, $zonal, $jefe, $ejecutivo), true);
        $tipoProductosBtm =  json_decode(Gestioncartera::tipoProductosBtm($id), true);
        $tipoFrecuenciaMERCADO =  json_decode(Gestioncartera::getTopBottomMERCADOFILTRO2($id), true);
        $tipoBANCOMERCADO =  json_decode(Gestioncartera::getTopBottomMERCADOFILTRO1($id), true);
        
        return view('GestionCartera')
            ->with('bancas', $bancas)
            ->with('zonales', $zonales)
            ->with('jefes', $jefes)
            ->with('ejecutivos', $ejecutivos)
            ->with('banca', $banca)
            ->with('zonal', $zonal)
            ->with('jefe', $jefe)
            ->with('ejecutivo', $ejecutivo)
            ->with('tipoProductosBtm', $tipoProductosBtm)
            ->with('tipoFrecuencia', $tipoFrecuencia)
            ->with('tipoProductos', $tipoProductos)
            ->with('tipoFrecuenciaMERCADO', $tipoFrecuenciaMERCADO)
            ->with('tipoBANCOMERCADO', $tipoBANCOMERCADO)
            ->with('usuario', $usuario)
            ->with('ROL', $ROL);
    }

    public function heatAnalytic(Request $request)
    {
        $datos = $request->all();
        // $registro   =  $this->user->getValue('_registro');
        // if (!isset($banca)) {
        //     $banca      =   json_decode(Gestioncartera::getBanca($registro), true)['0']['BANCA'];
        // }
        // if (!isset($zonal)) {
        //     $zonal      =   json_decode(Gestioncartera::getZonal($registro), true)['0']['NOMBRE_ZONAL'];
        // }
        // if (!isset($jefe)) {
        //     $jefe       =   json_decode(Gestioncartera::getJefe($registro), true)['0']['NOMBRE_JEFE'];
        // }
        // if (!isset($ejecutivo)) {
        //     $ejecutivo  =   json_decode(Gestioncartera::getEjecutivo($registro), true)['0']['ENCARGADO'];
        // }
        //$id = json_decode(Gestioncartera::getUsuario2(array('banca' => $banca, 'zonal' => $zonal, 'jefe' => $jefe, 'ejecutivo' => $ejecutivo)), true)[0]['ID'];
        // Gestioncartera::insertHA($datos, $registro, $id);
    }

    public function getZonales(Request $request)
    {
        $datos = $request->all();
        $zonales = json_decode(Gestioncartera::getZonales($datos['banca']), true);
        echo json_encode($zonales);
    }
    public function getJefes(Request $request)
    {
        $datos = $request->all();
        $jefes = json_decode(Gestioncartera::getJefes($datos['banca'], $datos['zonal']), true);
        echo json_encode($jefes);
    }
    public function DetalleClienteEvolutivo(Request $request)
    {
        $datos = $request->all();
        $evolutivo = Gestioncartera::DetalleClienteEvolutivo($datos);
        echo json_encode($evolutivo);
    }
    public function DetalleClienteFOTO(Request $request)
    {
        $datos = $request->all();
        $evolutivo = Gestioncartera::DetalleClienteFOTO($datos);
        echo json_encode($evolutivo);
    }
    public function getEjecutivos(Request $request)
    {
        $datos = $request->all();
        $ejecutivos = json_decode(Gestioncartera::getEjecutivos($datos['banca'], $datos['zonal'], $datos['jefe']), true);
        echo json_encode($ejecutivos);
    }
    public function coldirectas(Request $request)
    {
        $datos = $request->all();
        $coldirectas = Gestioncartera::coldirectas($datos);
        echo json_encode($coldirectas);
    }
    public function colindirectas(Request $request)
    {
        $datos = $request->all();
        $colindirectas = Gestioncartera::colindirectas($datos);
        echo json_encode($colindirectas);
    }
    public function depositos(Request $request)
    {
        $datos = $request->all();
        $tabla = Gestioncartera::depositos($datos);
        echo json_encode($tabla);
    }
    public function VarXProducto(Request $request)
    {
        $datos = $request->all();
        $VarXProducto = Gestioncartera::VarXProducto($datos);
        echo json_encode($VarXProducto);
    }
    public function getCumplimiento(Request $request)
    {
        $datos = $request->all();
        $Cumplimiento = Gestioncartera::getCumplimiento($datos);
        echo json_encode($Cumplimiento);
    }
    public function jefatura(Request $request)
    {
        print_r($request->all());
    }
    public function getUsuario(Request $request)
    {
        $datos = $request->all();
        $usuario = Gestioncartera::getUsuario2($datos);
        echo json_encode($usuario);
    }
    public function getTopBtm(Request $request)
    {
        $datos = $request->all();
        $list = Gestioncartera::getTopBtm($datos);
        echo json_encode($list);
    }
    public function getTopBottomMERCADO(Request $request)
    {
        $datos = $request->all();
        $list = Gestioncartera::getTopBottomMERCADO($datos);
        echo json_encode($list);
    }
    public function LineaMercadoTabla(Request $request)
    {
        $datos = $request->all();
        $datosMercado = Gestioncartera::MercadoTabla($datos);
        $datosLinea = Gestioncartera::LineaTabla($datos);
        $datosTabla = array("LINEA" => $datosLinea, "MERCADO" => $datosMercado);
        echo json_encode($datosTabla);
    }
    public function GraficosBottom(Request $request)
    {
        $datos = $request->all();
        $Pie = Gestioncartera::MercadoPieGrafico($datos);
        $Line = Gestioncartera::MercadoLineGrafico($datos);
        $datosGrafico = array("PIE" => $Pie, "LINE" => $Line);
        echo json_encode($datosGrafico);
    }
    public function Descuentos(Request $request)
    {
        $datos = $request->all();
        $arr = Gestioncartera::Descuentos($datos);
        echo json_encode($arr);
    }
    public function PAP(Request $request)
    {
        $datos = $request->all();
        $arr = Gestioncartera::PAP($datos);
        echo json_encode($arr);
    }
    public function CSUELDO(Request $request)
    {
        $datos = $request->all();
        $arr = Gestioncartera::CSUELDO($datos);
        echo json_encode($arr);
    }
    public function CUENTASUELDO(Request $request)
    {
        $datos = $request->all();
        $arr = Gestioncartera::CUENTASUELDO($datos);
        echo json_encode($arr);
    }
    public function indexchatbot(Request $request)
    {
        return view('chatbots.index');
    }
}
