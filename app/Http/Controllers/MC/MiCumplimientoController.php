<?php

namespace App\Http\Controllers\MC;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entity\MC\MiCumplimiento as Emicumplimiento;
use Validator;

class MiCumplimientoController extends Controller {

    public function index() {
        $periodos=Emicumplimiento::getPeriodos();
        $bancas=Emicumplimiento::getBancas();
        return view('mc.index')
                ->with('periodos',$periodos)
                ->with('bancas',$bancas);
    }

    public function getZonales(Request $request)
    {
        $bancas = $request->get('bancas',null);
        $zonales = Emicumplimiento::getZonales($bancas);
        return response()->json($zonales);
    }

    public function lista(Request $request){
        $banca = $request->get('bancas',null);
        $zonal = $request->get('zonales',null);
        $periodo = $request->get('periodos',null);

        $lista = Emicumplimiento::lista($banca,$zonal,$periodo);
        return response()->json($lista);
    }

    public function detalleindicadores(Request $request) {
        $periodos=Emicumplimiento::getPeriodos();
        $bancas=Emicumplimiento::getBancas();

        //$llave = $request->get('llave',null);dd($request->get('llave',null));
        return view('mc.detalle')
                ->with('periodos',$periodos)
                ->with('bancas',$bancas);
    }

    public function detalle(Request $request){
    	$banca = $request->get('bancas',null);
    	$zonal = $request->get('zonales',null);
    	$periodo = $request->get('periodos',null);
        $jefaturas = $request->get('jefaturas',null);
    	$ejecutivos = $request->get('ejecutivos',null);

        $datos = Emicumplimiento::getIndicadores($banca,$zonal,$periodo,$ejecutivos,$jefaturas);
        return response()->json($datos);
    }

    public function getEjecutivos(Request $request)
    {
        $bancas = $request->get('bancas',null);
        $cod_sect = $request->get('cod_sect',null);

        $ejecutivos = Emicumplimiento::getEjecutivos($bancas,$cod_sect);

        return response()->json($ejecutivos);
    }

    public function getJefaturas(Request $request)
    {
        $bancas = $request->get('bancas',null);
        $cod_sect = $request->get('cod_sect',null);

        $ejecutivos = Emicumplimiento::getJefaturas($bancas,$cod_sect);

        return response()->json($ejecutivos);
    }
}