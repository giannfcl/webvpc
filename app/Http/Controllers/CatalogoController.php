<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;
use DB;

class CatalogoController extends Controller {

    function getGirosBySector(Request $request){
		return response()->json(\App\Entity\Giro::getAll($request->get('sector',null)));
	}

	function getProvinciaByDepartamento(Request $request){
		return response()->json(\App\Entity\Ubigeo::getComboProvincia($request->get('departamento',null)));
	}

	function getDistritoByProvincia(Request $request){
		return response()->json(\App\Entity\Ubigeo::getComboDistrito($request->get('provincia',null),$request->get('departamento',null)));
	}

	function getTiendasByUbigeo(Request $request){
		return response()->json(\App\Entity\Ubigeo::getTiendasByUbigeo($request->get('distrito',null),$request->get('provincia',null),$request->get('departamento',null)));
	}
}
