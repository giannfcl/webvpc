<?php


namespace App\Http\Controllers\FIES\operaciones\ejecutivo;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Entity\Usuario as Usuario;

use App\Entity\fies\Operaciones as Operaciones;

use App\Entity\fies\OperacionAtributo as OperacionAtributo;
use App\Entity\fies\OperacionEstacion as OperacionEstacion;
use App\Entity\fies\OperacionProspecto as OperacionesProspectos;
use App\Entity\fies\OperacionProductoEstado as OperacionesProductoEstado;

use App\Entity\fies\OperacionEstacion as OperacionEstaciones;
use Jenssegers\Date\Date as Carbon;

use App\Entity\fies\OperacionProductoInformacion as ProductoInformacion;
use App\Entity\fies\OperacionEstado as OperacionEstados;
use App\Entity\fies\OperacionDias as OperacionDias;
use App\Entity\fies\OperacionCronograma as OperacionCronograma;

use Validator;


use App\Entity\fies\Clientes as Clientes;



class OperacionesController extends Controller {

    /**
     * Renderizar vista login regular en ambiente de producción
     *
     */
    public function listar(Request $request) {

        $busqueda = [
       		'page' => $request->get('page',1),
			    'documento' => $request->get('documento',NULL),
			    'grupo_zonal' => $request->get('grupo_zonal',null),
          'estacion' => $request->get('estacion',null),
          'ejeFies' => $request->get('ejeFies',null),
          'idEstacion' => $request->get('idEstacion',null),
          'mesComi' => $request->get('mesComi',null),
          'mesDesem' => $request->get('mesDesem',null),
         

		   ];

        $orden = [
            'sort' => $request->get('sort',null),
            'order' => $request->get('order',null),
        ];

        // Validamos los parametros de ordenamiento
        if (is_null($orden['sort']) || !in_array($orden['sort'], ['ejeFies','estacion','grupo_zonal','nomCliente','mesDesem','mesComi'])){
            $orden['sort'] = null;
            if (is_null($orden['order']) || !in_array($orden['order'], ['asc','desc'])){
                $orden = null;
            }
        }

    

    $meses= ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'];
    $ah = 2016;//Carbon::today()->year;
    $años = [$ah,$ah+1,$ah+2,$ah+3,$ah+4,$ah+5];
    


              if ($this->user->getValue('_rol')=="11"){

                $pagina = Operaciones::getOperacionesEjecutivoFies($this->user->getValue('_registro'),$busqueda,$orden)
                 ->setPath(config('app.url').'fies/ejecutivo/operaciones');
              		
                  return view('fies.operaciones.ejecutivo.operaciones')
                          ->with('operaciones',$pagina)
                          ->with('busqueda',$busqueda)
                          ->with('orden',$orden)
                          ->with('meses',$meses)
                          ->with('años',$años)
                          ->with('grupo_zonales',Operaciones::getGrupoZonalByEjecutivo($this->user->getValue('_registro')))
                          ->with('ejecutivos_fies',Operaciones::getRelacionFiesByEjecutivo($this->user->getValue('_registro')))
                          ->with('resumen',Operaciones::getResumenByEjecutivoFies($this->user->getValue('_registro'),$this->user->getValue('_rol')));
                }

              if ($this->user->getValue('_rol')=="12"){

                 $pagina = Operaciones::getOperacionesFies($this->user->getValue('_registro'),$busqueda,$orden)
                  ->setPath(config('app.url').'fies/ejecutivo/operaciones');
                  return view('fies.operaciones.ejecutivo.operaciones')
                            ->with('operaciones',$pagina)
                          ->with('busqueda',$busqueda)
                          ->with('orden',$orden)
                          ->with('meses',$meses)
                          ->with('años',$años)
                          ->with('rol',$this->user->getValue('_rol'))
                          ->with('grupo_zonales',Operaciones::getGrupoZonalByEjecutivo($request->get('ejeFies',null)))
                          ->with('ejecutivos_fies',Operaciones::getRelacionFiesByEjecutivo($request->get('ejeFies',null)))
                          ->with('resumen',Operaciones::getResumenByEjecutivoFies($this->user->getValue('_registro'),$this->user->getValue('_rol')));
          
                }
    }



 public function mostrarDatosOperacion(Request $request){
  $busqueda = [
    'codOperacion' => $request->get('codOperacion',null),            
  ];

  $meses= ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'];
  $ah = 2016;//Carbon::today()->year;
  $años = [$ah,$ah+1,$ah+2,$ah+3,$ah+4,$ah+5];
    
  
            

            $codOp = $request->get('codOperacion',null);    
            $tipoOperacionD = 'DESEMBOLSO';
            $tipoOperacionC = 'COMISION';
            $OperacionEstados    = new OperacionEstaciones();
            $operacionDias = new OperacionDias();
            //
             $cuotasDesembolso =  OperacionCronograma::setCronogramaListar($busqueda,$tipoOperacionD);
             $cuotasComision =  OperacionCronograma::setCronogramaListar($busqueda,$tipoOperacionC);        
            // 

            $infoProductoD = ProductoInformacion::getOperacionProducto($busqueda,$tipoOperacionD);
            $infoProductoC = ProductoInformacion::getOperacionProducto($busqueda,$tipoOperacionC);
            //
            $pagina =  Operaciones::getOperacionDatosGenerales($busqueda);
            $estacion =  Operaciones::getUltimaEstacion($request->get('codOperacion', null));

            //    
            $OperacionEstados    = new OperacionEstaciones();
            $tabla = $OperacionEstados->getOperacionEstacionByCodOpe($request->get('codOperacion', null));
            $tabla = $OperacionEstados->getOperacionEstacionByCodOpe($request->get('codOperacion', null));

            $dias=$operacionDias->getDiasByOperacion($request->get('codOperacion', null));
            \Debugbar::info($tabla);
            //listado de meses 
            //$meses = OperacionesController::listaMeses();
             
            return view('fies.operaciones.ejecutivo.detalleOperaciones')
                ->with('operacion',$pagina)
                ->with('cuotaDesembolsos',$cuotasDesembolso)
                ->with('cuotasComisiones',$cuotasComision)
                ->with('estacion',$estacion)
                ->with('estAtributo', $tabla)
                ->with('infoProductos',$infoProductoD)
                ->with('infoProductosC',$infoProductoC)                
                ->with('meses',$meses)
                ->with('años',$años)
                ->with('meses',$meses)
                ->with('diasOpe',$dias);


        }

    

    public function listaMeses(){        
        
        $meshoy = Carbon::today()->month;
        $Añohoy = (string)Carbon::today()->year;
        
        $monthNum  = 3;
        $monthName = date('M', mktime(0, 0, 0, $monthNum, 10)); // March

        $listMeses = [];
        
        for($j=($Añohoy-1) ;$j<=($Añohoy+1); $j++)
        {                        
                for ($i = 1; $i <= 12; $i++) 
                {                  
                  $mesValor=(string)$monthName = date('M', mktime(0, 0, 0, $i, 10))."-".(string)$j;
                  $listMeses =  array_add($listMeses,$mesValor,$mesValor);                                    
                }
        }
        
        $listMeses = array_flatten($listMeses);
       return $listMeses;

    }


    public function guardarDetalleGeneral(Request $request){    

    
             $operacionProspecto = new OperacionesProspectos();

            
            //Actualizar
            $validator = Validator::make($request->all(), [
                'ruc' => 'required|max:11',
                'cu' => 'nullable|digits_between:1,10',
                'grupoEconomico' => 'nullable||max:50',
                'sector' => 'nullable|max:100',
                'banca' => 'nullable||max:20',
                'grupoZonal' => 'nullable|max:50',
                'nomEjecutivoNegocio' => 'nullable|max:60',
                'segmento' => 'nullable|max:25',
            ]);

            if ($validator->fails()) {
                flash(array_flatten($validator->errors()->getMessages()))->error();
                return back()->withInput($request->input());
            }

            $operacionProspecto->getOperacionGeneralByCodOpe($request->get('codOperacion', null));

            $operacionProspecto->setValues([
                '_numdoc' => $request->get('ruc', null),
                '_codUnico' => $request->get('cu', null),
                '_grupoEconomico' => $request->get('grupoEconomico', null),
                '_sector' => $request->get('sector', null),
                '_banca' => $request->get('banca', null),
                '_grupoZonal' => $request->get('grupoZonal', null),
                '_segmento' => $request->get('segmento',null),
                '_regEjeNegocio' => $request->get('regEjeNegocio',null),
            ]);

           

           if ( $operacionProspecto->actualizarOperacionDatosGenerales()){
                                  
                    flash("Se guardo el detale")->success();
                     return back()->withInput($request->input());

                    //return response()->json(['msg' => 'ok'], 200);
                }else{

                    //return response()->json(['msg' => $feedback->getMessage()], 404);
                    //flash($OperacionCronograma->getMessage())->error();                        
                   flash("No Ese guardo")->error();
                   return back()->withInput($request->input());
                } 

    }		

    public function guardarProducto(Request $request){   
      
        $OperacionProductoEstado     = new OperacionesProductoEstado ();
    
         $OperacionProductoEstado->getProductoEstadoByCodOpe($request->get('codOperacion', null));

         $OperacionProductoEstado->setValues([
                '_producto' => $request->get('producto', null),
                
            ]);
    
            $OperacionProductoEstado->actualizarProducto();

           return back()->withInput($request->input());
   
    } 



  

        public function ConsultaCliente(Request $request){            
            
            $ruc = $request->get('ruc','null');           
            
            $pagina =  Clientes::getCliente($ruc);
            
            return response()->json($pagina);
        }

    public function guardarPipeline(Request $request)
        {    
                


                $hoy = Carbon::now()->toDateString();


                $operacionAtributo = new OperacionAtributo();

                $idGestion=$operacionAtributo->registrargestion($hoy,$this->user->getValue('_registro'));

                 $operacionAtributo->setValues([
                '_fechaReg' =>(string)$hoy,
                '_idEstacion' =>(string)"1",
                '_codOperacion' => (string)$request->get('codOperacion', null),
                '_atributo' => (string)'FECHA DE INGRESO',
                '_valor' => (string)$request->get('fechaRegistroPipeline', null),
                '_flgUltimo' => (string)"1",
                '_idGestion'=>  $idGestion,
                 ]);
                 
                //actualiza el flag ultimo --SOLO UNA VES POR ESTACION!!
                 $operacionAtributo->actualizaFlagUltimo();
                 // insertya los atributos
                 $operacionAtributo->registrarAtributos($request->get('codOperacion', null));

                 $operacionAtributo->setValues([
                '_atributo' => "ESTATUS",
                '_valor' => $request->get('estatusPipeline', null),
        
                 ]);

                  $operacionAtributo->registrarAtributos($request->get('codOperacion', null));
                 
                 if(!empty($request->get('sePipeline', null))  &&  $request->get('motivoSEPipeline', null)<>"--Seleccione--") {
        
                    $operacionAtributo->setValues([
                        '_atributo' => "SIN EXITO",
                        '_valor' => "1",
                         ]);

                 $operacionAtributo->registrarAtributos($request->get('codOperacion', null));

                 $operacionAtributo->setValues([
                        '_atributo' => "MOTIVO SIN EXITO",
                        '_valor' => $request->get('motivoSEPipeline', null),
                         ]);

                 $operacionAtributo->registrarAtributos($request->get('codOperacion', null));
                 
                  $operacionAtributo->setValues([
                        '_atributo' => "COMENTARIO",
                        '_valor' => $request->get('comentarioPipeline', null),
                         ]);

                 $operacionAtributo->registrarAtributos($request->get('codOperacion', null));

                 $operacionAtributo->setValues([
                        '_idEstacion' =>(string)"8",
                        '_atributo' => (string)'CERRAR',
                        '_valor' => (string)"1",
                        '_flgUltimo' => (string)"0",
                         ]);

                $operacionAtributo->registrarAtributos($request->get('codOperacion', null));
               
                }
                
                $operacionAtributo->actualizaEstacion();
                //return OperacionesController::mostrarDatosOperacion($request);
               
                return back()->withInput($request->input());


        }

    public function guardarCotizacion(Request $request)
        {        
                $hoy = Carbon::now()->toDateString();

                $operacionAtributo = new OperacionAtributo();
                //\Debugbar::info($operacionAtributo->validaFecha($request->get('fechaEnvioCotizacion', null),"1",$request->get('codOperacion', null),"estacion"));

                  if (!$operacionAtributo->validaFecha($request->get('fechaEnvioCotizacion', null),"1",$request->get('codOperacion', null),"estacion") && !empty($request->get('fechaEnvioCotizacion', null))  &&  $request->get('fechaEnvioCotizacion', null)<>"" ){
                    flash($operacionAtributo->getMessage())->error();
                    
                        return back()->withInput($request->input());
                  }

                $idGestion=$operacionAtributo->registrargestion($hoy,$this->user->getValue('_registro')); 

                if(!empty($request->get('fechaEnvioCotizacion', null))  &&  $request->get('fechaEnvioCotizacion', null)<>"") 
                {
                     $operacionAtributo->setValues([
                    '_fechaReg' =>(string)$hoy,
                    '_idEstacion' =>(string)"2",
                    '_codOperacion' => (string)$request->get('codOperacion', null),
                    '_atributo' => (string)'FECHA DE ENVIO',
                    '_valor' => (string)$request->get('fechaEnvioCotizacion', null),
                    '_flgUltimo' => (string)"1",
                    '_idGestion'=>  $idGestion,
                     ]);
                 
                //actualiza el flag ultimo --SOLO UNA VES POR ESTACION!!
                    $operacionAtributo->actualizaFlagUltimo();
                 // insertya los atributos
                    $operacionAtributo->registrarAtributos($request->get('codOperacion', null));

                    if($request->get('estatusCotizacion', null)<>"--Seleccione--") {
                    
                    $operacionAtributo->setValues([
                    '_atributo' => "ESTATUS",
                    '_valor' =>  $request->get('estatusCotizacion', null),
                    ]);

                     $operacionAtributo->registrarAtributos($request->get('codOperacion', null));

                    }

                    if($request->get('probabilidadCotizacion', null)<>"--Seleccione--") {
                    
                    $operacionAtributo->setValues([
                    '_atributo' => "PROBABILIDAD",
                    '_valor' =>  $request->get('probabilidadCotizacion', null),
                    ]);

                     $operacionAtributo->registrarAtributos($request->get('codOperacion', null));

                    }

                    if(!empty($request->get('seCotizacion', null))  &&  $request->get('motivoSECotizacion', null)<>"--Seleccione--") {
        
                    $operacionAtributo->setValues([
                        '_atributo' => "SIN EXITO",
                        '_valor' => "1",
                         ]);

                     $operacionAtributo->registrarAtributos($request->get('codOperacion', null));

                    $operacionAtributo->setValues([
                        '_atributo' => "MOTIVO SIN EXITO",
                        '_valor' => $request->get('motivoSECotizacion', null),
                         ]);

                     $operacionAtributo->registrarAtributos($request->get('codOperacion', null));
                 
                     $operacionAtributo->setValues([
                        '_atributo' => "COMENTARIO",
                        '_valor' => $request->get('comentarioCotizacion', null),
                         ]);

                    $operacionAtributo->registrarAtributos($request->get('codOperacion', null));

                    $operacionAtributo->setValues([
                        '_idEstacion' =>(string)"8",
                        '_atributo' => (string)'CERRAR',
                        '_valor' => (string)"1",
                        '_flgUltimo' => (string)"0",
                         ]);

                    $operacionAtributo->registrarAtributos($request->get('codOperacion', null));
               
                    }

                   



                }
                  $operacionAtributo->actualizaEstacion();
                    return back()->withInput($request->input());
               
    }
                
    
    public function guardarFies(Request $request)
        {        
                $hoy = Carbon::now()->toDateString();

                $operacionAtributo = new OperacionAtributo();

            if (!$operacionAtributo->validaFecha($request->get('fechaIngresoCotizacion', null),"2",$request->get('codOperacion', null),$request->get('estatusFies', null), $request->get('fechaEstatusFies', null)) && !empty($request->get('fechaIngresoCotizacion', null))  &&  $request->get('fechaIngresoCotizacion', null)<>""){
              flash($operacionAtributo->getMessage())->error();
              
                  return back()->withInput($request->input());
            }

                $idGestion=$operacionAtributo->registrargestion($hoy,$this->user->getValue('_registro')); 

            if(!empty($request->get('fechaIngresoCotizacion', null))  &&  $request->get('fechaIngresoCotizacion', null)<>"") 
                   {  $operacionAtributo->setValues([
                    '_fechaReg' =>(string)$hoy,
                    '_idEstacion' =>(string)"3",
                    '_codOperacion' => (string)$request->get('codOperacion', null),
                    '_atributo' => (string)'FECHA DE COTIZACION',
                    '_valor' => (string)$request->get('fechaIngresoCotizacion', null),
                    '_flgUltimo' => (string)"1",
                    '_idGestion'=>  $idGestion,
                     ]);
                 
                //actualiza el flag ultimo --SOLO UNA VES POR ESTACION!!
                    $operacionAtributo->actualizaFlagUltimo();
                 // insertya los atributos
                    $operacionAtributo->registrarAtributos($request->get('codOperacion', null));

                    if($request->get('estatusFies', null)<>"--Seleccione--" && !empty($request->get('fechaEstatusFies', null)) ) {
                    
                    $operacionAtributo->setValues([
                    '_atributo' => "ESTATUS",
                    '_valor' =>  $request->get('estatusFies', null),
                    ]);

                     $operacionAtributo->registrarAtributos($request->get('codOperacion', null));

                     $operacionAtributo->setValues([
                    '_atributo' => "FECHA ESTATUS",
                    '_valor' =>  $request->get('fechaEstatusFies', null),
                    ]);

                     $operacionAtributo->registrarAtributos($request->get('codOperacion', null));

                      $operacionDias = new OperacionDias();
                      $dias=$operacionDias->registrarOperacionesDias($request->get('codOperacion', null));
                      \Debugbar::info($dias);
                    }

                    if( $request->get('comiteFies', null)<>"--Seleccione--") {
                    $operacionAtributo->setValues([
                    '_atributo' => "COMITE",
                    '_valor' =>  $request->get('comiteFies', null),
                    ]);

                     $operacionAtributo->registrarAtributos($request->get('codOperacion', null));
               
                    }

                    if( $request->get('tipoFies', null)<>"--Seleccione--") {
                    $operacionAtributo->setValues([
                    '_atributo' => "TIPO",
                    '_valor' =>  $request->get('tipoFies', null),
                    ]);

                     $operacionAtributo->registrarAtributos($request->get('codOperacion', null));
               
                    }

                    if( $request->get('probabilidadFies', null)<>"--Seleccione--") {
                    $operacionAtributo->setValues([
                    '_atributo' => "PROBABILIDAD",
                    '_valor' =>  $request->get('probabilidadFies', null),
                    ]);

                     $operacionAtributo->registrarAtributos($request->get('codOperacion', null));
                    }

                     if(!empty($request->get('aceptaCotizacion', null))  ) {
                        $operacionAtributo->setValues([
                        '_atributo' => "ACEPTA COTIZACION",
                        '_valor' => "1",
                         ]);

                       $operacionAtributo->registrarAtributos($request->get('codOperacion', null));


                     }

                     if(!empty($request->get('seFies', null))  &&  $request->get('motivoSEFies', null)<>"--Seleccione--") {
        
                    $operacionAtributo->setValues([
                        '_atributo' => "SIN EXITO",
                        '_valor' => "1",
                         ]);

                     $operacionAtributo->registrarAtributos($request->get('codOperacion', null));

                    $operacionAtributo->setValues([
                        '_atributo' => "MOTIVO SIN EXITO",
                        '_valor' => $request->get('motivoSEFies', null),
                         ]);

                     $operacionAtributo->registrarAtributos($request->get('codOperacion', null));
                 
                     $operacionAtributo->setValues([
                        '_atributo' => "COMENTARIO",
                        '_valor' => $request->get('comentarioFies', null),
                         ]);

                    $operacionAtributo->registrarAtributos($request->get('codOperacion', null));

                    $operacionAtributo->setValues([
                        '_idEstacion' =>(string)"8",
                        '_atributo' => (string)'CERRAR',
                        '_valor' => (string)"1",
                        '_flgUltimo' => (string)"0",
                         ]);

                    $operacionAtributo->registrarAtributos($request->get('codOperacion', null));

               
                    }



                }
                  $operacionAtributo->actualizaEstacion();
                   return back()->withInput($request->input());
               
    }         

     public function guardarRiesgos(Request $request)
        {        
                $hoy = Carbon::now()->toDateString();

                $operacionAtributo = new OperacionAtributo();

                if (!$operacionAtributo->validaFecha($request->get('fechaRecepcionRiesgos', null),"3",$request->get('codOperacion', null),$request->get('estatusRiesgos', null), $request->get('fechaEstatusRiesgos', null))  && !empty($request->get('fechaRecepcionRiesgos', null)) &&  $request->get('fechaRecepcionRiesgos', null)<>"" ){
              flash($operacionAtributo->getMessage())->error();
              
                  return back()->withInput($request->input());
            }
                
                $idGestion=$operacionAtributo->registrargestion($hoy,$this->user->getValue('_registro')); 

                if(!empty($request->get('fechaRecepcionRiesgos', null))  &&  $request->get('fechaRecepcionRiesgos', null)<>"") 
                {   
                     $operacionAtributo->setValues([
                    '_fechaReg' =>(string)$hoy,
                    '_idEstacion' =>(string)"4",
                    '_codOperacion' => (string)$request->get('codOperacion', null),
                    '_atributo' => (string)'FECHA RECEPCION',
                    '_valor' => (string)$request->get('fechaRecepcionRiesgos', null),
                    '_flgUltimo' => (string)"1",
                    '_idGestion'=>  $idGestion,
                     ]);
                 
                //actualiza el flag ultimo --SOLO UNA VES POR ESTACION!!
                    $operacionAtributo->actualizaFlagUltimo();
                 // insertya los atributos
                    $operacionAtributo->registrarAtributos($request->get('codOperacion', null));

                    if($request->get('estatusRiesgos', null)<>"--Seleccione--" && !empty($request->get('estatusRiesgos', null)) && !empty($request->get('fechaEstatusRiesgos', null)) ) {
                    
                    $operacionAtributo->setValues([
                    '_atributo' => "ESTATUS",
                    '_valor' =>  $request->get('estatusRiesgos', null),
                    ]);

                     $operacionAtributo->registrarAtributos($request->get('codOperacion', null));

                     $operacionAtributo->setValues([
                    '_atributo' => "FECHA ESTATUS",
                    '_valor' =>  $request->get('fechaEstatusRiesgos', null),
                    ]);

                     $operacionAtributo->registrarAtributos($request->get('codOperacion', null));

                     $operacionDias = new OperacionDias();
                     $dias=$operacionDias->registrarOperacionesDias($request->get('codOperacion', null));

                    }

                    if( $request->get('wfRiesgos', null)<>"") {
                    $operacionAtributo->setValues([
                    '_atributo' => "WF",
                    '_valor' =>  $request->get('wfRiesgos', null),
                    ]);

                     $operacionAtributo->registrarAtributos($request->get('codOperacion', null));
               
                    }

                    if( $request->get('probabilidadRiesgos', null)<>"--Seleccione--") {
                    $operacionAtributo->setValues([
                    '_atributo' => "PROBABILIDAD",
                    '_valor' =>  $request->get('probabilidadRiesgos', null),
                    ]);

                     $operacionAtributo->registrarAtributos($request->get('codOperacion', null));
                    }

                     if(!empty($request->get('seRiesgos', null))  &&  $request->get('motivoSERiesgos', null)<>"--Seleccione--") {
        
                        $operacionAtributo->setValues([
                            '_atributo' => "SIN EXITO",
                            '_valor' => "1",
                             ]);

                         $operacionAtributo->registrarAtributos($request->get('codOperacion', null));

                        $operacionAtributo->setValues([
                            '_atributo' => "MOTIVO SIN EXITO",
                            '_valor' => $request->get('motivoSERiesgos', null),
                             ]);

                         $operacionAtributo->registrarAtributos($request->get('codOperacion', null));
                     
                         $operacionAtributo->setValues([
                            '_atributo' => "COMENTARIO",
                            '_valor' => $request->get('comentarioRiesgos', null),
                             ]);

                        $operacionAtributo->registrarAtributos($request->get('codOperacion', null));

                         $operacionAtributo->setValues([
                        '_idEstacion' =>(string)"8",
                        '_atributo' => (string)'CERRAR',
                        '_valor' => (string)"1",
                        '_flgUltimo' => (string)"0",
                         ]);

                        $operacionAtributo->registrarAtributos($request->get('codOperacion', null));


               
                    }



                }
      
                  $operacionAtributo->actualizaEstacion();
                    return back()->withInput($request->input());
               
    }
    
    public function guardarAprobado(Request $request)
        {        
                $hoy = Carbon::now()->toDateString();

                $operacionAtributo = new OperacionAtributo();

                if (!$operacionAtributo->validaFecha($request->get('fechaAprobacion', null),"4",$request->get('codOperacion', null)) && !empty($request->get('fechaAprobacion', null))  &&  $request->get('fechaAprobacion', null)<>"" ){
                    flash($operacionAtributo->getMessage())->error();
                    
                        return back()->withInput($request->input());
                  }
        
                $idGestion=$operacionAtributo->registrargestion($hoy,$this->user->getValue('_registro')); 

                if(!empty($request->get('fechaAprobacion', null))  &&  $request->get('fechaAprobacion', null)<>"") 
                {   
                     $operacionAtributo->setValues([
                    '_fechaReg' =>(string)$hoy,
                    '_idEstacion' =>(string)"5",
                    '_codOperacion' => (string)$request->get('codOperacion', null),
                    '_atributo' => (string)'FECHA APROBACION',
                    '_valor' => (string)$request->get('fechaAprobacion', null),
                    '_flgUltimo' => (string)"1",
                    '_idGestion'=>  $idGestion,
                     ]);
                 
                //actualiza el flag ultimo --SOLO UNA VES POR ESTACION!!
                    $operacionAtributo->actualizaFlagUltimo();
                 // insertya los atributos
                    $operacionAtributo->registrarAtributos($request->get('codOperacion', null));

                     $operacionDias = new OperacionDias();
                      $dias=$operacionDias->registrarOperacionesDias($request->get('codOperacion', null));
                     

                    if($request->get('tipoComisionAprobado', null)<>"--Seleccione--" && !empty($request->get('tipoComisionAprobado', null)) ) {
                    
                    $operacionAtributo->setValues([
                    '_atributo' => "TIPO COMISION",
                    '_valor' =>  $request->get('tipoComisionAprobado', null),
                    ]);

                     $operacionAtributo->registrarAtributos($request->get('codOperacion', null));

                    
                    }

                    if( $request->get('garantiaAprobado', null)<>"") {
                    $operacionAtributo->setValues([
                    '_atributo' => "GARANTIA",
                    '_valor' =>  $request->get('garantiaAprobado', null),
                    ]);

                     $operacionAtributo->registrarAtributos($request->get('codOperacion', null));
               
                    }

                    if( $request->get('tipoGarantiaAprobado', null)<>"--Seleccione--") {
                    $operacionAtributo->setValues([
                    '_atributo' => "TIPO_GARANTIA",
                    '_valor' =>  $request->get('tipoGarantiaAprobado', null),
                    ]);

                     $operacionAtributo->registrarAtributos($request->get('codOperacion', null));
                    }

                    if( $request->get('flagCovenantAprobado', null)=="Si" && ( !empty($request->get('covenant1', null)) || !empty($request->get('covenant2', null)) || !empty($request->get('covenant3', null)) || !empty($request->get('covenant4', null))  ) ){
                    $operacionAtributo->setValues([
                    '_atributo' => "ESTADO COVENANT",
                    '_valor' =>  $request->get('flagCovenantAprobado', null),
                    ]);

                     $operacionAtributo->registrarAtributos($request->get('codOperacion', null));

                     if(!empty($request->get('covenant1', null))){
                         $operacionAtributo->setValues([
                        '_atributo' => "COVENANT1",
                        '_valor' =>  $request->get('covenant1', null),
                        ]);
                         $operacionAtributo->registrarAtributos($request->get('codOperacion', null));
                         }
                      if(!empty($request->get('covenant2', null))){
                          $operacionAtributo->setValues([
                        '_atributo' => "COVENANT2",
                        '_valor' =>  $request->get('covenant2', null),
                        ]);
                         $operacionAtributo->registrarAtributos($request->get('codOperacion', null));
                        }
                      if(!empty($request->get('covenant3', null))){
                          $operacionAtributo->setValues([
                        '_atributo' => "COVENANT3",
                        '_valor' =>  $request->get('covenant3', null),
                        ]);
                         $operacionAtributo->registrarAtributos($request->get('codOperacion', null));
                        }
                      if(!empty($request->get('covenant1', null))){
                          $operacionAtributo->setValues([
                        '_atributo' => "COVENANT4",
                        '_valor' =>  $request->get('covenant4', null),
                        ]);
                         $operacionAtributo->registrarAtributos($request->get('codOperacion', null));
                       }



                    }

                  

                     if(!empty($request->get('seAprobado', null))  &&  $request->get('motivoSEAprobado', null)<>"--Seleccione--") {
        
                    $operacionAtributo->setValues([
                        '_atributo' => "SIN EXITO",
                        '_valor' => "1",
                         ]);

                     $operacionAtributo->registrarAtributos($request->get('codOperacion', null));

                    $operacionAtributo->setValues([
                        '_atributo' => "MOTIVO SIN EXITO",
                        '_valor' => $request->get('motivoSEAprobado', null),
                         ]);

                     $operacionAtributo->registrarAtributos($request->get('codOperacion', null));
                 
                     $operacionAtributo->setValues([
                        '_atributo' => "COMENTARIO",
                        '_valor' => $request->get('comentarioAprobado', null),
                         ]);

                    $operacionAtributo->registrarAtributos($request->get('codOperacion', null));

                    $operacionAtributo->setValues([
                        '_idEstacion' =>(string)"8",
                        '_atributo' => (string)'CERRAR',
                        '_valor' => (string)"1",
                        '_flgUltimo' => (string)"0",
                         ]);

                    $operacionAtributo->registrarAtributos($request->get('codOperacion', null));
               
                    }



                }
                  $operacionAtributo->actualizaEstacion();
                    return back()->withInput($request->input());
               
    }

    public function cerrarEstacion(Request $request)
        {   
            $operacionAtributo = new OperacionAtributo();
            $hoy = Carbon::now()->toDateString();
            if((string)$request->get('idEstacion', null)<>"6"){
            $operacionAtributo->setValues([
                    '_fechaReg' =>(string)$hoy,
                    '_idEstacion' =>(string)$request->get('idEstacion', null),
                    '_codOperacion' => (string)$request->get('codOperacion', null),
                    '_atributo' => (string)'CERRAR',
                    '_valor' => (string)"1",
                    '_flgUltimo' => (string)"1",
                     ]);

                 // insertya los atributos
            $operacionAtributo->registrarAtributos($request->get('codOperacion', null));
            }

            if((string)$request->get('idEstacion', null)=="5"){
           
                 $operacionAtributo->setValues([
                    '_fechaReg' =>(string)$hoy,
                    '_idEstacion' =>(string)"6",
                    '_codOperacion' => (string)$request->get('codOperacion', null),
                    '_atributo' => (string)'ACTIVADO',
                    '_valor' => (string)"1",
                    '_flgUltimo' => (string)"0",
                     ]);

                 // insertya los atributos
                 $operacionAtributo->registrarAtributos($request->get('codOperacion', null));
                  $operacionAtributo->actualizaEstacion();
            }

            if((string)$request->get('idEstacion', null)=="6"){

                 $operacionAtributo->setValues([
                    '_fechaReg' =>(string)$hoy,
                    '_idEstacion' =>(string)"6",
                    '_codOperacion' => (string)$request->get('codOperacion', null),
                    '_atributo' => (string)'CERRAR',
                    '_valor' => (string)"1",
                    '_flgUltimo' => (string)"0",
                     ]);

                 // insertya los atributos
                 $operacionAtributo->registrarAtributos($request->get('codOperacion', null));
                  $operacionAtributo->actualizaEstacion();
           
                 $operacionAtributo->setValues([
                    '_fechaReg' =>(string)$hoy,
                    '_idEstacion' =>(string)"7",
                    '_codOperacion' => (string)$request->get('codOperacion', null),
                    '_atributo' => (string)'ACTIVADO',
                    '_valor' => (string)"1",
                    '_flgUltimo' => (string)"0",
                     ]);

                 // insertya los atributos
                 $operacionAtributo->registrarAtributos($request->get('codOperacion', null));
                  $operacionAtributo->actualizaEstacion();
            }
          
            
             return back()->withInput($request->input());

        }


        public function mostrarCronograma(Request $request){
            
            $codOperacion = $request->get('codOperacion','null');    
            $tipoOperacion = $request->get('tipoOperacion','null');
            
            $pagina =  OperacionCronograma::setCronogramaListar($codOperacion,$tipoOperacion);
            
            return $pagina;          

        }


        public function agregarCuota(Request $request){


            $validator = Validator::make($request->all(), [
            'codOperacion' => 'required',
            'monto' => 'required',
            'fecha' => 'required',
            'tipo' => 'required',
            ]);


        if ($validator->fails()) {
            return response()->json(['msg' => 'Input incorrecto'], 404);
        }

       
        $mes=$request->get('fecha', null);
        $mes=(string)date("M", strtotime($request->get('fecha', null)));
        $year=(string)date("Y", strtotime($request->get('fecha', null)));
        $concat=$mes.'-'.$year;
         
       
        $OperacionCronograma = new OperacionCronograma();    
        $OperacionCronograma->setValues([
            '_codOperacion' => $request->get('codOperacion', null),
            '_monto' => str_replace (",","",$request->get('monto', null)),
            '_fecha' => $request->get('fecha', null),
            '_mes' => $concat ,
            '_estatus' =>$request->get('estado', null),                
            '_tipo' =>$request->get('tipo', null),                
        ]);
             

        \Debugbar::info($OperacionCronograma);
        if ($OperacionCronograma->registrarNuevaCuota()){
            
            flash("Guardo cuota exitosamente")->success();
            return redirect()->route('fies.operaciones.ejecutivo.operaciones.detalle',['codOperacion'=>$OperacionCronograma->getValue('_codOperacion')]);        

        }else{
          flash("No guardo cuota")->success();
           return redirect()->route('fies.operaciones.ejecutivo.operaciones.detalle',['codOperacion'=>$OperacionCronograma->getValue('_codOperacion')]);
        }
        }


        public function eliminarCuota(Request $request){
             
             
             $validator = Validator::make($request->all(), [
            'codOperacion' => 'required',            
            'fecha' => 'required',
            'tipo' => 'required',
            ]);
            
            
             if ($validator->fails()) {                
                    return response()->json(['msg' => 'Input incorrecto'], 404);
               } 

            
                     $OperacionCronograma = new OperacionCronograma();    
                     $OperacionCronograma->setValues([
                    '_codOperacion' => $request->get('codOperacion', null),            
                    '_fecha' => $request->get('fecha', null),                        
                    '_tipo' => $request->get('tipo', null),                        
                         ]);
                



            if ($OperacionCronograma->setFlag()){
                                  
                    flash("Elimino cuota exitosamente")->success();
                    return redirect()->route('fies.operaciones.ejecutivo.operaciones.detalle',['codOperacion'=>$OperacionCronograma->getValue('_codOperacion')]);        

                    //return response()->json(['msg' => 'ok'], 200);
                }else{

                    //return response()->json(['msg' => $feedback->getMessage()], 404);
                    //flash($OperacionCronograma->getMessage())->error();                        
                   flash("No Elimino cuota")->success();
                   return redirect()->route('fies.operaciones.ejecutivo.operaciones.detalle',['codOperacion'=>$OperacionCronograma->getValue('_codOperacion')]);
                } 

        }

        public function actualizarCuota(Request $request){




            $validator = Validator::make($request->all(), [
            'codOperacion' => 'required',
            'monto' => 'required',
            'fecha' => 'required',
            'tipo' => 'required',
            ]);

        if ($validator->fails()) {
            return response()->json(['msg' => 'Input incorrecto'], 404);
        }

       
        $mes=$request->get('fecha', null);
        $mes=(string)date("M", strtotime($request->get('fecha', null)));
        $year=(string)date("Y", strtotime($request->get('fecha', null)));
        $concat=$mes.'-'.$year;
         
       
        $OperacionCronograma = new OperacionCronograma();    
        $OperacionCronograma->setValues([
            '_codOperacion' => $request->get('codOperacion', null),
            '_monto' => str_replace (",","",$request->get('monto', null)),
            '_fecha' => $request->get('fecha', null),
            '_mes' =>$concat ,
            '_estatus' => $request->get('estado', null),                
            '_tipo' => $request->get('tipo', null),
            '_fechaRegistro' => $request->get('fechaRegistro', null),
        ]);
             
          \Debugbar::info($OperacionCronograma);
        if ($OperacionCronograma->updateCronograma()){
              
             flash("Guardo cuota exitosamente")->success();
            return redirect()->route('fies.operaciones.ejecutivo.operaciones.detalle',['codOperacion'=>$OperacionCronograma->getValue('_codOperacion')]);        

            //return response()->json(['msg' => 'ok'], 200);
        }else{
            flash("No Guardo cuota")->success();
           return redirect()->route('fies.operaciones.ejecutivo.operaciones.detalle',['codOperacion'=>$OperacionCronograma->getValue('_codOperacion')]);
        }
        }


       

        public function actualizarOperacionProducto(Request $request){

            $validator = Validator::make($request->all(), [
            'codOperacion' => 'required',
            'montoTotal' => 'required',
            'mesProbable' => 'required',
            'añoProbable' => 'required',
            'tipo' => 'required',
            'moneda' => 'required',
            ]);

       
        if ($validator->fails()) {
            return response()->json(['msg' => 'Input incorrecto'], 404);
        }
        

        
        $mesProbable=(string)$request->get('mesProbable', null).'-'.(string)$request->get('añoProbable', null); 
        
        

        $infoProductOp = new ProductoInformacion();    
        $infoProductOp->setValues([
            '_codOperacion' => $request->get('codOperacion', null),
            '_monto' => str_replace (",","",$request->get('montoTotal', null)),
            '_mesProbable' =>$mesProbable ,      
            '_tipo' => $request->get('tipo', null),
            '_moneda' => $request->get('moneda', null),
        ]);
            
        if ($infoProductOp->actualizarOperacionProducto()){
              
            
            flash("Guardo información exitosamente")->success();
            return redirect()->route('fies.operaciones.ejecutivo.operaciones.detalle',['codOperacion'=>$infoProductOp->getValue('_codOperacion')]);        

            //return response()->json(['msg' => 'ok'], 200);
        }else{
           
           flash("No guardo información")->success();
           return redirect()->route('fies.operaciones.ejecutivo.operaciones.detalle',['codOperacion'=>$infoProductOp->getValue('_codOperacion')]);
        }
        }

        public function setOperacion(Request $request){
           
                  //crear entidad para prospectos
           

                 if(!empty($request->get('ruc', null))  &&  $request->get('ruc', null)<>"" && $request->get('producto', null)<>"" && !empty($request->get('fechaRegistro', null))  &&  $request->get('fechaRegistro', null)<>"" && !empty($request->get('ejecutivoFies', null)) && $request->get('ejecutivoFies', null)<>"" && $request->get('ejecutivoFies', null)<>"--Seleccione--") 
                  {
                         $dataProspecto = new OperacionesProspectos();
                         
                         $dataProspecto->setValues([                    
                            '_codUnico' => $request->get('codUnico', null),
                            '_numdoc' => $request->get('ruc', null),
                            '_nomCli' => $request->get('razonSocial', null),
                            '_grupoEconomico' => $request->get('grupoEconomico', null),
                            '_sector' => $request->get('sectorEconomico', null),
                            '_banca' => $request->get('banca', null),
                            '_grupoZonal' => $request->get('Zonal', null),
                            '_segmento' => $request->get('segmento', null),
                            '_regEjeNegocio' => $request->get('regEjeNegocio', null),
                            '_fechaReg' => $request->get('fechaRegistro', null),

                         ]);


                           $error="";
                           $codOperacion = $dataProspecto->setProspectos();
                         
                       
                          if ($codOperacion<>null){
                              
                             $succes=  0;
                          }else{
                              $error="No cargo prospecto".$error;
                          } 
              
                 
                                
                             //crear entidad para operaciones

                            
                            $dataOperacion = new Operaciones();
                            
                            $dataOperacion->setValues([
                                '_codOperacion' => $codOperacion,
                                '_producto' => $request->get('producto', null),
                                '_regEjecutivoFies' => $request->get('ejecutivoFies', null),                                    
                                '_UltimoEstacion' => 1,
                             ]);
                             
                          
                            if ($dataOperacion->setNuevaOperacion()){
                                $succes=  0;
                            }else
                            {
                                $error="No cargo Operacion - ".$error;
                            }  

                 
                
                        //INFORMACION DE OPERACION
                              /*$mes=$request->get('mesProbable', null);
                              $mes=(string)date("M", strtotime($request->get('mesProbable', null)));
                              $year=(string)date("Y", strtotime($request->get('mesProbable', null)));
                              $concat=$mes.'-'.$year;*/

                              //mes probable
                                $mes=(string)$request->get('mesProbable', null);

                                $mesesCat= array();
                                $mesesCat = array_add($mesesCat,'Ene','1');
                                $mesesCat = array_add($mesesCat,'Feb','2');
                                $mesesCat = array_add($mesesCat,'Mar','3');
                                $mesesCat = array_add($mesesCat,'Abr','4');
                                $mesesCat = array_add($mesesCat,'Nay','5');
                                $mesesCat = array_add($mesesCat,'Jun','6');
                                $mesesCat = array_add($mesesCat,'Jul','7');
                                $mesesCat = array_add($mesesCat,'Ago','8');
                                $mesesCat = array_add($mesesCat,'Sep','9');
                                $mesesCat = array_add($mesesCat,'Oct','10');
                                $mesesCat = array_add($mesesCat,'Nov','11');
                                $mesesCat = array_add($mesesCat,'Dic','12');
                                                        
                                $numMes = (int)array_get($mesesCat,$mes);

                                \Debugbar::info($mes);

                                $año=(int)$request->get('añoProbable', null);

                                $dateProbable = Carbon::create($año,$numMes, 1, 0);

                                $mesProbable=(string)$request->get('mesProbable', null).'-'.(string)$request->get('añoProbable', null);
                             

                                  $infoProductOpDesembolso = new ProductoInformacion();
                                  $infoProductOpDesembolso->setValues([
                                      '_codOperacion' => $codOperacion,
                                      '_monto' => str_replace (",","",$request->get('montoDesembolso', null)),
                                      '_mesProbable' =>$mesProbable,    
                                      '_tipo' => "DESEMBOLSO",
                                      '_moneda' => $request->get('moneda', null),
                                  ]); 

                                 if ($infoProductOpDesembolso->registrarNuevoInfoProducto()){
                                   $succes=  0;
                                  }else{
                                      $error="No cargo informacion del producto Desembolso- ".$error;
                                  } 

                                $infoProductOpComision = new ProductoInformacion();
                              $infoProductOpComision->setValues([
                                  '_codOperacion' => $codOperacion,
                                  '_monto' => str_replace (",","",$request->get('montoComision', null)),
                                  '_mesProbable' =>$mesProbable,    
                                  '_tipo' => "COMISION",
                                  '_moneda' => $request->get('moneda', null),
                              ]); 

                                 if ($infoProductOpComision->registrarNuevoInfoProducto()){
                                  $succes=  0;
                                  }else{
                                      $error="No cargo informacion del producto comision - ".$error;
                                  } 


                 //crear entidad para cronogramas(desembolsos-comisiones)
                    
                             

                    $OperacionCronogramaDesembolso = new OperacionCronograma();    
                    $OperacionCronogramaDesembolso->setValues([
                                '_codOperacion' => $codOperacion,
                                '_monto' => str_replace (",","",$request->get('montoDesembolso', null)),
                                '_fecha' => $dateProbable,
                                '_mes' => $mesProbable , 
                                '_tipo' => "DESEMBOLSO" ,
                                '_estatus' => "PENDIENTE" ,                     
                            ]);
                        if ($OperacionCronogramaDesembolso->registrarNuevaCuota()){
                           $succes=  0;
                        }else{
                            $error="No cargo OperacionCronogramaDesembolso - ".$error;
                        }  

                    $OperacionCronogramaComision = new OperacionCronograma();    
                    $OperacionCronogramaComision->setValues([
                                '_codOperacion' => $codOperacion,
                                '_monto' => str_replace (",","",$request->get('montoComision', null)),
                                '_fecha' => $dateProbable,
                                '_mes' => $mesProbable , 
                                '_tipo' => "COMISION",
                                '_estatus' => "PENDIENTE" ,                     
                            ]);


                        if ($OperacionCronogramaComision->registrarNuevaCuota()){
                           $succes=  0;
                        }else{
                            $error="No cargo OperacionCronogramaComision - ".$error;
                        }  
                    // informacion para estacion
                      $hoy = Carbon::now()->toDateString();

                      $operacionAtributo = new OperacionAtributo();

                       $operacionAtributo->setValues([
                      '_fechaReg' =>(string)$hoy,
                      '_idEstacion' =>(string)"1",
                      '_codOperacion' => $codOperacion, 
                      '_atributo' => (string)'FECHA DE INGRESO',
                      '_valor' => $request->get('fechaRegistro', null),
                      '_flgUltimo' => (string)"1",
                       ]);
                       
                      //actualiza el flag ultimo --SOLO UNA VES POR ESTACION!!
                       $operacionAtributo->actualizaFlagUltimo();
                       // inserta los atributos
                       $operacionAtributo->registrarAtributos($codOperacion);


                       //ACTUALIZAR PAGINA

                        $busqueda = [
                            'page' => $request->get('page',1),
                            'documento' => $request->get('documento',NULL),
                            'grupo_zonal' => $request->get('grupo_zonal',null),
                            'estacion' => $request->get('estacion',null),
                            'ejeFies' => $request->get('ejecutivoFies',null),
                      ];

                    }
                    else{
                      $succes=1;
                       $busqueda = [
                            'page' => $request->get('page',1),
                            'documento' => $request->get('documento',NULL),
                            'grupo_zonal' => $request->get('grupo_zonal',null),
                            'estacion' => $request->get('estacion',null),
                            'ejeFies' => $request->get('ejecutivoFies',null),
                      ];
                    }

                    $mensaje ='';
                    if( $succes== 0){
                        
                        flash("Guardo operacion exitosamente")->success();
                       return back()->withInput($request->input());     
                    }else{
                       
                        flash("No se Guardo la operacion")->error();
                      return back()->withInput($request->input());     
                    }

            return $error;

        }

}    