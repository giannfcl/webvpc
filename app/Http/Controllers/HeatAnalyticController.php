<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entity\HeatAnalytic as HeatAnalytic;


class HeatAnalyticController extends Controller
{

    public function heatAnalytic(Request $request)
    {
        $datos = $request->all();
        $registro   =  $this->user->getValue('_registro');
        HeatAnalytic::insertHA($datos, $registro);
    }
}
