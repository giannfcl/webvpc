<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Entity\Landing;
use Illuminate\Http\Request;
use App\Entity\Usuario;
use Yajra\Datatables\Datatables as Datatables;
use PDF;


use Validator;

class LandingController extends Controller
{
	public function index(Request $request)
	{
		return view("covid19.index");
	}

	public function getClientesLanding(Request $request)
	{
		return Datatables::of(Landing::getClientesLanding($request->all()))->make(true);
	}

	public function gestioncliente(Request $request)
	{
		if (Landing::gestionarLanding($request->all())) {
			flash("Exito con la gestión")->success();
		}else{
			flash("Hubo un problema")->error();
		}
		return redirect()->route('covid19.lista');
	}
}