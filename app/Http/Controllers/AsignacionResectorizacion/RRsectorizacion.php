<?php 
namespace App\Http\Controllers\AsignacionResectorizacion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Entity\Usuario;
use App\Entity\AsignacionResectorizacion\Asignacion as Asignacion;

use App\Entity\BE\Lead as Lead;
use Validator;

class RRsectorizacion extends Controller{

    //Constantes de casos de Asignacion
    const CASO_ASIG_1 = 1;
    const CASO_ASIG_2 = 2;
    const CASO_ASIG_3 = 3;
    const CASO_ASIG_4 = 4;

    const CASO_ASIG_5 = 5;
    const CASO_ASIG_6 = 6;
    const CASO_ASIG_0 = 0;
    const agregar_referido = 1;
    const click_buscar = 2;
    /*
        Funciones usadas
        1. Lead->referidosMesActual
        2. Lead->buscarDatosReferido
        3. Lead->ClientesVPC
        4. Lead->getCodSectorista
        5. Lead->getOneCodSectorista
        6. Asignacion->guardarCorreoReferido
        7. Asignacion->Nuevoreferido
        8. Lead->getLeadByNum
        9. Lead->getSectoristaArbol
        10.Lead->reasignar
        11.Asignacion->registrar
    */
    
    public function index(Request $request)
    {
        // dd($request->all());
        $entidad=new Lead();
        $arbol=new Asignacion();
        $referidosMes=$entidad->referidosMesActual($this->user->getValue('_registro'));
        $cant=!empty($referidosMes) ? $referidosMes->NUM_REFERIDOS : 0;
        $mensaje=0;
        $buscando=0;

        if ($request->get('documento',null)!= null) {
            if (strlen($request->get('documento',null))<8) {
                flash('Debe ser un documento válido')->error();
                return view('AsignacionResectorizacion.index')
                ->with('lead', null)
                ->with('data',null)
                ->with('caso',null)
                ->with('datosejecutivo',null)
                ->with('flgactivo',0)
                ->with('datoa',$mensaje)
                ->with('cant',$cant)
                ->with('buscando',$buscando);
            }
        }elseif($request->get('codunico',null)!= null){
            if (strlen($request->get('codunico',null))<8) {
                flash('Debe ser un codunico valido')->error();
                return view('AsignacionResectorizacion.index')
                ->with('lead', null)
                ->with('data',null)
                ->with('caso',null)
                ->with('datosejecutivo',null)
                ->with('flgactivo',0)
                ->with('datoa',$mensaje)
                ->with('cant',$cant)
                ->with('buscando',$buscando);
            }
        }

        if ($request->get('documento')!= null || $request->get('codunico')!= null) {
            $buscando=1;
            $nuevo = 1;
            $datos['lead']= $entidad->buscarDatosReferido($request->get('documento'),$request->get('codunico'));
            $datos['empresa']=$entidad->ClientesVPC($request->get('documento'),$request->get('codunico'));

            if ($datos['lead']==null) {
                if ($datos['empresa']==null) {
                    $nuevo=1;
                }else{
                    $nuevo=0;
                }
            }
            if ($datos['lead']!=null) {
                $nuevo=0;
            }
            if ($cant>5) {
                flash('Este usuario ya tiene ingresado 5 referidos en este mes.')->error();
            }
            return view('AsignacionResectorizacion.index')
                ->with('nuevo',$nuevo)
                ->with('documento',$request->get('documento',null))
                ->with('datosejecutivo',$arbol->getCodSectorista($this->user->getValue('_registro')))
                ->with('undatoejecutivo',$arbol->getOneCodSectorista($this->user->getValue('_registro')))
                ->with('lead', $datos['lead'])
                ->with('data',$datos['empresa'])
                ->with('caso',null)
                ->with('flgactivo',0)
                ->with('datoa',$mensaje)
                ->with('cant',$cant)
                ->with('buscando',$buscando);
        }

        if ($request->get('documento')== null && $request->get('codunico')== null) {
            return view('AsignacionResectorizacion.index')
                    ->with('lead', null)
                    ->with('data',null)
                    ->with('caso',null)
                    ->with('datosejecutivo',null)
                    ->with('undatoejecutivo',null)
                    ->with('flgactivo',0)
                    ->with('datoa',$mensaje)
                    ->with('cant',$cant)
                    ->with('buscando',$buscando);
        }
    }

    public function registroReferidoNuevo(Request $request)
    {
        // dd($request->all());
        if ($request->get('facturacion')==null) {
            flash('Debe llenar el dato de facturación (soles).')->error();
            return redirect()->route('rr.sector',['documento'=>$request->get('documento')]);
        }
        if ($request->get('facturacion')<3000000) {
            flash('La facturación debe ser mayor a 3 MM soles.')->error();
            return redirect()->route('rr.sector',['documento'=>$request->get('documento')]);
        }else{
            if (!empty($request->file('adjuntoReferido'))) {
                $mensaje = (pathinfo($request->file('adjuntoReferido')->getClientOriginalName(), PATHINFO_EXTENSION)=='msg') ? "1" : "0";
                if ($mensaje=='0') {
                    flash('Debes Adjuntar el correo de tu Jefatura.')->error();
                    return redirect()->route('rr.sector',['documento'=>$request->get('documento')]);
                }else{
                    $correojefe=new Asignacion();
                    $correojefe->guardarCorreoReferido($request->get('documento'),$this->user->getValue('_registro'),$request->file('adjuntoReferido'));
                }
            }
            $data=new Asignacion();
            $entidad=new Lead();
            $lead= $entidad->buscarDatosReferido($request->get('documento'),$request->get('codunico'));
            if ($data->Nuevoreferido($lead!=null ? true : false, $request->get('nombre'),$request->get('documento'),$request->get('adjuntoReferido'),$this->user->getValue('_registro'))) {
                flash('Referido agregado correctamente')->success();
                return redirect()->route('be.miprospecto.lista.index', ['documento' => $request->get('documento',null)]);
            }
        }
    }
    
    public function guardar(Request $request){
            // dd($request->all());
            $correojefe = $request->file('adjuntoReferido');
            $adjunto1 = $request->file('adjuntoDocumento1');
            $adjunto2 = $request->file('adjuntoDocumento2');
            
            if (!empty($adjunto1)) {
                $mensaje1= (pathinfo($request->file('adjuntoDocumento1')->getClientOriginalName(), PATHINFO_EXTENSION)=='msg') ? "1" : "0";
                if ($mensaje1=='0') {
                    flash('Adjuntar correo de conformidad del Asignado actual.')->error();
                    return back();
                }
            }

            if (!empty($adjunto2)) {
                $mensaje2= (pathinfo($request->file('adjuntoDocumento2')->getClientOriginalName(), PATHINFO_EXTENSION)=='msg') ? "1" : "0";
                if ($mensaje2=='0') {
                    flash('Adjuntar correo de conformidad del Sectorista actual.')->error();
                    return back();
                }
            }

            if (!empty($correojefe)) {
                $mensaje3= (pathinfo($request->file('adjuntoReferido')->getClientOriginalName(), PATHINFO_EXTENSION)=='msg') ? "1" : "0";
                if ($mensaje3=='0') {
                    flash('Adjuntar correo de conformidad del Jefe.')->error();
                    return back();
                }else{
                    $correojefe=new Asignacion();
                    $correojefe->guardarCorreoReferido($request->get('numDocumento'),$this->user->getValue('_registro'),$request->file('adjuntoReferido'));
                }
            }
            $entidad = new Lead();

            $asignacion = new Asignacion();
            //asignacion de Valores a Guardar
            $asignacion->setValues([
                '_num_doc' => $request->get('numDocumento'),
                '_codUnico'=> (!empty($request->get('codigoUnico'))) ? $request->get('codigoUnico') : (!empty($entidad->getLeadByNum($request->get('numDocumento'))) ? $entidad->getLeadByNum($request->get('numDocumento'))->COD_UNICO : null),
                '_bancaOrigen'=> (!empty($request->get('banca')) ? $request->get('banca') : null),
                '_bancaDestino'=> $this->user->getValue('_registro') ? $entidad->getSectoristaArbol($this->user->getValue('_registro'))->BANCA : null,
                '_registroAsignado'=> $request->get('registroAsignado'),
                '_registroDest'=> $this->user->getValue('_registro'),
                '_codSectO'=> (!empty($request->get('codcortoOrigen')) ? $request->get('codcortoOrigen') : null),
                '_codSecDest'=> (!empty($request->get('codcorto')) ? $request->get('codcorto') : null),
                '_archivo1' => $adjunto1,
                '_extension1' => $adjunto1 ? pathinfo($request->file('adjuntoDocumento1')->getClientOriginalName(), PATHINFO_EXTENSION) : null,
                '_archivo2' => $adjunto2,
                '_extension2' => $adjunto2 ? pathinfo($request->file('adjuntoDocumento2')->getClientOriginalName(), PATHINFO_EXTENSION) : null,
                '_caso'=>(!empty($request->get('caso')) ? $request->get('caso') : null),
                '_flgpendiente'=>'1'
            ]);

            $result['rasig']=0;
            $result['rsec']=0;
            
            $datalead = $entidad->buscarDatosReferido($request->get('numDocumento'));
            if ($datalead==null) {
                $lead = new Lead();
                $lead->reasignar($asignacion);
                $result['rasig']=1;
            }else{
                if ($datalead->REGISTRO_EN==null || $datalead->REGISTRO_EN != $this->user->getValue("_registro") ) {
                    $lead = new Lead();
                    $lead->reasignar($asignacion);
                    $result['rasig']=1;
                }
            }

            $datavpc = $entidad->Reasignaciones($request->get('numDocumento'));//dd($datavpc);
            if ($result['rasig']=1 && ($request->get('codcortoOrigen')!=null || $request->get('codSectorista')!=null)) {
                if ($datavpc!=null) {
                    $result['0']=0;
                }else{
                    if ($request->get('codcortoOrigen')<>$request->get('codcorto')) {
                        $asignacion->registrar();
                        $result['rsec']=1;
                    }
                }
            }
            if ($result['rasig']==1 && $result['rsec']==0) {
                if ($request->get('registroAsignado')==null) {
                    flash('Se realizó la asignación correctamente.')->success();
                }elseif($request->get('registroAsignado')!=null){
                    flash('Se realizó la reasignación correctamente.')->success();
                }
            }
            if ($result['rasig']==1 && $result['rsec']==1) {
                if ($request->get('registroAsignado')==null) {
                    flash('Se realizó la asignación correctamente. En cuanto a la resectorización se verá el día de mañana.')->success();
                }elseif($request->get('registroAsignado')!=null){
                    flash('Se realizó la reasignación correctamente. En cuanto a la resectorización se verá el día de mañana.')->success();
                }
            }
            if ($entidad->getLeadByNum($request->get('numDocumento'))!=null) {
                if ($entidad->getLeadByNum($request->get('numDocumento'))->FLG_ES_CLIENTE=='1') {
                    return redirect()->route('be.actividades.index', ['documento' => $request->get('numDocumento', null)]);
                }else{
                    return redirect()->route('be.miprospecto.lista.index', ['documento' => $request->get('numDocumento',null)]);
                }
            }else{
                return redirect()->route('be.miprospecto.lista.index', ['documento' => $request->get('numDocumento',null)]);
            }
    }
   
}
