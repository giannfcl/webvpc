<?php

namespace App\Http\Controllers\API\BPE;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entity\Leads as Lead;
use App\Entity\LeadCampanha as Camp;
use Validator;

class LeadsController extends Controller {

    //listar todos los leads por ejecutivo
    public function listar(Request $request){

        //validar de que existe

        //registro
        $registro = $request->get('registro');
        $nombre = $request->get('nombre');
        $camp = $request->get('camp',null);

        $resultado = Lead::getLeadsEjecutivoResumido($registro,$camp,$nombre);
        return response()->json($resultado);
    }

    //traer la informacion de un lead
    public function get(Request $request){

    }

    //listar campañas por lead
    public function listarCampanhas(Request $request){
      $registro = $request ->get('registro');

      $resultado = Camp::getCampanhasByEjecutivoResumido($registro);
      return response()->json($resultado);

    }


    
      
}