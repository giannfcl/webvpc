<?php 
namespace App\Http\Controllers\BPE\Blacklist;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Entity\Usuario;
use App\Entity\Blacklist;
use Jenssegers\Date\Date as Carbon;
use DB;

use Validator;

class BlacklistController extends Controller{
	const VIGENTE=1;
	public function index(Request $request)
	{
		$busqueda = [
			'page' => $request->get('page',1),
			'documento' => $request->get('documento_b',null),
		];
		$orden = [
            'order' => $request->get('order',null),
        ];

        $entity = new Blacklist();
        $consulta = $entity->Buscar($busqueda);

        $result=$consulta->setPath(config('app.url').'bpe/blacklist/lista');

		return view('bpe.blacklist.index')
				->with('todos',$result)
				->with('orden',$orden)
				->with('busqueda',$busqueda);
	}

	public function nuevotelefonos(Request $request)
	{
		$entity_1 = new Blacklist();
		$datos=[
			'tipodoc'=>$request->get('tipodoc',null),
			'numdoc'=>$request->get('documento',null),
			'telefono'=>$request->get('numero',null),
		];
		if ($entity_1->gettelefonosexistentes($datos)>0) {
			$mensaje ="El número ".$request->get('numero',null) ." ya está registrado a la empresa con documento : ".$request->get('documento',null);
			flash($mensaje)->error();
			return redirect()->route('bpe.blacklist.lista');	
		}
		$nuevo=[
			'TIPO_DOC'=>$request->get('tipodoc',null),
			'NUM_DOC'=>$request->get('documento'),
			'NUMERO'=>$request->get('numero'),
			'VIGENTE'=>self::VIGENTE,
			'MOTIVO'=>$request->get('motivo'),
			'MOTIVO_DETALLE'=>$request->get('motivodetalle'),
			'FECHA_REGISTRO'=>Carbon::now(),
			'REGISTRO_EN'=>$this->user->getValue('_registro'),
		];
		
		$entity = new Blacklist();

		if ($entity->nuevotelefonos($nuevo)) {
			flash('Se agrego correctamento el documento y el teléfono')->success();
		}else{
			flash('No se pudo agregar el documento con el teléfono')->error();
		}
		return redirect()->route('bpe.blacklist.lista');
	}
}