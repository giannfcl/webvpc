<?php 
namespace App\Http\Controllers\BPE\Campanhas\Call;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entity\Leads as Leads;
use App\Entity\Cita as Citas;
use App\Entity\Canal as Canal;
use App\Entity\Banca as Banca;
use Validator;  

class CitaController extends Controller{

	public function nuevo(Request $request){

        $documento = $request->get('documento', null);

        if (!$documento){
            return view('bpe.campanhas.call.cita-nuevo')
            ->with('documento',$documento)
            ->with('lead',null);
        }

        $lead = Leads::getLeadCall($documento);

        if ($lead){
            $citasEjecutivo = Citas::getSemanaEjecutivo($lead->EN_REGISTRO);
            return view('bpe.campanhas.call.cita-nuevo')
            ->with('lead',$lead)
            ->with('citasEjecutivo',$citasEjecutivo)
            ->with('horarioEjecutivo',Citas::getFormattedSemanaEjecutivo($citasEjecutivo))
            ->with('calendario',Citas::getCalendarioEjecutivoCall())
            ->with('horario',Citas::getHorario())
            ->with('horasDisponibles',Citas::getHorasCitas())
            ->with('documento',$documento);
        }else{
            return view('bpe.campanhas.call.cita-nuevo')
            ->with('lead',$lead)
            ->with('documento',$documento);
        }
        
        return view('welcome');
    }

    public function guardar(Request $request){       


        $cita = new Citas();


        if (($request->get('cita', null))){
            
            //Actualizar
            $validator = Validator::make($request->all(), [
                'cita' => 'required',
                'fecha' => 'required|date_format:Y-m-d',
                'hora' => 'required|date_format:H:i',
                'pcontacto' => 'required|max:100',
                'telefono' => 'required|digits_between:6,9',
                'direccion' => 'required|max:255',
                'autDatos' => 'required',
            ]);

            if ($validator->fails()) {
                flash(array_flatten($validator->errors()->getMessages()))->error();
                return back()->withInput($request->input());
            }

            $cita->getById($request->get('cita', null));

            $cita->setValues([
                '_contacto' => $request->get('pcontacto', null),
                '_hora' => $request->get('hora', null),
                '_fecha' => $request->get('fecha', null),
                '_contactoTelefono' => $request->get('telefono', null),
                '_contactoDireccion' => $request->get('direccion', null),
                '_contactoReferencia' => $request->get('referencia', null),
                '_autorizacionDatos' => $request->get('autDatos',null)
            ]);

            if($cita->actualizar()){
                flash($cita->getMessage())->success();
                return redirect()->route('bpe.campanha.call.cita.nuevo');
            }   else{
                flash($cita->getMessage())->error();
                return back()->withInput($request->input());
            }

        }else{

            //Registrar
            $validator = Validator::make($request->all(), [
                'lead' => 'required',
                'regEjecutivo' => 'required',
                'pcontacto' => 'required|max:100',
                'telefono' => 'required|digits_between:6,9',
                'direccion' => 'required|max:255',
                'fecha' => 'required|date_format:Y-m-d',
                'hora' => 'required|date_format:H:i',
                'autDatos' => 'required',
            ]);

            if ($validator->fails()) {
                flash(array_flatten($validator->errors()->getMessages()))->error();
                return back()->withInput($request->input());
            }


            $cita->setValues([
                '_lead' => $request->get('lead', null),
                '_agendador' => $this->user->getValue('_registro'),
                '_ejecutivo' => $request->get('regEjecutivo', null),
                '_hora' => $request->get('hora', null),
                '_fecha' => $request->get('fecha', null),
                '_contacto' => $request->get('pcontacto', null),
                '_contactoTelefono' => $request->get('telefono', null),
                '_contactoDireccion' => $request->get('direccion', null),
                '_contactoReferencia' => $request->get('referencia', null),
                '_tipo' => Canal::CALL_CENTER,
                '_autorizacionDatos' => $request->get('autDatos',null)
            ]);

            if($cita->registrar()){
                flash($cita->getMessage())->success();
                return redirect()->route('bpe.campanha.call.cita.nuevo');
            }   else{
                flash($cita->getMessage())->error();
                return back()->withInput($request->input());
            }    
        }

        
    }
}