<?php 
namespace App\Http\Controllers\BPE\Campanhas;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entity\Feedback as Feedback;
use App\Entity\GestionResultado as GestionResultado;
use App\Entity\Tienda as Tienda;
use App\Entity\Ubigeo as Ubigeo;
use Validator;

class UtilsController extends Controller{

	public function getMotivoByResultado(Request $request){
        return GestionResultado::getBpeMotivosByResultado($request->get('resultado'));
        ;
    }

    public function getTiendasByCentro(Request $request){
        return Tienda::getTiendas($request->get('centro'));
    }

    public function getEjecutivosByTienda(Request $request){        
        return Tienda::getEjecutivos($request->get('tienda'));
    }

    public function getCentrosByZonal(Request $request){
        return Tienda::getCentros($request->get('zonal'));
    }

    public function getDistritosByProvincia(Request $request){
        return Ubigeo::getDistrito($request->get('provincia'));
    }

    public function getProvinciasByDepartamento(Request $request){
        return Ubigeo::getProvincia($request->get('departamento'));
    }

    public function registrarFeedbackTelefono(Request $request){


    	$validator = Validator::make($request->all(), [
            'lead' => 'required',
            'telefono' => 'required',
            'feedback' => 'required',
        ]);

        if ($validator->fails()) {
        	return response()->json(['msg' => 'Input incorrecto'], 404);
        }


        
        $feedback = new Feedback();
        $feedback->setValues([
            '_lead' => $request->get('lead', null),
            '_ejecutivo' => $this->user->getValue('_registro'),
            '_tipoRegistro' => Feedback::TREGISTRO_TELEFONO_TEXTO,
            '_valor' => $request->get('telefono', null),
            '_feedback' => $request->get('feedback', null),
        ]);

        if ($feedback->registrar()){
        	return response()->json(['msg' => 'ok'], 200);
        }else{
        	return response()->json(['msg' => $feedback->getMessage()], 404);
        }

        
    }

    public function quitarFeedbackTelefono(Request $request){
    	$validator = Validator::make($request->all(), [
            'lead' => 'required',
            'telefono' => 'required',
        ]);

        if ($validator->fails()) {
        	return response()->json(['msg' => 'Input incorrecto'], 404);
        }
        
        $feedback = new Feedback();
        $feedback->setValues([
            '_lead' => $request->get('lead', null),
            '_tipoRegistro' => Feedback::TREGISTRO_TELEFONO_TEXTO,
            '_valor' => $request->get('telefono', null),
        ]);

        if ($feedback->eliminar()){
        	return response()->json(['msg' => 'ok'], 200);
        }else{
        	return response()->json(['msg' => $feedback->getMessage()], 404);
        }

    }
}