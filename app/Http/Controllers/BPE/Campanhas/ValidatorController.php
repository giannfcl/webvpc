<?php 
namespace App\Http\Controllers\BPE\Campanhas;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entity\Cita as Citas;

class ValidatorController extends Controller{

	public function horarioEjecutivo(Request $request){
        $valid = Citas::esDisponible(
            $request->get('ejecutivo', null),
            $request->get('fecha', null),
            $request->get('hora', null),
        	$request->get('cita', null));
        return ['valid'=>$valid];
    }
}