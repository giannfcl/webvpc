<?php 

namespace App\Http\Controllers\BPE\Campanhas;

use App\Http\Controllers\Controller;
use App\Entity\Leads as EntityLead;
use App\Entity\GestionLead as GestionLead;
use App\Entity\LeadCampanha as LeadCampanha;
use App\Entity\Canal as Canal;

use Illuminate\Http\Request;

/**
* 
*/
class ConsultaNuevosController extends Controller
{	
	static function index(Request $req){
		$nro_doc= $req->get('nro_doc',null);

		$result = null;
		$gestiones = null;
		$campanhas = null;
		if ($nro_doc){
			$result = EntityLead::getLead('', $nro_doc);
			if ($result){
				$gestiones = GestionLead::getByLead($nro_doc);
				$campanhas = LeadCampanha::getCampanhasAtributoGestionByLead($nro_doc,Canal::getAll());
			}
			
		}

		return view('bpe/campanhas/consulta-nuevos')
		->with('lead',$result)
		->with('nro_doc', $nro_doc)
		->with('gestiones', $gestiones)
		->with('campanhas', $campanhas);
	}
}