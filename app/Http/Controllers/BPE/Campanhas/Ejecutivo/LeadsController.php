<?php
namespace App\Http\Controllers\BPE\Campanhas\Ejecutivo;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Entity\Leads as Leads;
use App\Entity\Usuario as Usuario;
use App\Entity\GestionResultado as GestionResultado;
use App\Entity\GestionLead as GestionLead;
use App\Entity\Feedback as Feedback;
use App\Entity\LeadCampanha as LeadCampanha;
use App\Entity\AddContacto as AddContacto;
use App\Entity\Banca as Banca;
use App\Entity\Canal as Canal;
use App\Entity\CatalogoMaestro;
use App\Entity\Cita as Citas;
use App\Entity\LeadEjecutivo as LeadEjecutivo;
use Jenssegers\Date\Date as Carbon;
use App\Entity\Tienda;
use Validator;
use PDF;

class LeadsController extends Controller{

	public function listar(Request $request){

		$busqueda = [
			'page' => $request->get('page',1),
			'lead' => $request->get('lead',null),
            'documento' => $request->get('documento',null),
            'campanha' => $request->get('campanha',null),
            'marca' => $request->get('marca',null),
            'distrito' => $request->get('distrito',null),
            'propension' => $request->get('propension',null),
        ];

        $orden = [
            'sort' => $request->get('sort','lead'),
            'order' => $request->get('order','asc'),
        ];

        //dd($orden);
        // Validamos los parametros de ordenamiento
        if (is_null($orden['sort']) || !in_array($orden['sort'], ['lead','direccion','deuda'])){
            $orden['sort'] = null;
            if (is_null($orden['order']) || !in_array($orden['order'], ['asc','desc'])){
                $orden = null;
            }
        }

        $pagina = Leads::getLeadsEjecutivoNegocio($this->user->getValue('_registro'),$busqueda,$orden)
        ->setPath(config('app.url').'/bpe/en/leads');




        $propension=LeadCampanha::getPropensiones();

        return view('bpe.campanhas.ejecutivo.leads')
        ->with('leads',$pagina)
        ->with('busqueda',$busqueda)
        ->with('orden',$orden)
        ->with('campanhas',LeadCampanha::getCampanhasByEjecutivo($this->user->getValue('_registro')))
        ->with('distritos',LeadCampanha::getDistritosByEjecutivo($this->user->getValue('_registro')))
        ->with('marcas',[1,2,3])
        ->with('propension',$propension)
        ->with('resumen',LeadCampanha::getResumenByEjecutivo($this->user->getValue('_registro'),$request->get('campanha',null)));

    }

    public function getPropensiones()
    {
        return LeadCampanha::getPropensiones();
    }


    public function detalle(Request $request){

        if (in_array(Auth::user()->ROL,[Usuario::ROL_GERENTE_ZONA, Usuario::ROL_GERENTE_CENTRO,Usuario::ROL_GERENTE_TIENDA,Usuario::ROL_ADMINISTRADOR])){
            $lead = Leads::getLeadEjecutivoNegocio($request->get('ejecutivo', null),$request->get('lead', null));
        } else {
            $lead = Leads::getLeadEjecutivoNegocio($this->user->getValue('_registro'),$request->get('lead', null));
        }
        //dd(Leads::getAvalByLead($request->get('lead', null)));

        if ($lead){

            $campanhas = LeadCampanha::getCampanhasAtributoGestionByLead($request->get('lead', null));
            //$citasEjecutivo = Citas::getSemanaEjecutivo($lead->EN_REGISTRO);
            $citasEjecutivo = [];
            return view('bpe.campanhas.ejecutivo.detalle')
            ->with('lead',$lead)
            ->with('resultados',GestionResultado::getBpeResultados(array_pluck($campanhas, 'ID_CAMP_EST')))
            ->with('motivos',GestionResultado::getBpeMotivos(array_pluck($campanhas, 'ID_CAMP_EST')))
            ->with('telefonos',Leads::formatTelefonos($lead))
            ->with('feedback',Feedback::listar($request->get('lead', null)))
            ->with('campanhas',$campanhas)
            ->with('contactos',AddContacto::getContactos($request->get('lead', null)))
            ->with('gestiones',GestionLead::getByLead($request->get('lead', null)))
            ->with('horasDisponibles',Citas::getHorasCitas())
            ->with('calendario',Citas::getCalendarioEjecutivoCall())
            ->with('horario',Citas::getHorario())
            ->with('citasEjecutivo',$citasEjecutivo)
            ->with('horarioEjecutivo',Citas::getFormattedSemanaEjecutivo($citasEjecutivo))
            ->with('zonales',Tienda::getZonales())
            ->with('sectores',\App\Entity\Sector::getAll())
            ->with('giros',\App\Entity\Giro::getAll())
            ->with('estadoCivil',\App\Entity\CatalogoMaestro::getCatalogo(null,'ESTADO-CIVIL','estadoCivil'))
            ->with('tipoSociedad',\App\Entity\CatalogoMaestro::getCatalogo(null,'TIPO-SOCIEDAD','tipoSociedad'))
            ->with('tipoVia',\App\Entity\CatalogoMaestro::getCatalogo(null,'TIPO-VIA','tipoVia'))
            ->with('departamentos',\App\Entity\Ubigeo::getComboDepartamento())
            ->with('provincias',\App\Entity\Ubigeo::getComboProvincia())
            ->with('distritos',\App\Entity\Ubigeo::getComboDistrito())
            ->with('bancos',\App\Entity\Banco::getBancosReactiva())
            ->with('resultadosAcepta',GestionResultado::getResultadosAcepta())
            ->with('productos',Leads::getProductosByRuc($request->get('lead', null)))
            ->with('avales',Leads::getAvalByLead($request->get('lead', null)))
            ->with('cargos',\App\Entity\CatalogoMaestro::getCatalogo(null,'CARGOS-COFIDE','cargosCofide'));
        }else{
            flash('No se encuentra el cliente/lead seleccionado')->error();
            return redirect()->route('bpe.campanha.ejecutivo.leads.listar');
        }
    }

    public function actualizarlead(Request $request)
    {
        $entidad = new Leads();
        return response()->json($entidad->actualizarLead($request->all(),$this->user->getValue('_registro')));
    }

    public function nuevoContacto(Request $request){

        $validator = null;
        switch ($request->get('cboTipoContacto',null)) {
            case 'TELEFONO':
            $validator = Validator::make($request->all(), [
                'lead' => 'required',
                'txtContacto' => 'required|digits_between:6,9',
            ]);
            break;
            case 'EMAIL':
            $validator = Validator::make($request->all(), [
                'lead' => 'required',
                'txtContacto' => 'required|email|max:75',
            ]);
            break;
            case 'DIRECCION':
            $validator = Validator::make($request->all(), [
                'lead' => 'required',
                'txtContacto' => 'required|max:255',
            ]);
            break;

            default:
            break;
        }



        if (!$validator || $validator->fails()) {
            return response()->json(['msg' => 'Input incorrecto'], 404);
        }

        $addContacto = new AddContacto();
        $addContacto->setValues([
            '_lead' => $request->get('lead', null),
            '_ejecutivo' => $this->user->getValue('_registro'),
            '_tipoContacto' => $request->get('cboTipoContacto'),
            '_valor' => $request->get('txtContacto', null)
        ]);
        if ($addContacto->registrar()){
            return response()->json(['msg' => 'ok'], 200);
        }else{
            return response()->json(['msg' => $addContacto->getMessage()], 404);
        }
    }

    public function nuevaGestion(Request $request){
        $validator = Validator::make($request->all(), [
            'lead' => 'required',
            'resultado' => 'required',
            'campanha' => 'required',
            'monto' => 'nullable|numeric',
            'tasa' => 'nullable|numeric',
            'plazo' => 'nullable|numeric',
            'gracia' => 'nullable|numeric',
            'comentario' => 'nullable|min:10|max:150',
        ]);

        if ($validator->fails()) {
            return response()->json(array_flatten($validator->errors()->getMessages()), 404);
        }



        $gestion = new GestionLead();
        $gestion->setValues([
            '_lead' => $request->get('lead', null),
            '_campanha' => $request->get('campanha', null),
            '_periodo' => $request->get('periodo', null),
            '_ejecutivo' => $this->user->getValue('_registro'),
            '_resultado' => $request->get('resultado', null),
            '_motivo' => $request->get('motivo', null),
            '_comentario' => $request->get('comentario', null),
            '_banca' => Banca::BPE,
            '_rolGestion' => Canal::EJECUTIVO_NEGOCIO,
            '_fechaVolverLLamar' => $request->get('fecha', null),
            '_visitado' => $request->get('visita', null),
            '_solicitudMonto' => $request->get('monto', null),
            '_solicitudTasa' => $request->get('tasa', null),
            '_solicitudPlazo' => $request->get('plazo', null),
            '_solicitudGracia' => $request->get('gracia', null),
        ]);

        if($gestion->registrar()){
            return response()->json($gestion->getById(), 200);
        }else{
            return response()->json(['msg' => $gestion->getMessage()], 404);
        }
    }

    public function updateEtiqueta(Request $request){

        $validator = Validator::make($request->all(), [
            'lead' => 'required',
            'etiqueta' => 'required',
        ]);

        $gestion = new LeadEjecutivo();
        $gestion->setValues([
            '_lead' => $request->get('lead', null),
            '_ejecutivo' => $this->user->getValue('_registro'),
            '_etiquetaEjecutivo' => $request->get('etiqueta', null)
        ]);

        if($gestion->updateEtiqueta()){
            return response()->json(['msg' => 'ok'], 200);
        }else{
            return response()->json(['msg' => $gestion->getMessage()], 404);
        }
    }

    public function imprimir(Request $request){
        $busqueda = [
            'page' => $request->get('page',1),
            'lead' => $request->get('lead',null),
            'documento' => $request->get('documento',null),
            'campanha' => $request->get('campanha',null),
            'marca' => $request->get('marca',null),
            'distrito' => $request->get('distrito',null),
            'ejecutivo' => $request->get('ejecutivo',null),
        ];

        $orden = [
            'sort' => $request->get('sort',null),
            'order' => $request->get('order',null),
        ];

        // Validamos los parametros de ordenamiento
        if (is_null($orden['sort']) || !in_array($orden['sort'], ['lead','direccion','deuda'])){
            $orden['sort'] = null;
            if (is_null($orden['order']) || !in_array($orden['order'], ['asc','desc'])){
                $orden = null;
            }
        }

        $leads = Leads::imprimirLeadsEjecutivoNegocio($this->user->getValue('_registro'),$busqueda,$orden);
        $pdf = \PDF::loadView('bpe.campanhas.ejecutivo.imprimir',['leads' => $leads])
        ->setPaper('a4', 'landscape')
        ->setWarnings(false);
        return $pdf->stream();
    }

    public function enviarAsistente(Request $request){
        $validator = Validator::make($request->all(), [
            'lead' => 'required',
            'marca' => 'required',
        ]);

        $gestion = new LeadEjecutivo();
        $gestion->setValues([
            '_lead' => $request->get('lead', null),
            '_ejecutivo' => $this->user->getValue('_registro'),
            '_marcaAsistenteComercial' => $request->get('marca', null)
        ]);

        if($gestion->updateMarca()){
            return response()->json(['msg' => 'ok'], 200);
        }else{
            return response()->json(['msg' => $gestion->getMessage()], 404);
        }
    }

    public function reprogramarCita(Request $request){
        $validator = Validator::make($request->all(), [
            'cita' => 'required',
            'fecha' => 'required|date_format:Y-m-d',
            'hora' => 'required|date_format:H:i',
        ]);

        $cita = new Citas();
        $cita->getById($request->get('cita', null));

        $fechaReprogramacion = Carbon::createFromFormat('Y-m-d H:i', $request->get('fecha') . ' ' . $request->get('hora'));

        if($cita->reprogramar($fechaReprogramacion)){
            flash($cita->getMessage())->success();
            return redirect()->route('bpe.campanha.ejecutivo.leads.detalle',['lead'=>$cita->getValue('_lead')]);
        }else{
            flash($cita->getMessage())->error();
            return redirect()->route('bpe.campanha.ejecutivo.leads.detalle',['lead'=>$cita->getValue('_lead')]);
        }
    }

    public function pagarepdf(Request $request)
	{
        $acuerdos = Leads::getDatosBasicosCliente($request->get('numdoc',null));
        $direccion = Leads::getDireccion($request->get('numdoc',null));
        $avales = Leads::getAvalByLead($request->get('numdoc', null));
        if (!isset($direccion[0]->DIRECCION)){
            return 'No se han completado los datos de dirección del cliente';
        }
        //dd($avales);
		$pdf = PDF::loadView('bpe.campanhas.ejecutivo.pagare',compact('acuerdos','direccion','avales'));
        return $pdf->download('Pagaré.pdf');
	}

	public function llenadopdf(Request $request)
	{
		$datos = Leads::getDatosBasicosCliente($request->get('numdoc',null));
        $direccion = Leads::getDireccion($request->get('numdoc',null));
        $avales = Leads::getAvalByLead($request->get('numdoc', null));

        if (!isset($direccion[0]->DIRECCION)){
            return 'No se han completado los datos de dirección del cliente';
        }
		$pdf = PDF::loadView('bpe.campanhas.ejecutivo.llenadopagare',compact('datos','direccion','avales'));
        return $pdf->download('Llenado.pdf');
    }

    public function ddjjfondocrecerpdf(Request $request)
    {
        $datos = Leads::getDatosBasicosCliente($request->get('numdoc',null));
        $nombremeses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        $fecha['dia'] = Carbon::now()->day;
        $fecha['mes'] = $nombremeses[(Carbon::now()->format('n')) - 1];
        $fecha['anio'] = Carbon::now()->year;
        $direccion = Leads::getDireccion($request->get('numdoc',null));
        $pdf = PDF::loadView('bpe.campanhas.ejecutivo.ddjjfondocrecer',compact('datos','fecha','direccion'));
        return $pdf->download('Declaración Jurada Fondo Crecer PN y PJ.pdf');
    }

    public function cartainstruccionpdf(Request $request)
    {
        $datos = Leads::getDatosBasicosCliente($request->get('numdoc',null));

        $nombremeses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        $fecha['dia'] = Carbon::now()->day;
        $fecha['mes'] = $nombremeses[(Carbon::now()->format('n')) - 1];
        $fecha['anio'] = Carbon::now()->year;

        $productos = Leads::getProductosByRuc($request->get('numdoc', null))->toArray();
        //dd(Leads::getProductosByRuc($request->get('numdoc', null)),$productos);
        $pdf = PDF::loadView('bpe.campanhas.ejecutivo.cartainstruccion',compact('datos','productos','fecha'));
        return $pdf->download('Carta de instrucción para cancelación de 2 o más créditos.pdf');
    }

}
