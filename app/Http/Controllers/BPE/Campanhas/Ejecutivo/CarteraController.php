<?php
namespace App\Http\Controllers\BPE\Campanhas\Ejecutivo;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Entity\Leads as Leads;
use App\Entity\Cliente as Cliente;
use App\Entity\Usuario as Usuario;
use App\Entity\GestionResultado as GestionResultado;
use App\Entity\GestionLead as GestionLead;
use App\Entity\Feedback as Feedback;
use App\Entity\LeadCampanha as LeadCampanha;
use App\Entity\AddContacto as AddContacto;
use App\Entity\Banca as Banca;
use App\Entity\Canal as Canal;
use App\Entity\Cita as Citas;
use App\Entity\Tienda;
use App\Entity\LeadEjecutivo as LeadEjecutivo;
use Jenssegers\Date\Date as Carbon;
use Validator;

class CarteraController extends Controller{

	public function cartera(Request $request){
        $busqueda = [
            'page' => $request->get('page',1),
            'cliente' => $request->get('cliente',null),
            'codigo'=>$request->get('codigo',null),
            'documento' => $request->get('documento',null),
            'campanha' => $request->get('campanha',null),
            'marca' => $request->get('marca',null),
            'distrito' => $request->get('distrito',null),
            'producto' => $request->get('producto',null),
            'bloqueo' => $request->get('bloqueo',null),
            'segmento' => $request->get('segmento',null),
            'riesgos' => $request->get('riesgos',null),
            'ejecutivo'=>$request->get('ejecutivo',null),
            'zonal'=>$request->get('zonal',null),
            'centro'=>$request->get('centro',null),
            'tienda'=>$request->get('tienda',null),
            'flgCanal'=>$request->get('flgCanal',null),
            'canal'=>$request->get('canal',null),
            'canalAtencion'=>$request->get('canalAtencion',null),
            'proximosBloq'=>$request->get('proximosBloq',null)
            ];

        //dd($busqueda);

        if(Auth::user()->ROL==Usuario::ROL_GERENTE_ZONA and $busqueda['zonal']==NULL)
            $busqueda['zonal']=$this->user->getValue('_zona');
        if(Auth::user()->ROL==Usuario::ROL_GERENTE_CENTRO and ($busqueda['centro']==NULL )  ) {
            $busqueda['centro']=$this->user->getValue('_centro');
        }
        if(Auth::user()->ROL==Usuario::ROL_GERENTE_TIENDA and ($busqueda['tienda']==NULL )  ) {
            $busqueda['tienda']=$this->user->getValue('_tienda');
        }


        $orden = [
            'sort' => $request->get('sort',null),
            'order' => $request->get('order',null),
        ];


        // Validamos los parametros de ordenamiento
        if (is_null($orden['sort']) || !in_array($orden['sort'], ['campanha','cliente','numProd','score','deuda','aprobado','disponible'])){
            $orden['sort'] = null;
            if (is_null($orden['order']) || !in_array($orden['order'], ['asc','desc'])){
                $orden = null;
            }
        }

        if (in_array(Auth::user()->ROL,[Usuario::ROL_GERENTE_ZONA, Usuario::ROL_GERENTE_CENTRO,Usuario::ROL_GERENTE_TIENDA,Usuario::ROL_ADMINISTRADOR])){
             $clientes=Cliente::getClientesEjecutivoNegocio($busqueda['ejecutivo'],$busqueda,$orden);
             $distritos=  LeadCampanha::getDistritosByEjecutivo($busqueda['ejecutivo'],1);
             $resumen=LeadCampanha::getResumenCarteraByJefe($busqueda['ejecutivo'],$busqueda);

        }
        else if(Auth::user()->ROL==Usuario::ROL_ASISTENTE_COMERCIAL){
            $clientes=Cliente::getClientesAsistenteComercial($this->user->getValue('_registro'),$busqueda,$orden);
            $distritos=  LeadCampanha::getDistritosByEjecutivo(null,1);
            $resumen=LeadCampanha::getResumenCarteraByAsistente($this->user->getValue('_registro'),$busqueda);
        }
        else {
             $clientes=Cliente::getClientesEjecutivoNegocio($this->user->getValue('_registro'),$busqueda,$orden);
             $distritos=  LeadCampanha::getDistritosByEjecutivo($this->user->getValue('_registro'),1);
             $resumen=LeadCampanha::getResumenCarteraByEjecutivo($this->user->getValue('_registro'),$busqueda);
        }

        $vista=view('bpe.campanhas.ejecutivo.cartera')
            ->with('clientes',$clientes->setPath(config('app.url').'bpe/cli/cartera'))
            ->with('busqueda',$busqueda)
            ->with('orden',$orden)
            ->with('campanhas',LeadCampanha::getCampanhasCartera())
            ->with('canales',Cliente::getCanalesCartera())
            ->with('canalesAtencion',Cliente::getCanalesAtencionCartera())
            ->with('distritos',$distritos)
            ->with('productos',Cliente::getProductosBPE())
            ->with('motivosB',Cliente::getMotivosBloqueoBPE())
            ->with('resumen',$resumen)
            ->with('marcas',[1,2,3]);


        $ejecutivos=Tienda::getEjecutivos($this->user->getValue('_tienda'));

        if (in_array(Auth::user()->ROL,[Usuario::ROL_GERENTE_ZONA, Usuario::ROL_GERENTE_CENTRO,Usuario::ROL_GERENTE_TIENDA,Usuario::ROL_ADMINISTRADOR]))
            $vista=$vista
                        ->with('ejecutivos',$ejecutivos)
                        ->with('zonales',Tienda::getZonales())
                        ->with('tiendas',Tienda::getTiendas($this->user->getValue('_centro')))
                        ->with('centros',Tienda::getCentros($this->user->getValue('_zona')));
        else {

            if(Auth::user()->ROL==Usuario::ROL_ASISTENTE_COMERCIAL)
                $vista=$vista->with('ejecutivos',Usuario::getEjecutivosByAsistenteComercial($this->user->getValue('_registro')));
            else
                $vista=$vista
                        ->with('ejecutivo',Usuario::getEjecutivoByRegistro($this->user->getValue('_registro')))
                        ->with('ejecutivos',null);

            $vista=$vista
                    ->with('zonales',null)
                    ->with('tiendas',null)
                    ->with('centros',null);

        }


        return $vista;
    }


	public function carteraDetalle(Request $request){
        if (in_array(Auth::user()->ROL,[Usuario::ROL_GERENTE_ZONA, Usuario::ROL_GERENTE_CENTRO,Usuario::ROL_GERENTE_TIENDA,Usuario::ROL_ADMINISTRADOR,Usuario::ROL_ASISTENTE_COMERCIAL])){

            $cliente=Cliente::getClienteEjecutivoNegocio($request->get('ejecutivo', null),$request->get('cliente', null));

        }
        else {
            $cliente=Cliente::getClienteEjecutivoNegocio($this->user->getValue('_registro'),$request->get('cliente', null));
        }

        if ($cliente){

           $campanhaCliente=$cliente->ID_CAMP_EST;

        //dd("HOLI");
           return view('bpe.campanhas.ejecutivo.cartera-detalle')
            ->with('cliente',$cliente)
            ->with('resultados',GestionResultado::getBpeResultados($campanhaCliente)[$campanhaCliente])
            ->with('motivos',GestionResultado::getBpeMotivos($campanhaCliente))
            ->with('telefonos',Leads::formatTelefonos($cliente))
            ->with('feedback',Feedback::listar($request->get('cliente', null)))
            ->with('contactos',AddContacto::getContactos($request->get('cliente', null)))
            ->with('gestiones',GestionLead::getByLead($request->get('cliente', null),1));

        }else{
            flash('No se encuentra el cliente seleccionado')->error();
            return redirect()->route('bpe.campanha.ejecutivo.clientes.cartera');
        }
	}


    public function cambioCampanha(Request $request){

        $campanhaNueva=$request->get('cboCampanhasActivas',null);
        //dd($campanhaNueva);
        $cliente=$request->get('clienteUpdate',null);
        //dd($campanhaNueva,$cliente);


        $leadCampanha = new LeadCampanha();

        if($leadCampanha->cambioCampanhaCliente($this->user->getValue('_registro'),$cliente,$campanhaNueva)){
            flash($leadCampanha->getMessage())->success();
            return back();
        }
        else{
            flash($leadCampanha->getMessage())->error();
            return back();
        }


    }

    public function nuevaGestion(Request $request){

         $validator = Validator::make($request->all(), [
            'cliente' => 'required',
            'resultado' => 'required',
            'campanha' => 'required',
            'fecha' => 'nullable|date_format:Y-m-d',
            'comentario' => 'nullable|min:10|max:150',
        ]);

        if ($validator->fails()) {
            return response()->json(array_flatten($validator->errors()->getMessages()), 404);
        }


        $gestion = new GestionLead();
        $gestion->setValues([
            '_lead' => $request->get('cliente', null),
            '_campanha' => $request->get('campanha', null),
            '_periodo' => $request->get('periodo', null),
            '_ejecutivo' => $this->user->getValue('_registro'),
            '_resultado' => $request->get('resultado', null),
            '_motivo' => $request->get('motivo', null),
            '_comentario' => $request->get('comentario', null),
            '_banca' => Banca::BPE,
            '_rolGestion' => Canal::EJECUTIVO_NEGOCIO,
            '_fechaVolverLLamar' => $request->get('fecha', null),
            '_visitado' => null,
        ]);

        if($gestion->registrarCartera()){
            return response()->json($gestion->getById(), 200);
        }else{
            return response()->json(['msg' => $gestion->getMessage()], 404);
        }
    }

    public function imprimir(Request $request){

        $busqueda = [
            'cliente' => $request->get('cliente',null),
            'codigo'=>$request->get('codigo',null),
            'documento' => $request->get('documento',null),
            'campanha' => $request->get('campanha',null),
            'marca' => $request->get('marca',null),
            'distrito' => $request->get('distrito',null),
            'producto' => $request->get('producto',null),
            'bloqueo' => $request->get('bloqueo',null),
            'segmento' => $request->get('segmento',null),
            'riesgos' => $request->get('riesgos',null),
            'ejecutivo'=>$request->get('ejecutivo',null),
            'zonal'=>$request->get('zonal',null),
            'centro'=>$request->get('centro',null),
            'tienda'=>$request->get('tienda',null)
            ];


        $orden = [
            'sort' => $request->get('sort',null),
            'order' => $request->get('order',null),
        ];

        $clientes = Cliente::imprimirClientes($this->user->getValue('_registro'),$busqueda,$orden);
        $pdf = \PDF::loadView('bpe.campanhas.ejecutivo.cartera-imprimir',['clientes' => $clientes])
        ->setPaper('a4', 'landscape')
        ->setWarnings(false);
        return $pdf->stream();
    }


}
