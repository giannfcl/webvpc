<?php

namespace App\Http\Controllers\BPE\Campanhas\Gerente;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entity\Usuario;
use App\Entity\Tienda;
use App\Entity\Leads;
use App\Entity\LeadCampanha;

class EjecutivoController extends Controller {

    public function resumen(Request $request) {
        $busqueda = [
            'page' => $request->get('page', 1),
            'ejecutivo' => $request->get('ejecutivo', null),
            'zonal' => $request->get('zonal', null),
            'tienda' => $request->get('tienda', null),
            'centro' => $request->get('centro', null),
        ];

        $orden = [
            'field' => $request->get('field', null),
            'order' => $request->get('order', null),
        ];
        
        // Validamos los parametros de ordenamiento
        if (is_null($orden['field']) || !in_array($orden['field'], ['ejecutivo','avance','vencidas'])){
            $orden['field'] = null;
            if (is_null($orden['order']) || !in_array($orden['order'], ['asc','desc'])){
                $orden = null;
            }
        }       
        

        $result = Usuario::getResumenEjecutivosGerente($this->user->getValue('_rol'),$busqueda,$orden,$this->user->getValue('_tienda'),$this->user->getValue('_centro'),$this->user->getValue('_zona'))->setPath(config('app.url').'bpe/gt/resumen-ejecutivos');

        return view('bpe.campanhas.gerente.resumen')
                        ->with('ejecutivos', $result)
                        ->with('busqueda', $busqueda)
                        ->with('zonales',Tienda::getZonales())
                        ->with('tiendas',Tienda::getTiendas($this->user->getValue('_centro')))
                        ->with('centros',Tienda::getCentros($this->user->getValue('_zona')))
                        ->with('orden', $orden);
    }

    public function detalle(Request $request) {
        $busqueda = [
            'page' => $request->get('page',1),
            'lead' => $request->get('lead',null),
            'documento' => $request->get('documento',null),
            'campanha' => $request->get('campanha',null),
            'marca' => $request->get('marca',null),
            'distrito' => $request->get('distrito',null),
            'ejecutivo' => $request->get('ejecutivo',null),
        ];

        $orden = [
            'sort' => $request->get('sort',null),
            'order' => $request->get('order',null),
        ];

        // Validamos los parametros de ordenamiento
        if (is_null($orden['sort']) || !in_array($orden['sort'], ['lead','direccion','deuda'])){
            $orden['sort'] = null;
            if (is_null($orden['order']) || !in_array($orden['order'], ['asc','desc'])){
                $orden = null;
            }
        }

        $pagina = Leads::getLeadsEjecutivoNegocio($request->get('ejecutivo',null),$busqueda,$orden)
                ->setPath(config('app.url').'bpe/gt/detalle-ejecutivos');

        return view('bpe.campanhas.gerente.ejecutivo-detalle')
        ->with('leads',$pagina)
        ->with('busqueda',$busqueda)
        ->with('orden',$orden)
        ->with('campanhas',LeadCampanha::getCampanhasByEjecutivo($request->get('ejecutivo',null)))
        ->with('distritos',LeadCampanha::getDistritosByEjecutivo($request->get('ejecutivo',null)))
        ->with('marcas',[1,2,3,4])
        ->with('resumen',LeadCampanha::getResumenByEjecutivo($request->get('ejecutivo',null),$request->get('campanha',null)));
    }

}
