<?php

namespace App\Http\Controllers\BPE\Campanhas\Gerente;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Usuario;
use App\Entity\LeadEjecutivo as LeadEjecutivo;
use App\Entity\Cita as CitasEj;
use App\Entity\Leads as Leads;

class ReasignarController extends Controller {

    function index(Request $request) {
        return view('bpe.campanhas.gerente.reasignacion-leads')
                        ->with('horasDisponibles', CitasEj::getHorasCitas());
    }

    function consultarLead(Request $request) {

        $lead = new Leads();
        $data = $lead->getLeadReasignacion($request->get('num_doc', null), $request->get('ejecutivo', null));
        if ($data) {
            return ['status' => 'ok', 'data' => (array) $data];
        } else {
            return ['status' => 'error', 'message' => $lead->getMessage()];
        }
    }

    function consultarEN(Request $request) {
        $registro_en = $request->get('registro_en', null);
        $ejecutivo = Usuario::getEjecutivo($registro_en);
        return $ejecutivo;
    }

    function reasignar(Request $request) {
        
        $leadej = new LeadEjecutivo();
        $citas = array_filter($request->get('cita', []));
        $fechas = array_filter($request->get('fecha', []));
        $horas = array_filter($request->get('hora', []));
        
        $result = $leadej->reasignacionMasiva(
                $this->user->getValue('_registro'),
                $request->get('lead', null), //leads
                $citas,
                $fechas,
                $horas,
                $request->get('en', null) //ejecutivo
        );



        if ($result) {
            flash($leadej->getMessage())->success();
        } else {
            flash($leadej->getMessage())->error();
        }
        return redirect()->route('bpe.campanha.gerente.reasignacion.index');
    }

    function getHorarioEN(Request $request) {
        $ejecutivo = $request->get('en', null);
        return CitasEj::getCitasPendientesByEjecutivo($ejecutivo);
    }

}
