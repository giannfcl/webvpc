<?php

namespace App\Http\Controllers\BPE\Campanhas\Gerente;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Entity\Cita;


class CitasController extends Controller {

    public function citasReporte(Request $request) {
        return view('bpe.campanhas.gerente.citas-reporte');
    }

    public function citasData(Request $request) {
       return Datatables::of(Cita::getAllDataTable($this->user))->make(true);
    }

}
