<?php

namespace App\Http\Controllers\BPE\Campanhas\Herramientas;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entity\Cita;
use App\Entity\Tienda;
use App\Entity\Usuario;
use Jenssegers\Date\Date as Carbon;

class CitasController extends Controller {

    function resumenCallIndex(Request $request){
        $cita = new Cita();
        return view('bpe.campanhas.herramientas.resumen-call')
            ->with('listaCitas', $cita->getAll())
            ->with('statsCall', $cita->getStatsCall())
            ->with('fechas', $cita->fechasStats());
    }

    function resumenCitasIndex(Request $request){
        $cita = new Cita();

        $hoy = Carbon::now()->toDateString();

        $busqueda = [
            'zonal' => $request->get('zonal',null),
            'centro' => $request->get('centro',null),
            'tienda' => $request->get('tienda',null),
            'fechaIni' => $request->get('fechaIni',$hoy),
            'fechaFin' => $request->get('fechaFin',null)
        ];

        $zona = $busqueda['zonal'] == null ? $this->user->getValue('_zona') : $busqueda['zonal'];
        $centro = $busqueda['centro'] == null ? $this->user->getValue('_centro') : $busqueda['centro'];
        $tienda = $busqueda['tienda'] == null ? $this->user->getValue('_tienda') : $busqueda['tienda'];
            
        $ejecutivos = $cita->getResumenCitas($this->user->getValue('_rol'), $busqueda, $zona, $centro, $tienda);

        return view('bpe.campanhas.herramientas.resumen-citas')->with('busqueda', $busqueda)->with('ejecutivos', $ejecutivos)->with('zonales', Tienda::getZonales())->with('centros', Tienda::getCentros($zona))->with('tiendas', Tienda::getTiendas($centro));
    }

}