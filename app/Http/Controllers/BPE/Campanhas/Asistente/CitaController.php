<?php 
namespace App\Http\Controllers\BPE\Campanhas\Asistente;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entity\Leads as Leads;
use App\Entity\Cita as Citas;
use App\Entity\GestionResultado as GestionResultado;
use App\Entity\GestionLead as GestionLead;
use App\Entity\Canal as Canal;
use App\Entity\Banca as Banca;
use App\Entity\Feedback as Feedback;
use App\Entity\AddContacto as AddContacto;
use Validator;

class CitaController extends Controller{

	public function nuevo(Request $request){

        $lead = Leads::getLeadAsistenteComercial($this->user->getValue('_registro'),$request->get('lead', null));
        if ($lead){
            $citasEjecutivo = Citas::getSemanaEjecutivo($lead->EN_REGISTRO);

            return view('bpe.campanhas.asistente.cita-nuevo')
            ->with('lead',$lead)
            ->with('citasEjecutivo',$citasEjecutivo)
            ->with('horarioEjecutivo',Citas::getFormattedSemanaEjecutivo($citasEjecutivo))
            ->with('calendario',Citas::getCalendarioEjecutivoAC())
            ->with('horario',Citas::getHorario())
            ->with('horasDisponibles',Citas::getHorasCitas())
            ->with('resultados',GestionResultado::getBpeResultadosAC())
            ->with('motivos',GestionResultado::getBpeMotivos())
            ->with('telefonos',Leads::formatTelefonos($lead))
            ->with('feedback',Feedback::listar($request->get('lead', null)))
            ->with('contactos',AddContacto::getContactos($request->get('lead', null)));

        }else{
            flash('No se encuentra el cliente/lead seleccionado')->error();
            return redirect()->route('bpe.campanha.asistente.leads.listar');
        }
    }

    public function registrar(Request $request){

        $validator = Validator::make($request->all(), [
            'lead' => 'required',
            'regEjecutivo' => 'required',
            'pcontacto' => 'required|max:100',
            'telefono' => 'required|digits:9',
            'direccion' => 'required|max:255',
            'fecha' => 'required|date_format:Y-m-d',
            'hora' => 'required|date_format:H:i',
        ]);

        if ($validator->fails()) {
            flash(array_flatten($validator->errors()->getMessages()))->error();
            return back()->withInput($request->input());
        }


        $cita = new Citas();
        $cita->setValues([
            '_lead' => $request->get('lead', null),
            '_agendador' => $this->user->getValue('_registro'),
            '_ejecutivo' => $request->get('regEjecutivo', null),
            '_hora' => $request->get('hora', null),
            '_fecha' => $request->get('fecha', null),
            '_contacto' => $request->get('pcontacto', null),
            '_contactoTelefono' => $request->get('telefono', null),
            '_contactoDireccion' => $request->get('direccion', null),
            '_contactoReferencia' => $request->get('referencia', null),
            '_tipo' => Canal::ASISTENTE_COMERCIAL,
            '_autorizacionDatos' => $request->get('autDatos',null)
        ]);

        if($cita->registrar()){
            flash($cita->getMessage())->success();
            return redirect()->route('bpe.campanha.asistente.leads.listar',['ejecutivo' => $cita->getValue('_ejecutivo')]);
        }else{
            flash($cita->getMessage())->error();
            return back()->withInput($request->input());
        }
    }

    public function registrarGestion(Request $request){

        $validator = Validator::make($request->all(), [
            'lead' => 'required',
            'resultado' => 'required',
            'fecha' => 'nullable|date_format:Y-m-d',
            'comentario' => 'nullable|min:10|max:150',
        ]);

         // FALTA FLASH MESSENGER
        if ($validator->fails()) {
            flash(array_flatten($validator->errors()->getMessages()))->error();
            return back()->withInput($request->input());
        }

        
        $gestion = new GestionLead();
        $gestion->setValues([
            '_lead' => $request->get('lead', null),
            '_ejecutivo' => $this->user->getValue('_registro'),
            '_resultado' => $request->get('resultado', null),
            '_motivo' => $request->get('motivo', null),
            '_comentario' => $request->get('comentario', null),
            '_banca' => Banca::BPE,
            '_rolGestion' => Canal::ASISTENTE_COMERCIAL,
            '_fechaVolverLLamar' => $request->get('fecha', null),
        ]);

        if($gestion->registrar()){
            flash($gestion->getMessage())->success();
            return redirect()->route('bpe.campanha.asistente.leads.listar',['ejecutivo' => $request->get('regEjecutivo')]);
        }else{
            flash($gestion->getMessage())->error();
            return back()->withInput($request->input());
        }
    }
}