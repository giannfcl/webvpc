<?php 
namespace App\Http\Controllers\BPE\Campanhas\Asistente;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entity\Leads as Leads;
use App\Entity\Usuario as Usuario;
use App\Entity\LeadCampanha as LeadCampanha;

class LeadsController extends Controller{

	public function listar(Request $request){
		$busqueda = [
			'page' => $request->get('page',1),
			'ejecutivo' => $request->get('ejecutivo',null),
			'lead' => $request->get('lead',null),
			'campanha' => $request->get('campanha',null),
			'documento' => $request->get('documento',null),
		];

		$pagina = Leads::getLeadsAsistenteComercial($this->user->getValue('_registro'),$busqueda)
            	->setPath(config('app.url').'/bpe/ac/leads');

		return view('bpe.campanhas.asistente.leads')
            ->with('leads',$pagina)
            ->with('ejecutivos',Usuario::getEjecutivosByAsistenteComercial($this->user->getValue('_registro')))
            ->with('busqueda',$busqueda)
            ->with('campanhas',LeadCampanha::getCampanhasByAsistente($this->user->getValue('_registro')));
	}
}