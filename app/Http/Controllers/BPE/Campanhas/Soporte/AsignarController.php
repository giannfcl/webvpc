<?php

namespace App\Http\Controllers\BPE\Campanhas\Soporte;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Usuario;
use App\Entity\LeadEjecutivo as LeadEjecutivo;
use App\Entity\Cita as CitasEj;
use App\Entity\Leads as Leads;
use Carbon\Carbon;

class AsignarController extends Controller {

    public function index(Request $request) {

        return view('bpe.campanhas.soporte.asignacion-leads');
    }

    public function consultar(Request $request) {
        $lead = new Leads();
        $data = $lead->getLeadAsignacion($request->get('num_doc', null),$request->get('ejecutivo', null));
        if ($data) {
            return ['status' => 'ok', 'data' => (array) $data];
        } else {
            return ['status' => 'error', 'message' => $lead->getMessage()];
        }
    }

    public function consultarEN(Request $request) {
        $registro_en = $request->get('registro_en', null);
        $ejecutivo = Usuario::getEjecutivo($registro_en);
        return $ejecutivo;
    }

    public function asignar(Request $request) {

        $leadej = new LeadEjecutivo();
        $leadej->setValue('_leads', $request->get('lead', null));
        $leadej->setValue('_ejecutivo', $request->get('en', null));
        if ($leadej->asignacionMasiva($this->user->getValue('_registro'))) {
            flash($leadej->getMessage())->success();
        } else {
            flash($leadej->getMessage())->error();
        }
        return redirect()->route('bpe.campanha.soporte.asignar.index');
    }

}