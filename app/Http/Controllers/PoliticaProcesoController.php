<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Entity\Infinity\PoliticaProceso as Politica;
use App\Entity\Infinity\ConocemeCliente as Conoceme;
use App\Entity\Infinity\Salida as Salida;

class PoliticaProcesoController extends Controller {
    const FLG_POLITICAPROCESO = false;
    
    public function performance() {
        $politicacliente=new Politica;
        $politicacliente->politicaPerformance($politicacliente->getPoliticasClientesInfinity(self::FLG_POLITICAPROCESO),self::FLG_POLITICAPROCESO);
        return '<p>Exito</p>';
    }
    
    public function semaforo() {
        $politicacliente=new Politica;
        $politicacliente->politicaSemaforo($politicacliente->getPoliticasClientesInfinity(self::FLG_POLITICAPROCESO),self::FLG_POLITICAPROCESO);
        //$politicacliente->politicaSemaforo($politicacliente->getPoliticasCliente('0015715032'));
        return '<p>Exito</p>';
    }
    
    public function documentaria() {
        // $politicaEEFF=new Politica;
        // $aa = $politicaEEFF->evaluacionDocumento('2018-12-31','2018-04-20','DDJJ');dd($aa);
        $politicacliente=new Politica;
        $politicacliente->politicaDocumentaria($politicacliente->getPoliticasClientesInfinity(self::FLG_POLITICAPROCESO),self::FLG_POLITICAPROCESO);
        //$politicacliente->politicaDocumentaria($politicacliente->getPoliticasClientesInfinity('0008612845'));
        return '<p>Exito</p>';
    }
    
    public function cualitativaDiaria() {
        $politicacliente=new Politica;

        $politicacliente->actualizarblindaje();

        $politicacliente->actualizarNivelPoliticas();
        // $politicacliente->agregarSemaforoRojoVencidos($politicacliente->getPoliticasClientesInfinity(self::FLG_POLITICAPROCESO));
        $politicacliente->politicaCualitativaDiaria($politicacliente->getPoliticasClientesInfinity(self::FLG_POLITICAPROCESO));
        return '<p>Exito</p>';   
    }

    public function PoliticasVencidas() {
        $politicacliente=new Politica;
        $politicacliente->PoliticasVencidas();
        return '<p>Exito</p>';
    }
}