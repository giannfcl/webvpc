<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables as Datatables;
use Illuminate\Support\Facades\Auth;
use App\Entity\Usuario;
use App\Entity\Reprogramacioncredito;
use Validator;

class ReprogramaciondecreditoController extends Controller
{
	public function index()
	{
		$entidad = new Reprogramacioncredito();
		return view("reprograc.index")
				->with("rol",$this->user->getValue('_rol'))
				->with("estadosrc",$entidad->getEstadosGestionRC());
	}

	public function getListaRP(Request $request)
    {
        return Datatables::of(Reprogramacioncredito::getListaRP($request->all(),$this->user))->make(true);
    }

    public function getrpcbycodcredito(Request $request)
    {
        return response()->json(Reprogramacioncredito::getRP($request->all(),$this->user));
    }

    public function gestionar(Request $request)
    {
    	$entidad = new Reprogramacioncredito();
    	if ($entidad->gestionar($request->all(),$this->user)) {
    		flash("Exito! se guardo la gestión")->success();
    	}else{
    		flash("Hubo un problema vuelva a intentar o contactanos")->error();
    	}
    	return redirect()->route('rp.lista');
    }

    public function editgestionar(Request $request)
    {
        $entidad = new Reprogramacioncredito();
        if ($entidad->editgestionar($request->all(),$this->user->getValue('_registro'))) {
            flash("Exito! se editó la gestión")->success();
        }else{
            flash("Hubo un problema vuelva a intentar o contactanos")->error();
        }
        return redirect()->route('rp.lista');
    }

    public function getEstadosGestionRC()
    {
    	$entidad = new Reprogramacioncredito();
    	return response()->json($entidad->getEstadosGestionRC());
    }
}
?>