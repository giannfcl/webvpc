<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entity\Mundial as Mundial;
use App\Entity\Usuario as Usuario;
use Validator;

class MundialController extends Controller {

    public function principal(Request $request) {

        $area=0;
        if(in_array($this->user->getValue('_rol'),[1,2,6,7,8,10,28]))
            $area=0; //Banca Pequeña Empresa
        else if(in_array($this->user->getValue('_rol'),[20,21,22,23,24,25,26,29,30,28]))
            $area=1; //Banca Empresa y Corporativa
        else if(in_array($this->user->getValue('_rol'),[666,32,11]))
            $area=2; //Inteligencia y sus amiguitos

        return view('mundial-principal')
            ->with('area',$area)           
            ->with('grupos',Mundial::listarPartidosRango($this->user->getValue('_registro'),[1,48]))           
            ->with('octavos',Mundial::listarPartidosRango($this->user->getValue('_registro'),[49,56]))           
            ->with('cuartos',Mundial::listarPartidosRango($this->user->getValue('_registro'),[57,60]))           
            ->with('semis',Mundial::listarPartidosRango($this->user->getValue('_registro'),[61,62]))           
            ->with('podio',Mundial::listarPartidosRango($this->user->getValue('_registro'),[63,64]))           
            ->with('puesto',Mundial::obtenerPuestoPuntaje($this->user->getValue('_registro'),$area))
        	->with('ranking',Mundial::obtenerRankingArea($area));
        
    }    
    
    public function insertarResultados(Request $request){

        $mundial=new Mundial();      
        //dd($request->all());
        if ($request->all()) {
            if ($mundial->insertarResultadoFecha($this->user->getValue('_registro'),$request->all())) {
                flash('Se registraron/actualizaron tus scores de manera correcta')->success();
            } else {
                flash('Hubo un error al registrar tus scores, comunícate con soporte')->error();
            }
        }else{
            flash('Ya no puedes guardar marcaciones para este partido')->error();
        }

        return redirect()->route('mundial.principal');

    }
}