<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entity\Notification as Notification;
use Yajra\Datatables\Datatables as Datatables;

class NotificationController extends Controller
{
    public function index(Request $request){
        $entidad = new Notification();
        $catalogo = $entidad->getCatalogoNotificaciones();
        return view('notification')
                ->with('catalogo',$catalogo);
    }
    public function getNotificationsTOT(Request $request){
        if ($this->user){
            $registro   =  $this->user->getValue('_registro');
            $filtro     =  $request->all();
            return Datatables::of(Notification::getNotificationsTOT($registro,$filtro))->make(true);
        }else{
            return json_encode([]);
        }
    }
    public function getNotifications(Request $request)
    {
        if ($this->user){
            $registro   =  $this->user->getValue('_registro');
            return json_encode(Notification::getNotifications($registro));
        }else{
            return json_encode([]);
        }
        
    }
    public function updateVisto(Request $request)
    {
        $registro   =  $this->user->getValue('_registro');
        return json_encode(Notification::updateVisto($registro));
    }
    public function updateVistoURL(Request $request)
    {
        $datos = $request->all();
        $registro   =  $this->user->getValue('_registro');
        return json_encode(Notification::updateVistoURL($registro, $datos['id']));
    }
}
