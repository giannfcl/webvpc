<?php 

namespace App\Http\Controllers;

class View360Controller extends Controller {

    public function index() {
        $banca = $this->user->getValue('_banca');
        $url = ($banca == 'BC'? 'https://app.powerbi.com/view?r=eyJrIjoiZDk2N2Y3MzctMTc4Zi00ZTIzLWI5ZDAtN2M1YmZkZWVlODJjIiwidCI6ImU1YTZlNDRlLWE1NzctNDM0OC1hOGUxLTdhMGYwNDMxNzU4NiIsImMiOjR9&pageName=ReportSection3e7408618ed4e0a0cc47' : 'https://app.powerbi.com/view?r=eyJrIjoiZmE3ZWM5N2QtZGVlMC00ZDM1LWI1M2QtMzBjODkyMjkwNTRmIiwidCI6ImU1YTZlNDRlLWE1NzctNDM0OC1hOGUxLTdhMGYwNDMxNzU4NiIsImMiOjR9&pageName=ReportSection');
        return view('view360')
        ->with('url',$url);
    }
}