<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Entity\Telefono;
use Illuminate\Http\Request;

class BusquedaController extends Controller {

    public function index(Request $request) {
        $variables = [
            'tipoBusqueda' => $request->get('tipoBusqueda', null),
            'termino' => $request->get('termino', null),
            'page' => $request->get('page', 1)
        ];
        $telefonos = Telefono::buscar(
                $variables['tipoBusqueda'] == 1 ? $variables['termino'] : '',
                $variables['tipoBusqueda'] == 2 ? $variables['termino'] : '',
                $variables['page']);
        
        return view('busqueda')
                ->with('telefonos',$telefonos->withPath('/busqueda'))
                ->with('termino',$variables['termino'])
                ->with('tipoBusqueda',$variables['tipoBusqueda'])
                ->with('page',$variables['page']);
        
    }
}