<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entity\Usuario as Usuario;
use App\Entity\EncuestaInterna as Encuesta;
use Validator;

class EncuestaInternaController extends Controller {

    public function principal(Request $request) {
     
            return view('encuestasInternas')
            ->with('analistas',Encuesta::getAnalistasUsuarioData($this->user->getValue('_registro')))
            //->with('preguntasGerencia',Encuesta::getPreguntaGerencia($this->user->getValue('_registro')))
            ->with('preguntas',Encuesta::getPreguntas());
    }    
    
    
    public function resumen(Request $request){
        return view('encuestasResumen')
            ->with('puntajePromedio',Encuesta::getPromedioGeneral())        
            ->with('avance',Encuesta::getAvance())        
            ->with('resumen',Encuesta::getResumen());        
    }

    public function guardarResultados(Request $request){

        //$puntajeGerencia=$request->get('estrellasGerencia',null);
        // dd($request->all());
        $puntajeGerencia=null;
        $preguntas=[];
        $vacio=false;
        if ($request->get('checkAnalista')!=NULL){
            foreach ($request->get('checkAnalista') as $check){
                $preguntas[$check]['registro']=$check;
                for ($i=1; $i <=3 ; $i++) {
                    $preguntas[$check][$i]=$request->get('estrellas')[$i.'-'.$check];
                }
                $preguntas[$check]['sugerencia']=$request->get('sugerencia')[$check];
            }
        }
        else{
            $vacio=true;
        }

        $registroEvaluador=$this->user->getValue('_registro');

        $encuesta=new Encuesta();

        if($encuesta->guardarResultados($registroEvaluador,$preguntas,$vacio,$puntajeGerencia)){
            flash('La encuesta se registró correctamente.')->success();
        }
        else{
            flash('No se puedo guardar la encuesta, intente nuevamente.')->error();
        }
        return back();
    }

}