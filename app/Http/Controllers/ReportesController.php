<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entity\Reportes as Reportes;

class ReportesController extends Controller
{

    public function index()
    {
        $reportes = Reportes::Reportes();
        $ROL =  $this->user->getValue('_rol');
        $EDITAR = Reportes::getInfoEditor($this->user->getValue('_registro'));

        return view('Reportes')
            ->with('reportes', $reportes["REPORTES"])
            ->with('niveles', $reportes["NIVEL"])
            ->with('categorias', $reportes["CATEGORIAS"])
            ->with("factores", $reportes["FACTOR"])
            ->with("EDITAR", $EDITAR);
    }
    public function actualizar(Request $request)
    {
        // dd($request->all());
        $datos = $request->all();
        return response()->json(Reportes::actualizar($datos) ? true : false);
    }
    public function datosdescarga(Request $request)
    {
        $datos = $request->all();
        $registro =  $this->user->getValue('_registro');
        $nombre =  $this->user->getValue('_nombre');
        $entidad = new  Reportes();
        return response()->json($entidad->guardardatosdescarga($datos,$registro,$nombre) ? 'Exito!' : 'Error');
    }
}
