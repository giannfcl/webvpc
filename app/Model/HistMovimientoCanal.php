<?php
namespace App\Model;
use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class HistMovimientoCanal extends Model

{
	protected $table = 'WEBVPC_HIST_MOVIMIENTO_CANAL';
    
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey =['PERIODO',
                            'NUM_DOCUMENTO',               
                            'FECHA_ACTUALIZACION',  
        					];
					        

        /**
     * The name of the "created at" column.
     *
     * @var string
     */
     public $timestamps = false;
    
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'CANAL_ORIGEN',
        'CANAL_DESTINO',
        ];

}