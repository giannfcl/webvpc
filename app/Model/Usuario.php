<?php
namespace App\Model;
use DB;
use Log;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Jenssegers\Date\Date as Carbon;


class Usuario extends Authenticatable{
	protected $table = 'WEBVPC_USUARIO';
    
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey ='ID';


        /**
     * The name of the "created at" column.
     *
     * @var string
     */
    public $timestamps = false;
    
    
    protected $fillable = [
        'REGISTRO',
        'PASSWORD',
        'NOMBRE',
        'TOKEN'
        ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'ID',
        'REGISTRO',
        'PASSWORD',
        'NOMBRE',
        'TOKEN'
        ];


    //protected $username = 'REGISTRO';


    public function getAuthPassword () {
        return $this->PASSWORD;
    }


        public function getRememberToken()
    {
        if (! empty($this->TOKEN)) {
            return $this->TOKEN;
        }
    }

    /**
     * Set the token value for the "remember me" session.
     *
     * @param  string  $value
     * @return void
     */
    public function setRememberToken($value)
    {
        if (! empty($this->TOKEN)) {
            $this->TOKEN = $value;
        }
    }

    public function hasRole($rol){
        return in_array($rol, [$this->ROL]);
    }

    public function getEjecutivoByAsistenteComercial($ac,$ejecutivo = []){
        $sql = DB::table('WEBVPC_AC_EN_RELACION as WAE')
                ->select('WU.REGISTRO', 'WU.NOMBRE')
                ->join('WEBVPC_USUARIO_ANT as WU','WU.REGISTRO','=','WAE.EJECUTIVO_NEGOCIO')
                ->where('WAE.ASISTENTE_COMERCIAL','=',$ac)
                ->where('WU.FLAG_ACTIVO','=','1');
        if ($ejecutivo){
            $sql = $sql->whereIn('WAE.EJECUTIVO_NEGOCIO',$ejecutivo);
        }
        return $sql;
    }
    static function getCargo($registro){
        $sql = DB::table('WEBVPC_USUARIO_ANT')->select('CARGO')->where('REGISTRO','=',$registro)->get()->first();
        return $sql->CARGO;
    }
    static function getEjecutivo($registro_en=[]) {
        $sql = DB::table('WEBVPC_USUARIO_ANT AS WU')
               ->select('WU.REGISTRO', 'WU.NOMBRE')
               ->where('WU.FLAG_ACTIVO','=',1);
        if ($registro_en){
            return $sql->where('WU.REGISTRO', 'LIKE', $registro_en . '%')->get();
        } else {
            return $sql->get();
        }
    }

    static function getEjecutivosProductoByZonal($zonal) {
        //AQUÝ        
        $sql = DB::table('WEBVPC_USUARIO_ANT AS WU')
               ->select('WU.REGISTRO', 'WU.NOMBRE')
               ->where('WU.ID_ZONA', 'like','%'.$zonal.'%')
               ->where('WU.ROL','=',31)
               ->where('WU.FLAG_ACTIVO','=','1');
               
        if($zonal==NULL){
            $sql = DB::table('WEBVPC_USUARIO_ANT AS WU')
               ->select('WU.REGISTRO', 'WU.NOMBRE')
               ->where('WU.ROL','=',31)
               ->where('WU.FLAG_ACTIVO','=','1');
        }
        
        if($zonal=="BEPZONAL1" or $zonal=="BEPZONAL2"){
            $sql = DB::table('WEBVPC_USUARIO_ANT AS WU')
               ->select('WU.REGISTRO', 'WU.NOMBRE')
               ->where('WU.ROL','=',31)
               ->where('WU.ID_ZONA', 'like','%BEP%')
               ->where('WU.FLAG_ACTIVO','=','1');
        }

        if($zonal=="BCGRUPO1" or $zonal=="BCGRUPO2"){
            $sql = DB::table('WEBVPC_USUARIO_ANT AS WU')
               ->select('WU.REGISTRO', 'WU.NOMBRE')
               ->where('WU.ROL','=',31)
               ->where('WU.ID_ZONA', 'like','%BC%')
               ->where('WU.FLAG_ACTIVO','=','1');
        }

        return $sql->get();
    }

    static function getEjecutivoByRegistro($registro,$rol = null) {

        $sql = DB::table('WEBVPC_USUARIO_ANT AS WU')
               ->select('WU.REGISTRO', 'WU.NOMBRE','WU.ROL','WU.ID','WU.BANCA')
               ->where('WU.REGISTRO', '=', $registro)
               ->where('WU.FLAG_ACTIVO','=','1');

        if ($rol){
            $sql = $sql->where('WU.ROL', '=', $rol);
        }
        
        return $sql->first();
    }

    static function getEjecutivoByRol($roles, $banca=null, $zonal = null,$centro = null){

        //AQUÝ
        $sql = DB::table('WEBVPC_USUARIO_ANT AS WU')
               ->select('WU.REGISTRO', 'WU.NOMBRE')
               ->whereIn('WU.ROL', $roles)
               ->where('WU.NOMBRE','<>','DUMMIE')
               ->where('WU.FLAG_ACTIVO','=','1');
        
        if ($banca){
            $sql=$sql->where('WU.BANCA','=',$banca);
        }       
        if ($zonal){
            $sql = $sql->where('WU.ID_ZONA', '=', $zonal);
        }

        if ($centro){
            $sql = $sql->where('WU.ID_CENTRO', '=', $centro);
        }
        
        if($zonal=="BEP"){
            $sql=DB::table('WEBVPC_USUARIO_ANT AS WU')
               ->select('WU.REGISTRO', 'WU.NOMBRE')
               ->whereIn('WU.ROL', $roles)
               ->where('WU.NOMBRE','<>','DUMMIE')
               ->where('WU.ID_ZONA','BEPZONAL1')
               ->orWhere('WU.ID_ZONA','BEPZONAL2')
               ->where('WU.FLAG_ACTIVO','=','1');
        }

        if($zonal=="BC"){
            $sql=DB::table('WEBVPC_USUARIO_ANT AS WU')
               ->select('WU.REGISTRO', 'WU.NOMBRE')
               ->whereIn('WU.ROL', $roles)
               ->where('WU.NOMBRE','<>','DUMMIE')
               ->where('WU.ID_ZONA','BCGRUPO1')
               ->orWhere('WU.ID_ZONA','BCGRUPO2')
               ->where('WU.FLAG_ACTIVO','=','1');
        }
        return $sql;
    }    

    function updateMasive(){
        DB::beginTransaction();
        $status = true;
        try {
            $usuarios = DB::table('TMP_GCL_USUARIOS AS WU')->select('WU.REGISTRO','WU.PASS')->get();
			//dd($usuarios);
			foreach ($usuarios as $usuario) {
				DB::table('WEBVPC_USUARIO_ANT')
				->where('REGISTRO','=',$usuario->REGISTRO)
				->update(['PASS' => Hash::make($usuario->PASS)]);
			}
			DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }

        return $status;
    }
    
    static function updatePassword($usuario,$password){
        return DB::table('WEBVPC_USUARIO_ANT')
             ->where('REGISTRO','=',$usuario)
            ->update(['PASS' => Hash::make($password)]);
    }

    public function updateNewPassword($usuario,$password){
        return DB::table('WEBVPC_USUARIO_ANT')
             ->where('REGISTRO','=',$usuario)
            ->update(['PASS' => Hash::make($password)]);
    }
    
    public function verifyPassword($usuario,$apassword){
        $sql = DB::table('WEBVPC_USUARIO_ANT AS WU')
               ->where('WU.REGISTRO', '=', $usuario)
               ->where('WU.FLAG_ACTIVO','=','1');
         
        //El usuario encontrado es la primera coincidencia (usuario es unico)     
        $usuario = $sql->first();
        //La función Hash::check() se encarga de confirmar si dos cadenas encriptadas son iguales
        return Hash::check($apassword, $usuario->PASS);
    }

    static function getUsuario($registro=null){
      $sql = DB::table('WEBVPC_USUARIO_ANT AS WU')
               ->select("WU.*","WR.NOMBRE as NOMBREROL")
               ->leftjoin('WEBVPC_ROL as WR',function($join){ 
                    $join->on('WU.ROL','=','WR.ID_ROL');
                });
        if ($registro) {
          if (is_array($registro)) {
            $sql=$sql->whereIn('registro',$registro)->get();
          }else{
            $sql=$sql->where('registro','=',$registro)->get()->first();
          }
        }else{
          $sql=$sql->get();
        }
        return $sql;
    }    

    static function getUsuariosComunicacion(){
      $sql = DB::table('ARBOL_DIARIO_2 AS ARB')
               ->select('ARB.LOGIN AS REGISTRO')
               ->where('ARB.BANCA','=','BEL')
               ->where('ARB.LOGIN','<>','')
               ->distinct();
        return $sql;
    }

    static function getEncuestadores(){
      $sql = DB::table('WEBVPC_ENCUESTADORES AS WE')
               ->select();
        return $sql;
    }
    

    static function getEjecutivoCorreo($registro){
        $sql = DB::table('WEBVPC_CORREOS_BEL')
               ->select('NOMBRE_EN','CORREO AS EMAIL_EN')
                ->where('REGISTRO',$registro);
        return $sql;
    }

    static function getJefe($registro){
        $sql = DB::table('WEBVPC_CORREOS_BEL')
               ->select('NOMBRE_JEFATURA','EMAIL_JEFATURA_ZONAL')
                ->where('REGISTRO',$registro);
        return $sql;
    }

    static function getGerente($registro){
        $sql = DB::table('WEBVPC_CORREOS_BEL')
               ->select('NOMBRE_GERENCIA','EMAIL_GERENCIA_ZONAL')
                ->where('REGISTRO',$registro);
        return $sql;

    }

    static function getGerenteDivisionBE(){
        $sql = DB::table('WEBVPC_USUARIO_ANT')
               ->select('NOMBRE','ROL')
                ->where('ROL','=','30')
                ->where('AREA','=','BANCA EMPRESA')
                ->where('FLAG_ACTIVO','=','1')->first();
        return $sql;
    }
    static function getGerenteZonalBE(){
        $sql = DB::table('WEBVPC_USUARIO_ANT')
               ->select('NOMBRE','ROL')
                ->where('ROL','=','25')
                ->where('CARGO','=','GERENTE ZONAL / JEFE DE GRUPO')
                ->where('ID_ZONA','=','BELZONAL2')
                ->where('FLAG_ACTIVO','=','1')->first();
        return $sql;
    }

    //Aún no se define si será solo él
    static function getAnalistaRiesgoBE(){
        $sql = DB::table('WEBVPC_USUARIO_ANT')
               ->select('NOMBRE','ROL')
                ->where('ROL','=','29')
                ->where('DNI','=','45281531')
                ->where('FLAG_ACTIVO','=','1')->first();
        return $sql;
    }

    static function getUsuarioInfinity(){
        $sql = DB::table('WEBVPC_ACCESOS_VISTAS')
                    ->SELECT('REGISTRO')
                    ->where("NOMBRE_VISTA","=","ALERTAS_CARTERA")
                    ->where("FLG_ACTIVO","=",DB::Raw('1'))
                    ->get();
        return $sql;
    }


    static function getJefeEjecutivo($registro)
    {
      $sql = DB::table('WEBVPC_USUARIO_ANT')
               ->select('ID_CENTRO','NOMBRE','ROL')
               ->where('REGISTRO','=',$registro)
               ->first();
      if (!empty($sql->ID_CENTRO)) {
        $jefe=DB::table('WEBVPC_USUARIO_ANT')
              ->where('ID_CENTRO','=',$sql->ID_CENTRO)
              ->where('ROL','=','24')
              ->where('FLAG_ACTIVO','=','1')
              ->get()
              ->first();
      }
      return isset($jefe) ? $jefe->CORREO : null;
    }

    static function getNombreByRegistro($registro){
        DB::beginTransaction();
        $nombre = null;
        if($registro!=null){
            try {
                $nombre = DB::table('WEBVPC_USUARIO_ANT') 
                ->where('REGISTRO','=',$registro)
                ->select('NOMBRE')->first();
            }catch(\Exception $e){
                Log::error('BASE_DE_DATOS|' . $e->getMessage());
                DB::rollback();
            }
        }
        return $nombre;
    }
    static function UpdateLogeo($usuario)
    {
      DB::beginTransaction();
        $status = true;
        try {
            DB::table('WEBVPC_USUARIO_ANT') 
            ->where('REGISTRO','=',$usuario)
            ->update(['FECHA_ULTIMO_LOGEO' => Carbon::now()]);

            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }

        return $status;
    }

    static function getAccesoVista($nombrevista)
    {
        $sql = DB::table('WEBVPC_ACCESOS_VISTAS')
                        ->SELECT('REGISTRO')
                        ->where('NOMBRE_VISTA','=',$nombrevista)
                        ->where('FLG_ACTIVO','=',DB::Raw('1'));
        return $sql;
    }

    function insertUsuario($data)
    {
      DB::beginTransaction();
        $status = true;
        try {
            DB::table('WEBVPC_USUARIO_ANT')->insert($data);

            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }

        return $status;
    }

    function updatetUsuario($data,$registro)
    {
      DB::beginTransaction();
        $status = true;
        try {
            DB::table('WEBVPC_USUARIO_ANT')
                ->where('REGISTRO','=',$registro)
                ->update($data);

            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }

        return $status;
    }

    function getRoles($idrol=null)
    {
      $sql = DB::table('WEBVPC_ROL');
        if ($idrol) {
          if (is_array($idrol)) {
            $sql=$sql->whereIn('ROL',$idrol)->get();
          }else{
            $sql=$sql->where('ROL','=',$idrol)->get()->first();
          }
        }else{
          $sql=$sql->get();
        }
        return $sql;
    }

    function guardarvxu($data)
    {
      DB::beginTransaction();
        $status = true;
        try {
            DB::table('WEBVPC_VISTAS_USUARIO')->insert($data);

            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }

        return $status;
    }

}