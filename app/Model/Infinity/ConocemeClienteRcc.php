<?php

namespace App\Model\Infinity;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class ConocemeClienteRcc extends Model {

    function get($periodo, $codunico) {
        $sql = DB::Table('WEBBE_INFINITY_CLIENTE_RCC')
                ->select('PERIODO','COD_UNICO','PERIODO_RCC','BANCO','TIPO','MONTO')
                ->where('PERIODO', $periodo)
                ->where('COD_UNICO', $codunico);
        return $sql;
    }
}