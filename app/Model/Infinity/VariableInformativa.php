<?php

namespace App\Model\Infinity;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class VariableInformativa extends Model {

    function get($codunico, $periodos = [],$soloNegativas = true) {
        $sql = DB::Table('WEBBE_INFINITY_VARIABLES AS WIV')
                ->select('WIV.PERIODO'
                    ,'WIC.COD_UNICO'
                    ,'WIV.VALOR_2'
                    ,'WIV.VALOR'
                    // ,'WIVC.DESCRIPCION'
                    ,DB::Raw("(CASE WHEN ([WIVC].[flg_positivo] = '3' and ISNUMERIC(WIV.valor)=0) THEN [WIVC].[DESCRIPCION] +': ' +[WIV].[VALOR] ELSE [WIVC].[DESCRIPCION] END) as DESCRIPCION")
                    ,'WIVC.GRUPO'
                    // ,'WIVC.FLG_POSITIVO'
                    ,DB::Raw("(
                        CASE WHEN ([WIVC].[flg_positivo] = '1' and case when isnumeric(WIV.valor)=1 then WIV.valor else '0.0'end > convert(numeric,WIVC.valor)) THEN 1 
                        WHEN ([WIVC].[flg_positivo] = '0' and case when isnumeric(WIV.valor)=1 then WIV.valor else '0.0'end < convert(numeric,WIVC.valor)) THEN 1
                        WHEN ([WIVC].[flg_positivo] = '2' and case when isnumeric(WIV.valor)=1 then WIV.valor else '0.0'end = convert(numeric,WIVC.valor)) THEN 1
                        WHEN ([WIVC].[flg_positivo] = '3' and ISNUMERIC(WIV.valor)=0) THEN 1
                        ELSE 0 END ) as FLG_POSITIVO")
                    ,'WIVC.TIPO_DATO')
                ->join('WEBBE_INFINITY_VARIABLES_CATALOGO AS WIVC', function($join) {
                    $join->on('WIV.ID_VARIABLE', '=', 'WIVC.ID_VARIABLE');
                })
                ->join('WEBBE_INFINITY_CLIENTE AS WIC', function($join) {
                    $join->on('WIV.COD_UNICO', '=', 'WIC.COD_UNICO');
                    $join->on('WIV.PERIODO', '=', 'WIC.PERIODO');
                })
                ->join('WEBBE_INFINITY_ALERTA AS WIA', function($join) {
                    $join->on('WIC.COD_UNICO', '=', 'WIA.COD_UNICO');
                    $join->on('WIC.PERIODO', '=', 'WIA.PERIODO');
                })
                ->where('WIVC.SITUACION_VARIABLE',DB::Raw("'VIGENTE'"))
                ->where(DB::Raw("(
                        CASE WHEN ([WIVC].[flg_positivo] = '1' and case when isnumeric(WIV.valor)=1 then WIV.valor else '0.0'end > convert(numeric,WIVC.valor)) THEN 1 
                        WHEN ([WIVC].[flg_positivo] = '0' and case when isnumeric(WIV.valor)=1 then WIV.valor else '0.0'end < convert(numeric,WIVC.valor)) THEN 1
                        WHEN ([WIVC].[flg_positivo] = '2' and case when isnumeric(WIV.valor)=1 then WIV.valor else '0.0'end = convert(numeric,WIVC.valor)) THEN 1
                        WHEN ([WIVC].[flg_positivo] = '3' and ISNUMERIC(WIV.valor)=0) THEN 1
                        ELSE 0 END ) "),"=","1");
        $sql=$sql->whereIn('WIA.NIVEL_ALERTA',array('3','33'));
        if ($codunico) {
            $sql = $sql->where('WIV.COD_UNICO', $codunico);
        }
        if ($periodos) {
            $sql = $sql->whereIn('WIA.PERIODO', $periodos);
        }
        // dd($sql->toSQL());
        return $sql;
    }
}