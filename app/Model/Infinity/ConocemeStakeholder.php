<?php

namespace App\Model\Infinity;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class ConocemeStakeholder extends Model {

    function get($codunico) {
        $sql = DB::Table('WEBBE_INFINITY_CONOCEME_STAKEHOLDER AS WICS')
                ->select('WICS.COD_UNICO', 'TIPO', 'DOCUMENTO', 'NOMBRE', 'CONCENTRACION', 'DESDE', 'EXCLUSIVIDAD', 'CONTRATO_FECHA_VENCIMIENTO', 'CONTRATO_ADJUNTO', 'NACIMIENTO','FLAG_CONTRATO')
                ->where('WICS.COD_UNICO', $codunico)
                ->orderBy('CONCENTRACION', 'desc');
        return $sql;
    }

}
