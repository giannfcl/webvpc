<?php
namespace App\Model\Infinity;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class ConocemeCommoditie extends Model{

    function get($codunico){
	    $sql=DB::Table('WEBBE_INFINITY_CONOCEME_COMMODITIE AS WIL')
	        ->select('COD_UNICO','NOMBRE')
	        ->where('WIL.COD_UNICO',$codunico);
	    return $sql;
    }
}