<?php

namespace App\Model\Infinity;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class ConocemeInversiones extends Model {

    function get($codunico) {
        $sql = DB::Table('WEBBE_INFINITY_INVERSIONES AS WII')
                ->select('COD_UNICO', 'TIPO_INVERSION')
                ->where('WII.COD_UNICO', $codunico);
        return $sql;
    }

}
