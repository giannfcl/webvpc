<?php

namespace App\Model\Infinity;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class AlertaExplicacion extends Model {

    //protected $table = 'WEBVPC_CLIENTE';
    //vista lista alertas

    function get($codunico, $periodos = []) {
        $sql = DB::Table('WEBBE_INFINITY_CLIENTE_POLITICA AS WICP')
                ->join('WEBBE_INFINITY_POLITICA AS WIP',function($join){
                    $join->on('WIP.POLITICA_ID','=','WICP.POLITICA_ID');
                })
                ->select('WIP.NOMBRE');
        if ($codunico) {
            $sql = $sql->where('WICP.COD_UNICO', $codunico);
        }
        return $sql;
    }

    function getCualitativas($codUnico, $periodos = []) {
        $sql = DB::Table('WEBBE_INFINITY_VARIABLES AS WIV')
                ->select('WIV.*', 'WIVC.DESCRIPCION'
                )
                ->join('WEBBE_INFINITY_VARIABLES_CATALOGO AS WIVC', function($join) {
                    $join->on('WIVC.ID_VARIABLE', '=', 'WIV.ID_VARIABLE');
                })
                ->where('WIV.COD_UNICO', '=', $codUnico)
                ->where('WIVC.TIPO', '=', 'CUALITATIVA')
                ->orderBy('WIV.ID_VARIABLE', 'ASC');

        if ($periodos) {
            $sql = $sql->whereIn('WIV.PERIODO', $periodos);
        }

        return $sql;
    }

    function getPerformance($codUnico, $periodos = []) {
        $sql = DB::Table('WEBBE_INFINITY_VARIABLES AS WIV')
                ->select('WIV.*', 'WIVC.DESCRIPCION'
                )
                ->join('WEBBE_INFINITY_VARIABLES_CATALOGO AS WIVC', function($join) {
                    $join->on('WIVC.ID_VARIABLE', '=', 'WIV.ID_VARIABLE');
                })
                ->where('WIV.COD_UNICO', '=', $codUnico)
                ->where('WIVC.TIPO', '=', 'PERFORMANCE')
                ->orderBy('WIV.ID_VARIABLE', 'ASC');

        if ($periodos) {
            $sql = $sql->whereIn('WIV.PERIODO', $periodos);
        }

        return $sql;
    }

    function getSalidas($codUnico, $periodos = []) {
        $sql = DB::Table('WEBBE_INFINITY_SALIDAS')
                ->select()
                ->where('COD_UNICO', '=', $codUnico);
        return $sql;
    }

}
