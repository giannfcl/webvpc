<?php

namespace App\Model\Infinity;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use App\Entity\Infinity\Gestion as Gestion;
use App\Entity\Infinity\PoliticaProceso as Politica;
use App\Model\TestPolitica as TestPolitica;
use Jenssegers\Date\Date as Carbon;


class Cliente extends Model {

    //protected $table = 'WEBVPC_CLIENTE';
    //Estado que solo pueden ver los jefes
    const ESTADO_REVISADO = 1;
    const DESAPROBADO = 0;
    const ESTADO_COMITE = 4;
    const TIPO_RECALCULO = 5;
    const TODOS = array('0','1','2');

    function getListaClientesAlerta($periodo, $codunico = null, $zonal = null, $jefatura = null, $ejecutivo = null,$infinity = 1,$filtros=null) {
        $periodo_sql = DB::Table(DB::raw("(SELECT MAX(PERIODO) AS PERIODO FROM WEBBE_INFINITY_CLIENTE) AS ABC "))->first();
        // dd($filtros);
        //dd($periodo_sql);
        $sql = DB::Table(DB::raw(
            "(             
                SELECT M.PERIODO,
                M.COD_UNICO,
                A.NUM_DOC,
                M.NOMBRE,
                ISNULL(A.BANCA,'BE') BANCA,
                M.COD_SECTORISTA,
                M.SALDO_INTERBANK,
                A.SALDO_RCC,
                ISNULL(A.RATING,VPC.RATING) RATING,
                A.FLG_INFINITY,
                A.CLASIFICACION,
                ISNULL(M.FEVE,VPC.FEVE) FEVE,
                A.SALDO_VENCIDO_IBK,
                A.SALDO_VENCIDO_RCC,
                A.METODIZADO_1_FECHA,
                A.METODIZADO_2_FECHA,
                A.FECHA_ULTIMA_VISITA,
                A.FECHA_ULTIMO_IBR,
                A.FECHA_ULTIMO_F02,
                A.GARANTIA,
                A.FECHA_ULTIMO_RECALCULO,
                A.VENTAS,
                M.MAX_DIAS_ATRASO,
                A.PROVINCIA,
                A.DISTRITO,
                A.FECHA_VENCIMIENTO_POLITICA,
                A.FLG_VISIBLE,
                M.RMG,
                A.FECHA_INGRESO_INFINITY,
                CASE WHEN A.SEGMENTO IS NULL THEN 
                CASE WHEN ARB.BANCA = 'BEL' AND ARB.ZONAL = 'BEL ZONAL 2' THEN 'MEDIANA EMPRESA'  
                    WHEN ARB.BANCA = 'BEL' AND ARB.ZONAL IN ('BEL ZONAL 3','BEL ZONAL 1') THEN 'GRAN EMPRESA' 
                    WHEN ARB.BANCA = 'BEP' THEN 'PROVINCIA'
                    WHEN ARB.BANCA = 'BC' THEN 'CORPORATIVA' END ELSE A.SEGMENTO END SEGMENTO,
                A.FECHA_CARGA,
                A.FLG_POLITICAPROCESO
                FROM (
                SELECT ISNULL(T1.PERIODO,'".($periodo_sql->PERIODO)."') PERIODO, ISNULL(T1.COD_UNICO,T2.COD_UNICO) COD_UNICO,
                    ISNULL(T1.NOMBRE,T2.NOMBRE) NOMBRE,ISNULL(T1.RMG,T2.RMG) RMG,
                    ISNULL(T1.FEVE,T2.FEVE) FEVE,
                    ISNULL(T1.MAX_DIAS_ATRASO,T2.MAX_DIAS_ATRASO) MAX_DIAS_ATRASO,
                    ISNULL(T1.COD_SECTORISTA,T2.CODSECTORISTA) COD_SECTORISTA,
                    ISNULL(T1.SALDO_INTERBANK,T2.SALDO_TOTAL) SALDO_INTERBANK
                FROM
                (SELECT DISTINCT PERIODO,COD_UNICO,FEVE,MAX_DIAS_ATRASO,RMG,NOMBRE,COD_SECTORISTA,SALDO_INTERBANK FROM WEBBE_INFINITY_CLIENTE WHERE BANCA = 'BE') T1
                FULL OUTER JOIN
                (SELECT DISTINCT COD_UNICO,FEVE,RMG,NOMBRE,MAX_DIAS_ATRASO,CODSECTORISTA,SALDO_TOTAL FROM WEBBE_INFINITY_INFO_DIARIA_CLI) T2
                ON T1.COD_UNICO = T2.COD_UNICO
                )M LEFT JOIN WEBBE_INFINITY_CLIENTE A ON M.COD_UNICO = A.COD_UNICO AND M.PERIODO = A.PERIODO
                LEFT JOIN ARBOL_DIARIO_2 ARB ON M.COD_SECTORISTA = ARB.COD_SECT_LARGO 
                LEFT JOIN WEBVPC_CLIENTES_VPC VPC ON M.COD_UNICO = VPC.COD_UNICO 
                WHERE LEFT(M.COD_SECTORISTA,3) != '070'
            ) WIC"
         ))
                ->leftJoin('WEBBE_INFINITY_ALERTA AS WIA', function($join) {
                    $join->on('WIC.COD_UNICO', '=', 'WIA.COD_UNICO');
                    $join->on('WIC.PERIODO', '=', 'WIA.PERIODO');
                })
                ->leftJoin('ARBOL_DIARIO_2 AS WLS', function($join) {
                    $join->on('WLS.COD_SECT_LARGO', '=', 'WIC.COD_SECTORISTA');
                })
                ->leftJoin('WEBVPC_USUARIO AS WU', 'WU.REGISTRO', '=', 'WLS.LOGIN')
                ->leftjoin('WEBBE_ZONAL AS WZ', 'WZ.ID_ZONAL', '=', 'WU.ID_ZONA')
                ->leftjoin('WEBBE_JEFATURA AS WJ', 'WJ.ID_JEFATURA', '=', 'WU.ID_CENTRO')
                ->leftjoin(DB::raw("
                (SELECT * FROM 
                    (SELECT A.*,B.POLITICA_ID,ROW_NUMBER() OVER (PARTITION BY A.COD_UNICO ORDER BY FECHA_SALIDA DESC) MAXIMO,CASE WHEN FECHA_SALIDA+45>GETDATE() THEN 1 ELSE 0 END PERMITIDOS 
                    FROM WEBBE_INFINITY_SALIDAS A 
                    LEFT JOIN WEBBE_INFINITY_CLIENTE_POLITICA B ON A.COD_UNICO=B.COD_UNICO AND B.ID_POLITICA_CLIENTE=A.POLITICA_CLIENTE_ID 
                    INNER JOIN WEBBE_INFINITY_POLITICA C ON B.POLITICA_ID=C.POLITICA_ID AND C.TIPO_GESTION='Con observación comité' AND C.ORIGEN='Cualitativa' AND A.TIPO_SALIDA IS NULL AND A.FECHA_REINGRESO IS NULL) A 
                WHERE A.MAXIMO=1) AS WIS"),function($join)
                {
                    $join->on('WIC.COD_UNICO', '=', 'WIS.COD_UNICO');
                    $join->on('WIS.MAXIMO','=',DB::raw('1'));
                })
                ->leftJoin(DB::Raw("(SELECT A.* FROM WEBBE_INFINITY_CLIENTE_POLITICA A INNER JOIN WEBBE_INFINITY_POLITICA B ON A.POLITICA_ID=B.POLITICA_ID AND B.POLITICA_ID='23' AND FLG_VIGENTE='1' ) AS WISRV"),function($join)
                {
                    $join->on('WIC.COD_UNICO', '=', 'WISRV.COD_UNICO');
                })
                ->leftjoin(DB::raw("(SELECT COD_UNICO,COUNT(1) CANT_POLI_CUALI FROM (SELECT WICP.* FROM WEBBE_INFINITY_CLIENTE_POLITICA WICP INNER JOIN ( SELECT * FROM WEBBE_INFINITY_POLITICA WHERE ORIGEN='Cualitativa' ) WIP ON WICP.POLITICA_ID=WIP.POLITICA_ID AND FLG_VIGENTE=1) A GROUP BY  COD_UNICO) AS WIVC"), function($join) {
                    $join->on('WIVC.COD_UNICO','=','WIC.COD_UNICO');
                })
                ->leftJoin(DB::raw("(SELECT DISTINCT A.COD_UNICO FROM WEBBE_INFINITY_CLIENTE_POLITICA A INNER JOIN WEBBE_INFINITY_POLITICA B ON A.POLITICA_ID=B.POLITICA_ID AND B.DIAS_GESTION>0 AND A.FLG_VIGENTE=1) AS WIG"),function($join) {
                    $join->on('WIC.COD_UNICO','=','WIG.COD_UNICO');
                    $join->on('WIC.FLG_INFINITY','>=',DB::raw('1'));
                })
                ->leftJoin(DB::raw("(SELECT COD_UNICO,COUNT(1) CANT_GESTIONES_PENDIENTES FROM WEBBE_INFINITY_CLIENTE_POLITICA WHERE FLG_VIGENTE=1 AND DIAS_GESTION>0 GROUP BY COD_UNICO) AS WIGP"),function($join) {
                    $join->on('WIC.COD_UNICO','=','WIGP.COD_UNICO');
                })
                ->leftJoin(DB::raw("(SELECT COD_UNICO,COUNT(1) CANT_GESTIONES_COMITE_PENDIENTES FROM (SELECT WICP.* FROM WEBBE_INFINITY_CLIENTE_POLITICA WICP INNER JOIN ( SELECT * FROM WEBBE_INFINITY_POLITICA WHERE TIPO_GESTION='Con observación comité' ) AS  WIP ON WICP.POLITICA_ID=WIP.POLITICA_ID AND FLG_VIGENTE=1) A GROUP BY  COD_UNICO) AS WIGCP"),function($join) {
                    $join->on('WIC.COD_UNICO','=','WIGCP.COD_UNICO');
                })
                ->leftjoin(DB::Raw("(SELECT *,DATEDIFF(MONTH,GETDATE(),FECHA_FIN) TIEMPO_RESTANTE FROM ( SELECT *,ROW_NUMBER() OVER (PARTITION BY COD_UNICO ORDER BY FECHA_FIN DESC) MAXIMO FROM WEBBE_INFINITY_CLIENTE_REVISION WHERE POLITICA_ID='23' AND FLG_VIGENTE='1' )A WHERE A.MAXIMO=1) AS WICR"), function($join) {
                    $join->on('WIC.COD_UNICO', '=', 'WICR.COD_UNICO');
                    $join->on('WICR.FECHA_FIN', '>', DB::Raw("GETDATE()"));
                })
                ->select("WIC.PERIODO","WU.REGISTRO","WU.ID_CENTRO","WU.ID_ZONA","WIC.MAX_DIAS_ATRASO","WICR.TIEMPO_RESTANTE",DB::Raw("(CASE WHEN WISRV.COD_UNICO IS NOT NULL THEN 1 ELSE 0 END) AS PERMITIDO_RSG"),'WIGCP.CANT_GESTIONES_COMITE_PENDIENTES','WIGP.CANT_GESTIONES_PENDIENTES','WIVC.CANT_POLI_CUALI','WIS.TIPO_SALIDA','WIS.FECHA_SALIDA','WIS.PERMITIDOS','WIC.FECHA_INGRESO_INFINITY','WIC.FLG_VISIBLE',DB::Raw('CAST (WIC.RMG AS NUMERIC) AS RMG'),'WIC.CLASIFICACION', 'WIC.FEVE', 'WIC.SALDO_VENCIDO_RCC', 'WIC.SALDO_VENCIDO_IBK', 'WIC.FLG_INFINITY', 'WIC.COD_UNICO', 'WIC.NOMBRE', 'WIC.SALDO_INTERBANK', 'WIC.SALDO_RCC', DB::Raw('(WIC.SALDO_INTERBANK*100/dbo.ISZERO(ISNULL(WIC.SALDO_RCC,1),1)) AS SOW'),'WIC.FECHA_VENCIMIENTO_POLITICA', DB::Raw("(CASE WHEN WIA.NIVEL_ALERTA='11' THEN '1' WHEN WIA.NIVEL_ALERTA='22' THEN '2' WHEN WIA.NIVEL_ALERTA='33' THEN '3' WHEN WIA.NIVEL_ALERTA='99' THEN '999' ELSE WIA.NIVEL_ALERTA END) AS NIVEL_ALERTA"), DB::Raw("(CASE WHEN WIA.NIVEL_ALERTA_ANTERIOR='11' THEN '1' WHEN WIA.NIVEL_ALERTA_ANTERIOR='22' THEN '2' WHEN WIA.NIVEL_ALERTA_ANTERIOR='33' THEN '3' WHEN WIA.NIVEL_ALERTA_ANTERIOR='99' THEN '999' ELSE WIA.NIVEL_ALERTA_ANTERIOR END) AS NIVEL_ALERTA_ANTERIOR"), DB::Raw("(CASE WHEN WIA.NIVEL_ALERTA_ANTERIOR_2='11' THEN '1' WHEN WIA.NIVEL_ALERTA_ANTERIOR_2='22' THEN '2' WHEN WIA.NIVEL_ALERTA_ANTERIOR_2='33' THEN '3' WHEN WIA.NIVEL_ALERTA_ANTERIOR_2='99' THEN '999' ELSE WIA.NIVEL_ALERTA_ANTERIOR_2 END) AS NIVEL_ALERTA_ANTERIOR_2"), 'WU.NOMBRE as EJECUTIVO', 'WZ.ZONAL', 'WJ.JEFATURA', 'WIC.RATING', DB::Raw('CONVERT(DATE,WIC.METODIZADO_1_FECHA) AS FECHA_ULTIMO_EEFF_1'), DB::Raw('CONVERT(DATE,WIC.METODIZADO_2_FECHA) AS FECHA_ULTIMO_EEFF_2'), DB::Raw('CONVERT(DATE,WIC.FECHA_ULTIMA_VISITA) AS FECHA_ULTIMA_VISITA'), DB::Raw('CONVERT(DATE,WIC.FECHA_ULTIMO_IBR) AS FECHA_ULTIMO_IBR'), 
                DB::Raw('CONVERT(DATE,WIC.FECHA_ULTIMO_F02) AS FECHA_ULTIMO_F02'), DB::Raw('CONVERT(DATE,WIC.FECHA_ULTIMO_RECALCULO) AS FECHA_ULTIMO_RECALCULO'), DB::Raw("CASE WHEN FLG_INFINITY != 1 THEN '1900-01-01' ELSE FECHA_ULTIMA_VISITA END AS FECHA_ULTIMA_VISITA_INFINITY"), DB::Raw("CASE WHEN FLG_INFINITY != 1 THEN '1900-01-01' ELSE FECHA_ULTIMO_RECALCULO END AS FECHA_ULTIMO_RECALCULO_INFINITY"),DB::Raw("(CASE WHEN WICR.COD_UNICO IS NOT NULL THEN 1 ELSE 0 END) AS PROTEGIDO"));

        // if ($infinity) {
        //     $sql = $sql->wherein('WIC.FLG_INFINITY', self::TODOS );
        // }
        if (is_array($periodo)) {
            $sql = $sql->whereIn('WIC.PERIODO', $periodo);
        } 
        else {
            $sql = $sql->where('WIC.PERIODO', $periodo_sql->PERIODO);
        }
        if ($codunico) {
            $sql = $sql->where('WIC.COD_UNICO', $codunico);
        }

        if ($ejecutivo) {
            $sql = $sql->where('WU.REGISTRO', $ejecutivo);
        }
        if ($jefatura) {
            $sql = $sql->where('WU.ID_CENTRO', $jefatura);
        }
        if ($zonal) {
            $sql = $sql->where('WU.ID_ZONA', $zonal);
        }
        
        if (!empty($filtros) && isset($filtros['zonal']) && (!empty($filtros['zonal']) || $filtros['zonal']!=null)) {
            if (isset($filtros['jefatura']) && (!empty($filtros['jefatura']) || $filtros['jefatura']!=null)) {
                if (isset($filtros['ejecutivo']) && (!empty($filtros['ejecutivo']) || $filtros['ejecutivo']!=null)) {
                    $sql = $sql->where('WU.REGISTRO', $filtros['ejecutivo']);
                }else{
                    $sql = $sql->wherein('WU.REGISTRO', self::getEjecutivosByJefeZonal2($filtros['jefatura']));
                }
            }else{
                if (isset($filtros['ejecutivo']) && (!empty($filtros['ejecutivo']) || $filtros['ejecutivo']!=null)) {
                    $sql = $sql->where('WU.REGISTRO', $filtros['ejecutivo']);
                }else{
                    $sql = $sql->wherein('WU.REGISTRO', self::getEjecutivosZonal3ByZonal($filtros['zonal']));
                }
            }
        }elseif(isset($filtros['banca']) && (!empty($filtros['banca']) || $filtros['banca']!=null))
        {
            $sql = $sql->where('WIC.SEGMENTO','=',$filtros['banca']);
        }
        return $sql;
    }

    function getEjecutivosByJefeZonal2($registrojefe) {
        $jefe = DB::Table('T_WEB_ARBOL_DIARIO')
                    ->where('REGISTRO','=',$registrojefe)
                    ->get()
                    ->first();

        $sql = DB::Table('T_WEB_ARBOL_DIARIO')
                    ->where('NOMBRE_JEFE','=',$jefe->NOMBRE_JEFE)
                    ->whereNotNull('ENCARGADO')
                    ->get();
        $registros=[];
        foreach ($sql as $key => $ejecutivo) {
            $registros[]=$ejecutivo->REGISTRO;
        }
        return $registros;
    }

    function getEjecutivosZonal3ByZonal($nombrezonal) {
        $sql = DB::Table('T_WEB_ARBOL_DIARIO')
                    ->where('NOMBRE_ZONAL','=',$nombrezonal)
                    ->whereNotNull('ENCARGADO')
                    ->get();
        $registros=[];
        foreach ($sql as $key => $ejecutivo) {
            $registros[]=$ejecutivo->REGISTRO;
        }
        return $registros;
    }

    function getListaGestiones($codUnico) {
        $sql = DB::Table('WEBBE_INFINITY_GESTION AS WIG')
                ->leftjoin('WEBBE_INFINITY_GESTION_ESTADO AS WIGE', function($join) {
                    $join->on('WIG.ESTADO_GESTION', '=', 'WIGE.ID_ESTADO_GESTION');
                })
                ->join('WEBVPC_USUARIO AS WU', function($join) {
                    $join->on('WU.REGISTRO', '=', 'WIG.REGISTRO_EN');
                })
                ->leftJoin('WEBBE_INFINITY_POLITICA AS WP', function($join) {
                    $join->on('WIG.POLITICA_ID', '=', 'WP.POLITICA_ID');
                })
                ->select(
                        'WP.NOMBRE AS NOMBRE_POLITICA', 'WIG.COD_UNICO', 'WIG.REGISTRO_EN', DB::Raw('CONVERT(DATE,WIG.FECHA_GESTION) AS FECHA_GESTION'), 'WIG.FECHA_REGISTRO', 'WIG.COMENTARIO', 'WIG.ADJUNTO', 'WIG.ESTADO_GESTION','WIGE.NOMBRE_ESTADO','WIG.DECISION', DB::Raw('dbo.InitCap(WU.NOMBRE) AS NOMBRE'), DB::Raw('dbo.InitCap(WU.CARGO) AS CARGO')
                )
                ->where('WIG.COD_UNICO', '=', $codUnico)
                ->orderBy('WIG.FECHA_GESTION', 'DESC');

        return $sql;
    }

    function getListaGestionesComite($codUnico)
    {
        $sql = DB::Table('WEBBE_INFINITY_GESTION AS WIG')
                ->leftjoin('WEBBE_INFINITY_GESTION_ESTADO AS WIGE', function($join) {
                    $join->on('WIG.ESTADO_GESTION', '=', 'WIGE.ID_ESTADO_GESTION');
                })
                ->join('WEBVPC_USUARIO AS WU', function($join) {
                    $join->on('WU.REGISTRO', '=', 'WIG.REGISTRO_EN');
                })
                ->leftJoin('WEBBE_INFINITY_POLITICA AS WP', function($join) {
                    $join->on('WIG.POLITICA_ID', '=', 'WP.POLITICA_ID');
                })
                ->select(
                        'WP.NOMBRE AS NOMBRE_POLITICA', 'WIG.COD_UNICO', 'WIG.REGISTRO_EN', DB::Raw('CONVERT(DATE,WIG.FECHA_GESTION) AS FECHA_GESTION'), 'WIG.FECHA_REGISTRO', 'WIG.COMENTARIO', 'WIG.ADJUNTO', 'WIG.ESTADO_GESTION','WIGE.NOMBRE_ESTADO','WIG.DECISION', DB::Raw('dbo.InitCap(WU.NOMBRE) AS NOMBRE'), DB::Raw('dbo.InitCap(WU.CARGO) AS CARGO')
                )
                ->where('WIG.COD_UNICO', '=', $codUnico)
                ->where('WIG.ESTADO_GESTION', '=', self::ESTADO_COMITE)
                ->orderBy('WIG.FECHA_GESTION', 'DESC');

        return $sql;
    }

    function getListaDocumentos($codUnico) {
        $sql = DB::Table('WEBBE_INFINITY_DOCUMENTACION AS WID')
                ->select(
                        'WID.*', DB::Raw('CONVERT(DATE,WID.FECHA_DOCUMENTO) AS FECHA_FIRMA')
                )
                ->where('WID.COD_UNICO', '=', $codUnico)
                ->where('WID.NOMBRE_DOCUMENTO', '!=', 'MDL')
                ->orderBy('WID.TIPO_DOCUMENTO', 'DESC')
                ->orderBy('WID.FECHA_REGISTRO', 'DESC');

        return $sql;
    }

    function getRecalculo($codUnico) {
        $sql = DB::Table('WEBBE_INFINITY_DOCUMENTACION AS WID')
                ->select(
                        'WID.*', DB::Raw('CONVERT(DATE,WID.FECHA_DOCUMENTO) AS FECHA_RECALCULO')
                )
                ->where('WID.COD_UNICO', '=', $codUnico)
                ->where('TIPO_DOCUMENTO', '=', self::TIPO_RECALCULO)
                ->orderBy('WID.FECHA_REGISTRO', 'DESC');

        return $sql;
    }

    function getEstadosGestion($esJefe) {
        $sql = DB::Table('WEBBE_INFINITY_GESTION_ESTADO AS WIGE')
                ->select()
                ->whereNotIn('ID_ESTADO_GESTION',array('4','5'));

        if (!$esJefe)
            $sql = $sql->where('ID_ESTADO_GESTION', '<>', self::ESTADO_REVISADO);
        return $sql;
    }

    function getHistoriaAlertas($periodos, $codunico) {
        $sql = $this->getListaClientesAlerta($periodos, $codunico,null,null,null,0);
        // $sql = $this->getListaClientesAlerta($periodos, $codunico,null,null,null,0)->where('WIC.SEGMENTO','=','MEDIANA EMPRESA');
        $sql->columns = [];
        $sql->select('WIC.PERIODO', 'WIC.COD_UNICO', 'WIC.RATING', 'WIA.NIVEL_ALERTA','WIC.FLG_INFINITY','WIC.SALDO_INTERBANK','WIC.SALDO_RCC','WIC.MAX_DIAS_ATRASO')
                ->orderBy('WIC.PERIODO', 'asc');
        return $sql;
    }

    /* Se puede crear una función para que inserte dado un nombre de tabla y un archivo */

    function guardarAdjunto($periodo, $documento, $archivo, $ruta, $actualizar) {

        DB::beginTransaction();
        $status = true;
        try {
            if ($documento['NOMBRE_DOCUMENTO']!='MDL') {
                $documento['MDL']=NULL;
            }
            DB::table('WEBBE_INFINITY_DOCUMENTACION')->insert($documento);
            $archivo->storeAs($ruta, $documento['ADJUNTO']);

            if ($actualizar) {
                DB::table('WEBBE_INFINITY_CLIENTE')
                        ->where('COD_UNICO', '=', $documento['COD_UNICO'])
                        ->where('PERIODO', '=', $periodo)
                        ->update([$actualizar => $documento['FECHA_DOCUMENTO']]);
            }

            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }

        return $status;
    }

    /* Se puede crear una función para que inserte dado un nombre de tabla y un archivo */

    function guardarGestion($gestion, $archivo, $ruta,$salida=null,$flg_salida,$politicaEliminar,$periodo,$tiemporevision=null) {
        // dd($gestion, $archivo, $ruta,$salida,$flg_salida,$politicaEliminar,$periodo,$tiemporevision);
        DB::beginTransaction();
        $status = true;
        try {

            $tbcliente =DB::table('WEBBE_INFINITY_CLIENTE')
                    ->select(DB::Raw("MAX(PERIODO) PERIODO"))
                    ->get()->first();

            if($archivo){
                $archivo->storeAs($ruta, $gestion['ADJUNTO']);
            }

            //EL ULTIMO DATO DE GESTION SE ACTUALIZA A LA POLITICA ASOCIADA
            //dd($gestion['ID_POLITICA_CLIENTE']);

            if (isset($gestion['ID_POLITICA_CLIENTE'])) {
                DB::table('WEBBE_INFINITY_CLIENTE_POLITICA')
                    ->where('ID_POLITICA_CLIENTE', '=', $gestion['ID_POLITICA_CLIENTE'])
                    ->update([
                        'ESTADO_GESTION'=>$gestion['ESTADO_GESTION'],
                        'FECHA_GESTION'=>$gestion['FECHA_GESTION'],
                        'REGISTRO_EN'=>$gestion['REGISTRO_EN'],
                        'FECHA_REGISTRO'=>Carbon::now()
                    ]);
            }

            DB::table('WEBBE_INFINITY_GESTION')->insert($gestion);
            if ($tiemporevision) {
                DB::table('WEBBE_INFINITY_CLIENTE_REVISION')->insert($tiemporevision);
            }

            if ($politicaEliminar){
                DB::table('WEBBE_INFINITY_CLIENTE_POLITICA')
                ->where('ID_POLITICA_CLIENTE', '=', $politicaEliminar)
                ->update([
                    'FLG_VIGENTE'=> 0,
                ]);
            }


            if($flg_salida){
                if (!empty($salida) & !isset($salida['FECHA_REINGRESO'])){
                    DB::table('WEBBE_INFINITY_SALIDAS')->insert($salida);
                        if ($gestion['DECISION']==self::DESAPROBADO) {

                            TestPolitica::queue_act_susp($gestion['COD_UNICO'],$periodo,'Suspensión','Suspensión',null,null,$gestion);
                        }
                }
                
                DB::table('WEBBE_INFINITY_CLIENTE')
                ->where('COD_UNICO', $salida['COD_UNICO'])
                ->where('PERIODO',$tbcliente->PERIODO)
                ->update([
                    'FLG_INFINITY'=> 0,
                ]);
            }
            DB::commit();

        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }

        return $status;
    }

    function guardarControl($control) {
        //dd($control);
        DB::beginTransaction();
        $status = true;
        try {
            //Actualizamos el flag infinity
            DB::table('WEBBE_INFINITY_CLIENTE')
                ->where('COD_UNICO', '=', $control['COD_UNICO'])
                ->where('PERIODO', '=', $control['PERIODO'])
                ->update([
                        'FLG_INFINITY' => $control['FLG_INFINITY_CAMBIO'],
                        'FECHA_VENCIMIENTO_POLITICA' => NULL,
                        'FLG_LOGO' => NULL
                ]);

            //Registramos la última actualización
            DB::table('WEBBE_INFINITY_CONTROL_CREDITOS')
                ->where('COD_UNICO', '=', $control['COD_UNICO'])
                ->update(['FLG_ULTIMO' => 0]);

            DB::table('WEBBE_INFINITY_CONTROL_CREDITOS')->insert($control);
            
            //Buscamos si tiene alguna politica asociada al comité
            $cantpoliticascomite = DB::table('WEBBE_INFINITY_CLIENTE_POLITICA')
                ->where('COD_UNICO', '=', $control['COD_UNICO'])
                ->where('PERIODO', '=', $control['PERIODO'])
                ->wherein('POLITICA_ID', array('4','5','6','7','11'))
                ->get()->count();

            //falta que comite dé usabilidad a la herramienta
            if (1<0) {
                DB::table('WEBBE_INFINITY_CLIENTE')
                    ->where('COD_UNICO', '=', $control['COD_UNICO'])
                    ->where('PERIODO', '=', $control['PERIODO'])
                    ->update([
                            'FLG_INFINITY' => '1',
                            'FLG_LOGO' => '1'
                    ]);
            }

            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }

        return $status;
    }


    function getIndicadoresInfinity($periodo,$filtro=null,$segmento = null, $codunico = null, $zonal = null, $jefatura = null, $ejecutivo = null) {
        $sql = $this->getListaClientesAlerta($periodo, $codunico, $zonal, $jefatura, $ejecutivo, 1 ,$filtro)->where('WIC.FLG_VISIBLE','=','1');
        if ($segmento!=null) {
            $sql = $sql->where('WIC.SEGMENTO','=',$segmento);
        }
        
        //Trucazo
        $documentacion = $sql->get();
        $documentacionCompleta=0;
        $visitaCheck=0;
       
        foreach($documentacion as $registro){
            if($registro->FLG_INFINITY>='1' && !in_array(\App\Entity\Infinity\AlertaDocumentacion::getNivelAlertaDocumentacionTotal($registro->FLG_INFINITY,$registro->FECHA_ULTIMO_EEFF_1,
                $registro->FECHA_ULTIMO_EEFF_2,$registro->FECHA_ULTIMO_IBR,$registro->FECHA_ULTIMO_F02,$registro->FECHA_INGRESO_INFINITY), array(\App\Entity\Infinity\AlertaDocumentacion::ALERTA_VERDE,\App\Entity\Infinity\AlertaDocumentacion::ALERTA_VERDE_1)))
                $documentacionCompleta++;
        }

        foreach($documentacion as $registro){
            if($registro->FLG_INFINITY>='1' && \App\Entity\Infinity\AlertaDocumentacion::getNivelAlerta(\App\Entity\Infinity\AlertaDocumentacion::ID_VISITA,$registro->FECHA_ULTIMA_VISITA,$registro->FLG_INFINITY)!=\App\Entity\Infinity\AlertaDocumentacion::ALERTA_VERDE)
                $visitaCheck++;
        }

        $sql->columns = [];
        $sql = $sql->select(DB::Raw("NIVEL_ALERTA")
                        , DB::Raw("SUM(CASE WHEN FLG_INFINITY >=1 THEN 1 ELSE 0 END) INFINITY")
                        , DB::Raw('COUNT(1) TOTAL')
                        , DB::Raw("0 GESTIONADO_INFINITY")
                        , DB::Raw("SUM(CASE WHEN FLG_INFINITY>=1 AND (DATEDIFF(MONTH,FECHA_ULTIMA_VISITA,GETDATE())>=5 OR FECHA_ULTIMA_VISITA IS NULL) THEN 1 ELSE 0 END) VISITA_INFINITY")
                        , DB::Raw("SUM(CASE WHEN FLG_INFINITY>=1 AND (DATEDIFF(MONTH,FECHA_ULTIMO_RECALCULO,GETDATE())>=5 OR FECHA_ULTIMO_RECALCULO IS NULL)  THEN 1 ELSE 0 END) RECALCULOS_PENDIENTES")
                        , DB::Raw("SUM(CASE WHEN FLG_INFINITY >=1 AND SALDO_VENCIDO_IBK>0 THEN 1 ELSE 0 END) VENCIDO_IBK")
                        , DB::Raw("SUM(CASE WHEN FLG_INFINITY >=1 AND SALDO_VENCIDO_RCC>0 THEN 1 ELSE 0 END) VENCIDO_RCC")
                        , DB::Raw("SUM(CASE WHEN FLG_INFINITY >=1 AND WIC.COD_UNICO=WIG.COD_UNICO THEN 1 ELSE 0 END) GESTIONES_PENDIENTES")
                )
                ->groupBy(DB::Raw("NIVEL_ALERTA"))
                ->orderBy(DB::Raw("NIVEL_ALERTA"))
                ->get();
            // $alerta->VISITA_INFINITY_2=[];
        foreach($sql as $alerta){
            $alerta->DOCUMENTACION_COMPLETA_INFINITY_2=$documentacionCompleta;
            $alerta->VISITA_INFINITY_2=$visitaCheck;
        }
        // dd($sql);
        $sql=$sql->keyBy('NIVEL_ALERTA');
        //dd($sql);
        return $sql;
    }

    function getIndicadoresInfinity_2($periodo,$filtro=null,$segmento = null, $codunico = null, $zonal = null, $jefatura = null, $ejecutivo = null) {
        $sql = $this->getListaClientesAlerta($periodo, $codunico, $zonal, $jefatura, $ejecutivo, 1 ,$filtro)->where('WIC.FLG_VISIBLE','=','1');
        if ($segmento!=null) {
            $sql = $sql->where('WIC.SEGMENTO','=',$segmento);
        }
        
        //Trucazo
        $documentacion = $sql->get();
        $documentacionCompleta=0;
        $visitaCheck=0;
       
        foreach($documentacion as $registro){
            if($registro->FLG_INFINITY>='1' && !in_array(\App\Entity\Infinity\AlertaDocumentacion::getNivelAlertaDocumentacionTotal($registro->FLG_INFINITY,$registro->FECHA_ULTIMO_EEFF_1,
                $registro->FECHA_ULTIMO_EEFF_2,$registro->FECHA_ULTIMO_IBR,$registro->FECHA_ULTIMO_F02,$registro->FECHA_INGRESO_INFINITY), array(\App\Entity\Infinity\AlertaDocumentacion::ALERTA_VERDE,\App\Entity\Infinity\AlertaDocumentacion::ALERTA_VERDE_1)))
                $documentacionCompleta++;
        }

        foreach($documentacion as $registro){
            if($registro->FLG_INFINITY>='1' && \App\Entity\Infinity\AlertaDocumentacion::getNivelAlerta(\App\Entity\Infinity\AlertaDocumentacion::ID_VISITA,$registro->FECHA_ULTIMA_VISITA,$registro->FLG_INFINITY)!=\App\Entity\Infinity\AlertaDocumentacion::ALERTA_VERDE)
                $visitaCheck++;
        }
        $data=[];
        $data['documentarion_completa']=$documentacionCompleta;
        $data['visita_check']=$visitaCheck;

        return $data;
    }

    function getListaClientesControl($periodo, $codunico = null, $zonal = null, $jefatura = null, $ejecutivo = null) {

        $sql = $this->getListaClientesAlerta($periodo, $codunico, $zonal, $jefatura, $ejecutivo)->where('WIC.FLG_VISIBLE','1');
        // $sql = $this->getListaClientesAlerta($periodo, $codunico, $zonal, $jefatura, $ejecutivo)->where('WIC.FLG_VISIBLE','1')->where('WIC.SEGMENTO','=','MEDIANA EMPRESA');
        
        $sql = $sql->addSelect('WICC.FECHA_REGISTRO','WICC.COMENTARIO','WICC.FLG_INFINITY_CAMBIO'
                )
                ->leftJoin('WEBBE_INFINITY_CONTROL_CREDITOS AS WICC',function ($join){
                   $join->on('WICC.FLG_ULTIMO','=',DB::Raw(1)); 
                   $join->on('WICC.COD_UNICO','=','WIC.COD_UNICO'); 
                })->get();

        return $sql;
    }

    static function ChangeInfinityEstado($periodo,$codUnico)
    {
        $fecha_vencimiento = DB::table('WEBBE_INFINITY_CLIENTE')
                ->select('FECHA_VENCIMIENTO_POLITICA')
                ->where('COD_UNICO', '=', $codUnico)
                ->where('PERIODO', '=', $periodo)
                ->get()
                ->first();
        $fechaMin_cliente = Carbon::parse($fecha_vencimiento->FECHA_VENCIMIENTO_POLITICA);
        $now = Carbon::now();
        $timporestante = $fechaMin_cliente->diffInDays($now);

        $cliente_politica = DB::table('WEBBE_INFINITY_CLIENTE_POLITICA')
                ->select('POLITICA_ID','FECHA_REGISTRO','FECHA_VENCIMIENTO')
                ->where('COD_UNICO', '=', $codUnico)
                ->where('PERIODO', '=', $periodo)
                ->get();
        if ($cliente_politica) {
            foreach ($cliente_politica as $key => $value) {
                $fechas_politicas_id[$value->POLITICA_ID]=$value->FECHA_VENCIMIENTO;
            }
            $fechaMin=min($fechas_politicas_id);

            $POLITICA_ID = DB::table('WEBBE_INFINITY_CLIENTE_POLITICA')
                ->select('POLITICA_ID','FECHA_REGISTRO','FECHA_VENCIMIENTO')
                ->where('COD_UNICO', '=', $codUnico)
                ->where('PERIODO', '=', $periodo)
                ->where('FECHA_VENCIMIENTO', '=', $fechaMin)
                ->get()->first();
            $fechaMin_clientepolitica = Carbon::parse($fechaMin);
        }
        if (1<0){
            $sql = DB::table('WEBBE_INFINITY_CLIENTE')
                ->select('COD_UNICO','FLG_INFINITY')
                ->where('COD_UNICO', '=', $codUnico)
                ->where('PERIODO', '=', $periodo)
                ->update([
                            'FLG_INFINITY' => 0,
                ]);
        }
        return isset($sql) ? $sql : '';
    }

    static function PoliticasComiteCliente($periodo,$codunico)
    {
        $politicascomite= DB::table('WEBBE_INFINITY_POLITICA')->select('POLITICA_ID')->where('TIPO_GESTION','=','Con observación comité')->get();
        foreach ($politicascomite as $in => $val) {
            $ids[]=$val->POLITICA_ID;
        }
        $sql = DB::table('WEBBE_INFINITY_CLIENTE_POLITICA AS WCP')
                ->join('WEBBE_INFINITY_POLITICA AS WIP', function($join) {
                    $join->on('WCP.POLITICA_ID', '=', 'WIP.POLITICA_ID');
                })
                ->select('WCP.ID_POLITICA_CLIENTE','WCP.POLITICA_ID','WIP.NOMBRE')
                ->where('WCP.COD_UNICO', '=', $codunico)
                ->where('WCP.FLG_VIGENTE', '=', 1)
                ->whereIn('WCP.POLITICA_ID',$ids)
                ->get();

        if (!empty($sql)){
            $options="<option value=''>Seleccionar Política</option>";
            foreach ($sql as $key => $value) {
                $options.="<option politica_id='".$value->POLITICA_ID."' value='".$value->ID_POLITICA_CLIENTE."'>".$value->NOMBRE."</option>";
            }
        }
        return isset($options) ? $options : '';
    }

    static function getUsuarioComite($rmg,$rating)
    {
        $autonomias = DB::table("WEBBE_INFINITY_AUTONOMIA AS WIAU")
                ->select("WU.NOMBRE","WIAU.*")
                ->leftjoin('WEBVPC_USUARIO AS WU', function($join) {
                    $join->on('WIAU.REGISTRO', '=', 'WU.REGISTRO');
                })
                ->where("WIAU.RMG_MIN","<",$rmg)
                ->where("WIAU.RMG_MAX",">=",$rmg)
                ->where("WIAU.RATING","=",$rating)
                ->where("WIAU.FLG_PRINCIPALES",1)
                ->get();
        $todos = DB::table("WEBBE_INFINITY_AUTONOMIA AS WIAU")
                ->where("WIAU.RMG_MIN","<",$rmg)
                ->where("WIAU.RMG_MAX",">=",$rmg)
                ->where("WIAU.RATING","=",$rating)
                ->get();
        $data['autonomias']=$autonomias;
        $data['todos']=$todos;
        return $data;
    }

    static function getSemaforosCliente($periodo,$clientes = array(),$hayseleccionados = false){
        $sql = DB::Table('WEBBE_INFINITY_CLIENTE AS WIC')
                ->join('WEBBE_INFINITY_ALERTA AS WIA', function($join) {
                    $join->on('WIC.COD_UNICO', '=', 'WIA.COD_UNICO');
                    $join->on('WIC.PERIODO', '=', 'WIA.PERIODO');
                })
                ->leftjoin(DB::Raw("(SELECT * FROM ( SELECT *,ROW_NUMBER() OVER (PARTITION BY COD_UNICO ORDER BY FECHA_FIN DESC) MAXIMO FROM WEBBE_INFINITY_CLIENTE_REVISION WHERE POLITICA_ID IN ('23') AND FLG_VIGENTE='1' )A WHERE A.MAXIMO=1) AS WICR"), function($join) {
                    $join->on('WIC.COD_UNICO', '=', 'WICR.COD_UNICO');
                    $join->on('WICR.FECHA_FIN', '>', DB::Raw("GETDATE()"));
                })
                ->select('WIC.COD_UNICO','WIA.NIVEL_ALERTA','WIA.NIVEL_ALERTA_ANTERIOR','WIA.NIVEL_ALERTA_ANTERIOR_2')
                ->whereNull('WICR.COD_UNICO')
                ->where('WIC.PERIODO',$periodo)
                ->where('WIC.SEGMENTO','=','MEDIANA EMPRESA')
                ->where('WIC.FLG_INFINITY','>',0);
        if ($clientes){
            $sql = $sql->whereIn('WIC.COD_UNICO',$clientes);
        }
        if ($hayseleccionados) {
            $sql= $sql->where('WIC.FLG_POLITICAPROCESO','=',1);
        }
        return $sql->get();

    }


    static function getPerformanceCliente($periodo, $clientes = array(),$hayseleccionados = false){
        $sqlgrupoecorelacionado =  DB::Table('WEBBE_INFINITY_GRUPO_ECO_RELACIONADO AS GE')
            ->select('C.PERIODO','C.COD_UNICO'
                    ,DB::raw("(CASE WHEN SUM(MONTO_VENCIDO)=0 THEN '0' ELSE CAST(SUM(MONTO_VENCIDO) AS VARCHAR(50)) END) AS MONTO_VENCIDO")
                    ,DB::raw("(CASE WHEN SUM(MONTO_VENCIDO_ANTERIOR)=0 THEN '0' ELSE CAST(SUM(MONTO_VENCIDO_ANTERIOR) AS VARCHAR(50)) END) AS MONTO_VENCIDO_ANTERIOR")
                    ,DB::raw("(CASE WHEN SUM(GE.MONTO_COACTIVA + GE.MONTO_LABORAL) =0 THEN '0' ELSE CAST(SUM(GE.MONTO_COACTIVA + GE.MONTO_LABORAL) AS VARCHAR(50)) END) DEUDA_LABORAL_COACTIVA")
                    ,DB::raw("(CASE WHEN SUM(GE.MONTO_COACTIVA_ANTERIOR + GE.MONTO_LABORAL_ANTERIOR) =0 THEN '0' ELSE CAST(SUM(GE.MONTO_COACTIVA_ANTERIOR + GE.MONTO_LABORAL_ANTERIOR) AS VARCHAR(50)) END) AS DEUDA_LABORAL_COACTIVA_ANTERIOR")
                    ,DB::raw('MIN(GE.FLG_CLASIFICACION ) PEOR_CLASIFICACION')
                    ,DB::Raw('0 FLG_POLITICA_GE')
                    )
            ->join('WEBBE_INFINITY_CLIENTE AS C', function ($join) {
                $join->on('C.COD_UNICO', '=', 'GE.COD_UNICO');
                $join->on('C.PERIODO', '=', 'GE.PERIODO');
            })
            ->where('C.PERIODO', $periodo)
            ->where('C.FLG_INFINITY','>=','1')
            ->where('C.SEGMENTO','=','MEDIANA EMPRESA')
            ->groupBy('C.PERIODO','C.COD_UNICO');
        if ($hayseleccionados) {
            $sqlgrupoecorelacionado= $sqlgrupoecorelacionado->where('C.FLG_POLITICAPROCESO','=',1);
        }
        $sqlgrupoecorelacionado=$sqlgrupoecorelacionado->get();


        $sqlgrupoeco = DB::table("WEBBE_INFINITY_GRUPO_ECO as WIGE")
                ->select("WIGE.PERIODO","WIGE.COD_UNICO","WIGE.FLG_POLITICA_GE")
                ->join('WEBBE_INFINITY_CLIENTE AS WIC', function ($join) {
                    $join->on('WIC.COD_UNICO', '=', 'WIGE.COD_UNICO');
                    $join->on('WIC.PERIODO', '=', 'WIGE.PERIODO');
                })
                ->where('WIC.PERIODO', $periodo)
                ->where('WIC.FLG_INFINITY','>=','1')
                ->where('WIC.SEGMENTO','=','MEDIANA EMPRESA')
                ->where('WIGE.FLG_POLITICA_GE','=',DB::Raw('1'));
        if ($hayseleccionados) {
            $sqlgrupoeco= $sqlgrupoeco->where('WIC.FLG_POLITICAPROCESO','=',1);
        }
        $sqlgrupoeco=$sqlgrupoeco->get();



        $grupoeco=[];
        $grupoecorelacionado=[];
        if (count($sqlgrupoecorelacionado)>0) {
            foreach ($sqlgrupoecorelacionado as $key => $value) {
                $grupoecorelacionado[$value->COD_UNICO] = $value;
            }
        }
        if (count($sqlgrupoeco)>0) {
            foreach ($sqlgrupoeco as $key => $value) {
                $grupoeco[$value->COD_UNICO] = $value;
            }
        }

        if ($grupoecorelacionado) {
            foreach ($grupoecorelacionado as $key => $value) {
                foreach ($grupoeco as $key_2 => $value_2) {
                    if ($key==$key_2) {
                        $grupoecorelacionado[$key]->FLG_POLITICA_GE = !empty($value_2->FLG_POLITICA_GE) ? $value_2->FLG_POLITICA_GE : 0;
                    }
                }
            }
        }

        $data=[];

        if (!empty($grupoecorelacionado) && count($grupoecorelacionado)>0) {
            foreach ($grupoecorelacionado as $codunico => $datos) {
                $data[]=$datos;
            }
        }
        return $data;
    }


    static function getDocumentosCliente($periodo,$clientes = array(),$hayseleccionados = false){
        $sql =  DB::Table('WEBBE_INFINITY_CLIENTE AS WIC')
                ->select('WIC.COD_UNICO','WIC.FECHA_INGRESO_INFINITY','WIC.METODIZADO_1_FECHA AS FECHA_DDJJ','METODIZADO_2_FECHA AS FECHA_EEFF','FECHA_ULTIMO_IBR AS FECHA_IBR','FECHA_ULTIMO_F02 AS FECHA_F02','FECHA_ULTIMA_VISITA AS FECHA_ULTIMA_VISITA')
                ->where('WIC.PERIODO',$periodo)
                ->where('WIC.FLG_INFINITY','>=','1')
                ->where('WIC.SEGMENTO','=','MEDIANA EMPRESA');
        if ($hayseleccionados) {
            $sql= $sql->where('WIC.FLG_POLITICAPROCESO','=',1);
        }
        return $sql->get();
    }

    static function getConocemeStakeHolder($conoceme_hist_id,$nombre,$tipo){
        $sql= DB::Table('WEBBE_INFINITY_CONOCEME_STAKEHOLDER_HIST AS WICS')
                ->select()
                ->where('WICS.CONOCEME_HIST_ID','=',$conoceme_hist_id)
                ->where('WICS.NOMBRE','=',$nombre)
                ->where('WICS.TIPO','=',$tipo)
                ->get()->first();
        return $sql;
    }

    static function getDatosCliente($codcliente,$periodo)
    {
        $sql= DB::Table('WEBBE_INFINITY_CLIENTE as WIC')
                ->select('USU.REGISTRO','USU.NOMBRE AS NOMBRE_EJECUTIVO','WIC.COD_UNICO','WIC.NOMBRE','WIC.NUM_DOC','USU.CORREO','WIC.SEGMENTO','WIC.RMG')
                ->join('ARBOL_DIARIO_2 AS AD', function ($join) {
                    $join->on('WIC.COD_SECTORISTA', '=', 'AD.COD_SECT_LARGO');
                })
                ->join('WEBVPC_USUARIO AS USU', function ($join) {
                    $join->on('AD.LOGIN', '=', 'USU.REGISTRO');
                })
                ->where('WIC.COD_UNICO','=',$codcliente)
                ->where('PERIODO','=',$periodo)
                ->get()->first();
        return $sql;
    }

    static function getDatosClienteVISITAMECOVID($codcliente)
    {
        $sql = DB::table('WEBBE_INFINITY_ME_BUCKET')->where('CODUNICOCLI',$codcliente)->select('BUCKET')->first();
        $periodo_sql = DB::Table(DB::raw("(SELECT MAX(PERIODO) AS PERIODO FROM WEBBE_INFINITY_CLIENTE) AS ABC "))->first();
        $sql= DB::Table(DB::raw("( SELECT M.PERIODO,
        M.COD_UNICO,
        A.NUM_DOC,
        M.NOMBRE,
        ISNULL(A.BANCA,'BE') BANCA,
        M.COD_SECTORISTA,
        M.SALDO_INTERBANK,
        A.SALDO_RCC,
        ISNULL(A.RATING,VPC.RATING) RATING,
        A.FLG_INFINITY,
        A.CLASIFICACION,
        ISNULL(M.FEVE,VPC.FEVE) FEVE,
        A.SALDO_VENCIDO_IBK,
        A.SALDO_VENCIDO_RCC,
        A.METODIZADO_1_FECHA,
        A.METODIZADO_2_FECHA,
        A.FECHA_ULTIMA_VISITA,
        A.FECHA_ULTIMO_IBR,
        A.FECHA_ULTIMO_F02,
        A.GARANTIA,
        M.SALDO_RIESGO,
        A.FECHA_ULTIMO_RECALCULO,
        A.VENTAS,
        M.MAX_DIAS_ATRASO,
        A.PROVINCIA,
        A.DISTRITO,
        A.FECHA_VENCIMIENTO_POLITICA,
        A.FLG_VISIBLE,
        M.RMG,
        A.FECHA_INGRESO_INFINITY,
        CASE WHEN A.SEGMENTO IS NULL THEN 
        CASE WHEN ARB.BANCA = 'BEL' AND ARB.ZONAL = 'BEL ZONAL 2' THEN 'MEDIANA EMPRESA'  
            WHEN ARB.BANCA = 'BEL' AND ARB.ZONAL IN ('BEL ZONAL 3','BEL ZONAL 1') THEN 'GRAN EMPRESA' 
            WHEN ARB.BANCA = 'BEP' THEN 'PROVINCIA'
            WHEN ARB.BANCA = 'BC' THEN 'CORPORATIVA' END ELSE A.SEGMENTO END SEGMENTO,
        A.FECHA_CARGA,
        A.FLG_POLITICAPROCESO
        FROM (
        SELECT ISNULL(T1.PERIODO,'".($periodo_sql->PERIODO)."') PERIODO, ISNULL(T1.COD_UNICO,T2.COD_UNICO) COD_UNICO,
            ISNULL(T1.NOMBRE,T2.NOMBRE) NOMBRE,ISNULL(T1.RMG,T2.RMG) RMG,
            ISNULL(T1.FEVE,T2.FEVE) FEVE,
            ISNULL(T1.MAX_DIAS_ATRASO,T2.MAX_DIAS_ATRASO) MAX_DIAS_ATRASO,
            ISNULL(T1.COD_SECTORISTA,T2.CODSECTORISTA) COD_SECTORISTA,
            ISNULL(T1.SALDO_INTERBANK,T2.SALDO_TOTAL) SALDO_INTERBANK,
            T2.SALDO_RIESGO
        FROM
        (SELECT DISTINCT PERIODO,COD_UNICO,FEVE,MAX_DIAS_ATRASO,RMG,NOMBRE,COD_SECTORISTA,SALDO_INTERBANK FROM WEBBE_INFINITY_CLIENTE WHERE BANCA = 'BE' AND PERIODO = '".($periodo_sql->PERIODO)."') T1
        FULL OUTER JOIN
        (SELECT DISTINCT COD_UNICO,FEVE,RMG,NOMBRE,MAX_DIAS_ATRASO,CODSECTORISTA,SALDO_TOTAL,SALDO_RIESGO FROM WEBBE_INFINITY_INFO_DIARIA_CLI) T2
        ON T1.COD_UNICO = T2.COD_UNICO
        )M LEFT JOIN WEBBE_INFINITY_CLIENTE A ON M.COD_UNICO = A.COD_UNICO AND M.PERIODO = A.PERIODO
        LEFT JOIN ARBOL_DIARIO_2 ARB ON M.COD_SECTORISTA = ARB.COD_SECT_LARGO 
        LEFT JOIN WEBVPC_CLIENTES_VPC VPC ON M.COD_UNICO = VPC.COD_UNICO 
        WHERE LEFT(M.COD_SECTORISTA,3) != '070'
) AS WCV"))
                ->leftjoin('ARBOL_DIARIO_2 AS A', function ($join) {
                    $join->on('WCV.COD_SECTORISTA', '=', 'A.COD_SECT_LARGO');
                })
                ->leftjoin('WEBBE_INFINITY_ME_BUCKET AS BUCK', function ($join) {
                    $join->on('WCV.COD_UNICO', '=', 'BUCK.CODUNICOCLI');
                })
                ->leftjoin(DB::raw("
                    (SELECT CODUNICOCLI,SUM(IMPORTE*(COBERTURA_REPO)) SALDO_COBERTURA,SUM(IMPORTE) IMPORTE FROM MKT_DESEMBOLSO_REACTIVA
                    GROUP BY CODUNICOCLI
                    ) as MKT_R"),function($join){
						$join->on('WCV.COD_UNICO', '=', 'MKT_R.CODUNICOCLI');
                })
                ->select(DB::raw("WCV.COD_UNICO, A.NOMBRE_JEFE, WCV.NOMBRE, WCV.COD_SECTORISTA,
                (CASE WHEN 
                WCV.SALDO_INTERBANK IS NULL THEN ISNULL(WCV.SALDO_RIESGO,0) ELSE
                CASE WHEN WCV.SALDO_INTERBANK - MKT_R.SALDO_COBERTURA > 0 THEN WCV.SALDO_INTERBANK - MKT_R.SALDO_COBERTURA
                ELSE WCV.SALDO_INTERBANK END END)/3.5 SALDO_RIESGO, A.CODIGO_JEFE,SALDO_COBERTURA,SALDO_INTERBANK,
                CASE WHEN A.BANCA = 'BEL' AND A.ZONAL = 'BEL ZONAL 2' THEN 'MEDIANA EMPRESA' ELSE 
                CASE WHEN A.BANCA = 'BEL' AND A.ZONAL IN ('BEL ZONAL 3','BEL ZONAL 1') THEN 'GRAN EMPRESA' ELSE 
                CASE WHEN A.BANCA = 'BEP' THEN 'PROVINCIA' ELSE A.BANCA END END END  SEGMENTO, BUCK.BUCKET"))
                ->where('WCV.COD_UNICO','=',$codcliente)->first();
        return $sql;
    }

    static function getJefes($registro)
    {
        $ejecutivo= DB::Table('WEBVPC_USUARIO as USU')
                ->select('USU.REGISTRO','USU.NOMBRE','USU.ID_ZONA','USU.ID_CENTRO')
                ->where('REGISTRO','=',$registro)
                ->where('FLAG_ACTIVO','=',1)
                ->get()->first();

        $sql= DB::Table('WEBVPC_USUARIO as USU')
                ->select('USU.REGISTRO','USU.NOMBRE','USU.CORREO')
                ->where('ID_CENTRO','=',$ejecutivo->ID_CENTRO)
                ->where('ROL','=',24)
                ->where('FLAG_ACTIVO','=',1)
                ->get()->first();
        return $sql;
    }

    static function getCorreo($registro)
    {
        $sql= DB::Table('WEBVPC_USUARIO as USU')
                ->select('USU.REGISTRO','USU.NOMBRE','USU.CORREO')
                ->where('REGISTRO','=',$registro)
                ->where('FLAG_ACTIVO','=',1)
                ->get()->first();
        return $sql;
    }

    static function getDatosPolitica($politica_id)
    {
        $sql= DB::Table('WEBBE_INFINITY_POLITICA as WIP')
                ->where('POLITICA_ID','=',$politica_id)
                ->get()->first();
        return $sql;
    }

    static function getLineas($codunico)
    {
        $arraylineas=[];
        $sql= DB::table("WEBVPC_LINEAS_CLIENTE AS WIL")
                ->where('WIL.ESTADO','=','VIGENTE')
                ->where(DB::Raw("CONVERT(FLOAT,WIL.CU)"),'=',DB::Raw("CONVERT(FLOAT,".$codunico.")"))->get();
        if (!empty($sql)) {
            foreach ($sql as $key => $value) {
                array_push($arraylineas, $value->CODIGOLINEAOPERACION);
            }
            $lineas = implode(",", $arraylineas);
        }else{
            $lineas='';
        }
        return $lineas;
    }

    function eliminarPoliticas($codunico)
    {
        DB::beginTransaction();
        $status = true;
        try {

            DB::table('WEBBE_INFINITY_CLIENTE_POLITICA')
                ->where('COD_UNICO', '=', $codunico)
                ->update([
                    'FLG_VIGENTE' => 0,
                ]);
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }

        return $status;
    }

    static function getClienteBlindadoxPerformance($codunico)
    {
        $sql=DB::table(DB::Raw("(SELECT * FROM ( SELECT *,ROW_NUMBER() OVER (PARTITION BY COD_UNICO ORDER BY FECHA_FIN DESC) MAXIMO FROM WEBBE_INFINITY_CLIENTE_REVISION WHERE POLITICA_ID IN ('8','9','10') AND FLG_VIGENTE='1' )A WHERE A.MAXIMO=1) AS WICR"))->where('COD_UNICO','=',$codunico)->get()->first();
        return $sql;
    }

    static function datosForNotificationGys($codunico)
    {
        return DB::table(DB::Raw("(SELECT '10' ID_TIPO_NOTIFICACION, B.REGISTRO, REPLACE(C.CONTENIDO, '[VALOR_1]', A.NOMBRE) CONTENIDO, GETDATE() FECHA_NOTIFICACION,
                        0 FLG_LEIDO, A.NOMBRE VALOR_1, A.COD_UNICO VALOR_2, A.PERIODO VALOR_3, 'alertascartera/detalle?cu='+A.COD_UNICO URL
                FROM (SELECT NOMBRE, RTRIM(LTRIM(COD_SECTORISTA)) COD_SECTORISTA, COD_UNICO, PERIODO
                FROM WEBBE_INFINITY_CLIENTE WHERE COD_UNICO = '$codunico' AND PERIODO = (SELECT MAX(PERIODO) FROM WEBBE_INFINITY_CLIENTE)) A
                LEFT JOIN (
                    SELECT RTRIM(LTRIM(LOGIN)) REGISTRO, RTRIM(LTRIM(COD_SECT_LARGO)) COD_SECTORISTA FROM ARBOL_DIARIO_2
                ) B ON A.COD_SECTORISTA = B.COD_SECTORISTA
                LEFT JOIN (SELECT * FROM T_WEB_NOTIFICACIONES_CATALOGO WHERE ID_NOTIFICACION = 10) C ON 1=1) as A"))->get()->first();
    }

}