<?php
namespace App\Model\Infinity;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class ConocemeZona extends Model{

    function get($codunico){
	    $sql=DB::Table('WEBBE_INFINITY_CONOCEME_ZONA AS WIL')
	        ->select('COD_UNICO','TIPO','ZONA')
	        ->where('WIL.COD_UNICO',$codunico);
	    return $sql;
    }
}