<?php

namespace App\Model\Infinity;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class ConocemeMixVentas extends Model {

    function get($codunico) {
        $sql = DB::Table('WEBBE_INFINITY_MIX_VENTAS AS WIMV')
                ->select('COD_UNICO', 'PRODUCTO', 'PARTICIPACION')
                ->where('WIMV.COD_UNICO', $codunico)
                ->orderBy('PARTICIPACION', 'desc');
        return $sql;
    }

}
