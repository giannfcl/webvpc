<?php

namespace App\Model\Infinity;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class ConocemeClientePolitica extends Model {

	const VIGENTE = '1';

    function get($codunico) {
        $sql = DB::Table('WEBBE_INFINITY_CLIENTE_POLITICA AS WICP')
                ->join ('WEBBE_INFINITY_POLITICA AS WIP', function($join) {
                    $join->on('WICP.POLITICA_ID', '=', 'WIP.POLITICA_ID');
                })
                ->select('WICP.ID_POLITICA_CLIENTE','WICP.COD_UNICO','WIP.NOMBRE','WIP.POLITICA_ID','WIP.ORIGEN AS TIPO','WICP.ESTADO_FINAL_GESTION',DB::raw('CONVERT(VARCHAR(6),WICP.FECHA_CAIDA,112) PERIODO'))
                ->where('WICP.COD_UNICO', $codunico)
                ->where('WICP.FLG_VIGENTE','=',self::VIGENTE);
                
        return $sql;
    }

}
