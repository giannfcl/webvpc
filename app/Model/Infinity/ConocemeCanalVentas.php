<?php

namespace App\Model\Infinity;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class ConocemeCanalVentas extends Model {

    function get($codunico) {
        $sql = DB::Table('WEBBE_INFINITY_CANAL_VENTAS AS WICV')
                ->select('COD_UNICO', 'CANAL_VENTAS', 'PARTICIPACION')
                ->where('WICV.COD_UNICO', $codunico)
                ->orderBy('PARTICIPACION', 'desc');
        return $sql;
    }

}
