<?php
namespace App\Model\Infinity;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class ConocemeLinea extends Model{

    function get($codunico){
	    $sql=DB::Table('WEBBE_INFINITY_LINEAS AS WIL')
	        ->select('COD_UNICO','BANCO','LINEA','TIPO_GARANTIA')
	        ->where('WIL.COD_UNICO',$codunico);
	    return $sql;
    }
}