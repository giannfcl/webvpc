<?php

namespace App\Model\Infinity;

use DB;
use Log;
use App\Entity\Infinity\ConocemeStakeHolder as EntityConocemeStakeHolder;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Model;
use App\Entity\Infinity\Cliente as eCliente;
use App\Entity\Usuario as Usuario;
use Jenssegers\Date\Date as Carbon;
use App\Entity\Notification as eNotification;

class Visita extends Model {
    const ROL_VALIDADOR = '3';
    const LLENADOR_BLINDAJE = '2';
    const LLENADOR_SIN_BLINDAJE = '1';
    const SOLO_LECTURA = '0';
    const bajo = 'bajo';
	const medio = 'medio';
	const alto = 'alto';
    const FLG_CAMBIO_CONCENTRACION_PROVEEDORES = "2";
    const FLG_CAMBIO_GERENCIA_GENERAL = "3";
    const FLG_CAMBIO_ACCIONISTAS = "4";
    const FLG_CAMBIO_INVERSION_ACTIVO_PATRIMONIO = "6";
    const FLG_CAMBIO_PRESTAMO_DESVIO = "7";
    const FLG_CAMBIO_CONCENTRACION_VENTAS = "1";
    const FLG_CAMBIO_MODELO_NEGOCIO = "11";
    const FLG_IMPACTO_COVID = "25";
    const FLG_NO_OPERANDO_COVID = "26";
    const FLG_CAIDA_VENTAS_COVID = "27";

    public function getValidadorPOST($data=null,$registro,$rol,$privilegios){
        $cu = $data['codunico'];
        $alertas = [];
        $response = false;
        $datosCliente =  eCliente::getDatosClienteVISITAMECOVID($cu);
        // Proyección de ventas con caída del 50%​ COVID
        $proyec = isset($data['slcQ5_1'])?$data['slcQ5_1']:null;
        if($proyec==null?false:$proyec<=-50){
            array_push($alertas,self::FLG_CAIDA_VENTAS_COVID);
        }
        
        // Cambio en el nivel de calificación de la alerta a “Alto” COVID
        if(isset($data['slcQ13_1']) == 'bajo'){
            $cali=self::bajo;
        }
        else if (isset($data['slcQ13_2']) == 'medio'){
            $cali=self::medio;
        }
        else{
            $cali=self::alto;
            array_push($alertas,self::FLG_IMPACTO_COVID);
        }
        // Empresa que no opera COVID
        $opera = isset($data['slcQ2_1'])?$data['slcQ2_1']:null;
        if($opera=="2"){
            array_push($alertas,self::FLG_NO_OPERANDO_COVID);
        }
        // Cambio en el modelo de negocios​ VISITAME
        if(array_key_exists('flagCambioModelo',$data)){
            $flg = $data['flagCambioModelo'];
            if( $flg == "on"){
                array_push($alertas,self::FLG_CAMBIO_MODELO_NEGOCIO);
            }
        }
        // Cambio del accionariado​ VISITAME
        
        if(array_key_exists('flagCambioAccionistas',$data)){
            $flg = $data['flagCambioAccionistas'];
            if($flg == "on"){
                array_push($alertas,self::FLG_CAMBIO_ACCIONISTAS);
            }
        }
        
        // Inversiones mayor al 50% de Activo Fijo o del patrimonio ​VISITAME
        
        if(array_key_exists('flagCambioInversionActivoPatrimonio',$data)){
            $flg = $data['flagCambioInversionActivoPatrimonio'];
            if($flg == "on"){
                array_push($alertas,self::FLG_CAMBIO_INVERSION_ACTIVO_PATRIMONIO);
            }
        }
        
        // Cambio del principal proveedor VISITAME
        
        if(array_key_exists('flagCambioConcentracionProveedores',$data)){
            $flg = $data['flagCambioConcentracionProveedores'];
            if($flg == "on"){
                array_push($alertas,self::FLG_CAMBIO_CONCENTRACION_PROVEEDORES);
            }
        }
        
         // Cambio del principal cliente VISITAME
         
         if(array_key_exists('flagCambioConcentracionVentas',$data)){
            $flg = $data['flagCambioConcentracionVentas'];
            if($flg == "on"){
                array_push($alertas,self::FLG_CAMBIO_CONCENTRACION_VENTAS);
            }
        }
        
        // Cambio de gerencia general de la empresa VISITAME
        
        if(array_key_exists('flagCambioGerenciaGeneral',$data)){
            $flg = $data['flagCambioGerenciaGeneral'];
            if($flg == "on"){
                array_push($alertas,self::FLG_CAMBIO_GERENCIA_GENERAL);
            }
        }
        
        // Préstamos a accionistas o vinculados de por los menos el 15% del activo en el último ejercicio
        
        if(array_key_exists('flagCambioPrestamoDesvio',$data)){
            $flg = $data['flagCambioPrestamoDesvio'];
            if($flg == "on"){
                array_push($alertas,self::FLG_CAMBIO_PRESTAMO_DESVIO);
            }
        }
        switch ($datosCliente->SEGMENTO) {
            case 'MEDIANA EMPRESA':
                switch ($privilegios) {
                    case self::SOLO_LECTURA:
                    break;
                    case self::LLENADOR_SIN_BLINDAJE:
                    break;
                    //No es rol Validador
                    case self::LLENADOR_BLINDAJE:
                        $response = count($alertas)==0;
                    break;
                    case self::ROL_VALIDADOR:
                        if(count($alertas)>0){
                            //Hubo una alerta cualitativa por lo que el Jefe zonal no puede aprobar
                            $response = $rol!= Usuario::ROL_JEFATURA_BE;
                        }else{
                            $response = true;
                        }
                    break;
                    default:
                    break;
                }
            case 'GRAN EMPRESA':
                switch ($privilegios) {
                    case self::SOLO_LECTURA:
                        break;
                    case self::LLENADOR_SIN_BLINDAJE:
                    break;
                    case self::LLENADOR_BLINDAJE:
                    break;
                    case self::ROL_VALIDADOR:
                        $response = true;
                    break;
                    default:
                    break;
                }
            break;
            case 'PROVINCIA':
                switch ($privilegios) {
                    case self::SOLO_LECTURA:
                        break;
                    case self::LLENADOR_SIN_BLINDAJE:
                    break;
                    case self::LLENADOR_BLINDAJE:
                    break;
                    case self::ROL_VALIDADOR:
                        $response = true;
                    break;
                    default:
                    break;
                }
            break;
            default:
            break;
        }
        return array("response"=>$response,"alertas"=>$alertas);
    }
    public function getValidadores($registro,$cu,$datoscovid){
            $res= null;
            $bucket=0;
            if(isset($cu)){
                $datosCliente =  eCliente::getDatosClienteVISITAMECOVID($cu);
                $bucket = $datosCliente->BUCKET;
                switch ($datosCliente->SEGMENTO) {
                    case 'MEDIANA EMPRESA':
                        if($bucket==1){
                            $res = DB::table('WEBVPC_USUARIO')
                                ->select('REGISTRO','NOMBRE','ROL')
                                ->where('FLAG_ACTIVO','=','1')
                                ->whereIn('ROL',array(
                                Usuario::ROL_EJECUTIVO_SEGUIMIENTO_CARTERA,
                                Usuario::ROL_ANALISTA_GYS,
                                Usuario::ROL_SUBGERENTE_GYS,
                                Usuario::ROL_JEFE_RIESGOS,
                                Usuario::ROL_GERENTE_RIESGOS,
                                Usuario::ROL_SUBGERENTE_RIESGOS))
                                ->orWhere(function($query){
                                    $query  ->where('ROL',Usuario::ROL_ANALISTA_RIESGOS_ADMISION)
                                            ->where('ID_ZONA','BELZONAL2')
                                            ->where('FLAG_ACTIVO','1');
                                })
                                ->orWhere(function($query){
                                    $query  ->where('ROL',Usuario::ROL_GERENCIA_ZONAL_BE)
                                            ->where('ID_GC','BEL ZONAL 2')
                                            ->where('FLAG_ACTIVO','1');
                                })
                                ->orWhere(function($query ) use ($datosCliente){
                                    $query  ->where('ROL',Usuario::ROL_JEFATURA_BE)
                                            ->where('ID_GC',$datosCliente->CODIGO_JEFE)
                                            ->where('FLAG_ACTIVO','1');
                                })->orderBy('NOMBRE')->get();
                        }else if($bucket==2 ){
                            $res = DB::table('WEBVPC_USUARIO')->select('REGISTRO','NOMBRE','ROL')
                                ->where('FLAG_ACTIVO','=','1')
                                ->whereIn('ROL',array(
                                    Usuario::ROL_ANALISTA_GYS,
                                    Usuario::ROL_EJECUTIVO_SEGUIMIENTO_CARTERA,
                                    Usuario::ROL_SUBGERENTE_GYS,
                                    Usuario::ROL_JEFE_RIESGOS,
                                    Usuario::ROL_GERENTE_RIESGOS,
                                    Usuario::ROL_SUBGERENTE_RIESGOS))
                                ->orWhere(function($query){
                                    $query  ->where('ROL',Usuario::ROL_ANALISTA_RIESGOS_ADMISION)
                                            ->where('ID_ZONA','BELZONAL2')
                                            ->where('FLAG_ACTIVO','1');
                                })
                                ->orderBy('NOMBRE')->get();
                        }else if($bucket==3 ){
                            $res = DB::table('WEBVPC_USUARIO')->select('REGISTRO','NOMBRE','ROL')
                                ->where('FLAG_ACTIVO','=','1')
                                ->whereIn('ROL',array(
                                    Usuario::ROL_JEFE_RIESGOS,
                                    Usuario::ROL_SUBGERENTE_GYS,
                                    Usuario::ROL_GERENTE_RIESGOS,
                                    Usuario::ROL_SUBGERENTE_RIESGOS))->orderBy('NOMBRE')->get();
                        }else if($bucket==4){
                            $res = DB::table('WEBVPC_USUARIO')->select('REGISTRO','NOMBRE','ROL')
                                    ->where('FLAG_ACTIVO','=','1')
                                    ->whereIn('ROL',array(
                                        Usuario::ROL_GERENTE_RIESGOS,
                                        Usuario::ROL_JEFE_RIESGOS,
                                        Usuario::ROL_SUBGERENTE_GYS,
                                        Usuario::ROL_SUBGERENTE_RIESGOS))->orderBy('NOMBRE')->get();
                        }
                    break;
                    case 'GRAN EMPRESA':
                        $res = DB::table('WEBVPC_USUARIO')->select('REGISTRO','NOMBRE','ROL')
                                ->where('FLAG_ACTIVO','=','1')
                                ->whereIn('ROL',array(
                                    Usuario::ROL_ANALISTA_GYS,
                                    Usuario::ROL_SUBGERENTE_GYS,
                                    Usuario::ROL_ANALISTA_RIESGOS_ADMISION,
                                    Usuario::ROL_JEFE_RIESGOS,
                                    Usuario::ROL_GERENTE_RIESGOS,
                                    Usuario::ROL_SUBGERENTE_RIESGOS,
                                    Usuario::ROL_GERENCIA_ZONAL_BE,
                                    Usuario::ROL_GERENTE_BANCA,
                                    Usuario::ROL_JEFE_ESCOM,
                                    Usuario::ROL_JEFATURA_BE))->orderBy('NOMBRE')->get();
                    break;
                    case 'PROVINCIA':
                        $res = DB::table('WEBVPC_USUARIO')->select('REGISTRO','NOMBRE','ROL')
                                ->where('FLAG_ACTIVO','=','1')
                                ->whereIn('ROL',array(
                                    Usuario::ROL_ANALISTA_GYS,
                                    Usuario::ROL_SUBGERENTE_GYS,
                                    Usuario::ROL_ANALISTA_RIESGOS_ADMISION,
                                    Usuario::ROL_JEFE_RIESGOS,
                                    Usuario::ROL_GERENTE_RIESGOS,
                                    Usuario::ROL_SUBGERENTE_RIESGOS,
                                    Usuario::ROL_GERENCIA_ZONAL_BE,
                                    Usuario::ROL_GERENTE_BANCA,
                                    Usuario::ROL_JEFE_ESCOM,
                                    Usuario::ROL_JEFATURA_BE))->orderBy('NOMBRE')->get();
                    break;
                    default:
                    break;
                }
            }
        return array("validadores"=>$res==null?[]:$res,"segmento"=>($datosCliente->SEGMENTO),"bucket"=>$bucket);
    }
    public function getAreasRevision($rol){
        $revision=[];
        if(in_array($rol,array(
            Usuario::ROL_EJECUTIVO_FARMER_BE,
            Usuario::ROL_EJECUTIVO_HUNTER_BE,
            Usuario::ROL_ANALISTA_NEGOCIO_ZONAL_BE,
            Usuario::ROL_JEFATURA_BE,
        ))){
            //Solo pueden añadir compromisos comerciales
            array_push($revision,'Comercial');
        }else if(in_array($rol,array(
            Usuario::ROL_JEFE_RIESGOS,
            Usuario::ROL_ANALISTA_RIESGOS_ADMISION,
            Usuario::ROL_GERENTE_RIESGOS,
            Usuario::ROL_SUBGERENTE_RIESGOS
        ))){
            //Añaden Comercial, Seguimiento comercial y de Admisión
            array_push($revision,'Comercial');
            array_push($revision,'Seguimiento comercial');
            array_push($revision,'Admisión');
        }else if(in_array($rol,array(
                Usuario::ROL_EJECUTIVO_SEGUIMIENTO_CARTERA,
                Usuario::ROL_GERENCIA_ZONAL_BE,
                Usuario::ROL_GERENTE_BANCA
            ))){
            //Añaden Comercial y Seguimiento comercial
            array_push($revision,'Comercial');
            array_push($revision,'Seguimiento comercial');
        }else if(in_array($rol,array(
            Usuario::ROL_ANALISTA_GYS,
            Usuario::ROL_SUBGERENTE_GYS
        ))){
            //Añaden Comercial, Seguimiento comercial y de GYS
            array_push($revision,'Comercial');
            array_push($revision,'Seguimiento comercial');
            array_push($revision,'GYS');
        }
        return $revision;
    }
    public function getValidador($cu,$registro,$rol,$datoscovid){
        
        $privilegios=self::SOLO_LECTURA;
        $objValidadores=$this->getValidadores($registro,$cu,$datoscovid);
        $validadores=json_decode(json_encode( $objValidadores['validadores'],true),true);
        $segmento= $objValidadores['segmento'];
        $roles = array_filter($validadores,function($e) use($rol){
          return  $e['ROL']==$rol;
        });
        $rolValido = count($roles)>0;
        switch ($segmento) {
            case 'MEDIANA EMPRESA':
                if($rolValido){
                    $privilegios = self::ROL_VALIDADOR;
                }else{
                    if(in_array($rol,array(
                        Usuario::ROL_EJECUTIVO_FARMER_BE,
                        Usuario::ROL_EJECUTIVO_HUNTER_BE,
                        Usuario::ROL_ANALISTA_NEGOCIO_ZONAL_BE))){
                        //Blindaje
                        $blindaje = false;
                        if(!empty($datoscovid)){
                            $blindajeBruto = $datoscovid->PREGUNTA_14_4;
                            if($blindajeBruto=="1"){
                                $meses = $datoscovid->PREGUNTA_14_5;
                                $now = Carbon::now();
                                $fechallenado =  new Carbon($datoscovid->FECHA_CREACION);
                                $diff_in_months = $now->diffInMonths($fechallenado);
                                $blindaje = $diff_in_months<=$meses;
                            }
                        }
                        if($blindaje){
                            $privilegios = self::LLENADOR_BLINDAJE;
                        }else{
                            $privilegios = self::LLENADOR_SIN_BLINDAJE;
                        }
                    }
                }
            break;
            case 'GRAN EMPRESA':
                if($rolValido){
                    $privilegios = self::ROL_VALIDADOR;
                }else{
                    $necesitanValidacion = in_array($rol,array(Usuario::ROL_EN_ESCOM,Usuario::ROL_EJECUTIVO_FARMER_BE,Usuario::ROL_EJECUTIVO_HUNTER_BE,Usuario::ROL_ANALISTA_NEGOCIO_ZONAL_BE));
                    if($necesitanValidacion){
                        $privilegios = self::LLENADOR_SIN_BLINDAJE;
                    }
                }
            break;
            case 'PROVINCIA':
                if($rolValido){
                    $privilegios = self::ROL_VALIDADOR;
                }else{
                    $necesitanValidacion = in_array($rol,array(Usuario::ROL_EN_ESCOM,Usuario::ROL_EJECUTIVO_FARMER_BE,Usuario::ROL_EJECUTIVO_HUNTER_BE,Usuario::ROL_ANALISTA_NEGOCIO_ZONAL_BE));
                    if($necesitanValidacion){
                        $privilegios = self::LLENADOR_SIN_BLINDAJE;
                    }
                }
            break;
        }
        return $privilegios;
    }
    public function getUltimaVisitaCliente($codUnico) {
        $sql = DB::table(DB::raw("(SELECT * FROM (SELECT *, ROW_NUMBER() OVER (PARTITION BY COD_UNICO ORDER BY FECHA_VISITA DESC) ULTIMO FROM WEBBE_INFINITY_VISITA) A WHERE ULTIMO=1) AS VISITAS"))
                ->where('COD_UNICO','=',$codUnico)
                ->get();
        return (!empty($sql) ? $sql : null);
    }

    public function getVisitas($codUnico) {
        $sql = DB::table('WEBBE_INFINITY_VISITA AS WIV')
                ->select('WIV.*', 'WU.NOMBRE AS NOMBRE_USUARIO', DB::Raw('CONVERT(DATE,WIV.FECHA_VISITA) AS FECHA_VISITA_DATE'))
                ->leftJoin('WEBVPC_USUARIO AS WU', function($join) {
                    $join->on('WU.REGISTRO', '=', 'WIV.REGISTRO');
                })
                ->where('WIV.COD_UNICO', '=', $codUnico);
        return $sql;
    }

    public function getUltimaVisita($codUnico) {
        $sql = $this->getVisitas($codUnico);
        $sql->orderBy('FECHA_VISITA', 'desc')
            ->orderBy('FECHA_REGISTRO','desc');
        return $sql;
    }

    public function getbyId($idVisita) {
        $sql = $this->getVisitas($idVisita);
        $sql->wheres = [];
        $sql = $sql
        ->where('WIV.ID', $idVisita);
        return $sql;
    }

        
    function registrar($infoVisita, $registrar, $cliente,$objCovid,$alertas,$nombreUsuario,$notificar) {
        DB::beginTransaction();
        $result=null;
        try {
            //Insert en la tabla visita
            $id = DB::table('WEBBE_INFINITY_VISITA')->insertGetId($infoVisita);


            //En $cliente vienen arrays, asi que lo sacamos para el insert
            $data = array_filter($cliente, function($k) {
                return !is_array($k);
            });

            /* Inser para la tabla historica conoceme */
            $hist = $data;
            $hist['VISITA_ID'] = $id;
            $hist['TIPO'] = 'VISITA';
            $hist['FECHA_ACTUALIZACION']=$infoVisita['FECHA_VISITA'];
            //$hist['FECHA_ACTUALIZACION'] = $infoVisita;
            $historiaId = DB::table('WEBBE_INFINITY_CONOCEME_HIST')->insertGetId($hist);

            /*STAKEHOLDER*/
            foreach ($cliente['stakeholders'] as &$stakeholder) {
                // print_r($stakeholder);
                $stakeholder['CONOCEME_HIST_ID'] = $historiaId;
                if (!empty($stakeholder['CONTRATO_ADJUNTO'])) {
                    if (isset($stakeholder['CONTRATO_ADJUNTO']['adjuntodeProveedores'])) {
                        $stakeholder['CONTRATO_ADJUNTO']['adjuntodeProveedores']->storeAs('infinity/stakeholders/proveedores/'.$stakeholder['COD_UNICO'].'/'.$stakeholder['NOMBRE'].'/', $stakeholder['CONTRATO_ADJUNTO']['adjuntodeProveedores']->getClientOriginalName());
                        $stakeholder['CONTRATO_ADJUNTO']=$stakeholder['CONTRATO_ADJUNTO']['adjuntodeProveedores']->getClientOriginalName();
                    }elseif (isset($stakeholder['CONTRATO_ADJUNTO']['adjuntodeClientes']))
                    {
                        $stakeholder['CONTRATO_ADJUNTO']['adjuntodeClientes']->storeAs('infinity/stakeholders/clientes/'.$stakeholder['COD_UNICO'].'/'.$stakeholder['NOMBRE'].'/', $stakeholder['CONTRATO_ADJUNTO']['adjuntodeClientes']->getClientOriginalName());
                        $stakeholder['CONTRATO_ADJUNTO']=$stakeholder['CONTRATO_ADJUNTO']['adjuntodeClientes']->getClientOriginalName();
                    }
                }
            }
            DB::table('WEBBE_INFINITY_CONOCEME_STAKEHOLDER_HIST')->insert($cliente['stakeholders']);
            unset($stakeholder);
            
            /*LINEAS*/
            foreach ($cliente['lineas'] as &$linea) {
                $linea['CONOCEME_HIST_ID'] = $historiaId;
            }
            DB::table('WEBBE_INFINITY_LINEAS_HIST')->insert($cliente['lineas']);
            unset($linea);

            /*ZONAS*/
            foreach ($cliente['zonas'] as &$zona) {
                $zona['CONOCEME_HIST_ID'] = $historiaId;
            }
            DB::table('WEBBE_INFINITY_CONOCEME_ZONA_HIST')->insert($cliente['zonas']);
            unset($zona);

            
            /*COMMODITIES*/
            foreach ($cliente['commodities'] as &$com) {
                $com['CONOCEME_HIST_ID'] = $historiaId;
            }
            DB::table('WEBBE_INFINITY_CONOCEME_COMMODITIE_HIST')->insert($cliente['commodities']);
            unset($com);

            /*MIXVENTAS*/   
            foreach ($cliente['mix'] as &$mix) {
                $mix['CONOCEME_HIST_ID'] = $historiaId;
            }
            DB::table('WEBBE_INFINITY_MIX_VENTAS_HIST')->insert($cliente['mix']);
            unset($mix);

            /*INVERSIONES*/   
            foreach ($cliente['inversiones'] as &$inv) {
                $inv['CONOCEME_HIST_ID'] = $historiaId;
            }
            DB::table('WEBBE_INFINITY_INVERSIONES_HIST')->insert($cliente['inversiones']);
            unset($inv);

            /*CANALVENTAS*/   
            foreach ($cliente['canales'] as &$can) {
                $can['CONOCEME_HIST_ID'] = $historiaId;
            }
            DB::table('WEBBE_INFINITY_CANAL_VENTAS_HIST')->insert($cliente['canales']);
            ////////////////////////////// PARTE FICHA COVID //////////////////////////////////////////
            $data = $objCovid['covid'];
            $data['ID_VISITA'] = $id;
            $cumplimiento = $objCovid['cumplimiento'];
            $idCovid = DB::table('WEBVPC_COVID19')->insertGetId($data);
            if (count($cumplimiento)>0) {
                $array = range(0,count($cumplimiento)-1);
                foreach ($array as $i) {
                        $cumplimiento[$i]['ID_COVID'] = $idCovid;
                        $cumplimiento[$i]['ID_VISITA'] = $id;
                        $cumplimiento[$i]['STATUS_COMPROMISO'] = 'NO CONFIRMADO'; 
                        DB::table('WEBVPC_COVID19_CUMPLIMIENTO')->insert($cumplimiento[$i]);
                }
            }
            if($notificar){
                ////////////////////////////// PARTE DE NOTIFICACIONES - LLENADO DE FICHA ////////////////////////////////////
                $cu = $data['COD_UNICO'];
                $datosCliente =  eCliente::getDatosClienteVISITAMECOVID($cu);
                $notification = new eNotification();
                $notification->setValue('_idTipo', '11');
                $notification->setValue('_registro', $data['USUARIO_NOTIFICADO']);
                $notification->setValue('_contenido', "Tienes una ficha pendiente de aprobación del cliente " . strtoupper($datosCliente->NOMBRE) . ", ".$cu." ingresada por ".$nombreUsuario);
                $notification->setValue('_fechaNotificacion', Carbon::now());
                $notification->setValue('_flgLeido', '0');
                $notification->setValue('_fechaLeido', null);
                $notification->setValue('_valor1', null);
                $notification->setValue('_valor2', null);
                $notification->setValue('_valor3', null);
                $notification->setValue('_valor4', null);
                $notification->setValue('_url', 'infinity/me/cliente/visita?cu=' . $cu.'&flg_notificado=1');
                $notification = $notification->setValueToTable();
                DB::table('T_WEB_NOTIFICACIONES')->insert($notification);
                ////////////////////////////// PARTE DE NOTIFICACIONES - ALERTAS CUALITATIVAS /////////////////////////////////

                foreach($alertas as $alerta){
                    $alertaObj=DB::table('WEBBE_INFINITY_POLITICA')->select('NOMBRE')->where('POLITICA_ID',$alerta)->first();
                    $notification = new eNotification();
                    $notification->setValue('_idTipo', '13');
                    $notification->setValue('_registro', $data['USUARIO_NOTIFICADO']);
                    $notification->setValue('_contenido', 
                    "La ficha ingresada del cliente " .strtoupper($datosCliente->NOMBRE). ", " .$cu. " por ".$nombreUsuario." presenta una alerta cualitativa (".strtoupper($alertaObj->NOMBRE)."), favor de coordinar y revisar la ficha.");
                    $notification->setValue('_fechaNotificacion', Carbon::now());
                    $notification->setValue('_flgLeido', '0');
                    $notification->setValue('_fechaLeido', null);
                    $notification->setValue('_valor1', null);
                    $notification->setValue('_valor2', null);
                    $notification->setValue('_valor3', null);
                    $notification->setValue('_valor4', null);
                    $notification->setValue('_url', 'infinity/me/cliente/visita?cu=' . $cu.'&flg_notificado=1');
                    $notification = $notification->setValueToTable();
                    DB::table('T_WEB_NOTIFICACIONES')->insert($notification);
                    // if($data['USUARIO_GYS']!=null){
                    //     $notificationULT->setValue('_registro', $data['USUARIO_GYS']);
                    //     $notification = $notificationULT->setValueToTable();
                    //     DB::table('T_WEB_NOTIFICACIONES')->insert($notification);
                    // }
                    // if($data['USUARIO_ADMISION']!=null){
                    //     $notificationULT2->setValue('_registro', $data['USUARIO_ADMISION']);
                    //     $notification = $notificationULT2->setValueToTable();
                    //     DB::table('T_WEB_NOTIFICACIONES')->insert($notification);
                    // }
                    //Queda registro de la alerta cualitativa
                    DB::table('WEBVPC_ALERTAS_CUALITATIVAS_HIST')->insert(
                        ['COD_UNICO'=>$cu,
                        'ID_VISITA'=>$id,
                        'POLITICA_ID'=>$alerta]
                    );
                }
            }
            
            $result = array($id,$historiaId);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            dd($e->getMessage());
            $result = null;
            DB::rollback();
        }
        return $result;
    }
    function confirmar($periodo,$infoVisita, $cliente, $idVisita,$historiaId,$objCovid,$alertas,$nombreUsuario) {
      
        DB::beginTransaction();
        $status = true;
        try {
            //Insert en la tabla visita
            DB::table('WEBBE_INFINITY_VISITA')->where('ID', $idVisita)->update(array_except($infoVisita, ['ID']));

            //En $cliente vienen arrays, asi que lo sacamos para el insert
            $data = array_filter($cliente, function($k) {
                return !is_array($k);
            });
            
            $data['FECHA_ACTUALIZACION']=$infoVisita['FECHA_VISITA'];//dd($data);

            /* ACTUALIZANDO LAS TABLAS CONOCEME E HISTORIA */
            DB::table('WEBBE_INFINITY_CONOCEME')->where('COD_UNICO', $cliente['COD_UNICO'])->update(array_except($data, ['COD_UNICO']));
            DB::table('WEBBE_INFINITY_CONOCEME_HIST')->where('ID', $historiaId)->update(array_except($data, ['COD_UNICO']));

            /* INSERTANDO STAKEHOLDERS EN CONOCEME Y EN HISTORIA */
            // print_r($cliente['stakeholders']);
            foreach ($cliente['stakeholders'] as &$stakeholder) {
                $stakeholder['CONOCEME_HIST_ID'] = $historiaId;
                if (!empty($stakeholder['CONTRATO_ADJUNTO'])) {
                    if (isset($stakeholder['CONTRATO_ADJUNTO']['adjuntodeProveedores'])) {
                        $stakeholder['CONTRATO_ADJUNTO']['adjuntodeProveedores']->storeAs('infinity/stakeholders/proveedores/'.$stakeholder['COD_UNICO'].'/'.$stakeholder['NOMBRE'].'/', $stakeholder['CONTRATO_ADJUNTO']['adjuntodeProveedores']->getClientOriginalName());
                        $stakeholder['CONTRATO_ADJUNTO']=$stakeholder['CONTRATO_ADJUNTO']['adjuntodeProveedores']->getClientOriginalName();
                    }elseif (isset($stakeholder['CONTRATO_ADJUNTO']['adjuntodeClientes']))
                    {
                        $stakeholder['CONTRATO_ADJUNTO']['adjuntodeClientes']->storeAs('infinity/stakeholders/clientes/'.$stakeholder['COD_UNICO'].'/'.$stakeholder['NOMBRE'].'/', $stakeholder['CONTRATO_ADJUNTO']['adjuntodeClientes']->getClientOriginalName());
                        $stakeholder['CONTRATO_ADJUNTO']=$stakeholder['CONTRATO_ADJUNTO']['adjuntodeClientes']->getClientOriginalName();
                    }
                }elseif (!empty(EntityConocemeStakeHolder::getConocemeStakeHolder($historiaId,$stakeholder['NOMBRE'],$stakeholder['TIPO'])))
                {
                    $stakeholder['CONTRATO_ADJUNTO']=EntityConocemeStakeHolder::getConocemeStakeHolder($historiaId,$stakeholder['NOMBRE'],$stakeholder['TIPO'])->CONTRATO_ADJUNTO;
                }
            }
            DB::table('WEBBE_INFINITY_CONOCEME_STAKEHOLDER')->where('COD_UNICO', $cliente['COD_UNICO'])->delete();
            DB::table('WEBBE_INFINITY_CONOCEME_STAKEHOLDER_HIST')->where('CONOCEME_HIST_ID', $historiaId)->delete();
            DB::table('WEBBE_INFINITY_CONOCEME_STAKEHOLDER_HIST')->insert($cliente['stakeholders']);

            foreach ($cliente['stakeholders'] as &$stakeholder) {
                unset($stakeholder['CONOCEME_HIST_ID']);
            }

            DB::table('WEBBE_INFINITY_CONOCEME_STAKEHOLDER')->insert($cliente['stakeholders']);
            // print_r($cliente['stakeholders']);die();
            unset($stakeholder);
            

            /* LINEAS */
            DB::table('WEBBE_INFINITY_LINEAS')->where('COD_UNICO', $cliente['COD_UNICO'])->delete();
            DB::table('WEBBE_INFINITY_LINEAS_HIST')->where('CONOCEME_HIST_ID', $historiaId)->delete();
            DB::table('WEBBE_INFINITY_LINEAS')->insert($cliente['lineas']);
            if (!empty($cliente['lineas'])) {
                foreach ($cliente['lineas'] as &$linea) {
                    $linea['CONOCEME_HIST_ID'] = $historiaId;
                }
            }
            DB::table('WEBBE_INFINITY_LINEAS_HIST')->insert($cliente['lineas']);
            if (isset($linea)) {
                unset($linea);
            }

            /* ZONAS */
            DB::table('WEBBE_INFINITY_CONOCEME_ZONA')->where('COD_UNICO', $cliente['COD_UNICO'])->delete();
            DB::table('WEBBE_INFINITY_CONOCEME_ZONA_HIST')->where('CONOCEME_HIST_ID', $historiaId)->delete();
            DB::table('WEBBE_INFINITY_CONOCEME_ZONA')->insert($cliente['zonas']);
            if (!empty($cliente['zonas'])) {
                foreach ($cliente['zonas'] as &$zona) {
                    $zona['CONOCEME_HIST_ID'] = $historiaId;
                }
            }
            DB::table('WEBBE_INFINITY_CONOCEME_ZONA_HIST')->insert($cliente['zonas']);
            if (isset($zona)) {
                unset($zona);
            }


            /* COMMODITIES */
            DB::table('WEBBE_INFINITY_CONOCEME_COMMODITIE')->where('COD_UNICO', $cliente['COD_UNICO'])->delete();
            DB::table('WEBBE_INFINITY_CONOCEME_COMMODITIE_HIST')->where('CONOCEME_HIST_ID', $historiaId)->delete();
            DB::table('WEBBE_INFINITY_CONOCEME_COMMODITIE')->insert($cliente['commodities']);
            if (!empty($cliente['commodities'])) {
                foreach ($cliente['commodities'] as &$com) {
                    $com['CONOCEME_HIST_ID'] = $historiaId;
                }
            }
            DB::table('WEBBE_INFINITY_CONOCEME_COMMODITIE_HIST')->insert($cliente['commodities']);
            if (isset($com)) {
                unset($com);
            }

            /* MIXVENTAS */
                DB::table('WEBBE_INFINITY_MIX_VENTAS')->where('COD_UNICO', $cliente['COD_UNICO'])->delete();
                DB::table('WEBBE_INFINITY_MIX_VENTAS_HIST')->where('CONOCEME_HIST_ID', $historiaId)->delete();
                DB::table('WEBBE_INFINITY_MIX_VENTAS')->insert($cliente['mix']);
            if (!empty($cliente['mix'])) {
                foreach ($cliente['mix'] as &$mix) {
                    $mix['CONOCEME_HIST_ID'] = $historiaId;
                }
            }
            DB::table('WEBBE_INFINITY_MIX_VENTAS_HIST')->insert($cliente['mix']);
            if (isset($mix)) {
                unset($mix);
            }

            /* INVERSIONES */
                DB::table('WEBBE_INFINITY_INVERSIONES')->where('COD_UNICO', $cliente['COD_UNICO'])->delete();
                DB::table('WEBBE_INFINITY_INVERSIONES_HIST')->where('CONOCEME_HIST_ID', $historiaId)->delete();
                DB::table('WEBBE_INFINITY_INVERSIONES')->insert($cliente['inversiones']);
            if (!empty($cliente['inversiones'])) {
                foreach ($cliente['inversiones'] as &$inv) {
                    $inv['CONOCEME_HIST_ID'] = $historiaId;
                }
            }
            DB::table('WEBBE_INFINITY_INVERSIONES_HIST')->insert($cliente['inversiones']);
            if (isset($inv)) {
                unset($inv);
            }

            /* CANALVENTAS */
            DB::table('WEBBE_INFINITY_CANAL_VENTAS')->where('COD_UNICO', $cliente['COD_UNICO'])->delete();
            DB::table('WEBBE_INFINITY_CANAL_VENTAS_HIST')->where('CONOCEME_HIST_ID', $historiaId)->delete();
            DB::table('WEBBE_INFINITY_CANAL_VENTAS')->insert($cliente['canales']);
            if (!empty($cliente['canales'])) {
                foreach ($cliente['canales'] as &$can) {
                    $can['CONOCEME_HIST_ID'] = $historiaId;
                }
            }
            DB::table('WEBBE_INFINITY_CANAL_VENTAS_HIST')->insert($cliente['canales']);
            if (isset($can)) {
                unset($can);
            }
            
            DB::table('WEBBE_INFINITY_CLIENTE')
                    ->where('PERIODO',$periodo)
                    ->where('COD_UNICO',$cliente['COD_UNICO'])
                    ->update(['FECHA_ULTIMA_VISITA'=>$infoVisita['FECHA_VISITA']]);

            ////////////////////////////// PARTE FICHA COVID //////////////////////////////////////////
            $data = $objCovid['covid'];
            $cumplimiento = $objCovid['cumplimiento'];
            //ACTUALIZAR FICHA COVID
            DB::table('WEBVPC_COVID19')->where([
                'COD_UNICO'=>$data['COD_UNICO'],
                'ID_VISITA'=>$data['ID_VISITA']
            ])->update($data); 
            //OBTENER ID_COVID
            $idCovid = DB::table('WEBVPC_COVID19')->where([
                'COD_UNICO'=>$data['COD_UNICO'],
                'ID_VISITA'=>$data['ID_VISITA']
            ])->select('ID')->first();
            //INSERTAR NUEVOS CUMPLIMIENTOS
            if (count($cumplimiento)>0) {
                $array = range(0,count($cumplimiento)-1);
                foreach ($array as $i) {
                    $cumplimiento[$i]['ID_COVID'] = $idCovid->ID;
                    DB::table('WEBVPC_COVID19_CUMPLIMIENTO')->insert($cumplimiento[$i]);
                }
            }
            ////////////////////// CONFIRMAR ALERTAS CUALITATIVAS //////////////////////////////
            // BORRAR ANTERIORES ALERTAS CUALITATIVAS ANTERIORES (NO VALIDADAS)
            DB::table('WEBVPC_ALERTAS_CUALITATIVAS_HIST')
                ->where(
                    ['COD_UNICO'=>$data['COD_UNICO'],'ID_VISITA'=>$data['ID_VISITA']]
                )->delete();
            // CREAR ALERTAS CUALITATIVAS NUEVAS (VALIDADAS)
            foreach($alertas as $alerta){
                DB::table('WEBVPC_ALERTAS_CUALITATIVAS_HIST')->insert(  
                ['COD_UNICO'=>$data['COD_UNICO'],
                'ID_VISITA'=>$data['ID_VISITA'],
                'POLITICA_ID'=>$alerta]);
            }
            //////////////////////////////////////////////////////////////////////////////////
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }
}
