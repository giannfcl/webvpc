<?php

namespace App\Model\Infinity;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;


class NotificacionInfinity extends Model {

    static function getClientesCorreos($periodo,$filtros){
        $sql=DB::table("WEBBE_INFINITY_CLIENTE AS WIC")
                ->select(
                    'WIC.COD_UNICO', 'WIC.NUM_DOC', 'WIC.NOMBRE',
                    'WIC.METODIZADO_1_FECHA AS FECHA_ULTIMO_EEFF1','WIC.METODIZADO_2_FECHA AS FECHA_ULTIMO_EEFF2',
                    'WIC.FECHA_ULTIMA_VISITA', 'WIC.FECHA_ULTIMO_IBR', 'WIC.FECHA_ULTIMO_F02',
                    'WIA.NIVEL_ALERTA', 'WIA.FLG_GESTIONADO', 'WIA.FLAG_GESTIONABLE', 'WIA.FECHA_ULTIMA_GESTION',
                    'WCB.ZONAL', 'WCB.JEFATURA', 'WCB.REGISTRO AS REGISTRO_EN', 'WCB.NOMBRE_EN', 'WCB.CORREO AS EMAIL_EN',
                    'WCB.REGISTRO_JEFATURA', 'WCB.NOMBRE_JEFATURA', 'WCB.EMAIL_JEFATURA_ZONAL',
                    'WCB.REGISTRO_GERENCIA_ZONAL AS REGISTRO_GERENCIA', 'WCB.NOMBRE_GERENCIA', 'WCB.EMAIL_GERENCIA_ZONAL'
                )
                ->join('WEBBE_INFINITY_ALERTA AS WIA', function($join) {
                    $join->on('WIC.COD_UNICO', '=', 'WIA.COD_UNICO');
                    $join->on('WIC.PERIODO', '=', 'WIA.PERIODO');
                })
                ->join('ARBOL_DIARIO_2 AS WLS', function($join) {
                    $join->on('WLS.COD_SECT_LARGO', '=', 'WIC.COD_SECTORISTA');
                })               
                ->join('WEBVPC_CORREOS_BEL AS WCB',function($join){
                    $join->on('WLS.LOGIN','=','WCB.REGISTRO');
                })
                ->where('WIC.PERIODO','=',$periodo)
                ->orderBy('WCB.REGISTRO','WCB.REGISTRO_JEFATURA','WCB.REGISTRO_GERENCIA_ZONAL');

        if (isset($filtros['flgInfinity'])){
            $sql=$sql->where('WIC.FLG_INFINITY','=',$filtros['flgInfinity']);
        }

        if (isset($filtros['zonal'])){
            $sql=$sql->where('WCB.ZONAL','=',$filtros['zonal']);
        }
        return $sql;
    }

}
