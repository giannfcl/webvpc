<?php

namespace App\Model\Infinity;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class ConocemeCliente extends Model {

    const LISTA_CAMPOS_CONOCEME = ['WIC.FLG_INFINITY', 'WIC.COD_UNICO', 'WICON.COD_UNICO as COD_UNICO_CONOCEME', 'WIC.NOMBRE', 'WICON.ACTIVIDAD', 'WICON.SUBSECTOR', 'WICON.BACKLOG', 'WICON.MODELO_NEGOCIO', 'WICON.VENTAJA_COMPETITIVA', 'WICON.FORTALEZAS_RIESGOS', 'WICON.INTEGRACION_VERTICAL'
                        , 'WICON.GESTION_COMPRAS', 'WICON.GERENTE_GENERAL', 'WICON.GERENTE_FINANCIERO_TIPO', 'GERENTE_FINANCIERO_NOMBRE', 'TIPO_CONTABILIDAD'
                ,'LINEA_SUCESION','LINEA_SUCESION_FLAG', 'PROCEDENCIA_MPRIMA','MONTO_LINEA_PROVEEDORES','ACTIVO_LIBRE_GRAVAMEN','PROYECCION_VENTAS','PROYECCION_INVERSION'
                ,'CAMBIO_GERENCIA_GENERAL_ANNIO','CAMBIO_ACCIONISTAS_ANNIO','WICON.FECHA_ACTUALIZACION','WU1.REGISTRO AS USUARIOR_REGISTRO','WU1.NOMBRE AS USUARIOR_NOMBRE','VENTAS'
                        ,'WU.REGISTRO as SECTORISTA_REGISTRO','WU.NOMBRE as SECTORISTA_NOMBRE','WU.ID_ZONA as SECTORISTA_ZONAL','METODIZADO_1_FECHA'
                        ,'WU.ID_CENTRO as SECTORISTA_JEFATURA','WU.ROL as SECTORISTA_ROL','WICON.INICIO_OP','WICON.INICIO_IBK','WIC.PROVINCIA','WIC.DISTRITO','WIC.CLASIFICACION','WIC.SALDO_INTERBANK','WIC.SALDO_RCC'];

    function get($periodo, $codunico = null, $zonal = null, $jefatura = null, $ejecutivo = null) {
        $periodo_sql = DB::Table(DB::raw("(SELECT MAX(PERIODO) AS PERIODO FROM WEBBE_INFINITY_CLIENTE) AS ABC "))->first();
        
        //dd($sql->first());

        $sql = DB::Table(DB::raw(
            "(
                SELECT ISNULL(A.PERIODO,".($periodo_sql->PERIODO).") PERIODO, ISNULL(A.COD_SECTORISTA,B.CODSECTORISTA) COD_SECTORISTA, ISNULL(A.COD_UNICO,B.COD_UNICO) COD_UNICO, 
                A.RATING,ISNULL(A.NOMBRE,B.NOMBRE) NOMBRE,B.SALDO_TOTAL,B.SALDO_VIG_SIN_ATRASO,B.SALDO_VIG_CON_ATRASO,
                B.SALDO_NOVIG,B.CTD_MESES_ATRASOS_SINDSCTOS_MAS8,B.CTD_MESES_ATRASOS_TOT_MAS8,A.FLG_INFINITY,B.FLG_SOLO_LEASING,B.RANGO_DEUDA_PROBLEMAS,B.DIAS_ATRASO_HOM,ISNULL(B.FEVE,A.FEVE) FEVE,A.CLASIFICACION,
                A.PROVINCIA,A.DISTRITO,A.SALDO_INTERBANK,A.SALDO_RCC,
                B.FECHA,A.VENTAS,A.METODIZADO_1_FECHA
                FROM 
                    WEBBE_INFINITY_INFO_DIARIA_CLI B 
                    FULL JOIN WEBBE_INFINITY_CLIENTE A ON A.COD_UNICO = B.COD_UNICO  
            ) WIC"
         ))
        ->leftjoin('WEBBE_INFINITY_ALERTA AS WIA', function($join) {
            $join->on('WIC.COD_UNICO', '=', 'WIA.COD_UNICO');
            $join->on('WIC.PERIODO', '=', 'WIA.PERIODO');
        })
        ->leftjoin('ARBOL_DIARIO_2 AS WLS', function($join) {
            $join->on('WIC.COD_SECTORISTA', '=', 'WLS.COD_SECT_LARGO');
        })
        ->leftjoin('WEBVPC_USUARIO AS WU', 'WU.REGISTRO', '=', 'WLS.LOGIN')
        ->leftjoin('WEBBE_ZONAL AS WZ', 'WZ.ID_ZONAL', '=', 'WU.ID_ZONA')
        ->leftjoin('WEBBE_JEFATURA AS WJ', 'WJ.ID_JEFATURA', '=', 'WU.ID_CENTRO')
        ->leftJoin('WEBBE_INFINITY_CONOCEME AS WICON', 'WIC.COD_UNICO', '=', 'WICON.COD_UNICO')
        ->leftJoin('WEBVPC_USUARIO AS WU1','WICON.REGISTRO','=','WU1.REGISTRO')
        ->select(self::LISTA_CAMPOS_CONOCEME);

        if (is_array($periodo)) {
           // $sql = $sql->whereIn('WIC.PERIODO', $periodo);
        } else {
            //$sql = $sql->where('WIC.PERIODO', $periodo);
        }
        if ($codunico) {
            $sql = $sql->where('WIC.COD_UNICO', $codunico);
        }
        if ($ejecutivo) {
            //$sql = $sql->where('WU.REGISTRO', $ejecutivo);
        }
        if ($jefatura) {
            //$sql = $sql->where('WU.ID_CENTRO', $jefatura);
        }
        if ($zonal) {
            //$sql = $sql->where('WU.ID_ZONA', $zonal);
        }
        
        return $sql;
    }

    function registrar($registrar, $cliente) {
        DB::beginTransaction();
        $status = true;

        $data = array_filter($cliente, function($k) {
            return !is_array($k);
        });

        try {
            // Insertamos en CONOCEME y su HISTORICO
            if ($registrar) {
                DB::table('WEBBE_INFINITY_CONOCEME')->insert($data);
            } else {
                DB::table('WEBBE_INFINITY_CONOCEME')->where('COD_UNICO', $cliente['COD_UNICO'])->update(array_except($data, ['COD_UNICO']));
            }
            $data['TIPO'] = 'CONOCEME';
            $historiaId = DB::table('WEBBE_INFINITY_CONOCEME_HIST')->insertGetId($data);

            /* STAKEHOLDER */
            DB::table('WEBBE_INFINITY_CONOCEME_STAKEHOLDER')->where('COD_UNICO', $cliente['COD_UNICO'])->delete();
            DB::table('WEBBE_INFINITY_CONOCEME_STAKEHOLDER')->insert($cliente['stakeholders']);
            foreach ($cliente['stakeholders'] as &$stakeholder) {
                $stakeholder['CONOCEME_HIST_ID'] = $historiaId;
            }
            DB::table('WEBBE_INFINITY_CONOCEME_STAKEHOLDER_HIST')->insert($cliente['stakeholders']);
            unset($stakeholder);

            /* LINEAS */
            DB::table('WEBBE_INFINITY_LINEAS')->where('COD_UNICO', $cliente['COD_UNICO'])->delete();
            DB::table('WEBBE_INFINITY_LINEAS')->insert($cliente['lineas']);
            foreach ($cliente['lineas'] as &$linea) {
                $linea['CONOCEME_HIST_ID'] = $historiaId;
            }
            DB::table('WEBBE_INFINITY_LINEAS_HIST')->insert($cliente['lineas']);
            unset($linea);

            /* ZONAS */
            DB::table('WEBBE_INFINITY_CONOCEME_ZONA')->where('COD_UNICO', $cliente['COD_UNICO'])->delete();
            DB::table('WEBBE_INFINITY_CONOCEME_ZONA')->insert($cliente['zonas']);
            foreach ($cliente['zonas'] as &$zona) {
                $zona['CONOCEME_HIST_ID'] = $historiaId;
            }
            DB::table('WEBBE_INFINITY_CONOCEME_ZONA_HIST')->insert($cliente['zonas']);
            unset($zona);

            /* COMMODITIES */
            DB::table('WEBBE_INFINITY_CONOCEME_COMMODITIE')->where('COD_UNICO', $cliente['COD_UNICO'])->delete();
            DB::table('WEBBE_INFINITY_CONOCEME_COMMODITIE')->insert($cliente['commodities']);
            foreach ($cliente['commodities'] as &$com) {
                $com['CONOCEME_HIST_ID'] = $historiaId;
            }
            DB::table('WEBBE_INFINITY_CONOCEME_COMMODITIE_HIST')->insert($cliente['commodities']);
            unset($com);

            /* MIXVENTAS */
            DB::table('WEBBE_INFINITY_MIX_VENTAS')->where('COD_UNICO', $cliente['COD_UNICO'])->delete();
            DB::table('WEBBE_INFINITY_MIX_VENTAS')->insert($cliente['mix']);
            foreach ($cliente['mix'] as &$mix) {
                $mix['CONOCEME_HIST_ID'] = $historiaId;
            }
            DB::table('WEBBE_INFINITY_MIX_VENTAS_HIST')->insert($cliente['mix']);
            unset($mix);

            /* INVERSIONES */
            DB::table('WEBBE_INFINITY_INVERSIONES')->where('COD_UNICO', $cliente['COD_UNICO'])->delete();
            DB::table('WEBBE_INFINITY_INVERSIONES')->insert($cliente['inversiones']);
            foreach ($cliente['inversiones'] as &$inv) {
                $inv['CONOCEME_HIST_ID'] = $historiaId;
            }
            DB::table('WEBBE_INFINITY_INVERSIONES_HIST')->insert($cliente['inversiones']);
            unset($inv);

            /* CANALVENTAS */
            DB::table('WEBBE_INFINITY_CANAL_VENTAS')->where('COD_UNICO', $cliente['COD_UNICO'])->delete();
            DB::table('WEBBE_INFINITY_CANAL_VENTAS')->insert($cliente['canales']);
            foreach ($cliente['canales'] as &$can) {
                $can['CONOCEME_HIST_ID'] = $historiaId;
            }
            DB::table('WEBBE_INFINITY_CANAL_VENTAS_HIST')->insert($cliente['canales']);
            unset($can);

            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }

}
