<?php
namespace App\Model\Infinity;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class Resumen extends Model{

    static function graficarGestion($periodoActual,$filtros){
        $sql=DB::table('WEBBE_INFINITY_CLIENTE AS WIC')
                 ->select(
            DB::Raw("SUM(CASE WHEN WIA.NIVEL_ALERTA=2 AND WIA.FLG_GESTIONADO=1 THEN 1 ELSE 0 END)/
            dbo.ISZERO(SUM(CASE WHEN WIA.NIVEL_ALERTA=2 THEN 1 ELSE 0 END),1) AS GESTION_AMBAR"),
            DB::Raw("SUM(CASE WHEN WIA.NIVEL_ALERTA=3 AND WIA.FLG_GESTIONADO=1 THEN 1 ELSE 0 END)/
            dbo.ISZERO(SUM(CASE WHEN WIA.NIVEL_ALERTA=3 THEN 1 ELSE 0 END),1) AS GESTION_ROJO"),
            DB::Raw("SUM(CASE WHEN WIA.FLG_DOCUMENTACION_COMPLETA=1 AND WIA.FLG_GESTIONADO=1 THEN 1 ELSE 0 END)/
            dbo.ISZERO(SUM(CASE WHEN WIA.FLG_GESTIONADO=1 THEN 1 ELSE 0 END),1) AS GESTION_DOCUMENTACION")
            )
            ->join('WEBBE_INFINITY_ALERTA AS WIA', function($join){
                $join->on('WIA.COD_UNICO','=','WIC.COD_UNICO');
                $join->on('WIA.PERIODO','=','WIC.PERIODO');
            })
            ->leftJoin('ARBOL_DIARIO_2 AS WLS', function($join){
                $join->on('WLS.COD_SECT_LARGO','=','WIC.COD_SECTORISTA');
            }) 
            ->leftJoin('WEBVPC_USUARIO AS WU', function($join){
                $join->on('WLS.LOGIN','=','WU.REGISTRO');
            })
            ->whereIn('WIC.PERIODO',[$periodoActual]);

        if (isset($filtros['zonal'])){
            $sql = $sql->where('WU.ID_ZONA','=',$filtros['zonal']);
        }

        if (isset($filtros['jefatura'])){
            $sql = $sql->where('WU.ID_CENTRO','=',$filtros['jefatura']);
        }    

        if (isset($filtros['flgInfinity'])){
            $sql = $sql->whereIn('WIC.FLG_INFINITY',$filtros['flgInfinity']);
        }

        return $sql;

    }

    static function graficarMora($periodoActual,$periodoAnterior,$filtros){
        $sql=DB::table('WEBBE_INFINITY_CLIENTE AS WIC')
            ->select(
                DB::Raw("SUM(CASE WHEN WIA.NIVEL_ALERTA=2 AND WIC.PERIODO='".$periodoActual."' THEN WIC.MORA_INTERBANK ELSE 0 END) AS MORA_AMBAR"),
                DB::Raw("SUM(CASE WHEN WIA.NIVEL_ALERTA=1 AND WIC.PERIODO='".$periodoActual."' THEN WIC.MORA_INTERBANK ELSE 0 END) AS MORA_VERDE"),
                DB::Raw("SUM(CASE WHEN WIA.NIVEL_ALERTA=3 AND WIC.PERIODO='".$periodoActual."' THEN WIC.MORA_INTERBANK ELSE 0 END) AS MORA_ROJO"),
                DB::Raw("SUM(CASE WHEN WIC.PERIODO='".$periodoActual."' THEN WIC.MORA_INTERBANK ELSE 0 END) AS MORA_TOTAL"),
                DB::Raw("(SUM(CASE WHEN WIC.PERIODO='".$periodoActual."' THEN WIC.MORA_INTERBANK ELSE 0 END)-SUM(CASE WHEN WIC.PERIODO='".$periodoAnterior."' THEN WIC.MORA_INTERBANK ELSE 0 END))*100/
                dbo.ISZERO(SUM(CASE WHEN WIC.PERIODO='".$periodoAnterior."' THEN WIC.MORA_INTERBANK ELSE 0 END),1) AS VARIACION_MORA")
            )
            ->join('WEBBE_INFINITY_ALERTA AS WIA', function($join){
                $join->on('WIA.COD_UNICO','=','WIC.COD_UNICO');
                $join->on('WIA.PERIODO','=','WIC.PERIODO');
            })
            ->leftJoin('ARBOL_DIARIO_2 AS WLS', function($join){
                $join->on('WLS.COD_SECT_LARGO','=','WIC.COD_SECTORISTA');
            }) 
            ->leftJoin('WEBVPC_USUARIO AS WU', function($join){
                $join->on('WLS.LOGIN','=','WU.REGISTRO');
            })
            ->whereIn('WIC.PERIODO',[$periodoActual,$periodoAnterior]);

        if (isset($filtros['zonal'])){
            $sql = $sql->where('WU.ID_ZONA','=',$filtros['zonal']);
        }

        if (isset($filtros['jefatura'])){
            $sql = $sql->where('WU.ID_CENTRO','=',$filtros['jefatura']);
        } 

        if (isset($filtros['flgInfinity'])){
            $sql = $sql->whereIn('WIC.FLG_INFINITY',$filtros['flgInfinity']);
        }

        return $sql;    
    }

    static function graficarLineal($filtros){
        $sql=DB::table('WEBBE_INFINITY_CLIENTE AS WIC')
                ->select(
                    'WIC.PERIODO AS PERIODO',
                    DB::Raw("SUM(CASE WHEN WIA.NIVEL_ALERTA=2 THEN 1 ELSE 0 END) AS CLIENTES_AMBAR"),
                    DB::Raw("SUM(CASE WHEN WIA.NIVEL_ALERTA=1 THEN 1 ELSE 0 END) AS CLIENTES_VERDE"),
                    DB::Raw("SUM(CASE WHEN WIA.NIVEL_ALERTA=3 THEN 1 ELSE 0 END) AS CLIENTES_ROJO"),
                    DB::Raw("SUM(CASE WHEN WIA.NIVEL_ALERTA=2 THEN WIC.SALDO_INTERBANK ELSE 0 END) AS SALDO_AMBAR"),
                    DB::Raw("SUM(CASE WHEN WIA.NIVEL_ALERTA=1 THEN WIC.SALDO_INTERBANK ELSE 0 END) AS SALDO_VERDE"),
                    DB::Raw("SUM(CASE WHEN WIA.NIVEL_ALERTA=3 THEN WIC.SALDO_INTERBANK ELSE 0 END) AS SALDO_ROJO")
            )
            ->join('WEBBE_INFINITY_ALERTA AS WIA', function($join){
                $join->on('WIA.COD_UNICO','=','WIC.COD_UNICO');
                $join->on('WIA.PERIODO','=','WIC.PERIODO');
            })
            ->leftJoin('ARBOL_DIARIO_2 AS WLS', function($join){
                $join->on('WLS.COD_SECT_LARGO','=','WIC.COD_SECTORISTA');
            }) 
            ->leftJoin('WEBVPC_USUARIO AS WU', function($join){
                $join->on('WLS.LOGIN','=','WU.REGISTRO');
            })
            ->groupBy('WIC.PERIODO');    

        if (isset($filtros['zonal'])){
            $sql = $sql->where('WU.ID_ZONA','=',$filtros['zonal']);
        }

        if (isset($filtros['jefatura'])){
            $sql = $sql->where('WU.ID_CENTRO','=',$filtros['jefatura']);
        } 

        if (isset($filtros['flgInfinity'])){
            $sql = $sql->whereIn('WIC.FLG_INFINITY',$filtros['flgInfinity']);
        }

        return $sql;   

    }

    static function getMovimientos($periodoActual,$periodoAnterior,$filtros){
        /*Se debe de buscar una forma de optimizar este método*/
        $sql=DB::table('WEBBE_INFINITY_CLIENTE AS WIC')
                 ->select(
                    DB::Raw("SUM(CASE WHEN WIC.FLG_CLIENTE_NUEVO=1 THEN 1 ELSE 0 END) AS LINEAS_NUEVAS")
            )
            ->join('WEBBE_INFINITY_ALERTA AS WIA', function($join){
                $join->on('WIA.COD_UNICO','=','WIC.COD_UNICO');
                $join->on('WIA.PERIODO','=','WIC.PERIODO');
            })
            ->leftJoin('ARBOL_DIARIO_2 AS WLS', function($join){
                $join->on('WLS.COD_SECT_LARGO','=','WIC.COD_SECTORISTA');
            }) 
            ->leftJoin('WEBVPC_USUARIO AS WU', function($join){
                $join->on('WLS.LOGIN','=','WU.REGISTRO');
            })
            ->whereIn('WIC.PERIODO',[$periodoActual,$periodoAnterior]);    
   

        if (isset($filtros['zonal'])){
            $sql = $sql->where('WU.ID_ZONA','=',$filtros['zonal']);
        }

        if (isset($filtros['jefatura'])){
            $sql = $sql->where('WU.ID_CENTRO','=',$filtros['jefatura']);
        } 

        if (isset($filtros['flgInfinity'])){
            $sql = $sql->whereIn('WIC.FLG_INFINITY',$filtros['flgInfinity']);
        }

        $lineasNuevas=$sql->first()->LINEAS_NUEVAS;

        /*Armamos los números de líneas que ya no existen*/
        $sql1=DB::table('WEBBE_INFINITY_CLIENTE AS WIC')
            ->select('WIC.COD_UNICO')
            ->join('WEBBE_INFINITY_ALERTA AS WIA', function($join){
                $join->on('WIA.COD_UNICO','=','WIC.COD_UNICO');
                $join->on('WIA.PERIODO','=','WIC.PERIODO');
            })
            ->leftJoin('ARBOL_DIARIO_2 AS WLS', function($join){
                $join->on('WLS.COD_SECT_LARGO','=','WIC.COD_SECTORISTA');
            }) 
            ->leftJoin('WEBVPC_USUARIO AS WU', function($join){
                $join->on('WLS.LOGIN','=','WU.REGISTRO');
            })
            ->whereIn('WIC.PERIODO',[$periodoAnterior]);

        if (isset($filtros['zonal'])){
            $sql1 = $sql1->where('WU.ID_ZONA','=',$filtros['zonal']);
        }

        if (isset($filtros['jefatura'])){
            $sql1 = $sql1->where('WU.ID_CENTRO','=',$filtros['jefatura']);
        }

        if (isset($filtros['flgInfinity'])){
            $sql1 = $sql1->whereIn('WIC.FLG_INFINITY',$filtros['flgInfinity']);
        }

        $antiguas=$sql1->get();

        $sql2=DB::table('WEBBE_INFINITY_CLIENTE AS WIC')
            ->select('WIC.COD_UNICO')
            ->join('WEBBE_INFINITY_ALERTA AS WIA', function($join){
                $join->on('WIA.COD_UNICO','=','WIC.COD_UNICO');
                $join->on('WIA.PERIODO','=','WIC.PERIODO');
            })
            ->leftJoin('ARBOL_DIARIO_2 AS WLS', function($join){
                $join->on('WLS.COD_SECT_LARGO','=','WIC.COD_SECTORISTA');
            }) 
            ->leftJoin('WEBVPC_USUARIO AS WU', function($join){
                $join->on('WLS.LOGIN','=','WU.REGISTRO');
            })
            ->whereIn('WIC.PERIODO',[$periodoActual]);

        if (isset($filtros['zonal'])){
            $sql2 = $sql2->where('WU.ID_ZONA','=',$filtros['zonal']);
        }

        if (isset($filtros['jefatura'])){
            $sql2 = $sql2->where('WU.ID_CENTRO','=',$filtros['jefatura']);
        } 

        if (isset($filtros['flgInfinity'])){
            $sql2 = $sql2->whereIn('WIC.FLG_INFINITY',$filtros['flgInfinity']);
        }

        $aux=$sql1->get();
        $nuevas=[];

        foreach ($aux as $elem) {
            $nuevas[]=$elem->COD_UNICO;
        }
        
        $lineasPerdidas=0;

        foreach ($antiguas as $antigua) {
            if(!in_array($antigua->COD_UNICO, $nuevas)){
                $lineasPerdidas++;
            }
        }

        $movimientos=[
            'LINEAS_NUEVAS'=>$lineasNuevas,
            'LINEAS_PERDIDAS'=>$lineasPerdidas
        ];

        return $movimientos;    

    }

    static function getMigraciones($periodoActual,$filtros){
        $sql=DB::table('WEBBE_INFINITY_CLIENTE AS WIC')
             ->select(
                    DB::Raw("SUM(CASE WHEN WIA.NIVEL_ALERTA_ANTERIOR=1 AND WIA.NIVEL_ALERTA=3 THEN 1 ELSE 0 END) AS MIG_CLIENTE_1"),
                    DB::Raw("SUM(CASE WHEN WIA.NIVEL_ALERTA_ANTERIOR=2 AND WIA.NIVEL_ALERTA=3 THEN 1 ELSE 0 END) AS MIG_CLIENTE_2"),
                    DB::Raw("SUM(CASE WHEN WIA.NIVEL_ALERTA_ANTERIOR=1 AND WIA.NIVEL_ALERTA=2 THEN 1 ELSE 0 END) AS MIG_CLIENTE_3"),
                    DB::Raw("SUM(CASE WHEN WIA.NIVEL_ALERTA_ANTERIOR IN (2,3) AND WIA.NIVEL_ALERTA=1 THEN 1 ELSE 0 END) AS MIG_CLIENTE_4"),
                    DB::Raw("SUM(CASE WHEN WIA.NIVEL_ALERTA_ANTERIOR=1 AND WIA.NIVEL_ALERTA=3 THEN WIC.SALDO_INTERBANK ELSE 0 END) AS MIG_SALDO_1"),
                    DB::Raw("SUM(CASE WHEN WIA.NIVEL_ALERTA_ANTERIOR=2 AND WIA.NIVEL_ALERTA=3 THEN WIC.SALDO_INTERBANK ELSE 0 END) AS MIG_SALDO_2"),
                    DB::Raw("SUM(CASE WHEN WIA.NIVEL_ALERTA_ANTERIOR=1 AND WIA.NIVEL_ALERTA=2 THEN WIC.SALDO_INTERBANK ELSE 0 END) AS MIG_SALDO_3"),
                    DB::Raw("SUM(CASE WHEN WIA.NIVEL_ALERTA_ANTERIOR IN (2,3) AND WIA.NIVEL_ALERTA=1 THEN WIC.SALDO_INTERBANK ELSE 0 END) AS MIG_SALDO_4")
            )
            ->join('WEBBE_INFINITY_ALERTA AS WIA', function($join){
                $join->on('WIA.COD_UNICO','=','WIC.COD_UNICO');
                $join->on('WIA.PERIODO','=','WIC.PERIODO');
            })
            ->leftJoin('ARBOL_DIARIO_2 AS WLS', function($join){
                $join->on('WLS.COD_SECT_LARGO','=','WIC.COD_SECTORISTA');
            }) 
            ->leftJoin('WEBVPC_USUARIO AS WU', function($join){
                $join->on('WLS.LOGIN','=','WU.REGISTRO');
            })
            ->whereIn('WIC.PERIODO',[$periodoActual]);    

        if (isset($filtros['zonal'])){
            $sql = $sql->where('WU.ID_ZONA','=',$filtros['zonal']);
        }

        if (isset($filtros['jefatura'])){
            $sql = $sql->where('WU.ID_CENTRO','=',$filtros['jefatura']);
        } 

        if (isset($filtros['flgInfinity'])){
            $sql = $sql->whereIn('WIC.FLG_INFINITY',$filtros['flgInfinity']);
        }

        return $sql;

    }

    static function getComposicionCartera($periodoActual,$periodoAnterior,$filtros){
        $sql=DB::table('WEBBE_INFINITY_CLIENTE AS WIC')
            ->select(
                DB::Raw("SUM(CASE WHEN WIA.NIVEL_ALERTA=1 AND WIC.PERIODO='".$periodoActual."' THEN 1 ELSE 0 END) AS CLIENTES_VERDE_ACTUAL"),
                DB::Raw("SUM(CASE WHEN WIA.NIVEL_ALERTA=2 AND WIC.PERIODO='".$periodoActual."' THEN 1 ELSE 0 END) AS CLIENTES_AMBAR_ACTUAL"),
                DB::Raw("SUM(CASE WHEN WIA.NIVEL_ALERTA=3 AND WIC.PERIODO='".$periodoActual."' THEN 1 ELSE 0 END) AS CLIENTES_ROJO_ACTUAL"),
                DB::Raw("SUM(CASE WHEN WIA.NIVEL_ALERTA=1 AND WIC.PERIODO='".$periodoAnterior."' THEN 1 ELSE 0 END) AS CLIENTES_VERDE_ANT"),
                DB::Raw("SUM(CASE WHEN WIA.NIVEL_ALERTA=2 AND WIC.PERIODO='".$periodoAnterior."' THEN 1 ELSE 0 END) AS CLIENTES_AMBAR_ANT"),
                DB::Raw("SUM(CASE WHEN WIA.NIVEL_ALERTA=3 AND WIC.PERIODO='".$periodoAnterior."' THEN 1 ELSE 0 END) AS CLIENTES_ROJO_ANT"),
                DB::Raw("SUM(CASE WHEN WIA.NIVEL_ALERTA=1 AND WIC.PERIODO='".$periodoActual."' THEN WIC.SALDO_INTERBANK ELSE 0 END) AS SALDO_VERDE_ACTUAL"),
                DB::Raw("SUM(CASE WHEN WIA.NIVEL_ALERTA=2 AND WIC.PERIODO='".$periodoActual."' THEN WIC.SALDO_INTERBANK ELSE 0 END) AS SALDO_AMBAR_ACTUAL"),
                DB::Raw("SUM(CASE WHEN WIA.NIVEL_ALERTA=3 AND WIC.PERIODO='".$periodoActual."' THEN WIC.SALDO_INTERBANK ELSE 0 END) AS SALDO_ROJO_ACTUAL"),
                DB::Raw("SUM(CASE WHEN WIA.NIVEL_ALERTA=1 AND WIC.PERIODO='".$periodoAnterior."' THEN WIC.SALDO_INTERBANK ELSE 0 END) AS SALDO_VERDE_ANT"),
                DB::Raw("SUM(CASE WHEN WIA.NIVEL_ALERTA=2 AND WIC.PERIODO='".$periodoAnterior."' THEN WIC.SALDO_INTERBANK ELSE 0 END) AS SALDO_AMBAR_ANT"),
                DB::Raw("SUM(CASE WHEN WIA.NIVEL_ALERTA=3 AND WIC.PERIODO='".$periodoAnterior."' THEN WIC.SALDO_INTERBANK ELSE 0 END) AS SALDO_ROJO_ANT")
            )
            ->join('WEBBE_INFINITY_ALERTA AS WIA', function($join){
                $join->on('WIA.COD_UNICO','=','WIC.COD_UNICO');
                $join->on('WIA.PERIODO','=','WIC.PERIODO');
            })
            ->leftJoin('ARBOL_DIARIO_2 AS WLS', function($join){
                $join->on('WLS.COD_SECT_LARGO','=','WIC.COD_SECTORISTA');
            }) 
            ->leftJoin('WEBVPC_USUARIO AS WU', function($join){
                $join->on('WLS.LOGIN','=','WU.REGISTRO');
            })
            ->whereIn('WIC.PERIODO',[$periodoAnterior,$periodoActual]);  

        if (isset($filtros['zonal'])){
            $sql = $sql->where('WU.ID_ZONA','=',$filtros['zonal']);
        }

        if (isset($filtros['jefatura'])){
            $sql = $sql->where('WU.ID_CENTRO','=',$filtros['jefatura']);
        } 

        if (isset($filtros['flgInfinity'])){
            $sql = $sql->whereIn('WIC.FLG_INFINITY',$filtros['flgInfinity']);
        }

        return $sql;    
    }
}



