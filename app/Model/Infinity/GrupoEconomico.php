<?php

namespace App\Model\Infinity;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;


class GrupoEconomico extends Model {

    static function getGrupoEconomicoCliente($periodo, $codUnico){
     
        $sql=DB::table('WEBBE_INFINITY_GRUPO_ECO AS WIG')
        ->select()       
        ->where('WIG.PERIODO','=',$periodo)
        ->where('WIG.COD_UNICO','=',$codUnico);

        return $sql;
    }

    static function getGrupoEconomicoClienteRelacionado($periodo, $codUnico){
        $sql=DB::table('WEBBE_INFINITY_GRUPO_ECO_RELACIONADO AS WIGR')
        ->select(
            'NUM_DOC_REL',
            'COD_UNICO_REL',
            'NOMBRE_REL',
            'FLG_CLASIFICACION',
            'MONTO_VENCIDO',
            'FLG_FEVE',
            'MONTO_REFINANCIADO',
            'MONTO_REESTRUCTURADO',
            'MONTO_JUDICIAL',
            'MONTO_COACTIVA',
            'MONTO_LABORAL'
        )        
        ->where('WIGR.PERIODO','=',$periodo)
        ->where('WIGR.COD_UNICO','=',$codUnico);

        return $sql;
    }

}
