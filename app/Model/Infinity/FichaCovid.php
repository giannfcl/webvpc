<?php

namespace App\Model\Infinity;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;
use App\Entity\Usuario as Usuario;
use Jenssegers\Date\Date as Carbon;
use App\Entity\Notification as eNotification;
use App\Entity\Infinity\Cliente as eCliente;

class FichaCovid extends Model
{

    function save($data = [])
    {
        DB::beginTransaction();
        $status = true;
        try {
            if (count($data) > 0) {
                DB::table('WEBVPC_COVID19')->insert($data);
                DB::commit();
            }
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }
    function save2($data = [], $cumplimiento = [])
    {
        $status = true;
        DB::beginTransaction();
        $maxIDV = null;
        try {
            //ULTIMA FICHA
            $datoscovid = $this->getDatosByCliente2($data['COD_UNICO']);
            //REVISAR SI ES QUE LA ULTIMA FICHA ESTA A ESPERA DE VALIDACION (GUARDADA)
            $condicion = !empty($datoscovid) ? $datoscovid->ESTADO_APROBACION == "0" : false;

            //FICHA GUARDADA VS EN => no deberia guardar ni aprobar nada
            //FICHA APROBADA VS EN => INSERTAR
            //FICHA GUARDADA VS VALIDADOR => UPDATE
            //FICHA APROBADA VS VALIDADOR => INSERTAR

            if ($condicion && $data['ESTADO_APROBACION'] == '1') {
                $maxIDV = $datoscovid->ID;
                DB::table('WEBVPC_COVID19')->where([
                    'COD_UNICO' => $data['COD_UNICO'],
                    'ID' => $maxIDV
                ])
                    ->update($data);
            } else if (!$condicion) {
                $maxIDV = DB::table('WEBVPC_COVID19')->insertGetId($data);
            }
            if (count($cumplimiento) > 0) {
                $array = range(0, count($cumplimiento) - 1);
                foreach ($array as $i) {
                    if (!array_key_exists('ID', $cumplimiento[$i])) {
                        $cumplimiento[$i]['ID_COVID'] = $maxIDV;
                        DB::table('WEBVPC_COVID19_CUMPLIMIENTO')->insert($cumplimiento[$i]);
                    } else {
                        $idCUMPL = $cumplimiento[$i]['ID'];
                        unset($cumplimiento[$i]['ID']);
                        DB::table('WEBVPC_COVID19_CUMPLIMIENTO')->where(
                            'ID',
                            '=',
                            $idCUMPL
                        )->update($cumplimiento[$i]);
                    }
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }
    function getCumplimientoCategoria($id)
    {
        $categoria = null;
        $sql = DB::table('WEBVPC_COVID19_CUMPLIMIENTO')->where('ID', '=', $id);
        if ($sql != null) {
            $categoria = $sql->TIPO_GESTION;
        }
        return $categoria;
    }
    function updateEstrategia($dataObj, $registro, $nombre)
    {
        $status = true;
        $data = (array) $dataObj;
        $now = Carbon::now();
        $fechaStr = $now->toDateString();
        $fechaAR = explode("-", $fechaStr);
        $fechaTexto = $fechaAR[2] . ' de ' . ucwords($now->format('F')) . ' de ' . $fechaAR[0];
        DB::beginTransaction();
        try {
            $updateData = [
                'PREGUNTA_13' => isset($data['slcQ13']) ? $data['slcQ13'] : NULL,
                'PREGUNTA_14_1' => isset($data['slcQ14_1']) ? $data['slcQ14_1'] : NULL,
                'PREGUNTA_14_2' => isset($data['slcQ14_2']) ? $data['slcQ14_2'] : NULL,
                'PREGUNTA_14_3' => isset($data['slcQ14_3']) ? $data['slcQ14_3'] : NULL,
                'MONTO_CRECER' => isset($data['montoCrecer']) ? $data['montoCrecer'] : NULL,
                'DETALLE_CRECER' => isset($data['detalleCrecer']) ? $data['detalleCrecer'] : NULL
            ];
            $data = DB::table('WEBVPC_COVID19')->where([
                'COD_UNICO' => $dataObj->cu,
                'ID' => $dataObj->ID
            ])->select()->first();

            if ($data->PREGUNTA_13 != $updateData['PREGUNTA_13']) {
                $impacto = $data->PREGUNTA_13 != null ? $data->PREGUNTA_13 : '';
                DB::table('WEBVPC_ULTIMOS_CAMBIOS')->insert([
                    'MENSAJE' => 'Se cambió el impacto de ' . $impacto . ' a ' . $updateData['PREGUNTA_13'],
                    'USUARIO'  => $registro,
                    'FECHA'  => $now,
                    'FECHA_TEXTO'  => $fechaTexto,
                    'NOMBRE_USUARIO'  => $nombre,
                    'COD_UNICO' => $dataObj->cu,
                    'ID_COVID' => $dataObj->ID,
                    'TIPO' => 'ESTRATEGIA',
                    'VARIABLE' => 'IMPACTO',
                    'VALOR_ANTERIOR' => $impacto,
                    'VALOR_ACTUAL' => $updateData['PREGUNTA_13'],
                    'ID_COMPROMISO' => null
                ]);
            }
            if ($data->PREGUNTA_14_1 != $updateData['PREGUNTA_14_1']) {
                $plan = $data->PREGUNTA_14_1 != null ? $data->PREGUNTA_14_1 : '';
                DB::table('WEBVPC_ULTIMOS_CAMBIOS')->insert([
                    'MENSAJE' => 'Se cambió el plan propuesto de ' . $plan . ' a ' . $updateData['PREGUNTA_14_1'],
                    'USUARIO'  => $registro,
                    'FECHA'  => $now,
                    'FECHA_TEXTO'  => $fechaTexto,
                    'NOMBRE_USUARIO'  => $nombre,
                    'COD_UNICO' => $dataObj->cu,
                    'ID_COVID' => $dataObj->ID,
                    'TIPO' => 'ESTRATEGIA',
                    'VARIABLE' => 'PLAN',
                    'VALOR_ANTERIOR' => $plan,
                    'VALOR_ACTUAL' => $updateData['PREGUNTA_14_1'],
                    'ID_COMPROMISO' => null
                ]);
            }
            if ($data->PREGUNTA_14_2 != $updateData['PREGUNTA_14_2']) {
                $feve = $data->PREGUNTA_14_2 != null ? $data->PREGUNTA_14_2 : '';
                DB::table('WEBVPC_ULTIMOS_CAMBIOS')->insert([
                    'MENSAJE' => 'Se cambió el feve de ' . $feve . ' a ' . $updateData['PREGUNTA_14_2'],
                    'USUARIO'  => $registro,
                    'FECHA'  => $now,
                    'FECHA_TEXTO'  => $fechaTexto,
                    'NOMBRE_USUARIO'  => $nombre,
                    'COD_UNICO' => $dataObj->cu,
                    'ID_COVID' => $dataObj->ID,
                    'TIPO' => 'ESTRATEGIA',
                    'VARIABLE' => 'FEVE',
                    'VALOR_ANTERIOR' => $feve,
                    'VALOR_ACTUAL' => $updateData['PREGUNTA_14_2'],
                    'ID_COMPROMISO' => null
                ]);
            }
            if ($data->PREGUNTA_14_3 != $updateData['PREGUNTA_14_3']) {
                $clasificacion = $data->PREGUNTA_14_3 != null ? $data->PREGUNTA_14_3 : '';
                DB::table('WEBVPC_ULTIMOS_CAMBIOS')->insert([
                    'MENSAJE' => 'Se cambió la clasificación de ' . $clasificacion . ' a ' . $updateData['PREGUNTA_14_3'],
                    'USUARIO'  => $registro,
                    'FECHA'  => $now,
                    'FECHA_TEXTO'  => $fechaTexto,
                    'NOMBRE_USUARIO'  => $nombre,
                    'COD_UNICO' => $dataObj->cu,
                    'ID_COVID' => $dataObj->ID,
                    'TIPO' => 'ESTRATEGIA',
                    'VARIABLE' => 'CLASIFICACION',
                    'VALOR_ANTERIOR' => $clasificacion,
                    'VALOR_ACTUAL' => $updateData['PREGUNTA_14_3'],
                    'ID_COMPROMISO' => null
                ]);
            }
            if ($data->MONTO_CRECER != $updateData['MONTO_CRECER']) {
                $plan = $updateData['PREGUNTA_14_1'];
                if ($plan == 'Crecer') {
                    $monto = $data->MONTO_CRECER != null ? $data->MONTO_CRECER : '0';
                    $monto = $monto == '' ? '0' : $monto;
                    DB::table('WEBVPC_ULTIMOS_CAMBIOS')->insert([
                        'MENSAJE' => 'Se cambió el monto de ' . $monto . ' a ' . $updateData['MONTO_CRECER'],
                        'USUARIO'  => $registro,
                        'FECHA'  => $now,
                        'FECHA_TEXTO'  => $fechaTexto,
                        'NOMBRE_USUARIO'  => $nombre,
                        'COD_UNICO' => $dataObj->cu,
                        'ID_COVID' => $dataObj->ID,
                        'TIPO' => 'ESTRATEGIA',
                        'VARIABLE' => 'MONTO_CRECER',
                        'VALOR_ANTERIOR' => $monto,
                        'VALOR_ACTUAL' => $updateData['MONTO_CRECER'],
                        'ID_COMPROMISO' => null
                    ]);
                }
            }
            if ($data->DETALLE_CRECER != $updateData['DETALLE_CRECER']) {
                $plan = $updateData['PREGUNTA_14_1'];
                if ($plan == 'Crecer') {
                    $detalle = $data->DETALLE_CRECER != null ? $data->DETALLE_CRECER : '';
                    if ($detalle == '') {
                        DB::table('WEBVPC_ULTIMOS_CAMBIOS')->insert([
                            'MENSAJE' => 'Se añadió el detalle del monto: ' . $updateData['DETALLE_CRECER'],
                            'USUARIO'  => $registro,
                            'FECHA'  => $now,
                            'FECHA_TEXTO'  => $fechaTexto,
                            'NOMBRE_USUARIO'  => $nombre,
                            'COD_UNICO' => $dataObj->cu,
                            'ID_COVID' => $dataObj->ID,
                            'TIPO' => 'ESTRATEGIA',
                            'VARIABLE' => 'DETALLE_CRECER',
                            'VALOR_ANTERIOR' => $detalle,
                            'VALOR_ACTUAL' => $updateData['DETALLE_CRECER'],
                            'ID_COMPROMISO' => null
                        ]);
                    } else {
                        DB::table('WEBVPC_ULTIMOS_CAMBIOS')->insert([
                            'MENSAJE' => 'Se cambió el detalle del monto de ' . $detalle . ' a ' . $updateData['DETALLE_CRECER'],
                            'USUARIO'  => $registro,
                            'FECHA'  => $now,
                            'FECHA_TEXTO'  => $fechaTexto,
                            'NOMBRE_USUARIO'  => $nombre,
                            'COD_UNICO' => $dataObj->cu,
                            'ID_COVID' => $dataObj->ID,
                            'TIPO' => 'ESTRATEGIA',
                            'VARIABLE' => 'DETALLE_CRECER',
                            'VALOR_ANTERIOR' => $detalle,
                            'VALOR_ACTUAL' => $updateData['DETALLE_CRECER'],
                            'ID_COMPROMISO' => null
                        ]);
                    }
                }
            }

            DB::table('WEBVPC_COVID19')->where([
                'COD_UNICO' => $dataObj->cu,
                'ID' => $dataObj->ID
            ])
                ->update($updateData);
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }
    function getCambios($id)
    {
        $data = DB::table('WEBVPC_ULTIMOS_CAMBIOS')->where('ID_COVID', '=', $id)->orderby('ID', 'desc')->get();
        return $data;
    }
    function addCumplimiento($dataObj, $registro, $rol, $nombre)
    {
        $status = array('status' => true, 'data' => (object)[]);
        DB::beginTransaction();
        $now = Carbon::now();
        $data = (array) $dataObj;
        $fechaStr = $now->toDateString();
        $fechaAR = explode("-", $fechaStr);
        $fechaTexto = $fechaAR[2] . ' de ' . ucwords($now->format('F')) . ' de ' . $fechaAR[0];
        try {
            $datacovid = DB::table('WEBVPC_COVID19')->where([
                'ID' => $dataObj->ID
            ])->select()->first();
            $orden = DB::table('WEBVPC_COVID19_CUMPLIMIENTO')->where([
                'COD_UNICO' => $dataObj->cu,
                'ID_COVID' => $dataObj->ID,
            ])->where('STATUS_COMPROMISO', '!=', 'ELIMINADO')->where("STATUS_COMPROMISO", '!=', 'NO CONFIRMADO')->max('ORDEN');
            $detalle = null;
            if ($data['TIPO_COMPROMISO'] == 'Comerciales') {
                $detalle = $data['TIPO_COMPROMISO_DETALLE_Comerciales'];
            } else if ($data['TIPO_COMPROMISO'] == 'Canalización de flujos IN') {
                $detalle = $data['TIPO_COMPROMISO_DETALLE_Canalización_de_flujos_IN'];
            } else if ($data['TIPO_COMPROMISO'] == 'Incremento de flujos OUT') {
                $detalle = $data['TIPO_COMPROMISO_DETALLE_Canalización_de_flujos_OUT'];
            } else if ($data['TIPO_COMPROMISO'] == 'Garantías') {
                $detalle = $data['TIPO_COMPROMISO_DETALLE_Garantías'];
            } else if ($data['TIPO_COMPROMISO'] == 'Estados Financieros') {
                $detalle = $data['TIPO_COMPROMISO_DETALLE_Estados_Financieros'];
            } else {
                $detalle = $data['TIPO_COMPROMISO_DETALLE_Otros'];
            }
            $cumplimiento = [
                'ID_COVID' => $dataObj->ID,
                'COD_UNICO' => $dataObj->cu,
                'FECHA_REGISTRO' => $now,
                'ID_VISITA' => $datacovid->ID_VISITA,
                'REGISTRO_INFORMACION' => $registro,
                'USUARIO_MODIFICACION' => null,
                'TIPO_GESTION' => isset($data['TIPO_GESTION']) ? $data['TIPO_GESTION'] == '-Seleccionar-' ? NULL : $data['TIPO_GESTION'] : NULL,
                'TIPO_COMPROMISO' => isset($data['TIPO_COMPROMISO']) ? $data['TIPO_COMPROMISO'] == '-Seleccionar-' ? NULL : $data['TIPO_COMPROMISO'] : NULL,
                'TIPO_COMPROMISO_DETALLE' => $detalle,
                'DETALLE_COMPRA' => isset($data['DETALLE_COMPRA']) ? $data['DETALLE_COMPRA'] : NULL,
                'INDICADOR' => isset($data['INDICADOR']) ? $data['INDICADOR'] : NULL,
                'FECHA_COMPROMISO' => isset($data['FECHA_COMPROMISO']) ? $data['FECHA_COMPROMISO'] == '-Seleccionar-' ? NULL : $data['FECHA_COMPROMISO'] : NULL,
                'GESTOR' => isset($data['GESTOR']) ? $data['GESTOR'] : NULL,
                'STATUS_COMPROMISO' => null,
                'ORDEN' => $orden + 1
            ];
            $newid = DB::table('WEBVPC_COVID19_CUMPLIMIENTO')->insertGetId($cumplimiento);

            DB::table('WEBVPC_ULTIMOS_CAMBIOS')->insert([
                'MENSAJE' => 'Se añadió el compromiso #' . $newid,
                'USUARIO'  => $registro,
                'FECHA'  => $now,
                'FECHA_TEXTO'  => $fechaTexto,
                'NOMBRE_USUARIO'  => $nombre,
                'COD_UNICO' => $dataObj->cu,
                'ID_COVID' => $dataObj->ID,
                'TIPO' => 'COMPROMISO',
                'VARIABLE' => 'ID',
                'VALOR_ANTERIOR' => null,
                'VALOR_ACTUAL' => $newid,
                'ID_COMPROMISO' => $newid
            ]);
            DB::commit();
            $cumplimiento['ID'] = $newid;
            $status['data'] = (object) $cumplimiento;
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status['status'] = false;
            DB::rollback();
        }
        return $status;
    }
    function updateCumplimiento($dataObj, $registro, $nombre, $eliminar)
    {
        $status = true;
        DB::beginTransaction();
        $mensaje = "";
        $now = Carbon::now();
        $fechaStr = $now->toDateString();
        $fechaAR = explode("-", $fechaStr);
        $fechaTexto = $fechaAR[2] . ' de ' . ucwords($now->format('F')) . ' de ' . $fechaAR[0];
        try {
            $dataAnterior =  DB::table('WEBVPC_COVID19_CUMPLIMIENTO')->where([
                'COD_UNICO' => $dataObj->cu,
                'ID' => $dataObj->ID,
            ])->select()->first();

            DB::table('WEBVPC_COVID19_CUMPLIMIENTO')->where([
                'COD_UNICO' => $dataObj->cu,
                'ID' => $dataObj->ID,
            ])->update([
                "FECHA_COMPROMISO" => $eliminar ? (($dataObj->FECHA_COMPROMISO == null || $dataObj->FECHA_COMPROMISO == '') ? $dataAnterior->FECHA_COMPROMISO : $dataObj->FECHA_COMPROMISO) : $dataAnterior->FECHA_COMPROMISO,
                "STATUS_COMPROMISO" => ($dataObj->STATUS_COMPROMISO == null || $dataObj->STATUS_COMPROMISO == '') ? $dataAnterior->STATUS_COMPROMISO : $dataObj->STATUS_COMPROMISO,
                'USUARIO_MODIFICACION' => $registro,
            ]);
            if($eliminar){
                if ($dataObj->FECHA_COMPROMISO != null || $dataObj->FECHA_COMPROMISO != '') {
                    $fecha1 = Carbon::parse($dataObj->FECHA_COMPROMISO);
                    $fechaStr = $fecha1->toDateString();
                    $fechaAR = explode("-", $fechaStr);
                    $fechaNueva = $fechaAR[2] . ' de ' . ucwords($fecha1->format('F')) . ' de ' . $fechaAR[0];
    
                    $fecha2 = Carbon::parse($dataAnterior->FECHA_COMPROMISO);
                    $fechaStr = $fecha2->toDateString();
                    $fechaAR = explode("-", $fechaStr);
                    $fechaAnterior = $fechaAR[2] . ' de ' . ucwords($fecha2->format('F')) . ' de ' . $fechaAR[0];
                    if ($fechaNueva != $fechaAnterior) {
                        $mensaje = 'Se cambió la fecha de ' . $fechaAnterior . ' a ' . $fechaNueva . ' del compromiso #' . ($dataObj->ID);
                        DB::table('WEBVPC_ULTIMOS_CAMBIOS')->insert([
                            'MENSAJE' => $mensaje,
                            'USUARIO'  => $registro,
                            'FECHA'  => $now,
                            'FECHA_TEXTO'  => $fechaTexto,
                            'NOMBRE_USUARIO'  => $nombre,
                            'COD_UNICO' => $dataObj->cu,
                            'ID_COVID' => $dataObj->ID_COVID,
                            'TIPO' => 'COMPROMISO',
                            'VARIABLE' => 'FECHA',
                            'VALOR_ANTERIOR' => $fechaAnterior,
                            'VALOR_ACTUAL' => $fechaNueva,
                            'ID_COMPROMISO' => $dataObj->ID
                        ]);
                    }
                }
            }
            
            if (($dataObj->STATUS_COMPROMISO) != ($dataAnterior->STATUS_COMPROMISO)) {
                if (($dataObj->STATUS_COMPROMISO == null || $dataObj->STATUS_COMPROMISO == '') && ($dataAnterior->STATUS_COMPROMISO != null || $dataAnterior->STATUS_COMPROMISO != '')) {
                    $mensaje = 'Se cambió el status de ' . ($dataAnterior->STATUS_COMPROMISO) . ' a sin respuesta del compromiso #' . ($dataObj->ID);
                    DB::table('WEBVPC_ULTIMOS_CAMBIOS')->insert([
                        'MENSAJE' => $mensaje,
                        'USUARIO'  => $registro,
                        'FECHA'  => $now,
                        'FECHA_TEXTO'  => $fechaTexto,
                        'NOMBRE_USUARIO'  => $nombre,
                        'COD_UNICO' => $dataObj->cu,
                        'ID_COVID' => $dataObj->ID_COVID,
                        'TIPO' => 'COMPROMISO',
                        'VARIABLE' => 'STATUS',
                        'VALOR_ANTERIOR' => ($dataAnterior->STATUS_COMPROMISO),
                        'VALOR_ACTUAL' => null,
                        'ID_COMPROMISO' => ($dataObj->ID)
                    ]);
                } elseif (($dataObj->STATUS_COMPROMISO != null || $dataObj->STATUS_COMPROMISO != '') && ($dataAnterior->STATUS_COMPROMISO == null || $dataAnterior->STATUS_COMPROMISO == '')) {
                    $mensaje = 'Se cambió el status de sin respuesta a ' . ($dataObj->STATUS_COMPROMISO) . ' del compromiso #' . ($dataObj->ID);
                    DB::table('WEBVPC_ULTIMOS_CAMBIOS')->insert([
                        'MENSAJE' => $mensaje,
                        'USUARIO'  => $registro,
                        'FECHA'  => $now,
                        'FECHA_TEXTO'  => $fechaTexto,
                        'NOMBRE_USUARIO'  => $nombre,
                        'COD_UNICO' => $dataObj->cu,
                        'ID_COVID' => $dataObj->ID_COVID,
                        'TIPO' => 'COMPROMISO',
                        'VARIABLE' => 'STATUS',
                        'VALOR_ANTERIOR' => null,
                        'VALOR_ACTUAL' => ($dataObj->STATUS_COMPROMISO),
                        'ID_COMPROMISO' => ($dataObj->ID)
                    ]);
                } else {
                    $mensaje = 'Se cambió el status de ' . ($dataAnterior->STATUS_COMPROMISO) . ' a ' . ($dataObj->STATUS_COMPROMISO) . ' del compromiso #' . ($dataObj->ID);
                    DB::table('WEBVPC_ULTIMOS_CAMBIOS')->insert([
                        'MENSAJE' => $mensaje,
                        'USUARIO'  => $registro,
                        'FECHA'  => $now,
                        'FECHA_TEXTO'  => $fechaTexto,
                        'NOMBRE_USUARIO'  => $nombre,
                        'COD_UNICO' => $dataObj->cu,
                        'ID_COVID' => $dataObj->ID_COVID,
                        'TIPO' => 'COMPROMISO',
                        'VARIABLE' => 'STATUS',
                        'VALOR_ANTERIOR' => ($dataAnterior->STATUS_COMPROMISO),
                        'VALOR_ACTUAL' => ($dataObj->STATUS_COMPROMISO),
                        'ID_COMPROMISO' => ($dataObj->ID)
                    ]);
                }
            }
            ////////////////////////////// PARTE DE NOTIFICACIONES ////////////////////////////////////
            $cu = $dataObj->cu;

            $datosCliente =  eCliente::getDatosClienteVISITAMECOVID($cu);
            $areaEliminacion = $dataAnterior->TIPO_GESTION;
            if ($areaEliminacion == 'Comercial') {
                $segmento = $datosCliente->SEGMENTO;
                if ($segmento == 'MEDIANA EMPRESA') {
                    $arbol = DB::table('ARBOL_DIARIO_2')
                        ->select('CODIGO_JEFE')->where('COD_SECT_LARGO', $datosCliente->COD_SECTORISTA)->first();
                    $usuarios = DB::table('WEBVPC_USUARIO as WU')
                        ->select('REGISTRO')
                        ->where('ROL', Usuario::ROL_JEFATURA_BE)->where('FLAG_ACTIVO', 1)->where('ID_GC', $arbol->CODIGO_JEFE)->get();
                }
            } else if ($areaEliminacion == 'Admisión') {
                $arrayIn = array(
                    Usuario::ROL_JEFE_RIESGOS,
                    Usuario::ROL_GERENTE_RIESGOS,
                    Usuario::ROL_SUBGERENTE_RIESGOS
                );
                $usuarios = DB::table('WEBVPC_USUARIO')->select('REGISTRO')->whereIn('ROL', $arrayIn)->where('FLAG_ACTIVO', 1)->get();
            } else if ($areaEliminacion == 'Seguimiento comercial') {
                $usuarios = DB::table('WEBVPC_USUARIO')->select('REGISTRO')->where('ROL', Usuario::ROL_EJECUTIVO_SEGUIMIENTO_CARTERA)->where('FLAG_ACTIVO', 1)->get();
            } else if ($areaEliminacion == 'GYS') {
                $usuarios = DB::table('WEBVPC_USUARIO')->select('REGISTRO')->whereIn(
                    'ROL',
                    array(Usuario::ROL_ANALISTA_GYS, Usuario::ROL_SUBGERENTE_GYS)
                )->where('FLAG_ACTIVO', 1)->get();
            }
            foreach ($usuarios as $usuario) {
                $notification = new eNotification();
                $notification->setValue('_idTipo', '12');
                $notification->setValue('_registro', $usuario->REGISTRO);
                $notification->setValue('_contenido', strtoupper($datosCliente->NOMBRE) . ": " . $mensaje . ". Puede revisar el cumplimiento aquí");
                $notification->setValue('_fechaNotificacion', Carbon::now());
                $notification->setValue('_flgLeido', '0');
                $notification->setValue('_fechaLeido', null);
                $notification->setValue('_valor1', null);
                $notification->setValue('_valor2', null);
                $notification->setValue('_valor3', null);
                $notification->setValue('_valor4', null);
                $notification->setValue('_url', 'alertascartera/detalle?cu=' . $cu);
                $notification = $notification->setValueToTable();
                DB::table('T_WEB_NOTIFICACIONES')->insert($notification);
            }
            ///////////////////////////////////////////////////////////////////////////////////////////
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }
    function getDatosByCliente2($id)
    {
        return DB::table("WEBVPC_COVID19")
            ->select("*", DB::Raw('CONVERT(DATE,FECHA_REGISTRO,3) AS FECHA_VISITA'))
            ->where('ID_VISITA', '=', $id)
            ->orderby('ID', 'desc')->get()->first();
    }
    function getDatosByClienteAprobado($cu)
    {

        $sql = DB::table("WEBVPC_COVID19")
            ->select("*", DB::Raw('CONVERT(DATE,FECHA_REGISTRO,3) AS FECHA_VISITA'))
            ->where('COD_UNICO', '=', $cu)
            ->where('ESTADO_APROBACION', '=', '1')
            ->orderby('ID', 'desc')->get()->first();
        return $sql;
    }
    function getDatosByCliente2byID($id)
    {
        return DB::table("WEBVPC_COVID19")
            ->select("*", DB::Raw('CONVERT(DATE,FECHA_REGISTRO,3) AS FECHA_VISITA'))
            ->where(["ID_VISITA" => $id])
            ->get()->first();
    }
    function ultimoCambioEstrategiaRiesgos($id)
    {
        $res = false;
        $sql = DB::table("WEBVPC_ULTIMOS_CAMBIOS")
            ->where(["ID_COVID" => $id])
            ->where(["TIPO" => "ESTRATEGIA"])
            ->orderBy('ID', 'desc')
            ->get()->first();
        if ($sql != null) {
            $usuario = Usuario::getUsuario($sql->USUARIO);
            $res = in_array($usuario->ROL, array(
                Usuario::ROL_JEFE_RIESGOS,
                Usuario::ROL_ANALISTA_RIESGOS_ADMISION,
                Usuario::ROL_GERENTE_RIESGOS,
                Usuario::ROL_SUBGERENTE_RIESGOS,
                Usuario::ROL_ANALISTA_GYS,
                Usuario::ROL_SUBGERENTE_GYS,
            ));
        }
        return $res;
    }

    function getDatosCumplimientobyID($id)
    {
        $now = Carbon::now();
        $sql = DB::table("WEBVPC_COVID19_CUMPLIMIENTO")
            ->select()
            ->where("ID_VISITA", "=", $id)->where("STATUS_COMPROMISO", '!=', 'ELIMINADO')->where("STATUS_COMPROMISO", '!=', 'NO CONFIRMADO');
        $sqlNull = DB::table("WEBVPC_COVID19_CUMPLIMIENTO")
            ->select()
            ->where("ID_VISITA", "=", $id)->whereNull("STATUS_COMPROMISO");
        $sqlFinal = $sql->union($sqlNull);
        return $sqlFinal->get();
    }
    function getCumplimientoById($id)
    {
        return DB::table("WEBVPC_COVID19_CUMPLIMIENTO")
            ->select()
            ->where(["ID" => $id])
            ->first();
    }
    function getDatosCumplimiento($id, $codunico)
    {
        $now = Carbon::now();
        $sql = DB::table("WEBVPC_COVID19_CUMPLIMIENTO")
            ->select()
            ->where("COD_UNICO", "=", $codunico)->where("STATUS_COMPROMISO", '!=', 'ELIMINADO')->where("STATUS_COMPROMISO", '!=', 'NO CONFIRMADO')->where("ID_COVID", "=", $id);
        $sqlNull = DB::table("WEBVPC_COVID19_CUMPLIMIENTO")
            ->select()
            ->where("COD_UNICO", "=", $codunico)->whereNull("STATUS_COMPROMISO")->where("ID_COVID", "=", $id);
        $sqlFinal = $sql->union($sqlNull);
        return $sqlFinal->get();
    }

    function getDatosCumplimientoIncompletos($id, $codunico)
    {
        $now = Carbon::now();
        $sql = DB::table("WEBVPC_COVID19_CUMPLIMIENTO")
            ->select(DB::raw('*'))
            ->where("COD_UNICO", "=", $codunico)->where("ID_COVID", "=", $id)
            ->whereNull('STATUS_COMPROMISO')->get();
        return $sql;
    }
    function getDatosByCliente($codunico)
    {
        return DB::table("WEBVPC_COVID19")->select("*", DB::Raw('CONVERT(DATE,FECHA_REGISTRO,3) AS FECHA_VISITA'))->where("COD_UNICO", "=", $codunico)->get()->first();
    }
}
