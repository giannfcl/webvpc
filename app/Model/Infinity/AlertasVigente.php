<?php
namespace App\Model\Infinity;
use DB;
use Log;
use Illuminate\Database\Eloquent\Model;
use App\Model\Infinity\Cliente as Cliente;
use App\Entity\Infinity\Politica as Politica;
use App\Entity\Infinity\PoliticaProceso as PoliticaProceso;
use Jenssegers\Date\Date as Carbon;

class AlertasVigente extends Model
{
    static function getMensajesAntiguos($id)
	{   
        $sql = DB::table('WEBBE_INFINITY_SEG_VIG_CASOS as A')
                ->select('A.ID AS ID_CASO',DB::Raw('(CASE WHEN F.FECHA_REGISTRO IS NOT NULL THEN 3 WHEN D.FECHA_REGISTRO IS NOT NULL THEN 2 WHEN B.FECHA_REGISTRO IS NOT NULL THEN 1 END) AS CANT_RES'),'B.FECHA_REGISTRO AS FEC_RES1','B.MENSAJE AS RES1_EN','C.MENSAJE AS RES1_COM','C.ESTADO AS RES1_ESTADO','D.FECHA_REGISTRO AS FEC_RES2','D.MENSAJE AS RES2_EN','E.MENSAJE AS RES2_COM','E.ESTADO AS RES2_ESTADO','F.FECHA_REGISTRO AS FEC_RES3','F.MENSAJE AS RES3_EN','G.MENSAJE AS RES3_COM','G.ESTADO AS RES3_ESTADO','B.ACCION AS ACC1_EN','D.ACCION AS ACC2_EN','F.ACCION AS ACC3_EN')
                ->leftjoin(DB::raw("(SELECT * FROM WEBBE_INFINITY_SEG_VIG_MENSAJE WHERE ORDEN = 1) AS B"), function($join)
                {
                    $join->on('A.ID', '=', 'B.ID_CASO');
                })
                ->leftjoin(DB::raw("(SELECT * FROM WEBBE_INFINITY_SEG_VIG_MENSAJE WHERE ORDEN = 2) AS C"), function($join)
                {
                    $join->on('A.ID', '=', 'C.ID_CASO');
                })
                ->leftjoin(DB::raw("(SELECT * FROM WEBBE_INFINITY_SEG_VIG_MENSAJE WHERE ORDEN = 3) AS D"), function($join)
                {
                    $join->on('A.ID', '=', 'D.ID_CASO');
                })
                ->leftjoin(DB::raw("(SELECT * FROM WEBBE_INFINITY_SEG_VIG_MENSAJE WHERE ORDEN = 4) AS E"), function($join)
                {
                    $join->on('A.ID', '=', 'E.ID_CASO');
                })
                ->leftjoin(DB::raw("(SELECT * FROM WEBBE_INFINITY_SEG_VIG_MENSAJE WHERE ORDEN = 5) AS F"), function($join)
                {
                    $join->on('A.ID', '=', 'F.ID_CASO');
                })
                ->leftjoin(DB::raw("(SELECT * FROM WEBBE_INFINITY_SEG_VIG_MENSAJE WHERE ORDEN = 6) AS G"), function($join)
                {
                    $join->on('A.ID', '=', 'G.ID_CASO');
                })
                ->where('A.ID', $id)
                ->get();
        return $sql;
    }
    static function getMensajes($id)
	{
        $sql = DB::table('WEBBE_INFINITY_SEG_VIG_MENSAJE as A')
                ->select('A.ID_CASO AS ID_CASO','A.ORDEN AS ORDEN','A.TIPO AS TIPO','A.GESTION AS GESTION','A.ACCION AS ACCION','A.MENSAJE AS MENSAJE','A.REGISTRO AS REGISTRO','A.FECHA_REGISTRO AS FECHA_REGISTRO','A.ESTADO AS ESTADO')
                ->where('A.ID_CASO', $id)
                ->get();
        return $sql;
    }
    static function getAntGestiones($cod_unico, $periodo)
	{
        $sql = DB::table('WEBBE_INFINITY_SEG_VIG_CASOS as A')
                ->select('A.ID AS ID','A.PERIODO AS PERIODO','A.COD_UNICO AS COD_UNICO','A.MOTIVO AS MOTIVO','A.FECHA_REGISTRO AS FECHA_REGISTRO','A.FLG_GESTIONADO AS FLG_GESTIONADO','A.FECHA_GESTIONADO AS FECHA_GESTIONADO', DB::Raw("CASE WHEN SUBSTRING(PERIODO,5,2) ='01' THEN 'ENERO '+SUBSTRING(PERIODO,1,4) WHEN SUBSTRING(PERIODO,5,2) ='02' THEN 'FEBRERO '+SUBSTRING(PERIODO,1,4) WHEN SUBSTRING(PERIODO,5,2) ='03' THEN 'MARZO '+SUBSTRING(PERIODO,1,4) WHEN SUBSTRING(PERIODO,5,2) ='04' THEN 'ABRIL '+SUBSTRING(PERIODO,1,4) WHEN SUBSTRING(PERIODO,5,2) ='05' THEN 'MAYO '+SUBSTRING(PERIODO,1,4) WHEN SUBSTRING(PERIODO,5,2) ='06' THEN 'JUNIO '+SUBSTRING(PERIODO,1,4) WHEN SUBSTRING(PERIODO,5,2) ='07' THEN 'JULIO '+SUBSTRING(PERIODO,1,4) WHEN SUBSTRING(PERIODO,5,2) ='08' THEN 'AGOSTO '+SUBSTRING(PERIODO,1,4) WHEN SUBSTRING(PERIODO,5,2) ='09' THEN 'SETIEMBRE '+SUBSTRING(PERIODO,1,4) WHEN SUBSTRING(PERIODO,5,2) ='10' THEN 'OCTUBRE '+SUBSTRING(PERIODO,1,4) WHEN SUBSTRING(PERIODO,5,2) ='11' THEN 'NOVIEMBRE '+SUBSTRING(PERIODO,1,4) WHEN SUBSTRING(PERIODO,5,2) ='12' THEN 'DICIEMBRE '+SUBSTRING(PERIODO,1,4) END TEXTO_ANO"))
                ->where('A.COD_UNICO', $cod_unico)
                ->where('A.PERIODO', '<' ,$periodo)
                ->get();
        return $sql;
    }
    
    static function getGestionActual($cod_unico, $periodo)
	{
        $sql = DB::table('WEBBE_INFINITY_SEG_VIG_CASOS as A')
                ->select('A.ID AS ID','A.PERIODO AS PERIODO','A.COD_UNICO AS COD_UNICO','A.MOTIVO AS MOTIVO','A.FECHA_REGISTRO AS FECHA_REGISTRO','A.FLG_GESTIONADO AS FLG_GESTIONADO','A.FECHA_GESTIONADO AS FECHA_GESTIONADO', DB::Raw("CASE WHEN SUBSTRING(PERIODO,5,2) ='01' THEN 'ENERO '+SUBSTRING(PERIODO,1,4) WHEN SUBSTRING(PERIODO,5,2) ='02' THEN 'FEBRERO '+SUBSTRING(PERIODO,1,4) WHEN SUBSTRING(PERIODO,5,2) ='03' THEN 'MARZO '+SUBSTRING(PERIODO,1,4) WHEN SUBSTRING(PERIODO,5,2) ='04' THEN 'ABRIL '+SUBSTRING(PERIODO,1,4) WHEN SUBSTRING(PERIODO,5,2) ='05' THEN 'MAYO '+SUBSTRING(PERIODO,1,4) WHEN SUBSTRING(PERIODO,5,2) ='06' THEN 'JUNIO '+SUBSTRING(PERIODO,1,4) WHEN SUBSTRING(PERIODO,5,2) ='07' THEN 'JULIO '+SUBSTRING(PERIODO,1,4) WHEN SUBSTRING(PERIODO,5,2) ='08' THEN 'AGOSTO '+SUBSTRING(PERIODO,1,4) WHEN SUBSTRING(PERIODO,5,2) ='09' THEN 'SETIEMBRE '+SUBSTRING(PERIODO,1,4) WHEN SUBSTRING(PERIODO,5,2) ='10' THEN 'OCTUBRE '+SUBSTRING(PERIODO,1,4) WHEN SUBSTRING(PERIODO,5,2) ='11' THEN 'NOVIEMBRE '+SUBSTRING(PERIODO,1,4) WHEN SUBSTRING(PERIODO,5,2) ='12' THEN 'DICIEMBRE '+SUBSTRING(PERIODO,1,4) END TEXTO_ANO"),"A.MENSAJE")
                ->where('A.COD_UNICO', $cod_unico)
                ->where('A.PERIODO', DB::Raw("(SELECT MAX(PERIODO) FROM WEBBE_INFINITY_SEG_VIG_CASOS)"))
                ->get();
        return $sql;
    }

    static function addMensaje($mensaje, $notificaciones,$clirevision=null)
    {
        // dd($mensaje, $notificaciones,$clirevision,$mensaje['ORDEN']);
        DB::beginTransaction();
        $status = true;
        try {
            DB::table('WEBBE_INFINITY_SEG_VIG_MENSAJE')->insert($mensaje);
            foreach ($notificaciones as $notificacion) {
                DB::table('T_WEB_NOTIFICACIONES')->insert($notificacion);
            }
            if (isset($clirevision)) {
                DB::table('WEBBE_INFINITY_CLIENTE_REVISION')->insert($clirevision);
            }
            if ($mensaje['ORDEN']==1) {
                DB::table('WEBBE_INFINITY_SEG_VIG_CASOS')
                        ->where('ID', '=', $mensaje['ID_CASO'])
                        ->update([
                            'FLG_GESTIONADO' => 1,
                            'FECHA_GESTIONADO' => Carbon::now()
                        ]);
            }
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }

    function getNotificacion($codunico,$idcaso,$rol)
    {
        $sql = DB::table(DB::Raw("( SELECT NOMBRE, RTRIM(LTRIM(COD_SECTORISTA)) COD_SECTORISTA, COD_UNICO FROM WEBBE_INFINITY_CLIENTE WHERE COD_UNICO = '$codunico' AND PERIODO = (SELECT MAX(PERIODO) FROM WEBBE_INFINITY_CLIENTE) ) AS A"))
            ->select(DB::Raw("'8' AS ID_TIPO_NOTIFICACION"), "REGISTRO AS REGISTRO_EN", DB::Raw("(REPLACE(C.CONTENIDO, '[VALOR_1]', A.NOMBRE)) AS CONTENIDO"),DB::Raw("GETDATE() AS FECHA_NOTIFICACION"),
            DB::Raw("0 AS FLG_LEIDO"), DB::Raw("NULL AS FECHA_LEIDO"), "A.NOMBRE AS VALOR_1", "D.ID AS VALOR_2", "A.COD_UNICO AS VALOR_3", DB::Raw("(CASE WHEN D.ORDEN IS NOT NULL THEN D.ORDEN+1 ELSE 1 END) AS VALOR_4"),DB::Raw("NULL AS VALOR_5"),DB::Raw("NULL AS VALOR_6"),DB::Raw("NULL AS VALOR_7"),DB::Raw("NULL AS VALOR_8"),DB::Raw("NULL AS VALOR_9"),DB::Raw("NULL AS VALOR_10"),DB::Raw("NULL AS VALOR_11"),DB::Raw("NULL AS VALOR_12"),DB::Raw("NULL AS VALOR_13"),DB::Raw("NULL AS VALOR_14"),DB::Raw("NULL AS VALOR_15"),DB::Raw("NULL AS VALOR_16"),DB::Raw("NULL AS VALOR_17"),DB::Raw("NULL AS VALOR_18"),DB::Raw("NULL AS VALOR_19"),DB::Raw("NULL AS VALOR_20"), DB::Raw("('alertascartera/detalle?cu='+A.COD_UNICO) AS URL"))
            ->leftjoin(DB::raw("(SELECT * FROM T_WEB_NOTIFICACIONES_CATALOGO WHERE ID_NOTIFICACION = 8) AS C"), function($join)
            {
                $join->on(DB::Raw("1"), '=', DB::Raw("1"));
            })
            ->leftjoin(DB::raw("(SELECT '$idcaso' as ID, ISNULL(MAX(ORDEN),1) ORDEN FROM WEBBE_INFINITY_SEG_VIG_MENSAJE WHERE ID_CASO = '$idcaso') AS D"), function($join)
            {
                $join->on(DB::Raw("1"), '=', DB::Raw("1"));
            });
            if (in_array($rol, array('20','21'))) {
                $sql = $sql->leftjoin(DB::raw("(SELECT * FROM WEBVPC_USUARIO WHERE ROL IN ('38','52', '49') AND FLAG_ACTIVO = 1) AS E"), function($join)
                {
                    $join->on(DB::Raw("1"), '=', DB::Raw("1"));
                });
            }elseif(in_array($rol, array('25','30','34','35','38','42','49','52'))){
                $sql = $sql->leftjoin(DB::raw("(SELECT RTRIM(LTRIM(LOGIN)) REGISTRO, RTRIM(LTRIM(COD_SECT_LARGO)) COD_SECTORISTA FROM ARBOL_DIARIO_2) AS E"), function($join)
                {
                    $join->on(DB::Raw("A.COD_SECTORISTA"), '=', DB::Raw("E.COD_SECTORISTA"));
                });
            }
            // dd($sql->toSql());

            return $sql->get();
    }

    function getdatosCaso($idcaso)
    {
        return DB::Table(DB::Raw("(SELECT CASE WHEN ISNULL(MAX(ORDEN),0)%2=0 THEN 'EN' ELSE 'COMITE' END TOCA_RESPONDER, ISNULL(MAX(ORDEN),0)+1 SIG_ORDEN FROM WEBBE_INFINITY_SEG_VIG_MENSAJE WHERE ID_CASO = '$idcaso') as DC"))->get()->first();
    }
}
?>