<?php
namespace App\Model\Infinity;
use DB;
use Log;
use Illuminate\Database\Eloquent\Model;
use App\Model\Infinity\Cliente as Cliente;
use App\Entity\Infinity\Politica as Politica;
use App\Entity\Infinity\PoliticaProceso as PoliticaProceso;
use Jenssegers\Date\Date as Carbon;

class Alertacartera extends Model
{
	static function getfechaSegActividad()
	{
		$sql=DB::table("WEBBE_INFINITY_INFO_DIARIA_CLI")
				->select(DB::Raw("CONVERT(VARCHAR(10), MAX(FECHA), 103) AS FECHA_ACT"))
				->get()->first();
				// dd($sql);
        return $sql;
	}
	static function getBancas($banca=null)
	{
		$selected = '';
		$sql=DB::table('T_WEB_ARBOL_DIARIO')->select('TIPO_BANCA')->whereNotNull('TIPO_BANCA')->groupBy('TIPO_BANCA')->get();
		if($banca==null){
			$selected = 'selected';
		}
		$combo = "<option value=''>Todos...</option>";
		foreach ($sql as $key => $tipo) {
			if(($tipo->TIPO_BANCA) === $banca){
				$selected = 'selected';
			}else{
				$selected = '';
			}
			$combo .= "<option $selected value='".$tipo->TIPO_BANCA."'>".$tipo->TIPO_BANCA."</option>";
		}
		return $combo;
	}

	static function getZonales($data)
	{
		$selected = '';
		if($data['banca']==null || $data['banca']=='MEDIANA EMPRESA'){
			$combo = "<option value=''>Todos...</option><option selected value='ZONAL SAN ISIDRO'>ZONAL SAN ISIDRO</option>";
		}else{
			$sql=DB::table('T_WEB_ARBOL_DIARIO')->select('NOMBRE_ZONAL')->where('TIPO_BANCA','=',$data['banca'])->groupBy('NOMBRE_ZONAL')->get();
			if(count($sql)==0){
				$selected = 'selected';
			}
			$combo = "<option $selected value=''>Todos...</option>";
			foreach ($sql as $key => $zonales) {
				if(($zonales->NOMBRE_ZONAL) === $data['zonal']){
					$selected = 'selected';
				}else{
					$selected = '';
				}
				$combo .= "<option $selected value='".$zonales->NOMBRE_ZONAL."'>".$zonales->NOMBRE_ZONAL."</option>";
			}
		}
		return $combo;
	}

	static function getPDF_Meses()
	{
		$sql=DB::table(DB::raw("(SELECT MES
								FROM (SELECT 
								SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),1,4) + '/' + SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),5,2) +  ' - ' + (CASE WHEN CAST(SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),5,2) AS INTEGER) = 1 THEN 'ENERO'
									WHEN CAST(SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),5,2) AS INTEGER) = 2 THEN 'FEBRERO'
									WHEN CAST(SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),5,2) AS INTEGER) = 3 THEN 'MARZO'
									WHEN CAST(SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),5,2) AS INTEGER) = 4 THEN 'ABRIL'
									WHEN CAST(SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),5,2) AS INTEGER) = 5 THEN 'MAYO'
									WHEN CAST(SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),5,2) AS INTEGER) = 6 THEN 'JUNIO'
									WHEN CAST(SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),5,2) AS INTEGER) = 7 THEN 'JULIO'
									WHEN CAST(SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),5,2) AS INTEGER) = 8 THEN 'AGOSTO'
									WHEN CAST(SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),5,2) AS INTEGER) = 9 THEN 'SETIEMBRE'
									WHEN CAST(SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),5,2) AS INTEGER) = 10 THEN 'OCTUBRE'
									WHEN CAST(SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),5,2) AS INTEGER) = 11 THEN 'NOVIEMBRE'
									WHEN CAST(SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),5,2) AS INTEGER) = 12 THEN 'DICIEMBRE' END) MES,
								CASE WHEN CAST(SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),7,2) AS INTEGER) >= 22 THEN '4° SEMANA' 
									WHEN CAST(SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),7,2) AS INTEGER) >= 15 THEN '3° SEMANA'
									WHEN CAST(SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),7,2) AS INTEGER) >= 8 THEN '2° SEMANA'
									ELSE '1° SEMANA' END SEMANA,
								CAST(A.FECHA_REGISTRO AS DATE) FECHA_REGISTRO, 
								D.NOMBRE_JEFE,
								A.COD_UNICO CU, 
								B.NOMBRE CLIENTE, 
								A.COMENTARIO ACUERDO, 
								A.COMPROMISO_PAGO FECHA_COMPROMISO, 
								D.ENCARGADO RESPONSABLE
														FROM (SELECT *
																	FROM dbo.WEBBE_INFINITY_SEG_ACTIVIDAD A
																	WHERE RESPONSABLE = 'COMITÉ GESTIÓN PORTAFOLIO'
															) A
														LEFT JOIN (SELECT * 
																	FROM WEBBE_INFINITY_INFO_DIARIA_CLI B 
																	) B ON A.COD_UNICO = B.COD_UNICO
														LEFT JOIN (SELECT RTRIM(LTRIM(LOGIN)) REGISTRO, RTRIM(LTRIM(COD_SECT_LARGO)) COD_SECTORISTA FROM ARBOL_DIARIO_2) C ON B.CODSECTORISTA = C.COD_SECTORISTA
														LEFT JOIN T_WEB_ARBOL_DIARIO D ON C.REGISTRO = D.REGISTRO) A
								GROUP BY MES) as A"))
								->orderBy('MES','DESC')->get();
		return $sql;
	
	}	

	static function getPDF_Semanas($mes)
	{
		$sql=DB::table(DB::raw("(SELECT SEMANA, MES
								FROM (SELECT 
								SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),1,4) + '/' + SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),5,2) +  ' - ' + (CASE WHEN CAST(SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),5,2) AS INTEGER) = 1 THEN 'ENERO'
									WHEN CAST(SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),5,2) AS INTEGER) = 2 THEN 'FEBRERO'
									WHEN CAST(SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),5,2) AS INTEGER) = 3 THEN 'MARZO'
									WHEN CAST(SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),5,2) AS INTEGER) = 4 THEN 'ABRIL'
									WHEN CAST(SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),5,2) AS INTEGER) = 5 THEN 'MAYO'
									WHEN CAST(SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),5,2) AS INTEGER) = 6 THEN 'JUNIO'
									WHEN CAST(SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),5,2) AS INTEGER) = 7 THEN 'JULIO'
									WHEN CAST(SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),5,2) AS INTEGER) = 8 THEN 'AGOSTO'
									WHEN CAST(SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),5,2) AS INTEGER) = 9 THEN 'SETIEMBRE'
									WHEN CAST(SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),5,2) AS INTEGER) = 10 THEN 'OCTUBRE'
									WHEN CAST(SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),5,2) AS INTEGER) = 11 THEN 'NOVIEMBRE'
									WHEN CAST(SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),5,2) AS INTEGER) = 12 THEN 'DICIEMBRE' END) MES,
								CASE WHEN CAST(SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),7,2) AS INTEGER) >= 22 THEN '4° SEMANA' 
									WHEN CAST(SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),7,2) AS INTEGER) >= 15 THEN '3° SEMANA'
									WHEN CAST(SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),7,2) AS INTEGER) >= 8 THEN '2° SEMANA'
									ELSE '1° SEMANA' END SEMANA,
								CAST(A.FECHA_REGISTRO AS DATE) FECHA_REGISTRO, 
								D.NOMBRE_JEFE,
								A.COD_UNICO CU, 
								B.NOMBRE CLIENTE, 
								A.COMENTARIO ACUERDO, 
								A.COMPROMISO_PAGO FECHA_COMPROMISO, 
								D.ENCARGADO RESPONSABLE
														FROM (SELECT *
																	FROM dbo.WEBBE_INFINITY_SEG_ACTIVIDAD A
																	WHERE RESPONSABLE = 'COMITÉ GESTIÓN PORTAFOLIO'
															) A
														LEFT JOIN (SELECT * 
																	FROM WEBBE_INFINITY_INFO_DIARIA_CLI B 
																	) B ON A.COD_UNICO = B.COD_UNICO
														LEFT JOIN (SELECT RTRIM(LTRIM(LOGIN)) REGISTRO, RTRIM(LTRIM(COD_SECT_LARGO)) COD_SECTORISTA FROM ARBOL_DIARIO_2) C ON B.CODSECTORISTA = C.COD_SECTORISTA
														LEFT JOIN T_WEB_ARBOL_DIARIO D ON C.REGISTRO = D.REGISTRO) A
								GROUP BY SEMANA, MES) as A"))
								->where('MES', '=', $mes)
								->orderBy('SEMANA','ASC')->get();
		return $sql;
	
	}

	static function get_datosPdf($mes)
	{
		$sql=DB::table(DB::raw("(SELECT * 
								FROM (SELECT 
								SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),1,4) + '/' + SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),5,2) +  ' - ' + (CASE WHEN CAST(SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),5,2) AS INTEGER) = 1 THEN 'ENERO'
									WHEN CAST(SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),5,2) AS INTEGER) = 2 THEN 'FEBRERO'
									WHEN CAST(SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),5,2) AS INTEGER) = 3 THEN 'MARZO'
									WHEN CAST(SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),5,2) AS INTEGER) = 4 THEN 'ABRIL'
									WHEN CAST(SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),5,2) AS INTEGER) = 5 THEN 'MAYO'
									WHEN CAST(SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),5,2) AS INTEGER) = 6 THEN 'JUNIO'
									WHEN CAST(SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),5,2) AS INTEGER) = 7 THEN 'JULIO'
									WHEN CAST(SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),5,2) AS INTEGER) = 8 THEN 'AGOSTO'
									WHEN CAST(SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),5,2) AS INTEGER) = 9 THEN 'SETIEMBRE'
									WHEN CAST(SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),5,2) AS INTEGER) = 10 THEN 'OCTUBRE'
									WHEN CAST(SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),5,2) AS INTEGER) = 11 THEN 'NOVIEMBRE'
									WHEN CAST(SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),5,2) AS INTEGER) = 12 THEN 'DICIEMBRE' END) MES,
								CASE WHEN CAST(SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),7,2) AS INTEGER) >= 22 THEN '4° SEMANA' 
									WHEN CAST(SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),7,2) AS INTEGER) >= 15 THEN '3° SEMANA'
									WHEN CAST(SUBSTRING(CONVERT(VARCHAR(8), A.FECHA_REGISTRO, 112),7,2) AS INTEGER) >= 8 THEN '2° SEMANA'
									ELSE '1° SEMANA' END SEMANA,
								CAST(A.FECHA_REGISTRO AS DATE) FECHA_REGISTRO, 
								D.NOMBRE_JEFE,
								A.COD_UNICO CU, 
								B.NOMBRE CLIENTE, 
								A.COMENTARIO ACUERDO, 
								A.COMPROMISO_PAGO FECHA_COMPROMISO, 
								D.ENCARGADO RESPONSABLE
														FROM (SELECT *
																	FROM dbo.WEBBE_INFINITY_SEG_ACTIVIDAD A
																	WHERE RESPONSABLE = 'COMITÉ GESTIÓN PORTAFOLIO'
															) A
														LEFT JOIN (SELECT * 
																	FROM WEBBE_INFINITY_INFO_DIARIA_CLI B 
																	) B ON A.COD_UNICO = B.COD_UNICO
														LEFT JOIN (SELECT RTRIM(LTRIM(LOGIN)) REGISTRO, RTRIM(LTRIM(COD_SECT_LARGO)) COD_SECTORISTA FROM ARBOL_DIARIO_2) C ON B.CODSECTORISTA = C.COD_SECTORISTA
														LEFT JOIN T_WEB_ARBOL_DIARIO D ON C.REGISTRO = D.REGISTRO) A) as A"))
								->where('MES', '=', $mes)
								->orderBy('FECHA_REGISTRO','ASC')->get();
		return $sql;
	}

	static function getJefaturas($data)
	{
		$sql=DB::table('T_WEB_ARBOL_DIARIO')
				->select('NOMBRE_JEFE','REGISTRO')
				->where('TIPO_BANCA','=',$data['banca']==null?'MEDIANA EMPRESA':$data['banca'])
				->where('NOMBRE_ZONAL','=',$data['zonal']==null?'ZONAL SAN ISIDRO':$data['zonal'])
				->whereNotNull('NOMBRE_JEFE')
				->whereNull('ENCARGADO')
				->groupBy('NOMBRE_JEFE','REGISTRO')->get();
		$combo = "<option value=''>Todos...</option>";
		foreach ($sql as $key => $jefaturas) {
			if($data['jefatura']==$jefaturas->REGISTRO){
				$combo .= "<option selected value='".$jefaturas->REGISTRO."'>".$jefaturas->NOMBRE_JEFE."</option>";
			}else{
				$combo .= "<option value='".$jefaturas->REGISTRO."'>".$jefaturas->NOMBRE_JEFE."</option>";
			}
			
		}
		return $combo;
	}

	static function getEjecutivos($data)
	{
		$combo = "<option value='' selected>Todos...</option>";
		if ($data['zonal']=="ZONAL SAN ISIDRO") {
			if($data['jefatura']!=null){
				$sql=DB::table(DB::Raw("(SELECT TWAB2.REGISTRO REGISTRO_JEFE,TWAB.REGISTRO,TWAB.ENCARGADO FROM T_WEB_ARBOL_DIARIO TWAB
						LEFT JOIN T_WEB_ARBOL_DIARIO TWAB2
						ON TWAB.NOMBRE_JEFE = TWAB2.NOMBRE_JEFE AND TWAB2.ENCARGADO IS NULL
						WHERE TWAB.ENCARGADO IS NOT NULL) as WZO")
						)->where('REGISTRO_JEFE','=',$data['jefatura'])->get();
				if($sql!=null){
					foreach ($sql as $key => $encargados) {
						$combo .= "<option value='".$encargados->REGISTRO."'>".$encargados->ENCARGADO."</option>";
					}
				}
			}
		}else{
			$sql=DB::table("T_WEB_ARBOL_DIARIO")
						->whereNotNull("ENCARGADO")
						->where("NOMBRE_ZONAL","=",$data['zonal'])
						->get();
			foreach ($sql as $key => $encargados) {
				$combo .= "<option value='".$encargados->REGISTRO."'>".$encargados->ENCARGADO."</option>";
			}
		}
		return $combo;
	}

	static function getUsuarioTWarbol($registro)
    {
        $sql = DB::table('T_WEB_ARBOL_DIARIO')
            ->select()
            ->where('REGISTRO', '=', $registro)
            ->get()
            ->first();
        return $sql;
    }

    static function getJefeRegistro($nomjefe)
    {
        $sql=DB::table('T_WEB_ARBOL_DIARIO')
				->select('NOMBRE_JEFE','REGISTRO')
				->where('NOMBRE_JEFE','=',$nomjefe)
				->whereNotNull('NOMBRE_JEFE')
				->whereNull('ENCARGADO')
				->groupBy('NOMBRE_JEFE','REGISTRO')->first();
        return $sql;
    }

    static function getByID($id)
    {
        $sql=DB::table('T_WEB_ARBOL_DIARIO')
				->where('ID','=',$id)
				->get()
				->first();
        return $sql;
    }

    static function getProdDiario($cu)
    {
        $sql=DB::table('WEBBE_INFINITY_PROD_DIARIO')
				->where('COD_UNICO','=',$cu)
				->orderBy('NUM_DIAS_VENCIDOS','DESC')
				->get();
        return $sql;
    }

    static function getSegActividad($cu)
    {
        $sql=DB::table('WEBBE_INFINITY_SEG_ACTIVIDAD')
				->where('COD_UNICO','=',$cu)
				->orderBy('FECHA_ACTIVIDAD','DESC')
				->get();
        return $sql;
	}

	static function getSegActividadByid($id)
    {
        $sql=DB::table('WEBBE_INFINITY_SEG_ACTIVIDAD')
				->where('ID','=',$id)
				->get();
        return $sql;
	}

	static function CombosParaActividad($campo,$rol=null)
	{
		$sql = DB::table('T_WEBBE_INFINITY_ACT_COMBOS')
				->where('CAMPO',$campo);
				if ($campo=='RESPONSABLE' && in_array($rol, array('20','21'))) {
					$sql=$sql->where('OPCION','EN');
				}
		return $sql->get();
	}

	static function getDatosSegActividad($cu)
    {
        $sql=DB::table(DB::Raw("(
        			SELECT A.*, ISNULL((DEUDA_VENCIDA+DEUDA_ATRASADA)/NULLIF(DEUDA_TOTAL,0),0) PCT_DEUDA_PROBLEMA
        			FROM (
		                SELECT COD_UNICO,
		                MAX(CASE WHEN ORDEN = 1 THEN FECHA_VENCIMIENTO ELSE NULL END) FECHA_VENCIMIENTO,
		                MAX(CASE WHEN ORDEN = 1 THEN FECHA ELSE NULL END) FECHA_ACTUALIZACION_DOC,
		                SUM(DEUDA) DEUDA_TOTAL,
		                SUM(CASE WHEN NUM_DIAS_VENCIDOS IS NOT NULL AND NUM_DIAS_VENCIDOS > 0 AND SITUACION = 'VIGENTE' THEN DEUDA ELSE 0 END) DEUDA_ATRASADA,
		                SUM(CASE WHEN SITUACION = 'VENCIDO' THEN DEUDA ELSE 0 END) DEUDA_VENCIDA,
		                MAX(CASE WHEN ORDEN = 1 THEN NUM_DIAS_VENCIDOS ELSE NULL END) DIAS FROM (
		                SELECT ROW_NUMBER() OVER(PARTITION BY COD_UNICO ORDER BY NUM_DIAS_VENCIDOS DESC) ORDEN ,A.* FROM WEBBE_INFINITY_PROD_DIARIO A
		                ) A
	                	GROUP BY COD_UNICO
						) A
					) A"
				))
				->where('A.COD_UNICO','=',$cu)
				->get()
				->first();
        return $sql;
	}

	function saveActividad($data){
		DB::beginTransaction();
        $status = true;
        try {
            if ($data) {
				DB::table('WEBBE_INFINITY_SEG_ACTIVIDAD')->insert($data);
			}
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }

        return $status;
	}

	function actualizaractividadcompromiso($id,$data)
	{
		DB::beginTransaction();
        $status = true;
        try {
            if ($id) {
				DB::table('WEBBE_INFINITY_SEG_ACTIVIDAD')
					->where('ID',$id)
                    ->update(['CUMPLE_COMPROMISO'=>$data]);
			}
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
	}

	function getDiasAtrasosmaximo($cu)
	{
		return DB::table("WEBBE_INFINITY_INFO_DIARIA_CLI AS A")
				->select("A.COD_UNICO"
					,DB::Raw("(CASE WHEN DIAS_ATRASO_HOM <> 'Sin Atrasos' THEN 1 ELSE 0 END) AS FLG_GEST_ATRASO")
					,DB::Raw("(CASE WHEN B.COD_UNICO IS NULL THEN 0 ELSE 1 END) AS FLG_GEST_VIG"))
				->leftjoin(DB::raw("(SELECT * FROM WEBBE_INFINITY_SEG_VIG_CASOS WHERE PERIODO = (SELECT MAX(PERIODO) FROM WEBBE_INFINITY_SEG_VIG_CASOS)) AS B"),function($join)
				{
					$join->on('A.COD_UNICO','=','B.COD_UNICO');
				})
				->where('A.COD_UNICO',$cu)
				->get()->first();
	}

	function DatoJefeEjecutivo($registroejecutivo)
	{
		$sql =DB::table('T_WEB_ARBOL_DIARIO')
				->where('REGISTRO',$registroejecutivo)
				->get()
				->first();
		$jefe=false;
		if ($sql) {
			$jefe = DB::table('T_WEB_ARBOL_DIARIO')
					->where('NOMBRE_JEFE',$sql->NOMBRE_JEFE)
					->whereNull('ENCARGADO')
					->get()
					->first();
			$registrojefe=$jefe->REGISTRO;
		}
		return  $jefe ? $registrojefe : false;
	}

	function NombreJefe($registrojefe)
	{
		$jefe = DB::table('T_WEB_ARBOL_DIARIO')
				->where('REGISTRO',$registrojefe)
				->whereNull('ENCARGADO')
				->get()
				->first();		
		return  $jefe ? $jefe->NOMBRE_JEFE : false;
	}
	function tablaCompromisos($filtros=null){
		$resultadosComp=null;
		$periodo = DB::table(DB::raw('(SELECT MAX(PERIODO) AS PERIODO FROM WEBBE_INFINITY_CLIENTE) M'))->first();
		if($periodo !=null){
			$periodo = $periodo->PERIODO;
			$sql = DB::Table(
				DB::raw("(SELECT 
				M.COD_UNICO,
				M.COD_SECTORISTA,
				M.NOMBRE
				FROM (
				SELECT  ISNULL(T1.COD_UNICO,T2.COD_UNICO) COD_UNICO,ISNULL(T1.NOMBRE,T2.NOMBRE) NOMBRE
				,ISNULL(T1.COD_SECTORISTA,T2.COD_SECTORISTA) COD_SECTORISTA
				FROM
				(SELECT DISTINCT COD_UNICO,NOMBRE,COD_SECTORISTA FROM WEBBE_INFINITY_CLIENTE 
				WHERE BANCA = 'BE' AND PERIODO = '".($periodo)."') T1
				FULL OUTER JOIN
				(SELECT DISTINCT COD_UNICO,NOMBRE,CODSECTORISTA AS COD_SECTORISTA
				 FROM WEBBE_INFINITY_INFO_DIARIA_CLI) T2
				ON T1.COD_UNICO = T2.COD_UNICO
				)M 
				LEFT JOIN ARBOL_DIARIO_2 ARB ON M.COD_SECTORISTA = ARB.COD_SECT_LARGO 
				LEFT JOIN WEBVPC_CLIENTES_VPC VPC ON M.COD_UNICO = VPC.COD_UNICO 
				WHERE LEFT(M.COD_SECTORISTA,3) != '070') as WICA"))
				->leftjoin(DB::raw("(SELECT  A.REGISTRO,B.ENCARGADO,B.NOMBRE_JEFE,  B.NOMBRE_ZONAL, D.REGISTRO AS REGISTRO_JEFE,B.TIPO_BANCA,A.COD_SECTORISTA
				FROM (SELECT RTRIM(LTRIM(LOGIN)) REGISTRO, RTRIM(LTRIM(COD_SECT_LARGO)) COD_SECTORISTA,CODIGO_JEFE
						FROM ARBOL_DIARIO_2 WHERE RTRIM(LTRIM(LOGIN)) <> '') A
				LEFT JOIN (SELECT * FROM T_WEB_ARBOL_DIARIO WHERE ENCARGADO <> 'SIN CLIENTES') B ON B.REGISTRO = A.REGISTRO 
				LEFT JOIN (SELECT ROL, NOMBRE, REGISTRO FROM WEBVPC_USUARIO) C ON A.REGISTRO = C.REGISTRO 
				LEFT JOIN (SELECT REGISTRO,ID_GC FROM WEBVPC_USUARIO WHERE ID_GC LIKE 'JEL%' AND ROL = '24' AND FLAG_ACTIVO = '1') D 
				ON A.CODIGO_JEFE = D.ID_GC AND B.NOMBRE_JEFE IS NOT NULL
				LEFT JOIN WEBVPC_ROL E ON E.ID_ROL = C.ROL) AS F"), function($join)
					{
						$join->on('WICA.COD_SECTORISTA', '=', 'F.COD_SECTORISTA');
					})->select();
			if(!empty($filtros)){
				if(isset($filtros['ejecutivo'])) {
					$sql = $sql->where('F.REGISTRO', $filtros['ejecutivo']);
				}
				if(!isset($filtros['ejecutivo']) && isset($filtros['jefatura'])) {
					$sql = $sql->where('F.REGISTRO_JEFE', $filtros['jefatura']);
				}
				if(!isset($filtros['ejecutivo']) && !isset($filtros['jefatura']) && isset($filtros['zonal'])) {
					$sql = $sql->where('F.NOMBRE_ZONAL', $filtros['zonal']);
				}
				if(!isset($filtros['ejecutivo']) && !isset($filtros['jefatura']) && !isset($filtros['zonal']) && isset($filtros['banca'])) {
					$sql = $sql->where('F.TIPO_BANCA', $filtros['banca']);
				}
			}
			$resultadosComp = $sql->join(DB::raw(
				'(SELECT * FROM 
				WEBVPC_COVID19_CUMPLIMIENTO
				WHERE ID_COVID IN (
				SELECT ID_COVID FROM (
					SELECT ID_COVID,ROW_NUMBER() OVER(PARTITION BY COD_UNICO ORDER BY ID_COVID DESC) ORDEN_RN
					FROM WEBVPC_COVID19_CUMPLIMIENTO
					)M WHERE ORDEN_RN = 1)) CUMP2'
			), function($join)
							{
								$join->on('CUMP2.COD_UNICO', '=', 'WICA.COD_UNICO');
							})->whereNull('CUMP2.STATUS_COMPROMISO')
							->select('WICA.NOMBRE','F.ENCARGADO','NOMBRE_JEFE','CUMP2.*');
		}
		
		return $resultadosComp;
	}
	function getListaClientesSeguimiento($filtros=null){
		// dd($filtros);
		$sql = null;
		$periodo = DB::table(DB::raw('(SELECT MAX(PERIODO) AS PERIODO FROM WEBBE_INFINITY_CLIENTE) M'))->first();
		if($periodo !=null){
			$periodo = $periodo->PERIODO;
			$sql = DB::Table(
					DB::raw("(SELECT M.PERIODO,
					M.COD_UNICO,
					M.SALDO_VIG_SIN_ATRASO,
					M.SALDO_VIG_CON_ATRASO,
					M.CTD_MESES_ATRASOS_TOT_MAS8,
					M.CTD_MESES_ATRASOS_SINDSCTOS_MAS8,
					M.DIAS_ATRASO_HOM,
					M.SALDO_NOVIG,
					M.FLG_SOLO_LEASING,
					M.RANGO_DEUDA_PROBLEMAS,
					M.FECHA,
					A.NUM_DOC,
					M.NOMBRE,
					ISNULL(A.BANCA,'BE') BANCA,
					M.COD_SECTORISTA,
					M.SALDO_INTERBANK,
					A.SALDO_RCC,
					ISNULL(A.RATING,VPC.RATING) RATING,
					A.FLG_INFINITY,
					A.CLASIFICACION,
					ISNULL(M.FEVE,VPC.FEVE) FEVE,
					A.SALDO_VENCIDO_IBK,
					A.SALDO_VENCIDO_RCC,
					A.METODIZADO_1_FECHA,
					A.METODIZADO_2_FECHA,
					A.FECHA_ULTIMA_VISITA,
					A.FECHA_ULTIMO_IBR,
					A.FECHA_ULTIMO_F02,
					A.GARANTIA,
					A.FECHA_ULTIMO_RECALCULO,
					A.VENTAS,
					M.MAX_DIAS_ATRASO,
					A.PROVINCIA,
					A.DISTRITO,
					A.FECHA_VENCIMIENTO_POLITICA,
					A.FLG_VISIBLE,
					M.RMG,
					A.FECHA_INGRESO_INFINITY,
					CASE WHEN A.SEGMENTO IS NULL THEN 
					CASE WHEN ARB.BANCA = 'BEL' AND ARB.ZONAL = 'BEL ZONAL 2' THEN 'MEDIANA EMPRESA'  
						WHEN ARB.BANCA = 'BEL' AND ARB.ZONAL IN ('BEL ZONAL 3','BEL ZONAL 1') THEN 'GRAN EMPRESA' 
						WHEN ARB.BANCA = 'BEP' THEN 'PROVINCIA'
						WHEN ARB.BANCA = 'BC' THEN 'CORPORATIVA' END ELSE A.SEGMENTO END SEGMENTO,
					A.FECHA_CARGA,
					A.FLG_POLITICAPROCESO
					FROM (
					SELECT ISNULL(T1.PERIODO,'".($periodo)."') PERIODO, ISNULL(T1.COD_UNICO,T2.COD_UNICO) COD_UNICO,
						ISNULL(T1.NOMBRE,T2.NOMBRE) NOMBRE,ISNULL(T1.RMG,T2.RMG) RMG,
						ISNULL(T1.FEVE,T2.FEVE) FEVE,
						ISNULL(T1.MAX_DIAS_ATRASO,T2.MAX_DIAS_ATRASO) MAX_DIAS_ATRASO,
						ISNULL(T1.COD_SECTORISTA,T2.CODSECTORISTA) COD_SECTORISTA,
						ISNULL(T1.SALDO_INTERBANK,T2.SALDO_TOTAL) SALDO_INTERBANK,
						FECHA,SALDO_VIG_SIN_ATRASO,SALDO_VIG_CON_ATRASO,SALDO_NOVIG,CTD_MESES_ATRASOS_TOT_MAS8,
						CTD_MESES_ATRASOS_SINDSCTOS_MAS8,FLG_SOLO_LEASING,RANGO_DEUDA_PROBLEMAS,DIAS_ATRASO_HOM
						
					FROM
					(SELECT DISTINCT PERIODO,COD_UNICO,FEVE,MAX_DIAS_ATRASO,RMG,NOMBRE,COD_SECTORISTA,SALDO_INTERBANK FROM WEBBE_INFINITY_CLIENTE WHERE BANCA = 'BE' AND PERIODO = '".($periodo)."') T1
					FULL OUTER JOIN
					(SELECT DISTINCT COD_UNICO,FEVE,RMG,NOMBRE,MAX_DIAS_ATRASO,CODSECTORISTA,SALDO_TOTAL,FECHA,SALDO_VIG_CON_ATRASO,SALDO_VIG_SIN_ATRASO,SALDO_NOVIG,CTD_MESES_ATRASOS_TOT_MAS8,
					CTD_MESES_ATRASOS_SINDSCTOS_MAS8,FLG_SOLO_LEASING,RANGO_DEUDA_PROBLEMAS,DIAS_ATRASO_HOM
					 FROM WEBBE_INFINITY_INFO_DIARIA_CLI) T2
					ON T1.COD_UNICO = T2.COD_UNICO
					)M LEFT JOIN WEBBE_INFINITY_CLIENTE A ON M.COD_UNICO = A.COD_UNICO AND M.PERIODO = A.PERIODO
					LEFT JOIN ARBOL_DIARIO_2 ARB ON M.COD_SECTORISTA = ARB.COD_SECT_LARGO 
					LEFT JOIN WEBVPC_CLIENTES_VPC VPC ON M.COD_UNICO = VPC.COD_UNICO 
					WHERE LEFT(M.COD_SECTORISTA,3) != '070') as WICA"))		
					->leftjoin(DB::raw('(SELECT CODUNICOCLI, SUM(ISNULL(SALDO_CREDITO,0)*(1-COBERTURA_REPO)) AS SALDO_NO_COBERTURA
					FROM MKT_DESEMBOLSO_REACTIVA
					GROUP BY CODUNICOCLI) as MKT_R'),function($join){
						$join->on('WICA.COD_UNICO', '=', 'MKT_R.CODUNICOCLI');
					})
					->leftjoin(DB::raw("(
							SELECT A.COD_UNICO,
							CASE WHEN A.COMPROMISO_PAGO >= CAST(GETDATE() AS DATE) THEN A.TIPO ELSE NULL END TIPO,
							CASE WHEN A.COMPROMISO_PAGO >= CAST(GETDATE() AS DATE) THEN A.COMPROMISO_PAGO ELSE NULL END COMPROMISO_PAGO,
							CASE WHEN A.COMPROMISO_PAGO >= CAST(GETDATE() AS DATE) THEN 'GESTIONADO' ELSE 'NO GESTIONADO' END FLG_GESTIONADO
							FROM (
								SELECT C.*, ROW_NUMBER() OVER(PARTITION BY COD_UNICO ORDER BY FECHA_ACTIVIDAD DESC) ORDEN
								FROM WEBBE_INFINITY_SEG_ACTIVIDAD C
							) A WHERE ORDEN = 1
						) as B"), function($join)
					{
						$join->on('WICA.COD_UNICO', '=', 'B.COD_UNICO');
					})
					->leftjoin(DB::raw("(SELECT * FROM (SELECT A.*, ROW_NUMBER() OVER(PARTITION BY COD_UNICO ORDER BY NUM_DIAS_VENCIDOS DESC) ORDEN FROM WEBBE_INFINITY_PROD_DIARIO A WHERE NUM_DIAS_VENCIDOS IS NOT NULL) A WHERE ORDEN = 1) AS C"),function($join)
					{
						$join->on('WICA.COD_UNICO','=','C.COD_UNICO');
					})
					->leftjoin(DB::raw("(SELECT COD_UNICO, NIVEL_ALERTA, NIVEL_ALERTA_ANTERIOR, NIVEL_ALERTA_ANTERIOR_2, SEMAFORO_DET_MORA, SEMAFORO_DET_RATING, SEMAFORO_DET_VAR_RATING FROM WEBBE_INFINITY_ALERTA WHERE PERIODO = '".$periodo."') AS D"),function($join)
					{
						$join->on('WICA.COD_UNICO', '=', 'D.COD_UNICO');
					})
					->leftjoin(DB::raw("(SELECT A.COD_SECTORISTA, A.REGISTRO, B.ENCARGADO NOMBRE_EJECUTIVO, B.NOMBRE_JEFE, B.NOMBRE_ZONAL, 
					B.TIPO_BANCA,D.REGISTRO AS REGISTRO_JEFE
					FROM (SELECT RTRIM(LTRIM(LOGIN)) REGISTRO, RTRIM(LTRIM(COD_SECT_LARGO)) COD_SECTORISTA,CODIGO_JEFE
							FROM ARBOL_DIARIO_2 WHERE RTRIM(LTRIM(LOGIN)) <> '') A
					LEFT JOIN T_WEB_ARBOL_DIARIO B ON B.REGISTRO = A.REGISTRO 
					LEFT JOIN (SELECT ROL, NOMBRE, REGISTRO FROM WEBVPC_USUARIO) C ON A.REGISTRO = C.REGISTRO 
					LEFT JOIN (SELECT REGISTRO,ID_GC FROM WEBVPC_USUARIO WHERE ID_GC LIKE 'JEL%' AND ROL = '24' AND FLAG_ACTIVO = '1') D 
					ON A.CODIGO_JEFE = D.ID_GC AND B.NOMBRE_JEFE IS NOT NULL
					LEFT JOIN WEBVPC_ROL E ON E.ID_ROL = C.ROL) AS F"), function($join)
					{
						$join->on('WICA.COD_SECTORISTA', '=', 'F.COD_SECTORISTA');
					})
					->leftjoin(DB::Raw("
						(SELECT A.*, CASE WHEN B.ID_CASO IS NOT NULL THEN B.ACCION ELSE NULL END ACCION FROM WEBBE_INFINITY_SEG_VIG_CASOS A 
							LEFT JOIN (SELECT *  FROM ( 
								  SELECT ID_CASO, ACCION, ROW_NUMBER() OVER(PARTITION BY ID_CASO ORDER BY ORDEN DESC) ORDEN 
								  FROM WEBBE_INFINITY_SEG_VIG_MENSAJE ) A WHERE ORDEN = 1 ) B 
							ON A.ID = B.ID_CASO WHERE A.PERIODO = '".$periodo."') AS G"),function ($join)
					{
						$join->on('WICA.COD_UNICO', '=', 'G.COD_UNICO');
					})
					->leftjoin(DB::Raw(
						"
						(SELECT M.COD_UNICO,CUMPL.ESTADO_APROBACION,CUMPL.PREGUNTA_14_4,CUMPL.PREGUNTA_14_5,CUMPL.FECHA_MODIFICACION FROM (SELECT COD_UNICO,MAX(ID) ID
						FROM WEBVPC_COVID19 GROUP BY 
						COD_UNICO )M
						LEFT JOIN WEBVPC_COVID19 CUMPL ON M.ID = CUMPL.ID) AS CUMPLFINAL
						"),function ($join)
					{
						$join->on('WICA.COD_UNICO', '=', 'CUMPLFINAL.COD_UNICO');
					})
					->leftjoin(DB::Raw(
						"
						(SELECT COD_UNICO,CASE WHEN REVISION_FECHA IS NULL THEN 
						'FALTA REVISAR' ELSE 'APROBADO' END
						ESTADO_APROBADO_VISITA FROM (select [WIV].*,
						row_number() over (partition by [WIV].[COD_UNICO] 
						order by [ID] DESC) ORDEN,
						CONVERT(DATE,WIV.FECHA_VISITA) AS FECHA_VISITA_DATE 
						from [WEBBE_INFINITY_VISITA] as [WIV] ) VISITA
						WHERE ORDEN = 1) VISITA
						"),function ($join)
					{
						$join->on('WICA.COD_UNICO', '=', 'VISITA.COD_UNICO');
					})
					->select(
						DB::Raw("(CASE WHEN G.COD_UNICO IS NULL AND WICA.RATING = 'B' AND D.SEMAFORO_DET_MORA = 'AMBAR' AND D.SEMAFORO_DET_VAR_RATING = 'VERDE' AND D.NIVEL_ALERTA = '1' THEN 1 ELSE 0 END ) AS FLG_GES_GYS_DET"),
						DB::Raw("(CASE WHEN WICA.RATING = 'B' AND D.SEMAFORO_DET_MORA = 'AMBAR' AND D.SEMAFORO_DET_VAR_RATING = 'VERDE' AND D.NIVEL_ALERTA = '1' THEN 1 ELSE 0 END ) AS FLG_GES_GYS"),
						DB::Raw('CONVERT(DATE,WICA.FECHA) AS FECHA'),
						DB::raw('ISNULL(C.NUM_DIAS_VENCIDOS,0) AS DIAS'),
						'F.NOMBRE_EJECUTIVO AS EJECUTIVO_NEGOCIO',
						'WICA.COD_UNICO',
						DB::Raw("ISNULL(WICA.NOMBRE,'') AS NOMBRE_CLIENTE"),
						DB::raw('ISNULL(WICA.SALDO_INTERBANK,0) AS SALDO_TOT_IBK'),
						DB::raw('ISNULL(WICA.SALDO_VIG_SIN_ATRASO,0) AS SALDO_IBK_SIN_ATRASO'),
						DB::raw('ISNULL(WICA.SALDO_VIG_CON_ATRASO,0) AS SALDO_IBK_CON_ATRASO'),
						DB::raw("ISNULL(WICA.SALDO_NOVIG,0) AS SALDO_NO_VIGENTE"),
						DB::raw("(RIGHT('000000000000'+CAST(CAST(ROUND(ISNULL(WICA.SALDO_VIG_CON_ATRASO,0) + ISNULL(WICA.CTD_MESES_ATRASOS_SINDSCTOS_MAS8,0),0) AS INTEGER) AS VARCHAR(12)),12)) AS SUMADOS"),
						'D.NIVEL_ALERTA',
						'D.NIVEL_ALERTA_ANTERIOR',
						'D.NIVEL_ALERTA_ANTERIOR_2',
						DB::Raw('ISNULL(WICA.CTD_MESES_ATRASOS_TOT_MAS8,0) 
						AS FRECUENCIA_ATRASOS_TOTPROD'),
						DB::Raw("ISNULL(WICA.CTD_MESES_ATRASOS_SINDSCTOS_MAS8,0) AS FRECUENCIA_ATRASOS_SINDSCTOS"),
						DB::Raw("(	CASE WHEN C.NUM_DIAS_VENCIDOS IS NOT NULL AND B.TIPO IS NOT NULL THEN B.TIPO
									  WHEN C.NUM_DIAS_VENCIDOS IS NOT NULL AND B.TIPO IS NULL THEN 'PENDIENTE'
									  WHEN C.NUM_DIAS_VENCIDOS IS NULL THEN '--' END) AS GESTION"),
						DB::Raw("(	CASE WHEN G.COD_UNICO IS NOT NULL AND G.FLG_GESTIONADO = 1 THEN G.ACCION
									  WHEN G.COD_UNICO IS NOT NULL AND G.FLG_GESTIONADO = 0 THEN 'PENDIENTE'
									  ELSE '--' END) AS GESTION_VIG"),
						DB::Raw("(	CASE WHEN CUMPLFINAL.ESTADO_APROBACION = 1 THEN 'APROBADO'
												  WHEN CUMPLFINAL.ESTADO_APROBACION = 0 THEN 'GUARDADO'
												  ELSE '--' END) AS ESTADO_APROBACION"),
						DB::Raw("(	CASE WHEN CUMPLFINAL.PREGUNTA_14_4 = 2 OR CUMPLFINAL.FECHA_MODIFICACION IS NULL  THEN 'NO'
					
												  WHEN CUMPLFINAL.PREGUNTA_14_5 >= DATEDIFF(MONTH, GETDATE(), CUMPLFINAL.FECHA_MODIFICACION) THEN 'BLINDAJE'
												  ELSE 'NO' END) AS BLINDAJE"),
						DB::Raw("(	CASE WHEN VISITA.ESTADO_APROBADO_VISITA IS NULL THEN 'LLENE FICHA CONOCEME'
															  ELSE VISITA.ESTADO_APROBADO_VISITA END) AS ESTADO_APROBADO_VISITA"),
						DB::Raw("( CASE WHEN C.NUM_DIAS_VENCIDOS IS NOT NULL AND B.COMPROMISO_PAGO >= CAST(GETDATE() AS DATE) THEN CONVERT(DATE, B.COMPROMISO_PAGO) ELSE NULL END ) AS FEC_COMPROMISO"),
						'WICA.COD_SECTORISTA',
						DB::Raw("ISNULL(WICA.FLG_INFINITY,0) as FLG_INFINITY"),
						DB::Raw("ISNULL(WICA.FLG_SOLO_LEASING,0) as FLG_SOLO_LEASING"),
						'WICA.RANGO_DEUDA_PROBLEMAS AS RANGO_DEUDA_VENCIDA',
						'WICA.DIAS_ATRASO_HOM AS RANGO_DIAS_ATRASO',
						DB::Raw("ISNULL(WICA.FEVE,'N/A') AS FEVE"), 
						'F.NOMBRE_EJECUTIVO',
						DB::Raw("ISNULL(F.NOMBRE_JEFE,'') AS NOMBRE_JEFE"),
						DB::Raw("ISNULL(F.NOMBRE_ZONAL,'') AS NOMBRE_ZONAL"),
						DB::Raw("ISNULL(F.TIPO_BANCA,'') as TIPO_BANCA"),
						DB::Raw("ISNULL(F.REGISTRO,'') as REGISTRO"),
						DB::Raw("(CASE WHEN NIVEL_ALERTA='11' THEN '1' WHEN NIVEL_ALERTA='22' THEN '2' WHEN NIVEL_ALERTA='33' THEN '3' WHEN NIVEL_ALERTA='99' THEN '999' ELSE NIVEL_ALERTA END) AS NIVEL_ALERTA_BUSQUEDA"),
						DB::Raw("( CASE WHEN C.NUM_DIAS_VENCIDOS IS NOT NULL AND B.COMPROMISO_PAGO >= CAST(GETDATE() AS DATE) THEN B.FLG_GESTIONADO ELSE 'NO GESTIONADO' END ) AS FLG_GESTIONADO")
					);
			if(!empty($filtros)){
				if(isset($filtros['ejecutivo'])) {
					$sql = $sql->where('F.REGISTRO', $filtros['ejecutivo']);
				}
				if(!isset($filtros['ejecutivo']) && isset($filtros['jefatura'])) {
					$sql = $sql->where('F.REGISTRO_JEFE', $filtros['jefatura']);
				}
				if(!isset($filtros['ejecutivo']) && !isset($filtros['jefatura']) && isset($filtros['zonal'])) {
					$sql = $sql->where('F.NOMBRE_ZONAL', $filtros['zonal']);
				}
				if(!isset($filtros['ejecutivo']) && !isset($filtros['jefatura']) && !isset($filtros['zonal']) && isset($filtros['banca'])) {
					$sql = $sql->where('F.TIPO_BANCA', $filtros['banca']);
				}
				if(isset($filtros['leasing']) && $filtros['leasing']) {
					$sql = $sql->wherein('WICA.FLG_SOLO_LEASING',$filtros['leasing']);
				}
				if(isset($filtros['rango_deuda']) && $filtros['rango_deuda']) {
					$sql = $sql->wherein('WICA.RANGO_DEUDA_PROBLEMAS',$filtros['rango_deuda']);
				}
				if(isset($filtros['semaforo_m0']) && $filtros['semaforo_m0']) {
					$sql = $sql->wherein('D.NIVEL_ALERTA',$filtros['semaforo_m0']);
				}
				// if(isset($filtros['semaforo_m1']) && $filtros['semaforo_m1']) {
				// 	$sql = $sql->wherein('E.NIVEL_ALERTA_ANTERIOR',$filtros['semaforo_m1']);
				// }
				if(isset($filtros['gestion']) && $filtros['gestion']) {
					$sql = $sql->wherein('B.FLG_GESTIONADO',$filtros['gestion']);
					//$sql = $sql->wherein('A.FLG_GESTIONADO',$filtros['gestion']);
				}
				if(isset($filtros['rango_atraso']) && $filtros['rango_atraso']) {
					$sql = $sql->wherein('WICA.DIAS_ATRASO_HOM',$filtros['rango_atraso']);
				}
				if(isset($filtros['frecuencia_atraso_tot']) && $filtros['frecuencia_atraso_tot']) {
					$sql = $sql->wherein('WICA.CTD_MESES_ATRASOS_TOT_MAS8',$filtros['frecuencia_atraso_tot']);
				}
				if(isset($filtros['frecuencia_atraso_sin_dscto'])) {
					$sql = $sql->wherein('WICA.CTD_MESES_ATRASOS_SINDSCTOS_MAS8',$filtros['frecuencia_atraso_sin_dscto']);
				}
				if(isset($filtros['es_infinity']) && $filtros['es_infinity']) {
					$sql = $sql->wherein('WICA.FLG_INFINITY',$filtros['es_infinity']);
				}
				if(isset($filtros['feve']) && $filtros['feve']) {
					$sql = $sql->wherein('WICA.FEVE',$filtros['feve']);
				}
				if(isset($filtros['gesgys']) && $filtros['gesgys']) {
					$sql = $sql->wherein(DB::Raw("(CASE WHEN WICA.RATING = 'B' AND D.SEMAFORO_DET_MORA = 'AMBAR' AND D.SEMAFORO_DET_VAR_RATING = 'VERDE' AND D.NIVEL_ALERTA = '1' THEN 1 ELSE 0 END )"),$filtros['gesgys']);
				}
				if(isset($filtros['cod_unico'])) {
					$sql = $sql->where('WICA.COD_UNICO','like','%'.$filtros['cod_unico'].'%');
				}
				if(isset($filtros['razon_social'])) {
					$sql = $sql->where('WICA.NOMBRE','like','%'.$filtros['razon_social'].'%');
				}
				if(isset($filtros['estado_aprobacion'])) {
					if(in_array('',$filtros['estado_aprobacion'])){
						$sql = $sql->whereNull('CUMPLFINAL.ESTADO_APROBACION')->orWhereIn('CUMPLFINAL.ESTADO_APROBACION',$filtros['estado_aprobacion']);
					}else{
						$sql = $sql->wherein('CUMPLFINAL.ESTADO_APROBACION',$filtros['estado_aprobacion']);
					}
				}
			}
		}
		return $sql;
		
	}

	function getResumenAlertas($filtros=null){
		\Debugbar::info($filtros);
		$sql = null;
		$periodo = DB::table(DB::raw('(SELECT MAX(PERIODO) AS PERIODO FROM WEBBE_INFINITY_CLIENTE) M'))->first();
		if($periodo !=null){
			$periodo = $periodo->PERIODO;
			$sql = DB::Table(DB::raw("(
				SELECT M.PERIODO,
                M.COD_UNICO,
                A.NUM_DOC,
                M.NOMBRE,
                ISNULL(A.BANCA,'BE') BANCA,
                M.COD_SECTORISTA,
                M.SALDO_INTERBANK,
                A.SALDO_RCC,
                ISNULL(A.RATING,VPC.RATING) RATING,
                A.FLG_INFINITY,
                A.CLASIFICACION,
				ISNULL(M.FEVE,VPC.FEVE) FEVE,
				M.RANGO_DEUDA_PROBLEMAS,
				M.DIAS_ATRASO_HOM,
                A.SALDO_VENCIDO_IBK,
                A.SALDO_VENCIDO_RCC,
                A.METODIZADO_1_FECHA,
                A.METODIZADO_2_FECHA,
                A.FECHA_ULTIMA_VISITA,
                A.FECHA_ULTIMO_IBR,
                A.FECHA_ULTIMO_F02,
                A.GARANTIA,
                A.FECHA_ULTIMO_RECALCULO,
                A.VENTAS,
                M.MAX_DIAS_ATRASO,
                A.PROVINCIA,
                A.DISTRITO,
                A.FECHA_VENCIMIENTO_POLITICA,
                A.FLG_VISIBLE,
                M.RMG,
                A.FECHA_INGRESO_INFINITY,
                CASE WHEN A.SEGMENTO IS NULL THEN 
                CASE WHEN ARB.BANCA = 'BEL' AND ARB.ZONAL = 'BEL ZONAL 2' THEN 'MEDIANA EMPRESA'  
                    WHEN ARB.BANCA = 'BEL' AND ARB.ZONAL IN ('BEL ZONAL 3','BEL ZONAL 1') THEN 'GRAN EMPRESA' 
                    WHEN ARB.BANCA = 'BEP' THEN 'PROVINCIA'
                    WHEN ARB.BANCA = 'BC' THEN 'CORPORATIVA' END ELSE A.SEGMENTO END SEGMENTO,
                A.FECHA_CARGA,
                A.FLG_POLITICAPROCESO
                FROM (
                SELECT ISNULL(T1.PERIODO,'".($periodo)."') PERIODO, ISNULL(T1.COD_UNICO,T2.COD_UNICO) COD_UNICO,
                    ISNULL(T1.NOMBRE,T2.NOMBRE) NOMBRE,ISNULL(T1.RMG,T2.RMG) RMG,
                    ISNULL(T1.FEVE,T2.FEVE) FEVE,
                    ISNULL(T1.MAX_DIAS_ATRASO,T2.MAX_DIAS_ATRASO) MAX_DIAS_ATRASO,
                    ISNULL(T1.COD_SECTORISTA,T2.CODSECTORISTA) COD_SECTORISTA,
					ISNULL(T1.SALDO_INTERBANK,T2.SALDO_TOTAL) SALDO_INTERBANK,
					DIAS_ATRASO_HOM,RANGO_DEUDA_PROBLEMAS
                FROM
                (SELECT DISTINCT PERIODO,COD_UNICO,FEVE,MAX_DIAS_ATRASO,RMG,NOMBRE,COD_SECTORISTA,SALDO_INTERBANK FROM WEBBE_INFINITY_CLIENTE WHERE BANCA = 'BE' AND PERIODO = '".($periodo)."') T1
                FULL OUTER JOIN
                (SELECT DISTINCT COD_UNICO,FEVE,RMG,NOMBRE,MAX_DIAS_ATRASO,CODSECTORISTA,SALDO_TOTAL,DIAS_ATRASO_HOM,RANGO_DEUDA_PROBLEMAS FROM WEBBE_INFINITY_INFO_DIARIA_CLI) T2
                ON T1.COD_UNICO = T2.COD_UNICO
                )M LEFT JOIN WEBBE_INFINITY_CLIENTE A ON M.COD_UNICO = A.COD_UNICO AND M.PERIODO = A.PERIODO
                LEFT JOIN ARBOL_DIARIO_2 ARB ON M.COD_SECTORISTA = ARB.COD_SECT_LARGO 
                LEFT JOIN WEBVPC_CLIENTES_VPC VPC ON M.COD_UNICO = VPC.COD_UNICO 
                WHERE LEFT(M.COD_SECTORISTA,3) != '070'
				) as A"))
					->leftJoin('WEBBE_INFINITY_ALERTA AS WIA', function($join) {
						$join->on('A.COD_UNICO', '=', 'WIA.COD_UNICO');
						$join->on('A.PERIODO', '=', 'WIA.PERIODO');
					})
					->leftjoin(DB::raw("(
						SELECT 
							D.COD_UNICO, D.FECHA_ATRASO, CASE WHEN B.FECHA_ACTIVIDAD > D.FECHA_ATRASO THEN B.TIPO ELSE '' END AS TIPO, 
							CASE WHEN B.FECHA_ACTIVIDAD > D.FECHA_ATRASO THEN B.COMPROMISO_PAGO ELSE NULL END AS COMPROMISO_PAGO, 
							CASE WHEN D.FECHA_ATRASO IS NOT NULL AND B.FECHA_ACTIVIDAD > D.FECHA_ATRASO THEN 'GESTIONADO' ELSE 'NO GESTIONADO' END FLG_GESTIONADO FROM WEBBE_INFINITY_INFO_DIARIA_CLI D 
							LEFT JOIN ( SELECT * FROM (SELECT C.*, ROW_NUMBER() OVER(PARTITION BY COD_UNICO ORDER BY FECHA_ACTIVIDAD DESC) ORDEN FROM WEBBE_INFINITY_SEG_ACTIVIDAD C ) A WHERE ORDEN = 1) AS B ON [D].[COD_UNICO] = [B].[COD_UNICO]
						) AS B"), function($join)
					{
						$join->on('A.COD_UNICO', '=', 'B.COD_UNICO');
					})
					->leftjoin(DB::raw("(
						SELECT 
						WIC.*,H.REGISTRO REGISTRO_JEFE 
						FROM (SELECT * FROM WEBBE_INFINITY_CLIENTE WHERE PERIODO = '".$periodo."' )WIC 
						left join ARBOL_DIARIO_2 AS G ON WIC.COD_SECTORISTA=G.COD_SECT_LARGO 
						left join (SELECT * FROM T_WEB_ARBOL_DIARIO WHERE ENCARGADO IS NULL AND NOMBRE_JEFE IS NOT NULL)AS H ON G.NOMBRE_JEFE=H.NOMBRE_JEFE
						) AS D"),function($join)
					{
						$join->on('A.COD_UNICO', '=', 'D.COD_UNICO');
					})
					->leftjoin(DB::raw("(
						SELECT 
						D.COD_SECTORISTA, A.REGISTRO, A.ENCARGADO NOMBRE_EJECUTIVO, A.NOMBRE_JEFE, A.NOMBRE_ZONAL, A.TIPO_BANCA 
						FROM (SELECT 
							RTRIM(LTRIM(LOGIN)) REGISTRO, RTRIM(LTRIM(COD_SECT_LARGO)) COD_SECTORISTA 
							FROM ARBOL_DIARIO_2 WHERE RTRIM(LTRIM(LOGIN)) <> '') D 
						LEFT JOIN T_WEB_ARBOL_DIARIO A ON A.REGISTRO = D.REGISTRO 
						LEFT JOIN (SELECT ROL, NOMBRE, REGISTRO FROM WEBVPC_USUARIO ) B ON A.REGISTRO = B.REGISTRO 
						LEFT JOIN (SELECT * FROM WEBVPC_ROL) C ON C.ID_ROL = B.ROL
						) AS F"), function($join)
					{
						$join->on('A.COD_SECTORISTA', '=', 'F.COD_SECTORISTA');
					})
					->leftjoin(DB::raw("(
						SELECT 
						A.COD_UNICO, B.ID_CASO, B.ORDEN, B.ESTADO 
						FROM (SELECT * FROM WEBBE_INFINITY_SEG_VIG_CASOS WHERE PERIODO = (SELECT MAX(PERIODO) FROM WEBBE_INFINITY_SEG_VIG_CASOS)) A 
						LEFT JOIN (
							SELECT * FROM ( SELECT ID_CASO, ORDEN, ESTADO, ROW_NUMBER() OVER(PARTITION BY ID_CASO ORDER BY ORDEN DESC) ORDEN_R FROM WEBBE_INFINITY_SEG_VIG_MENSAJE) A WHERE ORDEN_R = 1  
						) B ON A.ID = B.ID_CASO) AS G"), function($join)
					{
						$join->on('A.COD_UNICO', '=', 'G.COD_UNICO');
					})
					->leftjoin(DB::raw("(
						SELECT * FROM 
						(SELECT A.*, ROW_NUMBER() OVER(PARTITION BY COD_UNICO ORDER BY NUM_DIAS_VENCIDOS DESC) ORDEN 
						FROM WEBBE_INFINITY_PROD_DIARIO A WHERE NUM_DIAS_VENCIDOS IS NOT NULL) A WHERE ORDEN = 1
						) AS C"), function($join)
					{
						$join->on('A.COD_UNICO', '=', 'C.COD_UNICO');
					})
					->leftjoin(DB::raw("(
						SELECT 
						COD_UNICO, MAX(CASE WHEN FECHA_CAIDA > DATEADD(M,-1,GETDATE()) OR FLG_VIGENTE = 1 THEN 1 ELSE 0 END) FLG_INFINITY_POLITICA, MAX(CASE WHEN FLG_VIGENTE = 1 THEN 1 ELSE 0 END) FLG_INFINITY_PENDIENTE 
						FROM WEBBE_INFINITY_CLIENTE_POLITICA GROUP BY COD_UNICO) AS T"), function($join)
					{
						$join->on('A.COD_UNICO', '=', 'T.COD_UNICO');
					})
					->select('A.COD_UNICO', 'F.NOMBRE_EJECUTIVO', DB::raw("(ISNULL(F.NOMBRE_JEFE, '') ) AS NOMBRE_JEFE"), DB::raw("(ISNULL(F.NOMBRE_ZONAL, '') ) AS NOMBRE_ZONAL"), DB::raw("(ISNULL(F.TIPO_BANCA, '') ) AS TIPO_BANCA"), DB::raw("(ISNULL(F.REGISTRO, '') ) AS REGISTRO"), DB::RAW("(CASE WHEN A.DIAS_ATRASO_HOM = 'Sin Atrasos' OR A.DIAS_ATRASO_HOM IS NULL THEN 1 ELSE 0 END) AS FLG_VIGENTE"), DB::raw("(CASE WHEN G.COD_UNICO IS NULL THEN 0 ELSE 1 END ) AS FLG_POLITICA_VIGENTE"), DB::raw("(CASE WHEN G.ID_CASO IS NOT NULL THEN 1 ELSE 0 END ) AS FLG_VIG_GESTIONADO"), DB::raw("(CASE WHEN A.RANGO_DEUDA_PROBLEMAS = 'Menos de 50M' THEN 1 ELSE 0 END) AS FLG_MENOR_50K"), DB::raw("(CASE WHEN A.RANGO_DEUDA_PROBLEMAS = 'Más de 50M' THEN 1 ELSE 0 END) AS FLG_MAYOR_50K"), DB::raw("(CASE WHEN A.DIAS_ATRASO_HOM <> 'Sin Atrasos' THEN 1 ELSE 0 END ) AS FLG_POLITICA_ATRASO"), DB::raw("(CASE WHEN C.NUM_DIAS_VENCIDOS IS NOT NULL AND B.COMPROMISO_PAGO < GETDATE() THEN 0 ELSE (CASE WHEN B.TIPO <> '' THEN 1 ELSE 0 END) END ) AS FLG_ATRAS_GESTIONADO"), DB::raw("(CASE WHEN A.FLG_INFINITY IN (1,2) THEN 1 ELSE 0 END) AS FLG_INFINITY"), DB::raw("(CASE WHEN T.FLG_INFINITY_POLITICA = 1 THEN 1 ELSE 0 END) AS FLG_POLITICA_INFINITY"),DB::raw("(CASE WHEN T.FLG_INFINITY_PENDIENTE = 1 THEN 0 ELSE 1 END) AS FLG_INFINITY_PENDIENTE"), DB::raw("(CASE WHEN NIVEL_ALERTA IN (1,11) THEN 1 ELSE 0 END) AS FLG_VERDE"), DB::raw("(CASE WHEN NIVEL_ALERTA IN (3,33) THEN 1 ELSE 0 END) AS FLG_ROJO"))
					->where(function ($query) {
						$query->whereNotNull("A.cod_unico")
								->orWhereIn ("a.FLG_INFINITY",array(1,2));
					})
					->orderBy('A.COD_UNICO', 'asc');
			
			if(!empty($filtros)){
				if(isset($filtros['ejecutivo'])) {
					$sql = $sql->where('F.REGISTRO', $filtros['ejecutivo']);
				}
				
				if(!isset($filtros['ejecutivo']) && isset($filtros['jefatura'])) {
					if(!empty($filtros['jefatura'])){
						$sql = $sql->where('F.NOMBRE_JEFE', $this->NombreJefe($filtros['jefatura']));
					}
				}
				if(!isset($filtros['ejecutivo']) && !isset($filtros['jefatura']) && isset($filtros['zonal'])) {
					$sql = $sql->where('F.NOMBRE_ZONAL', $filtros['zonal']);
				}
				if(!isset($filtros['ejecutivo']) && !isset($filtros['jefatura']) && !isset($filtros['zonal']) && isset($filtros['banca'])) {
					$sql = $sql->where('F.TIPO_BANCA', $filtros['banca']);
				}
			}
		}
		\Debugbar::info('query armada');
		\Debugbar::info($sql);
		return $sql;
		
	}

	function guardarvistaVideoGF($data)
	{
		DB::beginTransaction();
        $status = true;
        try {
            DB::table("WEBVPC_VISITA_VIDEOS")->insert($data);
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
	}

	function buscarusuarioGF($data)
	{
		return DB::table("WEBVPC_VISITA_VIDEOS")->where("REGISTRO","=",$data['REGISTRO'])->get()->first();
	}

	function usuariosvideosGF()
	{
		return DB::table("WEBVPC_USUARIO")->select("REGISTRO")->whereNotNull("ID_GC")->where("ID_ZONA","=","BELZONAL2")->wherein("ROL",array('20','21','23','24','25','52','53'))->get();
	}

	function agregargestiongys($gestiongys)
	{
		DB::beginTransaction();
        $status = true;
        try {
            DB::table("WEBBE_INFINITY_SEG_VIG_CASOS")->insert($gestiongys);
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
	}
}
