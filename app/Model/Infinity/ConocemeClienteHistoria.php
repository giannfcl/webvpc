<?php

namespace App\Model\Infinity;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class ConocemeClienteHistoria extends Model {

    function getByVisita($periodo, $visita) {
        $sql = DB::Table(DB::raw("(SELECT M.PERIODO,
        M.COD_UNICO,
        A.NUM_DOC,
        M.NOMBRE,
        ISNULL(A.BANCA,'BE') BANCA,
        M.COD_SECTORISTA,
        M.SALDO_INTERBANK,
        A.SALDO_RCC,
        ISNULL(A.RATING,VPC.RATING) RATING,
        A.FLG_INFINITY,
        A.CLASIFICACION,
        ISNULL(M.FEVE,VPC.FEVE) FEVE,
        A.SALDO_VENCIDO_IBK,
        A.SALDO_VENCIDO_RCC,
        A.METODIZADO_1_FECHA,
        A.METODIZADO_2_FECHA,
        A.FECHA_ULTIMA_VISITA,
        A.FECHA_ULTIMO_IBR,
        A.FECHA_ULTIMO_F02,
        A.GARANTIA,
        M.SALDO_RIESGO,
        A.FECHA_ULTIMO_RECALCULO,
        A.VENTAS,
        M.MAX_DIAS_ATRASO,
        A.PROVINCIA,
        A.DISTRITO,
        A.FECHA_VENCIMIENTO_POLITICA,
        A.FLG_VISIBLE,
        M.RMG,
        A.FECHA_INGRESO_INFINITY,
        CASE WHEN A.SEGMENTO IS NULL THEN 
        CASE WHEN ARB.BANCA = 'BEL' AND ARB.ZONAL = 'BEL ZONAL 2' THEN 'MEDIANA EMPRESA'  
            WHEN ARB.BANCA = 'BEL' AND ARB.ZONAL IN ('BEL ZONAL 3','BEL ZONAL 1') THEN 'GRAN EMPRESA' 
            WHEN ARB.BANCA = 'BEP' THEN 'PROVINCIA'
            WHEN ARB.BANCA = 'BC' THEN 'CORPORATIVA' END ELSE A.SEGMENTO END SEGMENTO,
        A.FECHA_CARGA,
        A.FLG_POLITICAPROCESO
        FROM (
        SELECT ISNULL(T1.PERIODO,'".($periodo)."') PERIODO, ISNULL(T1.COD_UNICO,T2.COD_UNICO) COD_UNICO,
            ISNULL(T1.NOMBRE,T2.NOMBRE) NOMBRE,ISNULL(T1.RMG,T2.RMG) RMG,
            ISNULL(T1.FEVE,T2.FEVE) FEVE,
            ISNULL(T1.MAX_DIAS_ATRASO,T2.MAX_DIAS_ATRASO) MAX_DIAS_ATRASO,
            ISNULL(T1.COD_SECTORISTA,T2.CODSECTORISTA) COD_SECTORISTA,
            ISNULL(T1.SALDO_INTERBANK,T2.SALDO_TOTAL) SALDO_INTERBANK,
            T2.SALDO_RIESGO
        FROM
        (SELECT DISTINCT PERIODO,COD_UNICO,FEVE,MAX_DIAS_ATRASO,RMG,NOMBRE,COD_SECTORISTA,SALDO_INTERBANK FROM WEBBE_INFINITY_CLIENTE WHERE BANCA = 'BE' AND PERIODO = '".($periodo)."') T1
        FULL OUTER JOIN
        (SELECT DISTINCT COD_UNICO,FEVE,RMG,NOMBRE,MAX_DIAS_ATRASO,CODSECTORISTA,SALDO_TOTAL,SALDO_RIESGO FROM WEBBE_INFINITY_INFO_DIARIA_CLI) T2
        ON T1.COD_UNICO = T2.COD_UNICO
        )M LEFT JOIN WEBBE_INFINITY_CLIENTE A ON M.COD_UNICO = A.COD_UNICO AND M.PERIODO = A.PERIODO
        LEFT JOIN ARBOL_DIARIO_2 ARB ON M.COD_SECTORISTA = ARB.COD_SECT_LARGO 
        LEFT JOIN WEBVPC_CLIENTES_VPC VPC ON M.COD_UNICO = VPC.COD_UNICO 
        WHERE LEFT(M.COD_SECTORISTA,3) != '070') AS WIC"))
                ->join('ARBOL_DIARIO_2 AS WLS', function($join) {
                    $join->on('WLS.COD_SECT_LARGO', '=', 'WIC.COD_SECTORISTA');
                })
                ->join('WEBVPC_USUARIO AS WU', 'WU.REGISTRO', '=', 'WLS.LOGIN')
                ->join('WEBBE_ZONAL AS WZ', 'WZ.ID_ZONAL', '=', 'WU.ID_ZONA')
                ->join('WEBBE_INFINITY_CONOCEME_HIST AS WICON', 'WICON.COD_UNICO', '=', 'WIC.COD_UNICO')
                ->join('WEBVPC_USUARIO AS WU1','WU1.REGISTRO','=','WICON.REGISTRO')
                ->select(ConocemeCliente::LISTA_CAMPOS_CONOCEME)
                ->addSelect('WICON.ID AS CONOCEME_HIST_ID','WICON.VISITA_ID AS CONOCEME_VISITA_ID')
                ->where('WIC.PERIODO', $periodo)
                ->where('WICON.VISITA_ID', $visita);
        return $sql;
    }

    function getByHistoriaId($periodo,$id){
        $sql = $this->getByVisita($periodo,$id);
        $sql->wheres = [];
        $sql = $sql
        ->where('WIC.PERIODO', $periodo)
        ->where('WICON.ID', $id)
        ->distinct();
        return $sql;
    }

    function getStakeholders($historia) {
        $sql = DB::Table('WEBBE_INFINITY_CONOCEME_STAKEHOLDER_HIST AS WICS')
                ->select('WICS.COD_UNICO', 'TIPO', 'DOCUMENTO', 'NOMBRE', 'CONCENTRACION', 'DESDE', 'EXCLUSIVIDAD', 'CONTRATO_FECHA_VENCIMIENTO', 'CONTRATO_ADJUNTO', 'NACIMIENTO','FLAG_CONTRATO')
                ->where('WICS.CONOCEME_HIST_ID', $historia);
        return $sql;
    }
    
    function getByCliente($periodo,$cliente){
        $sql = DB::Table(DB::raw("(SELECT M.PERIODO,
        M.COD_UNICO,
        A.NUM_DOC,
        M.NOMBRE,
        ISNULL(A.BANCA,'BE') BANCA,
        M.COD_SECTORISTA,
        M.SALDO_INTERBANK,
        A.SALDO_RCC,
        ISNULL(A.RATING,VPC.RATING) RATING,
        A.FLG_INFINITY,
        A.CLASIFICACION,
        ISNULL(M.FEVE,VPC.FEVE) FEVE,
        A.SALDO_VENCIDO_IBK,
        A.SALDO_VENCIDO_RCC,
        A.METODIZADO_1_FECHA,
        A.METODIZADO_2_FECHA,
        A.FECHA_ULTIMA_VISITA,
        A.FECHA_ULTIMO_IBR,
        A.FECHA_ULTIMO_F02,
        A.GARANTIA,
        M.SALDO_RIESGO,
        A.FECHA_ULTIMO_RECALCULO,
        A.VENTAS,
        M.MAX_DIAS_ATRASO,
        A.PROVINCIA,
        A.DISTRITO,
        A.FECHA_VENCIMIENTO_POLITICA,
        A.FLG_VISIBLE,
        M.RMG,
        A.FECHA_INGRESO_INFINITY,
        CASE WHEN A.SEGMENTO IS NULL THEN 
        CASE WHEN ARB.BANCA = 'BEL' AND ARB.ZONAL = 'BEL ZONAL 2' THEN 'MEDIANA EMPRESA'  
            WHEN ARB.BANCA = 'BEL' AND ARB.ZONAL IN ('BEL ZONAL 3','BEL ZONAL 1') THEN 'GRAN EMPRESA' 
            WHEN ARB.BANCA = 'BEP' THEN 'PROVINCIA'
            WHEN ARB.BANCA = 'BC' THEN 'CORPORATIVA' END ELSE A.SEGMENTO END SEGMENTO,
        A.FECHA_CARGA,
        A.FLG_POLITICAPROCESO
        FROM (
        SELECT ISNULL(T1.PERIODO,'".($periodo)."') PERIODO, ISNULL(T1.COD_UNICO,T2.COD_UNICO) COD_UNICO,
            ISNULL(T1.NOMBRE,T2.NOMBRE) NOMBRE,ISNULL(T1.RMG,T2.RMG) RMG,
            ISNULL(T1.FEVE,T2.FEVE) FEVE,
            ISNULL(T1.MAX_DIAS_ATRASO,T2.MAX_DIAS_ATRASO) MAX_DIAS_ATRASO,
            ISNULL(T1.COD_SECTORISTA,T2.CODSECTORISTA) COD_SECTORISTA,
            ISNULL(T1.SALDO_INTERBANK,T2.SALDO_TOTAL) SALDO_INTERBANK,
            T2.SALDO_RIESGO
        FROM
        (SELECT DISTINCT PERIODO,COD_UNICO,FEVE,MAX_DIAS_ATRASO,RMG,NOMBRE,COD_SECTORISTA,SALDO_INTERBANK FROM WEBBE_INFINITY_CLIENTE WHERE BANCA = 'BE' AND PERIODO = '".($periodo)."') T1
        FULL OUTER JOIN
        (SELECT DISTINCT COD_UNICO,FEVE,RMG,NOMBRE,MAX_DIAS_ATRASO,CODSECTORISTA,SALDO_TOTAL,SALDO_RIESGO FROM WEBBE_INFINITY_INFO_DIARIA_CLI) T2
        ON T1.COD_UNICO = T2.COD_UNICO
        )M LEFT JOIN WEBBE_INFINITY_CLIENTE A ON M.COD_UNICO = A.COD_UNICO AND M.PERIODO = A.PERIODO
        LEFT JOIN ARBOL_DIARIO_2 ARB ON M.COD_SECTORISTA = ARB.COD_SECT_LARGO 
        LEFT JOIN WEBVPC_CLIENTES_VPC VPC ON M.COD_UNICO = VPC.COD_UNICO 
        WHERE LEFT(M.COD_SECTORISTA,3) != '070') AS WIC"))
                ->join('WEBBE_INFINITY_CONOCEME_HIST AS WICON', 'WICON.COD_UNICO', '=', 'WIC.COD_UNICO')
                ->join('WEBVPC_USUARIO AS WU', 'WU.REGISTRO', '=', 'WICON.REGISTRO')
                ->select('WICON.ID','WIC.NOMBRE','WICON.TIPO','WICON.COD_UNICO'
                        ,'WICON.REGISTRO','WU.NOMBRE AS USUARIO','WICON.FECHA_ACTUALIZACION')
                ->where('WIC.COD_UNICO', $cliente);
        return $sql;
    }

    function getCommodities($historia) {
        $sql = DB::Table('WEBBE_INFINITY_CONOCEME_COMMODITIE_HIST AS WIL')
                ->select('COD_UNICO', 'NOMBRE')
                ->where('CONOCEME_HIST_ID', $historia);
        return $sql;
    }

    function getLineas($historia) {
        $sql = DB::Table('WEBBE_INFINITY_LINEAS_HIST AS WIL')
                ->select('COD_UNICO', 'BANCO', 'LINEA', 'TIPO_GARANTIA')
                ->where('CONOCEME_HIST_ID', $historia);
        return $sql;
    }

    function getMixVentas($historia) {
        $sql = DB::Table('WEBBE_INFINITY_MIX_VENTAS_HIST AS WIMV')
                ->select('COD_UNICO', 'PRODUCTO', 'PARTICIPACION')
                ->where('CONOCEME_HIST_ID', $historia);
        return $sql;
    }

     function getInversiones($historia) {
        $sql = DB::Table('WEBBE_INFINITY_INVERSIONES_HIST AS WII')
                ->select('COD_UNICO', 'TIPO_INVERSION')
                ->where('CONOCEME_HIST_ID', $historia);
        return $sql;
    }

    function getCanales($historia) {
        $sql = DB::Table('WEBBE_INFINITY_CANAL_VENTAS_HIST AS WICV')
                ->select('COD_UNICO', 'CANAL_VENTAS', 'PARTICIPACION')
                ->where('CONOCEME_HIST_ID', $historia)
                ->orderBy('PARTICIPACION', 'desc');
        return $sql;
    }

    function getZonas($historia) {
        $sql = DB::Table('WEBBE_INFINITY_CONOCEME_ZONA_HIST AS WIL')
                ->select('COD_UNICO', 'TIPO', 'ZONA')
                ->where('CONOCEME_HIST_ID', $historia);
        return $sql;
    }

}
