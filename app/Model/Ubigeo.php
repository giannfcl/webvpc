<?php

namespace App\Model;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class Ubigeo extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'WEBVPC_UBIGEO';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = null;
	public $incrementing = false;

    protected $guarded = [
		'DEPARTAMENTO',
        'PROVINCIA',
        'DISTRITO',
    ];

    function getDepartamento() {
        $sql = DB::table('WEBVPC_UBIGEO as WU')
            ->select('WU.DEPARTAMENTO')
            ->orderBy('WU.DEPARTAMENTO')
            ->distinct();
        return $sql;
    }

    function getProvincia($departamento = null) {
        $sql = DB::table('WEBVPC_UBIGEO as WU')
            ->select('WU.PROVINCIA')
            ->orderBy('WU.PROVINCIA')
            ->distinct();
        if ($departamento){
            $sql = $sql->where('WU.DEPARTAMENTO','=',$departamento);
        }
        return $sql;
    }

    function getDistrito($provincia = null, $departamento = null) {
        $sql = DB::table('WEBVPC_UBIGEO as WU')
            ->select('WU.DISTRITO')
            ->orderBy('WU.DISTRITO')
            ->distinct();
        if ($provincia){
            $sql = $sql->where('WU.PROVINCIA','=',$provincia);
        }
        if ($departamento){
            $sql = $sql->where('WU.DEPARTAMENTO','=',$departamento);
        }
        return $sql;
    }

    function getComboDepartamento() {
        $sql = DB::table('VISTA_UBIGEO as WU')
                ->select(DB::raw('WU.CodigoDepartamento, WU.NombreDepartamento'))
                ->orderBy('WU.NombreDepartamento')
                ->distinct();
        return $sql;
    }

    function getComboProvincia($departamento = null) {
        $sql = DB::table('VISTA_UBIGEO as WU')
                ->select(DB::raw('WU.CodigoProvincia,WU.NombreProvincia'))
                ->orderBy('WU.NombreProvincia')
                ->distinct();
                if ($departamento){
                    $sql = $sql->where('WU.CodigoDepartamento','=',$departamento);
                }
        return $sql;
    }

    function getComboDistrito($provincia = null, $departamento = null) {
        $sql = DB::table('VISTA_UBIGEO as WU')
                ->select(DB::raw('WU.CodigoDistrito,WU.NombreDistrito'))
                ->orderBy('WU.NombreDistrito')
                ->distinct();
                if ($provincia){
                    $sql = $sql->where('WU.CodigoProvincia','=',$provincia);
                }
                if ($departamento){
                    $sql = $sql->where('WU.CodigoDepartamento','=',$departamento);
                }
        return $sql;
    }

}
