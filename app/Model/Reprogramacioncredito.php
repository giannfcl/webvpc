<?php
namespace App\Model;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class Reprogramacioncredito extends Model
{
    const TODOS = array('1','2','3');
    function getListaRP($data,$ejecutivo = null,$riesgos = null,$flgAllRiesgos = null)
    {
        // dd($data['rol']);
        $sql=DB::table("WEBVPC_CARTERA_COVID19 AS WCC")
            ->leftjoin("WEBVPC_CARTERA_COVID19_GESTIONES_ESTADOS AS WCCGE",function($join){
                $join->on('WCC.GESTION_ESTADO', '=', 'WCCGE.ID_GESTION_ESTADO');
            })
            ->leftjoin(DB::Raw("(SELECT * FROM ( SELECT *,ROW_NUMBER() OVER (PARTITION BY COD_CREDITO ORDER BY GESTION_FECHA DESC) ORDEN FROM WEBVPC_CARTERA_COVID19_GESTIONES ) A WHERE ORDEN=1) as WCCG"),function($join){
                $join->on('WCC.COD_CREDITO', '=', 'WCCG.COD_CREDITO');
            })
            ->leftjoin(DB::Raw("WEBVPC_USUARIO AS WU1"),function($join){
                $join->on('WCC.REGISTRO_EN', '=', 'WU1.REGISTRO');
            })
            ->leftjoin(DB::Raw("WEBVPC_USUARIO AS WU2"),function($join){
                $join->on('WCC.RIESGOS_RESPONSABLE', '=', 'WU2.REGISTRO');
            })
            ->select("WCC.*","WU1.NOMBRE AS NOMBREEJECUTIVO","WU2.NOMBRE AS NOMBRERIESGOS"
                    ,"WCCGE.*",DB::Raw("(ISNULL(CONVERT(varchar(20), WCC.MAX_DIAS_DE_GRACIA),'POR DEFINIR')) as DIAS"),DB::Raw("(ISNULL(CONVERT(varchar(20), WCC.MAX_CUOTAS_ADICIONAL),'POR DEFINIR')) as CUOTAS_ADIC"),DB::Raw("ISNULL(NUM_DOC_RRLL,' ') as RRLL"),DB::Raw("ISNULL(CLIENTE_RRLL,' ') as NOMBRE_RRLL"),DB::Raw("ISNULL(TEA,0) as V_TEA"),"WCCG.R_DIAS AS G_DIAS","WCCG.R_CUOTAS_ADIC AS G_CUOTAS_ADIC","WCCG.GESTION_TELEFONO AS G_GESTION_TELEFONO","WCCG.GESTION_EMAIL AS G_GESTION_EMAIL","WCCG.GESTION_COMENTARIO AS G_GESTION_COMENTARIO","WCCG.PROPUESTA_RIESGOS_COMENTARIOS as G_PROPUESTA_RIESGOS_COMENTARIOS",DB::Raw('CONVERT(DATE,WCCG.FECHA_PROX_PAGO_REVERTIR) AS G_FECHA_PROX_PAGO_REVERTIR'))
            ->where("FLG_VISIBLE","=",DB::Raw(1));
        if ($ejecutivo) {
                $sql = $sql->where("REGISTRO_EN",$ejecutivo);
        }
        if ($riesgos) {
            $sql = $sql->where("RIESGOS_RESPONSABLE",$riesgos)
                        ->wherein("WCC.RIESGOS_GESTION",self::TODOS);
        }
        if ($flgAllRiesgos) {
            $sql = $sql->whereNotNull("RIESGOS_RESPONSABLE")
                        ->wherein("WCC.RIESGOS_GESTION",self::TODOS);
        }
        if (isset($data['codcredito'])) {
            $sql = $sql->where("WCC.COD_CREDITO",$data['codcredito']);
        }
        if (isset($data['mcodcredito'])) {
            $sql = $sql->where("WCC.COD_CREDITO",$data['mcodcredito']);
        }
        if (isset($data['riesgos_gestion'])) {
            $sql = $sql->where("WCC.RIESGOS_GESTION",$data['riesgos_gestion']);
        }
        return $sql;
    }

    function getEstadosGestionRC()
    {
        $sql=DB::table("WEBVPC_CARTERA_COVID19_GESTIONES_ESTADOS");
        return $sql->get();
    }

    function gestionar($data,$actualizar,$gestion)
    {
        //dd($data,$actualizar,$gestion);
        DB::beginTransaction();
        $status = true;
        try {
            DB::table('WEBVPC_CARTERA_COVID19_GESTIONES')->insert($gestion);

            DB::table('WEBVPC_CARTERA_COVID19')
                    ->where('COD_CREDITO', '=', $gestion['COD_CREDITO'])
                    ->update($actualizar);
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }

        return $status;
    }

    function getEjecutivoActual($credito){
        $sql=DB::table('WEBVPC_CARTERA_COVID19 as A')
            ->join('WEBVPC_CARTERA_COVID19 as B',function($join){
                $join->on('A.NUM_DOC', '=', 'B.NUM_DOC');
            })
            ->join('WEBVPC_CARTERA_COVID19_EJECUTIVOS_RIESGOS AS C',function($join)
            {
                $join->on('B.RIESGOS_RESPONSABLE','=','C.REGISTRO');
                $join->on('C.FLG_ACTIVO','=',DB::Raw("1"));
            })
            ->select('B.RIESGOS_RESPONSABLE','A.NUM_DOC')
            ->where('A.COD_CREDITO',$credito)
            ->whereNotNull('B.RIESGOS_RESPONSABLE');
            // dd($sql->toSql());
        return $sql->first();
    }

    function getEjecutivoMenorCarga(){

        $sql=DB::table('WEBVPC_CARTERA_COVID19_EJECUTIVOS_RIESGOS as WCER')
            ->leftJoin('WEBVPC_CARTERA_COVID19 as WCLR',function($join){
                $join->on('WCLR.RIESGOS_RESPONSABLE', '=', 'WCER.REGISTRO');
                $join->on(DB::raw('CONVERT(DATE, WCLR.RIESGOS_FECHA_ASIGNACION)'),'>=', DB::raw('DATEADD(DAY,-3,CONVERT(DATE,GETDATE()))'));
            })
            ->select('WCER.REGISTRO as REGISTRO_RIESGOS',DB::raw('COUNT(WCLR.COD_CREDITO) as PENDIENTES'))
            ->where('WCER.FLG_ACTIVO',1)
            ->where('WCER.BANCA','BPE')
            ->groupBy('WCER.REGISTRO')
            ->orderBy(DB::raw('COUNT(WCLR.COD_CREDITO) '),'asc');
        // dd($sql->toSql());
        return $sql->first();
    }

}