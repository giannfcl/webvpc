<?php

namespace App\Model;

use Jenssegers\Date\Date as Carbon;
use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class Gestioncartera extends Model
{

    //Primer Filtro
    static function getBanca($id_gc)
    {
        $sql = DB::table('T_WEB_ARBOL_DIARIO')
            ->select('BANCA')
            ->distinct()
            ->where('ID', $id_gc);
        return $sql->get();
    }
    static function getZonal($id_gc)
    {
        $sql = DB::table('T_WEB_ARBOL_DIARIO')
            ->select('NOMBRE_ZONAL')
            ->distinct()
            ->where('ID', $id_gc);
        return $sql->get();
    }
    static function getJefe($id_gc)
    {
        $sql = DB::table('T_WEB_ARBOL_DIARIO')
            ->select('NOMBRE_JEFE')
            ->distinct()
            ->where('ID', $id_gc);
        return $sql->get();
    }
    static function getEjecutivo($id_gc)
    {
		$sql = DB::table('T_WEB_ARBOL_DIARIO')
			// ->select(DB::Raw("CASE WHEN ID IN ('BC GRUPO 1','BC GRUPO 2','BEP ZONAL 2','BEL ZONAL 1','BEL ZONAL 3','BEP ZONAL 1','BEL ZONAL 2','BEP ZONAL 3') THEN NULL ELSE ENCARGADO END AS ENCARGADO"))
			->select('ENCARGADO')
			->distinct()
			->where('ID', $id_gc)
			;
			// dd($sql->get());
		return $sql->get();
    }
    //Filtro UI
    static function getBancas()
    {
        $sql = DB::table('T_WEB_ARBOL_DIARIO')
            ->select('BANCA')
            ->distinct();
        return $sql->get();
    }
    static function getZonales($banca)
    {
        $sql = DB::table('T_WEB_ARBOL_DIARIO')
            ->select('NOMBRE_ZONAL', 'BANCA')
            ->distinct();
        if (isset($banca)) {
            $sql = $sql->where('BANCA', $banca);
        }
        return $sql->get();
    }
    static function getJefes($banca, $zonal)
    {
        $sql = DB::table('T_WEB_ARBOL_DIARIO')
            ->select('NOMBRE_JEFE', 'BANCA', 'NOMBRE_ZONAL')
            ->distinct();
        if (isset($banca)) {
            $sql = $sql->where('BANCA', $banca);
        }

        if (isset($zonal)) {
            $sql = $sql->where('NOMBRE_ZONAL', $zonal);
        }
        
        return $sql->get();
    }
    static function getEjecutivos($banca, $zonal, $jefe)
    {
        $sql = DB::table('T_WEB_ARBOL_DIARIO')
            ->select('ENCARGADO', 'BANCA', 'NOMBRE_ZONAL', 'NOMBRE_JEFE')
            ->distinct();

        if (isset($banca)) {
            $sql = $sql->where('BANCA', $banca);
        }
        if (isset($zonal)) {
            $sql = $sql->where('NOMBRE_ZONAL', $zonal);
        }
        if (isset($jefe)) {
            $sql = $sql->where('NOMBRE_JEFE', $jefe);
        }
        return $sql->get();
    }

    static function coldirectas($datos)
    {
        $sqlCD = DB::table('T_WEB_SALDOS')->select('id', 'M1', 'M2', 'M3', 'M4', 'M5', 'M6', 'M7', 'M8', 'M9', 'M10', 'M11', 'M12', 'VAR_MILES', 'VAR', 'VAR_P', 'CUMPLIMIENTO', 'META_MILES', 'SALPUNT_SOL', 'SALPROM_SOL', 'META_ANUAL', 'AVANCE')->where('id', '=', $datos['id'])->where('TIPOPRODUCTO', '=', 'COL_DIRECTAS')->get();
        $sqlCI = DB::table('T_WEB_SALDOS')->select('id', 'M1', 'M2', 'M3', 'M4', 'M5', 'M6', 'M7', 'M8', 'M9', 'M10', 'M11', 'M12', 'VAR_MILES', 'VAR', 'VAR_P', 'CUMPLIMIENTO', 'META_MILES', 'SALPUNT_SOL', 'SALPROM_SOL', 'META_ANUAL', 'AVANCE')->where('id', '=', $datos['id'])->where('TIPOPRODUCTO', '=', 'COL_INDIRECTAS')->get();
        $sqlD = DB::table('T_WEB_SALDOS')->select('id', 'M1', 'M2', 'M3', 'M4', 'M5', 'M6', 'M7', 'M8', 'M9', 'M10', 'M11', 'M12', 'VAR_MILES', 'VAR', 'VAR_P', 'CUMPLIMIENTO', 'META_MILES', 'SALPUNT_SOL', 'SALPROM_SOL', 'META_ANUAL', 'AVANCE')->where('id', '=', $datos['id'])->where('TIPOPRODUCTO', '=', 'DEPOSITOS')->get();
        $sqlFecha = DB::table('T_WEB_FECHAS')->select()->where("FUENTE", "T_WEB_ESTADO_LINEAS")->get();
        $sqlFechaLineas = DB::table('T_WEB_FECHAS')->select()->where("FUENTE", "T_WEB_ESTADO_LINEAS")->get();
        $sqlFechaMercado = DB::table('T_WEB_FECHAS')->select()->where("FUENTE", "T_WEB_MERCADO")->get();
        $sql = array("DIRECTAS" => $sqlCD, "INDIRECTAS" => $sqlCI, "DEPOSITOS" => $sqlD, "FECHAS" => $sqlFecha, "FECHASLINEAS" => $sqlFechaLineas, "FECHASMERCADO" => $sqlFechaMercado);
        return $sql;
    }
    static function heatanalytic($datos, $registro, $id)
    {
        $now = Carbon::now();
        $sql = array("HORA" => $now, "IDHTML" => $datos['IDHTML'], "REGISTRO" => $registro, "ID" => $id, "TIPO" => $datos['TIPO']);
        DB::table('HEATANALYTIC')->insert($sql);
    }
    static function Tipo_productos($banca = null, $zonal = null, $jefe = null, $encargado = null)
    {
        $sql = DB::table('T_WEB_SALDOS_PRODUCTO')
            ->select('ENCARGADO', 'BANCA', 'NOMBRE_ZONAL', 'NOMBRE_JEFE', 'TIPOPRODUCTO')
            ->distinct();
        if (isset($banca)) {
            $sql = $sql->where('BANCA', $banca);
        }
        if (isset($zonal)) {
            $sql = $sql->where('NOMBRE_ZONAL', $zonal);
        }
        if (isset($jefe)) {
            $sql = $sql->where('NOMBRE_JEFE', $jefe);
        }
        if (isset($encargado)) {
            $sql = $sql->where('ENCARGADO', $encargado);
        }
        return $sql->select('TIPOPRODUCTO')->distinct()->get();
    }
    static function DetalleClienteEvolutivo($datos)
    {

        $sqlIBK = DB::table('DBO.T_WEB_DETALLE_CLIENTE_02')
            ->select()
            ->where('CODUNICOCLI', '=', $datos['CODUNICOCLI'])
            ->where('TIPOPRODUCTO', '=', $datos['TIPOPRODUCTO'])
            ->where('BANCO', '=', 'IBK')
            ->distinct()->get();
        $sqlBCP = DB::table('DBO.T_WEB_DETALLE_CLIENTE_02')
            ->select()
            ->where('CODUNICOCLI', '=', $datos['CODUNICOCLI'])
            ->where('TIPOPRODUCTO', '=', $datos['TIPOPRODUCTO'])
            ->where('BANCO', '=', 'BCP')
            ->distinct()->get();
        $sqlBBVA = DB::table('DBO.T_WEB_DETALLE_CLIENTE_02')
            ->select()
            ->where('CODUNICOCLI', '=', $datos['CODUNICOCLI'])
            ->where('TIPOPRODUCTO', '=', $datos['TIPOPRODUCTO'])
            ->where('BANCO', '=', 'BBVA')
            ->distinct()->get();
        $sqlSCOTIA = DB::table('DBO.T_WEB_DETALLE_CLIENTE_02')
            ->select()
            ->where('CODUNICOCLI', '=', $datos['CODUNICOCLI'])
            ->where('TIPOPRODUCTO', '=', $datos['TIPOPRODUCTO'])
            ->where('BANCO', '=', 'SCOTIA')
            ->distinct()->get();
        $sqlBIF = DB::table('DBO.T_WEB_DETALLE_CLIENTE_02')
            ->select()
            ->where('CODUNICOCLI', '=', $datos['CODUNICOCLI'])
            ->where('TIPOPRODUCTO', '=', $datos['TIPOPRODUCTO'])
            ->where('BANCO', '=', 'BIF')
            ->distinct()->get();
        $sqlOtros = DB::table('DBO.T_WEB_DETALLE_CLIENTE_02')
            ->select()
            ->where('CODUNICOCLI', '=', $datos['CODUNICOCLI'])
            ->where('TIPOPRODUCTO', '=', $datos['TIPOPRODUCTO'])
            ->where('BANCO', '=', 'OTROS')
            ->distinct()->get();

        return array(
            "IBK" => json_encode(json_decode($sqlIBK, true)),
            "BCP" => json_encode(json_decode($sqlBCP, true)),
            "BBVA" => json_encode(json_decode($sqlBBVA, true)),
            "SCOTIA" => json_encode(json_decode($sqlSCOTIA, true)),
            "BIF" => json_encode(json_decode($sqlBIF, true)),
            "OTROS" => json_encode(json_decode($sqlOtros, true))
        );
    }
    static function DetalleClienteFOTO($datos)
    {
        $sql = DB::table('DBO.T_WEB_DETALLE_CLIENTE_03')
            ->select()
            ->where('CODUNICOCLI', '=', $datos['CODUNICOCLI'])
            ->where('TIPOPRODUCTO', '=', $datos['TIPOPRODUCTO'])
            ->get();
        $sql2 = DB::table('DBO.T_WEB_DETALLE_CLIENTE_01')
            ->select()
            ->where('CODUNICOCLI', '=', $datos['CODUNICOCLI'])
            ->get();
        $arr = array("FOTO1" => json_encode(json_decode($sql, true)), "FOTO2" => json_encode(json_decode($sql2, true)));
        return $arr;
    }
    static function VarXProducto($datos)
    {
        $sql = DB::table('T_WEB_SALDOS_PRODUCTO')
            ->select('ENCARGADO', 'BANCA', 'NOMBRE_ZONAL', 'NOMBRE_JEFE', 'TIPOPRODUCTO', 'FECHA', DB::raw("RIGHT('0' + DATENAME(DAY, FECHA), 2) + ' ' + DATENAME(MONTH, FECHA) as FECHA2"), 'SALDO_MILES')
            ->where('id', '=', $datos['id'])
            ->where('TIPOPRODUCTO', '=', $datos['tipoproducto'])
            ->orderBy('FECHA', 'ASC')
            ->distinct();
        return $sql->get();
    }

    static function getArbol($id)
    {

        $sql = DB::table('T_WEB_ARBOL_DIARIO')
            ->select()
            ->where('ID', '=', $id)
            ->get()
            ->first();
        
        return $sql;
    }
    static function getUsuario2($banca = null, $zonal = null, $jefe = null, $ejecutivo = null)
    {
        $sql = DB::table('T_WEB_ARBOL_DIARIO')
            ->select('ENCARGADO', 'BANCA', 'NOMBRE_ZONAL', 'NOMBRE_JEFE', 'ID')
            ->distinct();

        if ($banca != 'null') {
            $sql = $sql->where('BANCA', $banca);
        } else {
            $sql = $sql->where('BANCA', null);
        }

        if ($zonal != 'null') {
            $sql = $sql->where('NOMBRE_ZONAL', $zonal);
        } else {
            $sql = $sql->where('NOMBRE_ZONAL', null);
        }
        if ($jefe != 'null') {
            $sql = $sql->where('NOMBRE_JEFE', $jefe);
        } else {
            $sql = $sql->where('NOMBRE_JEFE', null);
        }

        if ($ejecutivo != 'null') {
            $sql = $sql->where('ENCARGADO', $ejecutivo);
        } else {
            $sql = $sql->where('ENCARGADO', null);
        }

        return $sql->select('ID')->distinct()->get();
    }
    static function getCumplimiento($datos)
    {
        if ($datos['id']) {
            $id = $datos['id'];
        }
        $sql = DB::table('T_WEB_TABLERO')
            ->select('ID', 'BANCA', 'NOMBRE_ZONAL', 'NOMBRE_JEFE', 'ENCARGADO', 'CANT_CLIE', DB::raw('ROUND(NOTA*100,2) NOTA'), DB::raw('ROUND(NOTA_FINANCIERA*100,2) NOTA_FINANCIERA'), DB::raw('ROUND(NOTA_ECOSISTEMA*100,2) NOTA_ECOSISTEMA'))
            ->where('ID', '=', $id)
            ->get()
            ->first();
        return $sql;
    }
    static function getTopBottomMERCADO($datos)
    {
        $id = $datos['id'];
        $tipoFrecuencia = $datos['tipoFrecuencia'];
        $tipoBanco = $datos['tipoBanco'];
        $tipoProducto = $datos['tipoProducto'];
        $topBtm = $datos['topBtm'];
        $sql = DB::table('T_WEB_MERCADO_PRINC_VARIACIONES')->select();

        if (isset($id) && $id != "null") {
            $sql =  $sql->where('ID', '=', $id);
        }

        if (isset($tipoFrecuencia) && $tipoFrecuencia != "null") {
            $sql =  $sql->where('TIPO_VARIACION', '=', $tipoFrecuencia);
        }
        if (isset($tipoBanco) && $tipoBanco != "null") {
            $sql =  $sql->where('BANCO', '=', $tipoBanco);
        }
        if (isset($tipoProducto) && $tipoProducto != "null") {
            $sql =  $sql->where('TIPOPRODUCTO', '=', $tipoProducto);
        }
        if ($topBtm == "true") {
            $sql =  $sql->where('TIPO', '=', 'TOP')->orderby('VAR', 'DESC');
        } else {
            $sql =  $sql->where('TIPO', '=', 'BOTTOM')->orderby('VAR', 'ASC');
        }
        return $sql->get();
    }
    static function getTopBottomMERCADOFILTRO1($id)
    {
        $sql = DB::table('T_WEB_MERCADO_PRINC_VARIACIONES')
            ->select('ID', 'BANCO')
            ->where('ID', '=', $id)
            ->distinct()
            ->get();
        return $sql;
    }
    static function getTopBottomMERCADOFILTRO2($id)
    {
        $sql = DB::table('T_WEB_MERCADO_PRINC_VARIACIONES')
            ->select('ID', 'TIPO_VARIACION')
            ->where('ID', '=', $id)
            ->distinct()
            ->get();
        return $sql;
    }
    static function getTopBtm($datos)
    {

        $id = $datos['id'];
        $tipoFrecuencia = $datos['tipoFrecuencia'];
        $tipoProducto = $datos['tipoProducto'];
        $topBtm = $datos['topBtm'];
        $sql = DB::table('T_WEB_VARIACIONES_SALDOS')->select('ID', 'COD_UNICO', 'CODDOC', 'NOMBRE_CLIENTE', 'RANKING', 'TIPO', 'VAR', 'TIPO_VAR', 'TIPOPRODUCTO', 'SALDO_PUNTUAL');

        if (isset($id) && $id != "null") {
            $sql =  $sql->where('ID', '=', $id);
        }

        if (isset($tipoFrecuencia) && $tipoFrecuencia != "null") {
            $sql =  $sql->where('TIPO_VAR', '=', $tipoFrecuencia);
        }
        if (isset($tipoProducto) && $tipoProducto != "null") {
            $sql =  $sql->where('TIPOPRODUCTO', '=', $tipoProducto);
        }
        if ($topBtm == "true") {
            $sql =  $sql->where('TIPO', '=', 'TOP')->orderby('VAR', 'DESC');
        } else {
            $sql =  $sql->where('TIPO', '=', 'BOTTOM')->orderby('VAR', 'ASC');
        }
        return $sql->get();
    }
    static function tipoFrecuencia($id)
    {
        $sql = DB::table('T_WEB_VARIACIONES_SALDOS')
            ->select('ID', 'TIPO_VAR')
            ->where('ID', '=', $id)
            ->distinct()
            ->get();
        return $sql;
    }
    static function tipoProductosBtm($id)
    {
        $sql = DB::table('T_WEB_MERCADO_CARTERA_SOW')
            ->select('ID', 'TIPOPRODUCTO')
            ->distinct()
            ->where('ID', '=', $id)
            ->get();
        return $sql;
    }
    static function LineaTabla($id)
    {
        $sql = DB::table('T_WEB_ESTADO_LINEAS')
            ->select('ID', 'CTD_LINEAS', 'VAR_CANT_LINEAS', 'UTILIZACION', 'VAR_UTILIZACION', 'CTD_VIGENTES', 'CTD_A_VENCER_MES', 'RIESGO_MAXIMO', 'VAR_RIESGO_MAXIMO')
            ->distinct()
            ->where('ID', '=', $id['id'])
            ->get()->first();
        return $sql;
    }
    static function MercadoTabla($id)
    {
        $sql = DB::table('T_WEB_MERCADO_CARTERA')
            ->select(
                'ID',
                'CTD_CLIENTES_DIR_SF',
                'VAR_CTD_CLIENTES_DIR_SF',
                'COL_DIRECTAS_VIG',
                'VAR_COL_DIRECTAS_VIG',
                'COL_INDIRECTAS_TOT',
                'VAR_COL_INDIRECTAS_TOT',
                'GAR_SF',
                'VAR_GAR_SF'
            )
            ->distinct()
            ->where('ID', '=', $id['id'])
            ->get()->first();
        return $sql;
    }
    static function MercadoPieGrafico($datos)
    {
        $id = $datos['id'];
        $tipoProducto = $datos['tipoProducto'];
        $sql = DB::table('T_WEB_MERCADO_CARTERA_SOW')
            ->select('ID', 'TIPOPRODUCTO', 'SOW_BCP', 'SOW_IBK', 'SOW_SCT', 'SOW_BBV', 'SOW_BIF')
            ->where('ID', '=', $id)
            ->distinct();
        if (isset($tipoProducto)) {
            $sql = $sql->where('TIPOPRODUCTO', $tipoProducto);
        }
        return $sql->get();
    }
    static function MercadoLineGrafico($datos)
    {
        $id = $datos['id'];
        $tipoProducto = $datos['tipoProducto'];
        $sql = DB::table('T_WEB_SALDOS_SF')
            ->select('ID', 'TIPOPRODUCTO', 'PERIODO AS CODMES', 'BANCO_SCOTIA', 'BANCO_BBVA', 'BANCO_BCP', 'BANCO_IBK', 'BANCO_BIF')
            ->where('ID', '=', $id)
            ->distinct();
        if (isset($tipoProducto)) {
            $sql = $sql->where('TIPOPRODUCTO', $tipoProducto);
        }
        return $sql->orderby('CODMES', 'ASC')->get();
    }
    static function Descuentos($datos)
    {
        $id = $datos['id'];
        $sql = DB::table('T_WEB_DESCUENTOS')
            ->select('ID', 'CAIDA', 'AUMENTO', 'SALDO_VEN_P0', 'DSCTO_SF_M0', 'PCT_CAIDA', 'PCT_AUMENTO', 'PCT_SF')
            ->where('ID', '=', $id)
            ->distinct()->get();
        $sql2 = DB::table('T_WEB_DESCUENTOS_EVOLUTIVO')
            ->select('ID', 'CODMES', 'SALDO_DEF', 'SALDO_DFF', 'SALDO_DL', 'SALDO_DP', 'SALDO_FAC', 'SALDO_FN')
            ->where('ID', '=', $id)
            ->distinct();
        $sql2 = $sql2->orderby('CODMES', 'ASC')->get();
        $arr = array("FOTO" => json_encode(json_decode($sql, true)), "EVOLUTIVO" => json_encode(json_decode($sql2, true)));
        return  $arr;
    }
    static function PAP($datos)
    {
        $id = $datos['id'];
        $sql = DB::table('T_WEB_PAP_INDICADORES')
            ->select('ID', 'CTD_ORD_ACT', 'CTD_BEN_ACT', 'NUEVO_BEN_ACT', 'CTD_PROX_ACT', 'VAR_CTD_ORD', 'VAR_NUEVOS_BEN', 'VAR_CTD_BEN')
            ->where('ID', '=', $id)
            ->distinct()
            ->get();
        $sql2 = DB::table('T_WEB_PAP_INDICADORES')
            ->select()
            ->where('ID', '=', $id)
            ->distinct()->get();
        $sql3 = DB::table('T_WEB_PAP_EVOLUTIVO')
            ->select(
                'ID',
                'VOL_PAP_CTA',
                'VOL_PAP_CON_CTA',
                'VOL_PAP_SIN_CTA',
                'PERIODO'
            )
            ->where('ID', '=', $id)
            ->orderby('PERIODO', 'ASC')
            ->distinct()->get();
        $sql4 = DB::table('T_WEB_DESCUENTO_TABLERO')
            ->select()
            ->where('ID', '=', $id)
            ->distinct()->get();
        $arr = array(
            "FOTO" => json_encode(json_decode($sql, true)),
            "EVOLUTIVO" => json_encode(json_decode($sql2, true)),
            "EVOLUTIVO2" => json_encode(json_decode($sql3, true)),
            "DCTO" => json_encode(json_decode($sql4, true))
        );
        return  $arr;
    }
    static function CSUELDO($datos)
    {
        $id = $datos['id'];
        $sql = DB::table('T_WEB_CUENTA_EVOLUTIVO')
            ->select('ID', 'MES', 'MONTO_FOCO')
            ->where('ID', '=', $id)
            ->distinct();
        $sql = $sql->orderby('MES', 'ASC')->get();
        return  $sql;
    }
    static function CUENTASUELDO($datos)
    {
        $sqlCS = DB::table('T_WEB_CUENTA')->select('id', 'M1', 'M2', 'M3', 'M4', 'M5', 'M6', 'M7', 'M8', 'M9', 'M10', 'M11', 'M12', 'VAR', 'VAR_P', 'CUMP_P', 'META', 'MONTO_FOCO')->where('id', '=', $datos['id'])->get();
        $sqlDF = DB::table('T_WEB_ACTIVIDADES_ECOSISTEMA')->select('id', 'M1', 'M2', 'M3', 'M4', 'M5', 'M6', 'M7', 'M8', 'M9', 'M10', 'M11', 'M12', 'VAR', 'VAR_P', 'CUMP_P', 'META', 'SALDO_ACT')->where('id', '=', $datos['id'])->where('TIPOPRODUCTO', '=', 'DESCTOS Y FACTORING')->get();
        $sqlPAP = DB::table('T_WEB_ACTIVIDADES_ECOSISTEMA')->select('id', 'M1', 'M2', 'M3', 'M4', 'M5', 'M6', 'M7', 'M8', 'M9', 'M10', 'M11', 'M12', 'VAR', 'VAR_P', 'CUMP_P', 'META', 'SALDO_ACT')->where('id', '=', $datos['id'])->where('TIPOPRODUCTO', '=', 'PAP')->get();
        $sqlDES = DB::table('T_WEB_FECHAS')->select()->where('FUENTE', "T_WEB_DESCUENTOS_1")->get();
        $sqlFPAP = DB::table('T_WEB_FECHAS')->select()->where('FUENTE', '=', "T_WEB_PAP")->get();
        $sql = array("CSUELDO" => $sqlCS, "FECHADES" => $sqlDES, "FECHAPAP" => $sqlFPAP, "DESCTOS Y FACTORING" => $sqlDF, 'PAP' => $sqlPAP);
        return $sql;
    }
}
