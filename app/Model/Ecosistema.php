<?php

namespace App\Model;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date as Carbon;

class Ecosistema extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'WEBVPC_ECOSISTEMA';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = null;
    public $incrementing = false;

    public $timestamps = false;

    static function getEcosistema(){
        $sql=DB::table('WEBVPC_ECOSISTEMA as WE')
        ->select('WE.*',
            DB::Raw('CASE WHEN A1.NUM_DOC IS NULL THEN 0 ELSE 1 END FLG_CLIENTE_IBK_1'),
            DB::Raw('CASE WHEN A2.NUM_DOC IS NULL THEN 0 ELSE 1 END FLG_CLIENTE_IBK_2'))
        ->leftJoin(
            DB::Raw("(SELECT DISTINCT WEE.NUM_DOC
                FROM WEBVPC_EMPRESAS_ECOSISTEMA AS WEE
                LEFT JOIN ARBOL_DIARIO_2 AS ARB
                ON ARB.COD_SECT_LARGO=WEE.CODSECTORISTA
                AND ARB.FECHA=(SELECT MAX(FECHA) FROM ARBOL_DIARIO_2)
                WHERE ARB.BANCA IS NOT NULL AND ARB.BANCA<>'G0') A1"
            ), function($join){
                $join->on('WE.RUC', '=', 'A1.NUM_DOC');
            })
        ->leftJoin(
            DB::Raw("(SELECT DISTINCT WEE.NUM_DOC
                FROM WEBVPC_EMPRESAS_ECOSISTEMA AS WEE
                LEFT JOIN ARBOL_DIARIO_2 AS ARB
                ON ARB.COD_SECT_LARGO=WEE.CODSECTORISTA
                AND ARB.FECHA=(SELECT MAX(FECHA) FROM ARBOL_DIARIO_2)
                WHERE ARB.BANCA IS NOT NULL AND ARB.BANCA<>'G0') A2"
            ), function($join){
                $join->on('WE.RUC_CP', '=', 'A2.NUM_DOC');
            });
            //lEFT JOIN PARA EL FLAG

        return $sql;
    }

    static function getNodoEcosistema2($ruc,$periodo){
        $sql=DB::table('WEBBE_LEAD')
        ->select('NUM_DOC','NOMBRE',DB::Raw("ISNULL(DEUDA_DIRECTA,'0') DEUDA_DIRECTA"),
            DB::Raw("ISNULL(COD_UNICO,'-') COD_UNICO"))
        ->where('PERIODO','=',$periodo)
        ->where('NUM_DOC','=',$ruc);

        dd($sql->get());
        return $sql;
    }


    static function getNodoEcosistema($ruc,$periodo){
            //Sorry :(
        $sql=DB::table(DB::Raw("(
            SELECT WE.*,ISNULL(WEE.CODUNICOCLI,'-') CODUNICOCLI,
            ISNULL(WEE.RESPONSABLE,'-') AS SECTORISTA,
            WEE.NOMBRE,
            CASE WHEN WEE.BANCA='BPE' THEN 'Banca Pequeña Empresa'
            WHEN  WEE.BANCA like 'BE%' THEN 'Banca Empresa'
            WHEN WEE.BANCA='BPE' THEN 'Banca Corporativa'
            ELSE '-' END AS BANCA,
            ISNULL(CONVERT(VARCHAR,CAST(WEE.FACTURACION/1000 AS INT),1),'-') FACTURACION,
            CASE WHEN CA.COD_UNICO IS NULL THEN 'NO' ELSE 'SI' END AS CLIENTE_ACTIVO_IBK,
            CASE WHEN WLE.ETAPA IS NULL AND LEL.NUM_DOC IS NULL THEN 'NO ASIGNADO'
            WHEN WLE.ETAPA IS NULL AND LEL.NUM_DOC IS NOT NULL THEN 'ELIMINADO'
            ELSE WLE.ETAPA END AS ETAPA ,ISNULL(LEL.MOTIVO,'') AS MOTIVO,ISNULL(LEL.DETALLE,'') AS DETALLE,
            CASE WHEN WLE.ETAPA IS NULL AND LEL.NUM_DOC IS NULL THEN NULL
            WHEN WLE.ETAPA IS NULL AND LEL.NUM_DOC IS NOT NULL THEN CONVERT(DATE,LEL.FECHA_ELIMINACION)
            ELSE CONVERT(DATE,WLE.FECHA_ETAPA) END AS FECHA_ETAPA,
            CASE WHEN WEE.FLG_CUENTA_IBK=0 THEN 'NO' ELSE 'SI' END TIENE_CUENTA_IBK,
            CONVERT(VARCHAR,CAST(WEE.DEUDA_IBK/1000 AS INT),1) DEUDA_IBK,
            CONVERT(VARCHAR,CAST(WEE.DEUDA_SF/1000 AS INT),1) AS COL_DIRECTAS,
            CONVERT(DECIMAL(15,2),(ISNULL(WEE.DEUDA_IBK,0)/(CASE WHEN DEUDA_SF=0 OR DEUDA_SF IS NULL THEN 1.0 ELSE DEUDA_SF END))*100,2) AS SOW_IBK,
            ISNULL(WEE.BANCO_PRINCIPAL,'-') AS BANCO_PRINCIPAL,
            ISNULL(WEE.CLASIFRIESGOSBS,'-') AS CALIFICACION_SBS,
            ISNULL(WEE.FEVE_ACTUAL,'-') AS FEVE,
            ISNULL(WEE.NIVEL_TIE_USUARIO,'-') AS NIVEL_TIE,
            ISNULL(WEE.FILTRO_DURO,'-') AS CAIDA_CAMPANHAS,
            ISNULL(CONVERT(VARCHAR,CAST(NULL AS INT),1),'-') PAGO_PROVEEDORES,
            ISNULL(WU.NOMBRE,'-') AS RESPONSABLE_GESTION,
            CASE WHEN WLCE.NUM_DOC IS NULL THEN ISNULL(WDE.MENSAJE_MACRO,'-')
            ELSE ISNULL(WU2.NOMBRE,'-') END  AS RESPONSABLE_ECOSISTEMA,
            CASE WHEN WLCE.NUM_DOC IS NULL THEN CONVERT(DATE,WDE.FECHA_ESTADO)
            ELSE CONVERT(DATE,WLCE.FECHA_CARGA) END  AS FECHA_ECOSISTEMA
            FROM (
                SELECT NUM_DOC,CONVERT(VARCHAR,CAST(SUM(CASH_OUT)/1000 AS INT),1) AS CASH_OUT,
                CONVERT(VARCHAR,CAST(SUM(CASH_IN)/1000 AS INT),1) AS CASH_IN FROM (
                    SELECT
                    RUC AS NUM_DOC,
                    SUM(CASE WHEN TIPO='P' THEN VOLUMEN_COMPRA_VENTA_SOL ELSE 0 END) CASH_OUT,
                    SUM(CASE WHEN TIPO='C' THEN VOLUMEN_COMPRA_VENTA_SOL ELSE 0 END) CASH_IN
                    FROM WEBVPC_ECOSISTEMA
                    GROUP BY RUC,CU
                    UNION ALL
                    SELECT
                    RUC_CP,
                    SUM(CASE WHEN TIPO='C' THEN VOLUMEN_COMPRA_VENTA_SOL ELSE 0 END) CASH_OUT,
                    SUM(CASE WHEN TIPO='P' THEN VOLUMEN_COMPRA_VENTA_SOL ELSE 0 END) CASH_IN
                    FROM WEBVPC_ECOSISTEMA
                    GROUP BY RUC_CP,CU_CP
                ) B
                GROUP BY NUM_DOC
            ) AS WE
            LEFT JOIN WEBVPC_EMPRESAS_ECOSISTEMA AS WEE ON WEE.NUM_DOC=WE.NUM_DOC
            LEFT JOIN CA_CARTERA_ACTIVA AS CA ON
            CA.COD_UNICO=WEE.CODUNICOCLI AND CA.PERIODO=(SELECT MAX(PERIODO) FROM CA_CARTERA_ACTIVA)
            LEFT JOIN (
                SELECT NUM_DOC,REGISTRO_EN,NOMBRE AS ETAPA,FECHA_REGISTRO AS FECHA_ETAPA FROM WEBBE_LEAD_ETAPA A
                LEFT JOIN WEBBE_ETAPA B ON A.ID_ETAPA=B.ID_ETAPA
                WHERE B.TIPO='PROSPECTOS' AND A.FLG_ULTIMO=1
                AND A.PERIODO=".$periodo."
            ) WLE ON WLE.NUM_DOC=WE.NUM_DOC
            LEFT JOIN (
                SELECT NUM_DOC,REGISTRO_EN,UPPER(DESC_MOTIVO) AS MOTIVO, UPPER(DESC_DETALLE) AS DETALLE,FECHA_ELIMINACION,
                ROW_NUMBER() OVER(PARTITION BY NUM_DOC ORDER BY FECHA_ELIMINACION DESC) RNK
                FROM WEBBE_LEAD_ELIMINACION A
                LEFT JOIN WEBBE_CAT_MOTIVO_ELIMINAR B ON B.ID_MOTIVO=A.MOTIVO
                LEFT JOIN WEBBE_CAT_DETALLE_ELIMINAR C ON A.MOTIVO=C.ID_MOTIVO AND A.DETALLE=C.ID_DETALLE
            ) LEL ON LEL.RNK=1 AND LEL.NUM_DOC=WE.NUM_DOC
            LEFT JOIN WEBVPC_USUARIO WU ON WU.REGISTRO=(
                CASE WHEN WLE.ETAPA IS NULL AND LEL.NUM_DOC IS NULL THEN ''
                WHEN WLE.ETAPA IS NULL AND LEL.NUM_DOC IS NOT NULL THEN LEL.REGISTRO_EN
                ELSE WLE.REGISTRO_EN END)
                LEFT JOIN WEBVPC_LEAD_CAMP_EST WLCE ON WLCE.PERIODO=".$periodo."
                AND WLCE.NUM_DOC=WE.NUM_DOC
                AND WLCE.FLG_HABILITADO=1
                AND WLCE.ID_CAMP_EST=81
                LEFT JOIN WEBVPC_USUARIO WU2 ON WU2.REGISTRO=WLCE.REGISTRO_RESPONSABLE
                LEFT JOIN WEBVPC_DERIVACION_ECOSISTEMA WDE ON WDE.NUM_DOC=WE.NUM_DOC AND WDE.FLG_ULTIMO=1) INFO_NODO"))
->select()
->where('INFO_NODO.NUM_DOC','=',$ruc);

return $sql;
}

static function buscarRucEcosistema($cuBuscar,$razonBuscar){

    if($cuBuscar){
        $sql=DB::table('WEBVPC_EMPRESAS_ECOSISTEMA AS WEE')
        ->select()
        ->where('WEE.CODUNICOCLI','like','%'.$cuBuscar.'%');
    }
    else{
        /*Búsqueda más rápida*/
        $sql=DB::table(DB::Raw("(
            SELECT DISTINCT * FROM (
                SELECT
                DISTINCT NOMBRE AS NOMBRE,
                RUC AS NUM_DOC
                FROM WEBVPC_ECOSISTEMA
                UNION ALL
                SELECT
                DISTINCT NOMBRE_CP AS NOMBRE,
                RUC_CP AS NUM_DOC
                FROM WEBVPC_ECOSISTEMA
            ) A ) B"))
        ->select()
        ->where('B.NOMBRE','=',$razonBuscar);
    }
            //dd($sql->get());

    return $sql;
}

static function obtenerEmpresas(){
    $sql=DB::table(DB::Raw("(
        SELECT DISTINCT NOMBRE FROM (
            SELECT
            DISTINCT NOMBRE AS NOMBRE
            FROM WEBVPC_ECOSISTEMA
            UNION ALL
            SELECT
            DISTINCT NOMBRE_CP AS NOMBRE
            FROM WEBVPC_ECOSISTEMA
        ) A ) B"))
    ->select()->whereNotNull('NOMBRE')->get();

    $empresas=[];

    foreach($sql as $empresa){
        $empresas[]=$empresa->NOMBRE;
    }
    return $empresas;
}

static function seguimientoIngresado($filtros=[]){

    $sql=DB::table(DB::Raw("(
        SELECT
        WU.REGISTRO,WU.NOMBRE,WU.BANCA,WU.ID_ZONA,ISNULL(WU.ID_CENTRO,WU.ID_ZONA) AS ID_JEFATURA,WZ.ZONAL,ISNULL(WJ.JEFATURA,WZ.ZONAL) AS JEFATURA,
        COUNT(DISTINCT CASE WHEN CONVERT(DATE,WE.FECHA_INICIAL)<='".$filtros['fechaCorte']."'  THEN  WE.RUC ELSE NULL END) AS NUM_EMPRESAS_CORTE,
        COUNT(DISTINCT RUC) AS NUM_EMPRESAS,COUNT (DISTINCT RUC_CP) AS TOTAL,
        SUM(CASE WHEN WDE.CLASE='MEL' THEN 1 ELSE 0 END) AS CUENTA_MEL,
        SUM(CASE WHEN WDE.CLASE='GEL' THEN 1 ELSE 0 END) AS CUENTA_GEL,
        SUM(CASE WHEN WDE.CLASE='BEP' THEN 1 ELSE 0 END) AS CUENTA_BEP,
        SUM(CASE WHEN WDE.CLASE='BC' THEN 1 ELSE 0 END) AS CUENTA_BC,
        SUM(CASE WHEN WDE.CLASE='BPE' THEN 1 ELSE 0 END) AS CUENTA_BPE,
        SUM(CASE WHEN WDE.CLASE='MESA' THEN 1 ELSE 0 END) AS CUENTA_MESA,
        0 AS VOLUMEN_ECOSISTEMA,
        0 AS VOLUMEN_REAL,
        0 AS VOLUMEN_META,
        0 AS PROV_TOTAL,
        0 AS PROV_REAL,
        0 AS PROV_META
        FROM WEBVPC_USUARIO WU
        LEFT JOIN WEBBE_ZONAL WZ ON WZ.ID_ZONAL=WU.ID_ZONA
        LEFT JOIN WEBBE_JEFATURA WJ ON WJ.ID_JEFATURA=WU.ID_CENTRO
        LEFT JOIN WEBVPC_ECOSISTEMA WE ON WE.REGISTRO=WU.REGISTRO
        AND WE.TIPO='P'
        AND CONVERT(DATE,WE.FECHA_INICIAL)<CONVERT(DATE,'".$filtros['fechaAvance']."') --FECHA VARIABLE
        LEFT JOIN (
            SELECT *,
            CASE
            WHEN MENSAJE_MACRO='DERIVADO A EN LIMA' AND SEGMENTO='S1' THEN 'GEL'
            WHEN MENSAJE_MACRO='DERIVADO A EN LIMA' AND SEGMENTO='S2' THEN 'MEL'
            WHEN MENSAJE_MACRO='DERIVADO A EN PROVINCIA' THEN 'BEP'
            WHEN MENSAJE_MACRO='PENDIENTE BC' THEN 'BC'
            WHEN MENSAJE_MACRO='PENDIENTE BPE' THEN 'BPE'
            WHEN MENSAJE_MACRO='PENDIENTE MESA DE AFILIACIÓN' THEN 'MESA'
            WHEN MENSAJE_MACRO='PENDIENTE EJECUTIVO DE CASH' THEN 'CASH'
            WHEN MENSAJE_MACRO='EN CAMPAÑA BE' THEN 'CAMPAÑA'
            ELSE 'MENSAJE' END AS CLASE
            FROM WEBVPC_DERIVACION_ECOSISTEMA
        ) WDE ON WDE.NUM_DOC=WE.RUC_CP
        WHERE WU.ROL IN (20,21) AND WU.NOMBRE NOT LIKE '%DUMMIE%'
        GROUP BY WU.REGISTRO,WU.NOMBRE,WU.BANCA,WU.ID_ZONA,WU.ID_CENTRO,WZ.ZONAL,WJ.JEFATURA) B"))
    ->select();

    if (isset($filtros['banca'])){
        $sql = $sql->where('B.BANCA','=',$filtros['banca']);
    }

    if (isset($filtros['zonal'])){
        $sql = $sql->where('B.ID_ZONA','=',$filtros['zonal']);
    }

    if (isset($filtros['jefatura'])){
        $sql = $sql->where('B.ID_JEFATURA','=',$filtros['jefatura']);
    }

    if (isset($filtros['ejecutivo'])){
        $sql = $sql->where('B.REGISTRO','=',$filtros['ejecutivo']);
    }



    return $sql;
}

static function seguimientoIngresadoTotales($filtros=[]){

    $sql=DB::table(DB::Raw("(
       SELECT
       DISTINCT WU.REGISTRO,WU.NOMBRE,WU.BANCA,WU.ID_ZONA,WU.ID_CENTRO AS ID_JEFATURA,WE.RUC,WE.RUC_CP,WE.FECHA_INICIAL,WDE.CLASE
       FROM WEBVPC_USUARIO WU
       LEFT JOIN WEBVPC_ECOSISTEMA WE ON WE.REGISTRO=WU.REGISTRO

       AND WE.TIPO='P'
       AND CONVERT(DATE,WE.FECHA_INICIAL)<=CONVERT(DATE,'".$filtros['fechaAvance']."') --FECHA VARIABLE
       LEFT JOIN (
        SELECT *,
        CASE
        WHEN MENSAJE_MACRO='DERIVADO A EN LIMA' AND SEGMENTO='S1' THEN 'GEL'
        WHEN MENSAJE_MACRO='DERIVADO A EN LIMA' AND SEGMENTO='S2' THEN 'MEL'
        WHEN MENSAJE_MACRO='DERIVADO A EN PROVINCIA' THEN 'BEP'
        WHEN MENSAJE_MACRO='PENDIENTE BC' THEN 'BC'
        WHEN MENSAJE_MACRO='PENDIENTE BPE' THEN 'BPE'
        WHEN MENSAJE_MACRO='PENDIENTE MESA DE AFILIACIÓN' THEN 'MESA'
        WHEN MENSAJE_MACRO='PENDIENTE EJECUTIVO DE CASH' THEN 'CASH'
        WHEN MENSAJE_MACRO='EN CAMPAÑA BE' THEN 'CAMPAÑA'
        ELSE 'MENSAJE' END AS CLASE
        FROM WEBVPC_DERIVACION_ECOSISTEMA
    ) WDE ON WDE.NUM_DOC=WE.RUC_CP
    WHERE WU.ROL IN (20,21) AND WU.NOMBRE NOT LIKE '%DUMMIE%'
) A"))
    ->select();

    if (isset($filtros['banca'])){
        $sql = $sql->where('A.BANCA','=',$filtros['banca']);
    }

    if (isset($filtros['zonal'])){
        $sql = $sql->where('A.ID_ZONA','=',$filtros['zonal']);
    }

    if (isset($filtros['jefatura'])){
        $sql = $sql->where('A.ID_JEFATURA','=',$filtros['jefatura']);
    }

    if (isset($filtros['ejecutivo'])){
        $sql = $sql->where('A.REGISTRO','=',$filtros['ejecutivo']);
    }

    $sql=$sql->select(
        DB::Raw("COUNT(DISTINCT CASE WHEN CONVERT(DATE,A.FECHA_INICIAL)<='".$filtros['fechaCorte']."' THEN  A.RUC ELSE NULL END) AS NUM_EMPRESAS_CORTE"),
        DB::Raw("COUNT(DISTINCT A.RUC) AS NUM_EMPRESAS"),
        DB::Raw("COUNT (DISTINCT RUC_CP) AS TOTAL"),
        DB::Raw("COUNT (DISTINCT CASE WHEN A.CLASE='MEL' THEN RUC_CP ELSE NULL END) AS CUENTA_MEL"),
        DB::Raw("COUNT (DISTINCT CASE WHEN A.CLASE='GEL' THEN RUC_CP ELSE NULL END) AS CUENTA_GEL"),
        DB::Raw("COUNT (DISTINCT CASE WHEN A.CLASE='BEP' THEN RUC_CP ELSE NULL END) AS CUENTA_BEP"),
        DB::Raw("COUNT (DISTINCT CASE WHEN A.CLASE='BC' THEN RUC_CP ELSE NULL END) AS CUENTA_BC"),
        DB::Raw("COUNT (DISTINCT CASE WHEN A.CLASE='BPE' THEN RUC_CP ELSE NULL END) AS CUENTA_BPE"),
        DB::Raw("COUNT (DISTINCT CASE WHEN A.CLASE='MESA' THEN RUC_CP ELSE NULL END) AS CUENTA_MESA"),
        DB::Raw("0 AS VOLUMEN_ECOSISTEMA"),
        DB::Raw("0 AS VOLUMEN_REAL"),
        DB::Raw("0 AS VOLUMEN_META"),
        DB::Raw("0 AS PROV_TOTAL"),
        DB::Raw("0 AS PROV_REAL"),
        DB::Raw("0 AS PROV_META")
    );


    return $sql;
}

static function getStatusProveedoresIngresados($periodo,$filtros=[]){

    $sql=DB::table( DB::Raw("(
            SELECT
            DISTINCT FECHA_INICIAL,NOMBRE_CP,VOLUMEN_COMPRA_VENTA_SOL,NOMBRE,RUC_CP,RUC,REGISTRO,TIPO FROM WEBVPC_ECOSISTEMA
        ) WE"))
    ->select(
        DB::Raw('CONVERT(DATE,WE.FECHA_INICIAL) AS FECHA_INGRESO'),
        DB::Raw("ISNULL(WET.NOMBRE,'-') AS ESTADO"),
        'WDE.BANCA AS BANCA',
        DB::Raw("ISNULL(CASE WHEN MENSAJE_MACRO='DERIVADO A EN LIMA' THEN DBO.INITCAP(WZ.ZONAL) ELSE NULL END,'-') AS ZONAL"),
        DB::Raw("ISNULL(WDE.ASIGNADO_A,'-') AS ASIGNADO_A"),
        'WE.NOMBRE_CP AS EMPRESA',
        DB::Raw('CONVERT(varchar, CAST(WE.VOLUMEN_COMPRA_VENTA_SOL  AS money), 1) AS POTENCIAL'),
        DB::Raw('DBO.INITCAP(WUR.NOMBRE) AS EJECUTIVO_REFIERE'),
        'WE.NOMBRE AS EMPRESA_REFIERE')
    ->leftJoin(
        DB::Raw("(
            SELECT
            A.*,
            CASE
            WHEN MENSAJE_MACRO='PENDIENTE MESA DE AFILIACIÓN' THEN 'Mesa de Afiliación'
            WHEN MENSAJE_MACRO='DERIVADO A EN PROVINCIA' THEN 'BE Provincia'
            WHEN MENSAJE_MACRO='Pendiente BPE' THEN 'Pendiente BPE'
            WHEN MENSAJE_MACRO='Pendiente BC' THEN 'Pendiente BC'
            ELSE  DBO.INITCAP(WUA.NOMBRE) END ASIGNADO_A,WUA.ID_ZONA,WUA.ID_CENTRO AS ID_JEFATURA,B.FECHA_REGISTRO
            FROM WEBVPC_DERIVACION_ECOSISTEMA A
            LEFT JOIN WEBBE_LEAD_SECTORISTA B ON A.NUM_DOC=B.NUM_DOC AND B.PERIODO='".$periodo."'
            AND A.MENSAJE_MACRO IN ('DERIVADO A EN LIMA','EN CAMPAÑA BE')
            LEFT JOIN WEBVPC_USUARIO WUA ON WUA.REGISTRO=B.REGISTRO_EN
        ) WDE"
    ), function($join){
            $join->on('WDE.NUM_DOC','=','WE.RUC_CP');
        })
    ->leftJoin('WEBVPC_USUARIO AS WUR', function($join){
        $join->on('WUR.REGISTRO','=','WE.REGISTRO');
    })
    /*->leftJoin('WEBVPC_USUARIO AS WUA', function($join){
        $join->on('WUA.REGISTRO','=','WDE.ASIGNADO_A');
    })*/
    ->leftJoin('WEBBE_LEAD_ETAPA AS WLE', function($join){
        $join->on(DB::raw('CONVERT(DATE,WLE.FECHA_REGISTRO)'),'>=',DB::raw('CONVERT(DATE,WDE.FECHA_REGISTRO)'));
        $join->on('WLE.FLG_ULTIMO','=', DB::raw('1'));
        $join->on('WLE.NUM_DOC','=','WE.RUC_CP');
    })
    ->leftJoin('WEBBE_ETAPA AS WET', function($join){
        $join->on('WET.ID_ETAPA','=','WLE.ID_ETAPA');
        $join->on('WET.TIPO','=',DB::Raw("'PROSPECTOS'"));
    })
    ->leftJoin('WEBBE_ZONAL AS WZ', function($join){
        $join->on('WZ.ID_ZONAL','=','WDE.ID_ZONA');
    })
    ->where('WE.TIPO','=','P')
    ->orderBy(DB::Raw('ISNULL(WET.ORDEN,10)'),'ASC')
    ->orderBy('WE.FECHA_INICIAL','ASC');

    if (isset($filtros['banca'])){
        $sql = $sql->where('WUR.BANCA','=',$filtros['banca']);
    }

    if (isset($filtros['zonal'])){
        $sql = $sql->where('WUR.ID_ZONA','=',$filtros['zonal']);
    }

    if (isset($filtros['jefatura'])){
        $sql = $sql->where('WUR.ID_CENTRO','=',$filtros['jefatura']);
    }

    if (isset($filtros['ejecutivo'])){
        $sql = $sql->where('WUR.REGISTRO','=',$filtros['ejecutivo']);
    }


    return $sql;
}


static function graficarIngresados($periodo,$filtros=[]){

    $fecha =$filtros['fechaCorte'];
    $arreglo=[];
    do {


        $sql1=DB::table('WEBVPC_ECOSISTEMA AS WE')
        ->select(DB::Raw('COUNT (DISTINCT RUC_CP) AS NUM_PROVEEDORES'))
            ->leftJoin('WEBVPC_USUARIO AS WU', function($join){
        $join->on('WU.REGISTRO','=','WE.REGISTRO');
        })
        ->where('WE.FECHA_INICIAL','<=',$fecha->format('Y-m-d'))
        ->whereIn('ROL',[20,21])
        ->where('TIPO','=','P');

        $sql2=DB::table('WEBVPC_ECOSISTEMA AS WE')
        ->select(DB::Raw('COUNT (DISTINCT RUC) AS NUM_EMPRESAS'))
            ->leftJoin('WEBVPC_USUARIO AS WU', function($join){
        $join->on('WU.REGISTRO','=','WE.REGISTRO');
        })
        ->where('WE.FECHA_INICIAL','<=',$fecha->format('Y-m-d'))
        ->whereIn('ROL',[20,21])
        ->where('TIPO','=','P');


         if (isset($filtros['banca'])){
            $sql1 = $sql1->where('WU.BANCA','=',$filtros['banca']);
            $sql2 = $sql2->where('WU.BANCA','=',$filtros['banca']);
        }

        if (isset($filtros['zonal'])){
            $sql1 = $sql1->where('WU.ID_ZONA','=',$filtros['zonal']);
            $sql2 = $sql2->where('WU.ID_ZONA','=',$filtros['zonal']);
        }

        if (isset($filtros['jefatura'])){
            $sql1 = $sql1->where('WU.ID_CENTRO','=',$filtros['jefatura']);
            $sql2 = $sql2->where('WU.ID_CENTRO','=',$filtros['jefatura']);
        }

        if (isset($filtros['ejecutivo'])){
            $sql1 = $sql1->where('WU.REGISTRO','=',$filtros['ejecutivo']);
            $sql2 = $sql2->where('WU.REGISTRO','=',$filtros['ejecutivo']);
        }

        if ($fecha->dayOfWeek>=1 and $fecha->dayOfWeek<=5  )  {
            $arreglo[]=[
                'FECHA'=>$fecha->format('d-m'),
                'NUM_PROVEEDORES'=>$sql1->first()->NUM_PROVEEDORES,
                'NUM_EMPRESAS'=>$sql2->first()->NUM_EMPRESAS,
            ];
        }

        $fecha=$fecha->addDays(1);


    } while ( $fecha->format('Y-m-d')<= $filtros['fechaAvance']->format('Y-m-d'));

    return $arreglo;

}

static function seguimientoRecibido($filtros=[]){

    $sql=DB::table(DB::Raw("(
        SELECT
        WU.REGISTRO,WU.NOMBRE,WU.BANCA,WU.ID_ZONA,WU.ID_CENTRO AS ID_JEFATURA,
        COUNT(DISTINCT CASE WHEN CONVERT(DATE,WE.FECHA_INICIAL)<='".$filtros['fechaCorte']."'  THEN  WE.RUC ELSE NULL END) AS NUM_EMPRESAS_CORTE,
        COUNT(DISTINCT RUC) AS NUM_EMPRESAS,COUNT (DISTINCT RUC_CP) AS TOTAL,
        SUM(CASE WHEN WDE.CLASE='MEL' THEN 1 ELSE 0 END) AS CUENTA_MEL,
        SUM(CASE WHEN WDE.CLASE='GEL' THEN 1 ELSE 0 END) AS CUENTA_GEL,
        SUM(CASE WHEN WDE.CLASE='BEP' THEN 1 ELSE 0 END) AS CUENTA_BEP,
        SUM(CASE WHEN WDE.CLASE='BC' THEN 1 ELSE 0 END) AS CUENTA_BC,
        SUM(CASE WHEN WDE.CLASE='BPE' THEN 1 ELSE 0 END) AS CUENTA_BPE,
        SUM(CASE WHEN WDE.CLASE='MESA' THEN 1 ELSE 0 END) AS CUENTA_MESA
        FROM WEBVPC_USUARIO WU
        LEFT JOIN WEBVPC_ECOSISTEMA WE ON WE.REGISTRO=WU.REGISTRO
        AND WE.TIPO='P'
        AND CONVERT(DATE,WE.FECHA_INICIAL)<CONVERT(DATE,'".$filtros['fechaAvance']."') --FECHA VARIABLE
        LEFT JOIN (
            SELECT *,
            CASE
            WHEN MENSAJE_MACRO='DERIVADO A EN LIMA' AND SEGMENTO='S1' THEN 'GEL'
            WHEN MENSAJE_MACRO='DERIVADO A EN LIMA' AND SEGMENTO='S2' THEN 'MEL'
            WHEN MENSAJE_MACRO='DERIVADO A EN PROVINCIA' THEN 'BEP'
            WHEN MENSAJE_MACRO='PENDIENTE BC' THEN 'BC'
            WHEN MENSAJE_MACRO='PENDIENTE BPE' THEN 'BPE'
            WHEN MENSAJE_MACRO='PENDIENTE MESA DE AFILIACIÓN' THEN 'MESA'
            WHEN MENSAJE_MACRO='PENDIENTE EJECUTIVO DE CASH' THEN 'CASH'
            WHEN MENSAJE_MACRO='EN CAMPAÑA BE' THEN 'CAMPAÑA'
            ELSE 'MENSAJE' END AS CLASE
            FROM WEBVPC_DERIVACION_ECOSISTEMA
        ) WDE ON WDE.NUM_DOC=WE.RUC_CP
        WHERE WU.ROL IN (20,21) AND WE.REGISTRO IS NOT NULL
        GROUP BY WU.REGISTRO,WU.NOMBRE,WU.BANCA,WU.ID_ZONA,WU.ID_CENTRO) B"))
    ->select();

    if (isset($filtros['banca'])){
        $sql = $sql->where('B.BANCA','=',$filtros['banca']);
    }

    if (isset($filtros['zonal'])){
        $sql = $sql->where('B.ID_ZONA','=',$filtros['zonal']);
    }

    if (isset($filtros['jefatura'])){
        $sql = $sql->where('B.ID_JEFATURA','=',$filtros['jefatura']);
    }

    if (isset($filtros['ejecutivo'])){
        $sql = $sql->where('B.REGISTRO','=',$filtros['ejecutivo']);
    }



    return $sql;
}

}
