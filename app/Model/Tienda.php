<?php

namespace App\Model;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class Tienda extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'WEBVPC_TIENDA';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = null;
	public $incrementing = false;
	
    protected $guarded = [
		'ID_TIENDA',
        'TIENDA',
        'ID_ZONA',
        'ZONA',
        'ID_CENTRO',
        'ID_CENTRO',
    ];

    function getZonales() {
        $sql = DB::table('WEBVPC_TIENDA as WT')
            ->select('WT.ID_ZONA','WT.ZONA')
            ->orderBy('WT.ZONA')
            ->distinct();
        return $sql;
    }


    function getEjecutivos($tienda=null,$centro = null, $zona = null){
        //dd($zona);
        $sql = DB::table('WEBVPC_USUARIO as WU')
            ->select('WU.REGISTRO','WU.NOMBRE')
            ->where('WU.ROL','=',1)
            ->orderBy('WU.NOMBRE')
            ->distinct();

        if ($centro)
            $sql = $sql->where('WU.ID_CENTRO','=',$centro);
        
        if ($zona)
            $sql = $sql->where('WU.ID_ZONA','=',$zona);
        
        if ($tienda)
            $sql = $sql->where('WU.ID_TIENDA','=',$tienda);
        //dd($sql->get());
        return $sql;
    }
    function getTiendas($centro = null, $zona = null) {
        $sql = DB::table('WEBVPC_TIENDA as WT')
            ->select('WT.ID_TIENDA','WT.TIENDA')
            ->orderBy('WT.TIENDA')
            ->distinct();
        if ($centro){
            $sql = $sql->where('WT.ID_CENTRO','=',$centro);
        }
        if ($zona){
            $sql = $sql->where('WT.ID_ZONA','=',$zona);
        }
        return $sql;
    }

    function getCentros($zona = null) {
        $sql = DB::table('WEBVPC_TIENDA as WT')
            ->select('WT.ID_CENTRO','WT.CENTRO')
            ->orderBy('WT.CENTRO')
            ->distinct();
        if ($zona){
            $sql = $sql->where('WT.ID_ZONA','=',$zona);
        }
        return $sql;
    }

}
