<?php
namespace App\Model;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;
use \App\Entity\Canal as Canal;

class Lead extends Model

{
	protected $table = 'WEBVPC_LEAD';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey =['PERIODO',
                            'TIPO_DOCUMENTO',
                            'NUM_DOC',
                            ];


        /**
     * The name of the "created at" column.
     *
     * @var string
     */
     public $timestamps = false;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'NOMBRE_CLIENTE',
        'REPRESENTANTE_LEGAL',
        'COD_UNICO',
        'TIPO_PERSONA',
        'DEPARTAMENTO',
        'PROVINCIA',
        'DISTRITO',
        'DIRECCION',
        'TELEFONO1',
        'TELEFONO2',
        'TELEFONO3',
        'TELEFONO4',
        'SCORE_BURO',
        'DEUDA_SSFF',
        'BANCO_PRINCIPAL_SSFF',
        'DEUDA_6M_SSFF',
        'VARIACION_DEUDA_6M_SSFF',
        'SEGMENTO',
        'ACTIVIDAD',
        'GIRO',
        'TIENDA',
        'ZONAL',
        'FLG_ES_CLIENTE'
        ];

    function getDatosBasicosCliente($documento){
        $sql = DB::table('WEBVPC_LEAD as WL')
        ->select('WL.NUM_DOC as NUM_RUC','WL.NOMBRE_CLIENTE as NOMBRE','WL.REPRESENTANTE_LEGAL as RRLL_NOMBRES'
        ,'WL.RRLL_DOCUMENTO_1 as RRLL_DOCUMENTO','WL.RRLL_APELLIDOS','WL.RRLL_ESTADO_CIVIL','WL.RRLL_DOCUMENTO_2',
        'WL.RRLL_NOMBRES_2','WL.RRLL_APELLIDOS_2','WL.RRLL_ESTADO_CIVIL_2','MC1.DESCRIPCION AS CARGO1','MC2.DESCRIPCION AS CARGO2')
				->leftJoin(DB::Raw("(SELECT * FROM MAE_CATALOGO WHERE CATALOGO='CARGOS-COFIDE') as MC1"), function($join){
						$join->on('WL.RRLL_CARGO_1', '=', 'MC1.CODIGO');
				})
				->leftJoin(DB::Raw("(SELECT * FROM MAE_CATALOGO WHERE CATALOGO='CARGOS-COFIDE') as MC2"), function($join){
						$join->on('WL.RRLL_CARGO_2', '=', 'MC2.CODIGO');
				})
        ->where('WL.NUM_DOC',$documento)
        ->orderBy('WL.PERIODO','desc');

        return $sql;
    }

    public function getDireccion($documento=null){
        $sql=DB::select("exec USP_LEADS_DIRECCION $documento ");
       return $sql;
    }

    /**
     * Devuelve todos los datos del lead
     *
     * @param string $periodo Periodo en que fue cargado el Lead.
     * @param string $lead Numero de documento del lead.
     * @param array $filtros Filtros de busqueda
     * @param int $cliente Determina si tambien se incluye cliente
     * @return table
     */
    function get($periodo,$lead,$filtros = [] ,$cliente = 0){
        $sql = DB::table('WEBVPC_LEAD as WL')
                ->select('WL.PERIODO','WL.COD_UNICO','WL.NUM_DOC','WL.NOMBRE_CLIENTE',
                    'WL.TIPO_DOCUMENTO','WL.TIPO_PERSONA','WL.DISTRITO','WL.DIRECCION','WL.SCORE_BURO',
                    'WL.DEUDA_SSFF_MONEDA','WL.DEUDA_SSFF','WL.BANCO_PRINCIPAL_SSFF','WL.VARIACION_DEUDA_6M_SSFF','WL.SEGMENTO'
                    ,'WL.TELEFONO1','WL.TELEFONO2','WL.TELEFONO3','WL.TELEFONO4','WL.ACTIVIDAD','WL.GIRO',
                    'WL.REPRESENTANTE_LEGAL','WL.DEPARTAMENTO','WL.PROVINCIA','WL.PROPENSION','WL.MARCA_ESTRELLA','WL.FLG_ES_CLIENTE','WL.TIPO_CUENTA','WL.NUM_CUENTA');

        if($lead && is_array($lead)){
            $sql = $sql->whereIn('WL.NUM_DOC',$lead);
        }

        if ($lead && !is_array($lead)){
            $sql = $sql->where('WL.NUM_DOC','=',$lead);
        }

        if (isset($filtros['distrito'])){
            $sql = $sql->where('WL.DISTRITO','=',$filtros['distrito']);
        }

        if (isset($filtros['lead'])){
            $sql = $sql->where('WL.NOMBRE_CLIENTE','like','%'.$filtros['lead'].'%');
        }

        if (isset($filtros['documento'])){
            $sql = $sql->where('WL.NUM_DOC','=',$filtros['documento']);
        }

        if (isset($filtros['propension'])){

            switch ($filtros['propension']) {
                case 9:             //Muy Bajo
                     $sql = $sql->where('WL.PROPENSION','>=',0)
                                ->where('WL.PROPENSION','<',0.03);
                    break;
                case 10:             //Bajo
                    $sql = $sql->where('WL.PROPENSION','>=',0.03)
                                ->where('WL.PROPENSION','<',0.048);
                    break;
                case 11:             //Medio
                    $sql = $sql->where('WL.PROPENSION','>=',0.048)
                                ->where('WL.PROPENSION','<',0.07);
                    break;
                case 12:             //Alto
                    $sql = $sql->where('WL.PROPENSION','>=',0.07)
                                ->where('WL.PROPENSION','<',0.1);
                    break;
                case 13:             //Recomendable
                    $sql = $sql->where('WL.PROPENSION','>=',0.1)
                                ->where('WL.PROPENSION','<=',1);
                    break;
                default:
                    $sql = $sql->where('WL.PROPENSION',$filtros['propension']);
            }
        }

        if ($cliente = 0){
            $sql = $sql->where('WL.FLG_ES_CLIENTE','=', $cliente);
        }

        return $sql;
    }

    /**
     * Devuelve todos los datos del lead y agrupa todas sus campañas, la última gestión de cada campaña y el canal de de despliegue. Se agrupan en un campo con el separador '|'
     *
     * @param string $periodo Periodo en que fue cargado el Lead
     * @param string $lead Numero de documento del lead.
     * @param array $canales Canales en que esta desplegado el Lead
     * @param array $filtros Filtros de busqueda
     * @return table
     */
    function getLeadsCampanhasGestion($periodo,$lead,$canales = [],$filtros = []){

        //Se requiere logica de group concat simulada para SQL Server

        // Paso 1. Definir los campos a agrupoar. en el Key va el alias y en el valor el campo de BD
        $campos = [
            'CAM_EST_ID' => 'CAST(WC.ID_CAMP_EST as VARCHAR(3))',
            'CAM_EST_NOMBRE' => 'WC.NOMBRE',
            'CAM_EST_ABREV' => 'WC.ABREVIATURA',
            'PRIORIDAD' => 'WLC.PRIORIDAD',
            'GESTION' => "ISNULL(WRG2.DESCRIPCION,'')",
            'CANALES' => "ISNULL(WLC.CANAL_ACTUAL,'')",
        ];

        //Paso 2. Agregamos cada campo a la consulta de agrupamiento
        $scripts = [];
        $where = '';
        foreach($campos as $key => $campo){

            //consulta agrupada - es necesario cruzar leadCampanha - CampanhaInstancia - Campanha - Gestion
            $join = DB::table('WEBVPC_LEAD_CAMP_EST as WLC')
            ->select(DB::raw("'|' + ".$campo))
            ->join('WEBVPC_CAMP_EST_INSTANCIA as WCI', function($join){
                    $join->on('WCI.PERIODO', '=', 'WLC.PERIODO');
                    $join->on('WCI.ID_CAMP_EST', '=', 'WLC.ID_CAMP_EST');
            })

            ->join('WEBVPC_CAMP_EST as WC', function($join){
                    $join->on('WC.ID_CAMP_EST','=','WCI.ID_CAMP_EST');
            })

            ->leftJoin('WEBVPC_GESTION_LEAD_CAMP_EST as WGL', function($join){
                    $join->on('WGL.NUM_DOC', '=', 'WLC.NUM_DOC');
                    $join->on('WGL.FLG_ULTIMO', '=', DB::raw('1'));
                    $join->on('WGL.ID_CAMP_EST', '=', 'WLC.ID_CAMP_EST');
                    $join->on('WGL.PERIODO', '=', 'WCI.PERIODO');
            })
            ->leftJoin('WEBVPC_RESULTADO_GESTION as WRG', function($join){
                    $join->on('WGL.ID_RESULTADO_GESTION', '=', 'WRG.ID_RESULTADO_GESTION');
            })
            ->leftJoin('WEBVPC_RESULTADO_GESTION_N2 as WRG2', function($join){
                    $join->on('WRG2.ID_RESULTADO_GESTION_N2', '=', 'WRG.ID_RESULTADO_GESTION_N2');
            })

            ->whereRaw('WL.NUM_DOC = WLC.NUM_DOC')
            ->whereRaw('WL.PERIODO = WLC.PERIODO')
            ->where('WCI.HABILITADO', '=', 1)
            ->where('WC.BANCA','BPE');

            if (isset($filtros['campanha'])){
                $join = $join->where('WC.ID_CAMP_EST','=',$filtros['campanha']);
            }

            //Si se han elegido canales
            if (count($canales) > 0){
                $join = $join->where(function($query) use ($canales){
                    $query->whereIn('WLC.CANAL_ACTUAL',$canales);
                });
            }

            $script = $join->toSql();

            foreach( $join->getBindings() as $binding ){
                $script = preg_replace("#\?#", is_numeric($binding) ? $binding : "'" . $binding . "'", $script, 1);
            }

            if ($key == 'CAM_EST_ID'){
                $where = "STUFF((". $script ." FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1, '')";
            }

            $scripts[] = "STUFF((". $script ." FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1, '') ".$key;

        }

        //Paso 3 agrego los campos agrupados (STUFF) a la consulta
        $sql = $this->get($periodo,$lead,$filtros)
                ->addSelect(
                            //DB::raw($scripts[0]),
                            DB::raw($scripts[1]),
                            DB::raw($scripts[2]),
                            //DB::raw($scripts[3]),
                            DB::raw($scripts[4]),
                            DB::raw($scripts[5]))
                ->whereNotNull(DB::raw($where))
                ;
        return $sql;
    }

    /**
     * Devuelve todos los datos del lead y agrupa todas sus campañas, gestiones y canales de un Ejecutivo de Negocio
     *
     * @param string $periodo Periodo en que fue cargado el Lead
     * @param string $ejecutivo Ejecutivo de Negocio
     * @param string $lead Numero de documento del lead.
     * @param array $canales Canales en que esta desplegado el Lead
     * @param array $filtros Filtros de busqueda
     * @param array $orden array con el campo a ordenar y como se ordena (asc|desc)
     * @return table
     */
    function getLeadEjecutivo($periodo,$ejecutivo = null, $lead = null,$canales = [],$filtros = null,$orden = null){
        $sql = $this->getLeadsCampanhasGestion($periodo,$lead,$canales,$filtros)
            ->addSelect('WU.NOMBRE as EN_NOMBRE','WU.REGISTRO as EN_REGISTRO','WLE.MARCA_ASISTENTE_COMERCIAL','WLE.ETIQUETA_EJECUTIVO')
            ->addSelect(DB::raw("CONVERT(VARCHAR(16),FECHA_CITA,120) FECHA_CITA"),'WCT.ID_CITA','WCT.PERSONA_CONTACTO as CITA_CONTACTO_PERSONA','WCT.TELEFONO_CONTACTO as CITA_CONTACTO_TELEFONO','WCT.DIRECCION_CONTACTO as CITA_CONTACTO_DIRECCION','WCT.REFERENCIA as CITA_CONTACTO_REFERENCIA','WCT.ESTADO as CITA_ESTADO','WCT.AUTORIZACION_DATOS','WCT.TIPO_HORARIO')
            ->addSelect('WTI.TIENDA','WTI.CENTRO','WTI.ZONA as ZONAL')
            ->addSelect('WL.EMAIL','WL.DIR_TIPO_VIA','WL.DIR_DESCRIPCION_VIA','WL.DIR_NRO','WL.DIR_LOTE'
            ,'WL.DIR_MANZANA','WL.DIR_INTERIOR','WL.DIR_URB_ZONA','WL.DIR_UBIGEO','WL.RRLL_DOCUMENTO_1'
            ,'WL.RRLL_APELLIDOS','WL.RRLL_ESTADO_CIVIL','WL.RRLL_DOCUMENTO_2','WL.RRLL_NOMBRES_2'
            ,'WL.RRLL_APELLIDOS_2','WL.RRLL_ESTADO_CIVIL_2','WL.NROEMPLEADOS','WL.TIPO_SOCIEDAD'
            ,'WL.DEUDA_ACTUAL','WL.CUOTA_ACTUAL','WL.RRLL_CARGO_1','WL.RRLL_CARGO_2'
            ,DB::Raw("(substring(DIR_UBIGEO,0,3)) as DIR_DEPARTAMENTO")
            ,DB::Raw("(substring(DIR_UBIGEO,3,2)) as DIR_PROVINCIA")
            ,DB::Raw("(substring(DIR_UBIGEO,5,2)) as DIR_DISTRITO")
            ,'FLG_REACTIVA','FLG_REPROGRAMACION','VENTAS_ANUALES','EXPORTACIONES_ANUALES')
            ->leftJoin(DB::Raw("(SELECT * FROM (SELECT *,
                ROW_NUMBER() OVER (PARTITION BY NUM_DOC ORDER BY FECHA_ASIGNACION DESC) ORDEN
                FROM WEBVPC_LEADS_EJECUTIVO
                WHERE FLG_ULTIMO = '1') A WHERE A.[ORDEN] = 1) as WLE"), function($join){
                    $join->on('WL.NUM_DOC', '=', 'WLE.NUM_DOC');
            })
            ->leftJoin('WEBVPC_USUARIO as WU','WU.REGISTRO', '=', 'WLE.REGISTRO_EN')
            ->leftJoin('WEBVPC_TIENDA as WTI',function($join){
                $join->on('WU.ID_TIENDA', '=', 'WTI.ID_TIENDA');
            })
            ->leftJoin('WEBVPC_CITAS as WCT', function($join){
                        $join->on('WCT.NUM_DOC', '=', 'WL.NUM_DOC');
                        $join->on(DB::raw('CONVERT(varchar(6),WCT.FECHA_CITA,112)'),'>=','WLE.PERIODO');
                    });

        if ($ejecutivo){
            $sql = $sql->where('WLE.REGISTRO_EN','=',$ejecutivo);
        }

        if (isset($filtros['marca'])){
            $sql = $sql->where('WLE.ETIQUETA_EJECUTIVO','=',$filtros['marca']);
        }

        // Si no se ha seleccionado un campo para ordenar, se da el ordenamiento por default:
        if (!$orden){
            $sql =$sql
                    // 1. Se ordena si la cita esta vencida
                    ->orderBy(DB::raw('CASE WHEN WCT.ESTADO IN (1,2,3) AND GETDATE() > WCT.FECHA_CITA THEN DATEDIFF(hour, WCT.FECHA_CITA,GETDATE()) ELSE 0 END'),'DESC')
                    // 2. Se ordena por la proxima cita agendada
                    ->orderBy(DB::raw('CASE WHEN WCT.FECHA_CITA IS NULL OR WCT.ESTADO NOT IN (1,2,3) THEN 99999 ELSE DATEDIFF(hour, GETDATE(),WCT.FECHA_CITA) END'),'ASC')
                    // 3. Se ordena por marca
                    ->orderBy('WL.MARCA_ESTRELLA','DESC')
                    // 3. Se ordena por propension
                    ->orderBy('WL.PROPENSION','DESC');
        }else{
            $sql = $sql->orderBy($this->getOrderColumn($orden['sort']),$orden['order']);
        }
        return $sql;


    }

    /**
     * Devuelve los datos del lead y agrupa todas sus campañas, gestiones y canales del ultimo periodo cargado
     *
     * @param string $lead Numero de documento del lead.
     * @return table
     */
    function consulta($lead){
        $sql = $this->getLeadEjecutivo(null)
            ->addSelect('WCLI.PRODUCTO_PRINCIPAL','WCLI.NUMERO_PRODUCTOS','WCLI.SCORE_COMPORTAMIENTO','WCLI.ATRASO_PROMEDIO',
                'WCLI.ATRASO_ULTIMO','WCLI.ULTIMA_FECHA_EVALUACION','WCLI.CALIFICACION_SBS','WCLI.MONTO_APROBADO','WCLI.MONTO_DISPONIBLE'
            )
            ->leftJoin('WEBVPC_CLIENTE AS WCLI', function($join) {
                    $join->on('WL.PERIODO', '=', 'WCLI.PERIODO');
                    $join->on('WL.NUM_DOC', '=', 'WCLI.NUM_DOC');
                 });
        $sql->orders = null;
        return $sql->where('WL.NUM_DOC','=',$lead)->orderBy('WL.PERIODO','DESC');
    }


    /**
     * Devuelve los datos del lead para la impresion. Aumento los numeros ingresados
     *
     * @param string $periodo Periodo en que fue cargado el Lead
     * @param string $ejecutivo Ejecutivo de Negocio
     * @param string $lead Numero de documento del lead.
     * @param array $canales Canales en que esta desplegado el Lead
     * @param array $filtros Filtros de busqueda
     * @param array $orden array con el campo a ordenar y como se ordena (asc|desc)
     * @return table
    */
    function getLeadEjecutivoFull($periodo,$ejecutivo = null, $lead = null,$canales = [],$filtros = null,$orden = null){
        $telefonos = DB::table('WEBVPC_ADD_CONTACTO as WACON')
            ->select(DB::raw("'|' + ".'WACON.VALOR'))
            ->whereRaw("WACON.TIPO_CONTACTO = 'TELEFONO'")
            ->whereRaw('WL.NUM_DOC = WACON.NUM_DOC')
            ->whereRaw('WACON.PERIODO = '.$periodo);

        $sql = $this->getLeadEjecutivo($periodo,$ejecutivo, $lead,$canales,$filtros,$orden)
                    ->addSelect(DB::raw("STUFF((". $telefonos->toSql() ." FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1, '') TELEFONOS_ADD"))
                    ;
                    /*
                    ->leftJoin('WEBVPC_FEEDBACK AS WF1',function($join){
                        $join->on('WL.PERIODO','=','WF1.PERIODO');
                        $join->on('WL.NUM_DOC','=','WF1.NUM_DOC');
                        $join->on('WL.TELEFONO1','=','WF1.VALOR');
                    })
                    ->leftJoin('WEBVPC_FEEDBACK AS WF2',function($join){
                        $join->on('WL.PERIODO','=','WF2.PERIODO');
                        $join->on('WL.NUM_DOC','=','WF2.NUM_DOC');
                        $join->on('WL.TELEFONO2','=','WF2.VALOR');
                    })
                    ->leftJoin('WEBVPC_FEEDBACK AS WF3',function($join){
                        $join->on('WL.PERIODO','=','WF3.PERIODO');
                        $join->on('WL.NUM_DOC','=','WF3.NUM_DOC');
                        $join->on('WL.TELEFONO3','=','WF3.VALOR');
                    })
                    ->leftJoin('WEBVPC_FEEDBACK AS WF4',function($join){
                        $join->on('WL.PERIODO','=','WF4.PERIODO');
                        $join->on('WL.NUM_DOC','=','WF4.NUM_DOC');
                        $join->on('WL.TELEFONO4','=','WF4.VALOR');
                    })*/
        return $sql;
    }

    /**
     * Devuelve los datos del lead de un asistente comercial
     *
     * @param string $periodo Periodo en que fue cargado el Lead
     * @param string $asistente Registro del Asistente Comercial
     * @param string $ejecutivo Registro del Ejecutivo de Negocio
     * @param string $lead Numero de documento del lead.
     * @param array $canales Canales en que esta desplegado el Lead
     * @param array $orden array con el campo a ordenar y como se ordena (asc|desc)
     * @return table
    */
    function getLeadAsistente($periodo,$asistente,$ejecutivo,$lead,$canales= []){
        $sql = $this->getLeadEjecutivo($periodo,$ejecutivo,$lead,$canales)
                ->join('WEBVPC_AC_EN_RELACION as WAER', function($join){
                        $join->on('WAER.EJECUTIVO_NEGOCIO', '=', 'WLE.REGISTRO_EN');
                    })
                ->where('WAER.ASISTENTE_COMERCIAL','=',$asistente);

        //Limpiamos el order anterior del lead ejecutivo
        $sql->orders = null;
        $sql = $sql->orderBy('WLE.MARCA_ASISTENTE_COMERCIAL', 'DESC');
        return $sql;
    }

    /**
     * Devuelve los datos del lead disponible para la asignacion/reasignacion
     *
     * @param string $lead Numero de documento del lead.
     * @param string $periodo Periodo en que fue cargado el Lead
     * @return table
    */
    function getLeadAsignacion($lead,$periodo){
        $sql = $this->consulta($lead);
        return $sql;
    }

    function getOrderColumn($field){
        switch ($field) {
                case 'lead':
                    return 'WL.NOMBRE_CLIENTE';
                case 'direccion':
                    return 'WL.DISTRITO';
                case 'deuda':
                    return 'WL.DEUDA_SSFF';
            };
    }

    function getLeadsEjecutivoResumido($periodo,$registro,$camp,$nombre){

        $filtros = [];
        $filtros['campanha'] = $camp;
        $filtros['lead'] = $nombre;

        $sql = $this->getLeadEjecutivo($periodo,$registro,null,[],$filtros,null);
        $sql->columns = ['WL.PERIODO','WL.NUM_DOC','WL.NOMBRE_CLIENTE'];
        return $sql;
    }

    function updateLead($numdoc,$data,$nuevosavales=[])
    {
        DB::beginTransaction();
        $status = true;
        try {

            $maxperiodo = DB::table("WEBVPC_LEAD")->select(DB::Raw("MAX(PERIODO) as PERIODO"))->get()->first();
            DB::table('WEBVPC_LEAD_AVAL')->where('NUM_DOC', '=', $numdoc)->delete();
            if (is_array($nuevosavales) && count($nuevosavales)>0) {
                DB::table('WEBVPC_LEAD_AVAL')->insert($nuevosavales);
            }

            DB::table('WEBVPC_LEAD')
                ->where('NUM_DOC', '=', $numdoc)
                ->where('PERIODO', '=', $maxperiodo->PERIODO)
                ->update($data);
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }

        return $status;
    }

    function getProductosByRuc($documento){
        $leadcampest = DB::table('WEBVPC_LEAD_CAMP_EST')->select(DB::Raw("MAX(periodo) as PERIODO"))->where("ID_CAMP_est","=",100)->get()->first();

        return DB::table('WEBVPC_LEAD_PRODUCTO as WLP')
        ->select('WLP.*')
        ->where('WLP.NUM_RUC',$documento)
        ->where('WLP.PERIODO',$leadcampest->PERIODO);
    }

    function getAvalByRuc($documento){
        return DB::table('WEBVPC_LEAD_AVAL as WLA')
        ->select('WLA.*')
        ->where('WLA.NUM_DOC',$documento);
    }
}
