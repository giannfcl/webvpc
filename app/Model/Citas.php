<?php
namespace App\Model;
use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class Citas extends Model

{
	protected $table = 'WEBVPC_CITAS';
    
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey =['periodo',           
                            'registro_en_agendador',
                            'registro_en',
                            'num_doc',  
        					];
					        

        /**
     * The name of the "created at" column.
     *
     * @var string
     */
     public $timestamps = true;
    
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'FECHA_CITA',
        'TIPO_HORARIO',
        'PERSONA_CONTACTO',
        'TELEFONO_CONTACTO',
        'DEPARTAMENTO',
        'PROVINCIA',
        'DISTRITO',
        'DIRECCION_CONTACTO',
        'REFERENCIA',
        'FECHA_REGISTRO',
        ];


    function getCitas($periodo = null, $lead= null,$zona = null,$centro = null){
        $sql = DB::table('WEBVPC_CITAS as WC')
                ->select('WC.FECHA_REGISTRO', 'WC.REGISTRO_EN_AGENDADOR', 'WU1.NOMBRE AS NOMBRE_AG', 
                    'WC.REGISTRO_EN', 'WU2.NOMBRE AS NOMBRE_EN', DB::raw('CONVERT(VARCHAR(16), WC.FECHA_CITA, 120) AS FECHA_CITA'),
                     'WT.TIENDA', 'WT.ZONA', 
                    'WC.NUM_DOC', 'WC.PERSONA_CONTACTO', 'WC.TELEFONO_CONTACTO', 'WC.DIRECCION_CONTACTO', 'WC.REFERENCIA','WT.CENTRO'
                    ,'WL.NOMBRE_CLIENTE','WL.DISTRITO','WLCE.CAMPANHA'
                )
                ->join('WEBVPC_USUARIO AS WU1', function($join){
                    $join->on('WU1.REGISTRO', '=', 'WC.REGISTRO_EN_AGENDADOR');
                })
                ->join('WEBVPC_USUARIO AS WU2', function($join){
                    $join->on('WU2.REGISTRO', '=', 'WC.REGISTRO_EN');
                })
                ->join('WEBVPC_TIENDA AS WT', function($join) {
                    $join->on('WU2.ID_TIENDA', '=', 'WT.ID_TIENDA');
                })
                ->join('WEBVPC_LEAD AS WL', function($join) {
                    $join->on('WL.NUM_DOC', '=', 'WC.NUM_DOC');
                })
                ->join(DB::Raw("(SELECT A.NUM_DOC,A.CAMPANHA FROM (
                                SELECT WLCE.NUM_DOC,WCE.NOMBRE CAMPANHA,
                                ROW_NUMBER() OVER (PARTITION BY WLCE.PERIODO,WLCE.NUM_DOC ORDER BY WLCE.FECHA_CARGA DESC) RNK
                                FROM WEBVPC_LEAD_CAMP_EST WLCE 
                                JOIN WEBVPC_CAMP_EST WCE ON  WCE.ID_CAMP_EST=WLCE.ID_CAMP_EST
                                JOIN WEBVPC_CAMP_EST_INSTANCIA WCEI ON WCEI.PERIODO=WLCE.PERIODO AND WCEI.ID_CAMP_EST=WLCE.ID_CAMP_EST
                                WHERE WCE.BANCA='BPE' 
                                AND WCE.TIPO='CAMPAÑA'
                                AND WCEI.HABILITADO=1
                            ) A WHERE A.RNK=1) WLCE"), function($join){
                   $join->on('WL.NUM_DOC', '=', 'WLCE.NUM_DOC');
                })
                ->where('WC.FECHA_REGISTRO','>=',DB::raw('DATEADD(MONTH,-1,GETDATE())'))
                //->orderBy('FECHA_REGISTRO', 'DESC')
                ;
        if ($periodo){
            $sql = $sql->where('WL.PERIODO','=',$periodo);
        }
        if ($lead){
            $sql = $sql->where('WC.NUM_DOC','=',$lead);
        }
        if ($zona){
            $sql = $sql->where('WU2.ID_ZONA','=',$zona);
        }
        if ($centro){
            $sql = $sql->where('WU2.ID_CENTRO','=',$centro);
        }
        return $sql;
    }

    function getResumenByEjecutivo($zonal=null, $centro=null, $tienda=null, $fechaIni=null, $fechaFin=null){
        $sql = DB::table('WEBVPC_USUARIO AS WU')
                ->select('WU.REGISTRO', 'WU.NOMBRE', 'WT.ZONA', 'WT.CENTRO', 'WT.TIENDA')
                ->addSelect(DB::raw('COUNT((CASE WHEN WC.FECHA_CITA > GETDATE() AND ESTADO IN (1,2,3,7) THEN ID_CITA ELSE NULL END)) PROGRAMADAS, COUNT((CASE WHEN WC.FECHA_CITA < GETDATE() AND ESTADO IN (1,2,3,7) THEN ID_CITA ELSE NULL END)) VENCIDAS, COUNT((CASE WHEN ESTADO IN (4,5) THEN ID_CITA ELSE NULL END)) REALIZADAS, COUNT(WC.FECHA_CITA) TOTAL'))
                ->join('WEBVPC_TIENDA AS WT', function($join){
                    $join->on('WT.ID_ZONA','=','WU.ID_ZONA');
                    $join->on('WT.ID_CENTRO','=','WU.ID_CENTRO');
                    $join->on('WU.ID_TIENDA','=','WT.ID_TIENDA');
                })
                ->join('WEBVPC_CITAS AS WC', function($join){
                    $join->on('WC.REGISTRO_EN','=','WU.REGISTRO');
                });
        if ($zonal){
            $sql = $sql->where('WU.ID_ZONA', $zonal);
        }
        if ($centro){
            $sql = $sql->where('WU.ID_CENTRO', $centro);
        }
        if ($tienda){
            $sql = $sql->where('WU.ID_TIENDA', $tienda);
        }
        if($fechaIni){
            $sql = $sql->whereDate('WC.FECHA_CITA', '>=', $fechaIni);
        }
        if($fechaFin){
            $sql = $sql->whereDate('WC.FECHA_CITA', '<=', $fechaFin);
        }
        $sql = $sql->groupBy('WU.REGISTRO', 'WU.NOMBRE', 'WT.ZONA', 'WT.CENTRO', 'WT.TIENDA');

        return $sql;
    }

    function getStatsCall(){
        $sql = DB::select("SELECT REGISTRO_EN_AGENDADOR, NOMBRE, SUM(_4) _4, SUM(_3) _3, SUM(_2) _2, SUM(_1) _1, SUM(HOY) HOY 
                           FROM (
                               SELECT REGISTRO_EN_AGENDADOR, WU.NOMBRE, FECHA_REGISTRO, (CASE WHEN CONVERT(DATE,FECHA_REGISTRO) = CONVERT(DATE, DATEADD(DAY, (CASE WHEN DATEPART(w, GETDATE())-4 > 0 THEN -4 ELSE -6 END), GETDATE())) THEN COUNT(FECHA_REGISTRO) ELSE 0 END) _4,
                                  (CASE WHEN CONVERT(DATE,FECHA_REGISTRO) = CONVERT(DATE, DATEADD(DAY, (CASE WHEN DATEPART(w, GETDATE())-3 > 0 THEN -3 ELSE -5 END), GETDATE())) THEN COUNT(FECHA_REGISTRO) ELSE 0 END) _3,
                                  (CASE WHEN CONVERT(DATE,FECHA_REGISTRO) = CONVERT(DATE, DATEADD(DAY, (CASE WHEN DATEPART(w, GETDATE())-2 > 0 THEN -2 ELSE -4 END), GETDATE())) THEN COUNT(FECHA_REGISTRO) ELSE 0 END) _2,
                                  (CASE WHEN CONVERT(DATE,FECHA_REGISTRO) = CONVERT(DATE, DATEADD(DAY, (CASE WHEN DATEPART(w, GETDATE())-1 > 0 THEN -1 ELSE -3 END), GETDATE())) THEN COUNT(FECHA_REGISTRO) ELSE 0 END) _1,
                                  (CASE WHEN CONVERT(DATE,FECHA_REGISTRO) = CONVERT(DATE, GETDATE()) THEN COUNT(FECHA_REGISTRO) ELSE 0 END) HOY
                                FROM WEBVPC_CITAS WC
                                INNER JOIN WEBVPC_USUARIO WU
                                ON WU.REGISTRO=WC.REGISTRO_EN_AGENDADOR
                                WHERE WC.TIPO='CALL'
                                GROUP BY CONVERT(DATE, FECHA_REGISTRO), REGISTRO_EN_AGENDADOR, WU.NOMBRE, FECHA_REGISTRO
                            ) STATS_CALL
                            GROUP BY REGISTRO_EN_AGENDADOR, NOMBRE");
        return $sql;
    }

    /**
     * Devuelve el horario de todas las citas del cliente
     *
     * @param  string $en Registro del ejecutivo de negocio
     * @param  string $inicio Fecha de inicio del horario del ejecutivo
     * @param  string $fin Fecha de fin del horario del ejecutivo
     * @param  string $horario Filtrar por el tipo de horario
     * @param  string $cita Id de cita que no se incluye en la busqueda
     * @param  array $estados Filtrar por lista de estados
     * @return table Lista de fechas,horario y leads de las citas
     */
    function getHorario($en,$inicio,$fin = null,$horario = null,$cita =null,$estados = null){
        $sql = DB::table('WEBVPC_CITAS as WC')
                ->select('WC.REGISTRO_EN',DB::raw('CONVERT(VARCHAR(25),WC.FECHA_CITA,120) as FECHA_CITA'),'WC.TIPO_HORARIO','WC.NUM_DOC')
                ->where('WC.REGISTRO_EN','=',$en)
                ->orderBy('WC.FECHA_CITA');

        if ($inicio)
            $sql = $sql->where('WC.FECHA_CITA','>=',$inicio);

        if ($horario)
            $sql = $sql->where('WC.TIPO_HORARIO','=',$horario);

        if ($fin)
            $sql = $sql->where('WC.FECHA_CITA','<=',$fin);

        if ($cita)
            $sql = $sql->where('WC.ID_CITA','<>',$cita);

        if ($estados)
            $sql = $sql->whereIn('WC.ESTADO',$estados);

        return $sql;
    }

    /**
     * Registra una cita
     *
     * @param  array $cita Datos de la cita. Ver entidad.
     * @param  array $leadCampanha Datos de la relacion lead-campanha
     * @param  array $movimientoCanal Datos de movimiento de canal del lead 
     * @param  array $citaEstadoHist Datos del estado histórico de la cita
     * @return bool
     */
    function nuevo($cita,$leadCampanha,$movimientoCanal,$citaEstadoHist){
        DB::beginTransaction();
        $status = true;
        try {
            $id = DB::table('WEBVPC_CITAS')->insertGetId($cita);
            $citaEstadoHist['ID_CITA'] = $id;
            DB::table('WEBVPC_CITA_ESTADO_HISTORICO')->insert($citaEstadoHist);
            DB::table('WEBVPC_HIST_MOVIMIENTO_CANAL')->insert($movimientoCanal);
            DB::table('WEBVPC_LEAD_CAMP_EST')
            ->where('PERIODO', $leadCampanha['PERIODO'])
            ->where('NUM_DOC', $leadCampanha['NUM_DOC'])
            ->update($leadCampanha);
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }

    /**
     * Busca una cita por ID
     *
     * @param  int $id Id de la cita
     * @return table Toda la información de la cita
     */
    function getById($id){
        $sql = DB::table('WEBVPC_CITAS as WC')
                ->select('WC.ID_CITA','WC.PERIODO','WC.NUM_DOC','WC.REGISTRO_EN_AGENDADOR','WC.REGISTRO_EN'
                        ,DB::raw('CONVERT(VARCHAR(25),WC.FECHA_CITA,120) as FECHA_CITA')
                        ,'WC.TIPO_HORARIO','WC.PERSONA_CONTACTO','WC.TELEFONO_CONTACTO','WC.DEPARTAMENTO','WC.PROVINCIA','WC.DISTRITO'
                        ,'WC.DIRECCION_CONTACTO','WC.REFERENCIA',DB::raw('CONVERT(VARCHAR(25),WC.FECHA_REGISTRO,120) as FECHA_REGISTRO'),'WC.TIPO','WC.ESTADO','WC.AUTORIZACION_DATOS')
                ->where('WC.ID_CITA','=',$id);
        return $sql;
    }

    /**
     * Busca una cita por Ejecutivo y Lead
     *
     * @param  string $ejecutivo Registro del ejecutivo de negocio
     * @param  string $lead Numero de documento del lead
     * @return table Toda la información de la cita
     */
    function buscar($ejecutivo,$lead){
        $sql = DB::table('WEBVPC_CITAS as WC')
                ->select('WC.ID_CITA','WC.PERIODO','WC.NUM_DOC','WC.REGISTRO_EN_AGENDADOR','WC.REGISTRO_EN'
                        ,DB::raw('CONVERT(VARCHAR(25),WC.FECHA_CITA,120) as FECHA_CITA')
                        ,'WC.TIPO_HORARIO','WC.PERSONA_CONTACTO','WC.TELEFONO_CONTACTO','WC.DEPARTAMENTO','WC.PROVINCIA','WC.DISTRITO'
                        ,'WC.DIRECCION_CONTACTO','WC.REFERENCIA',DB::raw('CONVERT(VARCHAR(25),WC.FECHA_REGISTRO,120) as FECHA_REGISTRO'),'WC.TIPO','WC.ESTADO','WC.AUTORIZACION_DATOS')
                ->where('WC.REGISTRO_EN','=',$ejecutivo)
                ->where('WC.NUM_DOC','=',$lead)
                ->where('WC.FECHA_CITA','>=',DB::raw('DATEADD(month, DATEDIFF(month, 0, GETDATE()), 0)'));
        return $sql;
    }


    /**
     * Reprogramar una Cita
     *
     * @param  int $id Id de la cita a reprogramar
     * @param  string $cita Datos de la cita a reprogramar
     * @return table Datos históricos del estado de la cita
     */
    function reprogramar($id,$cita,$historico){
        DB::beginTransaction();
        $status = true;
        try {
            DB::table('WEBVPC_CITA_ESTADO_HISTORICO')->insert($historico);
            DB::table('WEBVPC_CITAS')
            ->where('ID_CITA', $id)
            ->update(array_except($cita, ['ID_CITA']));
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;

    }

    public static function ultimos5Dias() {
        return DB::select("SELECT GETDATE() HOY, DATEADD(DAY, (CASE WHEN DATEPART(w, GETDATE())-1 > 0 THEN -1 ELSE -3 END), GETDATE()) _1, 
       DATEADD(DAY, (CASE WHEN DATEPART(w, GETDATE())-2 > 0 THEN -2 ELSE -4 END), GETDATE()) _2,
       DATEADD(DAY, (CASE WHEN DATEPART(w, GETDATE())-3 > 0 THEN -3 ELSE -5 END), GETDATE()) _3,
       DATEADD(DAY, (CASE WHEN DATEPART(w, GETDATE())-4 > 0 THEN -4 ELSE -6 END), GETDATE()) _4");
    }

}