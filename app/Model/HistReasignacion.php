<?php
namespace App\Model;
use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class HistEstCampanha extends Model

{
	protected $table = 'WEBVPC_HIST_REASIGNACION';
    
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey =['PERIODO',    
        					'REGISTRO_REASIGNADOR',
        					'FECHA_REGISTRO',
        					];
					        

        /**
     * The name of the "created at" column.
     *
     * @var string
     */
     public $timestamps = false;
    
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'REGISTRO_EN_ORIGEN',
        'REGISTRO_EN_DESTINO',
        ];

}


