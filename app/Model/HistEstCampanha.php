<?php

namespace App\Model;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class HistEstCampanha extends Model {

    protected $table = 'WEBVPC_HIST_EST_CAMPANHA';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'ID_CAMPANHA';

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'FECHA_ACTUALIZACION';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'ESTADO_CAMPANHA',
    ];

}
