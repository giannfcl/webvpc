<?php
namespace App\Model;
use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class GestionLeadCampanha extends Model

{
	protected $table = 'WEBVPC_GESTION_LEAD_CAMPANHA';
    
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey =['ID_GESTION'] ;
					        

        /**
     * The name of the "created at" column.
     *
     * @var string
     */
    public $timestamps = false;
    
    
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'PERIODO',
        'ID_CAMPANHA',
        'RESGISTRO_EN',
        'NUM_DOC',
        'ID_RESULTADO_GESTION',
        'ROL_GESTION',
        'COMENTARIO',
        'FLG_ULTIMO',
        'BANCA',
        ];


    /**
     * Traer todos los datos de la gestion por ID
     *
     * @param int $id Id de la gestión
     * @return table
     */
    function get($id = null){
        $sql = DB::table('WEBVPC_GESTION_LEAD_CAMP_EST as WGL')
                ->select(
                        'WGL.COMENTARIO',
                        'WGL.VISITADO',
                        'WGL.SOLICITUD_MONTO',
                        'WGL.SOLICITUD_TASA',
                        'WGL.SOLICITUD_PLAZO',
                        'WGL.SOLICITUD_GRACIA',
                        'WU.NOMBRE as EJECUTIVO',
                        'WCE.NOMBRE as CAMP_EST_NOMBRE',
                        DB::raw('CONVERT(VARCHAR(10),WGL.FECHA_REGISTRO,120) as FECHA_REGISTRO'),
                        DB::raw('CONVERT(VARCHAR(10),WGL.FECHA_VOLVER_LLAMAR,120) as FECHA_VOLVER_LLAMAR'),
                        'WRG.DESCRIPCION as GESTION_MOTIVO',
                        'WRG2.DESCRIPCION as GESTION_RESULTADO',
                        'WGL.ID_RESULTADO_GESTION',
                        'WRG2.ID_RESULTADO_GESTION_N2'
                    )
                ->join('WEBVPC_RESULTADO_GESTION as WRG', function($join){
                    $join->on('WGL.ID_RESULTADO_GESTION', '=', 'WRG.ID_RESULTADO_GESTION');
                })
                ->join('WEBVPC_RESULTADO_GESTION_N2 as WRG2', function($join){
                    $join->on('WRG2.ID_RESULTADO_GESTION_N2', '=', 'WRG.ID_RESULTADO_GESTION_N2');
                })
                ->join('WEBVPC_USUARIO as WU', function($join){
                    $join->on('WU.REGISTRO', '=', 'WGL.REGISTRO_EN');
                })
                ->join('WEBVPC_CAMP_EST as WCE', function($join){
                    $join->on('WCE.ID_CAMP_EST', '=', 'WGL.ID_CAMP_EST');
                });

        if ($id){
            $sql = $sql->where('WGL.ID_GESTION','=', $id);
        }
        return $sql;
    }

    /**
     * Traer todos los datos de la gestion por lead ordenado por fecha
     *
     * @param string $lead Numero de documento por el lead
     * @return table
     */
    function getByLead($lead,$cartera=NULL){
        $sql = $this->get()
                ->where('WGL.NUM_DOC','=', $lead)
                ->where('WGL.FLG_VISIBLE','=', 1);

        if($cartera==NULL)
            $sql=$sql->where('WCE.tipo',\App\Entity\Campanha::TIPO_CAMPANHA)
                ->orderBy('WGL.FECHA_REGISTRO', 'DESC');
        else
            $sql=$sql->where('WCE.tipo',\App\Entity\Campanha::TIPO_CARTERA)
                    ->orderBy('WGL.FECHA_REGISTRO', 'DESC');
        return $sql;
    }

    /**
     * Traer todos los datos de la gestion por lead ordenado por fecha
     *
     * @param string $lead Numero de documento por el lead
     * @return table
     */
    function registrar($gestion,$idcita=null,$cita = null, $citaHist = null){

        \Log::info($gestion);

        DB::beginTransaction();
        $status = true;
        try {
            //Update del resto de gestiones a flg_ultimo =0
            DB::table('WEBVPC_GESTION_LEAD_CAMP_EST')
            //->where('PERIODO', $gestion['PERIODO'])
            ->where('NUM_DOC', $gestion['NUM_DOC'])
            ->where('ID_CAMP_EST', $gestion['ID_CAMP_EST'])
            ->where('PERIODO', $gestion['PERIODO'])
            ->update(['FLG_ULTIMO' => 0]);



            $status = DB::table('WEBVPC_GESTION_LEAD_CAMP_EST')->insertGetId($gestion);
            
            if ($idcita){
                DB::table('WEBVPC_CITAS')
                    ->where('ID_CITA','=',$idcita)
                    ->update(array_except($cita,['ID_CITA']));
            }
            if ($citaHist){
                DB::table('WEBVPC_CITA_ESTADO_HISTORICO')->insert($citaHist);
            }
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }

    /**
     * Registro de varias gestiones (gestion de Asistente Comercial)
     *
     * @param array $gestiones Numero de documento por el lead
     * @return table
     */
    function registrarLote($gestiones){
        DB::beginTransaction();
        $status = true;
        try {
            foreach($gestiones as $gestion){
                // Quita el flag ultimo a a las gestiones anteriores
                DB::table('WEBVPC_GESTION_LEAD_CAMP_EST')
                //->where('PERIODO', $gestion['PERIODO'])
                ->where('NUM_DOC', $gestion['NUM_DOC'])
                ->where('ID_CAMP_EST', $gestion['ID_CAMP_EST'])
                ->update(['FLG_ULTIMO' => 0]);
                //Inserta las ultimas gestiones
                DB::table('WEBVPC_GESTION_LEAD_CAMP_EST')->insert($gestion);    
            }
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }
}