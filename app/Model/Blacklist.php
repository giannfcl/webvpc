<?php
namespace App\Model;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Jenssegers\Date\Date as Carbon;


class Blacklist extends Model {
	function buscar($busqueda)
	{
		$sql=DB::table(DB::Raw("
				(
				select CASE WHEN a.TIPO_DOC=1 THEN 'DNI' WHEN a.TIPO_DOC='2' THEN 'RUC' ELSE '' END TIPO_DOC,TIPO_DOC as TD, a.NUM_DOC, MAX(a.fecha_registro) FECHA_REGISTRO ,STUFF(
						(
				          select ','+NUMERO
				          from WEBVPC_DE_BLACKLIST b
				          where ISNULL(b.TIPO_DOC,'')=ISNULL(a.TIPO_DOC,'') and a.NUM_DOC=b.NUM_DOC
				          for xml path('')
				      	),1,1,''
				      	) NUMEROS
				from WEBVPC_DE_BLACKLIST a
				group by a.TIPO_DOC, a.NUM_DOC
				
				) as TODO 
        	"));
        if (isset($busqueda['documento'])) {
        	$sql=$sql->where('NUM_DOC',$busqueda['documento']);
        }
        $sql=$sql->orderBy("FECHA_REGISTRO","DESC");
        return $sql;
	}

	function NuevoTelefonoEmpresa($nuevo)
	{
		return DB::table("WEBVPC_DE_BLACKLIST")->insert($nuevo);
	}

	function gettelefonosexistentes($datos)
	{
		// dd($datos);
		$sql=DB::table("WEBVPC_DE_BLACKLIST")
				->where('TIPO_DOC',$datos['tipodoc'])
				->where('NUM_DOC',$datos['numdoc'])
				->where('NUMERO',$datos['telefono'])
				->count();
				// dd($sql);
		return $sql;
	}
}
?>