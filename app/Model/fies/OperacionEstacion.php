<?php
namespace App\Model\fies;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;




class OperacionEstacion extends Model{

   protected $table = 'WEBVPC_FIES_OP_EST_ATRIBUTO';



	 public $timestamps = false;

     protected $guarded = [
    'FECHA_REGISTRO	',
    'COD_OPERACION',
    'ID_ESTACION',
    'ATRIBUTO',
    'TIPO',
    'VALOR',
    'FLG_ULTIMO',
     ];


      function getEstadoAtributo ($codOperacion) {
         $sql = DB::table('WEBVPC_FIES_EST_ATRIBUTO AS WFEA')
                    ->select('WFEA.ID_ESTACION','WFEA.ORDEN','WFEA.ATRIBUTO','WFOEA.VALOR')  
                    ->leftJoin('WEBVPC_FIES_OP_EST_ATRIBUTO as WFOEA',function($join) use ($codOperacion) {
                                        $join->on('WFOEA.ID_ESTACION', '=', 'WFEA.ID_ESTACION');
                                        $join->on('WFOEA.ATRIBUTO','=', 'WFEA.ATRIBUTO');
                                        $join->on('WFOEA.FLG_ULTIMO','=', DB::raw(1));
                                        $join->on('WFOEA.COD_OPERACION','=', DB::raw($codOperacion));
                        })     
                    ->orderBy('WFEA.ID_ESTACION', 'ASC')
                    ->orderBy('WFEA.ORDEN', 'ASC')
                    ->distinct();       
        return $sql;      
     }

     
     

    function guardarAtributo($codOperacion,$operacionAtributo ){
          DB::beginTransaction();
        $status = true;
        try {
                // Quita el flag ultimo a a las gestiones anteriores
                DB::table('WEBVPC_FIES_OP_EST_ATRIBUTO')->insert($operacionAtributo);    
                  
                 
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            \Debugbar::info($status);
            DB::rollback();
        }


        return $status;

    }

    function actualizaFlagUltimo($codOperacion,$idEstacion ){
          DB::beginTransaction();
        $status = true;
        try {
                // Quita el flag ultimo a a las gestiones anteriores
                DB::table('WEBVPC_FIES_OP_EST_ATRIBUTO')
                ->where('COD_OPERACION',$codOperacion)
                ->where('ID_ESTACION',$idEstacion)
                ->update(['FLG_ULTIMO' => 0]);
            
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            \Debugbar::info($status);
            DB::rollback();
        }
        return $status;

    }

    function registrarHisGestion($fecha,$user){
           DB::beginTransaction();
                $status = true;
                $id=0;
                try {
                    $id = DB::table('WEBVPC_FIES_GESTION_HIST')->insertGetId(['FECHA_REGISTRO' => $fecha, 'REGISTRO' => $user]);
                    DB::commit();
                } catch (\Exception $e) {
                    Log::error('BASE_DE_DATOS|' . $e->getMessage());
                    $status = false;
                    DB::rollback();
                }
                return $id;

    }

       function validaFecha($id_estacion,$codOperacion){

    
           $sql = DB::table('WEBVPC_FIES_OP_EST_ATRIBUTO as WC')
                ->leftJoin('WEBVPC_FIES_EST_ATRIBUTO as WFEA',function($join){
                    $join->on('WFEA.ID_ESTACION', '=', 'WC.ID_ESTACION');
                    $join->on('WFEA.ATRIBUTO', '=', 'WC.ATRIBUTO');
                    })
                ->where('WC.ID_ESTACION','=',$id_estacion)
                ->where('WC.COD_OPERACION',$codOperacion)
                ->where('WC.FLG_ULTIMO','=', DB::raw(1))
                ->where('WFEA.TIPO','=',DB::raw("'FECHA'"))
                ->max('VALOR');
    


        return $sql;

    }

    function validaFechaEstatus($id_estacion,$codOperacion){

      
           $sql = DB::table('WEBVPC_FIES_OP_EST_ATRIBUTO as WC')
                ->leftJoin('WEBVPC_FIES_EST_ATRIBUTO as WFEA',function($join){
                    $join->on('WFEA.ID_ESTACION', '=', 'WC.ID_ESTACION');
                    $join->on('WFEA.ATRIBUTO', '=', 'WC.ATRIBUTO');
                    })
                ->where('WC.ID_ESTACION','=',$id_estacion)
                ->where('WC.COD_OPERACION',$codOperacion)
                ->where('WC.FLG_ULTIMO','=', DB::raw(1))
                ->where('WFEA.TIPO','=',DB::raw("'FECHA'"))
                ->max('VALOR');
    


        return $sql;

    }

    function validaEstatus($id_estacion,$codOperacion){

      
           $sql = DB::table('WEBVPC_FIES_OP_EST_ATRIBUTO as WC')
                ->leftJoin('WEBVPC_FIES_EST_ATRIBUTO as WFEA',function($join){
                    $join->on('WFEA.ID_ESTACION', '=', 'WC.ID_ESTACION');
                    $join->on('WFEA.ATRIBUTO', '=', 'WC.ATRIBUTO');
                    })
                ->where('WC.ID_ESTACION','=',$id_estacion)
                ->where('WC.COD_OPERACION',$codOperacion)
                ->where('WC.FLG_ULTIMO','=', DB::raw(1))
                ->where('WFEA.ATRIBUTO','=',DB::raw("'ESTATUS'"))
                ->max('VALOR');
    


        return $sql;

    }

    function actualizaMaxEstacion ($codOperacion) {
    DB::beginTransaction();
        $status = true;
    try {
        $idEstacion = DB::table('WEBVPC_FIES_OP_EST_ATRIBUTO AS WFEA')                         
                        ->where('WFEA.COD_OPERACION',$codOperacion)  
                        ->max('ID_ESTACION');

         $probabilidad=DB::table('WEBVPC_FIES_OP_EST_ATRIBUTO as WFOA')
                        ->where('ID_ESTACION',$idEstacion) 
                        ->where('COD_OPERACION',$codOperacion) 
                        ->where('FLG_ULTIMO',DB::raw(1)) 
                        ->where('ATRIBUTO',DB::raw("'PROBABILIDAD'")) 
                        ->max('WFOA.VALOR');

                        DB::table('WEBVPC_FIES_OPERACION')
                        ->where('COD_OPERACION',$codOperacion) 
                        ->update(['ULTIMO_ESTACION' => $idEstacion]);

                          DB::table('WEBVPC_FIES_OPERACION')
                        ->where('COD_OPERACION',$codOperacion) 
                        ->update(['PROBABILIDAD' => $probabilidad]);

                    DB::commit();
    }catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
  }

  function getOperacionDias ($codOperacion) {
         $sql1=  DB::table('WEBVPC_FIES_OP_EST_ATRIBUTO AS A') 
                    ->select(DB::raw("'Fecha Recepcion'"),'A.VALOR as FECHA')  
                    ->where('A.COD_OPERACION',$codOperacion)
                    ->where('A.ID_ESTACION',DB::raw(5))
                    ->where('A.FLG_ULTIMO',DB::raw(1)) 
                    ->where('A.ATRIBUTO',DB::raw("'FECHA APROBACION'"))
                    ->distinct();  

        $sql2=  DB::table('WEBVPC_FIES_OP_EST_ATRIBUTO AS A') 
                    ->select(DB::raw("'Fecha Recepcion'"),'A.VALOR as FECHA')  
                    ->where('A.COD_OPERACION',$codOperacion)
                    ->where('A.ID_ESTACION',DB::raw(4))
                    ->where('A.ATRIBUTO',DB::raw("'FECHA RECEPCION'"))
                    ->distinct();  

         $sql = DB::table('WEBVPC_FIES_OP_EST_ATRIBUTO AS A')
                    ->select('A.VALOR AS ESTATUS','B.VALOR as FECHA')  
                    ->leftJoin('WEBVPC_FIES_OP_EST_ATRIBUTO as B',function($join) use ($codOperacion) {
                                        $join->on('A.COD_OPERACION', '=', 'B.COD_OPERACION');
                                        $join->on('A.ID_GESTION','=', 'B.ID_GESTION');
                        }) 
                    ->where('A.COD_OPERACION',$codOperacion)    
                    ->where('A.ATRIBUTO',DB::raw("'ESTATUS'"))
                    ->where('B.ATRIBUTO',DB::raw("'FECHA ESTATUS'"))
                    ->where('A.ID_ESTACION','>', 2)
                    ->where('B.ID_ESTACION','>', 2)
                    ->distinct()
                    ->unionAll($sql1)  
                    ->unionAll($sql2)  
                    ->orderBy('FECHA', 'ASC');
        \Debugbar::info($sql);
  
        return $sql;      
     }




}