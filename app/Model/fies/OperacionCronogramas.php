<?php
namespace App\Model\fies;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;


class OperacionCronogramas extends Model

{
	 protected $table = 'WEBVPC_FIES_OP_CRONOGRAMA';

	 protected $primaryKey =['COD_OPERACION'];

	 public $timestamps = false;

     protected $guarded = [
    'COD_OPERACION',
    'FECHA_REGISTRO',
    'TIPO',
    'MES',
    'FECHA',
    'MONTO',
    'ESTATUS',
    'FLG_ULTIMO'
     ];


     function getCronograma($codOperacion,$tipoCronograma){
            
            return $sql = DB::table('WEBVPC_FIES_OP_CRONOGRAMA AS WFOC')
                    ->select('WFOC.COD_OPERACION','WFOC.TIPO','WFOC.MES','WFOC.FECHA','WFOC.MONTO','WFOC.ESTATUS')
                    ->where('WFOC.COD_OPERACION','=',$codOperacion)
                    ->where('WFOC.TIPO','=',$tipoCronograma)
                    ->where('WFOC.FLG_ULTIMO','=',DB::raw(1))
                    ->orderBy('WFOC.FECHA', 'ASC')
                    ->get();

     }

   
     function setNuevaCuota($nuevacuota){ 
        DB::beginTransaction();
        $status = true;
        try {
            DB::table('WEBVPC_FIES_OP_CRONOGRAMA')->insert($nuevacuota);
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
     }

       function setFlagCero($data){
           
            DB::beginTransaction();
        $status = true;
        try {
            DB::table('WEBVPC_FIES_OP_CRONOGRAMA')                    
                    ->where('COD_OPERACION','=',$data['COD_OPERACION'])
                    ->where('TIPO','=',$data['TIPO']) 
                    ->where('FECHA','=',$data['FECHA'])                                       
                    ->where('FLG_ULTIMO','=',DB::raw(1))
                    ->update(['FLG_ULTIMO' => DB::raw(0)]);
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
     }


      function setCronogramaNuevo($nuevoCronogramas){
           
        \Debugbar::info("here");
        DB::beginTransaction();
        $status = true;
        try {
            foreach($nuevoCronogramas as $nuevoCronograma){
            DB::table('WEBVPC_FIES_OP_CRONOGRAMA')->insert($nuevoCronograma);
             }
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
     }
        function updateCronograma($data){              
        DB::beginTransaction();
        $status = true;
        try {            
            $sql = DB::table('WEBVPC_FIES_OP_CRONOGRAMA')                    
                    ->where('COD_OPERACION','=',$data['COD_OPERACION'])
                    ->where('TIPO','=',$data['TIPO']) 
                    ->where('FECHA','=',$data['FECHA_REGISTRO'])                                       
                    ->where('FLG_ULTIMO','=',DB::raw(1))
                    ->update(['ESTATUS' => $data['ESTATUS'],'FECHA' => $data['FECHA'],'MES' => $data['MES'],'MONTO' => $data['MONTO']]);
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }        
        return $status;
     }

     function setCuotaDefault($cuota,$prospecto){
         DB::beginTransaction();
                    $status = true;
                    try {
                        DB::table('WEBVPC_FIES_OP_CRONOGRAMA')->insert($cuota);
                         DB::commit();
                    } catch (\Exception $e) {
                        Log::error('BASE_DE_DATOS|' . $e->getMessage());
                        $status = false;
                        DB::rollback();
                    }
                    return $status;
     }
}