<?php
namespace App\Model\fies;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
	protected $table= 'WEBVPC_FIE_CLIENTES';

	protected $primaryKey =['periodo',           
                            'coddoc',
                            'cod_unico'];

	
	/**
     * The name of the "created at" column.
     *
     * @var string
     */
     public $timestamps = false;
    
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'CODSBS',
        'TIPCLI',
        'VENTAS',
        'CIIU',
        'COD_SECTORISTA',
        'COD_SECT_UNIQ',        
        'TIPDOC',
        'SECTOR_ECONOMICO',
        'GRUPOECONOMICO',
        'RAZONSOCIAL',
        'RAZONCOMERCIAL',
        'NOMBRE_CLIENTE',
        'BANCA',
        'SEGMENTO',
        'COD_UBIGEO',
        'DEPARTAMENTO',
        'PROVINCIA',
        'DISTRITO'
        ];	


        function getCliente($ruc){
        	return $sql = DB::table('WEBVPC_FIES_CLIENTES AS WFC')
        			->select('WFC.COD_UNICO','WFC.RAZONSOCIAL','WFC.SECTOR_ECONOMICO','WFC.GRUPOECONOMICO','WFC.BANCA','WFC.SEGMENTO','WFAD.LOGIN','WFAD.NOMBRE_ZONAL2')
                    ->join('ARBOL_DIARIO AS WFAD','WFC.COD_SECTORISTA','WFAD.COD_SECT_LARGO')
        			->where('WFC.CODDOC','=',$ruc);
        }				        
}