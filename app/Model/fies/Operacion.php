<?php
namespace App\Model\fies;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;


class Operacion extends Model

{
	 protected $table = 'WEBVPC_FIES_PROSPECTOS';

	 protected $primaryKey =['COD_OPERACION'];

	 public $timestamps = false;

     protected $guarded = [
    'COD_OPERACION',
    'COD_UNICO',
    'NUM_DOC',
    'NOMBRE_CLIENTE',
    'GRUPO_ECONOMICO',
    'SECTOR',
    'BANCA',
    'GRUPO_ZONAL',
    'SEGMENTO',
    'REG_EJECUTIVO_NEGOCIO'
        ];


    function get($operacion,$filtros = []){
        $sql = DB::table('WEBVPC_FIES_PROSPECTOS as WL')
                ->select('WL.COD_OPERACION','WL.COD_UNICO','WL.NUM_DOC','WL.NOMBRE_CLIENTE','WL.GRUPO_ECONOMICO','WL.SECTOR','WL.BANCA',
                    'WL.GRUPO_ZONAL','WL.SEGMENTO','WL.REG_EJECUTIVO_NEGOCIO');
        if ($operacion){
            $sql = $sql->where('WL.NUM_DOC','=',$operacion);
        }

        if (isset($filtros['GRUPO_ZONAL'])){
            $sql = $sql->where('WL.GRUPO_ZONAL','=',$filtros['grupo_zonal']);
        }
        return $sql;
    }


    function getOperacionEjecutivo($ejecutivo = null, $operacion = null,$filtros = null,$orden = null){
        $sql = DB::table('WEBVPC_RELACION_EJE_FIES as WRF')
            ->addSelect('WL.COD_OPERACION', 'WL.COD_UNICO', 'WL.NUM_DOC', 
                'WL.NOMBRE_CLIENTE', 'WL.GRUPO_ECONOMICO', 'WL.SECTOR', 'WL.BANCA', 'AD.NOMBRE_ZONAL2 AS GRUPO_ZONAL', 
                'WL.SEGMENTO', 'WL.REG_EJECUTIVO_NEGOCIO')
            ->addSelect('AD.ENCARGADO as ENCARGADO', 'WO.PRODUCTO', 'WFE.NOMBRE AS ULTIMO_ESTACION', 'WU.NOMBRE',
             'WO.PROBABILIDAD', 'WOID.TIPO AS TIPO_DESEMB', 'WOID.MES_PROBABLE as MES_PROBABLE_DESEMB', 'WOID.MONTO as MONTO_PROBABLE_DESEMB','WOID.MONEDA as MONEDA_DESEMB', 
             'WOIC.TIPO AS TIPO_COM', 'WOIC.MES_PROBABLE as MES_PROBABLE_COM', 'WOIC.MONTO as MONTO_PROBABLE_COM','WOIC.MONEDA as MONEDA_COM','wo.REG_EJECUTIVO_FIES')
            ->leftJoin('WEBVPC_FIES_OPERACION as WO','WRF.REG_EJECUTIVO_FIES_BACKUP', '=', 'WO.REG_EJECUTIVO_FIES')
            ->leftJoin('WEBVPC_FIES_PROSPECTOS as WL','WL.COD_OPERACION', '=', 'WO.COD_OPERACION')
            ->leftJoin('WEBVPC_FIES_ESTACION as WFE','WFE.ID_ESTACION', '=', 'WO.ULTIMO_ESTACION')
            ->leftJoin('WEBVPC_FIES_OP_INFORMACION as WOID',function($join){
                    $join->on('WL.COD_OPERACION', '=', 'WOID.COD_OPERACION');
                    $join->on('WOID.TIPO','=',DB::raw("'DESEMBOLSO'"));
                    $join->on('WOID.FLG_ULTIMO','=',DB::raw(1));
            })
            ->leftJoin('WEBVPC_FIES_OP_INFORMACION as WOIC',function($join){
                    $join->on('WL.COD_OPERACION', '=', 'WOIC.COD_OPERACION');
                    $join->on('WOIC.TIPO','=',DB::raw("'COMISION'"));  
                    $join->on('WOIC.FLG_ULTIMO','=',DB::raw(1));
            })
            ->leftJoin('ARBOL_DIARIO as AD','WL.REG_EJECUTIVO_NEGOCIO', '=', 'AD.LOGIN')  
            ->join('WEBVPC_USUARIO as WU',function($join){
                    $join->on('WU.REGISTRO', '=', 'WO.REG_EJECUTIVO_FIES');         
            })
            ->distinct();
          


        if ($ejecutivo){
            $sql = $sql->where('WRF.REG_EJECUTIVO_FIES','=',$ejecutivo);
        }

        if (isset($filtros['estacion'])){
            $sql = $sql->where('WO.ULTIMO_ESTACION','=',$filtros['estacion']);
        }
        if (isset($filtros['documento'])){
            $sql = $sql->where('WL.NUM_DOC','=',$filtros['documento']);
        }
        if (isset($filtros['grupo_zonal'])){
            $sql = $sql->where('AD.NOMBRE_ZONAL2','=',$filtros['grupo_zonal']);
        }
        if (isset($filtros['ejeFies'])){
            $sql = $sql->where('WO.REG_EJECUTIVO_FIES','=',$filtros['ejeFies']);
        }
        if (isset($filtros['idEstacion'])){
            $sql = $sql->where('WO.ULTIMO_ESTACION','=',$filtros['idEstacion']);
        }

        // Si no se ha seleccionado un campo para ordenar, se da el ordenamiento por default:
        if (!$orden){
            $sql =$sql
                    // 1. Se ordena si la cita esta vencida
                    ->orderBy(DB::raw('WOID.MES_PROBABLE'),'DESC')
                    // 2. Se ordena por la proxima cita agendada
                    ->orderBy(DB::raw('WOIC.MES_PROBABLE'),'ASC');
                    // 3. Se ordena por propension
        }else{
            $sql = $sql->orderBy($this->getOrderColumn($orden['sort']),$orden['order']);
        }

        return $sql;
    }


     function consulta($Operacion){
        $sql = $this->getOperacionEjecutivo(null);
        $sql->wheres = null;
        $sql->orders = null;
        return $sql->where('WL.NUM_DOC','=',$Operacion)->orderBy('WL.PERIODO','DESC');
    }

    function getGrupoZonalByEjecutivo($en){
        $sql = DB::table('WEBVPC_FIES_PROSPECTOS AS WFP')
                    ->select('WFP.GRUPO_ZONAL')
                    ->leftJoin('WEBVPC_FIES_OPERACION as WO','WFP.COD_OPERACION', '=', 'WO.COD_OPERACION')
                    ->distinct();
            if (isset($en)){
                     $sql=$sql->where('wo.REG_EJECUTIVO_FIES',$en);
                    }
                    
        return $sql;
    }

    function getRelacionFiesByEjecutivo($en){
        $sql = DB::table('WEBVPC_RELACION_EJE_FIES AS WRF')
                    ->select('WU.NOMBRE','WU.REGISTRO')
                     ->leftJoin('WEBVPC_USUARIO as WU','WRF.REG_EJECUTIVO_FIES_BACKUP', '=', 'WU.REGISTRO')
                     ->distinct();
                     if (isset($en)){
                        $sql=$sql->where('WRF.REG_EJECUTIVO_FIES',$en);
                     }
                    
        return $sql;
    }



     function getResumenByEjecutivoFies($en ,$rol){
         $sql = DB::table('WEBVPC_FIES_OPERACION AS WFO')
            ->select(       
                    DB::raw('SUM(CASE WHEN ID_ESTACION=1 THEN 1 ELSE 0 END ) PIPELINE'),
                    DB::raw('SUM(CASE WHEN ID_ESTACION=2 THEN 1 ELSE 0 END ) COTIZACION'),
                    DB::raw('SUM(CASE WHEN ID_ESTACION=3 THEN 1 ELSE 0 END ) FIES'),
                    DB::raw('SUM(CASE WHEN ID_ESTACION=4 THEN 1 ELSE 0 END ) RIESGOS'),
                    DB::raw('SUM(CASE WHEN ID_ESTACION=5 THEN 1 ELSE 0 END ) APROBADO'),
                    DB::raw('SUM(CASE WHEN ID_ESTACION=6 THEN 1 ELSE 0 END ) DESEMBOLSADO'),
                    DB::raw('SUM(CASE WHEN ID_ESTACION=7 THEN 1 ELSE 0 END ) TERMINADO'),
                    DB::raw('SUM(CASE WHEN ID_ESTACION=8 THEN 1 ELSE 0 END ) PERDIDA')
            
            )
            ->leftJoin('WEBVPC_FIES_ESTACION as WE', function($join){
                    $join->on('WE.ID_ESTACION', '=', 'WFO.ULTIMO_ESTACION');
            });
          
        if ($rol=="11"){
            $sql = $sql->where('WFO.REG_EJECUTIVO_FIES',$en);
        }
       \Debugbar::info($rol=="11");
       \Debugbar::info($en);
           \Debugbar::info($sql);
        return $sql;
    }


    function getOrderColumn($field){
        switch ($field) {
                case 'operacion':
                    return 'WL.NOMBRE_CLIENTE';
                case 'ejeFies':
                    return 'WU.NOMBRE';
                case 'estacion':
                    return 'WFE.NOMBRE';
                case 'grupo_zonal':
                    return 'WL.GRUPO_ZONAL';
                case 'nomCliente':
                    return 'WL.NOMBRE_CLIENTE';
                case 'mesComi':
                    return 'WOIC.MONTO';
                case 'mesDesem':
                    return 'WOID.MONTO';
                    

            };
    }
    function setOperaciones($field){
        switch ($field) {
                case 'operacion':
                    return 'WL.NOMBRE_CLIENTE';
                case 'ejeFies':
                    return 'WU.NOMBRE';
                case 'estacion':
                    return 'WFE.NOMBRE';
                case 'grupo_zonal':
                    return 'WL.GRUPO_ZONAL';
                case 'nomCliente':
                    return 'WL.NOMBRE_CLIENTE';
                case 'mesComi':
                    return 'WOIC.MONTO';
                case 'mesDesem':
                    return 'WOID.MONTO';
                    
                    

            };
    }
        function setNuevaOperacion($operacion){
               DB::beginTransaction();
                    $status = true;
                    try {                        
                        DB::table('WEBVPC_FIES_OPERACION')->insert($operacion);
                        DB::commit();
                    } catch (\Exception $e) {
                        Log::error('BASE_DE_DATOS|' . $e->getMessage());
                        $status = false;
                        DB::rollback();
                    }
                    return $status;
    }    
}