<?php
namespace App\Model\fies;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;


class OperacionDias extends Model

{
	 protected $table = 'WEBVPC_FIES_OP_DIAS';

	 protected $primaryKey =['COD_OPERACION'];

	 public $timestamps = false;

     protected $guarded = [
    'COD_OPERACION',
    'FECHA_REGISTRO',
    'TIEMPO_FIES',
    'TIEMPO_CLIENTE',
    'TIEMPO_BANCA',
    'TIEMPO_RIESGOS'
        ];


    function getDiasOperacion ($codOperacion) {
         $sql = DB::table('WEBVPC_FIES_OP_DIAS AS WFEA')
                    ->select('WFEA.TIEMPO_FIES','WFEA.TIEMPO_CLIENTE','WFEA.TIEMPO_BANCA','WFEA.TIEMPO_RIESGOS')  
                    ->where('WFEA.COD_OPERACION ',$codOperacion);         
        return $sql;      
     }

    function registrarDiasOperacion ($codOperacion,$diasOperacion) {
            
         $status = true;
        try {
                // Quita el flag ultimo a a las gestiones anteriores
                DB::table('WEBVPC_FIES_OP_DIAS')
                ->where('COD_OPERACION ','=',$codOperacion)
                ->update($diasOperacion);
                          
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            \Debugbar::info($status);
            DB::rollback();
        }
        return $status;

     }
}