<?php
namespace App\Model\fies;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;


class OperacionesProspectos extends Model

{
  protected $table = 'WEBVPC_FIES_PROSPECTOS';

  protected $primaryKey =['COD_OPERACION'];

  public $timestamps = false;

  protected $guarded = [
    'COD_OPERACION',
    'COD_UNICO',
    'NUM_DOC',
    'NOMBRE_CLIENTE',
    'GRUPO_ECONOMICO',
    'SECTOR',
    'BANCA',
    'GRUPO_ZONAL',
    'SEGMENTO',
    'REG_EJECUTIVO_NEGOCIO',
];

function actualizar($codOperacion,$operacionProspecto ){
    DB::beginTransaction();
    $status = true;
    try {
        DB::table('WEBVPC_FIES_PROSPECTOS')
        ->where('COD_OPERACION', $codOperacion)
        ->update(array_except($operacionProspecto, ['COD_OPERACION']));
        DB::commit();
    } catch (\Exception $e) {
        Log::error('BASE_DE_DATOS|' . $e->getMessage());
        $status = false;
        DB::rollback();
    }
    return $status;

}

     /**
     * Reprogramar una Cita
     *
     * @param  int $id Id de la cita a reprogramar
     * @param  string $codOperacion Datos de la cita a reprogramar
     * @return table Datos históricos del estado de la cita
     */

     function getOperacionDatosGenerales ($codOperacion) {
       $sql = DB::table('WEBVPC_FIES_PROSPECTOS AS WFP')
       ->select('WFP.FECHA_REGISTRO','WFP.COD_OPERACION','WFP.NUM_DOC' ,'WFP.NOMBRE_CLIENTE','WFP.COD_UNICO','WFP.GRUPO_ECONOMICO',
        'WFP.SECTOR','AD.BANCA','AD.NOMBRE_ZONAL2 AS GRUPO_ZONAL','WFP.SEGMENTO','AD.ENCARGADO AS NOM_EJECUTIVO_NEGOCIO','WFP.REG_EJECUTIVO_NEGOCIO',
        'WFO.PRODUCTO','WFE.NOMBRE AS  ULTIMO_ESTACION','WFE.ID_ESTACION')          
       ->leftJoin('ARBOL_DIARIO as AD','WFP.REG_EJECUTIVO_NEGOCIO', '=', 'AD.LOGIN')  
       ->leftJoin('WEBVPC_FIES_OPERACION as WFO','WFO.COD_OPERACION', '=', 'WFP.COD_OPERACION')  
       ->leftJoin('WEBVPC_FIES_ESTACION as WFE','WFE.ID_ESTACION', '=', 'WFO.ULTIMO_ESTACION')          
       ->where('WFP.COD_OPERACION',$codOperacion);
       
       return $sql;                   
   }
   


   function registrarOperacion ($prospecto,$fecha ) {

      DB::beginTransaction();
      $status = true;
      try {
        $id = DB::table('WEBVPC_FIES_PROSPECTOS')->insertGetId($prospecto);
        DB::table('WEBVPC_FIES_OP_DIAS')->insert(['FECHA_REGISTRO' => $fecha, 'COD_OPERACION' =>$id,
         'TIEMPO_FIES'=>0,'TIEMPO_CLIENTE'=>0,'TIEMPO_BANCA'=>0,'TIEMPO_RIESGOS'=>0]);
        
        DB::commit();
          return $id; 
        
    } catch (\Exception $e) {
        Log::error('BASE_DE_DATOS|' . $e->getMessage());
        $status = false;
        DB::rollback();
         
    }
                
}


function getUltimaEstacion ($codOperacion) {
   $sql = DB::table('WEBVPC_FIES_OP_EST_ATRIBUTO AS WFEA') 
   ->where('FLG_ULTIMO',DB::raw(1))                         
   ->where('WFEA.COD_OPERACION',$codOperacion) 
   ->where('WFEA.ID_ESTACION','<',6) 
   ->max('ID_ESTACION');       
   
   
   return $sql;                   
}

}
