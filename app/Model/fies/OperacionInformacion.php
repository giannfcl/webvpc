<?php
namespace App\Model\fies;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;


class OperacionInformacion extends Model

{
	 protected $table = 'WEBVPC_FIES_OP_INFORMACION';

	 protected $primaryKey =['COD_OPERACION'];

	 public $timestamps = false;

     protected $guarded = [
    'COD_OPERACION',
    'TIPO',
    'MONEDA',
    'MONTO',
    'MES_PROBABLE',    
    'FLG_ULTIMO'
     ];

        function getoOperacionDesembolso($codOperacion,$tipoCronograma){
            //\Debugbar::info("aca");
            return $sql = DB::table('WEBVPC_FIES_OP_INFORMACION AS WFOI')
                    ->select('WFOI.COD_OPERACION','WFOI.TIPO','WFOI.MONEDA','WFOI.MONTO','WFOI.MES_PROBABLE','WFOI.FLG_ULTIMO')
                    ->where('WFOI.COD_OPERACION','=',$codOperacion)
                    ->where('WFOI.TIPO','=',$tipoCronograma)                                   
                    ->where('WFOI.FLG_ULTIMO','=',DB::raw(1))                                   
                    ->first();

        }

    function updateOperacion($data){            
        DB::beginTransaction();
        $status = true;
        try {            
            DB::table('WEBVPC_FIES_OP_INFORMACION')                    
                    ->where('COD_OPERACION','=',$data['COD_OPERACION'])
                    ->where('TIPO','=',$data['TIPO'])   
                    ->update(['FLG_ULTIMO' => DB::raw(0)]);
            DB::table('WEBVPC_FIES_OP_INFORMACION')->insert($data);
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
     }


     function registrarNuevoInfoProducto($data){            
        DB::beginTransaction();
        $status = true;
        try {                        
            DB::table('WEBVPC_FIES_OP_INFORMACION')->insert($data);
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
     }
}