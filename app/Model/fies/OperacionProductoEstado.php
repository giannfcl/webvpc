<?php
namespace App\Model\fies;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;




class OperacionProductoEstado extends Model{

   protected $table = 'WEBVPC_FIES_OPERACIÓN';

	 protected $primaryKey =['COD_OPERACION'];

	 public $timestamps = false;

     protected $guarded = [
    'FECHA_REGISTRO	',
    'COD_OPERACION',
    'PRODUCTO',
    'REG_EJECUTIVO_FIES',
    'ULTIMO_ESTACION',
     ];





 function actualizarProducto($codOperacion,$operacionProspecto ){
        DB::beginTransaction();
        $status = true;
        try {
            DB::table('WEBVPC_FIES_OPERACION')
            ->where('COD_OPERACION', $codOperacion)
            ->update(array_except($operacionProspecto, ['COD_OPERACION']));
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;

    }


 function getProductoEstado ($codOperacion) {
         $sql = DB::table('WEBVPC_FIES_OPERACION AS WFO')
                    ->select('WFO.FECHA_REGISTRO','WFO.COD_OPERACION','WFO.PRODUCTO','WFO.REG_EJECUTIVO_FIES','WFE.NOMBRE AS ULTIMO_ESTACION') 
                    ->leftJoin('WEBVPC_FIES_ESTACION as WFE','WFO.ULTIMO_ESTACION', '=', 'WFE.ID_ESTACION')   
                    ->where('WFO.COD_OPERACION',$codOperacion);    
        return $sql;                   
    }

}