<?php
namespace App\Model;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Jenssegers\Date\Date as Carbon;

class Landing extends Model
{

	static function getClientesLanding($data)
	{
		return DB::table("WEBVPC_CONSULTA_CLIENTE_COVID19")
				->select (DB::Raw('CONVERT(VARCHAR,FECHA_REGISTRO,20) AS FECHA'),DB::Raw("ISNULL(NOMBRE_COMPLETO,'') AS NOMBRE_COMPLETO"),"NUM_DOC","ID",DB::Raw("ISNULL(OTROS_COMENTARIOS,'') AS OTROS_COMENTARIOS"),DB::Raw("ISNULL(ACUERDOS,'') AS ACUERDOS"),DB::Raw("ISNULL(CONSULTA,'') AS CONSULTA"),"REGISTRO_EN","FECHA_GESTION",DB::Raw("ISNULL(EMAIL,'') AS EMAIL"),DB::Raw("ISNULL(CELULAR,'') AS CELULAR"),DB::Raw("ISNULL(ASUNTO,'') AS ASUNTO"))
				->get();
	}

	static function gestionarLanding($data,$actualizar)
	{
		DB::beginTransaction();
        $status = true;
        try {
            DB::table('WEBVPC_CONSULTA_CLIENTE_COVID19')
                ->where('ID','=',$data['formid'])
                ->update($actualizar);
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }

        return $status;
	}
}

?>