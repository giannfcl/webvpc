<?php
namespace App\Model\cmpemergencia;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class Maestras extends Model

{

    public function getProfesiones(){

        $sql = DB::table('WEBVPC_CMPEMERGENCIA_PROFESION AS WCG')
            ->select('WCG.*');
        return $sql;
    }

    public function getGradosInstruccion(){
        $sql = DB::table('WEBVPC_CMPEMERGENCIA_GRADO_INSTRUCCION AS WCG')
            ->select('WCG.*');
        return $sql;
    }

}