<?php
namespace App\Model\cmpemergencia;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class Subasta extends Model

{

    public function getTasas($monto,$cobertura,$banca){

        $sql=DB::table('MKT_SUBASTA_DESEMBOLSO_BANCA_AG as SUB')
            ->select(DB::raw('CONVERT(DECIMAL(8,2),TASA_REPO*100) as tasa'),'SUB.FECHA_LIMITE')
            ->where('SUB.COBERTURA',$cobertura)
            ->where('SUB.MONTO_DISPONIBLE','>=',$monto)
            ->where('SUB.FLG_ESTADO',1)
            ->whereIn('SUB.BANCA',$banca)
            ->orderBy('SUB.FECHA_LIMITE','asc')
            ->orderBy('SUB.FECHA_SUBASTA','asc');
        return $sql;
        
    }

   
}