<?php
namespace App\Model\cmpemergencia;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;
use App\Entity\cmpemergencia\Etapa as Etapa;
use App\Entity\cmpemergencia\Lead as ELead;


class Lead extends Model

{
    public $_message;

    public function direccionByRucLeads($ruc=null){
        $sql=DB::select("exec USP_CMPEMERGENCIA_LEADS_SEL $ruc ");
       return $sql;
    }

    public function datosLlenado($ruc=null){
        DB::beginTransaction();
        $data = null;
        try {
            $data = DB::table('WEBVPC_CMPEMERGENCIA_LEADS as WCL')
                ->where('NUM_RUC',$ruc)->get()->first();
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            DB::rollback();
        }
        return $data;
    }

    public function gestionar($lead,$etapa,$ejecutivoLibre = null,$solicitudCredito = null){

        DB::beginTransaction();
        $status = true;

        try {

            $id = DB::table('WEBVPC_CMPEMERGENCIA_LEADS as WCL')
                ->leftJoin("WEBVPC_USUARIO AS WU",function($join){
                    $join->on("WCL.ASIGNADO_EJECUTIVO","=","WU.REGISTRO");
                })
                ->where('NUM_RUC',$lead['NUM_RUC'])
                ->update(array_except($lead, ['NUM_RUC']));

            if($etapa){
                DB::table('WEBVPC_CMPEMERGENCIA_LEAD_ETAPA')->insert($etapa);
            }

            if($ejecutivoLibre){
                DB::table('WEBVPC_CMPEMERGENCIA_LEAD_RIESGOS')->insert($ejecutivoLibre);
            }

            if($solicitudCredito){
                $subasta = DB::table('MKT_SUBASTA_DESEMBOLSO_BANCA_AG')
                ->select('NRO_SUBASTA','BANCA')
                ->where('COBERTURA',$solicitudCredito['COBERTURA'])
                ->where('MONTO_DISPONIBLE','>=',$solicitudCredito['MONTO_SOLICITADO'])
                ->where('FLG_ESTADO','1')
                ->whereIn('BANCA',['BPE','RETAIL'])
                ->where(DB::raw('CONVERT(DECIMAL(8,2),TASA_REPO*100)'),$solicitudCredito['TASA'])
                ->orderBy('FECHA_SUBASTA','asc')
                ->first();

                if ($subasta){
                    $result = DB::table('MKT_SUBASTA_DESEMBOLSO_BANCA_AG')
                    ->where('NRO_SUBASTA',$subasta->NRO_SUBASTA)
                    ->where('MONTO_DISPONIBLE','>=',$solicitudCredito['MONTO_SOLICITADO'])
                    ->where('FLG_ESTADO','1')
                    ->where('BANCA',$subasta->BANCA)
                    ->update(['MONTO_DISPONIBLE' => DB::raw('MONTO_DISPONIBLE - ' . $solicitudCredito['MONTO_SOLICITADO'])]);
                    if ($result === 1){
                        $solicitudCredito['SUBASTA'] = $subasta->NRO_SUBASTA;
                        $solicitudCredito['SUBASTA_BANCA'] = $subasta->BANCA;
                        DB::table('WEBVPC_CMPEMERGENCIA_SOLICITUD_CREDITO')->insert($solicitudCredito);
                    }else{
                        $this->_message = 'La subasta ya no se encuentra disponible, por favor selecciona una nueva tasa';
                        DB::rollback();
                        return false;
                    }
                }else{
                    DB::rollback();
                    $this->_message = 'La subasta ya no se encuentra disponible, por favor selecciona una nueva tasa';
                    return false;
                }
            }

            DB::commit();
            return true;
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;

    }


    function actualizarlead($actualizar,$numruc){

        DB::beginTransaction();
        $status = true;

        try {

            DB::table('WEBVPC_CMPEMERGENCIA_LEADS')
                ->where('NUM_RUC',$numruc)
                ->update($actualizar);

            DB::commit();
            return true;
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;

    }

    public function listar($etapa,$banca,$ejecutivo,$etapas=[]){
        $sql=DB::table('WEBVPC_CMPEMERGENCIA_LEADS AS WCL')
            ->select(self::getColumnasByEtapa($etapa))
            ->join('WEBVPC_CMPEMERGENCIA_ETAPA as WCE', function($join) {
                $join->on('WCL.ETAPA_ID', '=', 'WCE.ETAPA_ID');
            })
            ->leftJoin('WEBVPC_CMPEMERGENCIA_GESTION as WCG', function($join) {
                $join->on('WCL.CONTACTO_RESULTADO', '=', 'WCG.GESTION_ID');
            })
            ->leftJoin('WEBVPC_USUARIO as WU', function($join) {
                $join->on('WU.REGISTRO', '=', 'WCL.ASIGNADO_EJECUTIVO');
            });

        if (count($etapas)>0) {
            $sql=$sql->whereIn('WCL.ETAPA_ID',$etapas);
        }else{
            if($etapa){
                $sql=$sql->where('WCL.ETAPA_ID',$etapa);
            }
        }

        if($banca){
            if (is_array($banca)){
                $sql=$sql->whereIn('WCL.BANCA',$banca);
            }else{
                $sql=$sql->where('WCL.BANCA',$banca);
            }
        }

        if(!empty($ejecutivo)){
            $sql=$sql->where('WCL.ASIGNADO_EJECUTIVO',$ejecutivo);
        }
        // dd($sql->toSql());
        return $sql;
    }


    /*Para Backoffice*/
     public function updateFlag($ruc, $flag, $hoy,$leadetapa){
        DB::beginTransaction();
        $status = true;

        try {
            //dd($ruc, $flag, $hoy,$leadetapa);
            DB::table('WEBVPC_CMPEMERGENCIA_LEADS')
                ->where('NUM_RUC', $ruc)
                ->update(['FLAG_REPORTE_TRIBUTARIO'=> $flag, 'FECHA_ASIGNADO_BACKOFFICE'=>$hoy]);

            if ($leadetapa){
                DB::table('WEBVPC_CMPEMERGENCIA_LEAD_ETAPA')->insert($leadetapa);
            }

            DB::commit();
            return true;
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;

    }



    public function listarBpe($etapa,$banca,$ejecutivo,$data,$aceptaroncuentasueldo=false,$aceptaroncobrosimple=false){

        $sql = $this->listar($etapa,$banca,$ejecutivo)
            ->leftJoin('WEBVPC_CMPEMERGENCIA_GESTION as WCGR', function($join) {
                $join->on('WCL.RIESGOS_GESTION', '=', 'WCGR.GESTION_ID');
            })
            ->leftjoin(db::Raw("(SELECT DISTINCT ID_CENTRO,CENTRO FROM WEBVPC_TIENDA) AS WT"),function($join) {
                $join->on('WU.ID_CENTRO', '=', 'WT.ID_CENTRO');
            })
            ->select('WCL.SUELDO_PROMEDIO','WCL.NRO_TRABAJADORES_QUINTA_CATEGORIA','WCL.MONTO_PAGO_ESSALUD','WCL.CORREO','WCL.TELEFONO1'
                ,'WCL.RRLL_APELLIDOS_2','WCL.RRLL_NOMBRES_2','WCL.RRLL_DOCUMENTO_2','WCL.RRLL_APELLIDOS','WCL.RRLL_NOMBRES','WCL.RRLL_DOCUMENTO'
                ,'WCL.NUM_RUC','WCL.NOMBRE','WCL.FLUJO','WCL.CANAL_ACTUAL'
                ,'WCL.SOLICITUD_PLAZO','WCL.SOLICITUD_VOLUMEN_VENTA','WCL.SOLICITUD_OFERTA','WCL.SOLICITUD_ESSALUD'
                ,'WCE.ETAPA_NOMBRE','WCL.CAMPANIA','WCL.CONTACTO_RESULTADO','WCG.GESTION_NOMBRE','WCE.ETAPA_ID','WCL.SOLICITUD_GRACIA'
            ,'WCL.PRIORIDAD','WCL.RIESGOS_GESTION','WCL.FLG_TOPE_MAXIMO','WCL.TOPE_MAXIMO','WCL.PRIORIDAD_BPE'
            ,'WCGR.GESTION_NOMBRE AS RIESGOS_GESTION_NOMBRE','WCL.FLAG_REPORTE_TRIBUTARIO AS FLAG'
            ,DB::Raw("CAST(CONTACTO_FECHA AS DATE) AS FECHA_GESTION")
            ,'WCL.FLG_COBRO_SIMPLE','WCL.CS_CANT_CLIENTES_PAGAN','WCL.CS_CONTROL_COBRANZA','WCL.CS_TICKET_PROM_COBRANZA'
            ,'WU.NOMBRE AS NOMBRE_EJECUTIVO','WT.CENTRO');
        if(isset($data['id_zona'])){
            $sql = $sql->where("WU.ID_ZONA","=",$data['id_zona']);
        }
        if(isset($data['id_centro'])){
            $sql = $sql->where("WU.ID_CENTRO","=",$data['id_centro']);
        }
        if(isset($data['id_tienda'])){
            $sql = $sql->where("WU.ID_TIENDA","=",$data['id_tienda']);
        }
        if(isset($data['gestion_en'])){
            $sql = $sql->where("WCL.CONTACTO_RESULTADO","=",$data['gestion_en']);
        }
        if(isset($data['gestion_r'])){
            $sql = $sql->where("WCL.RIESGOS_GESTION","=",$data['gestion_r']);
        }
        if(isset($data['prioridad'])){
            if($data['prioridad']==ELead::fechaPrimerContacto[0]){
                $sql = $sql->whereNull('WCL.FECHA_PRIMER_CONTACTO');
            }else if($data['prioridad']==ELead::fechaPrimerContacto[1]){
                $sql = $sql->whereBetween(DB::raw("DATEDIFF(day,GETDATE(),WCL.FECHA_PRIMER_CONTACTO)"),[0,1]);
            }else if($data['prioridad']==ELead::fechaPrimerContacto[2]){
                $sql = $sql->whereBetween(DB::raw("DATEDIFF(day,GETDATE(),WCL.FECHA_PRIMER_CONTACTO)"),[2,5]);
            }else if($data['prioridad']==ELead::fechaPrimerContacto[3]){
                $sql = $sql->whereBetween(DB::raw("DATEDIFF(day,GETDATE(),WCL.FECHA_PRIMER_CONTACTO)"),[5,8]);
            }else if($data['prioridad']==ELead::fechaPrimerContacto[4]){
                $sql = $sql->whereBetween(DB::raw("DATEDIFF(day,GETDATE(),WCL.FECHA_PRIMER_CONTACTO)"),[8,11]);
            }else if($data['prioridad']==ELead::fechaPrimerContacto[5]){
                $sql = $sql->where(DB::raw("DATEDIFF(day,GETDATE(),WCL.FECHA_PRIMER_CONTACTO)"),">",11);
            }
        }
        if(isset($data['traderdesembolso'])){
            $sql = $sql->where("WCL.FLG_DESTINO_COMPRA_DOLARES","=",$data['traderdesembolso']);
        }
        if(isset($data['clientescanal'])){
            $sql = $sql->where("WCL.CLIENTE_CANAL","=",$data['clientescanal']);
        }
        if($aceptaroncuentasueldo){
            $sql = $sql->where("WCL.FLG_ACEPTO_CUENTA_SUELDO","=",1);
        }
        if($aceptaroncobrosimple){
            $sql = $sql->whereIn("WCL.FLG_COBRO_SIMPLE",[1,0]);
        }

        return $sql;
    }

    public function listarRiesgos($ejecutivo, $gestion,$ruc,$estado = null){
        $sql = $this->listar(null,null,null)
        ->select('WCL.NUM_RUC','WU.ID_CENTRO','WCL.NOMBRE','wCL.RRLL_DOCUMENTO','wCL.RRLL_NOMBRE'
                ,'WU.NOMBRE as NOMBRE_EJECUTIVO','WCL.CAMPANIA','WCL.BANCA','WCL.SOLICITUD_OFERTA'
                ,'WCL.SOLICITUD_PLAZO','WCL.SOLICITUD_GRACIA','WCL.TIPO_CLIENTE'
                ,'WCL.RIESGOS_GESTION','WCL.RIESGOS_GESTION_SUB','WCGR.GESTION_NOMBRE','WCL.ETAPA_ID'
                ,'WCL.SOLICITUD_ESSALUD','WCL.SOLICITUD_VOLUMEN_VENTA','WCL.RIESGOS_COMENTARIO','WCL.MONTO_PRESTAMO_REACTIVA_1','WCL.RIESGOS_MONTO'
                ,'WCL.BANCO_REACTIVA_1','WCL.GIRO_DESCRIPCION',DB::raw("CASE WHEN FLAG_REPORTE_TRIBUTARIO = 1 THEN 'Actual' ELSE 'Anterior' END as TIPO_PROCESO"))
        ->join('WEBVPC_CMPEMERGENCIA_LEAD_RIESGOS as WCLR', function($join) {
            $join->on('WCL.NUM_RUC', '=', 'WCLR.NUM_RUC');
        })
        ->leftJoin('WEBVPC_CMPEMERGENCIA_GESTION as WCGR', function($join) {
            $join->on('WCL.RIESGOS_GESTION', '=', 'WCGR.GESTION_ID');
        })
        ->leftJoin('WEBVPC_CMPEMERGENCIA_GESTION_SUB as WCGS', function($join) {
            $join->on('WCL.RIESGOS_GESTION_SUB', '=', 'WCGS.GESTION_SUB_ID');
        })
        ->whereIn('CONTACTO_RESULTADO',$gestion);


        if($ejecutivo){
            $sql = $sql->where('WCLR.REGISTRO_RIESGOS',$ejecutivo);
        }

        if ($ruc){
            $sql = $sql->where('WCL.NUM_RUC',$ruc);
            }

        if ($estado){
            $sql = $sql->where(DB::raw('ISNULL(WCL.RIESGOS_GESTION,19)'),$estado);
        }

        return $sql;
    }


    function listarValidacionDocumentos($etapa,$banca,$zonal,$ejecutivo){

        $sql = $this->listar($etapa,$banca,$ejecutivo);
        $sql->wheres = [];

        $sql = $sql->leftJoin('WEBVPC_CMPEMERGENCIA_GESTION as WCGR', function($join) {
                $join->on('WCL.VALIDACION_DOCUMENTACION_GESTION', '=', 'WCGR.GESTION_ID');
            })
        ->where(function($q) use($etapa){
                $q->where('WCL.ETAPA_ID', $etapa);
                $q->orWhereNotNull('WCL.VALIDACION_DOCUMENTACION_GESTION');
            })
        ->where('WCL.BANCA','!=',DB::raw("'BPE'"));

        if($banca){
            $sql=$sql->where('WCL.BANCA',$banca);
        }

        //\Log::alert($sql->wheres);

        if($ejecutivo){
            $sql=$sql->where('WCL.ASIGNADO_EJECUTIVO',$ejecutivo);
        }

        //dd($zonal);
        if ($zonal){
            $sql = $sql->where('WU.ID_ZONA',$zonal);
        }
        return $sql;
    }

    function listarSolEmp($user,$data = null){

        $sql = DB::table('WEBVPC_CMPEMERGENCIA_VISTA_TOTAL_REACTIVA as WCVTR')
        ->select('WCVTR.*'
        ,DB::raw('CASE WHEN WCSP.NUM_RUC IS NOT NULL THEN 1 ELSE 0 END PRIORIZACION_FLAG')
        ,DB::raw("CASE WHEN WCVTR.FECHA_CONTACTO >= CONVERT(DATE,WCSP.FECHA_REGISTRO) THEN 1 ELSE 0 END PRIORIZACION_ESTADO")
        )
        ->leftJoin(DB::Raw("(SELECT * FROM ( SELECT *,ROW_NUMBER() OVER (PARTITION BY NUM_RUC ORDER BY FECHA_REGISTRO DESC) ORDEN FROM WEBVPC_CMPEMERGENCIA_SOLEMP_PRIORIZACION ) A WHERE A.ORDEN = 1) as WCSP"), function($join) {
            $join->on('WCVTR.NUM_RUC', '=', 'WCSP.NUM_RUC');
        });
        return $sql;
    }

    public function getLead($ruc,$ejecutivo=null,$etapa=null,$banca=null){

        $sql = $this->listar($etapa,$banca,$ejecutivo)
            ->leftJoin('WEBVPC_CMPEMERGENCIA_GESTION_SUB as WCGS', function($join) {
                $join->on('WCL.RIESGOS_GESTION_SUB', '=', 'WCGS.GESTION_SUB_ID');
            })
            ->leftJoin('WEBVPC_CMPEMERGENCIA_FOTO_RP1 as WCGUF', function($join) {
                $join->on('WCL.NUM_RUC', '=', 'WCGUF.NUM_RUC');
            })
            ->leftJoin('WEBVPC_CMPEMERGENCIA_LEAD_RIESGOS as WCLR', function($join) {
                $join->on('WCLR.NUM_RUC', '=', 'WCL.NUM_RUC');
            })
            ->leftJoin('WEBVPC_USUARIO as WUR', function($join) {
                $join->on('WCLR.REGISTRO_RIESGOS', '=', 'WUR.REGISTRO');
            })
            ->select('WCL.*','WCE.ETAPA_NOMBRE'
                    ,'WCGUF.ETAPA_ACTUAL as R1_ETAPA_ACTUAL'
                    ,'WCGUF.ETAPA_NOMBRE as R1_ETAPA_NOMBRE'
                    ,'WCGUF.TASA as R1_TASA'
                    ,'WCGUF.MONTO_CLIENTE as R1_MONTO'
                    ,'WCGUF.PLAZO as R1_PLAZO'
                    ,'WCGUF.GRACIA as R1_GRACIA'
                    ,DB::Raw('CONVERT(DATE,WCGUF.FECHA_GESTION) as R1_FECHA_CONTACTO')
                    ,DB::Raw("ISNULL(TASA_FINAL_CLIENTE,SOLICITUD_TASA) as TASACLIENTE")
                    ,'WUR.NOMBRE as ANALISTA_RIESGOS'
                    ,'WCGS.GESTION_SUB_NOMBRE')
            ->where('WCL.NUM_RUC',$ruc);
        // dd($sql->toSql());
        return $sql;
    }

    public function getByEtapa($ejecutivo,$activo=true,$etapa = null){
        $sql=DB::table('WEBVPC_CMPEMERGENCIA_ETAPA as WCL')
        ->select('WCL.ETAPA_ID','WCL.ETAPA_NOMBRE',DB::raw('count(WCE.NUM_RUC) as TOTAL'))
        ->leftJoin('WEBVPC_CMPEMERGENCIA_LEADS as WCE', function($join) use ($ejecutivo){
            $join->on('WCL.ETAPA_ID', '=', 'WCE.ETAPA_ID');
            //$join->on('WCE.BANCA', '=', DB::raw("'BE'"));
            if($ejecutivo){
                $join->on('WCE.ASIGNADO_EJECUTIVO', '=', DB::raw("'$ejecutivo'"));
            }
        });
        if($activo){
            $sql=$sql->where('WCL.FLG_ACTIVO',1);
        }
        if($etapa){
            $sql=$sql->where('WCL.ETAPA_ID',$etapa);
        }

        $sql=$sql->groupBy('WCL.ETAPA_ORDEN','WCL.ETAPA_ID','WCL.ETAPA_NOMBRE')
                ->orderBy('WCL.ETAPA_ORDEN')
                ->get();
        return $sql;
    }

    private static function getColumnasByEtapa($etapa){
        $columnas = [];
        switch ($etapa) {
            case Etapa::ASIGNADO:
                $columnas = [
                    'WCL.NUM_RUC','WCL.NOMBRE','WCL.FLUJO','WCL.CANAL_ACTUAL'
                    ,DB::Raw("(CASE WHEN WCL.CANAL_ACTUAL='WEB' THEN 2 WHEN WCL.CANAL_ACTUAL='RIESGOS' THEN 1 ELSE 0 END) AS ORDEN_CANAL")
                    ,'WCL.SOLICITUD_VOLUMEN_VENTA'
                    //,'WCL.SOLICITUD_ESSALUD'
                    ,'WCL.CONTACTO_RESULTADO'
                    ,'WCG.GESTION_NOMBRE'
                    ,'WCL.RIESGOS_MONTO'
                    ,DB::Raw("CAST(WCL.CEF_FECHA AS DATE) AS CEF_FECHA")
                    ,'WCL.CEF_VENTAS'
                    ,'WCL.CEF_TIPO'
                    ,'WCL.MONTO_PRESTAMO_REACTIVA_1'
                    ,DB::Raw("(WCL.MONTO_PRESTAMO_REACTIVA_1+WCL.RIESGOS_MONTO) AS MONTO_OFERTA_RIESGOS")
                ];
                break;
            case Etapa::CONTACTADO:
				$columnas = ['WCL.NOM','WCL.FLUJO'];
				break;
			case Etapa::INTERESADO:
                $columnas = [
                    'WCL.NUM_RUC','WCL.NOMBRE','WCL.FLUJO'
                    ,'WCL.CANAL_ACTUAL'
                    ,DB::Raw("(CASE WHEN WCL.CANAL_ACTUAL='WEB' THEN 2 WHEN WCL.CANAL_ACTUAL='RIESGOS' THEN 1 ELSE 0 END) AS ORDEN_CANAL")
                    ,'WCL.SOLICITUD_VOLUMEN_VENTA'
                    //,'WCL.SOLICITUD_ESSALUD'
                    ,'WCL.MONTO_PRESTAMO_REACTIVA_1'
                    ,'WCL.SOLICITUD_OFERTA'
                    ,'WCL.SOLICITUD_PLAZO'
                    ,'WCL.SOLICITUD_GRACIA'
                    ,DB::Raw("ISNULL(WCL.SOLICITUD_VOLUMEN_VENTA,0) AS SOLICITUD_VV")
                    ,DB::Raw("ISNULL(WCL.PRODUCTO_MONTO,0) AS OFERTA_REVISADA")
                    ,DB::Raw("ISNULL(WCL.SOLICITUD_OFERTA,0) AS OFERTA_SOLICITADA")
                    ,DB::Raw("ISNULL(WCL.RIESGOS_MONTO,0) AS MONTO_RIESGO")
                    ,DB::Raw("ISNULL(WCL.PRODUCTO_MONTO,0) AS MONTO_SOLICITADO")
                    ,DB::Raw("ISNULL(WCL.PRODUCTO_TASA,0) AS CONDICION")
                    ,DB::Raw("(CASE WHEN WCL.SOLICITUD_VOLUMEN_VENTA > WCL.SOLICITUD_ESSALUD  THEN WCL.SOLICITUD_VOLUMEN_VENTA ELSE WCL.SOLICITUD_ESSALUD END) AS MONTO_CALCULADO")
                    ,'WCL.FECHA_DESEMBOLSO_TENTATIVA'
                    ,'WCL.SOLICITUD_TASA'
                ];
                break;
			case Etapa::DOCUMENTOS_EVALUACION:
				$columnas = [
                    'WCL.NUM_RUC'
                    ,'WCL.NOMBRE'
                    ,'WCL.FLUJO'
                    ,'WCL.SOLICITUD_VOLUMEN_VENTA'
                    //,'WCL.SOLICITUD_ESSALUD'
                    ,'WCL.MONTO_PRESTAMO_REACTIVA_1'
                    ,'WCL.SOLICITUD_PLAZO'
                    ,'WCL.SOLICITUD_GRACIA'
                    ,'WCL.SOLICITUD_TASA'
                    ,'WCL.FECHA_DESEMBOLSO_TENTATIVA'
                    ,'WCL.RIESGOS_MONTO'
                    ,DB::Raw("(CASE WHEN WCL.DOCUMENTACION_PDTDJ = '1' THEN 'SI' ELSE 'NO' END) AS FLG_PDJT")
                    ,DB::Raw("(CASE WHEN WCL.FLG_REPORTE_TRIBUTARIO_TERCEROS = '1' THEN 'SI' ELSE 'NO' END) AS FLG_REPORTE_TRIBUTARIO_TERCEROS")
                    ,DB::Raw("(CASE WHEN WCL.DOCUMENTACION_ESSALUD= '1' THEN 'SI' WHEN WCL.DOCUMENTACION_ESSALUD= '2' THEN 'NO APLICA' ELSE 'NO' END) AS FLG_ESSALUD")
                    ,'WCL.DOCUMENTACION_ESSALUD'
                    ,'WCL.SOLICITUD_OFERTA'
                    ,DB::Raw("ISNULL(WCL.PRODUCTO_TASA,0) AS CONDICION")
                ];
				break;
			case Etapa::DERIVADO_GTP:
				$columnas = [
                    'WCL.NUM_RUC'
                    ,'WCL.NOMBRE'
                    ,'WCL.FLUJO'
                    ,'WCL.SOLICITUD_OFERTA'
                    ,DB::Raw("CAST(WCL.ASIGNADO_FECHA AS DATE) AS ASIGNADO_FECHA")
                    ,DB::Raw("CAST(WCL.DESEMBOLSO_FECHA AS DATE) AS DESEMBOLSO_FECHA")
                    ,'WCL.DESEMBOLSO_MONTO'
                ];
				break;
			case Etapa::DOCUMENTOS_DESEMBOLSO:
                $columnas = [
                    'WCL.NUM_RUC'
                    ,'WCL.NOMBRE'
                    ,'WCL.FLUJO'
                    ,'WCL.SOLICITUD_OFERTA'
                    ,'WCL.SOLICITUD_VOLUMEN_VENTA'
                    ,'WCL.SOLICITUD_ESSALUD'
                    ,'WCL.DOCUMENTACION_COMPLETA'
                    ,'WCL.TASA_FINAL_CLIENTE'
                    ,'WCL.FECHA_APROBACIO_SUBASTA'
                    ,'WCL.DESEMBOLSO_MONTO'
                    ,'WCL.SOLICITUD_GRACIA'
                    ,'WCL.SOLICITUD_PLAZO'
                    ,'WCL.ESTADO_FILE'
                ];
				break;
			case Etapa::DESEMBOLSADO:
                $columnas = [
                    'WCL.NUM_RUC'
                    ,'WCL.NOMBRE'
                    ,'WCL.WIO_PLAZO'
                    ,'WCL.WIO_GRACIA'
                    ,'DESEMBOLSO_FECHA'
                    ,DB::Raw("CAST(WCL.DESEMBOLSO_FECHA AS DATE) AS FORMAT_DESEMBOLSO_FECHA")
                    ,'WCL.DESEMBOLSO_MONTO'
                    ,'WCL.WIO_TASA'
                    ,'WCL.NUM_CREDITO'
                    ,'WCL.FECHA_APROBACIO_SUBASTA'
                ];
				break;
			case Etapa::RECHAZADO:
                $columnas = [
                     'WCL.NUM_RUC'
                    ,'WCL.NOMBRE'
                    ,'WCL.FLUJO'
                    ,'WCL.SOLICITUD_OFERTA'
                    ,'WCL.RECHAZO_RESPONSABLE'
                    ,DB::Raw("CAST(WCL.RECHAZO_FECHA AS DATE) AS RECHAZO_FECHA")
                    ,'WCL.RECHAZO_MOTIVO'
                ];
				break;
			case Etapa::REVISAR:
				$columnas = [
                    'WCL.NUM_RUC','WCL.NOMBRE','WCL.FLUJO','WCL.CANAL_ACTUAL'
                    ,DB::Raw("(CASE WHEN WCL.CANAL_ACTUAL='WEB' THEN 2 WHEN WCL.CANAL_ACTUAL='RIESGOS' THEN 1 ELSE 0 END) AS ORDEN_CANAL")
                    ,'WCL.SOLICITUD_VOLUMEN_VENTA'
                    ,'WCL.SOLICITUD_ESSALUD'
                    ,'WCL.CONTACTO_RESULTADO'
                    ,'WCL.MONTO_PRESTAMO_REACTIVA_1'
                    ,'WCG.GESTION_NOMBRE'
                    ,'WCL.RIESGOS_MONTO'
                    ,DB::Raw("CAST(WCL.CEF_FECHA AS DATE) AS CEF_FECHA")
                    ,'WCL.CEF_VENTAS'
                    ,'WCL.CEF_TIPO'
                    ,'WCL.SOLICITUD_OFERTA'
                    ,'WCL.PRODUCTO_MONTO'
                    ,'WCL.SECTORISTA_REGISTRO'
                    ,'WCL.SECTORISTA_NOMBRE'
                    ,'WCL.ID_ZONAl'
                    ,'WCL.NOMBRE_ZONAL'
                    ,'WCL.FLG_COMUNICO_RECHAZO'
                    ,'WCL.FLG_APROBACION'
                    ,'WCL.TASA_FINAL_CLIENTE'
                    ,'WCL.MOTIVO_OFERTA_BPE'
                ];
                break;
            case Etapa::ENVIO_SUBASTA:
                    $columnas = [
                        'WCL.NUM_RUC'
                        ,'WCL.NOMBRE'
                        ,'WCL.FLUJO'
                        ,'WCL.SOLICITUD_VOLUMEN_VENTA'
                        ,'WCL.SOLICITUD_ESSALUD'
                        ,'WCL.SOLICITUD_PLAZO'
                        ,'WCL.SOLICITUD_GRACIA'
                        ,'WCL.SOLICITUD_TASA'
                        ,'WCL.FECHA_DESEMBOLSO_TENTATIVA'
                        ,'WCL.SOLICITUD_OFERTA'
                        ,'WCL.ESTADO_SUBASTA'
                        ,'WCL.NROSUBASTA'
                        ,'WCL.TASA_MAXIMA_BCR'
                        ,'WCL.TASA_FINAL_CLIENTE'
                        ,'WCG.GESTION_NOMBRE'
                        ,'WCL.GESTION_SUBASTA'
                        ,DB::Raw("ISNULL(WCL.PRODUCTO_TASA,0) AS CONDICION")
                        ,'WCL.MONTO_PRESTAMO_REACTIVA_1'
                    ];
                    break;
            case Etapa::VALIDACION_DOCUMENTOS:
                $columnas=['WCL.NUM_RUC'
                ,'WCL.NOMBRE'
                ,'WCL.BANCA'
                ,'WCL.TASA_FINAL_CLIENTE'
                ,'WCL.SOLICITUD_OFERTA'
                ,'WU.NOMBRE as NOMBRE_EJECUTIVO'
                ,'WCL.VALIDACION_DOCUMENTACION_GESTION'
                ,'WCGR.GESTION_NOMBRE'
                ,'WCL.FECHA_APROBACIO_SUBASTA'
                ];
                break;
            case Etapa::WIO_INGRESADO:
                    $columnas=['WCL.NUM_RUC'
                    ,'WCL.NOMBRE'
                    ,'WCL.WIO_GRACIA'
                    ,'WCL.WIO_MONTO_SOLICITADO'
                    ,'WCL.WIO_PLAZO'
                    ,'WCL.WIO_TASA'
                    ,'WCL.WIO_FLAG_TIENE_LINEA'
                    ,'WCL.SUBASTA_DEADLINE'
                    ,'wCL.MONTO_PRESTAMO_REACTIVA_1'
                    ];
                    break;
            case Etapa::WIO_COFIDE:
                $columnas=[
                    'WCL.NUM_RUC'
                    ,'WCL.NOMBRE'
                    ,'WCL.TASA_FINAL_CLIENTE'
                    ,'WCL.WIO_GRACIA'
                    ,'WCL.WIO_MONTO_SOLICITADO'
                    ,'WCL.WIO_PLAZO'
                    ,'WCL.WIO_TASA'
                    ,'WCL.WIO_OBSERVACION'
                    ,DB::Raw("CASE WHEN WCL.WIO_OBSERVACION='1' THEN 'Monto prestamos supera ventas o Essalud'
                         WHEN WCL.WIO_OBSERVACION='2' THEN 'Sin subasta preasignada'
                         WHEN WCL.WIO_OBSERVACION='3' THEN 'Tasa ingresada mayor a tasa repo'
                         WHEN WCL.WIO_OBSERVACION='4' THEN 'Subasta Sin Numero de Operacion'
                         WHEN WCL.WIO_OBSERVACION='5' THEN 'Observado GTP'
                         WHEN WCL.WIO_OBSERVACION='6' THEN 'Reactiva 1 con otro Banco'
                         WHEN WCL.WIO_OBSERVACION='7' THEN 'Porcentaje coberturado por Monto prestamo'
                         WHEN WCL.WIO_OBSERVACION='8' THEN 'Prestamo excede saldo de Repo'
                         WHEN WCL.WIO_OBSERVACION='9' THEN 'Porcentaje coberturado por Monto prestamo'
                         WHEN WCL.WIO_OBSERVACION='10' THEN 'Observado' ELSE WCL.WIO_OBSERVACION
                         END WIO_OBSERVACION_TEXTO")
                    ,'WCL.WIO_OBSERVACION_FECHA'
                    ,'WCL.SUBASTA_DEADLINE'
                    ,'wCL.MONTO_PRESTAMO_REACTIVA_1'
                ];
                break;
            case Etapa::ENVIADO_COFIDE:
                    $columnas=[
                        'WCL.NUM_RUC'
                        ,'WCL.NOMBRE'
                        ,'WCL.WIO_MONTO_SOLICITADO'
                        ,'WCL.WIO_PLAZO'
                        ,'WCL.WIO_GRACIA'
                        ,'WCL.WIO_TASA'
                        ,'WCL.COFIDE_ESTADO'
                        ,'WCL.COFIDE_FECHA_ENVIO'
                        ,'WCL.SUBASTA_DEADLINE'
                        ,'wCL.MONTO_PRESTAMO_REACTIVA_1'
                    ];
                    break;
            default:
                $columnas = [];
                break;
        }
        return $columnas;
    }

    function asignar($data,$etapa=null){
        DB::beginTransaction();
        $status = true;

        try {

            if($data){
                DB::table('WEBVPC_CMPEMERGENCIA_LEADS')->insert($data);
            }

            if ($etapa) {
                DB::table('WEBVPC_CMPEMERGENCIA_LEAD_ETAPA')->insert($etapa);
            }

            DB::commit();
            return true;
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }

    public function getEjecutivoRiesgo($ruc){
        $sql=DB::table('WEBVPC_CMPEMERGENCIA_LEAD_RIESGOS as WCLR')
            ->select('WCLR.REGISTRO_RIESGOS')
            ->where('NUM_RUC',$ruc)->first();
        return $sql;
    }

    public function getEjecutivoMenorCarga($banca){
        $sql=DB::table('WEBVPC_CMPEMERGENCIA_EJECUTIVOS_RIESGOS as WCER')
            ->leftJoin('WEBVPC_CMPEMERGENCIA_LEAD_RIESGOS as WCLR',function($join){
                $join->on('WCLR.REGISTRO_RIESGOS', '=', 'WCER.REGISTRO');
                $join->on(DB::raw('CONVERT(DATE,WCLR.FECHA_ASIGNACION)'),DB::raw('CONVERT(DATE,GETDATE())'));
            })
            //->leftJoin('WEBVPC_CMPEMERGENCIA_LEADS AS WCL', function($join) {
            //    $join->on('WCL.NUM_RUC', '=', 'WCLR.NUM_RUC');
            //    $join->on('WCL.CONTACTO_RESULTADO',DB::raw(\App\Entity\cmpemergencia\Gestion::ENVIAR_RIESGOS));
                //$join->on(DB::raw('ISNULL(WCL.RIESGOS_GESTION,19)'), DB::raw(\App\Entity\cmpemergencia\Gestion::RIESGOS_PENDIENTE));

            //})
            ->select('WCER.REGISTRO as REGISTRO_RIESGOS',DB::raw('COUNT(WCLR.NUM_RUC) as PENDIENTES'))
            ->where('WCER.FLG_ACTIVO',1)
            ->where('WCER.BANCA',$banca)
            ->groupBy('WCER.REGISTRO')
            ->orderBy(DB::raw('COUNT(WCLR.NUM_RUC) '),'asc')
            ->first();
        return $sql;
    }

    /* CARGA PARA BACKOFFICE */

    public function getEjecutivoNegocio($ruc){
        $sql=DB::table('WEBVPC_CMPEMERGENCIA_LEADS as WCL')
            ->select('WCL.ASIGNADO_EJECUTIVO')
            ->where('NUM_RUC',$ruc)->first();
        return $sql;
    }



    public function getEjecutivoNegocioMenorCarga($banca){
        $sql=DB::table('WEBVPC_CMPEMERGENCIA_EJECUTIVOS as WCE')
            ->leftJoin('WEBVPC_CMPEMERGENCIA_LEADS as WCL',function($join){
                $join->on('WCL.ASIGNADO_EJECUTIVO', '=', 'WCE.REGISTRO');
                $join->on(DB::raw('CONVERT(DATE, WCL.FECHA_ASIGNADO_BACKOFFICE)'),'>=', DB::raw('DATEADD(DAY,-3,CONVERT(DATE,GETDATE()))'));
                })

            ->select('WCE.REGISTRO',DB::raw('COUNT(WCL.NUM_RUC) as PENDIENTES'))
            ->whereRaw("WCE.FLAG_ACTIVO = '1'")
            ->where('WCE.BANCA',$banca)
            ->groupBy('WCE.REGISTRO')
            ->orderBy(DB::raw('COUNT(WCL.NUM_RUC) '),'asc')
            ->first();
        return $sql;
    }
}
