<?php
namespace App\Model\cmpemergencia;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;
use App\Entity\cmpemergencia\Etapa as Etapa;

class Gestion extends Model

{

    public function listarBpe(){

        $sql = DB::table('WEBVPC_CMPEMERGENCIA_GESTION AS WCG')
            ->select('WCG.*')
            ->where('WCG.FLG_BPE',1);
            
        return $sql;
    }

    public function listarBe(){
        $sql = DB::table('WEBVPC_CMPEMERGENCIA_GESTION AS WCG')
            ->select('WCG.*')
            ->where('WCG.FLG_BE',1);
            
        return $sql;
    }

    
    public function listarRiesgos(){
        $sql = DB::table('WEBVPC_CMPEMERGENCIA_GESTION AS WCG')
            ->select('WCG.*')
            ->where('WCG.FLG_RIESGOS',1);
            
        return $sql;
    }

    public function listarSubRiesgos($sub){
        $sql = DB::table('WEBVPC_CMPEMERGENCIA_GESTION_SUB AS WCG')
            ->select('WCG.*')
            ->where('WCG.GESTION_ID',$sub);
            
        return $sql;
    }

    public function listarValidacion(){
        $sql = DB::table('WEBVPC_CMPEMERGENCIA_GESTION AS WCG')
            ->select('WCG.*')
            ->where('WCG.FLG_VALIDACION',1);
        return $sql;
    }

}