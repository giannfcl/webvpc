<?php
namespace App\Model\cmpemergencia;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class Priorizacion extends Model

{

    public function agregar($prior){

        DB::beginTransaction();
        $status = true;

        try {
                DB::table('WEBVPC_CMPEMERGENCIA_SOLEMP_PRIORIZACION')->insert($prior);
                DB::commit();
                $status = true;

        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
        
    }

    public function quitar($ruc){

        DB::beginTransaction();
        $status = true;

        try {

            DB::table('WEBVPC_CMPEMERGENCIA_SOLEMP_PRIORIZACION')
                ->where('NUM_RUC',$ruc)
                ->delete();
                DB::commit();
                $status = true;

        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
        
    }

    
}