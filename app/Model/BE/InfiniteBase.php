<?php

namespace App\Model\BE;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class InfiniteBase extends Model {

    protected $table = ['WEBBE_MORA_TABLERO'];

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = ['FECHA', 'ID_CLIE', 'NUM_DOCUMENTO'];

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
    ];

    public function getLista($fecha, $zonal, $jefatura, $regejecutivo) {
        
        $dia = DB::table('WEBBE_INFINITE_TABLERO as WMT')->max('WMT.FECHA');
           
        $sql = DB::table('WEBBE_INFINITE_TABLERO as WMT')
                ->select('WMT.FECHA', 'WMT.ID_CLIE', 'WMT.NC_CLIE', 'WMT.BANCA', 'WMT.REG_EJECUTIVO', 'WMT.ZONAL', 'WMT.EJECUTIVO', 'WMT.NUM_DOCUMENTO', 'WMT.COD_PRODUCTO', 'MP.NOMBRE_CORTO AS NOMBRE_PRODUCTO', 'WMT.FECHA_VENCIMIENTO', 'WMT.NUM_DIAS_VENCIDOS', 'WMT.DEUDA', 'WMT.FLG_GESTIONADO', 'WMT.ESTADO', 'WIG.COMENTARIO', 'WU.ID_CENTRO')
                ->join('MAE_PRODUCTOS AS MP', 'MP.COD_PDTO', '=', 'WMT.COD_PRODUCTO')
                ->leftJoin('WEBVPC_USUARIO as WU', function($join) {
                    $join->on('WU.REGISTRO', '=', 'WMT.REG_EJECUTIVO');
                })
                ->leftJoin('WEBBE_INFINITE_GESTIONES as WIG', function($join) {
                    //$join->on('WMT.FECHA', '=', 'WIG.FECHA_REGISTRO');
                    $join->on('WMT.ID_CLIE', '=', 'WIG.ID_CLIE');
                    $join->on('WMT.NUM_DOCUMENTO', '=', 'WIG.NUM_DOCUMENTO');
                    $join->on('WMT.ESTADO', '=', 'WIG.ESTADO');
                })


                //->where('WMT.FECHA_VENCIMIENTO', '>=', )
                ->where('WMT.FECHA', '=', $dia)
                ->where('WMT.BANCA', '<>', 'G0');

        if ($zonal && $zonal != 'Todos') {
            $sql = $sql->where('WMT.ZONAL', '=', $zonal);
        }

        if ($jefatura && $jefatura != 'Todos') {

            $sql = $sql->where('WU.ID_CENTRO', '=', $jefatura);
        }
        if ($regejecutivo && $regejecutivo != 'Todos') {
            $sql = $sql->where('WMT.REG_EJECUTIVO', '=', $regejecutivo);
        }

        $sql = $sql->orderBy('WMT.NUM_DIAS_VENCIDOS', 'DESC');
        return $sql;
    }

    public function getEjecutivos($banca, $zonal) {

        $sql = DB::table('ARBOL_DIARIO as AD')
                ->select('AD.LOGIN AS REGISTRO', 'AD.ENCARGADO AS NOMBRE_EJECUTIVO');

        if ($banca != 'Todos') {
            if ($zonal != 'Todos') {
                $sql->where('AD.BANCA', '=', $banca)
                        ->where('AD.ZONAL', '=', $zonal)
                        ->where('AD.TIPO', '=', 'EN')
                        ->where('AD.LOGIN', '<>', '')
                        ->distinct();
            } else {
                $sql->where('AD.BANCA', '=', $banca)
                        ->where('AD.TIPO', '=', 'EN')
                        ->where('AD.LOGIN', '<>', '')
                        ->distinct();
            }
        } else {
            $sql->whereIn('AD.BANCA', ['BC', 'BE', 'BEP', 'BEL'])
                    ->where('AD.TIPO', '=', 'EN')
                    ->where('AD.LOGIN', '<>', '')
                    ->distinct();
        }
        return $sql;
    }

    public function getZonal($banca) {
        if ($banca != 'Todos') {
            $sql = DB::table('ARBOL_DIARIO as AD')
                    ->select('AD.ZONAL AS ZONAL')
                    ->where('AD.BANCA', '=', $banca)
                    ->where('AD.ZONAL', '<>', NULL)
                    ->orderByRaw('AD.ZONAL')
                    ->distinct();
        } else {
            $sql = DB::table('WEB_CARTERA_ACTIVA as AD')
                    ->select('AD.ZONAL AS ZONAL')
                    ->whereIn('AD.BANCA', ['BC', 'BE', 'BEP', 'BEL'])
                    ->where('AD.ZONAL', '<>', NULL)
                    ->orderByRaw('AD.ZONAL')
                    ->distinct();
        }

        return $sql;
    }

    public function getBanca() {
        $sql = DB::table('WEB_CARTERA_ACTIVA as CA')
                ->select('CA.ZONAL AS ZONAL')
                ->where('CA.BANCA', '=', $banca)
                ->where('CA.ZONAL', '<>', NULL)
                ->orderByRaw('CA.ZONAL')
                ->distinct();
    }

    public function updateGestion($data) {

        //return true;
        \Debugbar::info('in');
        DB::beginTransaction();
        $status = true;
        try {
            $gestion = [
                'FECHA_REGISTRO' => $data['FECHA'],
                'ID_CLIE' => $data['ID_CLIE'],
                'NUM_DOCUMENTO' => $data['NUM_DOCUMENTO'],
                'ESTADO' => $data['ESTADO'],
                'COMENTARIO' => $data['COMENTARIO'],
            ];
            \Debugbar::info($data['MARCA']);

            DB::table('WEBBE_INFINITE_GESTIONES')
                    //->where('FECHA_REGISTRO', '=', $data['FECHA'])
                    ->where('ID_CLIE', '=', $data['ID_CLIE'])
                    ->where('NUM_DOCUMENTO', '=', $data['NUM_DOCUMENTO'])
                    ->where('ESTADO', '=', $data['ESTADO'])->delete();

            if ($data['MARCA'] == 1) {
                DB::table('WEBBE_INFINITE_GESTIONES')
                        ->insert($gestion);
            }
            DB::table('WEBBE_INFINITE_TABLERO')
                    ->where('FECHA', '=', $data['FECHA'])
                    ->where('ID_CLIE', '=', $data['ID_CLIE'])
                    ->where('NUM_DOCUMENTO', '=', $data['NUM_DOCUMENTO'])
                    ->where('ESTADO', '=', $data['ESTADO'])
                    ->update(['FLG_GESTIONADO' => $data['MARCA']]);
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }

}
    