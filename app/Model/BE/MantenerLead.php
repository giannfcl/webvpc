<?php
namespace App\Model\BE;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class MantenerLead extends Model

{
	protected $table = 'WEBBE_MANTENER_LEAD';
    
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey =['PERIODO','NUM_DOC'];
					        

        /**
     * The name of the "created at" column.
     *
     * @var string
     */
     public $timestamps = false;
                                                                                                                                                                                                                          

    /**
     *
     * @return table
     */
    function insert($data){
        DB::beginTransaction();
        $status = true;
        try {

            DB::table('WEBBE_MANTENER_LEAD')
                ->insert($data);
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }

    function quitar($periodo,$numdoc){
        DB::beginTransaction();
        $status = true;
        try {
            DB::table('WEBBE_MANTENER_LEAD')
                ->where('NUM_DOC',$numdoc)
                ->where('PERIODO',$periodo)
                ->delete();
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }
}