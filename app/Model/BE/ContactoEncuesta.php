<?php
namespace App\Model\BE;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class ContactoEncuesta extends Model

{
	protected $table = 'WEBBE_CONTACTO_ENCUESTA';
    
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey =['ID_CONTACTO','TIPO_CONTACTO'];
					        

        /**
     * The name of the "created at" column.
     *
     * @var string
     */
     public $timestamps = false;
    
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'NOMBRE'
        ];


    function agregar($data){
        DB::beginTransaction();
        $status = true;
        try {
            DB::table('WEBBE_CONTACTO_ENCUESTA')
            ->insert($data);
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }

    function quitar($data){
        DB::beginTransaction();
        $status = true;
        try {
            DB::table('WEBBE_CONTACTO_ENCUESTA')
            ->where('ID_CONTACTO',$data['ID_CONTACTO'])
            ->where('TIPO_CONTACTO',$data['TIPO_CONTACTO'])
            ->delete();
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }

    function actualizar($data){
        DB::beginTransaction();
        $status = true;
        try {
            DB::table('WEBBE_CONTACTO_ENCUESTA')
            ->where('ID_CONTACTO',$data['ID_CONTACTO'])
            ->where('TIPO_CONTACTO',$data['TIPO_CONTACTO'])
            ->update(array_except($data,['ID_CONTACTO','TIPO_CONTACTO']));
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }
    
}