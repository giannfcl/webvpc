<?php

namespace App\Model\BE;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class Jefatura extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'WEBBE_JEFATURA';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = null;
	public $incrementing = false;


    function getJefaturas($zonal = null,$banca=null) {
        $sql = DB::table('WEBBE_JEFATURA as WZJ')
            ->select('WZJ.ID_JEFATURA','WZJ.JEFATURA')
            ->orderBy('WZJ.JEFATURA');
        if ($zonal){
            $sql = $sql->where('WZJ.ID_ZONAL','=',$zonal);
        }
        if ($banca){
            if ($banca!='BE') $sql=$sql->whereNull('WZJ.ID_ZONAL');
            //else $sql = $sql->where('WZJ.ID_ZONAL','=','BEL_ZONAL_2');
        }
        return $sql;
    }

}