<?php
namespace App\Model\BE;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;
use App\Entity\Usuario as Usuario;

class SaldosDiarios extends Model

{
	   
   
   function getGraficoDirectas($ejecutivo,$periodo){

   		$sql = DB::table('WEBVPC_SALDOS_DIARIOS as SD')
                ->select(DB::raw('DAY(SD.FECHA) AS DIA'), DB::raw("SUM(SALDO_PUNTA)/1000000 SALDO_PUNTA"), DB::raw("SUM(SALDO_PROMEDIO)/1000000 SALDO_PROMEDIO"))
              	->join('WEBVPC_PRODUCTO as PRO',function($join){ 
            		$join->on('SD.ID_PRODUCTO','=','PRO.ID_PRODUCTO'); })
				->where('SD.FECHA','>=',DB::raw("(SELECT DATEADD(D, -30, FECHA_ACT_SD) FROM dbo.WEBNOTE_FECHAS WHERE PERIODO = '".$periodo."')"))
        ->where('SD.FECHA','<=',DB::raw("(SELECT FECHA_ACT_SD FROM dbo.WEBNOTE_FECHAS WHERE PERIODO = '".$periodo."')"))
				->where('SD.REGISTRO_EN','=', $ejecutivo)
				->where('PRO.TIPO_PRODUCTO', '=', 'DIRECTAS')
				->groupby('SD.FECHA');

        return $sql;
   }

   function getGraficoIndirectas($ejecutivo,$periodo){
     $sql = DB::table('WEBVPC_SALDOS_DIARIOS as SD')
                ->select(DB::raw('DAY(FECHA) AS DIA'), DB::raw("SUM(SALDO_PUNTA)/1000000 SALDO_PUNTA"), DB::raw("SUM(SALDO_PROMEDIO)/1000000 SALDO_PROMEDIO"))
                ->join('WEBVPC_PRODUCTO as PRO',function($join){ 
                $join->on('SD.ID_PRODUCTO','=','PRO.ID_PRODUCTO'); })
        ->where('SD.FECHA','>=',DB::raw("(SELECT DATEADD(D, -30, FECHA_ACT_SD) FROM dbo.WEBNOTE_FECHAS WHERE PERIODO = '".$periodo."')"))
        ->where('SD.FECHA','<=',DB::raw("(SELECT FECHA_ACT_SD FROM dbo.WEBNOTE_FECHAS WHERE PERIODO = '".$periodo."')"))
        ->where('SD.REGISTRO_EN','=', $ejecutivo)
        ->where('PRO.TIPO_PRODUCTO', '=', 'INDIRECTAS')
        ->groupby('FECHA');

        return $sql;
   }

   function getSaldos($periodo,$registro = null,$jefatura= null,$zonal= null,$banca= null){
     $sql = DB::table('WEBVPC_SALDOS_DIARIOS as SD')
            ->select(
              'SD.FECHA',
              DB::raw("SUM(CASE WHEN TIPO_PRODUCTO = 'DIRECTAS' THEN SALDO_PUNTA ELSE 0 END) SALDO_PUNTA_DIRECTAS"),
              DB::raw("SUM(CASE WHEN TIPO_PRODUCTO = 'DIRECTAS' THEN SALDO_PROMEDIO ELSE 0 END) SALDO_PROMEDIO_DIRECTAS"),
              DB::raw("SUM(CASE WHEN TIPO_PRODUCTO = 'INDIRECTAS' THEN SALDO_PUNTA ELSE 0 END) SALDO_PUNTA_INDIRECTAS"),
              DB::raw("SUM(CASE WHEN TIPO_PRODUCTO = 'INDIRECTAS' THEN SALDO_PROMEDIO ELSE 0 END) SALDO_PROMEDIO_INDIRECTAS")
            )
            ->join('WEBVPC_PRODUCTO as PRO',function($join){ 
              $join->on('SD.ID_PRODUCTO','=','PRO.ID_PRODUCTO'); 
            })
            ->join('WEBVPC_USUARIO as WU',function($join){
              $join->on('SD.REGISTRO_EN','=','WU.REGISTRO'); 
            })
            ->join('WEBBE_ZONAL as WZ',function($join){
              $join->on('WU.ID_ZONA','=','WZ.ID_ZONAL'); 
            });
            
            if($periodo){            
                $sql = $sql->where('SD.FECHA','>=',DB::raw("(SELECT DATEADD(D, -30, FECHA_ACT_SD) FROM dbo.WEBNOTE_FECHAS WHERE PERIODO = '".$periodo."')"));
                $sql = $sql->where('SD.FECHA','<=',DB::raw("(SELECT FECHA_ACT_SD FROM dbo.WEBNOTE_FECHAS WHERE PERIODO = '".$periodo."')"));
             }else{                            
                $sql = $sql->where('SD.FECHA','>=',DB::raw("(SELECT DATEADD(D, -30, FECHA_ACT_SD) FROM dbo.WEBNOTE_FECHAS WHERE PERIODO = (SELECT MAX(PERIODO) FROM WEBNOTE_FECHAS))"));
                $sql = $sql->where('SD.FECHA','<=',DB::raw("(SELECT FECHA_ACT_SD FROM dbo.WEBNOTE_FECHAS WHERE PERIODO = (SELECT MAX(PERIODO) FROM WEBNOTE_FECHAS) )"));                
             }
            
            $sql = $sql -> whereIn('ROL',[DB::raw(Usuario::ROL_EJECUTIVO_FARMER_BE),DB::raw(Usuario::ROL_EJECUTIVO_HUNTER_BE)]);
            $sql = $sql->groupby('FECHA')
            ->orderBy('FECHA');

    if($registro){
        $sql = $sql->where('SD.REGISTRO_EN',$registro);
      }

      if($jefatura){
        $sql = $sql->where('WU.ID_CENTRO',$jefatura);
      }

      if($zonal){
        $sql = $sql->where('WU.ID_ZONA',$zonal);
      }

      if($banca){
        $sql = $sql->where('WU.BANCA',$banca);
      }

      return $sql;
   }

    function getLastSaldoPunta($periodo,$registro = null,$jefatura= null,$zonal= null,$banca= null){

      $sql = DB::table('WEBVPC_SALDOS_DIARIOS as SD')
        ->select('SD.FECHA',
          DB::raw("SUM(CASE WHEN TIPO_PRODUCTO = 'DIRECTAS' THEN SALDO_PUNTA ELSE 0 END)  SALDO_PUNTA_DIRECTAS"),
          DB::raw("SUM(CASE WHEN TIPO_PRODUCTO = 'INDIRECTAS' THEN SALDO_PUNTA ELSE 0 END)  SALDO_PUNTA_INDIRECTAS")
        )
        ->join('WEBVPC_USUARIO as WU',function($join){
          $join->on('SD.REGISTRO_EN','=','WU.REGISTRO'); 
        })
        ->join('WEBBE_ZONAL as WZ',function($join){
          $join->on('WU.ID_ZONA','=','WZ.ID_ZONAL'); 
        })
        ->join('WEBVPC_PRODUCTO as WP',function($join){
          $join->on('WP.ID_PRODUCTO','=','SD.ID_PRODUCTO'); 
        })
        ->where('SD.FECHA','=',DB::raw("(SELECT FECHA_ACT_SD FROM WEBNOTE_FECHAS WHERE PERIODO = '".$periodo."')"))
        ->whereIn('WU.ROL',[DB::raw(Usuario::ROL_EJECUTIVO_FARMER_BE),DB::raw(Usuario::ROL_EJECUTIVO_HUNTER_BE)])
        ->groupBy('SD.FECHA');

      if($registro){
        $sql = $sql->where('SD.REGISTRO_EN',$registro);
      }

      if($jefatura){
        $sql = $sql->where('WU.ID_CENTRO',$jefatura);
      }

      if($zonal){
        $sql = $sql->where('WU.ID_ZONA',$zonal);
      }

      if($banca){
        $sql = $sql->where('WU.BANCA',$banca);
      }

      return $sql;
   }

    function getParametros($periodo){      
      $sql = DB::table('WEBNOTE_FECHAS as WF')
          ->select('WF.FECHA_ACT_SD','WF.FECHA_ACT_VENC','WF.FECHA_INI_SD','WF.PERIODO');           
           if($periodo){            
              $sql = $sql->where('WF.PERIODO','=',DB::raw($periodo));
           }else{                            
              $sql = $sql->where('WF.PERIODO','=',DB::raw('(SELECT MAX(PERIODO) FROM WEBNOTE_FECHAS WHERE PERIODO <>(SELECT MAX(PERIODO) FROM WEBNOTE_FECHAS ))')); 
           }
           
      return $sql;
    }
 
}