<?php
namespace App\Model\BE;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class EjecutivoSectorista extends Model

{
	protected $table = 'WEBBE_EJECUTIVO_SECTORISTA';
    
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey =['ID_CONTACTO'];
					        

        /**
     * The name of the "created at" column.
     *
     * @var string
     */
     public $timestamps = false;
    
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'NOMBRE'
        ];


    /**
     * Devuelve todos los datos del lead
     *
     * @param string $id Id del contacto
     * @return table
     */
    function getSectoristaByEjecutivo($ejecutivo){

        $sql = DB::table('WEBBE_LEAD_SECTORISTA as WES')
                    ->select('WES.COD_SECT_UNIQ_EN')
                    ->where('REGISTRO_EN',$ejecutivo)
                    ->first();
        return $sql;
    
    }

   
}