<?php

namespace App\Model\BE;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;
use App\Entity\BE\Etapa;
use Jenssegers\Date\Date as Carbon;
use App\Entity\Notification as eNotification;

class Lead extends Model

{
    protected $table = 'WEBBE_LEAD';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = [
        'PERIODO',
        'NUM_DOC',
    ];


    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    public $timestamps = false;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'NOMBRE'
    ];


    /**
     * Devuelve todos los datos del lead
     *
     * @param string $periodo Periodo en que fue cargado el Lead.
     * @param string $lead Numero de documento del lead.
     * @param array $filtros Filtros de busqueda
     * @return table
     */
    function get($periodo, $lead, $filtros = [], $orden = [])
    {
        $sql = DB::table(DB::Raw("(SELECT * FROM WEBBE_LEAD where PERIODO = '$periodo') as WL"))
            ->select(
                'WL.NUM_DOC',
                'WL.NOMBRE',
                'WL.SEGMENTO_BANCA',
                'WL.COD_UNICO',
                'WL.COD_SBS',
                'WL.DEUDA_DIRECTA',
                'WL.BANCO_PRINCIPAL',
                DB::Raw("ISNULL(CAST(WL.DEUDA_DIRECTA AS FLOAT),'')+ISNULL(CAST(WL.DEUDA_INDIRECTA AS FLOAT),'') as DEUDA_TOTAL"),
                'WL.DEUDA_INDIRECTA',
                'WL.GARANTIA',
                'WL.BANCO_GARANTIA',
                'WL.VERIFICADO',
                'WL.TIPO_PROSPECTO',
                'WL.CATEGORIA',
                'WL.DEPARTAMENTO',
                'WL.PROVINCIA',
                'WL.DISTRITO',
                'WL.DIRECCION',
                'WL.TELEFONO'
            );
        if ($lead) {
            $sql = $sql->where('WL.NUM_DOC', '=', $lead);
        }


        /***FILTROS****/

        if (isset($filtros['documento'])) {
            $sql = $sql->where('WL.NUM_DOC', $filtros['documento']);
        }

        if (isset($filtros['razonSocial'])) {
            $sql = $sql->where('WL.NOMBRE', 'like', '%' . $filtros['razonSocial'] . '%');
        }

        if (isset($filtros['verificado'])) {
            $sql = $sql->where('WL.VERIFICADO', '=', $filtros['verificado']);
        }
        if (isset($filtros['bcoPrincipal'])) {
            $sql = $sql->where('WL.BANCO_PRINCIPAL', '=', $filtros['bcoPrincipal']);
        }
        if (isset($filtros['minDeudaDirecta'])) {
            $sql = $sql->where('WL.DEUDA_DIRECTA', '>=', $filtros['minDeudaDirecta']);
        }
        if (isset($filtros['maxDeudaDirecta'])) {
            $sql = $sql->where('WL.DEUDA_DIRECTA', '<=', $filtros['maxDeudaDirecta']);
        }
        if (isset($filtros['minDeudaIndirecta'])) {
            $sql = $sql->where('WL.DEUDA_INDIRECTA', '>=', $filtros['minDeudaIndirecta']);
        }
        if (isset($filtros['maxDeudaIndirecta'])) {
            $sql = $sql->where('WL.DEUDA_INDIRECTA', '<=', $filtros['maxDeudaIndirecta']);
        }
        if (isset($filtros['bcoPrincipalGarantia']) && $filtros['bcoPrincipalGarantia'] <> 'Todos') {
            $sql = $sql->where('WL.BANCO_GARANTIA', '=', $filtros['bcoPrincipalGarantia']);
        }
        if (isset($filtros['minGarantia'])) {
            $sql = $sql->where('WL.GARANTIA', '>=', $filtros['minGarantia']);
        }
        if (isset($filtros['maxGarantia'])) {
            $sql = $sql->where('WL.GARANTIA', '<=', $filtros['maxGarantia']);
        }

        //dd($orden);

        //if (isset($orden['sort']) &&  $orden['sort']!='dias')
        //    $sql = $sql->orderBy($this->getOrderColumn($orden['sort']),$orden['order']);

        return $sql;
    }
    function getProductos($documento=null)
    {
        if ($documento) {
            $sql = DB::table('T_CLIENTES_PRODUCTOS_IBK')
                ->where('CODDOC',  $documento)->orderby('SALDO_PUNTA','desc')
                ->get();
            return $sql;
        }else{
            return false;
        }

    }
    function getRegistroJefe($registro_EN)
    {
        $registro_JEFE = null;
        $nombre = DB::table("ARBOL_DIARIO_2")
                    ->select('NOMBRE_JEFE')
                    ->where('LOGIN','=',$registro_EN)
                    ->get()
                    ->first();
        if($nombre && $nombre->NOMBRE_JEFE){
            $registro_JEFE = DB::table("WEBVPC_USUARIO")
                    ->select('REGISTRO')
                    ->where('NOMBRE','=',$nombre->NOMBRE_JEFE)
                    ->get()
                    ->first();
        }
       
        return  isset($registro_JEFE->REGISTRO) ? $registro_JEFE->REGISTRO : null;
    }
    function confirmarEliminar($coddoc, $periodo, $actividad, $registro, $empresa)
    {
        DB::beginTransaction();
        $status = true;
        try {

            DB::table('WEBBE_ACTIVIDAD')->insert($actividad);
            $notification = new eNotification();
            $notification->setValue('_idTipo', '10');
            $notification->setValue('_registro', $registro);
            $notification->setValue('_contenido', "La eliminación de la empresa " . strtoupper($empresa) . " fue aprobada");
            $notification->setValue('_fechaNotificacion', Carbon::now());
            $notification->setValue('_flgLeido', '0');
            $notification->setValue('_fechaLeido', null);
            $notification->setValue('_valor1', null);
            $notification->setValue('_valor2', null);
            $notification->setValue('_valor3', null);
            $notification->setValue('_valor4', null);
            $notification->setValue('_url', null);
            DB::table('T_WEB_NOTIFICACIONES')->insert($notification->setValueToTable());
            DB::table('WEBBE_LEAD')
                ->where('NUM_DOC', $coddoc)
                ->where('PERIODO', $periodo)
                ->update(['ESTADO_LEAD' => "0", 'VERIFICADO' => '0']);
            
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }
    function updateVerificado($numdoc, $actividad, $registro, $empresa)
    {
        DB::beginTransaction();
        $status = true;
        try {

            DB::table('WEBBE_ACTIVIDAD')->insert($actividad);
            $notification = new eNotification();
            $notification->setValue('_idTipo', '10');
            $notification->setValue('_registro', $registro);
            $notification->setValue('_contenido', "La eliminación de la empresa " . strtoupper($empresa) . " fue desaprobada");
            $notification->setValue('_fechaNotificacion', Carbon::now());
            $notification->setValue('_flgLeido', '0');
            $notification->setValue('_fechaLeido', null);
            $notification->setValue('_valor1', null);
            $notification->setValue('_valor2', null);
            $notification->setValue('_valor3', null);
            $notification->setValue('_valor4', null);
            $notification->setValue('_url',  '/be/actividades?documento=' . $numdoc . '&razonSocial=');
            DB::table('T_WEB_NOTIFICACIONES')->insert($notification->setValueToTable());
            DB::table('WEBBE_LEAD')->where('NUM_DOC', '=', $numdoc)->update(['VERIFICADO' => "0"]);

        DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }
    function getLeadEtapaEstrategia($periodo, $lead = null, $prospecto = true, $filtros = null, $orden = null)
    {
        /** REVISAR LA REUTILIZACIÓN PARA ACCIONES COMERCIALES **/
        //dd($prospecto);
        $sql = $this->get($periodo, $lead, $filtros, $orden)
            ->addSelect('WLS.FLG_CLIENTE as FLAG_ES_CLIENTE', 'WLS.COD_SECT_UNIQ_EN')
            ->addSelect('WLC.ESTRATEGIA_NOMBRE as ESTRATEGIA_NOMBRE', 'WLC.ID_CAMP_EST as ESTRATEGIA_ID', 'WLC.PERIODO as ESTRATEGIA_PERIODO')
            ->addSelect('WLET.ETAPA_NOMBRE as ETAPA_NOMBRE', 'WLET.ID_ETAPA as ETAPA_ID', 'WLET.FLG_EDITABLE as ETAPA_EDITABLE', 'WLET.FECHA_REGISTRO as ETAPA_FECHA')
            ->addSelect('WLS.REGISTRO_EN as REGISTRO_EN', 'WLS.EJECUTIVO_NOMBRE as EJECUTIVO_NOMBRE')
            ->addSelect('WLC.FECHA_CARGA', 'WLC.PRIORIDAD')
            ->addSelect(DB::raw('CASE WHEN [WLET].[ID_ETAPA] in (1,2,3,4,5) THEN DATEDIFF(DAY,[WLET].[FECHA_REGISTRO] ,GETDATE())
                                      ELSE NULL END DIAS_PENDIENTE'))
            ->addSelect("WLECOR.CANT_ECOR", "WLECOR.TOTAL_PAP_2019")

            ->join(DB::Raw("(SELECT [WLS].*,[WU].NOMBRE AS EJECUTIVO_NOMBRE,[WU].ID_CENTRO,WU.ID_ZONA,WU.BANCA FROM [WEBBE_LEAD_SECTORISTA] [WLS] INNER JOIN [WEBVPC_USUARIO] AS [WU] ON [WU].[REGISTRO] = [WLS].[REGISTRO_EN] WHERE [WLS].[FLG_ULTIMO] = '1') as WLS"), function ($join) {
                $join->on('WL.PERIODO', '=', 'WLS.PERIODO');
                $join->on('WL.NUM_DOC', '=', 'WLS.NUM_DOC');
            })

            ->leftJoin(DB::Raw("(SELECT [WLC].* FROM (SELECT [WLC].*,[WCE].NOMBRE AS ESTRATEGIA_NOMBRE,WCE.TIPO
                                   FROM [WEBVPC_LEAD_CAMP_EST] [WLC]
                                   INNER JOIN (SELECT * FROM [WEBVPC_CAMP_EST] [WCE] WHERE [WCE].[BANCA] = 'BE')[WCE] ON [WCE].[ID_CAMP_EST] = [WLC].[ID_CAMP_EST]
                                )[WLC]
                                     LEFT JOIN [WEBVPC_CAMP_EST_INSTANCIA] [WCEI] ON [WCEI].[PERIODO] = [WLC].[PERIODO] AND [WCEI].[ID_CAMP_EST] = [WLC].[ID_CAMP_EST]) as WLC"), function ($join) {
                $join->on('WL.PERIODO', '=', 'WLC.PERIODO');
                $join->on('WL.NUM_DOC', '=', 'WLC.NUM_DOC');
            })

            ->leftJoin(DB::Raw('(SELECT [WLET].*,[WET].NOMBRE AS ETAPA_NOMBRE,[WET].FLG_EDITABLE
                                FROM [WEBBE_LEAD_ETAPA] [WLET]
                                LEFT JOIN [WEBBE_ETAPA] as [WET] on [WET].[ID_ETAPA] = [WLET].[ID_ETAPA]
                                WHERE [WLET].[FLG_ULTIMO] = 1
                            ) as WLET'), function ($join) {
                $join->on('WLET.NUM_DOC', '=', 'WL.NUM_DOC');
                $join->on('WLET.REGISTRO_EN', '=', 'WLS.REGISTRO_EN');
                $join->on('WLET.ID_CAMP_EST', '=', 'WLC.ID_CAMP_EST');
                $join->on('WLET.FLG_ULTIMO', '=', DB::raw('1'));
                $join->on(DB::raw('CONVERT(DATE,WLET.FECHA_REGISTRO)'), '>=', DB::raw('CONVERT(DATE,WLS.FECHA_REGISTRO)'));
            })

            ->leftJoin(DB::raw("(SELECT COUNT(1) CANT_ECOR,SUM(PAP_2019) TOTAL_PAP_2019,NRODOC from WEBBE_LEAD_ECOSISTEMA_ORDENANTES GROUP BY NRODOC) as WLECOR"), function ($join) {
                $join->on('WLECOR.NRODOC', '=', 'WLC.NUM_DOC');
            });

        //Si es prospecto solo toma activos, prospectos o clientes tipo referido
        if ($prospecto) {
            $sql = $sql
                // ->where('WLS.FLG_CLIENTE', 0)
                ->where('WL.ESTADO_LEAD', 1)
                // ->where('WLC.TIPO', '=', 'PROSPECTOS');
                ->whereIn('WL.TIPO_PROSPECTO', array('R','P'));
        }

        if (isset($filtros['estrategia']) && $filtros['estrategia'] <> 'Todos') {
            $sql = $sql->where('WLC.ID_CAMP_EST', '=', $filtros['estrategia']);
        }

        if (isset($filtros['etapa']) && $filtros['etapa'] <> 'Todos') {
            $sql = $sql->where('WLET.ID_ETAPA', '=', $filtros['etapa']);
        }

        if (isset($filtros['ejecutivo'])) {
            $sql = $sql->where('WLS.REGISTRO_EN', $filtros['ejecutivo']);
        }

        if (isset($filtros['semaforo']) &&  $filtros['semaforo'] == 'verde') {
            $sql = $sql->whereRaw("( 
                ([WLET].[ID_ETAPA] IN (1,2,4) AND DATEDIFF(DAY,[WLET].[FECHA_REGISTRO] ,GETDATE())<15) OR 
                ([WLET].[ID_ETAPA] IN (3,5) AND DATEDIFF(DAY,[WLET].[FECHA_REGISTRO] ,GETDATE())<31)
            )");
        }

        if (isset($filtros['semaforo']) &&  $filtros['semaforo'] == 'ambar') {
            $sql = $sql->whereRaw("( 
                ([WLET].[ID_ETAPA] IN (1,2,4) AND DATEDIFF(DAY,[WLET].[FECHA_REGISTRO] ,GETDATE())>14 AND DATEDIFF(DAY,[WLET].[FECHA_REGISTRO] ,GETDATE())<22) OR 
                ([WLET].[ID_ETAPA] IN (3,5) AND DATEDIFF(DAY,[WLET].[FECHA_REGISTRO] ,GETDATE())>30 AND DATEDIFF(DAY,[WLET].[FECHA_REGISTRO] ,GETDATE())<61)
            )");
        }

        if (isset($filtros['semaforo']) &&  $filtros['semaforo'] == 'rojo') {
            $sql = $sql->whereRaw("( 
                ([WLET].[ID_ETAPA] IN (1,2,4)  AND DATEDIFF(DAY,[WLET].[FECHA_REGISTRO] ,GETDATE())>21) OR 
                ([WLET].[ID_ETAPA] IN (3,5) AND DATEDIFF(DAY,[WLET].[FECHA_REGISTRO] ,GETDATE())>60)
            )");
        }
        // Si no se ha seleccionado un campo para ordenar, se da el ordenamiento por default:
        if (isset($orden['sort']))
            $sql = $sql->orderBy(DB::raw($this->getOrderColumn($orden['sort'])), $orden['order']);
        else
            $sql = $sql->orderBy(DB::Raw("ISNULL(CAST(WL.DEUDA_DIRECTA AS FLOAT),'')+ISNULL(CAST(WL.DEUDA_INDIRECTA AS FLOAT),'')"), 'DESC');

        return $sql;
    }

    function getOrderColumn($field)
    {
        switch ($field) {
            case 'deudaD':
                return 'WL.DEUDA_DIRECTA';
            case 'deudaI':
                return 'WL.DEUDA_INDIRECTA';
            case 'garantia':
                return 'WL.GARANTIA';
            case 'dias':
                return 'CASE WHEN [WLET].[ID_ETAPA] in (1,2,3,4,5) THEN DATEDIFF(DAY,[WLET].[FECHA_REGISTRO] ,GETDATE()) ELSE NULL END';
            case 'volumenPAP':
                return 'WLECOR.TOTAL_PAP_2019';
        };
    }

    function getLeadEjecutivo($periodo, $ejecutivo, $prospecto = true, $lead = null, $filtros = null, $orden = null)
    {
        $sql = $this->getLeadEtapaEstrategia($periodo, $lead, $prospecto, $filtros, $orden);

        if ($ejecutivo) {
            $sql = $sql->where('WLS.REGISTRO_EN', '=', $ejecutivo);
        }

        return $sql;
    }

    function getLeadJefatura($periodo, $jefatura, $prospecto = true, $lead = null, $filtros = null, $orden = null)
    {
        $sql = $this->getLeadEtapaEstrategia($periodo, $lead, $prospecto, $filtros, $orden)
            ->where('WLS.ID_CENTRO', '=', $jefatura);

        if (isset($filtros['ejecutivo'])) {
            $sql = $sql->where('WLS.REGISTRO_EN', '=', $filtros['ejecutivo']);
        }

        return $sql;
    }

    function getLeadZonal($periodo, $zonal, $prospecto = true, $lead = null, $filtros = null, $orden = null)
    {
        $sql = $this->getLeadEtapaEstrategia($periodo, $lead, $prospecto, $filtros, $orden);

        if ($zonal == 'BC')
            $sql = $sql
                ->whereIn('WLS.ID_ZONA', array('BCGRUPO1', 'BCGRUPO2'));
        else
            $sql = $sql->where('WLS.ID_ZONA', '=', $zonal);

        if (isset($filtros['ejecutivo'])) {
            $sql = $sql->where('WLS.REGISTRO_EN', '=', $filtros['ejecutivo']);
        }

        if (isset($filtros['jefatura'])) {
            $sql = $sql->where('WLS.ID_CENTRO', '=', $filtros['jefatura']);
        }

        return $sql;
    }

    function getLeadBanca($periodo, $banca, $prospecto = true, $lead = null, $filtros = null, $orden = null)
    {
        $sql = $this->getLeadEtapaEstrategia($periodo, $lead, $prospecto, $filtros, $orden)
            ->where('WLS.BANCA', '=', $banca);

        if (isset($filtros['ejecutivo'])) {
            $sql = $sql->where('WLS.REGISTRO_EN', '=', $filtros['ejecutivo']);
        }

        if (isset($filtros['jefatura'])) {
            $sql = $sql->where('WLS.ID_CENTRO', '=', $filtros['jefatura']);
        }

        if (isset($filtros['zonal'])) {
            $sql = $sql->where('WLS.ID_ZONA', '=', $filtros['zonal']);
        }

        return $sql;
    }

    function getLeadGerencia($periodo, $prospecto = true, $lead = null, $filtros = null, $orden = null)
    {
        $sql = $this->getLeadEtapaEstrategia($periodo, $lead, $prospecto, $filtros, $orden);
        if (isset($filtros['ejecutivo'])) {
            $sql = $sql->where('WLS.REGISTRO_EN', '=', $filtros['ejecutivo']);
        }

        if (isset($filtros['jefatura'])) {
            $sql = $sql->where('WLS.ID_CENTRO', '=', $filtros['jefatura']);
        }

        if (isset($filtros['zonal'])) {
            $sql = $sql->where('WLS.ID_ZONA', '=', $filtros['zonal']);
        }

        if (isset($filtros['banca'])) {
            $sql = $sql->where('WLS.BANCA', '=', $filtros['banca']);
        }
        return $sql;
    }

    function getAtributoByEjecutivo($periodo, $ejecutivo, $clave, $atributo)
    {
        $sql = $this->getLeadEjecutivo($periodo, $ejecutivo);
        $sql->select = null;

        return $sql->addSelect($clave . ' as VALOR', $atributo . ' as NOMBRE')
            ->orderby($atributo)
            ->get();
    }

    function getLeadContactos($periodo, $lead, $ejecutivo)
    {

        /*
        $campos = [
            'CONTACTO_ID' => 'CONVERT(varchar(10), WCO.ID_CONTACTO)',
            'CONTACTO_NOMBRE' => "WCO.NOMBRE + ' ' + WCO.APELLIDO_PATERNO",
        ];

        //Paso 2. Agregamos cada campo a la consulta de agrupamiento
        $scripts = [];
        $where = '';
        foreach($campos as $key => $campo){

            //consulta agrupada - es necesario cruzar leadCampanha - CampanhaInstancia - Campanha - Gestion
            $join = DB::table('WEBBE_CONTACTO as WCO')
            ->select(DB::raw("'|' + ".$campo))
            ->whereRaw('WL.NUM_DOC = WCO.NUM_DOC');
            $script = $join->toSql();
            $scripts[] = "STUFF((". $script ." FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1, '') ".$key;
            
        }
        */

        $sql = $this->get($periodo, $lead)
            //->addSelect(DB::raw($scripts[0]),DB::raw($scripts[1]))
            ->join('WEBBE_LEAD_SECTORISTA as WLS', function ($join) {
                $join->on('WL.PERIODO', '=', 'WLS.PERIODO');
                $join->on('WL.NUM_DOC', '=', 'WLS.NUM_DOC');
            });

        if ($ejecutivo) {
            $sql = $sql->where('WLS.REGISTRO_EN', '=', $ejecutivo);
        }

        return $sql;
    }

    function getAutocompleteLead($periodo, $lead, $regEjecutivo = null)
    {

        $sql = $sql = DB::table('WEBBE_LEAD as WL')
            ->select('WL.NUM_DOC', 'WL.NOMBRE')
            ->join('WEBBE_LEAD_SECTORISTA as WLS', function ($join) {
                $join->on('WL.PERIODO', '=', 'WLS.PERIODO');
                $join->on('WL.NUM_DOC', '=', 'WLS.NUM_DOC');
            })
            ->join('WEBVPC_USUARIO as WU', 'WU.REGISTRO', '=', 'WLS.REGISTRO_EN')
            ->where('WL.PERIODO', $periodo)
            ->where('WL.NOMBRE', 'like', '%' . $lead . '%');
        if ($regEjecutivo) {
            $sql = $sql->where('WU.REGISTRO', $regEjecutivo);
        }
        return $sql;
    }

    function getAutocompleteEjecutivo($periodo, $lead, $ejecutivo)
    {
        $sql = $this->getAutocompleteLead($periodo, $lead)
            ->where('WU.REGISTRO', '=', $ejecutivo);
        return $sql;
    }

    function getAutocompleteJefatura($periodo, $lead, $jefatura, $regEjecutivo = null)
    {
        $sql = $this->getAutocompleteLead($periodo, $lead, $regEjecutivo)
            ->where('WU.ID_CENTRO', '=', $jefatura);
        return $sql;
    }

    function getAutocompleteZonal($periodo, $lead, $zonal, $regEjecutivo = null)
    {
        $sql = $this->getAutocompleteLead($periodo, $lead, $regEjecutivo)
            ->where('WU.ID_ZONA', '=', $zonal);
        return $sql;
    }

    function getAutocompleteBanca($periodo, $lead, $banca, $regEjecutivo = null)
    {
        $sql = $this->getAutocompleteLead($periodo, $lead, $regEjecutivo)
            ->where('WU.BANCA', '=', $banca);
        return $sql;
    }

    function getAutocompleteGerencia($periodo, $lead)
    {
        $sql = $this->getAutocompleteLead($periodo, $lead);
        return $sql;
    }

    function actualizar($data)
    {
        DB::beginTransaction();
        $status = true;
        try {
            DB::table('WEBBE_LEAD')
                ->where('NUM_DOC', $data['NUM_DOC'])
                ->where('PERIODO', $data['PERIODO'])
                ->update(array_except($data, ['NUM_DOC', 'PERIODO']));
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }

    function referidosMesActual($periodo, $registro = null)
    {

        $sql = DB::table(DB::Raw("(SELECT * FROM WEBBE_LEAD WL WHERE WL.ESTADO_LEAD = '1' AND WL.TIPO_PROSPECTO = 'R' AND WL.PERIODO = '$periodo') AS WL"))
            ->select('WLS.NOMBRE', 'WLS.REGISTRO_EN AS REGISTRO', DB::Raw('COUNT(*) AS NUM_REFERIDOS'))
            ->join(DB::Raw("(
                    SELECT WLS.*,WU.NOMBRE
                    FROM WEBBE_LEAD_SECTORISTA WLS INNER JOIN WEBVPC_USUARIO as WU on WU.REGISTRO = WLS.REGISTRO_EN
                    WHERE WLS.REGISTRO_EN = '$registro'
                    AND WLS.PERIODO = '$periodo '
                    AND  WLS.FLG_ULTIMO=1
                ) AS WLS"), function ($join) {
                $join->on('WLS.NUM_DOC', '=', 'WL.NUM_DOC');
                $join->on('WL.PERIODO', '=', 'WLS.PERIODO');
            })
            ->join("WEBBE_PERIODOS as WP", function ($join) {
                $join->on('WL.PERIODO', '=', 'WP.PERIODO');
            })
            ->where(DB::Raw("CAST(WLS.FECHA_REGISTRO AS DATE)"), ">=", DB::Raw("CAST(WP.FECHA_INI AS DATE)"))
            ->groupBy('WLS.NOMBRE', 'WLS.REGISTRO_EN');
        return $sql;
    }

    function buscarDatosReferido($periodo, $documento, $codunico)
    {
        $sql = DB::table(DB::Raw("(SELECT WL.* FROM WEBBE_LEAD WL WHERE WL.PERIODO='$periodo' AND ESTADO_LEAD=1) AS WL"))
            ->select('WL.NUM_DOC', 'WL.COD_UNICO', 'WL.FLG_ES_CLIENTE', 'WLS.FECHA_REGISTRO', 'WLS.REGISTRO_EN', 'WLS.EJECUTIVO_NOMBRE', 'WLET.ID_ETAPA', 'WLET.ETAPA_NOMBRE', 'WLET.FECHA_REGISTRO AS FECHA_ETAPA', 'WAC.FECHA_ACTIVIDAD AS FECHA_ULTIMA_VISITA', 'AD.COD_SECT_LARGO AS COD_SECTORISTA', 'AD.COD_SECT_UNIQ', 'AD.COD_CORTO')
            ->join(DB::Raw("(SELECT [WLS].*,[WU].NOMBRE as EJECUTIVO_NOMBRE
                        FROM [WEBBE_LEAD_SECTORISTA] [WLS] INNER JOIN [WEBVPC_USUARIO] AS [WU] ON [WU].[REGISTRO] = [WLS].[REGISTRO_EN]
                        WHERE [WLS].PERIODO='$periodo' AND [WLS].[FLG_ULTIMO] = 1
                    ) as WLS"), function ($join) {
                $join->on('WL.NUM_DOC', '=', 'WLS.NUM_DOC');
            })
            ->leftJoin(DB::Raw("(SELECT [WLET].*,[WET].NOMBRE AS ETAPA_NOMBRE
                          FROM [WEBBE_LEAD_ETAPA] [WLET]
                          left join [WEBBE_ETAPA] as [WET] on [WET].[ID_ETAPA] = [WLET].[ID_ETAPA]
                          WHERE FLG_ULTIMO=1
                      ) AS WLET "), function ($join) {
                $join->on('WL.NUM_DOC', '=', 'WLET.NUM_DOC');
                $join->on('WLS.REGISTRO_EN', '=', 'WLET.REGISTRO_EN');
                $join->on('WLET.FECHA_REGISTRO', '=', 'WLS.FECHA_REGISTRO');
            })
            ->leftJoin(DB::Raw("(
                    SELECT [WAC].*,
                       ROW_NUMBER()OVER(PARTITION BY NUM_DOC,REGISTRO_EN ORDER BY FECHA_ACTIVIDAD DESC) AS ORDEN
                       FROM WEBBE_ACTIVIDAD [WAC] WHERE TIPO='VISITA'
                    ) as WAC"), function ($join) {
                $join->on('WLS.NUM_DOC', '=', 'WAC.NUM_DOC');
                $join->on('WLS.REGISTRO_EN', '=', 'WAC.REGISTRO_EN');
                $join->on('WAC.ORDEN', '=', DB::Raw(1));
            })
            ->leftJoin(DB::Raw("(SELECT * FROM (SELECT *, ROW_NUMBER() OVER (PARTITION BY LOGIN ORDER BY COD_SECT_LARGO DESC) ORDEN FROM ARBOL_DIARIO_2 WHERE EN <>'SIN CLIENTES' ) A WHERE A.ORDEN='1') as AD"), function ($join) {
                $join->on('WLS.REGISTRO_EN', '=', 'AD.LOGIN');
            });
        if ($documento) {
            $sql = $sql->where('WL.NUM_DOC', $documento);
        }
        if ($codunico) {
            $sql = $sql->where('WL.COD_UNICO', $codunico);
        }
        return $sql;
    }


    function getReferido($periodo, $documento, $codunico)
    {
        $sql = DB::table('AD_TMP_MERCADO_REFERIDOS as MR')
            ->select(
                'MR.BANCA',
                'MR.NUM_DOC',
                'MR.COD_UNICO',
                'MR.CODSBS AS COD_SBS',
                DB::raw('ISNULL(MR.NOMBRE,WL.NOMBRE) as NOMBRE'),
                'MR.CALIFICACION',
                'MR.FLAG_FEVE',
                'MR.DEUDA_DIRECTA'
            )
            ->addSelect('WLS.COD_SECT_UNIQ_EN', 'WU.NOMBRE as EJECUTIVO_NOMBRE', 'WET.NOMBRE as ETAPA_NOMBRE', 'WLS.REGISTRO_EN', 'WL.FLG_ES_CLIENTE', 'AD.COD_SECT_LARGO AS COD_SECTORISTA')

            // se necesita un full outer para verificar tanto en la tabla de prospectos como en la de referidos
            ->join('WEBBE_LEAD as WL', function ($join) use ($periodo) {
                $join->on('WL.NUM_DOC', '=', 'MR.NUM_DOC');
            }, null, null, 'full outer')

            ->leftJoin('WEBBE_LEAD_SECTORISTA as WLS', function ($join) use ($periodo) {
                $join->on(DB::raw($periodo), '=', 'WLS.PERIODO');
                $join->on('WL.NUM_DOC', '=', 'WLS.NUM_DOC');
                $join->on('WLS.FLG_ULTIMO', '=', DB::raw(1));
            })

            ->leftJoin('WEBVPC_USUARIO as WU', function ($join) use ($periodo) {
                $join->on('WU.REGISTRO', '=', 'WLS.REGISTRO_EN');
            })
            ->leftJoin(DB::Raw("(SELECT * FROM (SELECT *, ROW_NUMBER() OVER (PARTITION BY LOGIN ORDER BY COD_SECT_LARGO DESC) ORDEN FROM ARBOL_DIARIO_2 ) A WHERE A.ORDEN='1') as AD"), function ($join) {
                $join->on('WLS.REGISTRO_EN', '=', 'AD.LOGIN');
            })

            ->leftJoin('WEBBE_LEAD_ETAPA as WLET', function ($join) use ($periodo) {
                $join->on('WLET.PERIODO', '=', DB::raw($periodo));
                $join->on('WLET.NUM_DOC', '=', 'MR.NUM_DOC');
                $join->on('WLET.REGISTRO_EN', '=', 'WLS.REGISTRO_EN');
                $join->on('WLET.FLG_ULTIMO', '=', DB::raw(1));
            })

            ->leftJoin('WEBBE_ETAPA as WET', function ($join) {
                $join->on('WET.ID_ETAPA', '=', 'WLET.ID_ETAPA');
            })
            ->where('WET.NOMBRE', '<>', NULL);

        if ($documento) {
            $sql = $sql->where('MR.NUM_DOC', $documento)
                ->orWhere('WL.NUM_DOC', $documento);
        }
        if ($codunico) {
            $sql = $sql->where('MR.COD_UNICO', $codunico)
                ->orWhere('WL.COD_UNICO', $codunico);
        }
        //dd($sql->toSql());
        return $sql;
    }


    function getReferidoExtra($periodo, $documento)
    {
        $sql = DB::table('AD_TMP_MERCADO_REFERIDOS as MR')
            ->select(
                'WL.PERIODO',
                'MR.NUM_DOC',
                'MR.COD_UNICO',
                'MR.CODSBS AS COD_SBS',
                DB::Raw('ISNULL(MR.NOMBRE, WL.NOMBRE) AS NOMBRE'),
                'MR.CALIFICACION',
                'MR.FLAG_FEVE',
                'MR.DEUDA_DIRECTA',
                'WLS.COD_SECT_UNIQ_EN',
                'WU.NOMBRE AS EJECUTIVO_NOMBRE',
                DB::Raw("CASE WHEN LEL.NUM_DOC IS NULL THEN UPPER(WET.NOMBRE) ELSE 'ELIMINADO' END AS ETAPA_NOMBRE"),
                DB::Raw("CONVERT(DATE,CASE WHEN LEL.NUM_DOC IS NULL THEN WLET.FECHA_REGISTRO ELSE LEL.FECHA_ELIMINACION END) AS  FECHA_ETAPA"),
                DB::Raw("ISNULL(MOT.DESC_MOTIVO,'')+'-'+ISNULL(DET.DESC_DETALLE,'') as MOTIVO_DETALLE"),
                DB::Raw("CONVERT(DATE,WAC.FECHA_ACTIVIDAD) AS  FECHA_ACTIVIDAD")
            )
            // se necesita un full outer para verificar tanto en la tabla de prospectos como en la de referidos
            ->join('WEBBE_LEAD as WL', function ($join) use ($periodo) {
                $join->on('WL.NUM_DOC', '=', 'MR.NUM_DOC');
            }, null, null, 'full outer')

            ->leftJoin('WEBBE_LEAD_SECTORISTA as WLS', function ($join) use ($periodo) {
                $join->on(DB::raw($periodo), '=', 'WLS.PERIODO');
                $join->on('WL.NUM_DOC', '=', 'WLS.NUM_DOC');
                $join->on('WLS.FLG_ULTIMO', '=', DB::raw(1));
            })

            ->leftJoin('WEBVPC_USUARIO as WU', function ($join) use ($periodo) {
                $join->on('WU.REGISTRO', '=', 'WLS.REGISTRO_EN');
            })

            ->leftJoin('WEBBE_LEAD_ETAPA as WLET', function ($join) use ($periodo) {
                /*$join->on('WLET.PERIODO', '=', DB::raw($periodo));*/
                $join->on('WLET.NUM_DOC', '=', 'MR.NUM_DOC');
                $join->on('WLET.FLG_ULTIMO', '=', DB::raw(1));
            })

            ->leftJoin('WEBBE_ETAPA as WET', function ($join) {
                $join->on('WET.ID_ETAPA', '=', 'WLET.ID_ETAPA');
            })
            ->leftJoin('WEBBE_LEAD_ELIMINACION AS LEL', function ($join) {
                $join->on('LEL.NUM_DOC', '=', 'WL.NUM_DOC');
                $join->on('LEL.FECHA_ELIMINACION', '>=', 'WLET.FECHA_REGISTRO');
            })
            ->leftJoin('WEBBE_CAT_MOTIVO_ELIMINAR as MOT', function ($join) {
                $join->on('MOT.ID_MOTIVO', '=', 'LEL.MOTIVO');
            })
            ->leftJoin('WEBBE_CAT_DETALLE_ELIMINAR as DET', function ($join) {
                $join->on('DET.ID_DETALLE', '=', 'LEL.DETALLE');
                $join->on('DET.ID_MOTIVO', '=', 'MOT.ID_MOTIVO');
            })
            ->leftJoin(DB::Raw("(SELECT *,ROW_NUMBER() OVER(PARTITION BY NUM_DOC ORDER BY FECHA_ACTIVIDAD DESC) RNK  FROM WEBBE_ACTIVIDAD
                                     WHERE TIPO='" . "VISITA" . "') WAC"), function ($join) {

                $join->on('WAC.NUM_DOC', '=', 'WL.NUM_DOC');
                $join->on('WAC.RNK', '=', DB::raw(1));
            })
            ->orderBy('WL.PERIODO', 'DESC');

        if ($documento) {
            $sql = $sql->where('MR.NUM_DOC', $documento)
                ->orWhere('WL.NUM_DOC', $documento);
        }
        //dd($sql->toSql());
        return $sql;
    }

    function insertReferido($lead, $leadEtapa, $leadEtapa2, $leadCampanha, $leadSectorista, $actividad, $actividad2)
    {
        DB::beginTransaction();
        $status = true;
        try {
            if (empty(self::getLeadByNum($lead['NUM_DOC'], $lead['PERIODO']))) {
                DB::table('WEBBE_LEAD')->insert($lead);
            }
            DB::table('WEBBE_LEAD_ETAPA')->insert($leadEtapa);
            DB::table('WEBBE_LEAD_ETAPA')->insert($leadEtapa2);
            if (empty(self::getCampaña($lead['NUM_DOC'], $lead['PERIODO']))) {
                DB::table('WEBVPC_LEAD_CAMP_EST')->insert($leadCampanha);
            } //die();
            DB::table('WEBBE_LEAD_SECTORISTA')->insert($leadSectorista);
            DB::table('WEBBE_ACTIVIDAD')->insert($actividad);
            DB::table('WEBBE_ACTIVIDAD')->insert($actividad2);
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }

    function eliminarLead($leadEliminado, $actividad, $ejecutivo, $empresa, $registro_JEFE)
    {
        DB::beginTransaction();
        $status = 0;

        try {
           
            if($registro_JEFE!=null){
                $notification = new eNotification();
                $notification->setValue('_idTipo', '10');
                $notification->setValue('_registro', $registro_JEFE);
                $notification->setValue('_contenido', "El ejecutivo " . strtoupper($ejecutivo) . " quiere eliminar " . strtoupper($empresa) . " de su lista de leads");
                $notification->setValue('_fechaNotificacion', Carbon::now());
                $notification->setValue('_flgLeido', '0');
                $notification->setValue('_fechaLeido', null);
                $notification->setValue('_valor1', null);
                $notification->setValue('_valor2', null);
                $notification->setValue('_valor3', null);
                $notification->setValue('_valor4', null);
                $notification->setValue('_url', '/be/actividades?documento=' . $leadEliminado['NUM_DOC'] . '&razonSocial=');
                $notification = $notification->setValueToTable();
                DB::table('T_WEB_NOTIFICACIONES')->insert($notification);
                DB::table('WEBBE_LEAD')
                ->where('NUM_DOC', $leadEliminado['NUM_DOC'])
                ->where('PERIODO', $leadEliminado['PERIODO'])
                ->update(['VERIFICADO' => "1"]);
                $status=1;
            }else{
                DB::table('WEBBE_LEAD')
                ->where('NUM_DOC', $leadEliminado['NUM_DOC'])
                ->where('PERIODO', $leadEliminado['PERIODO'])
                ->update(['ESTADO_LEAD' => "0"]);
                $status=2;
            }
            DB::table('WEBBE_LEAD_ELIMINACION')->insert($leadEliminado);
            DB::table('WEBBE_ACTIVIDAD')->insert($actividad);
            DB::commit();
        } catch (\Exception $e) {
            $status = 0;
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }
    static function getBancos()
    {
        $sql = DB::table('WEBVPC_BANCOS')
            ->select('CODIGO', 'NOMBRE');
        return $sql;
    }

    function getInfoReasignacion($periodo, $documento)
    {
        $sql = DB::table('WEBVPC_CLIENTES_VPC as WLP')
            ->select(
                'WL.PERIODO',
                'WLP.NUM_DOC',
                'WLP.COD_UNICO',
                'WLP.CODSBS AS COD_SBS',
                'WLP.BANCA AS BANCA',
                DB::Raw('ISNULL(WLP.NOMBRE_CLIENTE, WL.NOMBRE) AS NOMBRE'),
                'WLP.CLASIF_SBS_SF',
                'WLP.FEVE',
                DB::Raw("WLP.DEUDA_DIRECTA AS DEUDA_DIRECTA"),
                'WLS.COD_SECT_UNIQ_EN',
                DB::Raw("CASE WHEN LEL.NUM_DOC IS NULL THEN UPPER(WET.NOMBRE) ELSE 'ELIMINADO' END AS ETAPA_NOMBRE"),
                DB::Raw("CONVERT(DATE,CASE WHEN LEL.NUM_DOC IS NULL THEN WLET.FECHA_REGISTRO ELSE LEL.FECHA_ELIMINACION END) AS FECHA_ETAPA"),
                DB::Raw("ISNULL(MOT.DESC_MOTIVO,'')+'-'+ISNULL(DET.DESC_DETALLE,'') as MOTIVO_DETALLE"),
                DB::Raw("WLS.REGISTRO_EN AS REGISTRO_ASIG"),
                DB::Raw("UPPER(WLP.REGISTROCARGO) AS REGISTROCARGO"),
                'WU.NOMBRE AS EJECUTIVO_NOMBRE',
                DB::Raw("WLP.NBR_SECTORISTA"),
                DB::Raw("WLP.ENCARGADO AS NOMBRE_SECTORISTA"),
                DB::Raw("WLP.COD_SECT AS COD_SECTORISTA"),
                DB::Raw("WLS.COD_SECTORISTA AS COD_SECTORISTA_ASIGN"),
                DB::Raw("CONVERT(DATE,WAC.FECHA_ACTIVIDAD) AS FECHA_ACTIVIDAD")
            )
            // se necesita un full outer para verificar tanto en la tabla de prospectos como en la de referidos
            ->join('WEBBE_LEAD as WL', function ($join) use ($periodo) {
                $join->on('WL.NUM_DOC', '=', 'WLP.NUM_DOC');
            }, null, null, 'full outer')

            ->leftJoin('WEBBE_LEAD_SECTORISTA as WLS', function ($join) use ($periodo) {
                $join->on(DB::raw($periodo), '=', 'WLS.PERIODO');
                $join->on('WL.NUM_DOC', '=', 'WLS.NUM_DOC');
                $join->on('WLS.FLG_ULTIMO', '=', DB::raw(1));
            })

            ->leftJoin('WEBVPC_USUARIO as WU', function ($join) use ($periodo) {
                $join->on('WU.REGISTRO', '=', 'WLS.REGISTRO_EN');
            })

            ->leftJoin('WEBBE_LEAD_ETAPA as WLET', function ($join) use ($periodo) {
                /*$join->on('WLET.PERIODO', '=', DB::raw($periodo));*/
                $join->on('WLET.NUM_DOC', '=', 'WLP.NUM_DOC');
                $join->on('WLET.FLG_ULTIMO', '=', DB::raw(1));
            })

            ->leftJoin('WEBBE_ETAPA as WET', function ($join) {
                $join->on('WET.ID_ETAPA', '=', 'WLET.ID_ETAPA');
            })
            ->leftJoin('WEBBE_LEAD_ELIMINACION AS LEL', function ($join) {
                $join->on('LEL.NUM_DOC', '=', 'WL.NUM_DOC');
                $join->on('LEL.FECHA_ELIMINACION', '>=', 'WLET.FECHA_REGISTRO');
            })
            ->leftJoin('WEBBE_CAT_MOTIVO_ELIMINAR as MOT', function ($join) {
                $join->on('MOT.ID_MOTIVO', '=', 'LEL.MOTIVO');
            })
            ->leftJoin('WEBBE_CAT_DETALLE_ELIMINAR as DET', function ($join) {
                $join->on('DET.ID_DETALLE', '=', 'LEL.DETALLE');
                $join->on('DET.ID_MOTIVO', '=', 'MOT.ID_MOTIVO');
            })
            ->leftJoin(DB::Raw("(SELECT *,ROW_NUMBER() OVER(PARTITION BY NUM_DOC ORDER BY FECHA_ACTIVIDAD DESC) RNK  FROM WEBBE_ACTIVIDAD
                                     WHERE TIPO='" . "VISITA" . "') WAC"), function ($join) {

                $join->on('WAC.NUM_DOC', '=', 'WL.NUM_DOC');
                $join->on('WAC.RNK', '=', DB::raw(1));
            })
            ->orderBy('WL.PERIODO', 'DESC');

        if ($documento) {
            $sql = $sql->where('WLP.NUM_DOC', $documento)
                ->orWhere('WL.NUM_DOC', $documento);
        }
        //dd($documento);
        return $sql;
    }

    function getDetalleVolumenPAP($datos)
    {
        $sql = DB::table('WEBBE_LEAD_ECOSISTEMA_ORDENANTES as WLEO')
            ->select(DB::raw("CASE WHEN PAP_2018>0 THEN cast(CAST (PAP_2018 AS NUMERIC(20,1)) as varchar) ELSE '0' END AS PAP_2018"), DB::raw("CASE WHEN PAP_2019>0 THEN cast(CAST (PAP_2019 AS NUMERIC(20,1)) as varchar) ELSE '0' END AS PAP_2019"), 'CU_ORDENANTE', 'NOMBRE_ORDENANTE AS NOMBRE_COMPLETO', 'BANCA', 'EJECUTIVO AS EN', 'NOMBRE_ZONAL', 'NRODOC', 'NOMBRE_JEFE')
            ->where('NRODOC', '=', $datos['documento'])
            ->orderBy(DB::raw('PAP_2018+PAP_2019'), 'DESC')
            ->get();
        $cant = $sql->count();
        return ($cant > 0) ? $sql : 0;
    }

    function getDetalleVolumenPAP2($datos)
    {
        $sql = DB::table('WEBBE_LEAD_ECOSISTEMA_ORDENANTES as WLEO')
            ->select(DB::raw("CASE WHEN PAP_2018>0 THEN cast(CAST (PAP_2018 AS NUMERIC(20,1)) as varchar) ELSE '0' END AS PAP_2018"), DB::raw("CASE WHEN PAP_2019>0 THEN cast(CAST (PAP_2019 AS NUMERIC(20,1)) as varchar) ELSE '0' END AS PAP_2019"), 'CU_ORDENANTE', 'NOMBRE_ORDENANTE AS NOMBRE_COMPLETO', 'BANCA', 'EJECUTIVO AS EN', 'NOMBRE_ZONAL', 'NRODOC', 'NOMBRE_JEFE')
            ->where('NRODOC', '=', $datos['documento'])
            ->orderBy(DB::raw('PAP_2018+PAP_2019'), 'DESC')
            ->get();
        return $sql;
    }

    function getCantCliwithDatosOrdenantes($datos)
    {
        $sql = DB::table('WEBBE_LEAD_ECOSISTEMA_ORDENANTES as WLEO')
            ->where('NRODOC', '=', $datos['documento'])
            ->get()
            ->count();
        return $sql;
    }

    function getSectoristaArbol($login)
    {
        $sql = DB::table('ARBOL_DIARIO_2 as AD')
            ->where('LOGIN', '=', $login)
            ->get()
            ->first();
        return $sql;
    }

    function getLeadByNum($numdoc, $periodo)
    {
        $sql = DB::table('WEBBE_LEAD as WL')
            ->where('NUM_DOC', '=', $numdoc)
            ->where('PERIODO', '=', $periodo)
            ->get()
            ->first();
        return $sql;
    }

    function getLeadByNumDesc($numdoc)
    {
        $sql = DB::table('WEBBE_LEAD as WL')
            ->where('NUM_DOC', '=', $numdoc)
            ->orderBy('PERIODO', 'DESC')
            ->get()
            ->first();
        return $sql;
    }

    function getCampaña($numdoc, $periodo)
    {
        $sql = DB::table('WEBVPC_LEAD_CAMP_EST as WL')
            ->join(DB::raw("(SELECT * FROM WEBVPC_CAMP_EST WHERE BANCA='BE' AND TIPO='PROSPECTOS') as WCE"), function ($join) {
                $join->on('WL.ID_CAMP_EST', '=', 'WCE.ID_CAMP_EST');
            })
            ->where('WL.NUM_DOC', '=', $numdoc)
            ->where('WL.PERIODO', '=', $periodo)
            ->get()
            ->first();
        return $sql;
    }

    function ClientesVPC($numdoc = null, $codunico = null)
    {
        $sql = DB::table('WEBVPC_CLIENTES_VPC as CVPC')
            ->select('NUM_DOC', 'COD_UNICO', 'CVPC.BANCA AS BANCA_ACTUAL', 'CVPC.NOMBRE_SECTORISTA_ACTUAL', 'CVPC.REGISTRO_SECTORISTA_ACTUAL', 'CVPC.COD_CORTO AS COD_CORTO_ACTUAL', 'COD_SECT_UNIQ', 'CODSECTORISTA', 'NOMBRE_CLIENTE', 'FEVE', 'COD_SBS', 'CLASIF_SBS_SF', 'CVPC.DEUDA_DIRECTA');
        if ($numdoc) {
            $sql = $sql->where('CVPC.NUM_DOC', '=', $numdoc);
        }
        if ($codunico) {
            $sql = $sql->where('CVPC.COD_UNICO', '=', $codunico);
        }
        $sql = $sql->get()->first();
        return $sql;
    }

    function Reasignaciones($numdoc, $codunico)
    {
        $sql = DB::table('WEBVPC_RESC_ASIG_NUEVAS AS WRAN')
            ->where('WRAN.FLG_PENDIENTE', '=', DB::raw('1'));
        if ($numdoc) {
            $sql = $sql->where('WRAN.NUM_DOC', '=', $numdoc);
        }
        if ($codunico) {
            $sql = $sql->where('WRAN.COD_UNICO', '=', $codunico);
        }

        $sql = $sql->get()
            ->first();
        return $sql;
    }

    function getDatosCLientesVPC($numdoc)
    {
        $sql = DB::table(DB::Raw('WEBVPC_CLIENTES_VPC as WCV'))
            ->select("WCV.*","WCV.NOMBRE_CLIENTE AS NOMBRE_COMPLETO")
            ->where('WCV.NUM_DOC', '=', $numdoc)
            ->get()
            ->first();
        return $sql;
    }

    function LeadCampEstByNum($numdoc, $periodo)
    {
        return DB::table('WEBVPC_LEAD_CAMP_EST')
            ->where('NUM_DOC', $numdoc)
            ->where('PERIODO', $periodo)
            ->get()
            ->first();
    }


    function reasignar($nuevolead, $lead = null, $nuevacampaña, $campañanueva = null, $lead_sec, $lead_et, $lead_act, $data)
    {
        DB::beginTransaction();
        $status = true;
        try {
            if (!empty($nuevolead) && $nuevolead) {
                DB::table('WEBBE_LEAD')->insert($lead);
            }

            DB::table('WEBBE_LEAD_SECTORISTA')
                ->where('NUM_DOC', $data->getValue('_num_doc'))
                ->update(['FLG_ULTIMO' => "0"]);

            DB::table('WEBBE_LEAD_ETAPA')
                ->where('NUM_DOC', $data->getValue('_num_doc'))
                ->update(['FLG_ULTIMO' => "0"]);

            DB::table('WEBBE_LEAD_SECTORISTA')->insert($lead_sec);

            if (!empty($nuevacampaña) && $nuevacampaña) {
                DB::table('WEBVPC_LEAD_CAMP_EST')->insert($campañanueva);
            }

            DB::table('WEBBE_LEAD_ETAPA')->insert($lead_et);

            DB::table('WEBBE_ACTIVIDAD')->insert($lead_act);
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }
}
