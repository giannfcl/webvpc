<?php
namespace App\Model\BE;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class AddContacto extends Model

{
	protected $table = 'WEBBE_ADD_CONTACTO';
    
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey =['ID_CONTACTO'];
					        

        /**
     * The name of the "created at" column.
     *
     * @var string
     */
     public $timestamps = false;
    
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'NOMBRE'
        ];


    /**
     * Devuelve todos los datos del lead
     *
     * @param string $id Id del contacto
     * @return table
     */
    function getByContactos($contactos){

        $sql = DB::table('WEBBE_ADD_CONTACTO as WACO')
                    ->select('WACO.ID_CONTACTO','WACO.PERIODO','WACO.REGISTRO_EN','WACO.TIPO_CONTACTO','WACO.VALOR','WACO.ANEXO','WACO.FEEDBACK','WACO.TIPO_REGISTRO_CONTACTO')
                    //->orderBy('ID_CONTACTO','asc')
                    ->orderBy('WACO.FEEDBACK','desc')
                    ->orderBy('TIPO_REGISTRO_CONTACTO','desc')
                    ->orderBy('TIPO_CONTACTO','desc')
                    ->whereIn('ID_CONTACTO',$contactos);

        return $sql;
    
    }

    function registrar($data){
        DB::beginTransaction();
        $status = true;
        try {
            DB::table('WEBBE_ADD_CONTACTO')
            ->insert($data);
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }

    function actualizar($data){
        DB::beginTransaction();
        $status = true;
        try {
            DB::table('WEBBE_ADD_CONTACTO')
            ->where('ID_CONTACTO',$data['ID_CONTACTO'])
            ->where('PERIODO',$data['PERIODO'])
            ->update(array_except($data,['ID_CONTACTO','PERIODO']));
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }

    function actualizarFeedback($data){
        DB::beginTransaction();
        $status = true;

        try {
            DB::table('WEBBE_ADD_CONTACTO')
            ->where('ID_CONTACTO',$data['ID_CONTACTO'])
            //->where('PERIODO',$data['PERIODO'])
            ->where('VALOR',$data['VALOR'])
            ->update(array_except($data,['ID_CONTACTO','PERIODO','VALOR']));
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }
}