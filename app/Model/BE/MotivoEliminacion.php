<?php

namespace App\Model\BE;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class MotivoEliminacion extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'WEBBE_ZONAL';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = null;
	public $incrementing = false;

    function get($motivo,$detalle) {
        $sql = DB::table('WEBBE_CAT_MOTIVO_ELIMINAR as WME')
            ->join('WEBBE_CAT_DETALLE_ELIMINAR as WDE','WME.ID_MOTIVO','=','WDE.ID_MOTIVO')
            ->select('WME.ID_MOTIVO','WDE.ID_DETALLE','DESC_MOTIVO','DESC_DETALLE')
            ->where('WME.ID_MOTIVO',$motivo);
        if ($detalle){
            $sql = $sql->where('WDE.ID_DETALLE',$detalle);
        }
        return $sql;
    }

    function getAC($motivo,$detalle) {

        $sql = DB::table('AC_CAT_MOTIVO_ELIMINAR as AME')
            ->join('AC_CAT_DETALLE_ELIMINAR as ADE','AME.ID_MOTIVO','=','ADE.ID_MOTIVO')
            ->select('AME.ID_MOTIVO','ADE.ID_DETALLE','DESC_MOTIVO','DESC_DETALLE')
            ->where('AME.ID_MOTIVO',$motivo);
        
        if ($detalle){
            $sql = $sql->where('ADE.ID_DETALLE',$detalle);
        }
        return $sql;
    }

}