<?php

namespace App\Model\BE;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date as Carbon;
class Actividad extends Model {

    protected $table = 'WEBBE_ACTIVIDAD';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = ['ACTIVIDAD_ID'];

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'NUM_DOC',
        'REGISTRO_EN',
        'FECHA_ACTIVIDAD',
        'TITULO',
        'TIPO',
        'FECHA',
        'FLG_RENOVACION_LINEA',
        'UBICACION',
        'TEMAS_COMERCIALES',
        'TEMAS_CREDITICIOS',
        'FECHA_REGISTRO',
        'PERIODO',
        'ID_CAMP_EST'
    ];

    /**
     *
     * @return table
     */
    static function getActividades($numDoc, $periodo) {
        $textIbk = "UPPER(CONVERT(NVARCHAR,[WU].NOMBRE))";
        $textClientes = "UPPER(CONVERT(NVARCHAR,ISNULL(WC.NOMBRE,'')+' '+ISNULL(WC.APELLIDO_PATERNO,'')+' '+ISNULL(WC.APELLIDO_MATERNO,'')))";

        //Paso 2. Agregamos cada campo a la consulta de agrupamiento
        //consulta agrupada - es necesario cruzar leadCampanha - CampanhaInstancia - Campanha - Gestion
        $joinIbk = DB::table('WEBBE_ACTIVIDAD_PARTICIPANTE as WAP')
                ->select(DB::raw("'|' + UPPER(CONVERT(NVARCHAR,[WU].NOMBRE))"))
                ->join('WEBVPC_USUARIO AS WU', 'WAP.REGISTRO_EN', '=', 'WU.REGISTRO')
                ->whereRaw('WA.ACTIVIDAD_ID = WAP.ACTIVIDAD_ID');

        $script1 = $joinIbk->toSql();
        $scriptsIbk = "STUFF((" . $script1 . " FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1, '') CONTACTO_IBK";

        $joinClientes = DB::table('WEBBE_ACTIVIDAD_PARTICIPANTE as WAP')
                ->select(DB::raw("'|' + UPPER(CONVERT(NVARCHAR,ISNULL(WC.NOMBRE,'')+' '+ISNULL(WC.APELLIDO_PATERNO,'')+' '+ISNULL(WC.APELLIDO_MATERNO,'')))"))
                ->join('WEBBE_CONTACTO AS WC', 'WAP.ID_CONTACTO', '=', 'WC.ID_CONTACTO')
                ->whereRaw('WA.ACTIVIDAD_ID = WAP.ACTIVIDAD_ID')
                ->whereRaw('WC.FLG_VIGENTE = 1 ');

        $script2 = $joinClientes->toSql();
        $scriptsClientes = "STUFF((" . $script2 . " FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1, '') CONTACTO_CLIENTES";

        $sql = DB::table('WEBBE_ACTIVIDAD as WA')
                ->select('WA.ACTIVIDAD_ID', 'WA.NUM_DOC', 'WA.REGISTRO_EN', 'WA.FECHA_ACTIVIDAD', 'WA.TITULO', 'WA.TIPO', 'WA.FLG_RENOVACION_LINEA', 'WA.TEMAS_COMERCIALES',DB::Raw('SUBSTRING(WA.TEMAS_COMERCIALES,0,100) AS COMERCIAL_M'),
                DB::Raw('SUBSTRING(WA.TEMAS_COMERCIALES,100,1000) AS COMERCIAL_O'), 'WA.TEMAS_CREDITICIOS', 'WA.ID_CAMP_EST','WCE.NOMBRE AS NOMBRE_ACCION','WCE.TIPO AS TIPO_ACCION','WU.NOMBRE',DB::raw($scriptsIbk), DB::raw($scriptsClientes),'WA.FLG_LINKEDIN')
                //->where('WA.PERIODO', $periodo)
                ->join('WEBVPC_USUARIO AS WU','WU.REGISTRO','=','WA.REGISTRO_EN')
                ->leftJoin ('WEBVPC_CAMP_EST AS WCE', 'WCE.ID_CAMP_EST','=','WA.ID_CAMP_EST')
                ->where('WA.NUM_DOC', $numDoc)
                ->orderBy('FECHA_ACTIVIDAD', 'desc');


        return $sql;
    }


     static function buscarVisitasRepetidas($numDoc,$registro,$fecha) {
        $sql=DB::table('WEBBE_ACTIVIDAD')
        ->select()
        ->where('NUM_DOC','=',$numDoc)
        ->where('TIPO','=','VISITA')
        ->where('FECHA_ACTIVIDAD','=',$fecha);
        return $sql;
    }




    function getFechaAsignacion($periodo,$lead){
        $sql=DB::table('WEBBE_LEAD_SECTORISTA')
        ->select('FECHA_REGISTRO','FLG_CLIENTE')
        ->where('PERIODO','=',$periodo)
        ->where('NUM_DOC','=',$lead)
        ->where('FLG_ULTIMO','=',1);
        
        return $sql;
        
    }

    static function getComunicaciones($numDoc){
        $sql=DB::table('WEBBE_COMUNICACIONES')
        ->select('TIPO_COMUNICACION','FECHA_ENVIO')
        ->where('NUM_DOC','=',$numDoc)
        ->where('FLG_ACTIVO','=',1);
        
        return $sql;
    }

    function insertarActividad($data, $participantes,$etapas,$actividades,$accionesComerciales,$updateEtapas,$encargados,$visualizaciones,$notas) {
      
        //dd($data, $participantes,$etapas,$actividades,$accionesComerciales,$updateEtapas,$encargados,$visualizaciones,$notas);
        DB::beginTransaction();
        $status = true;
        try {

            $id = DB::table('WEBBE_ACTIVIDAD')->insertGetId($data);

            if($updateEtapas){
                DB::table('WEBBE_LEAD_ETAPA')
                    ->where('NUM_DOC', '=', $data['NUM_DOC'])
                    ->update(['FLG_ULTIMO' => 0]);    
            }

            foreach ($etapas as $etapa) {
                DB::table('WEBBE_LEAD_ETAPA')->insert($etapa);
            }

            foreach ($actividades as $actividad) {
                DB::table('WEBBE_ACTIVIDAD')->insert($actividad);   
            }

            foreach ($accionesComerciales as $accionComercial) {
                $accionComercial['ID_ACTIVIDAD']=$id;
                DB::table('WEBVPC_LEAD_CAMP_EST')->insert($accionComercial);   
            }

            foreach ($participantes as $participante) {
                $participante['ACTIVIDAD_ID'] = $id;
                DB::table('WEBBE_ACTIVIDAD_PARTICIPANTE')->insert($participante);
            }

            foreach ($encargados as $encargado) {                
                DB::table('WEBBE_ACCION_VISUALIZACION')->insert($encargado);
            }

            foreach ($visualizaciones as $visualizacion) {     
                if($visualizacion!=NULL){           
                    DB::table('WEBBE_ACCION_VISUALIZACION')->insert($visualizacion);
                }
            }

            foreach ($notas as $nota) {               
                if($nota!=NULL){
                    $idNota = DB::table('WEBBE_NOTAS_LEAD')->insertGetId($nota);
                }
            }

            DB::commit();
            return true;
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }

}
