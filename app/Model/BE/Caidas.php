<?php
namespace App\Model\BE;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class Caidas extends Model{

 function getCaidasProyectadas($periodo,$jefatura= null,$zonal= null,$banca= null){
 	
 		$sql1 = DB::table('DBO.WEBNOTE_OPERACIONES_NUEVAS as OP')
			->select('WU.BANCA', 'WZ.ZONAL', 'WU.NOMBRE AS EJECUTIVO', 'LE.NOMBRE AS CLIENTE', 'PRODUCTO', 
		 		 DB::raw("(MONTO_CERT/1000000)  as MONTO_CAIDA"),
				 DB::raw("'Precancelación' as FECHA_VENCIMIENTO")
			)
			->join('WEBVPC_PRODUCTO AS PC',function($join){
				$join->on('PC.ID_PRODUCTO','=','OP.ID_PRODUCTO');
			})
			->join('WEBVPC_USUARIO as WU',function($join){
				$join->on('OP.REGISTRO_EN','=','WU.REGISTRO'); 
			})
			->join('WEBBE_ZONAL as WZ',function($join){
				$join->on('WU.ID_ZONA','=','WZ.ID_ZONAL'); 
			})
			->leftJoin('WEBBE_LEAD as LE',function($join){
				$join->on('LE.COD_UNICO','=','OP.COD_UNICO');
				//$join->on('LE.PERIODO','=',DB::raw("(SELECT MAX(PERIODO) FROM WEBBE_LEAD)"));
			})
			->where('ID_TIPO_OPERACION', '=', 2)
			->where('LE.PERIODO', '=',DB::raw("(SELECT MAX(PERIODO) FROM WEBBE_LEAD WHERE PERIODO<='".$periodo."')"))
			->where(DB::raw('MONTH(FECHA_DESEMBOLSO)'),'=',DB::raw("SUBSTRING('".$periodo."', 5, 2)"))
		    ->where(DB::raw('YEAR(FECHA_DESEMBOLSO)'),'=',DB::raw("SUBSTRING('".$periodo."', 1, 4)"));

			if($jefatura){
				$sql1 = $sql1->where('WU.ID_CENTRO',$jefatura);
			}

			if($zonal){
				$sql1 = $sql1->where('WU.ID_ZONA',$zonal);
			}

			if($banca){
				$sql1 = $sql1->where('WU.BANCA',$banca);
			}

    
		$sql = DB::table('DBO.WEBVPC_VENC_AMORT as VA')
			->select(
				'WU.BANCA', 'WZ.ZONAL', 'WU.NOMBRE AS EJECUTIVO', 'LE.NOMBRE AS CLIENTE', 'PRODUCTO', 
		 		 DB::raw("(MONTO_SOLES/1000000) as MONTO_CAIDA"),
				DB::raw("CAST(FECHA_VENCIMIENTO AS VARCHAR(10)) as FECHA_VENCIMIENTO")
			)
			->join('WEBVPC_PRODUCTO AS PC',function($join){
				$join->on('PC.ID_PRODUCTO','=','VA.ID_PRODUCTO');
			})
			->join('WEBVPC_USUARIO as WU',function($join){
				$join->on('VA.REGISTRO_EN','=','WU.REGISTRO'); 
			})
			->join('WEBBE_ZONAL as WZ',function($join){
				$join->on('WU.ID_ZONA','=','WZ.ID_ZONAL'); 
			})
			->leftJoin('WEBBE_LEAD as LE',function($join){
				$join->on('LE.COD_UNICO','=','VA.COD_UNICO');
				//$join->on('LE.PERIODO','=',DB::raw("(SELECT MAX(PERIODO) FROM WEBBE_LEAD)"));
			})
			->where('VA.FECHA','=', DB::raw("(SELECT FECHA_ACT_SD FROM WEBNOTE_FECHAS WHERE PERIODO = '".$periodo."')"))
			->where('LE.PERIODO', '=',DB::raw("(SELECT MAX(PERIODO) FROM WEBBE_LEAD WHERE PERIODO<='".$periodo."')"))
			->where(DB::raw('MONTH(FECHA_VENCIMIENTO)'),'=',DB::raw("SUBSTRING('".$periodo."', 5, 2)"))
		    ->where(DB::raw('YEAR(FECHA_VENCIMIENTO)'),'=',DB::raw("SUBSTRING('".$periodo."', 1, 4)"))
		    ->where(DB::raw("((FLG_SE_RENUEVA = 0 AND VENC_AMORT = 'V') OR (VENC_AMORT = 'A' AND FECHA_VENCIMIENTO"), ">", DB::raw("(SELECT FECHA_ACT_SD FROM WEBNOTE_FECHAS WHERE PERIODO = '".$periodo."')))"))
		    ->unionAll($sql1);
			
			if($jefatura){
				$sql = $sql->where('WU.ID_CENTRO',$jefatura);
			}

			if($zonal){
				$sql = $sql->where('WU.ID_ZONA',$zonal);
			}

			if($banca){
				$sql = $sql->where('WU.BANCA',$banca);
			}

			

		  return $sql;
	}
}	