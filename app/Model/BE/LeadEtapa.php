<?php
namespace App\Model\BE;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class LeadEtapa extends Model

{
	protected $table = 'WEBBE_LEAD_ETAPA';
    
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey =['PERIODO','NUM_DOC'];
					        

        /**
     * The name of the "created at" column.
     *
     * @var string
     */
     public $timestamps = false;
    
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'NOMBRE'
        ];
                                                                                                                                                                                                                                    

    /**
     *
     * @return table
     */
    function actualizar($data,$actividad,$tipo=null){
        DB::beginTransaction();
        $status = true;
        try {            
            
            if($tipo=="accion"){

            DB::table('WEBBE_LEAD_ETAPA')
            ->where('NUM_DOC', $data['NUM_DOC'])
            ->where('ID_CAMP_EST',$data['ID_CAMP_EST'])
            ->where('TOOLTIP',$data['TOOLTIP'])
            ->update(['FLG_ULTIMO' => '0']);
            }
            else{
                DB::table('WEBBE_LEAD_ETAPA')
            ->where('NUM_DOC', $data['NUM_DOC'])
            ->where('ID_CAMP_EST',$data['ID_CAMP_EST'])
            //->where('TOOLTIP',$data['TOOLTIP'])
            ->update(['FLG_ULTIMO' => '0']);
            }


            DB::table('WEBBE_LEAD_ETAPA')->insert($data);
            DB::table('WEBBE_ACTIVIDAD')->insert($actividad);
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }
    /*
    function actualizarInicio($data,$hoy){
        //dd($data);
        $etapa=DB::table('WEBBE_LEAD_ETAPA')
            ->select('ID_ETAPA')
            ->where('NUM_DOC', $data['NUM_DOC'])
            ->where('ID_CAMP_EST',$data['ID_CAMP_EST'])
            ->where('TOOLTIP',$data['TOOLTIP'])
            ->where('FLG_ULTIMO','=','0')
            ->orderBy('ID_ETAPA','DESC')
            ->first();

        $inicio=DB::table('WEBVPC_LEAD_CAMP_EST')
            ->select('FECHA_INICIO')
            ->where('NUM_DOC', $data['NUM_DOC'])
            ->where('ID_CAMP_EST',$data['ID_CAMP_EST'])
            ->where('TOOLTIP',$data['TOOLTIP'])->first();

        
        if(($etapa->ID_ETAPA==8 || $etapa->ID_ETAPA==14) && $inicio->FECHA_INICIO>=$hoy){   
               DB::table('WEBVPC_LEAD_CAMP_EST')
                ->where('NUM_DOC', $data['NUM_DOC'])
                ->where('ID_CAMP_EST',$data['ID_CAMP_EST'])
                ->where('TOOLTIP',$data['TOOLTIP'])
                ->update(['FECHA_INICIO' => $hoy]);            
        }
    }
    */
}


    