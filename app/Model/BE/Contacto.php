<?php

namespace App\Model\BE;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class Contacto extends Model

{
    protected $table = 'WEBBE_CONTACTO';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = ['ID_CONTACTO'];


    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    public $timestamps = false;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'NOMBRE'
    ];


    /**
     * Devuelve todos los datos del lead
     *
     * @param string $id Id del contacto
     * @return table
     */
    function getByLead($documento)
    {

        $campos = [
            'TIPO_CONTACTO' => 'WCOE.TIPO_CONTACTO',
            'FLG_CONTACTO' => 'WCOE.FLG_CONTACTO',
        ];

        //Paso 2. Agregamos cada campo a la consulta de agrupamiento
        $scripts = [];
        $where = '';
        foreach ($campos as $key => $campo) {

            //consulta agrupada - es necesario cruzar leadCampanha - CampanhaInstancia - Campanha - Gestion
            $join = DB::table('WEBBE_CONTACTO_ENCUESTA as WCOE')
                ->select(DB::raw("'|' + " . $campo))
                ->whereRaw('WCO.ID_CONTACTO = WCOE.ID_CONTACTO');
            $script = $join->toSql();
            $scripts[] = "STUFF((" . $script . " FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1, '') " . $key;
        }

        $join = DB::table('WEBBE_CONTACTO_ENCUESTA as WCOE')
            ->select(DB::Raw('SUM(CONVERT(INT,WCOE.FLG_CONTACTO))'))
            ->whereRaw('WCO.ID_CONTACTO = WCOE.ID_CONTACTO');
        $scripts[2] = "(" . $join->toSql() . ")";


        $sql = DB::table('WEBBE_CONTACTO as WCO')
            ->select(
                'WCO.ID_CONTACTO',
                'WCO.NUM_DOC',
                'WCO.CLF_SF',
                'WCO.FLG_VIGENTE',
                'WCO.NUM_DOC_CONTACTO',
                'WCO.NOMBRE',
                'WCO.APELLIDO_PATERNO',
                'WCO.APELLIDO_MATERNO',
                'WCO.CARGO',
                'WCO.EMAIL',
                'WCO.DIRECCION',
                DB::raw($scripts[0]),
                DB::raw($scripts[1]),
                DB::raw($scripts[2] . ' SUMA')
            )
            ->where('WCO.FLG_VIGENTE', '>=', 1)
            ->where('WCO.VISUALIZAR', '=', 1)
            ->where('WCO.NUM_DOC', '=', $documento)
            ->orderBy(DB::raw($scripts[2]), 'desc')
            ->orderBy('WCO.ID_CONTACTO', 'desc');
        return $sql;
    }

    function actualizar($data)
    {
        DB::beginTransaction();
        $status = true;
        try {
            DB::table('WEBBE_CONTACTO')
                ->where('ID_CONTACTO', $data['ID_CONTACTO'])
                ->update(array_except($data, ['ID_CONTACTO']));
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }

    function setContacto($data, $addContacto)
    {
        DB::beginTransaction();
        $status = true;
        try {
            $id = DB::table('WEBBE_CONTACTO')->insertGetId($data);
            $addContacto['ID_CONTACTO'] = $id;
            DB::table('WEBBE_ADD_CONTACTO')->insert($addContacto);
            DB::commit();
            return $id;
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }

    function quitarContacto($data)
    {
        DB::beginTransaction();
        $status = true;
        $idContacto =  $data['ID_CONTACTO'];
        \Debugbar::info($idContacto);
        try {
            DB::table('WEBBE_CONTACTO')
                ->where('ID_CONTACTO', $idContacto)
                ->update(['FLG_VIGENTE' => '0']);
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }
}
