<?php
namespace App\Model\BE;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class Etapa extends Model

{
	protected $table = 'WEBBE_ETAPA';
    
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey =['PERIODO','NUM_DOC'];
					        

        /**
     * The name of the "created at" column.
     *
     * @var string
     */
     public $timestamps = false;
    
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'NOMBRE'
        ];
                                                                                                                                                                                                                                    

    /**
     *
     * @return table
     */
    function listar($flgHabilitado = null,$tipo=null){

        $sql = DB::table('WEBBE_ETAPA as WET')
                ->select('WET.ID_ETAPA','WET.NOMBRE')
                ->orderBy('WET.ORDEN');

        if ($flgHabilitado){
            $sql = $sql->where('WET.FLG_HABILITADO',$flgHabilitado);
        }

        if ($tipo){
            $sql=$sql->where('WET.TIPO','=',$tipo);
        }

        return $sql;
    }

}