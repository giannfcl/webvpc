<?php
namespace App\Model\BE;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class OperacionesNuevas extends Model{

	const ESCALA = 1000;
    const  ESCALA_JEFE = 1000000;

	protected $table = 'WEBNOTE_OPERACIONES_NUEVAS';

	function registrar($data){
		DB::beginTransaction();
        $status = true;
        try {
    		DB::table('WEBNOTE_OPERACIONES_NUEVAS')
            ->insert($data);
        	DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
	}

	function massiveUpdate($items){
		DB::beginTransaction();
        $status = true;
        try {
        	foreach ($items as $item) {
        		DB::table('WEBNOTE_OPERACIONES_NUEVAS')
	            ->where('ID_OPERACION', $item['ID_OPERACION'])
	            ->update(array_except($item, ['ID_OPERACION']));	
        	
        	}
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
	}
    
    function getOperacionesNuevas($periodo,$registro = null,$jefatura= null,$zonal= null,$banca= null,$certero= null,$cotizacion= null){		
		
		if(!$registro){
            $escala = self::ESCALA_JEFE;
        }
        else{
            $escala = self::ESCALA;
        }

		$sql = DB::table('WEBNOTE_OPERACIONES_NUEVAS as WON')
			->select(
				'WON.ID_OPERACION',
				'WL.NOMBRE as CLIENTE',
				'WP.PRODUCTO',
				'WCE.NOMBRE as ESTRATEGIA',
				DB::raw("WON.MONTO_CERT / ".$escala."as MONTO_CERT"),
				DB::raw("WON.MONTO_ENCOT / ".$escala." as MONTO_ENCOT"),
				'WON.FLG_DESEMBOLSADO',
				'WON.FLG_PERDIDO',
				'WON.FECHA_DESEMBOLSO',
				'WU.BANCA',
				'WU.NOMBRE as EJECUTIVO',
				'WZ.ZONAL'
			)
			->join('WEBBE_LEAD as WL',function($join){
				$join->on('WL.NUM_DOC','=','WON.NUM_DOC');
			})
			->join('WEBBE_LEAD_SECTORISTA as WLS',function($join){
				$join->on('WLS.NUM_DOC','=','WL.NUM_DOC'); 
				$join->on('WLS.PERIODO','=','WL.PERIODO'); 
			})
			->join('WEBVPC_USUARIO as WU',function($join){
				$join->on('WLS.REGISTRO_EN','=','WU.REGISTRO'); 
			})
			->join('WEBBE_ZONAL as WZ',function($join){
				$join->on('WU.ID_ZONA','=','WZ.ID_ZONAL'); 
			})
			->join('WEBVPC_PRODUCTO as WP',function($join){
				$join->on('WP.ID_PRODUCTO','=','WON.ID_PRODUCTO'); 
			})
			->leftJoin('WEBVPC_CAMP_EST as WCE',function($join){
				$join->on('WCE.ID_CAMP_EST','=','WON.ID_CAMP_EST');
			})
			->where('WL.PERIODO', '=',DB::raw("(SELECT MAX(PERIODO) FROM WEBBE_LEAD WHERE PERIODO<='".$periodo."')"))
			->where(DB::raw('MONTH(WON.FECHA_DESEMBOLSO)'),'>=',DB::raw("SUBSTRING('".$periodo."', 5, 2)"))
		    ->where(DB::raw('YEAR(WON.FECHA_DESEMBOLSO)'),'>=',DB::raw("SUBSTRING('".$periodo."', 1, 4)"))
			->where(DB::raw('MONTH(WON.FECHA_DESEMBOLSO)'),'<=',DB::raw("(MONTH(DATEADD(MONTH,1,(SELECT FECHA_ACT_SD FROM WEBNOTE_FECHAS WHERE PERIODO='".$periodo."'))) )"))
		    ->where(DB::raw('YEAR(WON.FECHA_DESEMBOLSO)'),'<=',DB::raw("(YEAR(DATEADD(MONTH,1,(SELECT FECHA_ACT_SD FROM WEBNOTE_FECHAS WHERE PERIODO='".$periodo."'))) )"));

			if($registro){
				$sql = $sql->where('WON.REGISTRO_EN',$registro);
			}

			if($jefatura){
				$sql = $sql->where('WU.ID_CENTRO',$jefatura);
			}

			if($zonal){
				$sql = $sql->where('WU.ID_ZONA',$zonal);
			}

			if($banca){
				$sql = $sql->where('WU.BANCA',$banca);
			}

			if($certero){
				$sql = $sql->where('WON.MONTO_CERT','>',0);
			}

			if($cotizacion){
				$sql = $sql->where('WON.MONTO_ENCOT','>',0);
			}
			//DD($sql->toSql());

		  return $sql;
	}

	function getOperacionesIniciales($periodo,$registro = null,$jefatura= null,$zonal= null,$banca= null,$certero= null,$cotizacion= null){	
		$sql = DB::table('WEBNOTE_OPERACIONES_NUEVAS_INICIAL as WONI')
			->select(
				'WONI.ID_OPERACION',
				'WL.NOMBRE as CLIENTE',
				'WP.PRODUCTO',
				'WCE.NOMBRE as ESTRATEGIA',
				DB::raw("WON.MONTO_CERT / 1000 as MONTO_CERT"),
				DB::raw("WON.MONTO_ENCOT / 1000 as MONTO_ENCOT"),
				'WONI.FLG_DESEMBOLSADO',
				'WONI.FLG_PERDIDO',
				'WONI.FECHA_DESEMBOLSO',
				'WU.BANCA',
				'WU.NOMBRE as EJECUTIVO',
				'WZ.ZONAL'
			)
			->join('WEBBE_LEAD as WL',function($join){
				$join->on('WL.NUM_DOC','=','WONI.NUM_DOC');
			})
			->join('WEBBE_LEAD_SECTORISTA as WLS',function($join){
				$join->on('WLS.NUM_DOC','=','WL.NUM_DOC'); 
				$join->on('WLS.PERIODO','=','WL.PERIODO'); 
			})
			->join('WEBVPC_USUARIO as WU',function($join){
				$join->on('WLS.REGISTRO_EN','=','WU.REGISTRO'); 
			})
			->join('WEBBE_ZONAL as WZ',function($join){
				$join->on('WU.ID_ZONA','=','WZ.ID_ZONAL'); 
			})
			->join('WEBVPC_PRODUCTO as WP',function($join){
				$join->on('WP.ID_PRODUCTO','=','WONI.ID_PRODUCTO'); 
			})
			->leftJoin('WEBVPC_CAMP_EST as WCE',function($join){
				$join->on('WCE.ID_CAMP_EST','=','WONI.ID_CAMP_EST');
			})			
			->where('WL.PERIODO', '=',DB::raw("(SELECT MAX(PERIODO) FROM WEBBE_LEAD WHERE PERIODO<='".$periodo."')"));
			if($periodo){
				$sql = $sql->where('WONI.PERIODO',$periodo);
			}
			//->where(DB::raw('MONTH(WONI.FECHA_DESEMBOLSO)'),'>=',DB::raw("SUBSTRING('".$periodo."', 5, 2)"))
		    //->where(DB::raw('YEAR(WONI.FECHA_DESEMBOLSO)'),'>=',DB::raw("SUBSTRING('".$periodo."', 1, 4)"));			
			if($registro){
				$sql = $sql->where('WONI.REGISTRO_EN',$registro);
			}

			if($jefatura){
				$sql = $sql->where('WU.ID_CENTRO',$jefatura);
			}

			if($zonal){				
				$sql = $sql->where('WU.ID_ZONA',$zonal);
			}

			if($banca){				
				$sql = $sql->where('WU.BANCA',$banca);
			}

			if($certero){
				$sql = $sql->where('WONI.MONTO_CERT','>',0);
			}

			if($cotizacion){
				$sql = $sql->where('WONI.MONTO_ENCOT','>',0);
			}		
		  return $sql;
	}

	function test(){
		$query = DB::table('WEBVPC_SALDOS_DIARIOS as WSD')
			->select(
				'FECHA',
				DB::raw("SUM(SALDO_PUNTA) AS SALDO")
			)
			->where('REGISTRO_EN','B10031')
			->where('FECHA','>=','2018-04-15')
			->groupBy('fecha');

		  return $query;
	}

	function getProyeccionOperacionesNuevas($periodo,$ejecutivo = null,$jefatura= null,$zonal= null,$banca= null){		
		$sql = $this->getOperacionesNuevas($periodo,$ejecutivo,$jefatura,$zonal,$banca);
		$sql->select(
			'WON.FECHA_DESEMBOLSO'
			,DB::raw("SUM(CASE WHEN (WP.TIPO_PRODUCTO = 'DIRECTAS' AND ID_TIPO_OPERACION IN ('1','3')) THEN MONTO_CERT ELSE 0 END) MONTO_DIR_NUEVO")
			,DB::raw("SUM(CASE WHEN (WP.TIPO_PRODUCTO = 'DIRECTAS' AND ID_TIPO_OPERACION='2') THEN MONTO_CERT ELSE 0 END) MONTO_DIR_PRECAN")
			,DB::raw("SUM(CASE WHEN (WP.TIPO_PRODUCTO = 'INDIRECTAS' AND ID_TIPO_OPERACION IN ('1','3')) THEN MONTO_CERT ELSE 0 END) MONTO_IND_NUEVO")
			,DB::raw("SUM(CASE WHEN (WP.TIPO_PRODUCTO = 'INDIRECTAS' AND ID_TIPO_OPERACION='2') THEN MONTO_CERT ELSE 0 END) MONTO_IND_PRECAN")
		)
			->groupBy('WON.FECHA_DESEMBOLSO');
			if($periodo){            
                $sql = $sql->where('fecha_desembolso','<=',DB::raw("(SELECT DATEADD(D, 30, FECHA_ACT_SD) FROM dbo.WEBNOTE_FECHAS WHERE PERIODO = '".$periodo."')"))
							->where('fecha_desembolso','>',DB::raw("(SELECT Dateadd(d,-30 , FECHA_ACT_SD) FROM dbo.WEBNOTE_FECHAS WHERE PERIODO = '".$periodo."')"));	
             }else{                                            
             	$sql = $sql->where('fecha_desembolso','<=',DB::raw("(SELECT DATEADD(D, 30, MAX(FECHA_ACT_SD)) FROM dbo.WEBNOTE_FECHAS)"))
						    ->where('fecha_desembolso','>',DB::raw("(SELECT Dateadd(d,-30 , max(FECHA_ACT_SD))  FROM dbo.WEBNOTE_FECHAS)"));             
             }            
		return $sql;
	}	

	function getProyeccionOperacionesIniciales($periodo,$ejecutivo = null,$jefatura= null,$zonal= null,$banca= null){
		$sql = $this->getOperacionesIniciales($periodo,$ejecutivo,$jefatura,$zonal,$banca);
		$sql->select(
			'WONI.FECHA_DESEMBOLSO'
			,DB::raw("SUM(CASE WHEN (WP.TIPO_PRODUCTO = 'DIRECTAS' AND ID_TIPO_OPERACION IN ('1','3')) THEN MONTO_CERT ELSE 0 END) MONTO_DIR_NUEVO")
			,DB::raw("SUM(CASE WHEN (WP.TIPO_PRODUCTO = 'DIRECTAS' AND ID_TIPO_OPERACION='2') THEN MONTO_CERT ELSE 0 END) MONTO_DIR_PRECAN")
			,DB::raw("SUM(CASE WHEN (WP.TIPO_PRODUCTO = 'INDIRECTAS' AND ID_TIPO_OPERACION IN ('1','3')) THEN MONTO_CERT ELSE 0 END) MONTO_IND_NUEVO")
			,DB::raw("SUM(CASE WHEN (WP.TIPO_PRODUCTO = 'INDIRECTAS' AND ID_TIPO_OPERACION='2') THEN MONTO_CERT ELSE 0 END) MONTO_IND_PRECAN")
		)
			->groupBy('WONI.FECHA_DESEMBOLSO');
			/*if($periodo){            
                $sql = $sql->where('fecha_desembolso','<=',DB::raw("(SELECT DATEADD(D, 30, FECHA_ACT_SD) FROM dbo.WEBNOTE_FECHAS WHERE PERIODO = '".$periodo."')"))
							->where('fecha_desembolso','>',DB::raw("(SELECT FECHA_ACT_SD FROM dbo.WEBNOTE_FECHAS WHERE PERIODO = '".$periodo."')"));	
             }else{                                            
             	$sql = $sql->where('fecha_desembolso','<=',DB::raw("(SELECT DATEADD(D, 30, MAX(FECHA_ACT_SD)) FROM dbo.WEBNOTE_FECHAS)"))
						    ->where('fecha_desembolso','>',DB::raw("(SELECT max(FECHA_ACT_SD) FROM dbo.WEBNOTE_FECHAS)"));             
            } */           
		return $sql;
	}

	function getCompromiso($periodo,$ejecutivo = null,$jefatura= null,$zonal= null,$banca= null){
		$sql = DB::table('WEBNOTE_PROYECCION_INICIAL as WPI')
			->select('WP.TIPO_PRODUCTO',DB::raw("Sum(WPI.SALDO_PROYEC_CERT)     AS COMPROMISO"))
			->join('WEBVPC_PRODUCTO AS WP',function($join){
				$join->on('WP.ID_PRODUCTO','=','WPI.ID_PRODUCTO');
			})
			->join('WEBVPC_USUARIO as WU',function($join){
				$join->on('WPI.REGISTRO_EN','=','WU.REGISTRO'); 
			})
			->leftjoin('WEBVPC_USUARIO as WEZ',function($join){ 
                $join->on('WEZ.ID_ZONA','=','WU.ID_ZONA');
                $join->on('WEZ.ROL','=',DB::raw(\App\Entity\Usuario::ROL_GERENCIA_ZONAL_BE/*25*/)); })
            ->leftjoin('WEBVPC_USUARIO as WEJ',function($join){ 
                $join->on('WEJ.ID_CENTRO','=','WU.ID_CENTRO');
                $join->on('WEJ.ROL','=',DB::raw(\App\Entity\Usuario::ROL_JEFATURA_BE/*24*/)); }) 
			->where('WPI.FECHA','=',DB::raw("(SELECT FECHA_FOTO FROM  WEBNOTE_FECHA_FOTO  WHERE  APLICATIVO='EN_COLOC' AND PERIODO='".$periodo."')"))
			->whereIn('WP.TIPO_PRODUCTO',['DIRECTAS','INDIRECTAS']);
			if($ejecutivo){
				$sql = $sql->where('WU.REGISTRO',$ejecutivo);
			}
			if($jefatura){
				$sql = $sql->where('WU.ID_CENTRO',$jefatura);
			}

			if($zonal){
				$sql = $sql->where('WU.ID_ZONA',$zonal);
			}

			if($banca){
				$sql = $sql->where('WU.BANCA',$banca);
			}	
			$sql = $sql->groupBy('WP.TIPO_PRODUCTO');		
		return $sql;	
	}
	function getMesesOperacion(){
		$query = DB::table('WEBNOTE_FECHAS as WF')
			->select('WF.PERIODO')
			->orderBy('WF.PERIODO');
		return $query;			
	}
  	
  	function getOperacionesPerdidas($periodo,$jefatura= null,$zonal= null,$banca= null){  
		$sql = DB::table('WEBNOTE_OPERACIONES_NUEVAS as WON')
			->select(
				'WON.FECHA_DESEMBOLSO','WON.COD_UNICO','WON.REGISTRO_EN','WU.NOMBRE AS EJECUTIVO','WU.BANCA', 'WZ.ZONAL','WL.NOMBRE AS CLIENTE','WP.PRODUCTO',DB::raw("(WON.MONTO_CERT/1000000) AS MONTO_CERT"),DB::raw("(WON.MONTO_ENCOT/1000000) AS MONTO_ENCOT"),'WON.FLG_PERDIDO'				
			)
			->join('WEBVPC_PRODUCTO AS WP',function($join){
				$join->on('WP.ID_PRODUCTO','=','WON.ID_PRODUCTO');
			})
			->join('WEBVPC_USUARIO as WU',function($join){
				$join->on('WON.REGISTRO_EN','=','WU.REGISTRO'); 
			})
			->join('WEBBE_ZONAL as WZ',function($join){
				$join->on('WU.ID_ZONA','=','WZ.ID_ZONAL'); 
			})
			->leftJoin('WEBBE_LEAD as WL',function($join){
				$join->on('WL.COD_UNICO','=','WON.COD_UNICO');
				//$join->on('WL.PERIODO','=',$perLead);//DB::raw("(SELECT MAX(PERIODO) FROM WEBBE_LEAD)"));
			})
			->where('ID_TIPO_OPERACION', '=', 1)
			->where('FLG_PERDIDO', '=', 1)
			//->where('MONTO_CERT', '<>', 0)
			->where('WL.PERIODO', '=',DB::raw("(SELECT MAX(PERIODO) FROM WEBBE_LEAD WHERE PERIODO<='".$periodo."')"))
			->where(DB::raw('MONTH(FECHA_DESEMBOLSO)'),'=',DB::raw("SUBSTRING('".$periodo."', 5, 2)"))
		    ->where(DB::raw('YEAR(FECHA_DESEMBOLSO)'),'=',DB::raw("SUBSTRING('".$periodo."', 1, 4)"));

		    if($jefatura){
				$sql = $sql->where('WU.ID_CENTRO',$jefatura);
			}

			if($zonal){
				$sql = $sql->where('WU.ID_ZONA',$zonal);
			}

			if($banca){
				$sql = $sql->where('WU.BANCA',$banca);
			}
		  return $sql;
  	}

  	function getDesembolsosCotizacionFuturos($periodo,$jefatura= null,$zonal= null,$banca= null){ 
  	\Debugbar::info($periodo); 		  		
		$sql = DB::table('WEBNOTE_OPERACIONES_NUEVAS as WON')
			->select(
				'WON.FECHA_DESEMBOLSO','WON.COD_UNICO','WON.REGISTRO_EN','WU.NOMBRE AS EJECUTIVO','WU.BANCA', 'WZ.ZONAL','WL.NOMBRE AS CLIENTE','WP.PRODUCTO',DB::raw("(WON.MONTO_CERT/1000) AS MONTO_CERT"),DB::raw("(WON.MONTO_ENCOT/1000000) AS MONTO_ENCOT"),'WON.FLG_PERDIDO'				
			)
			->join('WEBVPC_PRODUCTO AS WP',function($join){
				$join->on('WP.ID_PRODUCTO','=','WON.ID_PRODUCTO');
			})
			->join('WEBVPC_USUARIO as WU',function($join){
				$join->on('WON.REGISTRO_EN','=','WU.REGISTRO'); 
			})
			->join('WEBBE_ZONAL as WZ',function($join){
				$join->on('WU.ID_ZONA','=','WZ.ID_ZONAL'); 
			})
			->leftJoin('WEBBE_LEAD as WL',function($join){
				$join->on('WL.COD_UNICO','=','WON.COD_UNICO');
				//$join->on('WL.PERIODO','=',DB::raw("(SELECT MAX(PERIODO) FROM WEBBE_LEAD WHERE PERIODO<='".$periodo."')"));
			})
			->where('ID_TIPO_OPERACION', '=', 1)
			->where('FLG_PERDIDO', '<>', 1)
			->where('MONTO_ENCOT', '<>',0 )
			->where('FLG_DESEMBOLSADO', '<>',1 )	
			->where('WL.PERIODO', '=',DB::raw("(SELECT MAX(PERIODO) FROM WEBBE_LEAD WHERE PERIODO<='".$periodo."')"))		
			->where(DB::raw('(FECHA_DESEMBOLSO)'),'>=',DB::raw("(SELECT FECHA_ACT_SD FROM WEBNOTE_FECHAS WHERE PERIODO='".$periodo."')"))	;	    
		    
		    if($jefatura){
				$sql = $sql->where('WU.ID_CENTRO',$jefatura);
			}

			if($zonal){
				$sql = $sql->where('WU.ID_ZONA',$zonal);
			}

			if($banca){
				$sql = $sql->where('WU.BANCA',$banca);
			}
		  return $sql;
  	}

  	function validarPeriodo($periodo){
	    if($periodo==\App\Entity\BE\SaldosDiarios::getParametros('PERIODO',null)){
	            $periodo =$periodo -2;
	    }
	    else{
	            $periodo =$periodo -1;
	    }
	    return $periodo;
  	}
}