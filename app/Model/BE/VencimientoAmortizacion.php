<?php
namespace App\Model\BE;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;
use App\Entity\Usuario as Usuario;

class VencimientoAmortizacion extends Model

{
	protected $table = 'WEBVPC_VENC_AMORT';

  function test(){
    $query = DB::table('WEBVPC_SALDOS_DIARIOS as WSD')
      ->select('FECHA',DB::raw('CAST(ROUND(SUM(SALDO_PUNTA),0) AS INT) SALDO_PUNTA'),DB::raw('CAST(ROUND(SUM(saldo_promedio),0) AS INT) SALDO_PROMEDIO'))
      ->where('REGISTRO_EN','B10031')
      ->groupBy('FECHA')
      ->orderBy('FECHA');
      return $query;
  }

	function massiveUpdate($items){
		DB::beginTransaction();
        $status = true;
        try {
        	foreach ($items as $item) {
        		DB::table('WEBVPC_VENC_AMORT')
	            ->where('FECHA', $item['FECHA'])
	            ->where('COD_UNICO', $item['COD_UNICO'])
	            ->where('ID_PRODUCTO', $item['ID_PRODUCTO'])
	            ->where('FECHA_VENCIMIENTO', $item['FECHA_VENCIMIENTO'])
	            ->update(array_except($item, ['FECHA','COD_UNICO','ID_PRODUCTO','FECHA_VENCIMIENTO']));	
        	}
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
	}

	function getAmortVencEjecutivo($periodo,$registro = null,$tipo = null,$jefatura= null,$banca= null,$zonal= null,$flgConsulta=null){	

    $sql = DB::table('WEBVPC_VENC_AMORT as WVA')
			->select('WL.NOMBRE'        
        ,'WL.NUM_DOC','WP.PRODUCTO'
        ,'WVA.FECHA_VENCIMIENTO'
        ,'WVA.FECHA','WVA.COD_UNICO','WVA.ID_PRODUCTO',
				DB::raw('WVA.MONTO_SOLES/1000 as MONTO_SOLES'),
				'WVA.FLG_SE_RENUEVA',
				DB::raw("WVA.PORC_RENOVACION * 100 as PORC_RENOVACION"),
				DB::raw("WVA.MONTO_SOLES/100 * (1-WVA.PORC_RENOVACION) as MONTO_CAIDA")      
			)
			->join('WEBBE_LEAD as WL',function($join){
				//PENDIENTE (cambiar a NUM_DOC)
				$join->on('WL.COD_UNICO','=','WVA.COD_UNICO');
			})
			->join('WEBBE_LEAD_SECTORISTA as WLS',function($join){
        $join->on('WLS.NUM_DOC','=','WL.NUM_DOC'); 
        $join->on('WLS.PERIODO','=','WL.PERIODO'); 
      })
      ->join('WEBVPC_USUARIO as WU',function($join){
        $join->on('WLS.REGISTRO_EN','=','WU.REGISTRO'); 
      })
      ->join('WEBBE_ZONAL as WZ',function($join){
        $join->on('WU.ID_ZONA','=','WZ.ID_ZONAL'); 
      })
			->join('WEBVPC_PRODUCTO as WP',function($join){
				$join->on('WP.ID_PRODUCTO','=','WVA.ID_PRODUCTO'); 
			})

      
      ->where('WL.PERIODO', '=',DB::raw("(SELECT MAX(PERIODO) FROM WEBBE_LEAD WHERE PERIODO<='".$periodo."')"))
      ->where('WVA.FECHA_VENCIMIENTO', '>=',DB::raw("DATEADD(MONTH, DATEDIFF(MONTH, 0, FECHA), 0)"))
      ->where('WVA.FECHA_VENCIMIENTO', '<',DB::raw("DATEADD(MONTH,2,DATEADD(MONTH, DATEDIFF(MONTH, 1, FECHA), 0))"));  

      if($registro){
        $sql = $sql->where('WLS.REGISTRO_EN',$registro);
      }

      if($jefatura){
        $sql = $sql->where('WU.ID_CENTRO','=',$jefatura);
      }

      
      if($zonal){                
        $sql = $sql->where('WU.ID_ZONA','=',$zonal);
      }

      if($banca){              
        $sql = $sql->where('WU.BANCA','=',$banca);
      }
      
      if($tipo){
        $sql = $sql->where('WVA.VENC_AMORT',$tipo);
      }  
          
      if($flgConsulta){        
        if($periodo){            
          $sql = $sql->where('WVA.FECHA','=',DB::raw("(SELECT FECHA_ACT_VENC FROM WEBNOTE_FECHAS WHERE PERIODO='".$periodo."')"));
        }else{                            
          $sql = $sql->where('WVA.FECHA','=',DB::raw("(SELECT MAX(FECHA_ACT_VENC) FROM WEBNOTE_FECHAS)")); 
        } 
      }         
		  return $sql;
	}

  function getAmortVencEjecutivoInicial($periodo,$registro = null,$tipo = null,$jefatura= null,$banca= null,$zonal= null,$flgConsulta=null){ 

    $sql = DB::table('WEBVPC_VENC_AMORT as WVA')
      ->select('WL.NOMBRE'        
        ,'WL.NUM_DOC','WP.PRODUCTO'
        ,'WVA.FECHA_VENCIMIENTO'
        ,'WVA.FECHA','WVA.COD_UNICO','WVA.ID_PRODUCTO',
        DB::raw('WVA.MONTO_SOLES/1000 as MONTO_SOLES'),
        'WVA.FLG_SE_RENUEVA',
        DB::raw("WVA.PORC_RENOVACION * 100 as PORC_RENOVACION"),
        DB::raw("WVA.MONTO_SOLES/100 * (1-WVA.PORC_RENOVACION) as MONTO_CAIDA")      
      )
      ->join('WEBBE_LEAD as WL',function($join){
        //PENDIENTE (cambiar a NUM_DOC)
        $join->on('WL.COD_UNICO','=','WVA.COD_UNICO');
      })
      ->join('WEBBE_LEAD_SECTORISTA as WLS',function($join){
        $join->on('WLS.NUM_DOC','=','WL.NUM_DOC'); 
        $join->on('WLS.PERIODO','=','WL.PERIODO'); 
      })
      ->join('WEBVPC_USUARIO as WU',function($join){
        $join->on('WLS.REGISTRO_EN','=','WU.REGISTRO'); 
      })
      ->join('WEBBE_ZONAL as WZ',function($join){
        $join->on('WU.ID_ZONA','=','WZ.ID_ZONAL'); 
      })
      ->join('WEBVPC_PRODUCTO as WP',function($join){
        $join->on('WP.ID_PRODUCTO','=','WVA.ID_PRODUCTO'); 
      })

      
      ->where('WL.PERIODO', '=',DB::raw("(SELECT MAX(PERIODO) FROM WEBBE_LEAD WHERE PERIODO<='".$periodo."')"));
      //->where('WVA.FECHA_VENCIMIENTO', '>=',DB::raw("DATEADD(MONTH, DATEDIFF(MONTH, 0, FECHA), 0)"))
      //->where('WVA.FECHA_VENCIMIENTO', '<',DB::raw("DATEADD(MONTH,2,DATEADD(MONTH, DATEDIFF(MONTH, 1, FECHA), 0))"));  

      if($registro){
        $sql = $sql->where('WLS.REGISTRO_EN',$registro);
      }

      if($jefatura){
        $sql = $sql->where('WU.ID_CENTRO','=',$jefatura);
      }

      
      if($zonal){                
        $sql = $sql->where('WU.ID_ZONA','=',$zonal);
      }

      if($banca){              
        $sql = $sql->where('WU.BANCA','=',$banca);
      }
      
      if($tipo){
        $sql = $sql->where('WVA.VENC_AMORT',$tipo);
      }  
          
      if($flgConsulta){        
        if($periodo){            
          $sql = $sql->where('WVA.FECHA','=',DB::raw("(SELECT FECHA_ACT_VENC FROM WEBNOTE_FECHAS WHERE PERIODO='".$periodo."')"));
        }else{                            
          $sql = $sql->where('WVA.FECHA','=',DB::raw("(SELECT MAX(FECHA_ACT_VENC) FROM WEBNOTE_FECHAS)")); 
        } 
      }         
      return $sql;
  }
    
  
  function getResumenSaldos($ejecutivo,$periodo,$banca,$zonal){   		
      $sqlIni = DB::table('WEBVPC_SALDOS_DIARIOS as SD')
                ->select('TIPO_PRODUCTO', DB::raw("SUM(SALDO_PUNTA)/1000 AS SALDO_INICIAL"), DB::raw("0 AS SALDO_ACTUAL"))
              	->join('WEBVPC_PRODUCTO as PRO',function($join){ 
            		  $join->on('SD.ID_PRODUCTO','=','PRO.ID_PRODUCTO'); })
        				->where('SD.FECHA', '=', DB::raw("(SELECT FECHA_INI_SD FROM WEBNOTE_FECHAS WHERE PERIODO = '".$periodo."')"));
        				//->where('SD.REGISTRO_EN','=', $ejecutivo)
        				if($ejecutivo){
                  
                  $sqlIni = $sqlIni->where('SD.REGISTRO_EN','=', $ejecutivo);
                }
                if($zonal){
                  $sqlIni = $sqlIni->where(DB::raw("(REPLACE(SD.ZONAL,' ',''))"),'=', $zonal);
                }
                if($banca){
                  $sqlIni = $sqlIni->where('SD.BANCA','like',DB::raw("'%".$banca."%'"));
                }
                  $sqlIni = $sqlIni->whereIn('PRO.TIPO_PRODUCTO', ['DIRECTAS', 'INDIRECTAS']);
        				  $sqlIni = $sqlIni ->groupby('TIPO_PRODUCTO');

      $sqlAct = DB::table('WEBVPC_SALDOS_DIARIOS as SD')
              ->select('TIPO_PRODUCTO', DB::raw("0 AS SALDO_INICIAL"), DB::raw("SUM(SALDO_PUNTA)/1000 AS SALDO_ACTUAL"))
              ->join('WEBVPC_PRODUCTO as PRO',function($join){ 
                $join->on('SD.ID_PRODUCTO','=','PRO.ID_PRODUCTO'); })
              ->where('SD.FECHA', '=', DB::raw("(SELECT FECHA_ACT_SD FROM WEBNOTE_FECHAS WHERE PERIODO = '".$periodo."')"));
              
              if($ejecutivo){
                $sqlAct = $sqlAct->where('SD.REGISTRO_EN','=', $ejecutivo);
              }
              if($zonal){
                $sqlAct = $sqlAct->where(DB::raw("(REPLACE(SD.ZONAL,' ',''))"),'=', $zonal);
              }
              if($banca){
                $sqlAct = $sqlAct->where('SD.BANCA','like',DB::raw("'%".$banca."%'"));
              }
              
              $sqlAct = $sqlAct->whereIn('PRO.TIPO_PRODUCTO', ['DIRECTAS', 'INDIRECTAS']);
              $sqlAct = $sqlAct->groupby('TIPO_PRODUCTO');
              $sqlAct = $sqlAct->unionAll($sqlIni);

      $sql = DB::table(DB::raw("({$sqlAct->toSql()}) as sub"))
            ->mergeBindings($sqlAct)
            ->select('TIPO_PRODUCTO',DB::raw("SUM(SALDO_INICIAL) AS SALDO_INICIAL"),DB::raw("SUM(SALDO_ACTUAL) AS SALDO_ACTUAL"))   
            ->groupby('TIPO_PRODUCTO');        
          
        return $sql->get();
  }

  function getResumenMetaOld($ejecutivo,$periodo,$banca,$zonal){

      $sql1 = DB::table('WEBVPC_SALDOS_DIARIOS as SD')
                ->select('REGISTRO_EN', 'ID_PRODUCTO', DB::raw("SUM(SALDO_PROMEDIO) AS SALDO_PROMEDIO"))
                ->where('SD.FECHA',DB::raw("(SELECT FECHA_INI_SD FROM WEBNOTE_FECHAS WHERE PERIODO = '".$periodo."')"))
                //->where('SD.REGISTRO_EN','=', $ejecutivo)
                ->groupby('REGISTRO_EN', 'ID_PRODUCTO');


   		$sql = DB::table('WEBVPC_METAS_SALDOS as MR')
                ->select('TIPO_PRODUCTO', DB::raw('SUM(META/1000) AS META'), DB::raw('SUM(SD.SALDO_PROMEDIO/1000) AS SALDO_PROMEDIO'), DB::raw('SUM(SALDO_PROMEDIO - META)/1000 AS GAP'), DB::raw('(SUM(SALDO_PROMEDIO)/SUM(META))*100 AS AVANCE'))
               	->join('WEBVPC_PRODUCTO as PRO',function($join){ 
            		$join->on('PRO.ID_PRODUCTO','=','MR.ID_PRODUCTO'); 
            	})
              ->leftjoin(DB::raw("({$sql1->toSql()}) as SD"),function($join){ 
                $join->on('MR.ID_PRODUCTO','=','SD.ID_PRODUCTO'); 
                $join->on('MR.REGISTRO_EN','=','SD.REGISTRO_EN'); 
              })              
              ->mergeBindings($sql1)  
            	->whereIn('TIPO_PRODUCTO', ['DIRECTAS', 'INDIRECTAS'])
				      ->where('PERIODO','=',$periodo);				      
              if($ejecutivo){
                $sql = $sql->where('MR.REGISTRO_EN','=',$ejecutivo);
              }
              if($zonal){
                $sql = $sql->where(DB::raw("(REPLACE(MR.ZONAL,' ',''))"),'=',$zonal);
              }
              if($banca){
                $sql = $sql->where('MR.BANCA','like',DB::raw("'%".$banca."%'"));
              }
				      $sql = $sql->groupby('TIPO_PRODUCTO');

        return $sql->get();
  }

  function getResumenMeta($ejecutivo,$periodo,$banca,$zonal){
      
      $sql1 = DB::table('WEBVPC_METAS_SALDOS as MR')
                ->select('REGISTRO_EN','ID_PRODUCTO',DB::raw("SUM(META) AS META"),DB::raw("0 AS SALDO_PROMEDIO"))
                ->where('MR.PERIODO',$periodo)
                ->groupby('REGISTRO_EN', 'ID_PRODUCTO');

      $sql2 = DB::table('WEBVPC_SALDOS_DIARIOS as SD')
                ->select('REGISTRO_EN','ID_PRODUCTO',DB::raw("0 AS META"),DB::raw("SUM(SALDO_PROMEDIO) AS SALDO_PROMEDIO"))
                ->where('SD.FECHA',DB::raw("(SELECT FECHA_ACT_SD FROM WEBNOTE_FECHAS WHERE PERIODO = '".$periodo."')"))                
                ->groupby('REGISTRO_EN', 'ID_PRODUCTO')
                ->unionAll($sql1)
                ->mergeBindings($sql1);

      $sql = DB::table(DB::raw("({$sql2->toSql()}) as SUB"))
                ->select('TIPO_PRODUCTO', DB::raw('SUM(META/1000) AS META'), DB::raw('SUM(SUB.SALDO_PROMEDIO/1000) AS SALDO_PROMEDIO'), DB::raw('SUM(SALDO_PROMEDIO - META)/1000 AS GAP'), DB::raw('(CASE WHEN SUM(META)=0 THEN 0 ELSE  SUM(SALDO_PROMEDIO)/SUM(META) END)*100  AS AVANCE'))
                ->join('WEBVPC_PRODUCTO as PRO',function($join){ 
                $join->on('PRO.ID_PRODUCTO','=','SUB.ID_PRODUCTO'); 
              })
                ->join('WEBVPC_USUARIO as WU',function($join){ 
                $join->on('WU.REGISTRO','=','SUB.REGISTRO_EN'); 
                })
              /*->leftjoin(DB::raw("({$sql1->toSql()}) as SD"),function($join){ 
                $join->on('MR.ID_PRODUCTO','=','SD.ID_PRODUCTO'); 
                $join->on('MR.REGISTRO_EN','=','SD.REGISTRO_EN'); 
              })*/              
              ->mergeBindings($sql1)  
              ->whereIn('TIPO_PRODUCTO', ['DIRECTAS', 'INDIRECTAS']);
              //->where('PERIODO','=',$periodo);              
              if($ejecutivo){
                $sql = $sql->where('WU.REGISTRO','=',$ejecutivo);
              }
              if($zonal){
                $sql = $sql->where('WU.iD_ZONA',$zonal);
              }
              if($banca){
                $sql = $sql->where('WU.BANCA',$banca);
              }
              $sql = $sql->groupby('TIPO_PRODUCTO');

        return $sql->get();
  }

  function getResumenDesembolsos($ejecutivo,$periodo,$banca,$zonal){

   		$sql = DB::table('WEBNOTE_OPERACIONES_NUEVAS as OPN')
                ->select('OPN.REGISTRO_EN','ID_PRODUCTO',DB::raw('SUM(MONTO_CERT)/1000 AS MONTO_CERT'), DB::raw('SUM(MONTO_ENCOT)/1000 AS MONTO_ENCOT'));
               	if($ejecutivo){
                  $sql = $sql->where('REGISTRO_EN','=', $ejecutivo);
                }
        				$sql = $sql->where('FLG_PERDIDO','=',0)
                  				 ->where('FLG_DESEMBOLSADO','=',0)
                  				 ->whereIn('ID_TIPO_OPERACION',[1,3])
                  				 ->where(DB::raw('MONTH(FECHA_DESEMBOLSO)'),'=',DB::raw("SUBSTRING('".$periodo."', 5, 2)"))
                  				 ->where(DB::raw('YEAR(FECHA_DESEMBOLSO)'),'=',DB::raw("SUBSTRING('".$periodo."', 1, 4)"))
                  				 ->groupby('OPN.REGISTRO_EN', 'ID_PRODUCTO');

		  $sql1 = DB::table('WEBNOTE_PROYECCION_INICIAL as PI')
                ->select('TIPO_PRODUCTO', DB::raw('SUM(SALDO_DESEM_CERT)/1000 AS COMPROMISO'), DB::raw('SUM(ISNULL(MONTO_CERT,0)) AS MONTO_CERT'), DB::raw('SUM(ISNULL(MONTO_ENCOT,0)) AS MONTO_ENCOT'))
               	->join('WEBVPC_PRODUCTO as PRO',function($join){ 
              		$join->on('PRO.ID_PRODUCTO','=','PI.ID_PRODUCTO'); 
                	})
              	->leftjoin(DB::raw("({$sql->toSql()}) as DE"),function($join){ 
              		$join->on('DE.ID_PRODUCTO','=','PI.ID_PRODUCTO'); 
              	   })
                ->join('WEBVPC_USUARIO as WU',function($join){ 
                $join->on('WU.REGISTRO','=','PI.REGISTRO_EN'); 
                })
              	->mergeBindings($sql)
              	->where('FECHA','=',DB::raw("(SELECT MAX(FECHA_FOTO) FROM WEBNOTE_FECHA_FOTO WHERE APLICATIVO = 'EN_COLOC' AND PERIODO = '".$periodo."')"));
        				if($ejecutivo){
                  $sql1 = $sql1->where('PI.REGISTRO_EN','=',$ejecutivo);
                }  
                if($zonal){
                  $sql1 = $sql1->where('WU.ID_ZONA',$zonal);
                }
                if($banca){
                  $sql1 = $sql1->where('WU.BANCA',$banca);
                }              
        				$sql1 = $sql1->whereIn('TIPO_PRODUCTO', ['DIRECTAS', 'INDIRECTAS']);
        				$sql1 = $sql1->groupby('TIPO_PRODUCTO');
						
        return $sql1->get();
  }

  function getResumenProyeccInicial($ejecutivo,$periodo,$banca,$zonal){

   		$sql = DB::table('WEBNOTE_PROYECCION_INICIAL as PI')
                ->select('TIPO_PRODUCTO', DB::raw('SUM(SALDO_DESEM_CERT/1000) AS SALDO_DESEM_CERT'), DB::raw('SUM(SALDO_PROYEC_CERT/1000) AS SALDO_PROYEC_CERT'))
               	->join('WEBVPC_PRODUCTO as PRO',function($join){ 
            		$join->on('PRO.ID_PRODUCTO','=','PI.ID_PRODUCTO'); 
            	})    
                ->join('WEBVPC_USUARIO as WU',function($join){ 
                $join->on('WU.REGISTRO','=','PI.REGISTRO_EN'); 
              })   
            	->where('FECHA','=',DB::raw("(SELECT MAX(FECHA_FOTO) FROM WEBNOTE_FECHA_FOTO WHERE APLICATIVO = 'EN_COLOC' AND PERIODO = '".$periodo."')"));
              if($ejecutivo){
                  $sql = $sql->where('PI.REGISTRO_EN','=',$ejecutivo);
                }  
                if($zonal){
                  $sql = $sql->where('WU.ID_ZONA',$zonal);
                }
                if($banca){
                  $sql = $sql->where('WU.BANCA',$banca);
                } 
				      $sql = $sql->whereIn('TIPO_PRODUCTO', ['DIRECTAS', 'INDIRECTAS']);
				      $sql = $sql->groupby('TIPO_PRODUCTO');
        return $sql->get();
   }	

    

  function getResumenCaidas($ejecutivo,$periodo,$banca,$zonal){      
   		$sql2 = DB::table('WEBNOTE_OPERACIONES_NUEVAS as OP')
                ->select(DB::raw("'P' as VENC_AMORT"),'COD_UNICO','TIPO_PRODUCTO', DB::raw('MONTO_CERT as MONTO_SOLES'), DB::raw("'0' AS PORC_RENOVACION"))
               	->join('WEBVPC_PRODUCTO as PRO',function($join){ 
            		$join->on('PRO.ID_PRODUCTO','=','OP.ID_PRODUCTO'); 
            	  })
                ->join('WEBVPC_USUARIO as WU',function($join){ 
                $join->on('WU.REGISTRO','=','OP.REGISTRO_EN'); 
              });           	  
                if($ejecutivo){
                  $sql2 = $sql2->where('OP.REGISTRO_EN','=',$ejecutivo);
                }  
                if($zonal){
                  $sql2 = $sql2->where('WU.ID_ZONA',$zonal);
                }
                if($banca){
                  $sql2 = $sql2->where('WU.BANCA',$banca);
                }
            	  $sql2 = $sql2->where('ID_TIPO_OPERACION','=',2)
                              ->where('OP.FLG_DESEMBOLSADO','<>','1')                              
                              ->where('OP.FLG_PERDIDO','<>','1')
                          	  ->where(DB::raw('MONTH(FECHA_DESEMBOLSO)'),'=',DB::raw("SUBSTRING('".$periodo."', 5, 2)"))
              				        ->where(DB::raw('YEAR(FECHA_DESEMBOLSO)'),'=',DB::raw("SUBSTRING('".$periodo."', 1, 4)"));

   		$sql1 = DB::table('WEBVPC_VENC_AMORT as VE')
                ->select('VENC_AMORT','COD_UNICO','TIPO_PRODUCTO','MONTO_SOLES', 'PORC_RENOVACION')
               	->join('WEBVPC_PRODUCTO as PRO',function($join){ 
            		$join->on('PRO.ID_PRODUCTO','=','VE.ID_PRODUCTO'); 
            	   })
                ->join('WEBVPC_USUARIO as WU',function($join){ 
                $join->on('WU.REGISTRO','=','VE.REGISTRO_EN'); 
              }) 
            	  ->where('VE.FECHA','=', DB::raw("(SELECT FECHA_ACT_SD FROM WEBNOTE_FECHAS WHERE PERIODO = '".$periodo."')"));           	  
                if($ejecutivo){
                  $sql1 = $sql1->where('REGISTRO_EN','=',$ejecutivo);
                }  
                if($zonal){
                  $sql1 = $sql1->where('WU.ID_ZONA',$zonal);
                }
                if($banca){
                  $sql1 = $sql1->where('WU.BANCA',$banca);
                }
            	  $sql1 = $sql1->where(DB::raw('MONTH(FECHA_VENCIMIENTO)'),'=',DB::raw("SUBSTRING('".$periodo."', 5, 2)"))
      				        ->where(DB::raw('YEAR(FECHA_VENCIMIENTO)'),'=',DB::raw("SUBSTRING('".$periodo."', 1, 4)"))
                      ->where(DB::raw("((FLG_SE_RENUEVA = 0 AND VENC_AMORT = 'V') OR (VENC_AMORT = 'A' AND FECHA_VENCIMIENTO"), ">", DB::raw("(SELECT FECHA_ACT_SD FROM WEBNOTE_FECHAS WHERE PERIODO = '".$periodo."')))"))
      				        ->unionAll($sql2);
				
			
		  $sql3 = DB::table(DB::raw("({$sql1->toSql()}) as sub"))
				      ->mergeBindings($sql1)
			        ->select('TIPO_PRODUCTO',DB::raw("SUM(CASE WHEN VENC_AMORT = 'V' THEN  (MONTO_SOLES * (1 - PORC_RENOVACION)) ELSE MONTO_SOLES END)/1000 AS MONTO_CAIDA"))	

				      ->groupby('TIPO_PRODUCTO');


        return $sql3->get();
  }
  
  
  function getProyeccionVencAmort($periodo,$ejecutivo = null,$jefatura= null,$zonal= null,$banca= null){  
    $sql = $this->getAmortVencEjecutivo($periodo,$ejecutivo,null,$jefatura,$banca,$zonal ,1);    
    
    $sql->select(
      'WVA.FECHA_VENCIMIENTO'
      ,DB::raw("SUM(CASE WHEN (TIPO_PRODUCTO = 'DIRECTAS' AND VENC_AMORT='A') THEN WVA.MONTO_SOLES ELSE 0 END) as MONTO_CAIDA_DIR_AMORT")
      ,DB::raw("SUM(CASE WHEN (TIPO_PRODUCTO = 'DIRECTAS' AND VENC_AMORT='V') THEN WVA.MONTO_SOLES ELSE 0 END) as MONTO_CAIDA_DIR_VENC")
      ,DB::raw("SUM(CASE WHEN (TIPO_PRODUCTO = 'INDIRECTAS' AND VENC_AMORT='A')THEN WVA.MONTO_SOLES ELSE 0 END) as MONTO_CAIDA_IND_AMORT")
      ,DB::raw("SUM(CASE WHEN (TIPO_PRODUCTO = 'INDIRECTAS' AND VENC_AMORT='V')THEN WVA.MONTO_SOLES ELSE 0 END) as MONTO_CAIDA_IND_VENC")
    )
    
      ->groupBy('WVA.FECHA_VENCIMIENTO')
      ->where('WVA.FECHA_VENCIMIENTO','<=',DB::raw("(SELECT DATEADD(D, 30, FECHA_ACT_SD) FROM dbo.WEBNOTE_FECHAS WHERE PERIODO ='".$periodo."')"))
      ->where('WVA.FECHA_VENCIMIENTO','>',DB::raw("(SELECT FECHA_ACT_SD FROM dbo.WEBNOTE_FECHAS WHERE PERIODO = '".$periodo."')"))
      ->where('WVA.FECHA','>=',DB::raw("(SELECT FECHA_ACT_VENC from WEBNOTE_FECHAS WHERE PERIODO = '".$periodo."')"))      
      ->orderBy('WVA.FECHA_VENCIMIENTO','asc');
    return $sql;
  }

  function getProyeccionVencAmortInicial($periodo,$ejecutivo = null,$jefatura= null,$zonal= null,$banca= null){  
    $sql = $this->getAmortVencEjecutivoInicial($periodo,$ejecutivo,null,$jefatura,$banca,$zonal );        
    $sql->select(
      'WVA.FECHA_VENCIMIENTO'
      ,DB::raw("SUM(CASE WHEN (TIPO_PRODUCTO = 'DIRECTAS' AND VENC_AMORT='A') THEN WVA.MONTO_SOLES ELSE 0 END) as MONTO_CAIDA_DIR_AMORT")
      ,DB::raw("SUM(CASE WHEN (TIPO_PRODUCTO = 'DIRECTAS' AND VENC_AMORT='V') THEN WVA.MONTO_SOLES ELSE 0 END) as MONTO_CAIDA_DIR_VENC")
      ,DB::raw("SUM(CASE WHEN (TIPO_PRODUCTO = 'INDIRECTAS' AND VENC_AMORT='A')THEN WVA.MONTO_SOLES ELSE 0 END) as MONTO_CAIDA_INDS_AMORT")
      ,DB::raw("SUM(CASE WHEN (TIPO_PRODUCTO = 'INDIRECTAS' AND VENC_AMORT='V')THEN WVA.MONTO_SOLES ELSE 0 END) as MONTO_CAIDA_IND_VENC")
    )
    
      ->groupBy('WVA.FECHA_VENCIMIENTO')
      //->where('SUB.FECHA_VENCIMIENTO','<=',DB::raw("(SELECT DATEADD(D, 30, FECHA_ACT_SD) FROM dbo.WEBNOTE_FECHAS WHERE PERIODO ='".$periodo."')"))
      //->where('SUB.FECHA_VENCIMIENTO','>',DB::raw("(SELECT FECHA_ACT_SD FROM dbo.WEBNOTE_FECHAS WHERE PERIODO = '".$periodo."')"))
      ->where('WVA.FECHA','=',DB::raw("(SELECT FECHA_FOTO FROM  WEBNOTE_FECHA_FOTO  WHERE  APLICATIVO='EN_COLOC' AND PERIODO='".$periodo."')"))      
      ->orderBy('WVA.FECHA_VENCIMIENTO','asc');
    return $sql;
  }

  function getResumenSaldosJefes($ejecutivo,$periodo,$banca,$zonal,$jefatura,$rol){   
      $sqlIni = DB::table('WEBVPC_SALDOS_DIARIOS as SD')
                ->select('TIPO_PRODUCTO','WU.ID_ZONA','WEZ.REGISTRO AS REG_JEFE_ZONAL','WEZ.NOMBRE AS JEFE_ZONAL','WU.ID_CENTRO','WEJ.REGISTRO AS REG_JEFATURA',
                          'WEJ.NOMBRE AS JEFATURA','WU.REGISTRO AS REGISTRO_EN','WU.NOMBRE',DB::raw("SUM(SALDO_PUNTA)/1000 AS SALDO_INICIAL"), DB::raw("0 AS SALDO_ACTUAL"))
                ->join('WEBVPC_USUARIO as WU',function($join){ 
                  $join->on('SD.REGISTRO_EN','=','WU.REGISTRO');
                   })
                /*->join('WEBBE_ZONAL as WZ',function($join){ 
                  $join->on('WU.ID_ZONA','=','WZ.ID_ZONAL'); })*/
                ->leftjoin('WEBVPC_USUARIO as WEZ',function($join){ 
                  $join->on('WEZ.ID_ZONA','=','WU.ID_ZONA');
                  $join->on('WEZ.ROL','=',DB::raw(\App\Entity\Usuario::ROL_GERENCIA_ZONAL_BE/*25*/)); })
                ->leftjoin('WEBVPC_USUARIO as WEJ',function($join){ 
                  $join->on('WEJ.ID_CENTRO','=','WU.ID_CENTRO');
                  $join->on('WEJ.ROL','=',DB::raw(\App\Entity\Usuario::ROL_JEFATURA_BE/*24*/)); })
                ->join('WEBVPC_PRODUCTO as PRO',function($join){ 
                  $join->on('SD.ID_PRODUCTO','=','PRO.ID_PRODUCTO'); })
                ->where('SD.FECHA', '=', DB::raw("(SELECT FECHA_INI_SD FROM WEBNOTE_FECHAS WHERE PERIODO = '".$periodo."')"))
                ->whereIn('WU.ROL',[DB::raw(Usuario::ROL_EJECUTIVO_FARMER_BE),DB::raw(Usuario::ROL_EJECUTIVO_HUNTER_BE)]); 
                //->where('SD.REGISTRO_EN','=', $ejecutivo)
                if($ejecutivo){
                  
                  $sqlIni = $sqlIni->where('SD.REGISTRO_EN','=', $ejecutivo);
                }
                if($zonal){
                  $sqlIni = $sqlIni->where('WU.ID_ZONA','=', $zonal);
                }
                if($banca){
                  $sqlIni = $sqlIni->where('WU.BANCA','like',DB::raw("'%".$banca."%'"));
                }
                if($jefatura){
                  $sqlIni = $sqlIni->where('WU.ID_CENTRO','=',$jefatura);
                }
                  $sqlIni = $sqlIni->whereIn('PRO.TIPO_PRODUCTO', ['DIRECTAS', 'INDIRECTAS']);
                  $sqlIni = $sqlIni ->groupby('TIPO_PRODUCTO','WU.ID_ZONA','WEZ.REGISTRO','WEZ.NOMBRE','WU.ID_CENTRO','WEJ.REGISTRO','WEJ.NOMBRE','WU.REGISTRO','WU.NOMBRE');

      $sqlAct = DB::table('WEBVPC_SALDOS_DIARIOS as SD')
              ->select('TIPO_PRODUCTO','WU.ID_ZONA','WEZ.REGISTRO AS REG_JEFE_ZONAL','WEZ.NOMBRE AS JEFE_ZONAL','WU.ID_CENTRO','WEJ.REGISTRO AS REG_JEFATURA','WEJ.NOMBRE AS JEFATURA','WU.REGISTRO AS REGISTRO_EN','WU.NOMBRE', DB::raw("0 AS SALDO_INICIAL"), DB::raw("SUM(SALDO_PUNTA)/1000 AS SALDO_ACTUAL"))
              ->join('WEBVPC_USUARIO as WU',function($join){ 
                  $join->on('SD.REGISTRO_EN','=','WU.REGISTRO'); })
                /*->join('WEBBE_ZONAL as WZ',function($join){ 
                  $join->on('WU.ID_ZONA','=','WZ.ID_ZONAL'); })*/
                ->leftjoin('WEBVPC_USUARIO as WEZ',function($join){ 
                  $join->on('WEZ.ID_ZONA','=','WU.ID_ZONA');
                  $join->on('WEZ.ROL','=',DB::raw(\App\Entity\Usuario::ROL_GERENCIA_ZONAL_BE/*25*/)); })
                ->leftjoin('WEBVPC_USUARIO as WEJ',function($join){ 
                  $join->on('WEJ.ID_CENTRO','=','WU.ID_CENTRO');
                  $join->on('WEJ.ROL','=',DB::raw(\App\Entity\Usuario::ROL_JEFATURA_BE/*24*/)); })
                ->join('WEBVPC_PRODUCTO as PRO',function($join){ 
                  $join->on('SD.ID_PRODUCTO','=','PRO.ID_PRODUCTO'); })
              ->where('SD.FECHA', '=', DB::raw("(SELECT FECHA_ACT_SD FROM WEBNOTE_FECHAS WHERE PERIODO = '".$periodo."')"))
              ->whereIn('WU.ROL',[DB::raw(Usuario::ROL_EJECUTIVO_FARMER_BE),DB::raw(Usuario::ROL_EJECUTIVO_HUNTER_BE)]);              
              if($ejecutivo){
                $sqlAct = $sqlAct->where('SD.REGISTRO_EN','=', $ejecutivo);
              }
              if($zonal){
                $sqlAct = $sqlAct->where('WU.ID_ZONA','=', $zonal);
              }
              if($banca){
                $sqlAct = $sqlAct->where('WU.BANCA','like',DB::raw("'%".$banca."%'"));
              }
              if($jefatura){
                  $sqlAct = $sqlAct->where('WU.ID_CENTRO','=',$jefatura);
                }
              $sqlAct = $sqlAct->whereIn('PRO.TIPO_PRODUCTO', ['DIRECTAS', 'INDIRECTAS']);
              $sqlAct = $sqlAct->groupby('TIPO_PRODUCTO','WU.iD_ZONA','WEZ.REGISTRO','WEZ.NOMBRE','WU.ID_CENTRO','WEJ.REGISTRO','WEJ.NOMBRE','WU.REGISTRO','WU.NOMBRE');
              $sqlAct = $sqlAct->unionAll($sqlIni);


      $sql = DB::table(DB::raw("({$sqlAct->toSql()}) as sub"));           
            //GERENTEDIVISIO
            if($rol==Usuario::ROL_GERENTE_DIVISION_BE or $rol==Usuario::ROL_GERENTE_BANCA){
              $sql = $sql->Select(DB::raw("(SUB.TIPO_PRODUCTO + SUB.REG_JEFE_ZONAL) AS IND"),'TIPO_PRODUCTO','SUB.ID_ZONA','SUB.REG_JEFE_ZONAL');        
            }            
            if($rol == Usuario::ROL_GERENCIA_ZONAL_BE){
              
              if($zonal<>'BELZONAL2'){
                //JEFEZONAL<>BELZONAL2
                $sql = $sql->Select(DB::raw("(SUB.TIPO_PRODUCTO + SUB.REGISTRO_EN) AS IND"),'TIPO_PRODUCTO','SUB.ID_ZONA','SUB.REGISTRO_EN AS REGISTRO_EN');
              }
              else{
                //JEFEZONAL==BELZONA2        
                $sql = $sql->Select(DB::raw("(SUB.TIPO_PRODUCTO + SUB.REG_JEFATURA) AS IND"),'TIPO_PRODUCTO','SUB.ID_ZONA','SUB.ID_CENTRO','SUB.JEFATURA AS ENCARGADO');  
              }              
            }
            //JEFATURA
            if($rol==Usuario::ROL_JEFATURA_BE){
              $sql = $sql->Select(DB::raw('(TIPO_PRODUCTO + SUB.REGISTRO_EN) AS IND'),'TIPO_PRODUCTO','SUB.ID_ZONA','SUB.ID_CENTRO','SUB.JEFATURA','REGISTRO_EN AS REGISTRO','SUB.NOMBRE AS ENCARGADO');        
            }

            $sql = $sql->mergeBindings($sqlAct)
            ->addSelect(DB::raw("SUM(SALDO_INICIAL) AS SALDO_INICIAL"),DB::raw("SUM(SALDO_ACTUAL) AS SALDO_ACTUAL"));   
            //GERENTEDIVISIO
            if($rol==Usuario::ROL_GERENTE_DIVISION_BE or $rol ==Usuario::ROL_GERENTE_BANCA){                          
              $sql = $sql->groupby(DB::raw("(SUB.TIPO_PRODUCTO + SUB.REG_JEFE_ZONAL)"),'TIPO_PRODUCTO','SUB.ID_ZONA','SUB.REG_JEFE_ZONAL');        
            }            
            if($rol==Usuario::ROL_GERENCIA_ZONAL_BE){              
              if($zonal<>'BELZONAL2'){
                //JEFEZONAL<>BELZONAL2                
                $sql = $sql->groupby(DB::raw("(SUB.TIPO_PRODUCTO + SUB.REGISTRO_EN)"),'TIPO_PRODUCTO','ID_ZONA','REGISTRO_EN','NOMBRE');
              }
              else{
                //JEFEZONAL==BELZONA2                        
                $sql = $sql->groupby(DB::raw("(SUB.TIPO_PRODUCTO + SUB.REG_JEFATURA)"),'TIPO_PRODUCTO','ID_ZONA','ID_CENTRO','JEFATURA');  
              }              
            }
            //JEFATURA
            if($rol==Usuario::ROL_JEFATURA_BE){              
              $sql = $sql->groupby(DB::raw('(TIPO_PRODUCTO + SUB.REGISTRO_EN)'),'TIPO_PRODUCTO','ID_ZONA','ID_CENTRO','JEFATURA','REGISTRO_EN','SUB.NOMBRE');        
            }            
                  
        // DD($sql->toSql()) ;
        return $sql->get();
  }

  function getResumenDesembolsosJefes($ejecutivo,$periodo,$banca,$zonal,$jefatura,$rol){
      $sql = DB::table('WEBNOTE_OPERACIONES_NUEVAS as OPN')
                ->select('OPN.REGISTRO_EN','ID_PRODUCTO',DB::raw('SUM(MONTO_CERT)/1000 AS MONTO_CERT'), DB::raw('SUM(MONTO_ENCOT)/1000 AS MONTO_ENCOT'),db::raw('0 AS SALDO_DESEM_CERT'));
                if($ejecutivo){
                  $sql = $sql->where('REGISTRO_EN','=', $ejecutivo);
                }
                $sql = $sql->where('FLG_PERDIDO','=',0)
                           ->where('FLG_DESEMBOLSADO','=',0)
                           ->whereIn('ID_TIPO_OPERACION',[1,3])
                           ->where(DB::raw('MONTH(FECHA_DESEMBOLSO)'),'=',DB::raw("SUBSTRING('".$periodo."', 5, 2)"))
                           ->where(DB::raw('YEAR(FECHA_DESEMBOLSO)'),'=',DB::raw("SUBSTRING('".$periodo."', 1, 4)"))
                           ->groupby('OPN.REGISTRO_EN', 'ID_PRODUCTO');

      $sql2 = DB::table('WEBNOTE_PROYECCION_INICIAL as PI')
          ->select('REGISTRO_EN','ID_PRODUCTO',db::raw('0 AS MONTO_CERT'),db::raw('0 AS MONTO_ENCOT'),db::raw('SUM(SALDO_DESEM_CERT) AS SALDO_DESEM_CERT'))
          ->where('FECHA','=',DB::raw("(SELECT MAX(FECHA_FOTO) FROM WEBNOTE_FECHA_FOTO WHERE APLICATIVO = 'EN_COLOC' AND PERIODO = '".$periodo."')"))
          ->groupby('PI.REGISTRO_EN','ID_PRODUCTO')
          ->unionAll($sql)
          ->mergeBindings($sql);
                          

      $sql1 = DB::table(DB::raw("({$sql2->toSql()}) as SUB"))
                ->mergeBindings($sql2);
                //->select('TIPO_PRODUCTO', 
                  if($rol==Usuario::ROL_GERENTE_DIVISION_BE or $rol==Usuario::ROL_GERENTE_BANCA){
                    $sql1 = $sql1->Select(DB::raw("(TIPO_PRODUCTO + WEZ.REGISTRO) AS IND"),'PRO.TIPO_PRODUCTO','WU.ID_ZONA AS ZONA','WEZ.REGISTRO AS REGISTRO','WEZ.NOMBRE AS ENCARGADO');        
                  }            
                  if($rol == Usuario::ROL_GERENCIA_ZONAL_BE){
                    
                    if($zonal<>'BELZONAL2'){
                      //JEFEZONAL<>BELZONAL2
                      $sql1sql1 = $sql1->Select(DB::raw("(TIPO_PRODUCTO + WU.REGISTRO) AS IND"),'TIPO_PRODUCTO','WU.ID_ZONA','WU.REGISTRO AS REGISTRO','WU.NOMBRE AS ENCARGADO');
                    }
                    else{
                      //JEFEZONAL==BELZONA2                              
                      $sql1 = $sql1->Select(DB::raw("(TIPO_PRODUCTO + WEJ.REGISTRO) AS IND"),'TIPO_PRODUCTO','WU.ID_ZONA','WU.ID_CENTRO','WEJ.REGISTRO AS REGISTRO','WEJ.NOMBRE AS ENCARGADO');  
                    }              
                  }
                  //JEFATURA
                  if($rol==Usuario::ROL_JEFATURA_BE){                    
                    $sql1 = $sql1->Select(DB::raw("(TIPO_PRODUCTO + WU.REGISTRO) AS IND"),'TIPO_PRODUCTO','WU.ID_ZONA','WU.ID_CENTRO','WEJ.NOMBRE AS JEFATURA','WU.REGISTRO AS REGISTRO ','WU.NOMBRE AS ENCARGADO');        
                  } 
                  
                  $sql1 = $sql1->addSelect(DB::raw('SUM(SALDO_DESEM_CERT)/1000 AS COMPROMISO'), DB::raw('SUM(ISNULL(MONTO_CERT,0)) AS MONTO_CERT'), DB::raw('SUM(ISNULL(MONTO_ENCOT,0)) AS MONTO_ENCOT'))
                ->join('WEBVPC_PRODUCTO as PRO',function($join){                   
                  $join->on('PRO.ID_PRODUCTO','=','SUB.ID_PRODUCTO'); 
                  })                                
                ->join('WEBVPC_USUARIO as WU',function($join){ 
                $join->on('WU.REGISTRO','=','SUB.REGISTRO_EN'); 
                })
                ->leftjoin('WEBVPC_USUARIO as WEZ',function($join){ 
                  $join->on('WEZ.ID_ZONA','=','WU.ID_ZONA');
                  $join->on('WEZ.ROL','=',DB::raw(\App\Entity\Usuario::ROL_GERENCIA_ZONAL_BE/*25*/)); })
                ->leftjoin('WEBVPC_USUARIO as WEJ',function($join){ 
                  $join->on('WEJ.ID_CENTRO','=','WU.ID_CENTRO');
                  $join->on('WEJ.ROL','=',DB::raw(\App\Entity\Usuario::ROL_JEFATURA_BE/*24*/)); })                                
                //->where('FECHA','=',DB::raw("(SELECT MAX(FECHA_FOTO) FROM WEBNOTE_FECHA_FOTO WHERE APLICATIVO = 'EN_COLOC' AND PERIODO = '".$periodo."')"))                
                ->whereIn('WU.ROL',[DB::raw(Usuario::ROL_EJECUTIVO_FARMER_BE),DB::raw(Usuario::ROL_EJECUTIVO_HUNTER_BE)]);
                if($ejecutivo){
                  $sql1 = $sql1->where('SUB.REGISTRO_EN','=',$ejecutivo);
                }  
                if($zonal){
                  $sql1 = $sql1->where('WU.ID_ZONA',$zonal);
                }
                if($banca){
                  $sql1 = $sql1->where('WU.BANCA',$banca);
                }                  
                if($jefatura){
                  $sql1 = $sql1->where('WU.ID_CENTRO',$jefatura);
                }            
                $sql1 = $sql1->whereIn('TIPO_PRODUCTO', ['DIRECTAS', 'INDIRECTAS']);
                //$sql1 = $sql1->groupby('TIPO_PRODUCTO');
              
                if($rol==Usuario::ROL_GERENTE_DIVISION_BE or $rol ==Usuario::ROL_GERENTE_BANCA){             
                  $sql1 = $sql1->groupby(DB::raw("(TIPO_PRODUCTO + WEZ.REGISTRO)"),'PRO.TIPO_PRODUCTO','WU.ID_ZONA','WEZ.REGISTRO','WEZ.NOMBRE');        
                }            
                if($rol==Usuario::ROL_GERENCIA_ZONAL_BE){              
                  if($zonal<>'BELZONAL2'){
                    //JEFEZONAL<>BELZONAL2                
                    $sql1 = $sql1->groupby(DB::raw("(TIPO_PRODUCTO + WU.REGISTRO)"),'TIPO_PRODUCTO','WU.ID_ZONA','WU.REGISTRO','WU.NOMBRE');
                  }
                  else{
                    //JEFEZONAL==BELZONA2                        
                    $sql1 = $sql1->groupby(DB::raw("(TIPO_PRODUCTO + WEJ.REGISTRO)"),'TIPO_PRODUCTO','WU.ID_ZONA','WU.ID_CENTRO','WEJ.REGISTRO','WEJ.NOMBRE');  
                  }              
                }
                //JEFATURA
                if($rol==Usuario::ROL_JEFATURA_BE){                  
                  $sql1 = $sql1->groupby(DB::raw("(TIPO_PRODUCTO + WU.REGISTRO)"),'TIPO_PRODUCTO','WU.ID_ZONA','WU.ID_CENTRO','WEJ.NOMBRE','WU.REGISTRO','WU.NOMBRE');        
                } 
                //dd($sql1->toSql());
                //DD($sql1->ToSql());
                return $sql1->get();
  }

  function getResumenMetaJefes($ejecutivo,$periodo,$banca,$zonal,$jefatura,$rol){
      

      $sql1 = DB::table('WEBVPC_METAS_SALDOS as MR')
                ->select('REGISTRO_EN','ID_PRODUCTO',DB::raw("SUM(META) AS META"),DB::raw("0 AS SALDO_PROMEDIO"))
                ->where('MR.PERIODO',$periodo)
                //->where('SD.FECHA',DB::raw("(SELECT FECHA_ACT_SD FROM WEBNOTE_FECHAS WHERE PERIODO = '".$periodo."')"))
                //->where('SD.REGISTRO_EN','=', $ejecutivo)
                ->groupby('REGISTRO_EN', 'ID_PRODUCTO');

      $sql2 = DB::table('WEBVPC_SALDOS_DIARIOS as SD')
                ->select('REGISTRO_EN','ID_PRODUCTO',DB::raw("0 AS META"),DB::raw("SUM(SALDO_PROMEDIO) AS SALDO_PROMEDIO"))
                ->where('SD.FECHA',DB::raw("(SELECT FECHA_ACT_SD FROM WEBNOTE_FECHAS WHERE PERIODO = '".$periodo."')"))                
                ->groupby('REGISTRO_EN', 'ID_PRODUCTO')
                ->unionAll($sql1)
                ->mergeBindings($sql1);
      //dd($sql2->toSql());
      
      $sql = DB::table(DB::raw("({$sql2->toSql()}) as SUB"));

                  if($rol==Usuario::ROL_GERENTE_DIVISION_BE or $rol==Usuario::ROL_GERENTE_BANCA){                    
                    $sql = $sql->Select(DB::raw("(TIPO_PRODUCTO + WEZ.REGISTRO) AS IND"),'TIPO_PRODUCTO','WU.ID_ZONA AS ZONA','WEZ.REGISTRO AS REGISTRO','WEZ.NOMBRE AS ENCARGADO');        
                  }            
                  if($rol == Usuario::ROL_GERENCIA_ZONAL_BE){                  
                    if($zonal<>'BELZONAL2'){
                      //JEFEZONAL<>BELZONAL2
                      $sql = $sql->Select(DB::raw("(TIPO_PRODUCTO + WU.REGISTRO) AS IND"),'TIPO_PRODUCTO','WU.ID_ZONA','WU.REGISTRO AS REGISTRO','WU.NOMBRE AS ENCARGADO');
                    }
                    else{
                      //JEFEZONAL==BELZONA2                              
                      $sql = $sql->Select(DB::raw("(TIPO_PRODUCTO + WEJ.REGISTRO) AS IND"),'TIPO_PRODUCTO','WU.ID_ZONA','WU.ID_CENTRO','WEJ.REGISTRO AS REGISTRO','WEJ.NOMBRE AS ENCARGADO');  
                    }              
                  }
                  //JEFATURA
                  if($rol==Usuario::ROL_JEFATURA_BE){                    
                    $sql = $sql->Select(DB::raw("(TIPO_PRODUCTO + WU.REGISTRO) AS IND"),'TIPO_PRODUCTO','WU.ID_ZONA','WU.ID_CENTRO','WEJ.NOMBRE AS JEFATURA','WU.REGISTRO AS REGISTRO ','WU.NOMBRE AS ENCARGADO');        
                  } 

                $sql = $sql->addSelect(DB::raw('SUM(META/1000) AS META'), DB::raw('SUM(SALDO_PROMEDIO/1000) AS SALDO_PROMEDIO'), DB::raw('SUM(SALDO_PROMEDIO - META)/1000 AS GAP'), DB::raw('(( CASE WHEN SUM(META)=0 THEN 0 ELSE SUM(SALDO_PROMEDIO)/SUM(META) END ) )*100 AS AVANCE'))                
                ->join('WEBVPC_PRODUCTO as PRO',function($join){ 
                $join->on('PRO.ID_PRODUCTO','=','SUB.ID_PRODUCTO'); 
              })              
              /*->leftjoin(DB::raw("({$sql1->toSql()}) as MR"),function($join){ 
                $join->on('MR.ID_PRODUCTO','=','SUB.ID_PRODUCTO'); 
                $join->on('MR.REGISTRO_EN','=','SUB.REGISTRO_EN');                 
              }) */            
                ->join('WEBVPC_USUARIO as WU',function($join){ 
                $join->on('WU.REGISTRO','=','SUB.REGISTRO_EN');                 
                })
              ->leftjoin('WEBVPC_USUARIO as WEZ',function($join){ 
                $join->on('WEZ.ID_ZONA','=','WU.ID_ZONA');
                $join->on('WEZ.ROL','=',DB::raw(Usuario::ROL_GERENCIA_ZONAL_BE/*25*/)); })
              ->leftjoin('WEBVPC_USUARIO as WEJ',function($join){ 
                $join->on('WEJ.ID_CENTRO','=','WU.ID_CENTRO');
                $join->on('WEJ.ROL','=',DB::raw(Usuario::ROL_JEFATURA_BE/*24*/)); })                                
              ->mergeBindings($sql2)
              ->whereIn('TIPO_PRODUCTO', ['DIRECTAS', 'INDIRECTAS'])
              ->whereIn('WU.ROL',[DB::raw(Usuario::ROL_EJECUTIVO_FARMER_BE),DB::raw(Usuario::ROL_EJECUTIVO_HUNTER_BE)]);;
              if($ejecutivo){
                $sql = $sql->where('WU.REGISTRO','=',$ejecutivo);
              }
              if($zonal){                
                $sql = $sql->where('WU.iD_ZONA','=',$zonal);
              }
              if($banca){
                $sql = $sql->where('WU.BANCA','=',$banca);
              }
              if($jefatura){
                $sql = $sql->where('WU.ID_CENTRO','=',$jefatura);
              }              
                if($rol==Usuario::ROL_GERENTE_DIVISION_BE or $rol ==Usuario::ROL_GERENTE_BANCA){             
                  $sql = $sql->groupby(DB::raw("(TIPO_PRODUCTO + WEZ.REGISTRO)"),'TIPO_PRODUCTO','WU.ID_ZONA','WEZ.REGISTRO','WEZ.NOMBRE');        
                }            
                if($rol==Usuario::ROL_GERENCIA_ZONAL_BE){              
                  if($zonal<>'BELZONAL2'){
                    //JEFEZONAL<>BELZONAL2                
                    $sql = $sql->groupby(DB::raw("(TIPO_PRODUCTO + WU.REGISTRO)"),'TIPO_PRODUCTO','WU.ID_ZONA','WU.REGISTRO','WU.NOMBRE');
                  }
                  else{
                    //JEFEZONAL==BELZONA2                        
                    $sql = $sql->groupby(DB::raw("(TIPO_PRODUCTO + WEJ.REGISTRO)"),'TIPO_PRODUCTO','WU.ID_ZONA','WU.ID_CENTRO','WEJ.REGISTRO','WEJ.NOMBRE');  
                  }              
                }
                //JEFATURA
                if($rol==Usuario::ROL_JEFATURA_BE){                  
                  $sql = $sql->groupby(DB::raw("(TIPO_PRODUCTO + WU.REGISTRO)"),'TIPO_PRODUCTO','WU.ID_ZONA','WU.ID_CENTRO','WEJ.NOMBRE','WU.REGISTRO','WU.NOMBRE');        
                }          
        //DD($sql->toSql());
        return $sql->get();

  }

  function getResumenProyeccInicialJefes($ejecutivo,$periodo,$banca,$zonal,$jefatura,$rol){

      $sql = DB::table('WEBNOTE_PROYECCION_INICIAL as PI');
                  if($rol==Usuario::ROL_GERENTE_DIVISION_BE or $rol==Usuario::ROL_GERENTE_BANCA){
                    $sql = $sql->Select(DB::raw("(TIPO_PRODUCTO + WEZ.REGISTRO) AS IND"),'TIPO_PRODUCTO','WU.ID_ZONA AS ZONA','WEZ.REGISTRO AS REGISTRO','WEZ.NOMBRE AS ENCARGADO');        
                  }            
                  if($rol == Usuario::ROL_GERENCIA_ZONAL_BE){                  
                    if($zonal<>'BELZONAL2'){
                      //JEFEZONAL<>BELZONAL2
                      $sql = $sql->Select(DB::raw("(TIPO_PRODUCTO + WU.REGISTRO) AS IND"),'TIPO_PRODUCTO','WU.ID_ZONA','WU.REGISTRO AS REGISTRO','WU.NOMBRE AS ENCARGADO');
                    }
                    else{
                      //JEFEZONAL==BELZONA2                              
                      $sql = $sql->Select(DB::raw("(TIPO_PRODUCTO + WEJ.REGISTRO) AS IND"),'TIPO_PRODUCTO','WU.ID_ZONA','WU.ID_CENTRO','WEJ.REGISTRO AS REGISTRO','WEJ.NOMBRE AS ENCARGADO');  
                    }              
                  }
                  //JEFATURA
                  if($rol==Usuario::ROL_JEFATURA_BE){                    
                    $sql = $sql->Select(DB::raw("(TIPO_PRODUCTO + WU.REGISTRO) AS IND"),'TIPO_PRODUCTO','WU.ID_ZONA','WU.ID_CENTRO','WEJ.NOMBRE AS JEFATURA','WU.REGISTRO AS REGISTRO ','WU.NOMBRE AS ENCARGADO');        
                  } 
                $sql = $sql->addSelect(DB::raw('SUM(SALDO_DESEM_CERT/1000) AS SALDO_DESEM_CERT'), DB::raw('SUM(SALDO_PROYEC_CERT/1000) AS SALDO_PROYEC_CERT'))
                ->join('WEBVPC_PRODUCTO as PRO',function($join){ 
                $join->on('PRO.ID_PRODUCTO','=','PI.ID_PRODUCTO'); 
              })    
                ->join('WEBVPC_USUARIO as WU',function($join){ 
                $join->on('WU.REGISTRO','=','PI.REGISTRO_EN'); 
              })
                ->leftjoin('WEBVPC_USUARIO as WEZ',function($join){ 
                  $join->on('WEZ.ID_ZONA','=','WU.ID_ZONA');
                  $join->on('WEZ.ROL','=',DB::raw(Usuario::ROL_GERENCIA_ZONAL_BE/*25*/)); })
                ->leftjoin('WEBVPC_USUARIO as WEJ',function($join){ 
                  $join->on('WEJ.ID_CENTRO','=','WU.ID_CENTRO');
                  $join->on('WEJ.ROL','=',DB::raw(Usuario::ROL_JEFATURA_BE/*24*/)); })   
              ->where('FECHA','=',DB::raw("(SELECT MAX(FECHA_FOTO) FROM WEBNOTE_FECHA_FOTO WHERE APLICATIVO = 'EN_COLOC' AND PERIODO = '".$periodo."')"))
              ->whereIn('WU.ROL',[DB::raw(Usuario::ROL_EJECUTIVO_FARMER_BE),DB::raw(Usuario::ROL_EJECUTIVO_HUNTER_BE)]);;
              if($ejecutivo){
                  $sql = $sql->where('PI.REGISTRO_EN','=',$ejecutivo);
                }  
                if($zonal){
                  $sql = $sql->where('WU.ID_ZONA',$zonal);
                }
                if($banca){
                  $sql = $sql->where('WU.BANCA',$banca);
                } 
                if($jefatura){
                  $sql = $sql->where('WU.ID_CENTRO',$jefatura);
                }
              $sql = $sql->whereIn('TIPO_PRODUCTO', ['DIRECTAS', 'INDIRECTAS']);
              
                if($rol==Usuario::ROL_GERENTE_DIVISION_BE or $rol ==Usuario::ROL_GERENTE_BANCA){             
                  $sql = $sql->groupby(DB::raw("(TIPO_PRODUCTO + WEZ.REGISTRO)"),'TIPO_PRODUCTO','WU.ID_ZONA','WEZ.REGISTRO','WEZ.NOMBRE');        
                }            
                if($rol==Usuario::ROL_GERENCIA_ZONAL_BE){              
                  if($zonal<>'BELZONAL2'){
                    //JEFEZONAL<>BELZONAL2                
                    $sql = $sql->groupby(DB::raw("(TIPO_PRODUCTO + WU.REGISTRO)"),'TIPO_PRODUCTO','WU.ID_ZONA','WU.REGISTRO','WU.NOMBRE');
                  }
                  else{
                    //JEFEZONAL==BELZONA2                        
                    $sql = $sql->groupby(DB::raw("(TIPO_PRODUCTO + WEJ.REGISTRO)"),'TIPO_PRODUCTO','WU.ID_ZONA','WU.ID_CENTRO','WEJ.REGISTRO','WEJ.NOMBRE');  
                  }              
                }
                //JEFATURA
                if($rol==Usuario::ROL_JEFATURA_BE){                  
                  $sql = $sql->groupby(DB::raw("(TIPO_PRODUCTO + WU.REGISTRO)"),'TIPO_PRODUCTO','WU.ID_ZONA','WU.ID_CENTRO','WEJ.NOMBRE','WU.REGISTRO','WU.NOMBRE');        
                }                
        return $sql->get();
   } 

   function getResumenCaidasJefes($ejecutivo,$periodo,$banca,$zonal,$jefatura,$rol){      
      $sql2 = DB::table('WEBNOTE_OPERACIONES_NUEVAS as OP')
                ->select(DB::raw("'P' as VENC_AMORT"),'COD_UNICO','REGISTRO_EN',DB::raw("0 as FLG_SE_RENUEVA"),'TIPO_PRODUCTO', DB::raw('MONTO_CERT as MONTO_SOLES'), DB::raw("'0' AS PORC_RENOVACION"))
                ->join('WEBVPC_PRODUCTO as PRO',function($join){ 
                $join->on('PRO.ID_PRODUCTO','=','OP.ID_PRODUCTO'); 
                })
                ->join('WEBVPC_USUARIO as WU',function($join){ 
                $join->on('WU.REGISTRO','=','OP.REGISTRO_EN'); 
              });               
                if($ejecutivo){
                  $sql2 = $sql2->where('OP.REGISTRO_EN','=',$ejecutivo);
                }  
                if($zonal){
                  $sql2 = $sql2->where('WU.ID_ZONA',$zonal);
                }
                if($banca){
                  $sql2 = $sql2->where('WU.BANCA',$banca);
                }
                if($jefatura){
                 $sql2 = $sql2->where('WU.ID_CENTRO',$jefatura);   
                }
                $sql2 = $sql2->where('ID_TIPO_OPERACION','=',Db::raw(2))
                              ->where('FLG_DESEMBOLSADO','<>',Db::raw(1))
                              ->where('FLG_PERDIDO','<>',Db::raw(1))
                              ->where(DB::raw('MONTH(FECHA_DESEMBOLSO)'),'=',DB::raw("SUBSTRING('".$periodo."', 5, 2)"))
                              ->where(DB::raw('YEAR(FECHA_DESEMBOLSO)'),'=',DB::raw("SUBSTRING('".$periodo."', 1, 4)"));

      $sql1 = DB::table('WEBVPC_VENC_AMORT as VE')
                ->select('VENC_AMORT','COD_UNICO','REGISTRO_EN','FLG_SE_RENUEVA','TIPO_PRODUCTO','MONTO_SOLES', 'PORC_RENOVACION')
                ->join('WEBVPC_PRODUCTO as PRO',function($join){ 
                $join->on('PRO.ID_PRODUCTO','=','VE.ID_PRODUCTO'); 
                 })
                ->join('WEBVPC_USUARIO as WU',function($join){ 
                $join->on('WU.REGISTRO','=','VE.REGISTRO_EN'); 
              }) 
                ->where('VE.FECHA','=', DB::raw("(SELECT FECHA_ACT_SD FROM WEBNOTE_FECHAS WHERE PERIODO = '".$periodo."')"));               
                if($ejecutivo){
                  $sql1 = $sql1->where('REGISTRO_EN','=',$ejecutivo);
                }  
                if($zonal){
                  $sql1 = $sql1->where('WU.ID_ZONA',$zonal);
                }
                if($banca){
                  $sql1 = $sql1->where('WU.BANCA',$banca);
                }
                if($jefatura){
                 $sql1 = $sql1->where('WU.ID_CENTRO',$jefatura);   
                }
                $sql1 = $sql1->where(DB::raw('MONTH(FECHA_VENCIMIENTO)'),'=',DB::raw("SUBSTRING('".$periodo."', 5, 2)"))
                      ->where(DB::raw('YEAR(FECHA_VENCIMIENTO)'),'=',DB::raw("SUBSTRING('".$periodo."', 1, 4)"))
                      ->where(DB::raw("((VENC_AMORT = 'V') OR (VENC_AMORT = 'A' AND FECHA_VENCIMIENTO"), ">", DB::raw("(SELECT FECHA_ACT_SD FROM WEBNOTE_FECHAS WHERE PERIODO = '".$periodo."')))"))
                      ->unionAll($sql2);
        
      
      $sql3 = DB::table(DB::raw("({$sql1->toSql()}) as sub"))
              ->mergeBindings($sql1);
                
                //GERENTEDIVISIO
                  if($rol==Usuario::ROL_GERENTE_DIVISION_BE or $rol==Usuario::ROL_GERENTE_BANCA){
                    $sql3 = $sql3->Select(DB::raw("(TIPO_PRODUCTO + WEZ.REGISTRO) AS IND"),'TIPO_PRODUCTO','WU.ID_ZONA','WEZ.REGISTRO AS REGISTRO','WEZ.NOMBRE AS ENCARGADO');        
                  }            
                  if($rol == Usuario::ROL_GERENCIA_ZONAL_BE){                  
                    if($zonal<>'BELZONAL2'){
                      //JEFEZONAL<>BELZONAL2
                      $sql3 = $sql3->Select(DB::raw("(TIPO_PRODUCTO + WU.REGISTRO) AS IND"),'TIPO_PRODUCTO','WU.ID_ZONA','WU.REGISTRO AS REGISTRO','WU.NOMBRE AS ENCARGADO');
                    }
                    else{
                      //JEFEZONAL==BELZONA2                              
                      $sql3 = $sql3->Select(DB::raw("(TIPO_PRODUCTO + WEJ.REGISTRO) AS IND"),'TIPO_PRODUCTO','WU.ID_ZONA','WU.ID_CENTRO','WEJ.REGISTRO AS REGISTRO','WEJ.NOMBRE AS ENCARGADO');  
                    }              
                  }
                  //JEFATURA
                  if($rol==Usuario::ROL_JEFATURA_BE){                    
                    $sql3 = $sql3->Select(DB::raw("(TIPO_PRODUCTO + WU.REGISTRO) AS IND"),'TIPO_PRODUCTO','WU.ID_ZONA','WU.ID_CENTRO','WEJ.NOMBRE AS JEFATURA','WU.REGISTRO AS REGISTRO ','WU.NOMBRE AS ENCARGADO');        
                  }  
                $sql3 = $sql3->addSelect(DB::raw("SUM(CASE WHEN VENC_AMORT = 'V' THEN  (MONTO_SOLES * (1 - (CASE WHEN FLG_SE_RENUEVA=0 THEN 0 WHEN (porc_renovacion<1 AND PORC_RENOVACION>0) THEN PORC_RENOVACION ELSE 1 END))) ELSE MONTO_SOLES END)/1000 AS MONTO_CAIDA")) 
                ->join('WEBVPC_USUARIO as WU',function($join){ 
                  $join->on('WU.REGISTRO','=','SUB.REGISTRO_EN'); 
                  })
                ->leftjoin('WEBVPC_USUARIO as WEZ',function($join){ 
                  $join->on('WEZ.ID_ZONA','=','WU.ID_ZONA');
                  $join->on('WEZ.ROL','=',DB::raw(Usuario::ROL_GERENCIA_ZONAL_BE/*25*/)); })
                ->leftjoin('WEBVPC_USUARIO as WEJ',function($join){ 
                  $join->on('WEJ.ID_CENTRO','=','WU.ID_CENTRO');
                  $join->on('WEJ.ROL','=',DB::raw(Usuario::ROL_JEFATURA_BE/*24*/)); }) ; 
                  $sql3= $sql3->whereIn('WU.ROL',[DB::raw(Usuario::ROL_EJECUTIVO_FARMER_BE),DB::raw(Usuario::ROL_EJECUTIVO_HUNTER_BE)]);
              //GERENTEDIVISIO

                if($rol==Usuario::ROL_GERENTE_DIVISION_BE or $rol ==Usuario::ROL_GERENTE_BANCA){             
                  $sql3 = $sql3->groupby(DB::raw("(TIPO_PRODUCTO + WEZ.REGISTRO)"),'TIPO_PRODUCTO','WU.ID_ZONA','WEZ.REGISTRO','WEZ.NOMBRE');        
                }            
                if($rol==Usuario::ROL_GERENCIA_ZONAL_BE){              
                  if($zonal<>'BELZONAL2'){
                    //JEFEZONAL<>BELZONAL2                
                    $sql3 = $sql3->groupby(DB::raw("(TIPO_PRODUCTO + WU.REGISTRO)"),'TIPO_PRODUCTO','WU.ID_ZONA','WU.REGISTRO','WU.NOMBRE');
                  }
                  else{
                    //JEFEZONAL==BELZONA2                        
                    $sql3 = $sql3->groupby(DB::raw("(TIPO_PRODUCTO + WEJ.REGISTRO)"),'TIPO_PRODUCTO','WU.ID_ZONA','WU.ID_CENTRO','WEJ.REGISTRO','WEJ.NOMBRE');  
                  }              
                }
                //JEFATURA
                if($rol==Usuario::ROL_JEFATURA_BE){                  
                  $sql3 = $sql3->groupby(DB::raw("(TIPO_PRODUCTO + WU.REGISTRO)"),'TIPO_PRODUCTO','WU.ID_ZONA','WU.ID_CENTRO','WEJ.NOMBRE','WU.REGISTRO','WU.NOMBRE');        
                } 

        //dd($sql3->toSql());        
        return $sql3->get();
  }
  function validarPeriodo($periodo){
    if($periodo==\App\Entity\BE\SaldosDiarios::getParametros('PERIODO',null)){
            $periodo =$periodo -2;
    }
    else{
            $periodo =$periodo -1;
    }
    return $periodo;
  }

  
  function getPersonal($rol,$banca,$zonal,$jefatura){
              $sql= DB::table('WEBVPC_USUARIO as WU') ;                
                if($rol==Usuario::ROL_GERENTE_DIVISION_BE or (string) $rol ==Usuario::ROL_GERENTE_BANCA){             
                  $sql = $sql->Select('ROL','REGISTRO','NOMBRE',DB::RAW("REPLACE(REPLACE(REPLACE(ZONAL,'BC ',''),'BEP ',''),'BEL ','')  AS NOMBRE"))
                              ->join('WEBBE_ZONAL as WZ',function($join){ 
                                  $join->on('WZ.ID_ZONAl','=','WU.ID_ZONA');                  
                                })
                              ->where('ROL',DB::raw(Usuario::ROL_GERENCIA_ZONAL_BE/*25*/));
                  if($banca){
                    $sql = $sql->where('WU.BANCA',$banca);
                  }

                  $sql = $sql -> orderBy('ID_ZONAL');         
                }            
                if($rol==Usuario::ROL_GERENCIA_ZONAL_BE){              
                  $sql = $sql->Select('ROL','REGISTRO','NOMBRE');
                  if($zonal<>'BELZONAL2'){                    
                    $sql = $sql->whereIn('ROL',[DB::raw(Usuario::ROL_EJECUTIVO_FARMER_BE),DB::raw(Usuario::ROL_EJECUTIVO_HUNTER_BE)]);        
                    $sql = $sql->where('WU.ID_ZONA',$zonal);        

                    $sql = $sql -> orderBy('REGISTRO'); 
                  }
                  else{
                    $sql = $sql->Select('ROL','REGISTRO','NOMBRE');
                    //JEFEZONAL==BELZONA2                        
                    $sql = $sql->where('ROL',DB::raw(Usuario::ROL_JEFATURA_BE/*24*/));        
                    $sql = $sql->where('WU.ID_ZONA',$zonal);                      

                    $sql = $sql -> orderBy('REGISTRO'); 
                  }              
                }
                //JEFATURA
                if($rol==Usuario::ROL_JEFATURA_BE){                
                  $sql = $sql->Select('ROL','REGISTRO','NOMBRE');  
                  $sql = $sql->whereIn('ROL',[DB::raw(Usuario::ROL_EJECUTIVO_FARMER_BE),DB::raw(Usuario::ROL_EJECUTIVO_HUNTER_BE)]);        
                  $sql = $sql->where('WU.ID_ZONA',$zonal); 
                  $sql = $sql->where('WU.ID_CENTRO',$jefatura);         

                  $sql = $sql -> orderBy('REGISTRO'); 
                } 
                

                return $sql->get();

  }
}