<?php

namespace App\Model\BE;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class ZonalJefatura extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'WEBBE_ZONAL_JEFATURA';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = null;
	public $incrementing = false;

    function getZonales() {
        $sql = DB::table('WEBBE_ZONAL_JEFATURA as WZJ')
            ->select('WZJ.ID_ZONAL','WZJ.ZONAL')
            ->orderBy('WZJ.ZONAL')
            ->distinct();
        return $sql;
    }

    function getJefaturas($zonal = null) {
        $sql = DB::table('WEBBE_ZONAL_JEFATURA as WZJ')
            ->select('WZJ.ID_JEFATURA','WZJ.JEFATURA')
            ->orderBy('WZJ.JEFATURA')
            ->whereNotNull('WZJ.ID_JEFATURA')
            ->distinct();
        if ($zonal){
            $sql = $sql->where('WZJ.ID_ZONAL','=',$zonal);
        }
        return $sql;
    }

   

}