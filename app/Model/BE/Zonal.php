<?php

namespace App\Model\BE;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class Zonal extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'WEBBE_ZONAL';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = null;
	public $incrementing = false;

    function getZonales($banca =null,$zonal=null) {
        $sql = DB::table('WEBBE_ZONAL as WZJ')
            ->select('WZJ.ID_ZONAL','WZJ.ZONAL')
            ->orderBy('WZJ.ZONAL');
        if ($banca){
            $sql=$sql->where('WZJ.BANCA','=',$banca);
        }    
        if ($zonal){
            $sql=$sql->where('WZJ.ID_ZONAL','=',$zonal);
        } 
        return $sql;
    }

    function getBancas() {
        $sql = DB::table('WEBBE_ZONAL')
            ->select('BANCA')            
            ->distinct();
        return $sql;
    }
    
}