<?php

namespace App\Model\BE;
use DB;
use Log;
use Illuminate\Database\Eloquent\Model;


class ReporteEjecutivos extends Model {


	//Parámetros serán los filtros del reporte
	function getReporteEjecutivo($periodo,$filtros=null,$orden=null,$periodoActual){

		$sql= DB::table(DB::Raw("(SELECT [US].*,[ZONAL].ZONAL,[JEF].JEFATURA
				        FROM [WEBVPC_USUARIO] [US] LEFT JOIN [WEBBE_ZONAL] [ZONAL] ON [ZONAL].[ID_ZONAL] = [US].[ID_ZONA]
				           LEFT JOIN [WEBBE_JEFATURA] as [JEF] on [JEF].[ID_JEFATURA] = [US].[ID_CENTRO]
				        WHERE [US].[ROL] in ('20', '21')
				       AND [US].[FLAG_ACTIVO] = '1' ) as US"))
				->select("US.REGISTRO as REGISTRO",
				"US.NOMBRE as EJECUTIVO",
				"US.ZONAL as ZONAL",
				DB::Raw("ISNULL(US.JEFATURA,'-') AS JEFATURA"),
				       DB::Raw("ISNULL(LEAD_SEC.LEADS,0) AS LEADS"),
				       DB::Raw("ISNULL(LEAD_SEC.AVANCE,0) AS AVANCE"),
				       DB::Raw("ISNULL(LEAD_SEC.LEADS_ETAPA_CONTACTADOS,0) AS LEADS_ETAPA_CONTACTADOS"),
				       DB::Raw("ISNULL(LEAD_SEC.LEADS_ETAPA_EVALUACION_IBK,0) AS LEADS_ETAPA_EVALUACION_IBK"),
				       DB::Raw("ISNULL(WACT.CTD_VISITAS,0) AS VISITAS"))
				->leftjoin(DB::Raw("(
						SELECT
						      [LEAD_SEC].REGISTRO_EN,
						                   COUNT(DISTINCT [LEAD_SEC].NUM_DOC) LEADS,
						                   CASE WHEN COUNT(DISTINCT [LEAD_SEC].NUM_DOC)=0 THEN 0
						                   ELSE ROUND(COUNT(DISTINCT CASE WHEN ID_ETAPA NOT IN(1,2) THEN [LEAD_SEC].NUM_DOC ELSE NULL END)*100.0/COUNT(DISTINCT [LEAD_SEC].NUM_DOC),0) END  AVANCE,
						                   COUNT(DISTINCT CASE WHEN ID_ETAPA = 3 THEN [LEAD_SEC].NUM_DOC ELSE NULL END) LEADS_ETAPA_CONTACTADOS,
						                   COUNT(DISTINCT CASE WHEN ID_ETAPA = 4 THEN [LEAD_SEC].NUM_DOC ELSE NULL END) LEADS_ETAPA_EVALUACION_IBK
						            FROM (
						            SELECT DISTINCT [LEAD_SEC].NUM_DOC,
						            [LEAD_SEC].REGISTRO_EN,
						            [WLCE].ID_CAMP_EST,
						            [LEAD_SEC].FECHA_REGISTRO
						                FROM (
						                SELECT *
						                   FROM [WEBBE_LEAD_SECTORISTA] [LEAD_SEC]
						                   WHERE [FLG_ULTIMO] = '1'
						                   AND PERIODO='$periodo'
						                   AND FLG_CLIENTE=0
						)[LEAD_SEC]
						INNER JOIN (
						SELECT *
						                           FROM WEBBE_LEAD  [LEAD]
						                           WHERE PERIODO='$periodo'
						                           AND [LEAD].[ESTADO_LEAD] = '1'
						                )[LEAD] on [LEAD].[NUM_DOC] = [LEAD_SEC].[NUM_DOC]
						                INNER JOIN (
						                SELECT [WLCE].*
						                FROM [WEBVPC_LEAD_CAMP_EST] [WLCE]
						                           INNER JOIN (
						                               SELECT *
						                               FROM [WEBVPC_CAMP_EST]
						                               WHERE BANCA='BE'
						                               AND TIPO='PROSPECTOS'
						                           )[WCE] on [WLCE].[ID_CAMP_EST] = [WCE].[ID_CAMP_EST]
						                    WHERE PERIODO='$periodo'
						                )[WLCE] ON [LEAD].[NUM_DOC] = [WLCE].[NUM_DOC] AND [LEAD].[PERIODO] = [WLCE].[PERIODO]
						        )[LEAD_SEC]
						  LEFT JOIN (
						        SELECT [LEAD_ET].*
						               FROM [WEBBE_LEAD_ETAPA] [LEAD_ET]
						               INNER JOIN (
						                SELECT *FROM [WEBBE_ETAPA] WHERE TIPO='PROSPECTOS'
						)  [ETAPA] on [ETAPA].[ID_ETAPA] = [LEAD_ET].[ID_ETAPA]
						WHERE FLG_ULTIMO=1
						    )[LEAD_ET] ON [LEAD_SEC].NUM_DOC=[LEAD_ET].NUM_DOC
						    AND [LEAD_SEC].REGISTRO_EN=[LEAD_ET].REGISTRO_EN
						    AND [LEAD_SEC].ID_CAMP_EST=[LEAD_ET].ID_CAMP_EST
						    AND CAST([LEAD_ET].[FECHA_REGISTRO] AS DATE)>= CAST([LEAD_SEC].[FECHA_REGISTRO] AS DATE)
						          GROUP BY [LEAD_SEC].REGISTRO_EN
						) as LEAD_SEC"),function ($join)
				{
					$join->on("US.REGISTRO","=","LEAD_SEC.REGISTRO_EN");
				})
				->leftjoin(DB::Raw("(
				SELECT
				  REGISTRO_EN,
				  COUNT(1) AS CTD_VISITAS
				               FROM [WEBBE_ACTIVIDAD] [WACT]
				               WHERE TIPO='VISITA'
				               AND YEAR([WACT].[FECHA_ACTIVIDAD])*100+MONTH([WACT].[FECHA_ACTIVIDAD] )=YEAR(GETDATE())*100+MONTH([WACT].[FECHA_ACTIVIDAD])
				               GROUP BY REGISTRO_EN
				) WACT "),function ($join)
				{
					$join->on("WACT.REGISTRO_EN","=","LEAD_SEC.REGISTRO_EN");
				});
				
				//Deben de haber filtros avance, por zonal, por jefatura y por periodo				
				if(isset($filtros['zonal'])){
					$sql=$sql->where('US.ID_ZONA','=',$filtros['zonal']);
				}

				if(isset($filtros['banca'])){
					$sql=$sql->where('US.BANCA','=',$filtros['banca']);
				}

				if(isset($filtros['jefatura'])){
					if($filtros['jefatura']<> 'SinJefatura')
						$sql=$sql->where('US.ID_CENTRO','=',$filtros['jefatura']);
					else
						$sql=$sql->whereNull('US.ID_CENTRO');
				}


				if($filtros['avance']=='Mes'){ //Stock
					$sql=$sql->where('WLCE.FECHA_CARGA','>=',$periodoActual);
				}
				else if ($filtros['avance']=='Stock'){ //Mensual
					$sql=$sql->where('WLCE.FECHA_CARGA','<',$periodoActual);
				}

				if ($orden['sort']){
            		$sql = $sql->orderBy(DB::raw('ROUND(CAST(SUM(CASE WHEN LEAD_ET.ID_ETAPA NOT IN(1,2)  THEN 1 ELSE 0 END) AS FLOAT)/CAST (CASE WHEN COUNT(*)<>0 THEN COUNT(*) ELSE 1 END AS FLOAT)*100,2)'),$orden['order']);
				}else{
					$sql = $sql->orderBy('US.REGISTRO','asc');
				}
            	
            	           	
		return $sql;

	}

	function countReporteEjecutivo($periodo,$filtros,$orden,$periodoActual){

		$sql= DB::table(DB::Raw("(SELECT * FROM WEBVPC_USUARIO US WHERE [US].[ROL] in ('20', '21') AND [US].[FLAG_ACTIVO] = '1') AS US"))
				->select( DB::raw("COUNT(DISTINCT LEAD_SEC.REGISTRO_EN) as CANTIDAD") )
				->leftJoin(DB::Raw("(	
								SELECT [LEAD_SEC].*
								FROM ( SELECT * FROM [WEBBE_LEAD_SECTORISTA] [LEAD_SEC] WHERE [LEAD_SEC].[FLG_ULTIMO] = '1' AND PERIODO='$periodo' )[LEAD_SEC]
							    INNER JOIN (
									  SELECT * FROM [WEBBE_LEAD] [LEAD] WHERE PERIODO='$periodo' AND [LEAD].[ESTADO_LEAD] = '1'
							    )[LEAD] on [LEAD].[NUM_DOC] = [LEAD_SEC].[NUM_DOC] and [LEAD].[PERIODO] = [LEAD_SEC].[PERIODO]
							    INNER JOIN (
									SELECT [WLCE].* FROM [WEBVPC_LEAD_CAMP_EST] [WLCE]
							        inner join (
										SELECT * FROM [WEBVPC_CAMP_EST]WHERE BANCA='BE' AND TIPO='PROSPECTOS'
							        )[WCE] on [WLCE].[ID_CAMP_EST] = [WCE].[ID_CAMP_EST]
							   )[WLCE] ON [LEAD].[NUM_DOC] = [WLCE].[NUM_DOC] AND [LEAD].[PERIODO] = [WLCE].[PERIODO]
							) as LEAD_SEC"),function($join){
				 	$join->on('US.REGISTRO','=','LEAD_SEC.REGISTRO_EN');
				});

        if(isset($filtros['zonal'])){
			$sql=$sql->where('US.ID_ZONA','=',$filtros['zonal']);
		}

		if(isset($filtros['banca'])){
			$sql=$sql->where('US.BANCA','=',$filtros['banca']);
		}

		if(isset($filtros['jefatura'])){
			if($filtros['jefatura']<> 'SinJefatura')
				$sql=$sql->where('US.ID_CENTRO','=',$filtros['jefatura']);
			else
				$sql=$sql->whereNull('US.ID_CENTRO');
		}
		
        return $sql->get()->first();
	}

	function getBEResumenBanca($periodo,$idcentro=null)
	{
		$sql=DB::table(DB::Raw("(SELECT * FROM WEBVPC_USUARIO WHERE FLAG_ACTIVO=1 AND ROL IN (20,21)) as A"))
				->leftjoin(DB::Raw("(
					SELECT A.NUM_DOC,A.TIPO_PROSPECTO,B.REGISTRO_EN,B.FECHA_REGISTRO,C.ID_CAMP_EST,C.ESTRATEGIA,D.FECHA_REGISTRO AS FECHA_ETAPA,D.ETAPA,
	                    CASE WHEN F.NUM_DOC IS NOT NULL THEN 1 ELSE 0 END AS FLG_VISITA,
	                    F.FECHA_ACTIVIDAD AS FECHA_ULT_VISITA,
	                    A.DEUDA_DIRECTA,A.DEUDA_INDIRECTA,
	                    DATEDIFF(M,B.FECHA_REGISTRO,GETDATE()) AS DEMORA
	                FROM (
	                  		SELECT *
	                        FROM WEBBE_LEAD A
	                        WHERE PERIODO='$periodo'
	                        AND FLG_ES_CLIENTE='0'
	                        AND ESTADO_LEAD=1
	                ) A
	                INNER JOIN (
	                		SELECT *
	                        FROM WEBBE_LEAD_SECTORISTA
	                        WHERE PERIODO='$periodo'
	                        AND FLG_ULTIMO=1
	                        AND FLG_CLIENTE='0'
	                )B ON A.NUM_DOC=B.NUM_DOC
	                INNER JOIN (
	                			SELECT C1.*,C2.NOMBRE AS ESTRATEGIA
	                            FROM WEBVPC_LEAD_CAMP_EST C1
	                            INNER JOIN (
											SELECT *
	                                        FROM WEBVPC_CAMP_EST
	                                        WHERE BANCA='BE'
	                                        AND TIPO='PROSPECTOS'
	                            )C2 ON C1.ID_CAMP_EST=C2.ID_CAMP_EST
	                            WHERE C1.PERIODO='$periodo'
	                )C ON A.NUM_DOC=C.NUM_DOC
	                LEFT JOIN (
	                			SELECT D1.*,D2.NOMBRE AS ETAPA
	                            FROM WEBBE_LEAD_ETAPA D1
	                            INNER JOIN (
	   										SELECT *
	                                        FROM WEBBE_ETAPA 
	                                        WHERE TIPO='PROSPECTOS'
	                            ) D2 ON D1.ID_ETAPA=D2.ID_ETAPA
	                            WHERE D1.FLG_ULTIMO=1
	                ) D ON A.NUM_DOC=D.NUM_DOC
	                    AND D.ID_CAMP_EST=C.ID_CAMP_est
	                    AND B.REGISTRO_EN=D.REGISTRO_EN
	                    AND CAST(D.FECHA_REGISTRO AS DATE)>= CAST(B.FECHA_REGISTRO AS DATE)
	                LEFT JOIN (
	                			SELECT REGISTRO_EN,NUM_DOC,FECHA_ACTIVIDAD,
	                            ROW_NUMBER()OVER(PARTITION BY REGISTRO_EN,NUM_DOC ORDER BY FECHA_ACTIVIDAD DESC) AS ORDEN                                
	                            FROM WEBBE_ACTIVIDAD
	                            WHERE TIPO='VISITA'
	                )F ON B.REGISTRO_EN=F.REGISTRO_EN AND B.NUM_DOC=F.NUM_DOC AND F.ORDEN=1
	            ) as B"),function ($join)
	            {
	            	$join->on("A.REGISTRO","=","B.REGISTRO_EN");
	            })
	            ->leftJoin(DB::Raw("(
	        		SELECT B.REGISTRO_EN,
	                        SUM(CASE WHEN YEAR(FECHA_ACTIVIDAD)*100+MONTH(FECHA_ACTIVIDAD)=YEAR(GETDATE())*100+MONTH(GETDATE()) THEN 1 ELSE 0 END) AS CTD_VISITAS,
	                        COUNT(DISTINCT CASE WHEN YEAR(FECHA_ACTIVIDAD)*100+MONTH(FECHA_ACTIVIDAD)=YEAR(GETDATE())*100+MONTH(GETDATE())  THEN B.NUM_DOC END) AS CTD_CLIENTES_UNICOS,
	                        SUM(CASE WHEN B.ORDEN=1 AND YEAR(FECHA_ACTIVIDAD)*100+MONTH(FECHA_ACTIVIDAD)=YEAR(GETDATE())*100+MONTH(GETDATE()) THEN 1 ELSE 0 END) AS CTD_PRIMERAS_VISITAS
	                FROM (
	                		SELECT YEAR(FECHA_ACTIVIDAD)*100+MONTH(FECHA_ACTIVIDAD) AS PERIODO,REGISTRO_EN,NUM_DOC,FECHA_ACTIVIDAD,
	                        ROW_NUMBER()OVER(PARTITION BY REGISTRO_EN,NUM_DOC ORDER BY FECHA_ACTIVIDAD ASC) AS ORDEN
	                        FROM WEBBE_ACTIVIDAD
	                        WHERE TIPO='VISITA'
	                )B
	                GROUP BY B.REGISTRO_EN
	            ) as C"),function ($join)
	            {
	            	$join->on("A.REGISTRO","=","C.REGISTRO_EN");
	            })
	            ->where("A.ID_ZONA","=",DB::Raw("'BELZONAL2'"));
            if (!empty($idcentro)) {
            	$sql=$sql->where("A.ID_CENTRO","=",DB::Raw($idcentro));
            }
            return $sql->get();
		
	}
/*
		SELECT
			COUNT(1) AS CTD_LEADS,
	        SUM(CASE WHEN B.ETAPA IN ('Contactado','Evaluación IBK','Aprobado','Desembolsado','Denegado') THEN 1 ELSE 0 END) AS CTD_GESTIONADOS,
	        SUM(DEUDA_DIRECTA) AS DEUDA_DIRECTA,
	        SUM(DEUDA_INDIRECTA) AS DEUDA_INDIRECTA,
	        CAST(SUM(CASE WHEN B.ETAPA IN ('Contactado','Evaluación IBK','Aprobado','Desembolsado','Denegado') THEN 1 ELSE 0 END) AS FLOAT)/COUNT(1) AS PCT_GESTION,
	        SUM(CASE WHEN B.ETAPA='Pendiente' THEN 1 ELSE 0 END) CTD_PEND,
	        SUM(CASE WHEN B.ETAPA='Me interesa' THEN 1 ELSE 0 END) CTD_MEINT,
	        SUM(CASE WHEN B.ETAPA='Contactado' THEN 1 ELSE 0 END) CTD_CONTAC,
	        SUM(CASE WHEN B.ETAPA='Evaluación IBK' THEN 1 ELSE 0 END) EVAL,
	        SUM(CASE WHEN B.ETAPA='Aprobado' THEN 1 ELSE 0 END) CTD_APROB,
	        SUM(CASE WHEN B.ETAPA='Denegado' THEN 1 ELSE 0 END) CTD_DENEG,
	        SUM(CASE WHEN B.DEMORA=1 THEN 1 ELSE 0 END) CTD_PERMA_1,
	        SUM(CASE WHEN B.DEMORA=2 THEN 1 ELSE 0 END) CTD_PERMA_2,
	        SUM(CASE WHEN B.DEMORA=3 THEN 1 ELSE 0 END) CTD_PERMA_3,
	        SUM(CASE WHEN B.DEMORA=4 THEN 1 ELSE 0 END) CTD_PERMA_4,
	        SUM(CASE WHEN B.DEMORA=5 THEN 1 ELSE 0 END) CTD_PERMA_5,
	        SUM(CASE WHEN B.DEMORA=6 THEN 1 ELSE 0 END) CTD_PERMA_6,
	        SUM(CASE WHEN B.DEMORA>6 THEN 1 ELSE 0 END) CTD_PERMA_7,
	        SUM(CTD_VISITAS) AS CTD_VISITAS,
	        SUM(CTD_CLIENTES_UNICOS) AS CTD_CLIENTES_UNICOS,
	        SUM(CTD_PRIMERAS_VISITAS) AS CTD_PRIMERAS_VISITAS
		FROM (
			SELECT *
	        FROM WEBVPC_USUARIO
	        WHERE FLAG_ACTIVO=1
	        AND ROL IN (20,21)
	        AND ID_ZONA='BELZONAL2'
	    ) A
	    LEFT JOIN (  
	  				SELECT A.NUM_DOC,A.TIPO_PROSPECTO,B.REGISTRO_EN,B.FECHA_REGISTRO,C.ID_CAMP_EST,C.ESTRATEGIA,D.FECHA_REGISTRO AS FECHA_ETAPA,D.ETAPA,
	                    CASE WHEN F.NUM_DOC IS NOT NULL THEN 1 ELSE 0 END AS FLG_VISITA,
	                    F.FECHA_ACTIVIDAD AS FECHA_ULT_VISITA,
	                    A.DEUDA_DIRECTA,A.DEUDA_INDIRECTA,
	                    DATEDIFF(M,B.FECHA_REGISTRO,GETDATE()) AS DEMORA
	                FROM (
	                  		SELECT *
	                        FROM WEBBE_LEAD A
	                        WHERE PERIODO=201910
	                        AND FLG_ES_CLIENTE='0'
	                        AND ESTADO_LEAD=1
	                ) A
	                INNER JOIN (
	                		SELECT *
	                        FROM WEBBE_LEAD_SECTORISTA
	                        WHERE PERIODO=201910
	                        AND FLG_ULTIMO=1
	                        AND FLG_CLIENTE='0'
	                )B ON A.NUM_DOC=B.NUM_DOC
	                INNER JOIN (
	                			SELECT C1.*,C2.NOMBRE AS ESTRATEGIA
	                            FROM WEBVPC_LEAD_CAMP_EST C1
	                            INNER JOIN (
												SELECT *
	                                        FROM WEBVPC_CAMP_EST
	                                        WHERE BANCA='BE'
	                                        AND TIPO='PROSPECTOS'
	                            )C2 ON C1.ID_CAMP_EST=C2.ID_CAMP_EST
	                            WHERE C1.PERIODO=201910
	                )C ON A.NUM_DOC=C.NUM_DOC
	                LEFT JOIN (
	                			SELECT D1.*,D2.NOMBRE AS ETAPA
	                            FROM WEBBE_LEAD_ETAPA D1
	                            INNER JOIN (
	   										SELECT *
	                                        FROM WEBBE_ETAPA 
	                                        WHERE TIPO='PROSPECTOS'
	                            ) D2 ON D1.ID_ETAPA=D2.ID_ETAPA
	                            WHERE D1.FLG_ULTIMO=1
	                ) D ON A.NUM_DOC=D.NUM_DOC
	                    AND D.ID_CAMP_EST=C.ID_CAMP_est
	                    AND B.REGISTRO_EN=D.REGISTRO_EN
	                    AND CAST(D.FECHA_REGISTRO AS DATE)>= CAST(B.FECHA_REGISTRO AS DATE)
	                LEFT JOIN (
	                			SELECT REGISTRO_EN,NUM_DOC,FECHA_ACTIVIDAD,
	                            ROW_NUMBER()OVER(PARTITION BY REGISTRO_EN,NUM_DOC ORDER BY FECHA_ACTIVIDAD DESC) AS ORDEN                                
	                            FROM WEBBE_ACTIVIDAD
	                            WHERE TIPO='VISITA'
	                )F ON B.REGISTRO_EN=F.REGISTRO_EN AND B.NUM_DOC=F.NUM_DOC AND F.ORDEN=1
	    ) B ON A.REGISTRO=B.REGISTRO_EN
	    LEFT JOIN (
	    			SELECT B.REGISTRO_EN,
	                        SUM(CASE WHEN YEAR(FECHA_ACTIVIDAD)*100+MONTH(FECHA_ACTIVIDAD)=YEAR(GETDATE())*100+MONTH(GETDATE()) THEN 1 ELSE 0 END) AS CTD_VISITAS,
	                        COUNT(DISTINCT CASE WHEN YEAR(FECHA_ACTIVIDAD)*100+MONTH(FECHA_ACTIVIDAD)=YEAR(GETDATE())*100+MONTH(GETDATE())  THEN B.NUM_DOC END) AS CTD_CLIENTES_UNICOS,
	                        SUM(CASE WHEN B.ORDEN=1 AND YEAR(FECHA_ACTIVIDAD)*100+MONTH(FECHA_ACTIVIDAD)=YEAR(GETDATE())*100+MONTH(GETDATE()) THEN 1 ELSE 0 END) AS CTD_PRIMERAS_VISITAS
	                FROM (
	                		SELECT YEAR(FECHA_ACTIVIDAD)*100+MONTH(FECHA_ACTIVIDAD) AS PERIODO,REGISTRO_EN,NUM_DOC,FECHA_ACTIVIDAD,
	                        ROW_NUMBER()OVER(PARTITION BY REGISTRO_EN,NUM_DOC ORDER BY FECHA_ACTIVIDAD ASC) AS ORDEN
	                        FROM WEBBE_ACTIVIDAD
	                        WHERE TIPO='VISITA'
	                )B
	                GROUP BY B.REGISTRO_EN
	    )C ON A.REGISTRO=C.REGISTRO_EN
*/	
}
