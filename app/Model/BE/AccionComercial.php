<?php
namespace App\Model\BE;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;
use App\Model\BE\Lead as mLead;
use App\Entity\BE\Etapa;

class AccionComercial extends Model

{
  protected $table = 'WEBBE_LEAD';
    
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey =['PERIODO',
                            'NUM_DOC',
                            ];
                  

        /**
     * The name of the "created at" column.
     *
     * @var string
     */
     public $timestamps = false;
    
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'NOMBRE'
        ];
                                                                                                                                                                                                                                    

    /**
     * Devuelve todos los datos del lead
     *
     * @param string $periodo Periodo en que fue cargado el Lead.
     * @param string $lead Numero de documento del lead.
     * @param array $filtros Filtros de busqueda
     * @return table
     */

    function getTipos(){
        $sql=DB::table('WEBVPC_LEAD_CAMP_EST')
            ->select('TIPO_ASIGNACION')
            ->where('TIPO_ASIGNACION','<>','')
            ->distinct();
        return $sql;
    }

    function getNumDoc($codUnico,$periodo=NULL){
        $sql=DB::table('WEBBE_LEAD')
        ->select(DB::Raw('NUM_DOC'))
        ->where('COD_UNICO','=',$codUnico);

        if($periodo){
          $sql=$sql->where('PERIODO','=',$periodo);
        }
        return $sql;
    }

    function getCodUnico($numDoc,$periodo=NULL){
        $sql=DB::table('WEBBE_LEAD')
        ->select(DB::Raw('COD_UNICO'))
        ->where('NUM_DOC','=',$numDoc);

        if($periodo){
          $sql=$sql->where('PERIODO','=',$periodo);
        }
        return $sql;
    }

    function getMesesAsignacion($ejecutivo=NULL){
        $sql=DB::table('WEBVPC_LEAD_CAMP_EST')
            ->select(DB::Raw("CASE WHEN MONTH(FECHA_CARGA)=1 THEN 'Enero' 
                                WHEN MONTH(FECHA_CARGA)=2 THEN 'Febrero' 
                                WHEN MONTH(FECHA_CARGA)=3 THEN 'Marzo' 
                                WHEN MONTH(FECHA_CARGA)=4 THEN 'Abril' 
                                WHEN MONTH(FECHA_CARGA)=5 THEN 'Mayo' 
                                WHEN MONTH(FECHA_CARGA)=6 THEN 'Junio' 
                                WHEN MONTH(FECHA_CARGA)=7 THEN 'Julio' 
                                WHEN MONTH(FECHA_CARGA)=8 THEN 'Agosto' 
                                WHEN MONTH(FECHA_CARGA)=9 THEN 'Setiembre' 
                                WHEN MONTH(FECHA_CARGA)=10 THEN 'Octubre' 
                                WHEN MONTH(FECHA_CARGA)=11 THEN 'Noviembre' 
                                WHEN MONTH(FECHA_CARGA)=12 THEN 'Diciembre' END 
                                + ' '+CAST(YEAR(FECHA_CARGA) AS VARCHAR) AS MES_ASIGNACION"),
                        DB::Raw('MONTH(FECHA_CARGA) AS MES'),
                    DB::Raw('YEAR(FECHA_CARGA) AS ANHO'),DB::Raw("CAST(YEAR(FECHA_CARGA) AS VARCHAR) +CASE WHEN MONTH(FECHA_CARGA)<10 THEN '0' + CAST(MONTH(FECHA_CARGA) AS VARCHAR) 
                    ELSE CAST(MONTH(FECHA_CARGA)AS VARCHAR) END AS PERIODO")
                )
            ->whereIn('TIPO_ASIGNACION',['INTELIGENCIA','PLANING','EJECUTIVO'])            
            ->orderBy(DB::Raw('YEAR(FECHA_CARGA)'),'ASC')
            ->orderBy(DB::Raw('MONTH(FECHA_CARGA)'),'ASC');


        if($ejecutivo!=NULL){
            $sql=$sql->where('REGISTRO_RESPONSABLE','=',$ejecutivo)->distinct();
        }
        else{
            $sql=$sql->distinct();
        }

        return $sql;
    }

    function getNombreAcciones($idAccion=NULL){
        $sql=DB::table('WEBVPC_CAMP_EST')
            ->select('ID_CAMP_EST','NOMBRE')
            ->where('BANCA','BE')
            ->where('TIPO','ACCION')
            ->where('HABILITADO','=',1);
        if($idAccion!=NULL) $sql=$sql->where('ID_CAMP_EST','=',$idAccion);
        return $sql;
    }

     function getNombreLista($idAcciones=[]){
        $sql=DB::table('WEBVPC_CAMP_EST')
            ->select('NOMBRE')
            ->where('BANCA','BE')
            ->where('TIPO','ACCION')
            ->whereIn('ID_CAMP_EST',$idAcciones);        
        return $sql;
    }

    function getAccionCliente($periodo,$numDoc,$idAccion,$tooltip=null){
         $sql=DB::table('WEBVPC_LEAD_CAMP_EST')
            ->select('ID_CAMP_EST','NUM_DOC')
            //->where('PERIODO','=',$periodo)
            ->where('NUM_DOC','=',$numDoc)
            ->where('ID_CAMP_EST','=',$idAccion)
            ->where('FLG_HABILITADO','=',1);
        if($tooltip!=NULL)
            $sql=$sql->where('TOOLTIP','=',$tooltip);
        return $sql;
    }

    function getCliente($periodo,$codUnico){

        $sql=DB::table('WEBBE_LEAD as WL')
            ->select('WL.NOMBRE','WLS.COD_SECT_UNIQ_EN','WLS.REGISTRO_EN','WU.NOMBRE AS NOMBRE_EJECUTIVO',
              'WL.COD_UNICO','WL.NUM_DOC','WL.FLG_ES_CLIENTE','WU.ID_ZONA AS ZONA')
            ->join('WEBBE_LEAD_SECTORISTA as WLS', function($join){
                $join->on('WL.PERIODO', '=', 'WLS.PERIODO');
                $join->on('WL.NUM_DOC', '=', 'WLS.NUM_DOC');
            })
            ->join('WEBVPC_USUARIO_ANT as WU', function($join){
                $join->on('WU.REGISTRO', '=', 'WLS.REGISTRO_EN');
            })
            ->where('WL.PERIODO','=',$periodo)
            ->where('WLS.FLG_ULTIMO','=',1)
            ->where('WL.COD_UNICO','like','%'.$codUnico)
            ->where('WU.FLAG_ACTIVO','=',1);

        return $sql;
    }

    function getEtapaAccion($idAccion,$idEtapa=NULL){
         $sql=DB::table('WEBBE_ETAPA_CAMP as WEC')
            ->select('WEC.ID_ETAPA','WEC.ID_CAMP_EST','WE.NOMBRE AS NOMBRE_ETAPA','WCE.NOMBRE AS NOMBRE_ACCION')
            ->join('WEBBE_ETAPA as WE', function($join){
                $join->on('WE.ID_ETAPA', '=', 'WEC.ID_ETAPA');
            })
            ->join('WEBVPC_CAMP_EST as WCE', function($join){
                $join->on('WCE.ID_CAMP_EST', '=', 'WEC.ID_CAMP_EST');
            })
            ->where('WEC.ID_CAMP_EST','=',$idAccion);

            if($idEtapa!=NULL)
                $sql=$sql->where('WEC.ID_ETAPA','=',$idEtapa);
            
        return $sql;
    }

    function getCategorias(){
        return DB::table('AC_CATEGORIA')
                ->select('CATEGORIA');
    }

    function getAccionesEstrategias(){
        return DB::table('WEBVPC_CAMP_EST')
                ->select()
                ->addSelect(DB::Raw("TIPO_KPI +CASE WHEN TIPO_KPI='TX' THEN '' ELSE ' S/ (Miles)'  END PLACEHOLDER"))
                ->where('TIPO','=','ACCION')
                ->orderBy('ORDEN','ASC')                
                ->orderBy('ORDEN_ACCION','ASC');                
    }

    function getAccionesExistentes($periodo,$numDoc,$checks){

        $sql=DB::table(DB::Raw(
                "(SELECT WCE.ID_CAMP_EST,WCE.NOMBRE AS ACCION
                FROM WEBBE_LEAD AS WL 
                LEFT JOIN WEBVPC_LEAD_CAMP_EST AS WLCE ON WLCE.NUM_DOC = WL.NUM_DOC 
                INNER JOIN WEBVPC_CAMP_EST AS WCE ON WCE.ID_CAMP_EST=WLCE.ID_CAMP_est                 
                WHERE WL.PERIODO = '".$periodo."'                
                AND WCE.TIPO = '"."ACCION"."'
                AND WLCE.FLG_HABILITADO = 1
                AND WCE.HABILITADO = 1 
                AND wl.NUM_DOC='".$numDoc."') ACC_CLI"
            ))
            ->select('ACC_CLI.ACCION')
            ->whereIn('ID_CAMP_EST',$checks);         

        return $sql;  
    }

    function tieneCampanhaActiva($periodo,$numDoc){

        $sql=DB::table('WEBVPC_LEAD_CAMP_EST AS WLCE')
            ->select(DB::Raw('COUNT(*) CUENTA'))
            ->join('WEBVPC_CAMP_EST AS WCE',function($join){
                $join->on('WLCE.ID_CAMP_EST', '=', 'WCE.ID_CAMP_EST');
            })
            ->where('PERIODO','=',$periodo)
            ->where('WCE.TIPO','=','PROSPECTOS')
            ->where('NUM_DOC','=',$numDoc);
        return $sql->first()->CUENTA;
    }

    //Utils
    function getEstrategias($idAccion =null){
        $sql=DB::table('WEBVPC_CAMP_EST')
            ->select(DB::Raw('DISTINCT ESTRATEGIA AS ESTRATEGIA'),'TIPO_KPI')
            ->where('TIPO','=','ACCION');
        if(isset($idAccion))
            $sql=$sql->where('ID_CAMP_EST','=',$idAccion);
        return $sql;
    }


    function getEtapas($accion=null,$estrategia=null){
        $sql=DB::table('WEBBE_ETAPA_CAMP AS WEC')
            ->select('WE.ID_ETAPA','WE.NOMBRE AS ETAPA')
            ->join('WEBBE_ETAPA as WE', function($join){
                $join->on('WE.ID_ETAPA', '=', 'WEC.ID_ETAPA');
            });

        if(isset($accion))
            $sql=$sql->where('WEC.ID_CAMP_EST','=',$accion);

        return $sql;
    }

    function getAcciones($estrategia=null){
        $sql=DB::table('WEBVPC_CAMP_EST')
            ->select('ID_CAMP_EST AS ID_ACCION','NOMBRE AS ACCION')
            ->where('TIPO','=','ACCION');

        if(isset($estrategia))
            $sql=$sql->where('ESTRATEGIA','=',$estrategia);
        return $sql;
    }

    function getMesesActivacion(){
        $sql=DB::table('WEBBE_MESES_ACTIVACION')
            ->select()
            ->where('FECHA_FIN','>=',DB::Raw('GETDATE()'));

        return $sql;
    }

    function getAccionAvanzada($numDoc){
        return DB::select('exec AC_ESTRATEGIA_MAS_AVANZADA ?',array($numDoc));
    }

    function getAccionesComerciales($periodo,$filtros = [],$orden=[]){      

        $sql=DB::Table('WEBBE_LEAD AS WL') 
                ->select( 'WL.NUM_DOC',
                          'WCE.NOMBRE AS ACCION',
                          'WCE.ESTRATEGIA',
                          DB::Raw('CONVERT(INT,WLCE.PRIORIDAD) AS ESTRELLA'),
                          'WCE.TIPO_KPI',
                          'WL.NOMBRE AS NOMBRE_EMPRESA',
                          'WL.COD_UNICO',
                          'WL.CATEGORIA',
                          'WL.GRUPO_ECONOMICO',
                          'WL.VENTAS',
                          'WL.COSTO_VENTAS',
                          'WLCE.KPI',
                          'WLCE.TIPO_ASIGNACION',//ELIMINAR LUEGO 
                          'WLCE.ID_CAMP_EST',
                          'WLCE.FLG_ACCION',
                          DB::Raw('(WL.DEUDA_DIRECTA + WL.DEUDA_INDIRECTA) AS DEUDA_TOTAL_RCC'),
                          'WL.DEUDA_IBK AS DEUDA_IBK',
                        //   DB::Raw('(ISNULL(WL.DEUDA_IBK,0)/dbo.ISZERO(ISNULL(WL.DEUDA_DIRECTA + WL.DEUDA_INDIRECTA,1),1)) AS SOW'),
                        DB::Raw('0 AS SOW'),
                          'WE.NOMBRE AS ETAPA',
                          DB::Raw('DATEDIFF(dd, WLCE.FECHA_CARGA, GETDATE()) AS DIAS_ASIGNACION'),
                          DB::Raw('DATEDIFF(dd, WLCE.FECHA_CARGA, GETDATE())/30 AS MESES_ASIGNACION'),
                          DB::Raw('DATEDIFF(DD, WLE.FECHA_REGISTRO, GETDATE()) DIAS_GESTION'),
                          DB::Raw('DATEDIFF(DAY, GETDATE(), WLCE.FECHA_FIN) AS DIAS_VENCER'),
                          'WLCE.TOOLTIP',
                          'WLE.ID_ETAPA',
                          'WCE.ID_CAMP_est AS ID_ACCION',
                          DB::Raw('CONVERT(date, WLCE.FECHA_INICIO, 103) AS FECHA_INICIO'),
                          DB::Raw('CONVERT(date, WLCE.FECHA_FIN, 103) AS FECHA_FIN'),
                          DB::Raw('CONVERT(date, WLCE.FECHA_CARGA, 103) AS FECHA_CARGA'),
                          DB::Raw('CONVERT(date, WLE.FECHA_REGISTRO, 103) AS FECHA_ETAPA'),
                          DB::Raw("ISNULL(WLCE.MES_ACTIVACION,'-') AS MES_ACTIVACION"),
                          'WL.BANCO_PRINCIPAL',
                          'WL.PROVINCIA',
                          'WL.DISTRITO',
                          'WL.DEPARTAMENTO',
                          'WL.DIRECCION',
                          'WL.TELEFONO',
                          'WL.FLG_ES_CLIENTE'//,'UWUR.NOMBRE AS NOMBRE_REGISTRO_ACCION', 'UWUR.CARGO AS CARGO_REGISTRO_ACCION'//,
                          //DB::Raw(" UWUR.NOMBRE +' - '+UWUR.CARGO AS NOMBRE_CARGO")
                          //DB::Raw('CASE WHEN WLI.REGISTRO_EN IS NULL THEN 0 ELSE 1 END AS FLG_LINKEDIN_ACTIVIDAD')
                        )
        ->leftJoin('WEBVPC_LEAD_CAMP_EST AS WLCE' ,function($join){ 
            $join->on('WLCE.NUM_DOC', '=', 'WL.NUM_DOC');
        })
        ->join('WEBVPC_CAMP_EST AS WCE',function($join){
            $join->on('WLCE.ID_CAMP_EST', '=', 'WCE.ID_CAMP_EST');
        })
        ->leftJoin('WEBBE_LEAD_ETAPA AS WLE',function($join){
            $join->on('WLE.NUM_DOC', '=', 'WL.NUM_DOC');
            $join->on('WLE.ID_CAMP_EST', '=', 'WLCE.ID_CAMP_EST');
            $join->on('WLE.TOOLTIP','=', 'WLCE.TOOLTIP');
        })
        ->join('WEBBE_ETAPA_CAMP AS WEC',function($join){
            $join->on('WEC.ID_ETAPA', '=', 'WLE.ID_ETAPA');
            $join->on('WEC.ID_CAMP_EST', '=', 'WLCE.ID_CAMP_EST');
        })
        ->join('WEBBE_ETAPA AS WE',function($join){
            $join->on('WE.ID_ETAPA', '=', 'WEC.ID_ETAPA');
        })
        /*->leftJoin('WEBVPC_USUARIO_ANT AS UWUR',function($join){
            $join->on('UWUR.REGISTRO', '=', 'WLCE.REGISTRO_RESPONSABLE');
        })*/
        ->where('WL.PERIODO' ,'=', $periodo)
        ->where('WL.ESTADO_LEAD' ,'=',1)
        ->where('WCE.TIPO','=','ACCION')        
        ->where('WLCE.FLG_HABILITADO' ,'=', 1)
        ->where('WLE.FLG_ULTIMO' ,'=', 1);
        //->where('WCE.HABILITADO','=',1);   


        if (isset($filtros['codUnico'])){
            $sql = $sql->where('WL.COD_UNICO','like','%'.$filtros['codUnico'].'%');
        }
        if (isset($filtros['documento'])){
            $sql = $sql->where('WL.NUM_DOC','=',$filtros['documento']);
        }

        if (isset($filtros['razonSocial'])){
            $sql = $sql->where('WL.NOMBRE','like','%'.$filtros['razonSocial'].'%');
        }             
        
        if (isset($filtros['categoria'])){
            $sql = $sql->where('WL.CATEGORIA','=',$filtros['categoria']);
        }

        if (isset($filtros['estrategia'])){
            $sql = $sql->where('WCE.ESTRATEGIA','=',$filtros['estrategia']);
        }

        if (isset($filtros['accion'])){
            $sql = $sql->where('WCE.ID_CAMP_EST','=',$filtros['accion']);
        }

        if (isset($filtros['etapa'])){
            $sql = $sql->where('WLE.ID_ETAPA','=',$filtros['etapa']);
        }

        if (isset($filtros['tooltip'])){
            $sql = $sql->where('WLCE.TOOLTIP','=',$filtros['tooltip']);
        }

        if (isset($filtros['flgAccion'])){
            $sql = $sql->where('WLCE.FLG_ACCION','=',$filtros['flgAccion']);
        }
                         
        if (isset($filtros['semaforo'])){
            if($filtros['semaforo']=='Rojo')
                $sql = $sql->where(DB::Raw('DATEDIFF(dd, WLCE.FECHA_CARGA, GETDATE())/30'),'=>',2);
            else if ($filtros['semaforo']=='Ámbar')
                $sql = $sql->where(DB::Raw('DATEDIFF(dd, WLCE.FECHA_CARGA, GETDATE())/30'),'=',1);
            else if ($filtros['semaforo']=='Verde')
                $sql = $sql->where(DB::Raw('DATEDIFF(dd, WLCE.FECHA_CARGA, GETDATE())/30'),'=',0);
        }
        
        return $sql;
        
    }

    function getClienteActividades($periodo,$filtros = []){    

        $sql=DB::Table('WEBBE_LEAD AS WL') 
                ->select( 'WL.NUM_DOC',                          
                          'WL.NOMBRE AS NOMBRE_EMPRESA',
                          'WL.COD_UNICO',
                          'WL.CATEGORIA',                 
                          DB::Raw('(WL.DEUDA_DIRECTA + WL.DEUDA_INDIRECTA) AS DEUDA_TOTAL_RCC'),                                              
                          'WL.BANCO_PRINCIPAL',
                          'WL.PROVINCIA',
                          'WL.DISTRITO',
                          'WL.DEPARTAMENTO',
                          'WL.DIRECCION',
                          'WL.TELEFONO',
                          'WL.FLG_ES_CLIENTE','WU.*',DB::Raw('CASE WHEN WLI.REGISTRO_EN IS NULL THEN 0 ELSE 1 END AS FLG_LINKEDIN_ACTIVIDAD'))
        ->leftJoin('WEBBE_LEAD_SECTORISTA AS WLS' ,function($join){ 
            $join->on('WLS.NUM_DOC', '=', 'WL.NUM_DOC');
            $join->on('WLS.PERIODO', '=', 'WL.PERIODO');
        })     
 
        ->leftJoin('WEBVPC_USUARIO_ANT AS WU',function($join){ 
                    $join->on('WU.REGISTRO','=','WLS.REGISTRO_EN');
        })
        ->leftJoin('WEBVPC_LINKEDIN as WLI', function($join){
                $join->on('WLS.REGISTRO_EN', '=', 'WLI.REGISTRO_EN');
            })
        ->where('WL.PERIODO' ,'=', $periodo)
        ->where('WL.FLG_ES_CLIENTE' ,'=', 1)
        ->where('WL.ESTADO_LEAD' ,'=',1);   
      
        if (isset($filtros['documento'])){
            $sql = $sql->where('WL.NUM_DOC','=',$filtros['documento']);
        }
        return $sql;
        
    }
    

    function getOrderColumn($field){
        switch ($field) {
                case 'categoria':
                    return 'WL.CATEGORIA';       
                case 'mesActivacion':
                    return 'WLCE.MES_ACTIVACION';                 
                case 'volumen':
                    return 'WLCE.KPI'; 
                case 'ventas':
                    return 'WL.VENTAS';
                case 'deudaRCC':
                    return 'WL.DEUDA_DIRECTA + WL.DEUDA_INDIRECTA';
                case 'deudaIBK':
                    return 'WL.DEUDA_IBK';
                case 'accion':
                    return 'WCE.NOMBRE';
                case 'etapa':
                    return 'WE.NOMBRE';
            };
    }

    function getAccionesVisualizacion($periodo,$filtros = [],$orden=[]){
      //dd($this->getAccionesComerciales($periodo,$filtros,$orden)->get());
        $sql=$this->getAccionesComerciales($periodo,$filtros,$orden)
                    ->addSelect('WAV_EN.NOMBRE AS NOMBRE_REGISTRO_ACCION',
                                'WAV_EN.REGISTRO AS EN',
                                'WAV_EN.ENCARGADO AS ENCARGADO_EN',
                                'WAV_EN.NOMBRE AS NOMBRE_EN',
                                'WAV_EN.ID_ZONA AS ZONA_EN',
                                'WAV_EN.ID_CENTRO AS JEFE_EN',
                                'WAV_EN.BANCA',
                                'WAV_EC.REGISTRO AS EC',
                                'WAV_EC.ENCARGADO AS  ENCARGADO_EC',
                                'WAV_EC.NOMBRE AS NOMBRE_EC',
                                'WAV_EC.ID_ZONA AS ZONA_EC',
                                'WAV_EC.ID_CENTRO AS JEFE_EC',
                                DB::Raw('CASE WHEN WLI.REGISTRO_EN IS NULL THEN 0 ELSE 1 END AS FLG_LINKEDIN_ACTIVIDAD'))
            ->leftJoin(DB::Raw("(SELECT WAV.REGISTRO, WAV.ENCARGADO, WAV.NUM_DOC, WAV.ID_CAMP_EST,
                                WAV.TOOLTIP, WU.NOMBRE, WU.ID_ZONA, WU.ID_CENTRO, WU.BANCA FROM WEBBE_ACCION_VISUALIZACION  WAV
              LEFT JOIN WEBVPC_USUARIO_ANT WU ON WU.REGISTRO=WAV.REGISTRO
              WHERE FLG_ULTIMO=1 AND TIPO_EJ='"."EN"."') WAV_EN"),function ($join){
                    $join->on('WAV_EN.NUM_DOC','=','WLCE.NUM_DOC'); 
                    $join->on('WAV_EN.ID_CAMP_EST','=','WLCE.ID_CAMP_EST'); 
                    $join->on('WAV_EN.TOOLTIP','=','WLCE.TOOLTIP');                              
            })           
            ->leftJoin(DB::Raw("(SELECT WAV.REGISTRO, WAV.ENCARGADO, WAV.NUM_DOC, WAV.ID_CAMP_EST,
                                WAV.TOOLTIP, WU.NOMBRE, WU.ID_ZONA, WU.ID_CENTRO, WU.BANCA FROM WEBBE_ACCION_VISUALIZACION  WAV
              LEFT JOIN WEBVPC_USUARIO_ANT WU ON WU.REGISTRO=WAV.REGISTRO
              WHERE FLG_ULTIMO=1 AND TIPO_EJ='"."EC"."') WAV_EC"),function ($join){
                    $join->on('WAV_EC.NUM_DOC','=','WLCE.NUM_DOC'); 
                    $join->on('WAV_EC.ID_CAMP_EST','=','WLCE.ID_CAMP_EST'); 
                    $join->on('WAV_EC.TOOLTIP','=','WLCE.TOOLTIP');
                })            
            ->leftJoin('WEBVPC_LINKEDIN as WLI', function($join){
                $join->on('WAV_EN.REGISTRO', '=', 'WLI.REGISTRO_EN');
            });

        if (isset($filtros['ejecutivo'])){
            $sql = $sql->where('WAV_EN.REGISTRO','=',$filtros['ejecutivo']);
        }

        if (isset($filtros['ejecutivoProducto'])){
            $sql = $sql->where('WAV_EC.REGISTRO','=',$filtros['ejecutivoProducto']);
        }

        if (isset($filtros['jefatura'])){
            $sql = $sql->where('WAV_EN.ID_CENTRO','=',$filtros['jefatura']);
        }

        if (isset($filtros['zonal'])){
            $sql = $sql->where('WAV_EN.ID_ZONA','=',$filtros['zonal']);
        }

        //Trucazo temporal para arreglar el filtro por zonal
        if (isset($filtros['zonal'])){
            $sql = $sql->where('WAV_EN.ID_ZONA','=',$filtros['zonal']);
        }

        if (isset($filtros['banca'])){
            $sql = $sql->where('WAV_EN.BANCA','=',$filtros['banca']);
        }

        if(isset($orden['sort']) ) 
            $sql=$sql->orderBy( DB::raw($this->getOrderColumn($orden['sort'])) , $orden['order']);            
        else 
            $sql=$sql->orderBy('WLCE.PRIORIDAD','DESC')->orderBy('WL.NOMBRE', 'ASC');
       
        return $sql;             
    }

    function getClienteEjecutivo($periodo,$ejecutivo,$rol, $filtros = null,$orden = null,$flgActividades=false){
        
        if ($flgActividades){
            return $this->getClienteActividades($periodo,$filtros,$orden)
                    ->where('WU.REGISTRO','=',$ejecutivo);
        }

        $sql = $this->getAccionesVisualizacion($periodo,$filtros,$orden);        
        
        if ($ejecutivo){
            if($rol==31) 
                $sql = $sql->where('WAV_EC.REGISTRO','=',$ejecutivo);
            else
                $sql = $sql->where('WAV_EN.REGISTRO','=',$ejecutivo);
        }
        
        return $sql;
    }

    function getClienteJefatura($periodo,$jefatura, $filtros = null,$orden = null,$flgActividades=false){

        if ($flgActividades){
            return $this->getClienteActividades($periodo,$filtros,$orden)
                    ->where('WU.ID_CENTRO','=',$jefatura);
        }
        
        $sql = $this->getAccionesVisualizacion($periodo,$filtros,$orden)
            ->where('WAV_EN.ID_CENTRO','=',$jefatura);
      
        return $sql;
    }


    function getClienteZonal($periodo,$zonal, $filtros = null,$orden = null,$flgActividades=false){

        if ($flgActividades){
            $sql=$this->getClienteActividades($periodo,$filtros,$orden);
            if ($zonal=='BC')
                    $sql=$sql->where('WAV_EN.ID_ZONA','like','BC%');
            else
                $sql=$sql->where('WU.ID_ZONA','=',$zonal);

            return $sql;
        }

        
        if($zonal!='BC') $filtros['zonal']=$zonal; //Trucazo para el filtro por zonal
        $sql = $this->getAccionesVisualizacion($periodo,$filtros,$orden);
        if ($zonal=='BC'){
            $sql=$sql
                    ->where('WAV_EN.ID_ZONA','like','BC%'); 
            $sql=$sql
                    ->where('WAV_EN.ID_ZONA','like','BC%');            
        }
        else{
            $sql=$sql->where('WAV_EN.ID_ZONA','=',$zonal);
        }

       

        return $sql;
    }

    function getClienteBanca($periodo,$banca, $filtros = null,$orden = null,$flgActividades=false){

        if ($flgActividades){
            return $this->getClienteActividades($periodo,$filtros,$orden)
                    ->where('WU.BANCA','=',$banca);
        }
        $sql = $this->getAccionesVisualizacion($periodo,$filtros,$orden)
            ->where('WAV_EN.BANCA','=',$banca);

        /*if (isset($filtros['ejecutivo'])){
            $sql = $sql->where('WAV_EN.REGISTRO','=',$filtros['ejecutivo']);
        }

        if (isset($filtros['ejecutivoProducto'])){
            $sql = $sql->where('WAV_EC.REGISTRO','=',$filtros['ejecutivoProducto']);
        }

        if (isset($filtros['jefatura'])){
            $sql = $sql->where('WAV_EN.ID_CENTRO','=',$filtros['jefatura']);
        }

        if (isset($filtros['zonal'])){
            $sql = $sql->where('WAV_EN.ID_ZONA','=',$filtros['zonal']);
        }*/

        return $sql;
    }


    function getClienteGerencia($periodo, $filtros = null,$orden = null,$flgActividades=false){

        if ($flgActividades){
            return $this->getClienteActividades($periodo,$filtros,$orden);
        }

        $sql = $this->getAccionesVisualizacion($periodo,$filtros,$orden);

        /*if (isset($filtros['ejecutivo'])){
            $sql = $sql->where('WAV_EN.REGISTRO','=',$filtros['ejecutivo']);
        }

        if (isset($filtros['ejecutivoProducto'])){
            $sql = $sql->where('WAV_EC.REGISTRO','=',$filtros['ejecutivoProducto']);
        }

        if (isset($filtros['jefatura'])){
            $sql = $sql->where('WAV_EN.ID_CENTRO','=',$filtros['jefatura']);
        }

        if (isset($filtros['zonal'])){
            $sql = $sql->where('WAV_EN.ID_ZONA','=',$filtros['zonal']);
        }

        if (isset($filtros['banca'])){
            $sql = $sql->where('WAV_EN.BANCA','=',$filtros['banca']);
        }*/

        return $sql;

    }


    function getAtributoByEjecutivo($periodo,$ejecutivo,$clave,$atributo){
        $sql = $this->getLeadEjecutivo($periodo,$ejecutivo);
        $sql->select = null;

        return $sql->addSelect($clave . ' as VALOR',$atributo. ' as NOMBRE')
            ->orderby($atributo)
            ->get();

    }

    function getLeadContactos($periodo,$lead,$ejecutivo){

        $sql = $this->get($periodo,$lead)
                   ->join('WEBBE_LEAD_SECTORISTA as WLS', function($join){
                            $join->on('WL.PERIODO', '=', 'WLS.PERIODO');
                            $join->on('WL.NUM_DOC', '=', 'WLS.NUM_DOC');
                    });

        if ($ejecutivo){
            $sql = $sql->where('WLS.REGISTRO_EN','=',$ejecutivo);            
        }
        
        return $sql;
    }

    function getAutocompleteLead($periodo,$lead){
        
        $sql = $sql = DB::table('WEBBE_LEAD as WL')
                    ->select('WL.NUM_DOC','WL.NOMBRE')
                    ->join('WEBBE_LEAD_SECTORISTA as WLS', function($join){
                            $join->on('WL.PERIODO', '=', 'WLS.PERIODO');
                            $join->on('WL.NUM_DOC', '=', 'WLS.NUM_DOC');
                    })
                    ->join('WEBVPC_USUARIO_ANT as WU','WU.REGISTRO','=','WLS.REGISTRO_EN')
                    ->where('WL.PERIODO',$periodo)
                    ->where('WL.NOMBRE','like','%'.$lead.'%');
        return $sql;
    }

    function getAutocompleteEjecutivo($periodo,$lead,$ejecutivo){
        $sql = $this->getAutocompleteLead($periodo, $lead)
            ->where('WU.REGISTRO','=',$ejecutivo);
        return $sql;
    }

    function getAutocompleteJefatura($periodo,$lead,$jefatura){
        $sql = $this->getAutocompleteLead($periodo, $lead)
            ->where('WU.ID_CENTRO','=',$jefatura);
        return $sql;
    }

    function getAutocompleteZonal($periodo,$lead,$zonal){
        $sql = $this->getAutocompleteLead($periodo, $lead)
            ->where('WU.ID_ZONA','=',$zonal);
        return $sql;
    }

    function getAutocompleteBanca($periodo,$lead,$banca){
        $sql = $this->getAutocompleteLead($periodo, $lead)
            ->where('WU.BANCA','=',$banca);
        return $sql;
    }

    function getAutocompleteGerencia($periodo,$lead, $regEjecutivo=null){
        $sql = $this->getAutocompleteLead($periodo, $lead,$regEjecutivo);
        return $sql;

    }
    
    function actualizar($data){
        DB::beginTransaction();
        $status = true;
        try {
            DB::table('WEBBE_LEAD')
            ->where('NUM_DOC', $data['NUM_DOC'])
            ->where('PERIODO', $data['PERIODO'])
            ->update(array_except($data, ['NUM_DOC','PERIODO']));
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }

    function insertAccionComercial($leadEtapa,$leadCampanha,$actividad,$actividad2,$encargado,$visualizacion){
        DB::beginTransaction();
        $status = true;
        
        try {

            DB::table('WEBBE_ACCION_VISUALIZACION')->insert($encargado);
            if ($visualizacion!=NULL) DB::table('WEBBE_ACCION_VISUALIZACION')->insert($visualizacion);
            DB::table('WEBBE_LEAD_ETAPA')->insert($leadEtapa);
            DB::table('WEBVPC_LEAD_CAMP_EST')->insert($leadCampanha);
            DB::table('WEBBE_ACTIVIDAD')->insert($actividad);
            DB::table('WEBBE_ACTIVIDAD')->insert($actividad2);
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }


    function eliminarAccion($leadEliminado,$actividad){
        DB::beginTransaction();
        $status = true;        
        try {      

             DB::table('WEBVPC_LEAD_CAMP_EST')                    
                ->where('NUM_DOC',$leadEliminado['NUM_DOC'])
                ->where('ID_CAMP_EST',$leadEliminado['ID_CAMP_EST']) 
                ->where('TOOLTIP',$leadEliminado['TOOLTIP'])
                ->update(['FLG_HABILITADO' => "0"]);

            DB::table('WEBBE_LEAD_ETAPA')
                ->where('NUM_DOC',$leadEliminado['NUM_DOC'])
                ->where('ID_CAMP_EST',$leadEliminado['ID_CAMP_EST']) 
                ->where('TOOLTIP',$leadEliminado['TOOLTIP'])
                ->where('FLG_ULTIMO',1)
                ->update(['FLG_ULTIMO' => "0"]);

            DB::table('WEBBE_ACCION_VISUALIZACION')
                ->where('NUM_DOC',$leadEliminado['NUM_DOC'])
                ->where('ID_CAMP_EST',$leadEliminado['ID_CAMP_EST']) 
                ->where('TOOLTIP',$leadEliminado['TOOLTIP'])
                ->where('FLG_ULTIMO',1)
                ->update(['FLG_ULTIMO' => "0"]);

            DB::table('WEBBE_LEAD_ELIMINACION')->insert($leadEliminado);
            DB::table('WEBBE_ACTIVIDAD')->insert($actividad);
            DB::commit();

        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        
        return $status;
    }

    function updateMes($data){        
        DB::beginTransaction();
        $status = true;        
        try {      
             DB::table('WEBVPC_LEAD_CAMP_EST')                    
                ->where('NUM_DOC',$data['cliente'])
                ->where('ID_CAMP_EST',$data['accion']) 
                ->where('TOOLTIP',$data['tooltip'])
                ->update(['MES_ACTIVACION' => $data['mesActivCambio']]);
            
              DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }        
        return $status;
    }

    function updateKPI($data){   
        DB::beginTransaction();
        $status = true;        
        try {      
             DB::table('WEBVPC_LEAD_CAMP_EST')                    
                ->where('NUM_DOC',$data['cliente'])
                ->where('ID_CAMP_EST',$data['accion']) 
                ->where('TOOLTIP',$data['tooltip'])
                ->update(['KPI' => $data['kpi']*1000]);
            
              DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }        
        return $status;
    }

    static function getBancos(){
        $sql=DB::table('WEBVPC_BANCOS')
        ->select('CODIGO','NOMBRE');
        return $sql;
    }

    function updateEstrella($data){
        $estrella=DB::table('WEBVPC_LEAD_CAMP_EST') 
                ->select(DB::Raw('CONVERT(INT,PRIORIDAD) ESTRELLA'))                   
                ->where('NUM_DOC',$data['cliente'])
                ->where('ID_CAMP_EST',$data['accion']) 
                ->where('TOOLTIP',$data['tooltip'])->first()->ESTRELLA;

        $estrella=($estrella==1?0:1);

        DB::beginTransaction();
        $status = true;
        try {           


            DB::table('WEBVPC_LEAD_CAMP_EST')                    
                ->where('NUM_DOC',$data['cliente'])
                ->where('ID_CAMP_EST',$data['accion']) 
                ->where('TOOLTIP',$data['tooltip'])
                ->update(['PRIORIDAD' =>$estrella]);
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }

}
