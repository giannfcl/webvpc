<?php
namespace App\Model\BE;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class Producto extends Model

{
	protected $table = 'WEBBE_NOTAS_LEAD';
    
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey =['NOTA_ID'];
					        

        /**
     * The name of the "created at" column.
     *
     * @var string
     */
     public $timestamps = false;
    
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'NUM_DOC',
        'NOTA',
        'FECHA_REGISTRO',
        'PERIODO',
        'REGISTRO_EN'
        ];
                                  

    function listar(){
        $sql = DB::table('WEBVPC_PRODUCTO as WP')
            ->select('WP.ID_PRODUCTO','WP.PRODUCTO')
            ->where('WP.TIPO_PRODUCTO','<>','DEPOSITOS')
            ->orderBy('WP.PRODUCTO','asc');
        return $sql;
    }                                                                                                                                                     

}