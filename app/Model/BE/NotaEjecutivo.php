<?php
namespace App\Model\BE;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class NotaEjecutivo extends Model

{
	protected $table = 'WEBBE_NOTAS_LEAD';
    
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey =['NOTA_ID'];
					        

        /**
     * The name of the "created at" column.
     *
     * @var string
     */
     public $timestamps = false;
    
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'NUM_DOC',
        'NOTA',
        'FECHA_REGISTRO',
        'PERIODO',
        'REGISTRO_EN'
        ];
                                  
            
    static function listar($ejecutivo,$numdoc,$accion = NULL,$tooltip=NULL){
        $sql = DB::table('WEBBE_NOTAS_LEAD as WNL')
            ->select('WNL.*','WU.NOMBRE AS NOMBRE_EJECUTIVO')
            ->leftJoin('WEBVPC_USUARIO AS WU',function ($join){
                    $join->on('WU.REGISTRO','=','WNL.REGISTRO_EN'); 
            })
            ->where('NUM_DOC',$numdoc)
            ->orderBy('WNL.FECHA_REGISTRO','desc');

        if($accion!=NULL)
            $sql=$sql->where('ID_CAMP_EST','=',$accion);    
        if($tooltip!=NULL)
            $sql=$sql->where('TOOLTIP','=',$tooltip);  
        
        //dd($sql->get());
        return $sql;
    }

    static function listarByOperacion($operacion){
        $sql = DB::table('WEBBE_NOTAS_LEAD as WNL')
            ->select()
            ->where('ID_OPERACION',$operacion)
            ->orderBy('WNL.FECHA_REGISTRO','desc');
        return $sql;
    }

    function guardar($data){
        DB::beginTransaction();
        $status = true;             
        try {            
            $id = DB::table('WEBBE_NOTAS_LEAD')->insertGetId($data);
            DB::commit();
            return $id;
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }                          

    function eliminar($id){
        
        DB::beginTransaction();
        $status = true;             
        try {
            $sql=DB::table('WEBBE_NOTAS_LEAD')
            ->where('NOTA_ID','=',$id)
            ->delete();
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }                                                                                                                                                                

}