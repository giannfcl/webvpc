<?php

namespace App\Model;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date as Carbon;
use \App\Entity\Canal as Canal;

class LeadsEjecutivo extends Model {

    protected $table = 'WEBVPC_LEADS_EJECUTIVO';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = [
        'PERIODO',
        'NUM_DOC',
        'REGISTRO_EN',
    ];

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'FECHA_ASIGNACION';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'FLG_ULTIMO',
        'MARCA_ASISTENTE_COMERCIAL',
    ];


    static function actualizarFlags($numdoc){
        DB::beginTransaction();
        $status = true;
            try{
                 DB::table('WEBVPC_LEADS_EJECUTIVO')
                    ->where('PERIODO',201705)
                    ->whereiN('NUM_DOC',$numdoc)
                    ->update(['FLG_ULTIMO'=>0]);
                 DB::commit();   
            }catch(Exception $e){
                Log::error('BASE_DE_DATOS|' . $e->getMessage());
                $status = false;
                DB::rollback();
            }
         return $status;  
    } 

    static function setLeadUltimoSoporte($registroLeadejecutivo){
        DB::beginTransaction();
        $status = true;
            try{
             DB::table('WEBVPC_LEADS_EJECUTIVO')-> insert($registroLeadejecutivo);
                 DB::commit();   
            }catch(Exception $e){
                Log::error('BASE_DE_DATOS|' . $e->getMessage());
                $status = false;
                DB::rollback();
            }
          return $status;   
    }
    
    function updateMarca($data, $periodo){
        DB::beginTransaction();
        $status = true;
        try {
            DB::table('WEBVPC_LEADS_EJECUTIVO')
            //->where('PERIODO', $data['PERIODO'])
            ->where('NUM_DOC', $data['NUM_DOC'])
            ->where('REGISTRO_EN', $data['REGISTRO_EN'])
            ->where('FLG_ULTIMO', '1')
            ->update($data);
            DB::table('WEBVPC_LEADS_ETIQUETA_HISTORICO')
            ->insert(
                ['PERIODO' => $periodo, 'NUM_DOC' => $data['NUM_DOC'], 
                'REGISTRO_EN' => $data['REGISTRO_EN'], 'FECHA_ETIQUETA' => Carbon::now()->toDateTimeString(),
                'ETIQUETA_EJECUTIVO' => $data['ETIQUETA_EJECUTIVO']]
            );
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }

    function updateEtiqueta($data){
        DB::beginTransaction();
        $status = true;
        try {
            DB::table('WEBVPC_LEADS_EJECUTIVO')
            //->where('PERIODO', $data['PERIODO'])
            ->where('NUM_DOC', $data['NUM_DOC'])
            ->where('REGISTRO_EN', $data['REGISTRO_EN'])
            ->where('FLG_ULTIMO', '1')
            ->update($data);
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }

    function asignarLeads($leads,$data,$canalHistorico,$asignacionHistorico){
        DB::beginTransaction();
        $status = true;
        try {
            DB::table('WEBVPC_LEADS_EJECUTIVO')->insert($data);
            DB::table('WEBVPC_HIST_MOVIMIENTO_CANAL')->insert($canalHistorico);
            DB::table('WEBVPC_HIST_ASIGNACION')->insert($asignacionHistorico);

            foreach ($data as $item) {
                DB::table('WEBVPC_LEAD_CAMP_EST')
                    ->where('NUM_DOC',$item['NUM_DOC'])
                    ->where('PERIODO',$item['PERIODO'])
                    ->update(['CANAL_ACTUAL' => Canal::EJECUTIVO_NEGOCIO,
                        'FECHA_ACTUALIZACION' => Carbon::now()->toDateTimeString()
                        ]);
            }
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }

    function reasignarLeads($leadsEjecutivo,$reasignacionHistorico,$citas,$estadosCita){
        DB::beginTransaction();
        $status = true;
        try {
            // Quitamos el flag ultimo a las asignaciones anteriores
            foreach ($leadsEjecutivo as $leadEjecutivo) {
                DB::table('WEBVPC_LEADS_EJECUTIVO')
                ->where('PERIODO',$leadEjecutivo['PERIODO'])
                ->where('NUM_DOC',$leadEjecutivo['NUM_DOC'])
                //->where('REGISTRO_EN',$leadEjecutivo['REGISTRO_EN'])
                ->update(['FLG_ULTIMO' => 0]);
            }
            // Insertamos las nuevas asignaciones
            DB::table('WEBVPC_LEADS_EJECUTIVO')->insert($leadsEjecutivo);
            // Guardamos el historico de reasignaciones
            DB::table('WEBVPC_HIST_ASIGNACION')->insert($reasignacionHistorico);
            //Si hay citas que corregir por conflicto
            if (count($citas) > 0){
                // Actualizamos las citas
                foreach ($citas as $cita) {
                    DB::table('WEBVPC_CITAS')
                    ->where('ID_CITA',$cita['ID_CITA'])
                    ->update(array_except($cita, ['ID_CITA']));
                }

                // Insertamos los datos de las citas
                DB::table('WEBVPC_CITA_ESTADO_HISTORICO')->insert($estadosCita);
            }
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }
}