<?php
namespace App\Model;
use DB;
use Log;
use Illuminate\Database\Eloquent\Model;
use App\Model\Infinity\Cliente as Cliente;
use App\Entity\Infinity\Politica as Politica;
use App\Entity\Infinity\PoliticaProceso as PoliticaProceso;
use App\Model\Salida as Salida;
use App\Entity\SetCorreos as SetCorreos;
use Jenssegers\Date\Date as Carbon;

class TestPolitica extends Model
{
    static function getPoliticasInfinity($periodo,$codunico=null,$infinity = true,$hayseleccionados = false)
    {
        $sql = DB::Table('WEBBE_INFINITY_CLIENTE AS WIC')
                ->select('WIC.COD_UNICO','WICP.ID_POLITICA_CLIENTE',DB::raw('CONVERT(varchar,WICP.FECHA_CAIDA,120) FECHA_CAIDA'),'WIP.POLITICA_ID','WIP.ORIGEN','WICP.NIVEL','WICP.DIAS_GESTION')
                
                ->leftjoin('WEBBE_INFINITY_CLIENTE_POLITICA AS WICP', function ($join){
                    $join->on('WIC.COD_UNICO', '=', 'WICP.COD_UNICO');
                    $join->on('WICP.FLG_VIGENTE','=',DB::raw("1"));
                })

                ->leftjoin('WEBBE_INFINITY_POLITICA AS WIP', function ($join){
                    $join->on('WIP.POLITICA_ID', '=', 'WICP.POLITICA_ID');
                    $join->on('WIP.SITUACION_POLITICA','=',DB::raw("'VIGENTE'"));
                })
                ->where('PERIODO','=',$periodo);
                if ($infinity){
                    $sql = $sql->where('FLG_INFINITY','>=','1');
                }
                if (!empty($codunico)) {
                    $sql= $sql->where('WIC.COD_UNICO','=',$codunico);
                }
                if ($hayseleccionados) {
                    $sql= $sql->where('WIC.FLG_POLITICAPROCESO','=',1);
                }

        return (isset($sql) ? $sql : null);
    }

    static function getClientePolitica($id){
        $sql = DB::Table('WEBBE_INFINITY_CLIENTE_POLITICA')
            ->select('*')
            ->where('ID_POLITICA_CLIENTE',$id)->get()->first();
        return !empty($sql) ? $sql : null;
    }

    public function getPolitica($id){
        $sql = DB::Table('WEBBE_INFINITY_POLITICA')
            ->select('*')
            ->where('POLITICA_ID',$id);
        return $sql;
    }

    static function getSalidaByClientePolitica($id){
        $sql = DB::Table('WEBBE_INFINITY_SALIDAS')
            ->select('*')
            ->where('POLITICA_CLIENTE_ID',$id)->get()->first();
        return !empty($sql) ? $sql : null;
    }

    static function getUltimaByCodunico($codunico){
        $sql = DB::Table('WEBBE_INFINITY_SALIDAS as WIS')
            ->join('WEBBE_INFINITY_CLIENTE_POLITICA as WICP','WIS.POLITICA_CLIENTE_ID','=','WICP.ID_POLITICA_CLIENTE')
            ->select('WIS.*','WICP.POLITICA_ID')
            ->where('WIS.COD_UNICO',$codunico)
            ->orderBy('WIS.FECHA_SALIDA','desc')
            ->first();
        return $sql;
    }

    static function getLastGestion($codunico){
        $sql = DB::Table('WEBBE_INFINITY_GESTION as WIG')
            ->join('WEBVPC_USUARIO as WU','WIG.REGISTRO_EN','=','WU.REGISTRO')
            ->select('WIG.*','WU.NOMBRE as NOMBRE_USUARIO')
            ->where('WIG.COD_UNICO','=',$codunico)
            ->where('WIG.ESTADO_GESTION',4)
            ->orderBy('WIG.FECHA_REGISTRO','desc')
            ->first();
        return $sql;
    }

    function actualizarblindaje($value='')
    {
        DB::beginTransaction();
        $status = true;
        try {
            DB::table('WEBBE_INFINITY_CLIENTE_REVISION')
                ->where(DB::raw("CAST(DATEADD(MM, TIEMPO, FECHA_REGISTRO) AS DATE)"),'=',DB::raw("CAST(GETDATE() AS DATE)"))
                ->where('FLG_VIGENTE','=',DB::raw('1'))
                ->update(['FLG_VIGENTE' => 0]);

            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }

        return $status;
    }

    function PoliticasVencidas()
    {
        DB::beginTransaction();
        $status = true;
        try {
            
            DB::table('WEBBE_INFINITY_CLIENTE_POLITICA')
                ->where(DB::raw('FECHA_CAIDA+DIAS_GESTION'),'<',DB::raw('GETDATE()'))
                ->where('FLG_VIGENTE','=',DB::raw('1'))
                ->where('DIAS_GESTION','>','0')
                ->where('ESTADO_FINAL_GESTION','<>',PoliticaProceso::VENCIDA)
                ->update(['ESTADO_FINAL_GESTION' => PoliticaProceso::VENCIDA]);

            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }

        return $status;
    }

    function actualizarNivelPoliticas($periodo)
    {
        try {
            DB::beginTransaction();
            $status = true;
                    $idpoliticaclientes=DB::table(DB::Raw("
                                    (
                                        SELECT A.*,B.NIVEL_1,B.NIVEL_2,B.DIAS_SEGUNDA_GESTION
                                        FROM (
                                            SELECT ID_POLITICA_CLIENTE,POLITICA_ID,COD_UNICO,REGISTRO_EN,FECHA_CAIDA,ESTADO_FINAL_GESTION,FLG_VIGENTE, DIAS_GESTION
                                            FROM WEBBE_INFINITY_CLIENTE_POLITICA 
                                            WHERE POLITICA_ID IN ('23') AND FLG_VIGENTE='1' AND (CASE WHEN DATEADD(D,DIAS_GESTION,FECHA_CAIDA)<GETDATE() THEN 1 ELSE 0 END)=1
                                        ) A
                                        JOIN (SELECT POLITICA_ID,NIVEL_1,NIVEL_2,DIAS_SEGUNDA_GESTION FROM WEBBE_INFINITY_POLITICA) B
                                        ON A.POLITICA_ID=B.POLITICA_ID
                                    ) A"))->get();
                // dd($idpoliticaclientes);
                if (!empty($idpoliticaclientes)) {
                    foreach ($idpoliticaclientes as $key => $value) {
                               
                        $date = new Carbon($value->FECHA_CAIDA);
                        $hoy = Carbon::now();
                        // dd($value->FECHA_CAIDA,$value->DIAS_SEGUNDA_GESTION,$date->addDays($value->DIAS_SEGUNDA_GESTION),$hoy->greaterThanOrEqualTo($date->addDays($value->DIAS_SEGUNDA_GESTION)));
                        if ($value->FLG_VIGENTE==1 &&  $value->DIAS_GESTION=='30' && $hoy->greaterThan($date->addDays($value->DIAS_GESTION))) {
                            DB::table('WEBBE_INFINITY_SALIDAS')->insert([
                                'POLITICA_CLIENTE_ID'=>$value->ID_POLITICA_CLIENTE,
                                'COD_UNICO'=>$value->COD_UNICO,
                                'REGISTRO_EN'=>$value->REGISTRO_EN,
                                'FECHA_SALIDA'=>$hoy,
                                'TIPO_SALIDA'=>NULL,//COMITE
                                'LINEAS'=>Cliente::getLineas($value->COD_UNICO)
                            ]);
                            $this->queue_act_susp($value->COD_UNICO,$periodo,"Suspensión","Suspensión Automática",$value->ID_POLITICA_CLIENTE,Cliente::getLineas($value->COD_UNICO));
                        }

                        if ($value->ESTADO_FINAL_GESTION<>'VENCIDA') {
                            DB::table('WEBBE_INFINITY_CLIENTE_POLITICA')
                                ->where('ID_POLITICA_CLIENTE',$value->ID_POLITICA_CLIENTE)
                                ->update([
                                    'NIVEL' => $value->NIVEL_2,
                                    'DIAS_GESTION' => $value->DIAS_SEGUNDA_GESTION,
                                    'ESTADO_FINAL_GESTION' => 'VENCIDA'
                                ]);
                        }

                        if ($value->ESTADO_FINAL_GESTION=='VENCIDA' &&  $value->DIAS_GESTION =='75' && $value->FLG_VIGENTE==1 && $hoy->greaterThan($date->addDays($value->DIAS_SEGUNDA_GESTION))) {
                            DB::table('WEBBE_INFINITY_CLIENTE_POLITICA')
                                ->where('ID_POLITICA_CLIENTE',$value->ID_POLITICA_CLIENTE)
                                ->update([
                                    'FLG_VIGENTE' => '0',
                                ]);
                        }
                    }
                }
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }

    static function queue_act_susp($codunico,$periodo,$mensaje,$motivo,$idpoliticacliente=null,$lineas=null,$gestion=null)
    {
        // dd($gestion);
        // dd($codunico,$periodo,$mensaje,$motivo,$idpoliticacliente,$lineas);
        $CORREO_EJECUTIVO=Cliente::getDatosCliente($codunico,$periodo)->CORREO;
        $CORREO_JEFE=Cliente::getJefes(Cliente::getDatosCliente($codunico,$periodo)->REGISTRO)->CORREO;

        $participantescorreos= new SetCorreos();
        $destino = $participantescorreos->getusuariodestinoSalidaLA();
        $copiados = $participantescorreos->getusuariocopiadosSalidaLA($CORREO_EJECUTIVO,$CORREO_JEFE);

        if ($mensaje=='Suspensión') {
                if ($motivo=='Suspensión') {
                    $datos =[
                              'CU'=>$gestion['COD_UNICO'],
                              'CLIENTE'=>!empty(Cliente::getDatosCliente($gestion['COD_UNICO'],$periodo)->NOMBRE) ? Cliente::getDatosCliente($gestion['COD_UNICO'],$periodo)->NOMBRE : null,
                              'EJECUTIVO'=>!empty(Cliente::getDatosCliente($gestion['COD_UNICO'],$periodo)->NOMBRE_EJECUTIVO) ? Cliente::getDatosCliente($gestion['COD_UNICO'],$periodo)->NOMBRE_EJECUTIVO : null,
                              'JEFE'=>!empty(Cliente::getJefes(Cliente::getDatosCliente($gestion['COD_UNICO'],$periodo)->REGISTRO)->NOMBRE) ? Cliente::getJefes(Cliente::getDatosCliente($gestion['COD_UNICO'],$periodo)->REGISTRO)->NOMBRE : null,
                              'MENSAJE'=>$mensaje,
                              'MOTIVO'=>$motivo,
                              'LINEAS'=>Cliente::getLineas($gestion['COD_UNICO']),
                              'GESTION'=>[
                                'REGISTRO_EN'=>Cliente::getCorreo($gestion['REGISTRO_EN'])->NOMBRE,
                                'FECHA_GESTION'=>Carbon::now(),
                                'POLITICA'=>Cliente::getDatosPolitica($gestion['POLITICA_ID'])->NOMBRE,
                                'DECISION'=>'Desaprobado',
                                'COMENTARIO'=>$gestion['COMENTARIO']
                              ]
                            ];
                }elseif ($motivo=='Suspensión Automática') {
                    if ($lineas) {
                        $datos =[
                          'CU'=>$codunico,
                          'CLIENTE'=>!empty(Cliente::getDatosCliente($codunico,$periodo)->NOMBRE) ? Cliente::getDatosCliente($codunico,$periodo)->NOMBRE : null,
                          'EJECUTIVO'=>!empty(Cliente::getDatosCliente($codunico,$periodo)->NOMBRE_EJECUTIVO) ? Cliente::getDatosCliente($codunico,$periodo)->NOMBRE_EJECUTIVO : null,
                          'JEFE'=>!empty(Cliente::getJefes(Cliente::getDatosCliente($codunico,$periodo)->REGISTRO)->NOMBRE) ? Cliente::getJefes(Cliente::getDatosCliente($codunico,$periodo)->REGISTRO)->NOMBRE : null,
                          'MENSAJE'=>$mensaje,
                          'MOTIVO'=>$motivo.' por la politica '.Cliente::getDatosPolitica(TestPolitica::getClientePolitica($idpoliticacliente)->POLITICA_ID)->NOMBRE,
                          'LINEAS'=>$lineas
                        ];
                    }else{

                        $datos =[
                                  'CU'=>$codunico,
                                  'CLIENTE'=>!empty(Cliente::getDatosCliente($codunico,$periodo)->NOMBRE) ? Cliente::getDatosCliente($codunico,$periodo)->NOMBRE : null,
                                  'EJECUTIVO'=>!empty(Cliente::getDatosCliente($codunico,$periodo)->NOMBRE_EJECUTIVO) ? Cliente::getDatosCliente($codunico,$periodo)->NOMBRE_EJECUTIVO : null,
                                  'JEFE'=>!empty(Cliente::getJefes(Cliente::getDatosCliente($codunico,$periodo)->REGISTRO)->NOMBRE) ? Cliente::getJefes(Cliente::getDatosCliente($codunico,$periodo)->REGISTRO)->NOMBRE : null,
                                  'MENSAJE'=>$mensaje,
                                  'MOTIVO'=>$motivo.' por la politica '.Cliente::getDatosPolitica(TestPolitica::getClientePolitica($idpoliticacliente)->POLITICA_ID)->NOMBRE,
                                  'LINEAS'=>Cliente::getLineas($codunico)
                                ];
                    }
                }
        }
        if ($mensaje=='Reactivación') {
            
            $datos =[
                      'CU'=>$codunico,
                      'CLIENTE'=>!empty(Cliente::getDatosCliente($codunico,$periodo)->NOMBRE) ? Cliente::getDatosCliente($codunico,$periodo)->NOMBRE : null,
                      'EJECUTIVO'=>!empty(Cliente::getDatosCliente($codunico,$periodo)->NOMBRE_EJECUTIVO) ? Cliente::getDatosCliente($codunico,$periodo)->NOMBRE_EJECUTIVO : null,
                      'JEFE'=>!empty(Cliente::getJefes(Cliente::getDatosCliente($codunico,$periodo)->REGISTRO)->NOMBRE) ? Cliente::getJefes(Cliente::getDatosCliente($codunico,$periodo)->REGISTRO)->NOMBRE : null,
                      'MENSAJE'=>$mensaje,
                      'MOTIVO'=>$motivo,
                      'LINEAS'=>TestPolitica::getUltimaByCodunico($codunico)->LINEAS,
                      'GESTION'=>[
                        'REGISTRO_EN'=>TestPolitica::getLastGestion($codunico)->NOMBRE_USUARIO,
                        'FECHA_GESTION'=>Carbon::now(),
                        'POLITICA'=>Cliente::getDatosPolitica(TestPolitica::getLastGestion($codunico)->POLITICA_ID)->NOMBRE,
                        'DECISION'=>'Aprobado',
                        'COMENTARIO'=>TestPolitica::getLastGestion($codunico)->COMENTARIO
                      ]
                    ];
        }
        // dd($datos);

        $mail = new \App\Mail\Infinity\Correo(1,$datos,null,$destino,$copiados);
        dispatch(new \App\Jobs\MailQueue($mail));
    }

    function actualizar($politicas_agregar=[],$politicas_archivar=[],$clientesActualizar=[],$politicas_actualizar = [],$reingreso = null){
        // dd($politicas_agregar,$politicas_archivar,$clientesActualizar,$politicas_actualizar,$reingreso);
        try {
                DB::beginTransaction();
                $status = true;
                //Insertamos las politicas nuevas
                // if (!empty($politicas_agregar)) {
                if (count($politicas_agregar)>0) {
                    if($politicas_agregar){
                        foreach (array_chunk($politicas_agregar, 200) as $chunk)
                        {
                           DB::table('WEBBE_INFINITY_CLIENTE_POLITICA')->insert($chunk);
                        }  
                        //DB::table('WEBBE_INFINITY_CLIENTE_POLITICA')->insert($politicas_agregar);  
                    }
                }

                //Archivamos las politicas obsoletas
                // if (!empty($politicas_archivar)) {
                if (count($politicas_archivar)>0) {
                    if($politicas_archivar){
                        DB::table('WEBBE_INFINITY_CLIENTE_POLITICA')->whereIn('ID_POLITICA_CLIENTE',$politicas_archivar)->update(['FLG_VIGENTE' => 0]);
                    }
                }

                //Actualizamos las politicas con cambio de severidad
                // if (!empty($politicas_actualizar)) {
                if (count($politicas_actualizar)>0) {
                    foreach ($politicas_actualizar as $politica) {
                        DB::table('WEBBE_INFINITY_CLIENTE_POLITICA')
                            ->where('ID_POLITICA_CLIENTE',$politica['ID_POLITICA_CLIENTE'])
                            ->update(array_except($politica,['ID_POLITICA_CLIENTE','COD_UNICO','FECHA_CAIDA']));
                    }
                }
                
                //Actuailzamos los estados del cliente
                // if (!empty($clientesActualizar)) {
                if (count($clientesActualizar)>0) {
                    // dd($clientesActualizar);
                    foreach ($clientesActualizar as $key => $cliente) {

                        DB::table('WEBBE_INFINITY_CLIENTE')
                            ->where('PERIODO',$cliente['PERIODO'])
                            ->where('COD_UNICO',$cliente['COD_UNICO'])
                            ->where('FLG_INFINITY','>=',DB::Raw('1'))
                            ->update(array_except($cliente,['PERIODO','COD_UNICO','SALIDA_POLITICA']));


                        //Si el cliente tiene un flag de salida
                        if (isset($cliente['SALIDA_POLITICA']) && $cliente['SALIDA_POLITICA'] ){
                            //buscamos la politica con severidad salida
                            $politica_caida = Db::table('WEBBE_INFINITY_CLIENTE_POLITICA')
                                ->select('ID_POLITICA_CLIENTE')
                                ->where('FLG_VIGENTE','1')
                                ->where('ESTADO_FINAL_GESTION','<>',PoliticaProceso::VENCIDA)
                                ->where('NIVEL', Politica::NIVEL_SEVERIDAD_SALIDA)
                                ->where('COD_UNICO',$cliente['COD_UNICO'])
                                ->orderBy('FECHA_CAIDA','desc')
                                ->first();


                            //la insertamos en la tabla
                            if($politica_caida){
                                DB::table('WEBBE_INFINITY_SALIDAS')->insert([
                                    'POLITICA_CLIENTE_ID' => $politica_caida->ID_POLITICA_CLIENTE,
                                    'COD_UNICO' => $cliente['COD_UNICO'],
                                    'FECHA_SALIDA' => Carbon::now(),
                                    'LINEAS'=>Cliente::getLineas($cliente['COD_UNICO'])
                                ]);
                                    //***************************************************************************************************
                                    TestPolitica::queue_act_susp($cliente['COD_UNICO'],$cliente['PERIODO'],'Suspensión','Suspensión Automática',$politica_caida->ID_POLITICA_CLIENTE);
                                    //*****************************************************************************************************
                            }
                        }

                        //Si hay reingreso
                        if($reingreso){
                            DB::table('WEBBE_INFINITY_SALIDAS')
                            ->where('POLITICA_CLIENTE_ID',$reingreso['POLITICA_CLIENTE_ID'])
                            ->where('COD_UNICO',$reingreso['COD_UNICO'])
                            ->update(['FECHA_REINGRESO' => $reingreso['FECHA_REINGRESO']]);
                                    //*****************************************************************************************************
                                    // $this->queue_act_susp($reingreso['COD_UNICO'],$cliente['PERIODO'],'Reactivación','Reactivación');
                                    //*****************************************************************************************************

                            if (!empty($cliente)) {
                                DB::table('WEBBE_INFINITY_CLIENTE')
                                ->where('PERIODO',$cliente['PERIODO'])
                                ->where('COD_UNICO',$reingreso['COD_UNICO'])
                                ->update([
                                    'FLG_INFINITY' => $cliente['FLG_INFINITY'],
                                    'FECHA_VENCIMIENTO_POLITICA' => $cliente['FECHA_VENCIMIENTO_POLITICA']
                                ]);
                                    TestPolitica::queue_act_susp($reingreso['COD_UNICO'],$cliente['PERIODO'],'Reactivación','Reactivación',$reingreso['POLITICA_CLIENTE_ID'],$reingreso['LINEAS']);
                                    // $this->queue_act_susp($cliente['COD_UNICO'],$cliente['PERIODO'],'Suspensión','Suspensión Automática',$reingreso['LINEAS']);
                            }

                        }

                    }
                }

                DB::commit();

        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
    }
}