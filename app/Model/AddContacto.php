<?php
namespace App\Model;
use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class AddContacto extends Model

{
    protected $table = 'WEBVPC_ADD_CONTACTO';
    
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey =['PERIODO',
                            'FECHA_REGISTRO',               
                            'NUM_DOC',
                            'REGISTRO_EN',  
                            ];
                            

        /**
     * The name of the "created at" column.
     *
     * @var string
     */
     public $timestamps = false;
    
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'TIPO_CONTACTO',
        'VALOR',
        ];


    /**
     * Inserta un nuevo campo de contactabilidad
     *
     * @param  array $contacto datos del contacto a registrar. Ver entidad.
     * @return boolean devuelve true si las sentencias corrieron
     */
    function nuevo($contacto){
        \Debugbar::info('Que esta pasando?');
        DB::beginTransaction();
        $status = true;
        try {
            DB::table('WEBVPC_ADD_CONTACTO')->insert($contacto);
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }

    /**
     * Devuelve todos los datos de contactabilidad agregados para un lead
     *
     * @param  string $periodo periodo de registro de los contactos
     * @param  string $lead lead referidos de los contactos
     * @return table tipo de contacto (email/telefono/direccion) y valor
     */
    public function listar($periodo,$lead){
        $sql = DB::table('WEBVPC_ADD_CONTACTO as WAC')
                ->select('WAC.TIPO_CONTACTO','WAC.VALOR')
                ->where('WAC.PERIODO','=',$periodo)
                ->where('WAC.NUM_DOC','=',$lead)
                ->orderBy('WAC.TIPO_CONTACTO', 'DESC')
                ->orderBy('WAC.FECHA_REGISTRO', 'DESC');
        return $sql;
    }
}