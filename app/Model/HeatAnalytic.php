<?php

namespace App\Model;

use Jenssegers\Date\Date as Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;

class HeatAnalytic extends Model
{
    static function insertHA($datos, $registro)
    {
        $now = Carbon::now();
        $sql = array(
            "timestamp" => $datos['timestamp'], "idhtml" => $datos['idhtml'],
            "registro" => $registro, "modulo" => $datos['modulo'], "fecha" => $now,
            "tipo" => $datos['tipo'], "tipo_seccion" => $datos['tipo_seccion']
        );
        DB::table('HEATANALYTIC')->insert($sql);
    }
}
