<?php

namespace App\Model;
use DB;
use Log;
use Jenssegers\Date\Date as Carbon;
use Illuminate\Database\Eloquent\Model;

class LeadCampanha extends Model

{
	protected $table = 'WEBVPC_LEAD_CAMP_est';
    
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey =['PERIODO','NUM_DOC']; 

        /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'FECHA_CARGA';

    const UPDATED_AT = 'FECHA_ACTUALIZACION';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'CANAL_DESPLIEGUE', 
        'CANAL_ACTUAL', 
        'PRIORIDAD', 
        ];

    function getCampanhasByLead($periodo,$lead,$canales){

        $sql = DB::table('WEBVPC_LEAD_CAMP_EST AS WLC')
                    ->select('WC.ID_CAMP_EST','WC.NOMBRE','WC.ABREVIATURA','WCI.PERIODO')
                    ->join('WEBVPC_CAMP_EST_INSTANCIA AS WCI', function($join){
                        $join->on('WLC.PERIODO', '=', 'WCI.PERIODO');
                        $join->on('WLC.ID_CAMP_EST', '=', 'WCI.ID_CAMP_EST');
                        $join->on('WCI.HABILITADO', '=', DB::raw('1'));
                    })
                    ->join('WEBVPC_CAMP_EST AS WC', function($join){
                        $join->on('WLC.ID_CAMP_EST', '=', 'WC.ID_CAMP_EST');
                    })
                    //->where('WLC.PERIODO',$periodo)
                    ->whereIn('WLC.CANAL_ACTUAL',$canales)
                    ->where('WC.BANCA','BPE');

        if ($lead){
            $sql = $sql->where('WLC.NUM_DOC',$lead);
        }
        return $sql;
    }

    
    function getCampanhasByLeadFull($periodo,$lead,$canales){
        $campos = [
            'ATRIBUTO' => 'WLA.ATRIBUTO',
            'TIPO' => 'WLA.TIPO',
            'VALOR' => "ISNULL(CAST(WLA.VALOR as VARCHAR(100)),'')",
            'CONDICIONAL' => "ISNULL(CAST(WLA.CONDICIONAL as VARCHAR(3)),'')",
            'VISIBILIDAD' => "ISNULL(CAST(WLA.VISIBILIDAD as VARCHAR(1)),'')",
        ];

        //Paso 2. Agregamos cada campo a la consulta de agrupamiento
        $scripts = [];
        $where = '';
        foreach($campos as $key => $campo){

            //consulta agrupada - es necesario cruzar leadCampanha - CampanhaInstancia - Campanha - Gestion
            $join = DB::table('WEBVPC_LEAD_CAMP_EST_ATRIBUTO as WLA')
            ->select(DB::raw("'|' + ".$campo))
            ->whereRaw('WLA.ID_CAMP_EST = WCI.ID_CAMP_EST')
            ->whereRaw('WLA.PERIODO = WCI.PERIODO')
            ->whereRaw('WLA.NUM_DOC = WLC.NUM_DOC');
                
            $script = $join->toSql();

            // $scripts[] = "STUFF((". $script ." FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1, '') ".$key;
            // $scripts[] = "STUFF((". $script ." order by WLA.%%physloc%% FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1, '') ".$key;
            $scripts[] = "STUFF((". $script ." order by WLA.ATRIBUTO FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1, '') ".$key;
            
        }
        
        //Paso 3 agrego los campos agrupados (STUFF) a la consulta y la última gestión asociada
        $sql = $this->getCampanhasByLead($periodo,$lead,$canales)
                ->addSelect(
                            DB::raw($scripts[0]),
                            DB::raw($scripts[1]),
                            DB::raw($scripts[2]),
                            DB::raw($scripts[3]),
                            DB::raw($scripts[4]),
                            'WGL.COMENTARIO as GESTION_COMENTARIO',
                            'WGL.VISITADO as GESTION_VISITADO',
                            'WGL.SOLICITUD_MONTO',
                            'WGL.SOLICITUD_PLAZO',
                            'WGL.SOLICITUD_GRACIA',
                            'WGL.SOLICITUD_TASA',
                            'WGL.ID_RESULTADO_GESTION',
                            'WRG2.ID_RESULTADO_GESTION_N2',
                            DB::raw('CONVERT(VARCHAR(10),WGL.FECHA_VOLVER_LLAMAR,120) as GESTION_VOLVER_LLAMAR'),
                            'WRG.DESCRIPCION as GESTION_MOTIVO',
                            'WRG2.DESCRIPCION as GESTION_RESULTADO')
                ->leftJoin('WEBVPC_GESTION_LEAD_CAMP_EST as WGL', function($join){
                    $join->on('WGL.PERIODO', '=', 'WLC.PERIODO');
                    $join->on('WGL.NUM_DOC', '=', 'WLC.NUM_DOC');
                    $join->on('WGL.ID_CAMP_EST', '=', 'WLC.ID_CAMP_EST');
                    $join->on('WGL.FLG_ULTIMO', '=', DB::raw('1'));
                })
                ->leftJoin('WEBVPC_RESULTADO_GESTION as WRG', function($join){
                    $join->on('WGL.ID_RESULTADO_GESTION', '=', 'WRG.ID_RESULTADO_GESTION');
                })
                ->leftJoin('WEBVPC_RESULTADO_GESTION_N2 as WRG2', function($join){
                    $join->on('WRG2.ID_RESULTADO_GESTION_N2', '=', 'WRG.ID_RESULTADO_GESTION_N2');
                });

        return $sql;
    }

    function getDistritosByEjecutivo($periodo,$en,$canales,$tipo =NULL){

        $sql = DB::table('WEBVPC_LEAD_CAMP_EST AS WLC')
                    ->select('WL.DISTRITO')
                    ->join('WEBVPC_LEAD AS WL', function($join){
                        $join->on('WLC.NUM_DOC', '=', 'WL.NUM_DOC');
                        $join->on('WLC.PERIODO', '=', 'WL.PERIODO');
                    })
                    ->join('WEBVPC_CAMP_EST_INSTANCIA AS WCI', function($join){
                        $join->on('WLC.ID_CAMP_EST', '=', 'WCI.ID_CAMP_EST');
                        $join->on('WCI.HABILITADO', '=', DB::raw('1'));
                    })
                    ->join('WEBVPC_LEADS_EJECUTIVO as WLE', function($join){
                        $join->on('WL.PERIODO', '=', 'WLE.PERIODO');
                        $join->on('WL.NUM_DOC', '=', 'WLE.NUM_DOC');
                    })
                    ->where('WLE.FLG_ULTIMO','=',1)
                    //->where('WLC.PERIODO',$periodo)                    
                    ->where('WL.DISTRITO','<>',NULL)
                    ->distinct();

                if($en!=NULL)   
                    $sql=$sql->where('WLE.REGISTRO_EN',$en);
                if($tipo==NULL)
                    $sql=$sql->whereIn('WLC.CANAL_ACTUAL',$canales);
        return $sql;
    }

    function getCampanhasByEjecutivo($periodo,$en,$canales){
        $sql = $this->getCampanhasByLead($periodo,null,$canales)
            ->join('WEBVPC_LEAD AS WL', function($join){
                $join->on('WLC.NUM_DOC', '=', 'WL.NUM_DOC');
                $join->on('WLC.PERIODO', '=', 'WL.PERIODO');
            })->join('WEBVPC_LEADS_EJECUTIVO as WLE', function($join){
                    $join->on('WL.PERIODO', '=', 'WLE.PERIODO');
                    $join->on('WL.NUM_DOC', '=', 'WLE.NUM_DOC');
            })
            ->where('WLE.FLG_ULTIMO','=',1)
            ->distinct();
        if ($en){
            $sql = $sql->where('WLE.REGISTRO_EN',$en);
        }
        return $sql;
    }

    function getCampanhasByEjecutivoResumido($periodo,$en,$canales){
        $sql= $this->getCampanhasByEjecutivo($periodo,$en,$canales);
        $sql->columns= ['WC.ID_CAMP_EST','WC.NOMBRE']; 
        return $sql;

    }


    function getCampanhasByAsistente($periodo,$ac,$canales){
        $sql = $this->getCampanhasByEjecutivo($periodo,null,$canales)
            ->join('WEBVPC_AC_EN_RELACION as WACE','WACE.EJECUTIVO_NEGOCIO','=','WLE.REGISTRO_EN')
            ->where('WACE.ASISTENTE_COMERCIAL','=',$ac);
        return $sql;
    }

    function getPropensiones()
    {
        $sql = DB::table('WEBVPC_CAMPAÑA_PROPENSION as WCPRO');
        return $sql;
    }

    function getResumenByEjecutivo($periodo,$en,$canales,$campanhas,$zona = null,$centro = null,$tienda = null,$order = null){
         $sql = DB::table('WEBVPC_LEAD AS WL')
            ->select(
                    DB::raw('COUNT(1) as TOTAL'),
                    DB::raw('WU.REGISTRO as REGISTRO_EN'),
                    DB::raw('WU.NOMBRE as NOMBRE_EN'),
                    DB::raw('WT.TIENDA as TIENDA'),
                    DB::raw('WT.CENTRO as CENTRO'),
                    DB::raw('WT.ZONA as ZONA'),
                    DB::raw('COUNT(WGL.ID_GESTION) GESTIONES'),
                    DB::raw('COUNT(DISTINCT WL.NUM_DOC) LEADS'),
                    //DB::raw('COUNT(DISTINCT CASE WHEN ID_GESTION IS NULL AND WCIT.FECHA_CITA > GETDATE() THEN WCIT.ID_CITA ELSE NULL END) CITAS_PENDIENTES'),
                    //DB::raw('COUNT(DISTINCT CASE WHEN ID_GESTION IS NULL AND WCIT.FECHA_CITA < GETDATE() THEN WCIT.ID_CITA ELSE NULL END) CITAS_VENCIDAS')
                    DB::raw('COUNT(DISTINCT CASE WHEN WCIT.FECHA_CITA > GETDATE() AND WCIT.ESTADO IN (1,2,3) THEN WCIT.ID_CITA ELSE NULL END) CITAS_PENDIENTES'),
                    DB::raw('COUNT(DISTINCT CASE WHEN WCIT.FECHA_CITA < GETDATE() AND WCIT.ESTADO IN (1,2,3) THEN WCIT.ID_CITA ELSE NULL END) CITAS_VENCIDAS')
            )
            ->join('WEBVPC_LEAD_CAMP_EST AS WLC', function($join){
                $join->on('WLC.NUM_DOC', '=', 'WL.NUM_DOC');
                $join->on('WLC.PERIODO', '=', 'WL.PERIODO');
            })
            ->join('WEBVPC_CAMP_EST_INSTANCIA AS WCI', function($join){
                    $join->on('WLC.ID_CAMP_EST', '=', 'WCI.ID_CAMP_EST');
                    $join->on('WLC.PERIODO','=','WCI.PERIODO');
            })
            ->join('WEBVPC_LEADS_EJECUTIVO as WLE', function($join){
                    $join->on('WL.NUM_DOC', '=', 'WLE.NUM_DOC');
                    $join->on('WL.PERIODO', '=', 'WLE.PERIODO');
            })
            ->join('WEBVPC_USUARIO AS WU', function($join){
                $join->on('WU.REGISTRO', '=', 'WLE.REGISTRO_EN');
            })
            ->join('WEBVPC_TIENDA as WT', function($join){
                    $join->on('WT.ID_TIENDA', '=', 'WU.ID_TIENDA');
            })
            ->leftJoin('WEBVPC_GESTION_LEAD_CAMP_EST as WGL', function($join){
                    $join->on('WGL.NUM_DOC', '=', 'WLC.NUM_DOC');
                    $join->on('WGL.ID_CAMP_EST', '=', 'WLC.ID_CAMP_EST');
                    $join->on('WGL.PERIODO', '=', 'WLC.PERIODO');
                    $join->on('WGL.FLG_ULTIMO', '=', DB::raw('1'));
            })
            ->leftJoin('WEBVPC_CITAS as WCIT', function($join){
                    $join->on('WL.NUM_DOC', '=', 'WCIT.NUM_DOC');
                    $join->on(DB::raw('CONVERT(varchar(6),WCIT.FECHA_CITA,112)'),'>=','WLE.PERIODO');
            })
            ->groupBy('WU.REGISTRO','WU.NOMBRE','WT.TIENDA','WT.CENTRO','WT.ZONA')
            ->where('WCI.HABILITADO','=',1)
            ->where('WLE.FLG_ULTIMO','=',1)
            //->where('WLE.PERIODO','=',$periodo)
            ->whereIn('WLC.CANAL_ACTUAL',$canales);
        if ($en){
            $sql = $sql->where('WU.REGISTRO',$en);
        }
        if ($campanhas){
            $sql = $sql->whereIn('WLC.ID_CAMP_EST',$campanhas);
        }
        if ($zona){
            $sql = $sql->where('WT.ID_ZONA',$zona);
        }
        if ($centro){
            $sql = $sql->where('WT.ID_CENTRO',$centro);
        }
        if ($tienda){
            $sql = $sql->where('WT.ID_TIENDA',$tienda);
        }
        if ($order){
            $sql = $sql->orderBy($this->getOrderColumn($order['field']),$order['order']);
        }
        
        return $sql;
    }


    function countResumenByEjecutivo($periodo, $en, $canales, $campanhas, $zona, $centro, $tienda){
        $sql = $this->getResumenByEjecutivo($periodo, $en, $canales, $campanhas, $zona, $centro, $tienda);
        $sql->columns = null;
        $sql->groups = null;
        
        //dd($sql->toSql());
        $result = $sql->addSelect(DB::raw('COUNT(DISTINCT WU.REGISTRO) as CANTIDAD'))->first();
        
        return $result->CANTIDAD;
    }

    function getResumenByEjecutivoV2($periodo,$en,$canales,$campanhas,$zona = null,$centro = null,$tienda = null,$order = null){

        $sql = DB::table('WEBVPC_LEAD AS WL')
            ->select(
                    DB::raw('COUNT(1)-CAST(SUM(CASE WHEN WL.FLG_ES_CLIENTE = 1 THEN 1  ELSE 0 END) AS FLOAT) as TOTAL'),
                    DB::raw('WU.REGISTRO as REGISTRO_EN'),
                    DB::raw('WU.NOMBRE as NOMBRE_EN'),
                    DB::raw('WT.TIENDA as TIENDA'),'WU.ID_TIENDA AS ID_TIENDA',
                    DB::raw('WT.CENTRO as CENTRO'),'WU.ID_CENTRO AS ID_CENTRO',
                    DB::raw('WT.ZONA as ZONA'),'WU.ID_ZONA AS ID_ZONA',
                    DB::raw('SUM(CASE WHEN WGL.ID_GESTION IS NOT NULL AND WL.FLG_ES_CLIENTE=0 THEN 1 ELSE 0 END) GESTIONES'),
                    DB::raw('COUNT(DISTINCT CASE WHEN WL.FLG_ES_CLIENTE =0 THEN WL.NUM_DOC ELSE NULL END) LEADS'),
                    DB::raw('COUNT(DISTINCT CASE WHEN WCIT.FECHA_CITA > GETDATE() AND WCIT.ESTADO IN (1,2,3) THEN WCIT.ID_CITA ELSE NULL END) CITAS_PENDIENTES'),
                    DB::raw('COUNT(DISTINCT CASE WHEN WCIT.FECHA_CITA < GETDATE() AND WCIT.ESTADO IN (1,2,3) THEN WCIT.ID_CITA ELSE NULL END) CITAS_VENCIDAS'),
                    DB::raw('CAST(SUM(CASE WHEN WGL.ID_GESTION IS NOT NULL AND WL.FLG_ES_CLIENTE=1 THEN 1 ELSE 0 END)  AS FLOAT) GESTIONES_CLIENTES'),
                    DB::raw('CAST(SUM(CASE WHEN WL.FLG_ES_CLIENTE=1 AND WCE.ID_CAMP_EST>=45 THEN 1 ELSE 0 END) AS FLOAT)  CLIENTES'),
                    DB::raw('CAST(COUNT(DISTINCT CASE   WHEN WLC.ID_CAMP_EST = 45  THEN WL.NUM_DOC    ELSE NULL  END) AS FLOAT) TOTAL_CRECER'),
                    DB::raw('CAST(COUNT(DISTINCT CASE  WHEN WGL.ID_GESTION IS NOT NULL AND   WGL.FLG_ULTIMO = 1 AND    WGL.ID_CAMP_EST = 45 THEN WL.NUM_DOC   ELSE NULL  END) AS FLOAT) GESTION_CRECER')
            )
            ->join('WEBVPC_LEAD_CAMP_EST AS WLC', function($join){
                $join->on('WLC.NUM_DOC', '=', 'WL.NUM_DOC');
                $join->on('WLC.PERIODO', '=', 'WL.PERIODO');
            })
            ->join('WEBVPC_CAMP_EST_INSTANCIA AS WCEI', function($join){
                    $join->on('WLC.ID_CAMP_EST', '=', 'WCEI.ID_CAMP_EST');
                    $join->on('WLC.PERIODO','=','WCEI.PERIODO');
            })
            ->join('WEBVPC_CAMP_EST as WCE',function($join){ 
                $join->on('WCE.ID_CAMP_EST','=','WCEI.ID_CAMP_EST');
            })        
            ->join('WEBVPC_LEADS_EJECUTIVO as WLE', function($join){
                    $join->on('WL.NUM_DOC', '=', 'WLE.NUM_DOC');
                    $join->on('WL.PERIODO', '=', 'WLE.PERIODO');
            })
            ->leftJoin('WEBVPC_USUARIO AS WU', function($join){
                $join->on('WU.REGISTRO', '=', 'WLE.REGISTRO_EN');
            })
            ->join('WEBVPC_TIENDA as WT', function($join){
                    $join->on('WT.ID_TIENDA', '=', 'WU.ID_TIENDA');
            })
            ->leftJoin('WEBVPC_GESTION_LEAD_CAMP_EST as WGL', function($join){
                    $join->on('WGL.NUM_DOC', '=', 'WLC.NUM_DOC');
                    $join->on('WGL.ID_CAMP_EST', '=', 'WLC.ID_CAMP_EST');
                    $join->on('WGL.PERIODO', '=', 'WLC.PERIODO');
                    $join->on('WGL.FLG_ULTIMO', '=', DB::raw('1'));
            })
            ->leftJoin('WEBVPC_CITAS as WCIT', function($join){
                    $join->on('WL.NUM_DOC', '=', 'WCIT.NUM_DOC');
                    $join->on(DB::raw('CONVERT(varchar(6),WCIT.FECHA_CITA,112)'),'>=','WLE.PERIODO');
            })
            ->groupBy('WU.REGISTRO','WU.NOMBRE','WT.TIENDA','WU.ID_TIENDA','WT.CENTRO','WU.ID_CENTRO','WT.ZONA','WU.ID_ZONA')
            ->where('WCEI.HABILITADO','=',1)
            ->where('WLE.FLG_ULTIMO','=',1)
            ->where('WCE.BANCA','BPE')
            ->where('WU.FLAG_ACTIVO','=',1)
            ->where(function($q) use ($canales){
                $q->where(function ($query) use ($canales){
                    $query->whereIn('WLC.CANAL_ACTUAL',$canales);
                })
                ->orWhere(function($query) use ($canales){
                    $query->whereNull('WLC.CANAL_ACTUAL');
                });
            });
            

        if ($en){
            $sql = $sql->where('WU.REGISTRO',$en);
        }
        if ($campanhas){ 
            $sql = $sql->whereIn('WLC.ID_CAMP_EST',$campanhas);
        }
        if ($zona){
            $sql = $sql->where('WT.ID_ZONA',$zona);
        }
        if ($centro){
            $sql = $sql->where('WT.ID_CENTRO',$centro);
        }
        if ($tienda){
            $sql = $sql->where('WT.ID_TIENDA',$tienda);
        }
        if ($order){
            $sql = $sql->orderBy($this->getOrderColumn($order['field']),$order['order']);
        }
        
        return $sql;
    }

    function countResumenByEjecutivoV2($periodo, $en, $canales, $campanhas, $zona, $centro, $tienda){
        $sql = $this->getResumenByEjecutivoV2($periodo, $en, $canales, $campanhas, $zona, $centro, $tienda);
        $sql->columns = null;
        $sql->groups = null;
        
        //dd($sql->toSql());
        $result = $sql->addSelect(DB::raw('COUNT(DISTINCT WU.REGISTRO) as CANTIDAD'))->first();
        
        return $result->CANTIDAD;
    }

    function getOrderColumn($field){
        switch ($field) {
                case 'ejecutivo':
                    return 'WU.NOMBRE';
                case 'avance':
                    return DB::raw('COUNT(WGL.ID_GESTION) * 100/COUNT(1)');
                case 'vencidas':
                    return 'CITAS_VENCIDAS';
            };
    }

    //Cartera

    function getCampanhasCartera(){
        $sql=DB::table('WEBVPC_CAMP_EST')
        ->select('ID_CAMP_EST','NOMBRE')
        ->where('TIPO','=','CARTERA')
        ->where('BANCA','=','BPE');
        return $sql;
    }

    function getResumenCarteraByUsuario($periodo,$usuario=null,$filtros){
        $sql=DB::Table('WEBVPC_LEAD AS WL')
            ->select(DB::raw('CAST(COUNT(DISTINCT CASE WHEN WGES.ID_GESTION IS NOT NULL AND WGES.FLG_ULTIMO=1 AND WLCE.ID_CAMP_EST=45 THEN WL.NUM_DOC ELSE NULL END) AS FLOAT) AS GESTIONES'),DB::raw('CAST(COUNT(DISTINCT CASE WHEN WLCE.ID_CAMP_EST=45 THEN WL.NUM_DOC ELSE NULL END)AS FLOAT) AS TOTAL'), DB::raw('SUM(CASE WHEN WCE.ID_CAMP_EST=45 AND WGES.ID_GESTION IS NULL THEN WCLI.MONTO_DISPONIBLE ELSE 0 END) AS CRECER')
        )

        ->join('WEBVPC_CLIENTE as WCLI',function($join){ 
            $join->on('WCLI.NUM_DOC','=','WL.NUM_DOC'); 
            $join->on('WCLI.PERIODO','=','WL.PERIODO');
        })
        ->join('WEBVPC_LEADS_EJECUTIVO as WLE',function($join){ 
            $join->on('WLE.PERIODO','=','WL.PERIODO');
            $join->on('WLE.NUM_DOC','=','WL.NUM_DOC');
            $join->on('WLE.FLAG_CARTERA','=',DB::raw(1));            
        })        
        ->leftJoin('WEBVPC_CLASIFICACION_RIESGOS as WCR',function($join){ 
            $join->on('WCR.NUM_DOC','=','WL.NUM_DOC'); 
        })
        ->join('WEBVPC_LEAD_CAMP_EST as WLCE',function($join){ 
            $join->on('WLCE.PERIODO','=','WL.PERIODO');
            $join->on('WLCE.NUM_DOC','=','WL.NUM_DOC');
         })
        ->join('WEBVPC_CAMP_EST_INSTANCIA as WCEI',function($join){ 
            $join->on('WCEI.ID_CAMP_EST','=','WLCE.ID_CAMP_EST'); 
            $join->on('WCEI.PERIODO','=','WLCE.PERIODO');
        })
        ->join('WEBVPC_CAMP_EST as WCE',function($join){ 
            $join->on('WCE.ID_CAMP_EST','=','WCEI.ID_CAMP_EST');
        })
        ->leftJoin('WEBVPC_GESTION_LEAD_CAMP_EST as WGES',function($join){ 
            $join->on('WGES.PERIODO','=','WL.PERIODO ');
            $join->on('WL.NUM_DOC','=','WGES.NUM_DOC');
            $join->on('WGES.ID_CAMP_EST','=','WCEI.ID_CAMP_EST');
            $join->on('WGES.FLG_ULTIMO','=',DB::raw(1));
        })
        ->leftJoin('WEBVPC_USUARIO as WU',function($join){
            $join->on('WU.REGISTRO','=','WLE.REGISTRO_EN');
        })        

        ->where('WLE.FLG_ULTIMO','=',1)
        ->where('WL.FLG_ES_CLIENTE','=',1)
        ->where('WCE.TIPO','=','CARTERA')
        ->where('WCE.BANCA','BPE')
        ->where('WU.FLAG_ACTIVO','=',1)
        ->where('WCEI.HABILITADO','=',1);

        //Filtros y ordenamiento irían aquí
        if(isset($filtros['cliente']))
            $sql=$sql->where('WL.NOMBRE_CLIENTE','like','%'.$filtros['cliente'].'%');
               
        if(isset($filtros['zonal']))
            $sql=$sql->where('WU.ID_ZONA','=',$filtros['zonal']);
        
        if(isset($filtros['centro']))
            $sql=$sql->where('WU.ID_CENTRO','=',$filtros['centro']);

        if(isset($filtros['tienda']))
            $sql=$sql->where('WU.ID_TIENDA','=',$filtros['tienda']);

        if(isset($filtros['documento']))
            $sql=$sql->where('WL.NUM_DOC','=',$filtros['documento']);

        if(isset($filtros['codigo']))
            $sql=$sql->where('WL.COD_UNICO','like','%'.$filtros['codigo']);

        if(isset($filtros['campanha']))
            $sql=$sql->where('WCE.ID_CAMP_EST','=',$filtros['campanha']);

        if(isset($filtros['distrito']))
            $sql=$sql->where('WL.DISTRITO','=',$filtros['distrito']);

        if(isset($filtros['producto']))
            $sql=$sql->where('WCLI.PRODUCTO_PRINCIPAL','=',$filtros['producto']);

        if(isset($filtros['bloqueo']))
            $sql=$sql->where('WCLI.MOTIVO_BLOQUEO','=',$filtros['bloqueo']);

        if(isset($filtros['flgCanal']))
            $sql=$sql->whereIn('WCLI.CANAL',['EN']);

        if(isset($filtros['canal']))
            $sql=$sql->where('WCLI.CANAL','=',$filtros['canal']);


        if(isset($filtros['segmento'])){
            if($filtros['segmento']=='mBajo')
                $sql=$sql->where('WCLI.SCORE_COMPORTAMIENTO','like','%riesgo medio bajo%'); 
            else    
                $sql=$sql->where('WCLI.SCORE_COMPORTAMIENTO','like','riesgo '.$filtros['segmento'].'%');
        }

        

        return $sql;
    }

    function getResumenCarteraByEjecutivo($periodo,$ejecutivo=null,$filtros){

        if($ejecutivo !=NULL) $filtros['ejecutivo']=$ejecutivo;

        $sql=$this->getResumenCarteraByUsuario($periodo,$ejecutivo,$filtros)
                ->addSelect(DB::raw('ISNULL(WMC.AVANCE,0) COLOCACION'))
                ->addSelect(DB::raw('ISNULL(WMC.META,0) META'))
                ->addSelect(DB::raw('CASE WHEN ISNULL(WMC.META,0) <>0 THEN ROUND(ISNULL(WMC.AVANCE,0)*100/ISNULL(WMC.META,0),2)ELSE 0 END AVANCE_META'))
                ->addSelect(DB::raw('COUNT(DISTINCT(CASE WHEN WCLI.PROXIMOS_BLOQUEOS=1 THEN WL.NUM_DOC ELSE NULL END)) LINEAS_VENCER'))     
                ->leftJoin('WEBVPC_META_COLOCACION AS WMC',function($join){
                    $join->on('WMC.REGISTRO_EN','=','WLE.REGISTRO_EN');
                })
                ->groupBy('WMC.AVANCE','WMC.META');

        //Filtros y ordenamiento irían aquí
        if(isset($filtros['ejecutivo']))
            $sql=$sql->where('WLE.REGISTRO_EN','=',$filtros['ejecutivo']);
              
        if(isset($filtros['marca']))
            $sql=$sql->where('WLE.ETIQUETA_EJECUTIVO','=',$filtros['marca']);

        return $sql;
    }


    function getResumenCarteraByJefe($periodo,$ejecutivo=null,$filtros){
        
        if($ejecutivo !=NULL) $filtros['ejecutivo']=$ejecutivo;
        
        $sql=$this->getResumenCarteraByUsuario($periodo,$ejecutivo,$filtros);
        
        if(isset($filtros['ejecutivo']))
            $sql=$sql->where('WLE.REGISTRO_EN','=',$filtros['ejecutivo']);        
        
        return $sql;
    }


    function getResumenCarteraByAsistente($periodo,$asistente=null,$filtros){

        $sql=$this->getResumenCarteraByUsuario($periodo,$asistente,$filtros)
                ->join('WEBVPC_AC_EN_RELACION as ASIS',function($join){
                    $join->on('ASIS.EJECUTIVO_NEGOCIO','=','WU.REGISTRO');
                })      
                ->where('ASIS.ASISTENTE_COMERCIAL','=',$asistente);
      
        //Filtros y ordenamiento irían aquí
        if(isset($filtros['ejecutivo']) and $filtros['ejecutivo']!=NULL )
            $sql=$sql->where('WLE.REGISTRO_EN','=',$filtros['ejecutivo']);
             
        return $sql;
    }   

    function cambioCampanhaCliente($periodo,$ejecutivo,$cliente, $campanhaNueva){

        //ACTUALIZACIÓN
        DB::table('WEBVPC_LEAD_CAMP_EST')
            ->where('NUM_DOC','=',$cliente)
            ->where('PERIODO','=',$periodo)
            ->update(['ID_CAMP_EST' => $campanhaNueva,'FECHA_ACTUALIZACION'=>Carbon::now()->toDateTimeString()]);

        $sql=DB::table('WEBVPC_LEAD_CAMP_EST_HIST') 
            ->select(DB::raw('COUNT(1) AS CAMP_PERIODO'))
            ->where('NUM_DOC','=',$cliente) 
            ->where('PERIODO','=',$periodo);

        if($sql->first()->CAMP_PERIODO >0)
            $flg_inicial=0; //No es la primera campaña del periodo        
        else
            $flg_inicial=1;
        
        $registroHisto= [
            'PERIODO'=>$periodo,
            'NUM_DOC'=>$cliente,
            'ID_CAMP_EST'=>$campanhaNueva,
            'REGISTRO_EN'=>$ejecutivo,
            'FECHA_REGISTRO'=>Carbon::now()->toDateTimeString(),
            'FLAG_ESTRATEGIA_INICIAL'=>$flg_inicial
        ];
        

        //INSERCIÓN
        DB::table('WEBVPC_LEAD_CAMP_EST_HIST')
            ->insert($registroHisto);

        return;
    }


    //ACCIONES COMERCIALES
    function updateFechaFin($data){
        DB::beginTransaction();
        $status = true;
        try {

            DB::table('WEBVPC_AC_FECHAS_HISTORICO')->insert($data);
            
            DB::table('WEBVPC_LEAD_CAMP_EST')
            ->where('NUM_DOC', $data['NUM_DOC'])
            ->where('ID_CAMP_EST',$data['ID_CAMP_EST'])
            ->where('TIPO_ASIGNACION',$data['TIPO_ASIGNACION'])
            ->update(['FECHA_FIN' => $data['FECHA_FIN'],'FECHA_INICIO'=>$data['FECHA_INICIO']]);

            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }

        return $status;
    }

}
