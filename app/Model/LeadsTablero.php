<?php
namespace App\Model;
use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class LeadsTablero extends Model

{
	protected $table = 'WEBVPC_LEADS_TABLERO';
    
    const ITEMS_PER_PAGE=10;
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey =['PERIODO',
                            'TIPO_CAMPANHA',
                            'COD_UNICO',
                            'NUM_DOC',
                            'REGISTRO_EN',
                            ];
					        

        /**
     * The name of the "created at" column.
     *
     * @var string
     */
     public $timestamps = false;
    
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'ZONAL',
        'CENTRO',
        'TIENDA',
        'NOMBRE_CLIENTE',
        'TIPO_DOCUMENTO',
        'TIPO_PERSONA',
        'DISTRITO',
        'DIRECCION',
        'SCORE_BURO',
        'DEUDA_SSFF',
        'BANCO_PRINCIPAL_SSFF',
        'DEUDA_6M_SSFF',
        'VARIACION_DEUDA_6M_SSFF',
        'GESTION_TIPO',
        'GESTION_COMENTARIO',
        'FECHA_CITA',
        'TASA',
        'CANAL_DESPLIEGUE',
        'SEGMENTO',
        'PRIORIDAD',
        ];

        static function getValoresTableroByLeadsall($page){
            return DB:: table('WEBVPC_LEADS_TABLERO as v')
                        ->select(['PERIODO','TIPO_CAMPANHA','ZONAL','CENTRO','TIENDA','REGISTRO_EN','COD_UNICO','NUM_DOC','NOMBRE_CLIENTE','TIPO_DOCUMENTO','TIPO_PERSONA','DISTRITO','DIRECCION','SCORE_BURO','DEUDA_SSFF','BANCO_PRINCIPAL_SSFF','DEUDA_6M_SSFF','VARIACION_DEUDA_6M_SSFF','GESTION_TIPO','GESTION_COMENTARIO','FECHA_CITA','TASA','CANAL_DESPLIEGUE','SEGMENTO','PRIORIDAD'])
                        ->orderby('PERIODO')->get();
            //echo $SQL;
            //die();
        }

        static function getValoresTableroByLeads($page){
            $result = DB:: table('WEBVPC_LEADS_TABLERO as v')
                        ->select(['PERIODO','TIPO_CAMPANHA','ZONAL','CENTRO','TIENDA','REGISTRO_EN','COD_UNICO','NUM_DOC','NOMBRE_CLIENTE'])
                        ->orderby('PERIODO')
                        ->take(self::ITEMS_PER_PAGE)
                        ->skip(self::ITEMS_PER_PAGE*($page - 1))
                        ->get();

                        return $result;

        }


}


