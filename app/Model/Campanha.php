<?php
namespace App\Model;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;
//use App\Entity\BE\Etapa;

class Campanha extends Model

{
	protected $table = 'WEBVPC_CAMP_EST';
    
    /**
     * The primary key for the model.
     *
     * @var string
    protected $primaryKey =['PERIODO',
                            'NUM_DOC',
                            ];
     */
					        

        /**
     * The name of the "created at" column.
     *
     * @var string
     public $timestamps = false;
     */
    
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
    protected $guarded = [
        'NOMBRE'
        ];
                                                                                                                                                                                                                                    
     */

    /**
     * Devuelve todos los datos del lead
     *
     * @param string $periodo Periodo en que fue cargado el Lead.
     * @param string $lead Numero de documento del lead.
     * @param array $filtros Filtros de busqueda
     * @return table
     */

    static function getCampanhasBE(){        
        $sql=DB::table('WEBVPC_CAMP_EST')
        ->select('ID_CAMP_EST','NOMBRE')
        ->where('BANCA','=','BE')
        ->where('TIPO','=','PROSPECTOS')
        ->orderBy('ID_CAMP_EST ','ASC');

        return $sql;
    }

}



