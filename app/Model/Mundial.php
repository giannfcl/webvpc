<?php

namespace App\Model;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class Mundial extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'WEBVPC_PARTIDOS';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = null;
    public $incrementing = false;

    
    function listarPartidosCercanos($registro){
        $dias_seg=172800;

        $sql=DB::Table('WEBVPC_PARTIDOS AS WP')
        ->select('WP.ID_PARTIDO', 'EL.NOMBRE AS NOMBRE_LOCAL', 'EV.NOMBRE AS NOMBRE_VISITA','WP.ESTADIO','WP.HORA_CAD',
        'WP.FECHA','WP.HORA','EL.IMAGEN AS IMAGENL','EV.IMAGEN AS IMAGENV','WS.GOLES_LOCAL','WS.GOLES_VISITANTE','WS.RESULTADO','WS.FECHA_CARGA',DB::raw('DATEDIFF(SECOND,GETDATE(),WP.FECHA) AS DIF_FECHA'))
        ->join('WEBVPC_EQUIPO as EL', function($join){
            $join->on('EL.COD_PAIS', '=', 'WP.EQUIPO_LOCAL');
        })
        ->join('WEBVPC_EQUIPO as EV', function($join){
            $join->on('EV.COD_PAIS', '=', 'WP.EQUIPO_VISITANTE');
        })
        ->leftJoin('WEBVPC_SCORE as WS',function($join) use($registro){
            $join->on('WS.ID_PARTIDO','=','WP.ID_PARTIDO');
            $join->on('WS.REGISTRO','=',DB::raw("'".$registro."'"));            
        })
        ->where(DB::raw("DATEDIFF(SECOND,GETDATE(),WP.FECHA)"),'>=',0)
        ->where(DB::raw("DATEDIFF(SECOND,GETDATE(),WP.FECHA)"),'<=',$dias_seg)
        //->where('WS.REGISTRO','=',$registro)
        ->orderBy('WP.FECHA','ASC');

        return $sql;
    }

    function listarPartidosRango($registro,$rango=NULL){
        $dias_seg=172800;

        $sql=DB::Table('WEBVPC_PARTIDOS AS WP')
        ->select('WP.ID_PARTIDO', 'EL.NOMBRE AS NOMBRE_LOCAL', 'EV.NOMBRE AS NOMBRE_VISITA','WP.ESTADIO','WP.HORA_CAD',
        'WP.FECHA','WP.HORA',DB::Raw("ISNULL(CONVERT(VARCHAR,WP.GOLES_LOCAL),'".'-'."') AS LOCAL_REAL"),DB::Raw("ISNULL(CONVERT(VARCHAR,WP.GOLES_VISITANTE),'".'-'."') AS VISITANTE_REAL"),'EL.GRUPO AS GRUPOL','EV.GRUPO AS GRUPOV','EL.IMAGEN AS IMAGENL','EV.IMAGEN AS IMAGENV',
            'WS.GOLES_LOCAL','WS.GOLES_VISITANTE','WS.RESULTADO','WS.FECHA_CARGA',DB::raw("DATEDIFF(SECOND,"."GETDATE()".",WP.FECHA_HORA) AS DIF_FECHA"),
            DB::Raw('CASE WHEN WS.GOLES_LOCAL=WP.GOLES_LOCAL AND WS.GOLES_VISITANTE=WP.GOLES_VISITANTE THEN 3 WHEN WS.RESULTADO=WP.RESULTADO THEN 1 
                        ELSE 0 END AS PUNTOS'))
        ->join('WEBVPC_EQUIPO as EL', function($join){
            $join->on('EL.COD_PAIS', '=', 'WP.EQUIPO_LOCAL');
        })
        ->join('WEBVPC_EQUIPO as EV', function($join){
            $join->on('EV.COD_PAIS', '=', 'WP.EQUIPO_VISITANTE');
        })
        ->leftJoin('WEBVPC_SCORE as WS',function($join) use($registro){
            $join->on('WS.ID_PARTIDO','=','WP.ID_PARTIDO');
            $join->on('WS.REGISTRO','=',DB::raw("'".$registro."'"));            
        })
        //->whereIn(DB::raw("DATEDIFF(DAY,'"."20180614"."',WP.FECHA)"),[0,1])
        //->where('EL.GRUPO','=',$grupo)
        //->where('WS.REGISTRO','=',$registro)
        ->orderBy('WP.FECHA_HORA','ASC');

        if($rango!=NULL) $sql=$sql->whereBetween('WP.ID_PARTIDO',$rango);

        return $sql;
    }

    function obtenerPuestoPuntaje($area=null){
        $sql=DB::Table('WEBVPC_USUARIO AS WU')
                ->select(
            /*DB::Raw('DENSE_RANK() OVER (PARTITION BY (CASE WHEN WU.ROL IN (1,2,6,7,8,10) THEN 0 
                    WHEN WU.ROL IN (20,21,22,23,24,25,26,28,29,30,11) THEN 1 WHEN WU.ROL IN (32,666) THEN 2 END) ORDER BY SUM(CASE
                    WHEN WS.GOLES_LOCAL=WP.GOLES_LOCAL AND WS.GOLES_VISITANTE=WP.GOLES_VISITANTE THEN 3
                    WHEN WS.RESULTADO=WP.RESULTADO THEN 1 ELSE 0 END) DESC) AS PUESTO_BANCA'), */
            DB::Raw('DENSE_RANK() OVER (ORDER BY SUM(CASE
                    WHEN WS.GOLES_LOCAL=WP.GOLES_LOCAL AND WS.GOLES_VISITANTE=WP.GOLES_VISITANTE THEN 3
                    WHEN WS.RESULTADO=WP.RESULTADO THEN 1 ELSE 0 END) DESC) AS PUESTO_BANCA'), 

            'WU.REGISTRO','WU.NOMBRE',
            DB::Raw('SUM(CASE WHEN WS.GOLES_LOCAL=WP.GOLES_LOCAL AND WS.GOLES_VISITANTE=WP.GOLES_VISITANTE THEN 3
            WHEN WS.RESULTADO=WP.RESULTADO THEN 1 ELSE 0 END) AS PUNTOS'),DB::Raw('CASE WHEN WU.ROL IN (1,2,6,7,8,10) THEN 0 
                    WHEN WU.ROL IN (20,21,22,23,24,25,26,28,29,30,11) THEN 1 WHEN WU.ROL IN (32,666) THEN 2 END AREA'))
            ->leftJoin('WEBVPC_SCORE AS WS',function($join){ 
                $join->on('WU.REGISTRO','=','WS.REGISTRO');
            })
            ->leftJoin('WEBVPC_PARTIDOS AS WP',function($join){ 
                $join->on('WP.ID_PARTIDO','=','WS.ID_PARTIDO');
                $join->on(DB::raw("DATEDIFF(SECOND,"."GETDATE()".",WP.FECHA_HORA)"),'<=',DB::Raw(0));
            })
            //->where('WU.REGISTRO','=',$registro)
            //->whereIn('WU.ROL',[1,2,6,7,8,10,20,21,22,23,24,25,26,29,30,666])
            //
            ->groupBy('WU.REGISTRO','WU.NOMBRE','WU.ROL')
            //->orderBy('PUESTO_BANCA', 'ASC')
            ->orderBy('PUNTOS', 'DESC')
            ->orderBy('WU.NOMBRE', 'ASC');
            
            // if($area!=NULL) $sql=$sql->where(DB::Raw('CASE WHEN WU.ROL IN (1,2,6,7,8,10) THEN 0 
            //         WHEN WU.ROL IN (20,21,22,23,24,25,26,29,30) THEN 1 WHEN WU.ROL=666 THEN 2 END'),'=',$area);
        return $sql;
    }

    function insertarResultadoPartido($registro,$partidos,$data){

        DB::beginTransaction();
        $status = true;      

        try {
            DB::table('WEBVPC_SCORE')
                    ->where('REGISTRO','=',$registro)
                    ->whereIn('ID_PARTIDO',$partidos)
                    ->delete();
            DB::table('WEBVPC_SCORE')->insert($data);
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }


}