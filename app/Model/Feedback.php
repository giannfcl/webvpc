<?php
namespace App\Model;
use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class Feedback extends Model

{
	protected $table = 'WEBVPC_FEEDBACK';
    
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey =['PERIODO',
                            'FECHA_REGISTRO',               
                            'NUM_DOC', 
                            'REGISTRO_EN', 
        					];
					        

        /**
     * The name of the "created at" column.
     *
     * @var string
     */
     public $timestamps = false;
    
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [       
        'FEEDBACK',
        'TIPO_REGISTRO',
        'VALOR',
        ];


    /**
     * Registro de Feedback
     *
     * @param  array $feedback Datos del feedback. Ver Entidad.
     * @return bool
     */
    public function insert($feedback){
        DB::beginTransaction();
        $status = true;
        try {
            // Borra todos los feedback anteriores del periodo
            DB::table('WEBVPC_FEEDBACK')
            ->where('PERIODO', $feedback['PERIODO'])
            ->where('NUM_DOC', $feedback['NUM_DOC'])
            ->where('VALOR', $feedback['VALOR'])
            ->where('TIPO_REGISTRO', $feedback['TIPO_REGISTRO'])
            ->delete();

            //Inserta Feedback
            DB::table('WEBVPC_FEEDBACK')->insert($feedback);
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }

    /**
     * Eliminar un Feedback
     *
     * @param array $feedback Datos del feedback. Ver Entidad.
     * @return bool
     */
    public function eliminar($feedback){
        DB::beginTransaction();
        $status = true;
        try {
            DB::table('WEBVPC_FEEDBACK')
            ->where('PERIODO', $feedback['PERIODO'])
            ->where('NUM_DOC', $feedback['NUM_DOC'])
            ->where('VALOR', $feedback['VALOR'])
            ->where('TIPO_REGISTRO', $feedback['TIPO_REGISTRO'])
            ->delete();
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }

    /**
     * Listar todos los feedback segun periodo y lead
     *
     * @param array $periodo Periodo en que se registro el feedback
     * @param array $lead Numero de documento del lead
     * @return bool
     */
    public function listar($periodo,$lead){
        $sql = DB::table('WEBVPC_FEEDBACK as WF')
                ->select('WF.TIPO_REGISTRO','WF.VALOR','FEEDBACK')
                ->where('WF.PERIODO','=',$periodo)
                ->where('WF.NUM_DOC','=',$lead);
        return $sql;
    }
}