<?php

namespace App\Model;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class Telefono extends Model {

    const ITEMS_PER_PAGE = 20;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'telefonos';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = null;
	public $incrementing = false;
	
    protected $guarded = [
		'NOMBRE_CLIENTE',
        'NUM_DOC',
        'FUENTE_FIJO',
		'TELEFONO_FIJO1',
		'TELEFONO_FIJO2',
		'FUENTE_CEL',
		'TELEFONO_CEL1',
		'TELEFONO_CEL2',
    ];

    function get($ruc, $razonSocial, $page) {
        $result = DB::table('CONTACTO_TELEFONO_RESUMEN')
                ->select('NOMBRE_CLIENTE', 'NUM_DOC', 'FUENTE_FIJO','TELEFONO_FIJO1','TELEFONO_FIJO2','FUENTE_CEL','TELEFONO_CEL1','TELEFONO_CEL2')
                ->orderBy('NOMBRE_CLIENTE');

        if ($ruc) {
            $result->where('NUM_DOC', '=', $ruc);
        }
        if ($razonSocial) {
            $result->where('NOMBRE_CLIENTE', 'like', $razonSocial . '%');
        }

        $totalCount = $result->count();
        $results = $result
                ->take(self::ITEMS_PER_PAGE)
                ->skip(self::ITEMS_PER_PAGE * ($page - 1))
                ->get();

        $paginator = new \Illuminate\Pagination\LengthAwarePaginator($results, $totalCount, self::ITEMS_PER_PAGE, $page);

        return $paginator;
    }

}
