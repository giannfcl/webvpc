<?php
namespace App\Model;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;
use \App\Entity\Canal as Canal;

class Cliente extends Model

{
	//protected $table = 'WEBVPC_CLIENTE';
  
    function getClienteEjecutivo($periodo,$ejecutivo = null, $cliente = null,$filtros = null,$orden = null){

        if($ejecutivo !=NULL) $filtros['ejecutivo']=$ejecutivo;
        //dd($filtros);
        $sql=DB::Table('WEBVPC_LEAD AS WL')
            ->select('WL.PERIODO', 'WL.COD_UNICO', 'WL.NUM_DOC', 'WL.NOMBRE_CLIENTE', 'WL.REPRESENTANTE_LEGAL', 'WL.TIPO_DOCUMENTO','WL.DIRECCION', 'WL.PROVINCIA', 'WL.DEPARTAMENTO', 'WL.DISTRITO','WL.TIENDA', 'WL.TELEFONO1', 'WL.TELEFONO2', 'WL.TELEFONO3', 'WL.TELEFONO4',  'WL.DEUDA_SSFF','WL.DEUDA_SSFF_MONEDA','WL.DEUDA_6M_SSFF', 'WL.VARIACION_DEUDA_6M_SSFF','WL.BANCO_PRINCIPAL_SSFF','WL.GIRO','WL.ACTIVIDAD','WL.MARCA_ESTRELLA', 'WCLI.PRODUCTO_PRINCIPAL', 'WCLI.NUMERO_PRODUCTOS', 'WCLI.MOTIVO_BLOQUEO', 'WCLI.SCORE_COMPORTAMIENTO','WCLI.ATRASO_PROMEDIO','WCLI.ATRASO_ULTIMO', 
                'WCLI.MONTO_APROBADO','WCLI.SALDO','WCLI.MONTO_DISPONIBLE',
                'WLE.REGISTRO_EN','WLE.ETIQUETA_EJECUTIVO','WU.NOMBRE AS NOMBRE_EN','WCE.ID_CAMP_EST','WCE.NOMBRE','WCE.NOMBRE AS NOMBRE_CAMP','WGES.ID_RESULTADO_GESTION AS ID_RESULTADO', 'RGES2.DESCRIPCION AS DESCRIPCION_RESULTADO','RGES.ID_RESULTADO_GESTION AS ID_MOTIVO','RGES.DESCRIPCION AS DESCRIPCION_MOTIVO', 'WGES.COMENTARIO AS COMENTARIO_GESTION',DB::raw('CONVERT(VARCHAR(10),WGES.FECHA_VOLVER_LLAMAR,103) AS FECHA_VOLVER_LLAMAR'),
                'HIST.ID_CAMP_EST AS HISTORIA','HISTNOM.NOMBRE AS NOMB_HISTORIA','WCLI.SALDO_CDD','WCLI.BANCO_CDD','WCLI.FECHA_VENCIMIENTO_LINEA','WCLI.CANAL','WCLI.ESTRATEGIA as ESTRATEGIA_LINEA','WCLI.CANAL_ATENCION','WCLI.PROXIMOS_BLOQUEOS')

        ->join('WEBVPC_CLIENTE as WCLI',function($join){ 
            $join->on('WCLI.NUM_DOC','=','WL.NUM_DOC'); 
            $join->on('WCLI.PERIODO','=','WL.PERIODO');            
        })
        ->join('WEBVPC_LEADS_EJECUTIVO as WLE',function($join){ 
            $join->on('WLE.PERIODO','=','WL.PERIODO');
            $join->on('WLE.NUM_DOC','=','WL.NUM_DOC');
            $join->on('WLE.FLAG_CARTERA','=',DB::raw(1));
        })
        ->leftJoin('WEBVPC_USUARIO as WU',function($join){
            $join->on('WU.REGISTRO','=','WLE.REGISTRO_EN');
        })
        ->leftJoin('WEBVPC_CLASIFICACION_RIESGOS as WCR',function($join){ 
            $join->on('WCR.NUM_DOC','=','WL.NUM_DOC'); 
            $join->on('WCR.PERIODO','=','WL.PERIODO'); 
        })
        ->join('WEBVPC_LEAD_CAMP_EST as WLCE',function($join){ 
            $join->on('WLCE.PERIODO','=','WL.PERIODO');
            $join->on('WLCE.NUM_DOC','=','WL.NUM_DOC');
         })
        ->join('WEBVPC_CAMP_EST_INSTANCIA as WCEI',function($join){ 
            $join->on('WCEI.ID_CAMP_EST','=','WLCE.ID_CAMP_EST'); 
            $join->on('WCEI.PERIODO','=','WLCE.PERIODO');
        })
        ->join('WEBVPC_CAMP_EST as WCE',function($join){ 
            $join->on('WCE.ID_CAMP_EST','=','WCEI.ID_CAMP_EST');
        })        
        ->leftJoin('WEBVPC_GESTION_LEAD_CAMP_EST as WGES',function($join){ 
            $join->on('WGES.PERIODO','=','WL.PERIODO ');
            $join->on('WL.NUM_DOC','=','WGES.NUM_DOC');
            $join->on('WGES.ID_CAMP_EST','=','WCEI.ID_CAMP_EST');
            $join->on('WGES.FLG_ULTIMO','=',DB::raw(1));
        })
        ->leftJoin('WEBVPC_RESULTADO_GESTION as RGES', function($join){
            $join->on('RGES.ID_RESULTADO_GESTION','=','WGES.ID_RESULTADO_GESTION');
        })
        ->leftJoin('WEBVPC_RESULTADO_GESTION_N2 as RGES2', function($join){
            $join->on('RGES.ID_RESULTADO_GESTION_N2','=','RGES2.ID_RESULTADO_GESTION_N2');
        })
        ->leftJoin('WEBVPC_LEAD_CAMP_EST_HIST as HIST', function($join){
            $join->on('HIST.PERIODO','=','WL.PERIODO');
            $join->on('WL.NUM_DOC','=','HIST.NUM_DOC'); 
            $join->on('FLAG_ESTRATEGIA_INICIAL','=',DB::raw(1)); 
        })
        ->leftJoin('WEBVPC_CAMP_EST as HISTNOM',function($join){ 
            $join->on('HISTNOM.ID_CAMP_EST','=','HIST.ID_CAMP_EST');
        }) 

        //->where('WL.PERIODO','=',$periodo)
        ->where('WLE.FLG_ULTIMO','=',1)
        ->where('WL.FLG_ES_CLIENTE','=',1)
        //->where('WLE.REGISTRO_EN','=',$ejecutivo)
        ->where('WCE.TIPO','=','CARTERA')
        ->where('WCE.BANCA','BPE')        
        ->where('WCEI.HABILITADO','=',1);

        if(isset($cliente) and $cliente!=NULL){
            $sql=$sql->where('WL.NUM_DOC','=',$cliente);
        }
        
        //Filtros y ordenamiento irían aquí
        if(isset($filtros['cliente']))
            $sql=$sql->where('WL.NOMBRE_CLIENTE','like','%'.$filtros['cliente'].'%');

        if(isset($filtros['ejecutivo']))
            $sql=$sql->where('WLE.REGISTRO_EN','=',$filtros['ejecutivo']);
        
        if(isset($filtros['zonal']))
            $sql=$sql->where('WU.ID_ZONA','=',$filtros['zonal']);
        //dd($sql->get());
        if(isset($filtros['centro']))
            $sql=$sql->where('WU.ID_CENTRO','=',$filtros['centro']);

        if(isset($filtros['tienda']))
            $sql=$sql->where('WU.ID_TIENDA','=',$filtros['tienda']);

        if(isset($filtros['documento']))
            $sql=$sql->where('WL.NUM_DOC','=',$filtros['documento']);

        if(isset($filtros['codigo']))
            $sql=$sql->where('WL.COD_UNICO','like','%'.$filtros['codigo']);

        if(isset($filtros['campanha']))
            $sql=$sql->where('WCE.ID_CAMP_EST','=',$filtros['campanha']);

        if(isset($filtros['distrito']))
            $sql=$sql->where('WL.DISTRITO','=',$filtros['distrito']);

        if(isset($filtros['producto']))
            $sql=$sql->where('WCLI.PRODUCTO_PRINCIPAL','=',$filtros['producto']);

        if(isset($filtros['bloqueo']))
            $sql=$sql->where('WCLI.MOTIVO_BLOQUEO','=',$filtros['bloqueo']);

        if(isset($filtros['marca']))
            $sql=$sql->where('WLE.ETIQUETA_EJECUTIVO','=',$filtros['marca']);

        if(isset($filtros['segmento'])){
            if($filtros['segmento']=='mBajo')
                $sql=$sql->where('WCLI.SCORE_COMPORTAMIENTO','like','%riesgo medio bajo%'); 
            else    
                $sql=$sql->where('WCLI.SCORE_COMPORTAMIENTO','like','riesgo '.$filtros['segmento'].'%');
        }

        if(isset($filtros['flgCanal']))
            $sql=$sql->whereIn('WCLI.CANAL',['EN']);

        if(isset($filtros['canal']))
            $sql=$sql->where('WCLI.CANAL','=',$filtros['canal']);

        if(isset($filtros['canalAtencion']))
            $sql=$sql->where('WCLI.CANAL_ATENCION','=',$filtros['canalAtencion']);

        if(isset($filtros['proximosBloq']))
            $sql=$sql->where('WCLI.PROXIMOS_BLOQUEOS','=',1);
        

        if (!$orden or $orden['sort']==NULL)            
            $sql=$sql;
            //$sql =$sql->orderBy('WL.NUM_DOC','ASC'); //Orden por defecto
        else
            $sql = $sql->orderBy($this->getOrderColumn($orden['sort']),$orden['order']);
        
        
        return $sql;
    }
    

    function getClientesAsistenteComercial($periodo,$asistente = null, $cliente = null,$filtros = null,$orden = null){

        $sql=DB::Table('WEBVPC_LEAD AS WL')
            ->select('ASIS.ASISTENTE_COMERCIAL','WL.PERIODO', 'WL.COD_UNICO', 'WL.NUM_DOC', 'WL.NOMBRE_CLIENTE', 'WL.REPRESENTANTE_LEGAL', 'WL.TIPO_DOCUMENTO','WL.DIRECCION', 'WL.PROVINCIA', 'WL.DEPARTAMENTO', 'WL.DISTRITO','WL.TIENDA', 'WL.TELEFONO1', 'WL.TELEFONO2', 'WL.TELEFONO3', 'WL.TELEFONO4',  'WL.DEUDA_SSFF','WL.DEUDA_SSFF_MONEDA','WL.DEUDA_6M_SSFF', 'WL.VARIACION_DEUDA_6M_SSFF','WL.BANCO_PRINCIPAL_SSFF','WL.GIRO','WL.ACTIVIDAD', 'WCLI.PRODUCTO_PRINCIPAL', 'WCLI.NUMERO_PRODUCTOS', 'WCLI.MOTIVO_BLOQUEO','WCLI.SCORE_COMPORTAMIENTO','WCLI.ATRASO_PROMEDIO','WCLI.ATRASO_ULTIMO', 
                'WCLI.MONTO_APROBADO','WCLI.SALDO','WCLI.MONTO_DISPONIBLE',
                'WLE.REGISTRO_EN','WU.NOMBRE AS NOMBRE_EN','WCE.ID_CAMP_EST','WCE.NOMBRE','WCE.NOMBRE AS NOMBRE_CAMP','WGES.ID_RESULTADO_GESTION AS ID_RESULTADO', 'RGES2.DESCRIPCION AS DESCRIPCION_RESULTADO','RGES.ID_RESULTADO_GESTION AS ID_MOTIVO','RGES.DESCRIPCION AS DESCRIPCION_MOTIVO', 'WGES.COMENTARIO AS COMENTARIO_GESTION','WGES.FECHA_VOLVER_LLAMAR',
                'HIST.ID_CAMP_EST AS HISTORIA','HISTNOM.NOMBRE AS NOMB_HISTORIA','WCLI.SALDO_CDD','WCLI.BANCO_CDD','WCLI.FECHA_VENCIMIENTO_LINEA','WCLI.CANAL','WCLI.ESTRATEGIA as ESTRATEGIA_LINEA','WCLI.CANAL_ATENCION','WCLI.PROXIMOS_BLOQUEOS')

        ->join('WEBVPC_CLIENTE as WCLI',function($join){ 
            $join->on('WCLI.NUM_DOC','=','WL.NUM_DOC'); 
            $join->on('WCLI.PERIODO','=','WL.PERIODO');
        })
        ->join('WEBVPC_LEADS_EJECUTIVO as WLE',function($join){ 
            $join->on('WLE.PERIODO','=','WL.PERIODO');
            $join->on('WLE.NUM_DOC','=','WL.NUM_DOC');
        })
        ->leftJoin('WEBVPC_USUARIO as WU',function($join){
            $join->on('WU.REGISTRO','=','WLE.REGISTRO_EN');
        })
        ->leftJoin('WEBVPC_CLASIFICACION_RIESGOS as WCR',function($join){ 
            $join->on('WCR.NUM_DOC','=','WL.NUM_DOC'); 
            $join->on('WCR.PERIODO','=','WL.PERIODO'); 
        })
        ->join('WEBVPC_LEAD_CAMP_EST as WLCE',function($join){ 
            $join->on('WLCE.PERIODO','=','WL.PERIODO');
            $join->on('WLCE.NUM_DOC','=','WL.NUM_DOC');
         })
        ->join('WEBVPC_CAMP_EST_INSTANCIA as WCEI',function($join){ 
            $join->on('WCEI.ID_CAMP_EST','=','WLCE.ID_CAMP_EST'); 
            $join->on('WCEI.PERIODO','=','WLCE.PERIODO');
        })
        ->join('WEBVPC_CAMP_EST as WCE',function($join){ 
            $join->on('WCE.ID_CAMP_EST','=','WCEI.ID_CAMP_EST');
        })        
        ->leftJoin('WEBVPC_GESTION_LEAD_CAMP_EST as WGES',function($join){ 
            $join->on('WGES.PERIODO','=','WL.PERIODO ');
            $join->on('WL.NUM_DOC','=','WGES.NUM_DOC');
            $join->on('WGES.ID_CAMP_EST','=','WCEI.ID_CAMP_EST');
            $join->on('WGES.FLG_ULTIMO','=',DB::raw(1));
        })
        ->leftJoin('WEBVPC_RESULTADO_GESTION as RGES', function($join){
            $join->on('RGES.ID_RESULTADO_GESTION','=','WGES.ID_RESULTADO_GESTION');
        })
        ->leftJoin('WEBVPC_RESULTADO_GESTION_N2 as RGES2', function($join){
            $join->on('RGES.ID_RESULTADO_GESTION_N2','=','RGES2.ID_RESULTADO_GESTION_N2');
        })
        ->leftJoin('WEBVPC_LEAD_CAMP_EST_HIST as HIST', function($join){
            $join->on('HIST.PERIODO','=','WL.PERIODO');
            $join->on('WL.NUM_DOC','=','HIST.NUM_DOC'); 
            $join->on('FLAG_ESTRATEGIA_INICIAL','=',DB::raw(1)); 
        })
        ->leftJoin('WEBVPC_CAMP_EST as HISTNOM',function($join){ 
            $join->on('HISTNOM.ID_CAMP_EST','=','HIST.ID_CAMP_EST');
        }) 
        ->join('WEBVPC_AC_EN_RELACION as ASIS',function($join){
            $join->on('ASIS.EJECUTIVO_NEGOCIO','=','WLE.REGISTRO_EN');
        })

        //->where('WL.PERIODO','=',$periodo)
        ->where('WLE.FLG_ULTIMO','=',1)
        ->where('WL.FLG_ES_CLIENTE','=',1)
        ->where('ASIS.ASISTENTE_COMERCIAL','=',$asistente)
        ->where('WCE.TIPO','=','CARTERA')
        ->where('WCE.BANCA','BPE')
        ->where('WCEI.HABILITADO','=',1);

        if(isset($cliente) and $cliente!=NULL){
            $sql=$sql->where('WL.NUM_DOC','=',$cliente);
        }

        //Filtros y ordenamiento irían aquí
        if(isset($filtros['cliente']))
            $sql=$sql->where('WL.NOMBRE_CLIENTE','like','%'.$filtros['cliente'].'%');

        if(isset($filtros['ejecutivo']))
            $sql=$sql->where('WLE.REGISTRO_EN','=',$filtros['ejecutivo']);
        
        if(isset($filtros['zonal']))
            $sql=$sql->where('WU.ID_ZONA','=',$filtros['zonal']);
        //dd($sql->get());
        if(isset($filtros['centro']))
            $sql=$sql->where('WU.ID_CENTRO','=',$filtros['centro']);

        if(isset($filtros['tienda']))
            $sql=$sql->where('WU.ID_TIENDA','=',$filtros['tienda']);

        if(isset($filtros['documento']))
            $sql=$sql->where('WL.NUM_DOC','=',$filtros['documento']);

        if(isset($filtros['codigo']))
            $sql=$sql->where('WL.COD_UNICO','like','%'.$filtros['codigo']);

        if(isset($filtros['campanha']))
            $sql=$sql->where('WCE.ID_CAMP_EST','=',$filtros['campanha']);

        if(isset($filtros['distrito']))
            $sql=$sql->where('WL.DISTRITO','=',$filtros['distrito']);

        if(isset($filtros['producto']))
            $sql=$sql->where('WCLI.PRODUCTO_PRINCIPAL','=',$filtros['producto']);

        if(isset($filtros['bloqueo']))
            $sql=$sql->where('WCLI.MOTIVO_BLOQUEO','=',$filtros['bloqueo']);

        if(isset($filtros['segmento'])){
            if($filtros['segmento']=='mBajo')
                $sql=$sql->where('WCLI.SCORE_COMPORTAMIENTO','=',"Medio Bajo"); 
            else    
                $sql=$sql->where('WCLI.SCORE_COMPORTAMIENTO','=',$filtros['segmento']);
        }

        if(isset($filtros['flgCanal']))
            $sql=$sql->whereIn('WCLI.CANAL',['EN']);

        if(isset($filtros['canal']))
            $sql=$sql->where('WCLI.CANAL','=',$filtros['canal']);

        if(isset($filtros['canalAtencion']))
            $sql=$sql->where('WCLI.CANAL_ATENCION','=',$filtros['canalAtencion']);

        if(isset($filtros['proximosBloq']))
            $sql=$sql->where('WCLI.PROXIMOS_BLOQUEOS','=',1);


        if (!$orden or $orden['sort']==NULL)            
            $sql=$sql;

            //$sql =$sql->orderBy('WL.COD_UNICO','ASC'); //Orden por defecto
        else
            $sql = $sql->orderBy($this->getOrderColumn($orden['sort']),$orden['order']);
        
        
        return $sql;
    }

    function getProductosBPE(){
        $sql=DB::table('WEBVPC_CLIENTE')
                ->select(DB::raw('DISTINCT PRODUCTO_PRINCIPAL'))
                ->whereNotIn('PRODUCTO_PRINCIPAL',['NULL',''])
                ->orderBy('PRODUCTO_PRINCIPAL','ASC');
        return $sql;
    }

    function getCanalesCartera(){
        $sql=DB::table('WEBVPC_CLIENTE')
                ->select(DB::raw('DISTINCT CANAL'))
                ->whereNotIn('CANAL',['NULL',''])
                ->orderBy('CANAL','ASC');
        return $sql;
    }

    function getCanalesAtencionCartera(){
        $sql=DB::table('WEBVPC_CLIENTE')
                ->select(DB::raw('DISTINCT CANAL_ATENCION'))
                ->whereNotIn('CANAL_ATENCION',['NULL',''])
                ->orderBy('CANAL_ATENCION','ASC');
        return $sql;
    }

    function getMotivosBloqueoBPE(){
        $sql=DB::table('WEBVPC_CLIENTE')
                ->select(DB::raw('DISTINCT MOTIVO_BLOQUEO'))
                ->whereNotIn('MOTIVO_BLOQUEO',['NULL',''])
                ->orderBy('MOTIVO_BLOQUEO','ASC');
        return $sql;
    }

    function getOrderColumn($field){
        switch ($field) {
            case 'campanha':
                return 'WCE.NOMBRE';
            case 'cliente':
                return 'WL.NOMBRE_CLIENTE';
            case 'numProd':
                return 'WCLI.NUMERO_PRODUCTOS';
            case 'score':
                return 'WCLI.SCORE_COMPORTAMIENTO';
            case 'deuda':
                return 'WL.DEUDA_SSFF';
            case 'aprobado':
                return 'WCLI.MONTO_APROBADO';
            case 'disponible':
                return 'WCLI.MONTO_DISPONIBLE';
        };
    }

    function imprimir($periodo,$ejecutivo = null, $cliente = null,$filtros = null,$orden = null){
        $sql = $this->getClienteEjecutivo();
        $sql->select = null;
        $sql->addSelect( 'WL.COD_UNICO', 'WL.NUM_DOC', 'WL.NOMBRE_CLIENTE', 'WL.REPRESENTANTE_LEGAL','WL.DIRECCION', 'WL.DISTRITO','WL.TELEFONO1', 'WL.TELEFONO2', 'WL.TELEFONO3', 'WL.TELEFONO4',  'WL.DEUDA_SSFF','WL.DEUDA_SSFF_MONEDA','WL.DEUDA_6M_SSFF', 'WL.VARIACION_DEUDA_6M_SSFF','WL.BANCO_PRINCIPAL_SSFF','WCLI.MONTO_APROBADO','WCLI.SALDO','WCLI.MONTO_DISPONIBLE','WCE.NOMBRE AS NOMBRE_CAMP', 'WCLI.SCORE_COMPORTAMIENTO','WCLI.ATRASO_PROMEDIO','WCLI.ATRASO_ULTIMO', 'RGES2.DESCRIPCION AS DESCRIPCION_RESULTADO' );

        return $sql;

    }

}



