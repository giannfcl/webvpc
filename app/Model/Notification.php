<?php

namespace App\Model;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Jenssegers\Date\Date as Carbon;


class Notification extends Model {
    static function getCatalogoNotificaciones(){
        $sql = DB::table('T_WEB_NOTIFICACIONES_CATALOGO AS A')
                ->leftJoin('T_WEB_NOTIFICACIONES AS B',function($join){
                    $join->on('A.ID_NOTIFICACION', '=', 'B.ID_TIPO_NOTIFICACION');
                })
                ->select(DB::raw('A.CATEGORIA,A.ORDEN, COUNT(1) NOTIFICACIONES_PENDIENTES'))
                ->where('B.FLG_LEIDO','0')
                ->groupBy('A.CATEGORIA','A.ORDEN')->orderBy('A.ORDEN','asc')->get();
        return $sql;
    }
    static function getNotificationsTOT($registro,$filtro=null){
        $today = Carbon::now();
        $one_week_ago = Carbon::now()->subWeeks(4);
        $sql = DB::table('T_WEB_NOTIFICACIONES AS A')
            ->leftJoin('T_WEB_NOTIFICACIONES_CATALOGO AS B',function($join){
                $join->on('B.ID_NOTIFICACION', '=', 'A.ID_TIPO_NOTIFICACION');
            })
            ->select(
                DB::raw(
                    'A.ID_VALOR,B.MODULO, B.CATEGORIA, A.FECHA_NOTIFICACION, A.FLG_LEIDO, A.CONTENIDO, A.URL'
                )
            )
            ->where('REGISTRO_EN', $registro);
        if($filtro!=null){
            if($filtro['modulo']!=null){
                $sql = $sql->where('B.MODULO',$filtro['modulo']);
            }
            if($filtro['categoria']!=null){
                $sql = $sql->where('B.CATEGORIA',$filtro['categoria']);
            }
            if($filtro['contenido']!=null){
                $sql = $sql->where('A.CONTENIDO','like','%'.$filtro['contenido'].'%');
            }
            if($filtro['leido']!=null){
                $sql = $sql->where('A.FLG_LEIDO',$filtro['leido']);
            }
            if($filtro['fechaIni']!=null){
                $sql = $sql->where('A.FECHA_NOTIFICACION','>=',$filtro['fechaIni']);
            }
            if($filtro['fechaFin']!=null){
                $sql = $sql->where('A.FECHA_NOTIFICACION','<=',$filtro['fechaFin']);
            }
        }
        $sql = $sql->orderBy('FECHA_NOTIFICACION', 'DESC');
        return $sql->get();
    }
    static function getNotifications($registro)
    {
        $today = Carbon::now();
        $one_week_ago = Carbon::now()->subWeeks(4);
        $sql = DB::table('T_WEB_NOTIFICACIONES')
            ->select()
            ->distinct()
            ->whereBetween(DB::raw('FECHA_NOTIFICACION'), [$one_week_ago, $today])
            ->where('REGISTRO_EN', $registro)
            ->orderBy('FECHA_NOTIFICACION', 'DESC');
        return $sql->get();
    }
    static function updateVisto($registro)
    {
        DB::beginTransaction();
        $status = true;
        try {
            $today = Carbon::now();
            $one_week_ago = Carbon::now()->subWeeks(4);
            DB::table('T_WEB_NOTIFICACIONES')
                ->whereBetween(DB::raw('FECHA_NOTIFICACION'), [$one_week_ago, $today])
                ->where('REGISTRO_EN', $registro)
                ->whereNull('URL')
                ->update(['FLG_LEIDO' => 1,'FECHA_LEIDO' =>$today]);
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }
    static function updateVistoURL($registro, $id)
    {
        DB::beginTransaction();
        $status = true;
        try {
            $today = Carbon::now();
            $one_week_ago = Carbon::now()->subWeeks(4);
            DB::table('T_WEB_NOTIFICACIONES')
                ->whereBetween(DB::raw('FECHA_NOTIFICACION'), [$one_week_ago, $today])
                ->where('REGISTRO_EN', $registro)
                ->where('ID_VALOR', $id)
                ->whereNotNull('URL')
                ->update(['FLG_LEIDO' => 1,'FECHA_LEIDO' =>$today]);
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }

    function agregarnotificacion($notificacion)
    {
        DB::beginTransaction();
        $status = true;
        try {
            DB::table("T_WEB_NOTIFICACIONES")->insert($notificacion);
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;        
    }
}
