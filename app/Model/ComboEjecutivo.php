<?php

namespace App\Model;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class ComboEjecutivo extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'WEBVPC_SSRS_COMBOS_EJECUTIVO';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = null;
    public $incrementing = false;
	
    protected $guarded = [
    ];

    static function getBancas($bpe = false,$banca=false) {
        
        $sqlBPE = DB::table(DB::Raw("(SELECT 'BPE' AS BANCA, 'B. Pequeña Empresa' AS NOMBRE_BANCA) AS A"))
            ->select('A.BANCA','A.NOMBRE_BANCA');
        
        $sql = DB::table('WEBVPC_SSRS_COMBOS_EJECUTIVO AS A')
            ->select('A.BANCA','A.NOMBRE_BANCA')
            ->distinct();
        if ($bpe) {
            $sql = $sql->union($sqlBPE);
        }
        if ($banca) {
            $sql->where("BANCA",$banca);
        }
        return $sql;
    }
    
    static function getZonales($banca = null, $zonal = null) {
        $sql = DB::table('WEBVPC_SSRS_COMBOS_EJECUTIVO AS A')
            ->select('A.ZONAL','A.NOMBRE_ZONAL','A.BANCA')
            ->where('A.NOMBRE_ZONAL', '<>', '')
            ->distinct()
            ->orderBy('A.BANCA')
            ->orderBy('A.NOMBRE_ZONAL');
        if ($banca) {
            $sql = $sql->where('A.BANCA', '=', $banca);
        }
        if ($zonal) {
            $sql = $sql->where('A.ZONAL', '=', $zonal);
        }
        return $sql;
    }
    
    static function getJefaturas($banca = null, $zonal = null, $jefatura = null) {
        $sql = DB::table('WEBVPC_SSRS_COMBOS_EJECUTIVO AS A')
            ->select('A.JEFATURA', 'A.NOMBRE_JEFE')
            ->where('A.NOMBRE_JEFE', '<>', '')
            ->distinct()
            ->orderBy('A.JEFATURA');
        if ($banca) {
            $sql = $sql->where('A.BANCA', '=', $banca);
        }
        if ($zonal){
            $sql = $sql->where('A.ZONAL', '=', $zonal);
        }
        if ($jefatura) {
            $sql = $sql->where('A.JEFATURA', '=', $jefatura);
        }
        return $sql;
    }
    
    static function getEjecutivos($banca = null, $zonal = null, $jefatura = null){
        //AQUÍ
        $sql = DB::table('WEBVPC_SSRS_COMBOS_EJECUTIVO AS A')
            ->select('A.COD_SECT_UNIQ', DB::raw('COALESCE(A.NOMBRE_ENCARGADO, A.ENCARGADO) ENCARGADO'))
            ->where('A.ENCARGADO', '<>', '')
            ->where('A.COD_SECT_UNIQ', '<>', '')
            ->distinct();
        if ($banca){
            $sql = $sql->where('A.BANCA', '=', $banca);
        }
        
        if ($zonal){
            $sql = $sql->where('A.ZONAL', '=', $zonal);
        }
        
        if ($jefatura){
            $sql = $sql->where('A.JEFATURA', '=', $jefatura);
        }
        return $sql;
    }
    
    static function getFecha() {
        $sql = DB::table(DB::Raw("(select MAX(fecha) FECHA from [B29310T16W7\SQL2008].vp.dbo.saldo_diarios_resumen) AS A"))
        ->select('A.FECHA');

        return $sql;
    }
    
    static function getNombreEncargado($ejecutivo = null) {
        $sql = DB::table('WEBVPC_SSRS_COMBOS_EJECUTIVO AS A')
            ->select('A.ENCARGADO');
        
        if ($ejecutivo){
            $sql = $sql->where('A.COD_SECT_UNIQ', '=', $ejecutivo);
        }
        
        return $sql;
    }

    static function getByCodsectU($ejecutivo = null) {
        $sql = DB::table('WEBVPC_SSRS_COMBOS_EJECUTIVO AS A');
        
        if ($ejecutivo){
            $sql = $sql->where('A.COD_SECT_UNIQ', '=', $ejecutivo);
        }
        
        return $sql;
    }
}
