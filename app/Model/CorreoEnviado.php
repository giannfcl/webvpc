<?php

namespace App\Model;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class CorreoEnviado extends Model {

	function guardar($data)
	{
		if (!empty($data->cc)) {
			foreach ($data->cc as $key => $value) {
				$arraycopiados[]=$value['address'];
			}
		}
		if (!empty($data->_datos)) {
			foreach ($data->_datos as $key => $value) {
				if (!is_array($value)) {
					$arraydatos[]=$key.": ".$value;
				}else{
					foreach ($value as $k => $v) {
						if (!is_array($v)) {
							$arraydatos[]=$key."= ".$k.": ".$v;
						}
					}
				}
			}
		}
		// dd($data,$data->_datos,implode(";", $arraycopiados),implode(";", $arraydatos));
		return DB::table("CORREOS_ENVIADOS")->insert([
			"EMISOR"=>$data->from['0']['address'],
			"RECEPTORES"=>$data->_to['0'],
			"COPIADOS"=>implode(";", $arraycopiados),
			"ASUNTO"=>$data->_subject,
			"CONTENIDO"=>implode(";", $arraydatos)
		]);
	}

}