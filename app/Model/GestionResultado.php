<?php
namespace App\Model;
use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class GestionResultado extends Model

{
	protected $table = 'WEBVPC_GESTION_RESULTADO';
    
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey =['ID_RESULTADO_GESTION']  ;
					        

        /**
     * The name of the "created at" column.
     *
     * @var string
     */
    public $timestamps = false;
    
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'ID_RESULTADO_GESTION_N1',
        'ID_RESULTADO_GESTION_N2',
        'DESCRIPCION',
        'BANCA',
        'ESTADO',
        ];


    /**
     * Traer todos los tipos Nivel 2 de gestion - (BPE:Resultado) 
     *
     * @param string $banca Tipo de banca
     * @param char $estado Flag si la opción esta activa (1 y 0)
     * @return table
     */
    static function getNivel2($banca,$estado){
        $sql = DB::table('WEBVPC_RESULTADO_GESTION_N2')
                ->select('ID_RESULTADO_GESTION_N2 as id','DESCRIPCION as desc')
                ->where('BANCA','=', $banca)
                ->where('ESTADO', '=', $estado);
        return $sql;
    }

    /**
     * Traer todos los resultados de gestiones Nivel último - (BPE:Motivos) 
     *
     * @param string $banca Tipo de banca
     * @param char $estado Flag si la opción esta activa (1 y 0)
     * @param string $nivel2 Código del nivel 2 - (BPE:Resultado)
     * @return table
     */
    static function get($banca,$estado,$nivel2){
        $sql = DB::table('WEBVPC_RESULTADO_GESTION')
                ->select('ID_RESULTADO_GESTION as id','DESCRIPCION as desc')
                ->where('BANCA','=',$banca)
                ->where('ESTADO','=',$estado);
        if ($nivel2){
            $sql = $sql->where('ID_RESULTADO_GESTION_N2','=',$nivel2);
        }
        return $sql;
    }

    static function getResultados($campanhas){
        $sql = DB::table('WEBVPC_RESULTADO_GESTION_N2 as A')
                ->select('A.ID_RESULTADO_GESTION_N2 as id','A.DESCRIPCION as desc','B.ID_CAMP_EST')
                ->join('WEBVPC_CAMPANHA_RESULTADO as B','A.ID_RESULTADO_GESTION_N2','=','B.ID_RESULTADO_GESTION_N2')
                ->where('ESTADO', 1);
        if (is_array($campanhas)){
            $sql = $sql->whereIn('B.ID_CAMP_EST',$campanhas);
        }else{
            $sql = $sql->where('B.ID_CAMP_EST',$campanhas);
        }
        
        return $sql;   
    }
}
