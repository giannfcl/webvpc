<?php

namespace App\Model;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Reportes extends Model


{
    const NOMBRE_VISTA_GC='REPORTES_EXCEL';
    static function actualizar($datos)
    {
        DB::beginTransaction();
        $status = true;
        try {

            // dd($datos);
            $CATEGORIA = $datos['CATEGORIA'];
            $REPORTE = $datos['REPORTE'];
            $REPORTEANT = $datos['REPORTEANT'];
            $DESCRIPCION = $datos['DESCRIPCION'];
            $NIVEL = $datos['NIVEL'];
            $FECHA = $datos['FECHA'];
            $FILE = $datos['FILE'];
            $path = Storage::disk('local')->putFileAs('RGC/', $FILE,$datos['FILENAME']);
            DB::table('T_WEB_RELACION_REPORTES')
                ->where('CATEGORIA', $CATEGORIA)
                ->where('NIVEL', $NIVEL)
                ->where('REPORTE', $REPORTEANT)
                ->where('FLG_ACTIVO', 1)
                ->update([
                    'REPORTE' => $REPORTE, 'DESCRIPCION' => $DESCRIPCION,
                    'FECHA ACTUALIZACION' => $FECHA,
                    'NOMREPOR' => substr($path, 5,  strlen($path))
                ]);
            
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }

        return $status;
    }
    static function Reportes()
    {
        $sqlReportes = DB::table('T_WEB_RELACION_REPORTES')
            ->where('FLG_ACTIVO', 1)
            ->orderby("CATEGORIA", "NIVEL")
            ->orderby("PRIORIDAD")->get();

        $sqlCATEGORIA = DB::table('T_WEB_RELACION_REPORTES')
            ->select("CATEGORIA")
            ->distinct("CATEGORIA")
            ->where('FLG_ACTIVO', 1)
            ->orderby("CATEGORIA")
            ->whereNotNull("CATEGORIA")->get();
        $sqlNROREPORTES = DB::table('T_WEB_RELACION_REPORTES')
            ->select("NIVEL", "CATEGORIA", DB::raw('count(*) as FACTOR'))
            ->where('FLG_ACTIVO', 1)
            ->groupBy("CATEGORIA", "NIVEL")
            ->orderby("CATEGORIA")
            ->orderby("NIVEL")
            ->whereNotNull("NIVEL")
            ->get();

        $sqlNIVEL = DB::table('T_WEB_RELACION_REPORTES')
            ->select("NIVEL", "CATEGORIA")
            ->distinct("NIVEL", "CATEGORIA")
            ->where('FLG_ACTIVO', 1)
            ->orderby("CATEGORIA")
            ->orderby("NIVEL")
            ->whereNotNull("NIVEL")->get();

        $arr = array("CATEGORIAS" => json_decode($sqlCATEGORIA, true), "NIVEL" => json_decode($sqlNIVEL, true), "REPORTES" => json_decode($sqlReportes, true), "FACTOR" => json_decode($sqlNROREPORTES, true));
        return $arr;
    }

    static function getInfoEditor($registro)
    {
        $sql = DB::table('WEBVPC_ACCESOS_VISTAS')
                        ->where('NOMBRE_VISTA','=',self::NOMBRE_VISTA_GC)
                        ->where('FLG_ACTIVO','=',DB::Raw('1'))
                        ->where('REGISTRO','=',$registro)
                        ->get();
        return (!empty($sql) && count($sql)>0 ? true : false);
    }

    function guardardatosdescarga($data)
    {
        DB::beginTransaction();
        $status = true;
        try {

            DB::table('WEBVPC_USOREPORTESEXCEL')->insert($data);
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }

        return $status;
    }
}
