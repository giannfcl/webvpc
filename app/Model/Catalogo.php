<?php

namespace App\Model;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;


class Catalogo extends Model {

    static function get($catalogo,$codigo2 = null){
        $sql = DB::table('MAE_CATALOGO')
            ->where('CATALOGO',$catalogo);
        if ($codigo2){
            $sql = $sql->where('CODIGO_2',$codigo2);
        }
        return $sql->get();
    }
}
