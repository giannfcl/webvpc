<?php

namespace App\Model;

use DB;
use Illuminate\Database\Eloquent\Model;
use Log;

class CatCampanha extends Model {

    protected $table = 'WEBVPC_CAT_CAMPANHA';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'ID_CAMPANHA';

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'ABREV_CAMPANHA',
        'NOMBRE_CAMPANHA',
        'FECHA_CREACION',
        'ESTADO_CAMPANHA',
        'DESCRIPCION',
    ];

}
