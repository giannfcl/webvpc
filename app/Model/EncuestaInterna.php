<?php

namespace App\Model;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class EncuestaInterna extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'WEBVPC_EVALUACION_ANALISTA';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = null;
	public $incrementing = false;


    static function getAnalistasUsuario($registro){
            $sql=DB::table('WEBVPC_JEFES_ANALISTAS AS WJA')
            ->select('*',DB::Raw("SUBSTRING(NOMBRE,1, CHARINDEX(' ',NOMBRE)) AS PRIMER_NOMBRE"))
            ->leftJoin('WEBVPC_ANALISTA as WA', function($join){
                $join->on('WJA.REGISTRO_ANALISTA', '=', 'WA.REGISTRO');
            })
            ->where('WJA.REGISTRO_JEFE','=',$registro);

            return $sql;
    }

    static function getRegistroAnalistasUsuario($registro){
            $sql=DB::table('WEBVPC_JEFES_ANALISTAS AS WJA')
            ->select('REGISTRO_ANALISTA')
            ->where('WJA.REGISTRO_JEFE','=',$registro);

            return $sql->get();
    }
    
    static function getAnalistasUsuarioData($registro){
            $sql=DB::table('WEBVPC_JEFES_ANALISTAS AS WJA')
            ->select('*',DB::Raw("SUBSTRING(NOMBRE,1, CHARINDEX(' ',NOMBRE)) AS PRIMER_NOMBRE"),DB::Raw("ROW_NUMBER() OVER(ORDER BY WA.AREA,WA.NOMBRE) AS RNK"))
            ->leftJoin('WEBVPC_ANALISTA as WA', function($join){
                $join->on('WJA.REGISTRO_ANALISTA', '=', 'WA.REGISTRO');
                $join->on('WA.ESTADO', '=', DB::Raw("'1'"));
            })
            ->leftJoin('WEBVPC_EVALUACION_ANALISTA as EV', function($join){
                $join->on('WJA.REGISTRO_ANALISTA', '=', 'EV.REGISTRO_ANALISTA');
                $join->on('WJA.REGISTRO_JEFE', '=', 'EV.REGISTRO_JEFE');
                $join->on('EV.FLG_ULTIMO', '=', DB::Raw('1'));
            })
            ->where('WJA.REGISTRO_JEFE','=',$registro)
            ->orderBy('WA.AREA','asc')
            ->orderBy('WA.NOMBRE','asc');
            // dd($sql->toSql());
            
            return $sql;
    }
    static function getPreguntaGerencia($registro){
            $sql=DB::table('WEBVPC_PREGUNTA_GERENCIA AS WPG')
            ->select()            
            ->where('WPG.REGISTRO_EVALUADOR','=',$registro)
            ->where('WPG.FLG_ULTIMO','=',1);


            if(count($sql->get())==0)
                return null;
            else 
                return $sql->first();
    }
    static function getAvance(){
       $sql=DB::table(DB::Raw("(SELECT WJA.REGISTRO_JEFE,COUNT(WJA.REGISTRO_ANALISTA) NUM_A_EVALUAR,
                            CASE WHEN COUNT(WEA.REGISTRO_ANALISTA)=0 THEN 0 ELSE 1 END FLG_ENCUESTA,
                            COUNT(CASE WHEN FLG_RECONOCE=1 THEN WEA.REGISTRO_ANALISTA ELSE NULL END) CONOCE,
                            COUNT(CASE WHEN FLG_RECONOCE=0 THEN WEA.REGISTRO_ANALISTA ELSE NULL END) NO_CONOCE FROM WEBVPC_JEFES_ANALISTAS WJA
                            LEFT JOIN WEBVPC_EVALUACION_ANALISTA WEA ON WEA.REGISTRO_JEFE=WJA.REGISTRO_JEFE 
                            AND WEA.REGISTRO_ANALISTA=WJA.REGISTRO_ANALISTA AND WEA.FLG_ULTIMO=1
                            GROUP BY WJA.REGISTRO_JEFE
                            ) AS A"))
        ->select()
        ->leftJoin('WEBVPC_USUARIO AS WU', function($join){
            $join->on('A.REGISTRO_JEFE', '=', 'WU.REGISTRO');
        })            
        ->orderBy('A.FLG_ENCUESTA','DESC')
        ->orderBy('A.NUM_A_EVALUAR','DESC')
        ->orderBy('WU.NOMBRE','ASC');
        return $sql;
    }

    static function getPromedioGeneral(){
       $sql=DB::table(DB::Raw("(SELECT ISNULL((AVG(CONVERT(FLOAT,PUNTAJE))+AVG(CONVERT(FLOAT,PUNTAJE_2)))/2,0) PROMEDIO_GENERAL,
                                COUNT(*) PERSONAS FROM WEBVPC_PREGUNTA_GERENCIA WHERE FLG_ULTIMO=1
                            ) AS A"))
        ->select();
        //DD($sql->get());
        return $sql;
    }

    static function getResumen(){
           $sql=DB::table(DB::Raw('( SELECT REGISTRO_ANALISTA,COUNT(*) AS TOTAL_EVALUADORES FROM WEBVPC_JEFES_ANALISTAS
                                    GROUP BY REGISTRO_ANALISTA) AS EVA'))
            ->select('WA.NOMBRE','WA.AREA','A.*',DB::Raw('CONVERT(FLOAT,(PREGUNTA_1+PREGUNTA_2+PREGUNTA_3))/3 AS PUNTAJE_PROMEDIO'),'EVA.TOTAL_EVALUADORES')
            ->join('WEBVPC_ANALISTA AS WA', function($join){
                $join->on('EVA.REGISTRO_ANALISTA', '=', 'WA.REGISTRO');
            }, null,null,'full outer')
            ->leftJoin(DB::Raw("(SELECT REGISTRO_ANALISTA,SUM(FLG_RECONOCE) AS RECONOCIDO, COUNT(FLG_RECONOCE)-SUM(FLG_RECONOCE) AS NO_RECONOCIDO,
                        SUM(CASE WHEN FLG_RECONOCE=1 THEN PREGUNTA_1*1.0 ELSE 0 END)/(CASE WHEN SUM(FLG_RECONOCE)>0 THEN SUM(FLG_RECONOCE) ELSE 1 END)  PREGUNTA_1,
                        SUM(CASE WHEN FLG_RECONOCE=1 THEN PREGUNTA_2*1.0 ELSE 0 END)/(CASE WHEN SUM(FLG_RECONOCE)>0 THEN SUM(FLG_RECONOCE) ELSE 1 END) PREGUNTA_2,
                        SUM(CASE WHEN FLG_RECONOCE=1 THEN PREGUNTA_3*1.0 ELSE 0 END)/(CASE WHEN SUM(FLG_RECONOCE)>0 THEN SUM(FLG_RECONOCE) ELSE 1 END) PREGUNTA_3
                        FROM WEBVPC_EVALUACION_ANALISTA WHERE FLG_ULTIMO=1 GROUP BY REGISTRO_ANALISTA) AS A"), function($join){
                $join->on('EVA.REGISTRO_ANALISTA', '=', 'A.REGISTRO_ANALISTA');
            })
            ->where('WA.ESTADO','=',1)
            ->orderBy('AREA','ASC')
            ->orderBy(DB::Raw('CONVERT(FLOAT,(PREGUNTA_1+PREGUNTA_2+PREGUNTA_3))/3'),'DESC')
            ->orderBy('NOMBRE','ASC');
            //dd($sql->get());
            Log::error($sql->toSQL());
            return $sql;
    }

    static function getPreguntas(){
           $sql=DB::table('WEBVPC_PREGUNTAS_ENCUESTA AS PRE')
            ->select();
            return $sql;
    }

    static function getPuntaje(){
           $sql=DB::table('WEBVPC_PUNTAJE AS PUN')
            ->select();
            return $sql;
    }


    function guardarResultados($registro,$analistas,$data,$dataGeneral){
        //dd($registro,$analistas,$data);
        DB::beginTransaction();
        $status = true;      
        try {
            DB::table('WEBVPC_EVALUACION_ANALISTA')
                    ->where('REGISTRO_JEFE','=',$registro)
                    ->whereIn('REGISTRO_ANALISTA',$analistas)
                    ->update(['FLG_ULTIMO' => "0"]);

            DB::table('WEBVPC_PREGUNTA_GERENCIA')
                    ->where('REGISTRO_EVALUADOR','=',$registro)
                    ->update(['FLG_ULTIMO' => "0"]);

            DB::table('WEBVPC_EVALUACION_ANALISTA')->insert($data);
            
            if(isset($dataGeneral['PUNTAJE']))
                DB::table('WEBVPC_PREGUNTA_GERENCIA')->insert($dataGeneral);
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }

}