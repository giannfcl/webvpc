<?php

namespace App\Model\AsignacionResectorizacion;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use App\Model\BE\Lead as Lead;
use Jenssegers\Date\Date as Carbon;


class Reasignacion extends Model {
    /* Se puede crear una función para que inserte dado un nombre de tabla y un archivo */

    function guardarReasignacion($resectorizar ,$archivo1, $ruta1,$archivo2,$ruta2) {
        DB::beginTransaction();
        $status = true;
        try {               
            if($resectorizar){
                if (empty($resectorizar['COD_SECT_O']) && !empty($resectorizar['REG_EJECUTIVO_O'])) {
                    $resectorizar['COD_SECT_O']=!empty($this->getCodSectorista($resectorizar['REG_EJECUTIVO_O'])->COD_CORTO) ? $this->getCodSectorista($resectorizar['REG_EJECUTIVO_O'])->COD_CORTO : NULL;
                }
                DB::table('WEBVPC_RESC_ASIG_NUEVAS')->insert($resectorizar);
               if($archivo1){ 
                    $archivo1->storeAs($ruta1, $resectorizar['ADJUNTO1']);
                }
               if($archivo2){ 
                    $archivo2->storeAs($ruta2, $resectorizar['ADJUNTO2']);
                }
            }                
            DB::commit();
        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }

        return $status;
    }


    function getSectoristaArbol($login)
    {
        $sql = DB::table('ARBOL_DIARIO_2 as AD')
                ->where('LOGIN','=',$login)
                ->where('EN','<>',DB::Raw("'SIN CLIENTES'"))
                ->get()
                ->first();
        return $sql;
    }

    function getLeadByNum($numdoc,$periodo)
    {
        $sql = DB::table('WEBBE_LEAD as WL')
                ->where('NUM_DOC','=',$numdoc)
                ->where('PERIODO','=',$periodo)
                ->get()
                ->first();
        return $sql;
    }

    function getCampaña($numdoc,$periodo)
    {
        $sql = DB::table('WEBVPC_LEAD_CAMP_EST as WL')
                ->join(DB::raw("(SELECT * FROM WEBVPC_CAMP_EST WHERE BANCA='BE' AND TIPO='PROSPECTOS') as WCE"),function($join)
                {
                    $join->on('WL.ID_CAMP_EST', '=', 'WCE.ID_CAMP_EST');
                })
                ->where('WL.NUM_DOC','=',$numdoc)
                ->where('WL.PERIODO','=',$periodo)
                ->get()
                ->first();
        return $sql;
    }

    function guardarNuevoReferido($existelead,$lead,$leadCampanha,$leadsec,$leadetapa,$actividad)
    {
        DB::beginTransaction();
        $status = true;
        try {
            if (!$existelead) {
                DB::table('WEBBE_LEAD')->insert($lead);
                DB::table('WEBVPC_LEAD_CAMP_EST')->insert($leadCampanha);
            }
            DB::table('WEBBE_LEAD_SECTORISTA')->insert($leadsec);
            DB::table('WEBBE_LEAD_ETAPA')->insert($leadetapa);
            DB::table('WEBBE_ACTIVIDAD')->insert($actividad);

            DB::commit();

        } catch (\Exception $e) {
            Log::error('BASE_DE_DATOS|' . $e->getMessage());
            $status = false;
            DB::rollback();
        }
        return $status;
    }


    function guardarCorreoReferido($data,$correo,$ruta)
    {
        if ($correo) {
            $correo->storeAs($ruta, $data['CONFORMIDAD_JEFE']);
        }
        $sql=DB::table('ADJUNTOS_REFERIDOS')->insert($data);
        return $sql;
    }

    function getCodSectorista($registro)
    {
         $sql=DB::table('ARBOL_DIARIO_2 AS ARB')
                ->select('LOGIN','COD_SECT_LARGO','COD_CORTO','EN','ENCARGADO')
                ->where('LOGIN','=',$registro)
                ->wherein('TIPO',array('EN','GERENTE DE TIENDA CON META'))
                ->where('LOGIN','!=','')
                ->where('EN','<>',DB::Raw("'SIN CLIENTES'"))
                ->orderBy('COD_CORTO','!=','')
                ->get();
        return $sql;
    }
}
