<?php

namespace App\Model\MC;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class MiCumplimiento extends Model {
    static function getPeriodos()
    {
        $sql = DB::Table('METAS_AVANCE_NOTA')
                ->select('periodo')
                ->groupBy('periodo')
                ->orderby('periodo','desc')
                ->get();
        return !empty($sql) ? $sql : '';
    }

    static function getBancas()
    {
        $sql = DB::Table('METAS_AVANCE_NOTA')
                ->select('banca')
                ->groupBy('banca')
                ->orderby('banca','asc')
                ->get();
        return !empty($sql) ? $sql : '';
    }

    static function getZonales($banca)
    {
        if (!empty($banca)) {
            if ($banca == 'BC') {
                $sql = DB::Table('METAS_AVANCE_NOTA')
                ->select('COD_SECT')
                ->where('banca','=',$banca)
                ->where('COD_SECT','like','%BC %')
                ->groupBy('COD_SECT')
                ->orderby('COD_SECT','asc')
                ->get();

                $combo = "<option value='todos'>TODOS</option>";
                foreach ($sql as $key => $value) {
                    $combo .= "<option value='".$value->COD_SECT."'>".$value->COD_SECT."</option>";
                }
            }else if ($banca == 'BEL'){
                $sql = DB::Table('METAS_AVANCE_NOTA')
                ->select('COD_SECT')
                ->where('banca','=',$banca)
                ->where('COD_SECT','like','%BEL%')
                ->groupBy('COD_SECT')
                ->orderby('COD_SECT','asc')
                ->get();

                $combo = "<option value='todos'>TODOS</option>";
                foreach ($sql as $key => $value) {
                    $combo .= "<option value='".$value->COD_SECT."'>".$value->COD_SECT."</option>";
                }
            }else if ($banca == 'BEP'){
                $sql = DB::Table('METAS_AVANCE_NOTA')
                ->select('COD_SECT')
                ->where('banca','=',$banca)
                ->where('COD_SECT','like','%BEP%')
                ->groupBy('COD_SECT')
                ->orderby('COD_SECT','asc')
                ->get();

                $combo = "<option value='todos'>TODOS</option>";
                foreach ($sql as $key => $value) {
                    $combo .= "<option value='".$value->COD_SECT."'>".$value->COD_SECT."</option>";
                }
            }
        }
        return !empty($combo) ? $combo : '';
    }

    static function lista($banca,$zonal,$periodo){
        if (!empty($banca)) {
            $sql = DB::Table('base_resumen')
                    ->select(DB::Raw('LTRIM(RTRIM(PERIODO)) AS PERIODO'),DB::Raw('LTRIM(RTRIM(BANCA)) AS BANCA'),DB::Raw('LTRIM(RTRIM(GRUPO)) AS GRUPO'),'EJECUTIVO','COD_SECT',DB::Raw('ROUND(SUM(NOTA)*100,2) AS NOTATOTAL'))
                    ->where('BANCA','=',$banca)
                    ->where('PERIODO','=',$periodo);
            if ($zonal != 'todos') {
                if ($zonal=='BC GRUPO 1') {
                    $sql=$sql->where('GRUPO','=','GRUPO 1');
                }
                if ($zonal=='BC GRUPO 2') {
                    $sql=$sql->where('GRUPO','=','GRUPO 2');
                }
                if ($zonal=='BEL ZONAL 1') {
                    $sql=$sql->where('GRUPO','=','SAN ISIDRO');
                }
                if ($zonal=='BEL ZONAL 2') {
                    $sql=$sql->where('GRUPO','=','CHACARILLA');
                }
                if ($zonal=='BEL ZONAL 3') {
                    $sql=$sql->where('GRUPO','=','MIRAFLORES');
                }
                if ($zonal=='BEP ZONAL 1') {
                    $sql=$sql->where('GRUPO','=','NORTE');
                }
                if ($zonal=='BEP ZONAL 2') {
                    $sql=$sql->where('GRUPO','=','CENTRO ORIENTE');
                }
                if ($zonal=='BEP ZONAL 3') {
                    $sql=$sql->where('GRUPO','=','SUR');
                }
            }elseif ($zonal == 'todos' && $banca=='BC'){
                $sql=$sql->where('COD_SECT','LIKE','%BC%');
            }elseif ($zonal == 'todos' && $banca=='BEL') {
                $sql=$sql->where('COD_SECT','LIKE','%BEL%');
            }elseif ($zonal == 'todos' && $banca=='BEP') {
                $sql=$sql->where('COD_SECT','LIKE','%BEP%');
            }
            $sql = $sql->groupBy('COD_SECT','EJECUTIVO','JEFE','PERIODO','GRUPO','BANCA')
                        ->orderby('JEFE','DESC')
                        ->orderby('NOTATOTAL','DESC')->get();
        }
        return !empty($sql) ? $sql : '';
    }

    static function getIndicadores($banca,$zonal,$periodo,$ejecutivos,$jefaturas=null){
        if (!empty($banca) && !empty($zonal) && !empty($periodo) && !empty($ejecutivos)) {
            if ($ejecutivos=='todos' && $banca='BC') {
                $jefe = DB::Table('base_resumen')
                        ->select(DB::Raw('DISTINCT EJECUTIVO'))
                        ->where('JEFE','=',1)
                        ->where('banca','=',$banca)
                        ->where('periodo','=',$periodo)
                        ->where('COD_SECT','=',$zonal)
                        ->get();
                        //dd($jefe[0]->EJECUTIVO);
                $ejecutivos=$jefe[0]->EJECUTIVO;
            }else if($ejecutivos=='todos' && $banca='BEL'){
            }
            $sql['financiera']= DB::Table('base_resumen')
                        ->select('INDICADOR','UNIDADES DE MEDICION AS UNIDADES_MEDICION',DB::Raw('CONVERT(VARCHAR, CAST(ROUND(AVANCE,2) AS MONEY), 1) AS AVANCE'),DB::Raw('CONVERT(VARCHAR, CAST(ROUND(META,2) AS MONEY), 1) AS META'),DB::Raw('CONVERT(VARCHAR, CAST(ROUND(AVANCE-META,2) AS MONEY), 1) AS DIFERENCIA'),DB::Raw('CONVERT(VARCHAR, CAST(ROUND(CUMPLIMIENTO,2)*100 AS MONEY), 1) AS CUMPLIMIENTO'),DB::Raw('CONVERT(VARCHAR, CAST(ROUND(PESO,2)*100 AS MONEY), 1) AS PESO'),DB::Raw('CONVERT(VARCHAR, CAST(ROUND(NOTA,2)*100 AS MONEY), 1) AS NOTA'))
                        ->where('banca','=',$banca)
                        ->where('periodo','=',$periodo)
                        ->where('ejecutivo','=',$ejecutivos)
                        ->where('PERSPECTIVA','=','Financiero')
                        ->get();

            $sql['ecosistema']= DB::Table('base_resumen')
                        ->select('INDICADOR','UNIDADES DE MEDICION AS UNIDADES_MEDICION',DB::Raw('CONVERT(VARCHAR, CAST(ROUND(AVANCE,2) AS MONEY), 1) AS AVANCE'),DB::Raw('CONVERT(VARCHAR, CAST(ROUND(META,2) AS MONEY), 1) AS META'),DB::Raw('CONVERT(VARCHAR, CAST(ROUND(AVANCE-META,2) AS MONEY), 1) AS DIFERENCIA'),DB::Raw('CONVERT(VARCHAR, CAST(ROUND(CUMPLIMIENTO,2)*100 AS MONEY), 1) AS CUMPLIMIENTO'),DB::Raw('CONVERT(VARCHAR, CAST(ROUND(PESO,2)*100 AS MONEY), 1) AS PESO'),DB::Raw('CONVERT(VARCHAR, CAST(ROUND(NOTA,2)*100 AS MONEY), 1) AS NOTA'))
                        ->where('banca','=',$banca)
                        ->where('periodo','=',$periodo)
                        ->where('ejecutivo','=',$ejecutivos)
                        ->where('PERSPECTIVA','=','Ecosistema')
                        ->get();

            $sql['aceleradores']= DB::Table('base_resumen')
                        ->select('INDICADOR','UNIDADES DE MEDICION AS UNIDADES_MEDICION',DB::Raw('CONVERT(VARCHAR, CAST(ROUND(AVANCE,2) AS MONEY), 1)AS AVANCE'),DB::Raw('CONVERT(VARCHAR, CAST(ROUND(META,2) AS MONEY), 1) AS META'),DB::Raw('CONVERT(VARCHAR, CAST(ROUND(AVANCE-META,2) AS MONEY), 1) AS DIFERENCIA'),DB::Raw('CONVERT(VARCHAR, CAST(ROUND(CUMPLIMIENTO,2)*100 AS MONEY), 1) AS CUMPLIMIENTO'),DB::Raw('CONVERT(VARCHAR, CAST(ROUND(PESO,2)*100 AS MONEY), 1) AS PESO'),DB::Raw('CONVERT(VARCHAR, CAST(ROUND(NOTA,2)*100 AS MONEY), 1) AS NOTA'))
                        ->where('banca','=',$banca)
                        ->where('periodo','=',$periodo)
                        ->where('ejecutivo','=',$ejecutivos)
                        ->where('PERSPECTIVA','=','Aceleradores')
                        ->get();
        }
        return isset($sql) ? $sql : '';
    }

    static function getEjecutivos($bancas,$cod_sect){
        if (!empty($bancas)) {
            if ($bancas == 'BC') {
                $grupo='';
                if($cod_sect=='BC GRUPO 1'){
                    $grupo='GRUPO 1';
                    $sql = DB::Table('base_resumen')
                    ->select(DB::Raw('DISTINCT EJECUTIVO'))
                    ->where('banca','=',$bancas)
                    ->where('grupo','=',$grupo)
                    ->get();
                }else if ($cod_sect=='BC GRUPO 2'){
                    $grupo='GRUPO 2';
                    $sql = DB::Table('base_resumen')
                    ->select(DB::Raw('DISTINCT EJECUTIVO'))
                    ->where('banca','=',$bancas)
                    ->where('grupo','=',$grupo)
                    ->get();
                }
                if (!empty($sql)){
                    $combo = "<option value='todos'>TODOS</option>";
                    foreach ($sql as $key => $value) {
                        $combo .= "<option value='".$value->EJECUTIVO."'>".$value->EJECUTIVO."</option>";
                    }
                }
            }
        }
        return isset($combo) ? $combo : '';
    }

    static function getJefaturas($bancas,$cod_sect)
    {
        if (!empty($bancas)) {
            if ($bancas == 'BEL') {
                $grupo='';
                if($cod_sect=='BEL ZONAL 2'){
                    $grupo='SAN ISIDRO';
                    $sql = DB::Table('base_resumen')
                    ->select(DB::Raw('DISTINCT EJECUTIVO'))
                    ->where('banca','=',$bancas)
                    ->where('grupo','=',$grupo)
                    ->where('COD_SECT','LIKE','JEL%')
                    ->get();
                }else if ($cod_sect=='BEL ZONAL 3'){
                    $grupo='MIRAFLORES';
                    $sql = DB::Table('base_resumen')
                    ->select(DB::Raw('DISTINCT EJECUTIVO'))
                    ->where('banca','=',$bancas)
                    ->where('grupo','=',$grupo)
                    ->where('COD_SECT','LIKE','JEL%')
                    ->get();
                }else if ($cod_sect=='BEL ZONAL 1'){
                    $grupo='CHACARILLA';
                    $sql = DB::Table('base_resumen')
                    ->select(DB::Raw('DISTINCT EJECUTIVO'))
                    ->where('banca','=',$bancas)
                    ->where('grupo','=',$grupo)
                    ->where('COD_SECT','LIKE','JEL%')
                    ->get();
                }
                if (!empty($sql)){
                    $combo = "<option value='todos'>TODOS</option>";
                    foreach ($sql as $key => $value) {
                        $combo .= "<option value='".$value->EJECUTIVO."'>".$value->EJECUTIVO."</option>";
                    }
                }
            }
        }
        return isset($combo) ? $combo : '';
    }

}
