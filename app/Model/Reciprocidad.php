<?php

namespace App\Model;

use Jenssegers\Date\Date as Carbon;
use DB;
use Log;
use Illuminate\Database\Eloquent\Model;

class Reciprocidad extends Model
{
    static function buscarNumDoc($cuBuscar,$registro){

        if($cuBuscar){
            $sql=DB::table(DB::Raw("(SELECT B.COD_UNICO, RTRIM(LTRIM(REPLACE(B.NOMBRE,CHAR(13),' ')))NOMBRE, A.REGISTRO_EN
  FROM  WEBBE_LEAD_SECTORISTA  A 
  LEFT JOIN WEBBE_LEAD B 
    ON A.PERIODO = B.PERIODO 
   AND A.NUM_DOC = B.NUM_DOC 
 WHERE A.PERIODO = (SELECT MAX(PERIODO) FROM WEBBE_LEAD)
   AND B.COD_UNICO IS NOT NULL 
   AND A.FLG_ULTIMO=1) AS A"))
            ->select()
            ->where('COD_UNICO','=',$cuBuscar)
            ->where('REGISTRO_EN','=',$registro);
        }
        return $sql;
    }

    static function Resultados_simular($datos){
        if($datos["Codunicollevar"]){
          $sql=DB::table(DB::Raw("(SELECT * FROM (
            select COD_UNICO,TIPO,SUBTIPO,PERIODO,SUM(MONTO_TRANSAC_SOL)IMPORTE_REAL from WEB_TRANSACCION B
            GROUP BY B.PERIODO,b.COD_UNICO,b.TIPO,b.SUBTIPO)A) as Tabla_final"));
        }

        $sql=$sql->WHERE('COD_UNICO',$datos["Codunicollevar"]);
        $sql=$sql->WHERE('TIPO',$datos["tipollevar"]);
        $sql=$sql->WHERE('SUBTIPO',$datos["subtipollevar"]);
        $sql=$sql->WHERE('PERIODO','>=',$datos["desdellevar"]);
        $sql=$sql->WHERE('PERIODO','<=',$datos["hastallevar"]);
        $sql=$sql->get();
        return $sql;
    }

    function getTiposCompromiso()
    {
        return $sql=DB::table(DB::Raw("(SELECT A.* FROM WEB_CATALOGO_SUBTIPO_TRANSAC  A
                                        WHERE TIPO<>'O')A"))->get();
    }

    function getTiposCompromiso2()
    {
         return $sql=DB::table(DB::Raw("(SELECT A.* FROM WEB_CATALOGO_SUBTIPO_TRANSAC  A
                                        WHERE TIPO<>'I')A"))->get();
    }

    static function Tabla_transacciones($datos=null,$registro)
    {
        $sql=DB::table(DB::Raw("(SELECT DISTINCT A.COD_UNICO,A.TIPO,A.SUBTIPO,A.FECHA_DESDE,A.FECHA_HASTA,
                A.ULTIMO_MES,A.IMPORTE_PROY_MENSUAL,A.IMPORTE_REAL_ULTIMO_MES,
                CUMPLIMIENTO_ULTIMO_MES,
                CAST(A.CUMPLIMIENTO_ULTIMO_MES AS VARCHAR)+'%' as CUMPLIMIENTO_ULTIMO_MES2,
                A.IMPORTE_PROY_ACUMULADO,
                A.IMPORTE_REAL_ACUMULADO,
                CUMPLIMIENTO_ACUMULADO,
                CASE WHEN A.CUMPLIMIENTO_ACUMULADO IS NULL THEN '0.00%' ELSE 
                CAST(A.CUMPLIMIENTO_ACUMULADO AS VARCHAR)+'%' END CUMPLIMIENTO_ACUMULADO2,
                A.MESES_OBJETIVO,A.MESES_CUMPLIMIENTO,
                B.DESCRIPCION,C.NOMBRE, C.REGISTRO_EN,
        CASE WHEN A.TIPO='I' THEN 'CASH IN' ELSE 'CASH OUT' END TIPO_FINAL
 FROM WEB_RECIP_COMPROMISOS_DETALLE A
 LEFT JOIN WEB_CATALOGO_SUBTIPO_TRANSAC B 
   ON A.SUBTIPO = B.CODIGO
 LEFT JOIN (SELECT B.COD_UNICO, RTRIM(LTRIM(REPLACE(B.NOMBRE,CHAR(13),' ')))NOMBRE, A.REGISTRO_EN
			  FROM WEBBE_LEAD_SECTORISTA  A 
			  LEFT JOIN WEBBE_LEAD B 
				ON A.PERIODO = B.PERIODO 
			   AND A.NUM_DOC = B.NUM_DOC 
			 WHERE A.PERIODO = (SELECT MAX(PERIODO) FROM WEBBE_LEAD)
			   AND B.COD_UNICO IS NOT NULL 
			   AND A.FLG_ULTIMO=1)C 
   ON A.COD_UNICO = C.COD_UNICO) as TABLA_FINAL"));

        if ($datos!=null) {
            $sql=$sql->whereIn('REGISTRO_EN',$registro);
            $sql=$sql->where('COD_UNICO',$datos["codUnico"])
                     ->orWhere('NOMBRE', $datos["RazonSocial"]);
        }
        else{
            $sql=$sql->where('COD_UNICO',' ')
                     ->orWhere('NOMBRE',' ');
        }

        if ($registro!=null) {
            $sql=$sql->whereIn('REGISTRO_EN',$registro);
        }
        //dd($sql);
        $sql=$sql->get();

        return $sql;
    }
    
    static function Tabla_transacciones2()
    {
        return $sql=DB::table(DB::Raw("(SELECT A.*,CAST(A.CUMPLIMIENTO AS VARCHAR)+'%' as CUMPLIMIENTO2,CAST(A.CUMPLIMIENTO_ACUMULADO AS VARCHAR)+'%' as CUMPLIMIENTO_ACUMULADO2 FROM T_RATING_COMPROMISOS_FINAL A WHERE RN_TOP<=5)A
                                        ORDER BY RN_TOP ASC    "))->get();
    }
    static function Tabla_transacciones3()
    {
        return $sql=DB::table(DB::Raw("(SELECT A.*,CAST(A.CUMPLIMIENTO AS VARCHAR)+'%' as CUMPLIMIENTO2,CAST(A.CUMPLIMIENTO_ACUMULADO AS VARCHAR)+'%' as CUMPLIMIENTO_ACUMULADO2 FROM T_RATING_COMPROMISOS_FINAL A WHERE RN_BOTTOM<=5)A ORDER BY RN_BOTTOM ASC"))->get();
    }

    static function guardarDatos($datos,$registro)
    {
        // dd($datos);

        if ($datos) {
            $data=[
                'COD_UNICO'=>$datos['Codunicollevar'],
                'TIPO'=>$datos['tipollevar'],
                'SUBTIPO'=>$datos['subtipollevar'],
                'FECHA_DESDE'=>$datos['desdellevar'],
                'FECHA_HASTA'=>$datos['hastallevar'],
                'NOMBRE_CLIENTE'=>$datos['empresallevar'],
                'CONCEPTO'=>$datos['conceptollevar'],
                'IMPORTE_PROY_MENSUAL'=>$datos['importellevar'],
                'USUARIO'=>$registro
            ];
            DB::table("WEB_RECIP_COMPROMISOS")->insert($data);
        }

        $parametro= "'".$datos['Codunicollevar']."'";
        $reg['REGISTRO_EN']=$registro;
        DB::table("T_WEBVPC_REGISTRO_RATING_COMPROMISOS")->delete();
        DB::table("T_WEBVPC_REGISTRO_RATING_COMPROMISOS")->insert($reg);
        DB::update("exec SP_RECIP_COMPROMISOS_DETALLE $parametro ");
        DB::update("exec SP_RATING_COMPROMISOS");
        return true;
    }

    static function getUsuario2($banca = null, $zonal = null, $jefe = null, $ejecutivo = null)
    {

        $sql = DB::table('T_WEB_ARBOL_DIARIO')
            ->select('ENCARGADO', 'BANCA', 'NOMBRE_ZONAL', 'NOMBRE_JEFE', 'REGISTRO')
            ->distinct();

        // if ($banca != 'null') {
        //     $sql = $sql->where('BANCA', $banca);
        // } else {
        //     $sql = $sql->where('BANCA', null);
        // }

        if ($zonal != 'null') {
            $sql = $sql->where('NOMBRE_ZONAL', $zonal);
        } else {
            $sql = $sql->where('NOMBRE_ZONAL', null);
        }

        
        if ($jefe != 'null') {
            $sql = $sql->where('NOMBRE_JEFE', $jefe);
        } else {
            $sql = $sql->where('NOMBRE_JEFE', null);
        }

        if ($ejecutivo != 'null') {
            $sql = $sql->where('ENCARGADO', $ejecutivo);
        } else {
            $sql = $sql->where('ENCARGADO', null);
        }
        return $sql->select('REGISTRO')->distinct()->first();
    }
    static function Registros_finales($datos=null)
    {
        $REGISTRO="'".$datos."'";
        $sql=DB::SELECT(DB::Raw("(SELECT DISTINCT REGISTRO FROM T_WEB_ARBOL_DIARIO 
        WHERE ENCARGADO=(
        SELECT ENCARGADO FROM T_WEB_ARBOL_DIARIO
        WHERE REGISTRO=$REGISTRO)
        UNION
        SELECT DISTINCT REGISTRO FROM T_WEB_ARBOL_DIARIO 
        WHERE NOMBRE_JEFE=(
        SELECT NOMBRE_JEFE FROM T_WEB_ARBOL_DIARIO
        WHERE REGISTRO=$REGISTRO AND  ENCARGADO IS NULL)    
        UNION
        SELECT DISTINCT REGISTRO FROM T_WEB_ARBOL_DIARIO 
        WHERE NOMBRE_ZONAL=(
        SELECT NOMBRE_ZONAL FROM T_WEB_ARBOL_DIARIO
        WHERE REGISTRO=$REGISTRO AND  ENCARGADO IS NULL  AND NOMBRE_JEFE IS NULL)
        UNION
        SELECT DISTINCT REGISTRO FROM T_WEB_ARBOL_DIARIO 
        WHERE BANCA=(
        SELECT BANCA FROM T_WEB_ARBOL_DIARIO
        WHERE REGISTRO=$REGISTRO AND ENCARGADO IS NULL AND NOMBRE_ZONAL IS NULL AND NOMBRE_JEFE IS NULL))"));
        $sql=$sql;
        return $sql;
    }

    static function Razones_sociales($registro)
    {             
         $sql=DB::table(DB::Raw("(SELECT B.COD_UNICO, RTRIM(LTRIM(REPLACE(B.NOMBRE,CHAR(13),' ')))NOMBRE, A.REGISTRO_EN
  FROM  WEBBE_LEAD_SECTORISTA  A 
  LEFT JOIN WEBBE_LEAD B 
    ON A.PERIODO = B.PERIODO 
   AND A.NUM_DOC = B.NUM_DOC 
 WHERE A.PERIODO = (SELECT MAX(PERIODO) FROM WEBBE_LEAD)
   AND B.COD_UNICO IS NOT NULL 
   AND A.FLG_ULTIMO=1)A"));
         $sql=$sql->WhereIn("REGISTRO_EN",$registro);
         $sql=$sql->get();
         return $sql;
    }

        static function guardarDatos2($datos,$registro)
    {
        // dd($datos);

        if ($datos) {
            $data=[
                'COD_UNICO'=>$datos['Codunicollevar'],
                'TIPO'=>$datos['tipollevar'],
                'SUBTIPO'=>$datos['subtipollevar'],
                'FECHA_DESDE'=>$datos['desdellevar'],
                'FECHA_HASTA'=>$datos['hastallevar'],
                'NOMBRE_CLIENTE'=>$datos['empresallevar'],
                'CONCEPTO'=>$datos['conceptollevar'],
                'IMPORTE_PROY_MENSUAL'=>$datos['importellevar'],
                'USUARIO'=>$registro
            ];
            DB::table("WEB_RECIP_COMPROMISOS_EVALUAR")->insert($data);
        }   
        return $datos;
    }

    static function rating($registro)
    {
        //$val=$registro;
        $registro=array_values($registro);
         DB::table("T_WEBVPC_REGISTRO_RATING_COMPROMISOS")->delete();
        for ($i=0; $i < sizeof($registro) ; $i++) { 
            $reg=[];
            $reg['REGISTRO_EN']=$registro[$i];
        
            DB::table("T_WEBVPC_REGISTRO_RATING_COMPROMISOS")->insert($reg);
        };
        
        DB::update("exec SP_RATING_COMPROMISOS ");
    }
}
